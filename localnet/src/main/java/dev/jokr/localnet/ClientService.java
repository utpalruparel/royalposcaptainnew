package dev.jokr.localnet;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.IBinder;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.text.TextUtils;
import android.text.format.Formatter;
import android.util.Log;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;

import dev.jokr.localnet.discovery.models.DiscoveryReply;
import dev.jokr.localnet.models.IncomingServerMessage;
import dev.jokr.localnet.models.Payload;
import dev.jokr.localnet.models.RegisterMessage;
import dev.jokr.localnet.models.SessionMessage;
import dev.jokr.localnet.utils.MessageType;

/**
 * Created by JoKr on 8/29/2016.
 */
public class ClientService extends Service implements ClientSocketThread.ServiceCallback {

    public static final String ACTION = "action";
    public static final String DISCOVERY_REPLY = "reply";
    public static final int NOTIFICATION_ID = 521;

    // Keys for extras
    public static final String PAYLOAD = "payload";

    // Possible service actions:
    public static final int DISCOVERY_REQUEST = 1;
    public static final int SESSION_MESSAGE = 2;

    private Payload<?> registerPayload;
    private ClientSocketThread clientSocketThread;
    private SendHandler sendHandler;

    private DiscoveryReply reply;
    private LocalBroadcastManager manager;

    @Override
    public void onCreate() {
        super.onCreate();

        this.manager = LocalBroadcastManager.getInstance(this);
        clientSocketThread=new ClientSocketThread(this);
        Thread t = new Thread(clientSocketThread);
        t.start();
        runServiceInForeground();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            if (intent.hasExtra(DISCOVERY_REPLY)) {
                DiscoveryReply reply = (DiscoveryReply) intent.getSerializableExtra(DISCOVERY_REPLY);
                if (reply != null) {
                    this.reply = reply;
                }
            }
            int action = intent.getIntExtra(ACTION, 0);
            processAction(action, intent);
        }catch (Exception e){
            Log.d("ClientService","---error"+e.getMessage());

        }
        return START_STICKY;
    }

    private void processAction(int action, Intent intent) {
        if (action == 0)
            return;
        if (action == DISCOVERY_REQUEST)
            registerPayload = (Payload<?>) intent.getSerializableExtra(PAYLOAD);
        else if (action == SESSION_MESSAGE)
            sendSessionMessage((Payload<?>) intent.getSerializableExtra(PAYLOAD));
    }

    @Override
    public void onInitializedSocket(int port) {
        if(reply!=null){
            RegisterMessage message = new RegisterMessage(registerPayload, getLocalIp(), port);
            sendHandler=new SendHandler(new IncomingServerMessage(MessageType.REGISTER, message), reply.getIp(), reply.getPort());
            Thread t = new Thread(sendHandler);
            t.start();
        }
    }

    @Override
    public void onSessionMessage(SessionMessage message) {
        Intent i = new Intent(LocalClient.SESSION_MESSAGE);
        i.putExtra(SessionMessage.class.getName(), message);
        manager.sendBroadcast(i);
    }

    private void runServiceInForeground() {
        String channel_id=this.getResources().getString(R.string.app_name);
        androidx.core.app.NotificationCompat.Builder notification = new androidx.core.app.NotificationCompat.Builder(this,channel_id)
                .setContentTitle("RoyalPOS Waiter Session")
                .setContentText("Session is currently running")
                .setSmallIcon(R.drawable.ic_play_circle_filled_black_24dp)
                .setChannelId(channel_id);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(channel_id, channel_id, NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(mChannel);
        }
        startForeground(NOTIFICATION_ID, notification.build());

    }

    private void sendSessionMessage(Payload<?> payload){
        if(payload!=null && reply!=null) {
            SessionMessage message = new SessionMessage(payload);
            Log.d("DATA SendMsg",payload.getPayload()+" "+reply.getIp()+" "+reply.getPort());
            sendHandler=new SendHandler(new IncomingServerMessage(MessageType.SESSION, message), reply.getIp(), reply.getPort());
            Thread t = new Thread(sendHandler);
            t.start();
        }
    }

//    private String getLocalIp() {
//        WifiManager wm = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
//        return Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
//    }

    public String getLocalIp() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface networkInterface = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = networkInterface.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                        String host = inetAddress.getHostAddress();
                        if (!TextUtils.isEmpty(host)) {

                            return host;
                        }
                    }
                }

            }
        } catch (Exception ex) {
            Log.e("IP Address", "getLocalIpAddress", ex);
        }
        return null;
    }
    @Override
    public void onDestroy() {
        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(NOTIFICATION_ID);
        stopSelf();
        super.onDestroy();
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
    }
}
