package asyncProcess;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.PowerManager;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class AsyncUrlGet extends AsyncTask<String, String, String> {
	private AsyncResponse delegate=null;
	private Activity activity=null;
	
	private String  internalClassName ="AsyncUrlGet";
	private ProgressDialog progressDialog;			//activity bar
	private PowerManager.WakeLock mWakeLock;
	private JSONObject json=null;
	public boolean sendJsonAsData =true;
	private String serverUrl="";
	private String content ="";
	public String error_string=""; 
	public String processName="";
	public Boolean visibleconnection=true;
	public AsyncUrlGet() {	}
	
	

	public void initialice(AsyncResponse delegate,Activity activity,String tmpUrl, JSONObject json) {
		this.delegate=delegate;
		this.json = json;
		this.activity=activity;
		this.serverUrl=tmpUrl;
		Log.d(internalClassName ,"Start Url Action to URL:"+ this.serverUrl );
		progressDialog = new ProgressDialog (activity);
		progressDialog.setMessage(processName + "...");
		progressDialog.setIndeterminate(true);
		//progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressDialog.setCancelable(true);
		progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				dialog.cancel();
			}
		});




	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		Log.d(internalClassName ,"Start conection");
		progressDialog.setMessage(processName );
		
		PowerManager pm = (PowerManager) this.activity.getSystemService(Context.POWER_SERVICE);
		mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,getClass().getName());
		mWakeLock.acquire();

		if (asyncProcess.HttpUtils.internetConectionPresent(activity)==false)
		{
			Log.d(internalClassName ,"No internet conection Error");
			progressDialog.setMessage("No Internet Conection Error" );
			
		}
		if (visibleconnection) progressDialog.show();

	}
	protected void onProgressUpdate(String... progress) 
	{
		try {
			if (visibleconnection) 
				{
				progressDialog.setProgress(Integer.parseInt(progress[0]));
				
				}
			else
			{
				delegate.processUpdate(Integer.parseInt(progress[0]));	
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected String doInBackground(String... params) {


		if (asyncProcess.HttpUtils.internetConectionPresent(this.activity))
		{
			
			
			
			
			Log.d(internalClassName ,"start doInBackground url=" + serverUrl.toString());
			HttpResponse response = null;
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			@SuppressWarnings("unchecked")
			Iterator<String> iter = json.keys();
			while (iter.hasNext()) {
				String key = iter.next();
				try {
					Object value = this.json.get(key);
					nameValuePairs.add(new BasicNameValuePair(key.toString(), value.toString()));

				} catch (JSONException e) {
					// Something went wrong!
				}
			}
			if (sendJsonAsData)
			{
				String data=json.toString();
				nameValuePairs.add(new BasicNameValuePair("data", data.toString()));
			}

			try {
				response =HttpUtils.LlamadaHttpPost(this.serverUrl.toString(), nameValuePairs, this.activity);
				content = asyncProcess.HttpUtils.getResponseBody( response);
				publishProgress("50");
			} catch (SinConexionException e) {
				Log.e(internalClassName, "error:=" + e.getMessage().toString() );
				error_string="Can not connect to server:" +e.getMessage().toString() +" System is on?";
				content =null;
				e.printStackTrace();
			}


		}
		else
		{
			content =null;
			error_string="Error: Internet conection ERROR  WIFI o GPRS";
		}
		publishProgress("100");
		return content;
	}

	@Override
	protected void onPostExecute(String result) {

		Log.d(internalClassName ,"onPostExecute->1"+ result );
		mWakeLock.release();
		if (visibleconnection) progressDialog.dismiss();
		if (result==null)
		{
			DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					switch (which){
					case DialogInterface.BUTTON_POSITIVE:
						//Yes button clicked
						AsyncUrlGet asyncgetUrlData=new AsyncUrlGet();
						asyncgetUrlData.initialice(delegate,activity,serverUrl, json);
						asyncgetUrlData.execute();
						break;
					case DialogInterface.BUTTON_NEGATIVE:
						Log.d(internalClassName ,"Post Execute Cancelado.");
						break;
					}
				}
			};
			AlertDialog.Builder builder = new AlertDialog.Builder(this.activity);
			builder.setMessage("Error in conection, do again?");
			builder.setPositiveButton("Do again", dialogClickListener);
			builder.setNegativeButton("Cancel", dialogClickListener);
			builder.show();
		}
		else
		{
			JSONObject json = new JSONObject();
			try {
				json.put( "result", "ok" );
				json.put( "async", "geturl" );
				json.put( "process", 	processName );
				json.put( "data", result.toString() );
				delegate.processFinish(json.toString());	
			} catch (JSONException e) {
				e.printStackTrace();
			}	
			Log.d(internalClassName ,"onPostExecute->END" );
		}
	}
}