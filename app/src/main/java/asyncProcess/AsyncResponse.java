package asyncProcess;

public interface AsyncResponse {
    void processFinish(String output);
	void processUpdate(int parseInt);
	void processStringCallBack(String  stringText);
}
