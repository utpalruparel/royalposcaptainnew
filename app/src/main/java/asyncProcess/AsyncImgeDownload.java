package asyncProcess;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.PowerManager;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;


public class AsyncImgeDownload extends AsyncTask<String, String, String> {
	public AsyncResponse delegate=null;
	private Activity activity=null;
	private String  internalClassName ="AsyncImgeDownload";
	private PowerManager.WakeLock mWakeLock;
	public String processName="";
	
	private ProgressDialog progressDialog;// activity bar
	private String urlToDownload="";
	private String UrlSd="";
	public AsyncImgeDownload() {}
	public void initialice(AsyncResponse delegate, Activity activity,String tmpUrlImagen, String tmpUrlDestino) {
		this.delegate=delegate;
		this.activity=activity;
		this.urlToDownload = tmpUrlImagen;
		this.UrlSd = tmpUrlDestino;//"/sdcard/file_name.extension";
		
		
		progressDialog = new ProgressDialog(activity);
		progressDialog.setMessage("Download image");//mActivity.getApplicationContext().getResources().getString(R.string.textdata) );
		progressDialog.setIndeterminate(false);
		progressDialog.setMax(100);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		//mProgressDialog.setCancelable(false);
		progressDialog.setCancelable(true);
		progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				//this.cancel(true);
			}
		});


	}
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		Log.d(internalClassName ,"onPreExecute");
		PowerManager pm = (PowerManager) activity.getSystemService(Context.POWER_SERVICE);
		mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,getClass().getName());
		mWakeLock.acquire();
		
		if (asyncProcess.HttpUtils.internetConectionPresent(activity)==false)
		{
			Log.d(internalClassName ,"Star Conection: error no Internet found");
			progressDialog.setMessage("Error conection" );
			progressDialog.show();
		}
	}
	protected void onProgressUpdate(String... progress) 
	{
		progressDialog.setIndeterminate(false);
		progressDialog.setMax(100);
		progressDialog.setProgress(Integer.parseInt(progress[0]));
	}
	@Override
	protected String doInBackground(String... sUrl) {

		InputStream input = null;
		OutputStream output = null;
		HttpURLConnection connection = null;
		try {
			URL url = new URL(this.urlToDownload);
			
			
			connection = (HttpURLConnection) url.openConnection();
			connection.connect();

			// expect HTTP 200 OK, so we don't mistakenly save error report
			// instead of the file
			Log.d(internalClassName ,"doInBackground->1" );

			if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
				return "Server returned HTTP " + connection.getResponseCode()
						+ " " + connection.getResponseMessage();
			}
			// this will be useful to display download percentage
			// might be -1: server did not report the length
			int fileLength = connection.getContentLength();

			// download the file
			input = connection.getInputStream();
			output = new FileOutputStream(this.UrlSd);//"/sdcard/file_name.extension");
			Log.d(internalClassName ,"doInBackground->2" );
			byte data[] = new byte[4096];
			long total = 0;
			int count;
			while ((count = input.read(data)) != -1) {
				// allow canceling with back button
				if (isCancelled()) {
					input.close();
					return null;
				}
				Log.d(internalClassName ,"doInBackground->3"+String.valueOf (total * 100 / fileLength) );
				total += count;
				// publishing the progress....
				if (fileLength > 0) // only if total length is known
					publishProgress(String.valueOf (total * 100 / fileLength)  );
				output.write(data, 0, count);
			}
		} catch (Exception e) {
			Log.d(internalClassName ,"doInBackground->error" + e.toString() );
			return e.toString();
		} finally {
			try {
				if (output != null)
					output.close();
				if (input != null)
					input.close();
			} catch (IOException ignored) {
			}
			if (connection != null)
				connection.disconnect();
		}
		Log.d(internalClassName ,"doInBackground->4" );
		return null;
	}

	@SuppressWarnings("null")
	@Override
	protected void onPostExecute(String result) {
		Log.i(internalClassName, "onPostExecute"+ result);
		mWakeLock.release();
		progressDialog.dismiss();
		if (result != null)
			Toast.makeText(activity,"Download error: "+result, Toast.LENGTH_LONG).show();
		else
		{
			JSONObject json = new JSONObject();
			try {
				json.put( "result", "ok" );
				json.put( "async", "imageDownload" );
				json.put( "process", 	processName );
				json.put( "url", UrlSd );
				json.put( "data", "OK");
				delegate.processFinish(json.toString());	
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
}