package com.royalpos.waiter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.royalpos.waiter.helper.AppConst;
import com.royalpos.waiter.helper.ConnectionDetector;
import com.royalpos.waiter.helper.MemoryCache;
import com.royalpos.waiter.model.M;
import com.royalpos.waiter.model.MasterLoginPojo;
import com.royalpos.waiter.model.SuccessPojo;
import com.royalpos.waiter.model.Waiter;
import com.royalpos.waiter.webservices.APIServiceN;
import com.royalpos.waiter.webservices.AuthenticationAPI;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    TextInputLayout txt1,txt2;
    TextView tvheading,tvuserforgot;
    CheckBox cbopwd;
    EditText etusername,etpwd;
    Button btnsignin,btndevicecode;
    Context context;
    ConnectionDetector connectionDetector;
    String TAG="LoginActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        if(getResources().getBoolean(R.bool.portrait_only)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        }
        MemoryCache memoryCache = new MemoryCache();
        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        context=LoginActivity.this;
        connectionDetector=new ConnectionDetector(context);
        M.setWaitername("",context);
        M.setWaiterid("",context);
        M.setRestID("",context);
        M.setRestUniqueID("",context);
        initview();

    }

    private void initview() {
        txt1=(TextInputLayout)findViewById(R.id.txt1);
        txt1.setTypeface(AppConst.font_regular(context));
        txt2=(TextInputLayout)findViewById(R.id.txt2);
        txt2.setTypeface(AppConst.font_regular(context));
        tvheading=(TextView)findViewById(R.id.tvheading);
        tvheading.setText(R.string.activity_title_login);
        tvuserforgot=(TextView)findViewById(R.id.tvuserforgot);
        etusername=(EditText) findViewById(R.id.etusername);
        etusername.setTypeface(AppConst.font_regular(context));
        etpwd=(EditText) findViewById(R.id.etpassword);
        etpwd.setTypeface(AppConst.font_regular(context));

        btnsignin=(Button)findViewById(R.id.btnsignin);
        btnsignin.setTypeface(AppConst.font_regular(context));
        btndevicecode=(Button)findViewById(R.id.btndevicecode);
        btndevicecode.setTypeface(AppConst.font_regular(context));

        cbopwd=(CheckBox)findViewById(R.id.cbpwd);

        btnsignin.setOnClickListener(this);
        tvuserforgot.setOnClickListener(this);
        btndevicecode.setOnClickListener(this);

        cbopwd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    etpwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    etpwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        if(view==btnsignin){
            if(etusername.getText().toString()==null){
                M.T(context, getString(R.string.empty_usernameemail));
            }else if(etpwd.getText().toString()==null){
                M.T(context,getString(R.string.empty_password));
            }else if(etusername.getText().toString().trim().length()<=0){
               M.T(context, getString(R.string.empty_usernameemail));
            }else if(etpwd.getText().toString().trim().length()<=0){
                M.T(context,getString(R.string.empty_password));
            }else{
                String nm=etusername.getText().toString();
                String pwd=etpwd.getText().toString();

                masterlogin(nm,pwd);
            }
        }else if(view==tvuserforgot){
            final Dialog dialog=new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_rest_forgot_pwd);
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            dialog.setCancelable(false);
            ImageView ivclose=(ImageView)dialog.findViewById(R.id.ivclose);
            final EditText etusernm=(EditText)dialog.findViewById(R.id.etusernm);
            TextView btnsubmit=(TextView) dialog.findViewById(R.id.btnsubmit);

            ivclose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

            btnsubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(etusernm.getText().toString().trim().length()<=0)
                        etusernm.setError(getString(R.string.empty_email));
                    else{
                        String nm=etusernm.getText().toString();
                        forgotrestpwd(nm,dialog);
                    }
                }
            });
            dialog.show();
        }else if(view==btndevicecode){
            Intent it=new Intent(context,LoginDeviceCodeActivity.class);
            startActivity(it);
        }
    }
    private void masterlogin(final String nm, final String pwd) {
        if(connectionDetector.isConnectingToInternet()) {
            M.showLoadingDialog(this);
            AuthenticationAPI mAuthenticationAPI = APIServiceN.createService(LoginActivity.this, AuthenticationAPI.class);
            Call<MasterLoginPojo> call = mAuthenticationAPI.masterLogin(nm, pwd);
            call.enqueue(new retrofit2.Callback<MasterLoginPojo>() {
                @Override
                public void onResponse(Call<MasterLoginPojo> call, Response<MasterLoginPojo> response) {
                    if (response.isSuccessful()) {
                        MasterLoginPojo loginModel = response.body();
                        if (loginModel != null) {
                            if (loginModel.getSuccess()!=null && loginModel.getSuccess().equals("1")) {
                                M.setAdminId("",context);
                                M.setDecimalVal(loginModel.getDecimal_value(),context);
                                Boolean directLogin=false;
                                if(loginModel.getRole()!=null && loginModel.getRole().equals("outlet_admin"))
                                    directLogin=true;
                                else if(loginModel.getRole()!=null && loginModel.getRole().equals("brand_admin") && loginModel.getOutlet_count()==1)
                                    directLogin=true;
                                else
                                    directLogin=false;
                                if(directLogin) {
                                    String rid = loginModel.getRestaurant().getId() + "";
                                    String rnm = loginModel.getRestaurant().getName();
                                    String logo = loginModel.getRestaurant().getLogo();
                                    String uid = loginModel.getRestaurant().getRest_unique_id();
                                    String bid = loginModel.getRestaurant().getBrand_user_id();

                                    AppConst.currency = loginModel.getRestaurant().getCurrency();
                                    M.setRestID(rid, context);
                                    M.setRestName(rnm, context);
                                    M.setRestUserName(loginModel.getUsername(), context);
                                    M.setRestAddress(loginModel.getRestaurant().getAddress(), context);
                                    M.setRestPhoneNumber(loginModel.getRestaurant().getPhno(), context);
                                    M.setRestImg(logo, context);
                                    M.setRestUniqueID(uid, context);
                                    M.setGST(loginModel.getRestaurant().getGst_no(), context);
                                    M.setCurrency(loginModel.getRestaurant().getCurrency(), context);
                                    M.setCashDrawer(loginModel.getRestaurant().getCash_drawer(), context);
                                    M.setReportingemails(loginModel.getRestaurant().getReporting_emails(), context);
                                    M.setBrandName(loginModel.getRestaurant().getBrand_name(), context);
                                    M.setBrandId(loginModel.getRestaurant().getBrand_user_id(), context);
                                    M.setTrackInventory(loginModel.getRestaurant().getRecipe_inventory(), context);
                                    M.setDeduction(loginModel.getRestaurant().getStart_stock_deduct(), context);
                                    M.setType(loginModel.getRestaurant().getType_of_business(), context);
                                    M.setPlanid(loginModel.getRestaurant().getPlan_id() + "", context);
                                    M.setfooterphone(loginModel.getRestaurant().getRecipt_footer_phone(), context);
                                    M.setExpiry(loginModel.getRestaurant().getMembership_end_date(), context);
                                    M.setPackcharge(loginModel.getRestaurant().getPackaging_charge(),context);
                                    M.setChangePrice(Boolean.parseBoolean(loginModel.getRestaurant().getCan_change_price()),context);
                                    if(loginModel.getRestaurant().getShow_item_price_without_tax()!=null){
                                        M.setPriceWT(Boolean.parseBoolean(loginModel.getRestaurant().getShow_item_price_without_tax()),context);
                                    }
                                    if (loginModel.getWaiters() != null) {
                                        if (AppConst.waiters == null) {
                                            AppConst.waiters = new ArrayList<Waiter>();
                                        }
                                        AppConst.waiters.clear();
                                        AppConst.waiters = (ArrayList<Waiter>) loginModel.getWaiters();
                                    }
                                    if(!loginModel.getRestaurant().getService_charges().equals("disable"))
                                        M.setServiceCharge(true,context);
                                    else
                                        M.setServiceCharge(false,context);
                                    if(loginModel.getRestaurant().getIs_rounding_on()!=null && loginModel.getRestaurant().getIs_rounding_on().equals("on") &&
                                            loginModel.getRestaurant().getRounding_id()!=null && !loginModel.getRestaurant().getRounding_id().equals("0")){
                                        try {
                                            JSONObject jsonObject=new JSONObject();
                                            jsonObject.put("val_interval",loginModel.getRestaurant().getInterval_number());
                                            jsonObject.put("rule_tag",loginModel.getRestaurant().getRounding_tag());
                                            jsonObject.put("rounding_id",loginModel.getRestaurant().getRounding_id());
                                            M.setRoundBoundary(jsonObject+"",context);
                                            M.setRoundFormat(true,context);
                                        }catch (JSONException e){}
                                    }else
                                        M.setRoundFormat(false,context);
                                    M.setPointsAmt(loginModel.getRestaurant().getPoints_per_one_currency(),context);
                                    if(loginModel.getRestaurant().getAllow_item_wise_discount()!=null && loginModel.getRestaurant().getAllow_item_wise_discount().equals("enable"))
                                        M.setItemDiscount(true,context);
                                    else
                                        M.setItemDiscount(false,context);
                                    M.saveVal(M.key_tip_cal,loginModel.getRestaurant().getTip_calculation(),context);
                                    M.saveVal(M.key_tip_pref,loginModel.getRestaurant().getTip_calculation_preference(),context);
                                    M.hideLoadingDialog();
                                    Intent mIntent = new Intent(context, WaiterListActivity.class);
                                    startActivity(mIntent);
                                    finish();
                                }else if(loginModel.getUser_data()!=null){
                                    M.setBrandName(loginModel.getUser_data().getName(), context);
                                    M.setBrandId(loginModel.getBrand_user_id(), context);
                                    M.hideLoadingDialog();
                                    Intent mIntent = new Intent(context, RestaurantListActivity.class);
                                    startActivity(mIntent);
                                    finish();
                                }
                            } else if (loginModel.getSuccess().equals("2")){
                                M.hideLoadingDialog();
                                M.showToast(context, ""+loginModel.getMessage());
                            }else {
                                M.hideLoadingDialog();
                                M.showToast(context, ""+loginModel.getMessage());
                            }
                        }

                    } else {
                        M.hideLoadingDialog();
                        int statusCode = response.code();
                        ResponseBody errorBody = response.errorBody();
                        Log.d(TAG, "error:" + statusCode + " " + errorBody);
                    }
                }

                @Override
                public void onFailure(Call<MasterLoginPojo> call, Throwable t) {
                    Log.d("response:", "fail:" + t.getMessage());
                    M.hideLoadingDialog();
                    // handle execution failures like no internet connectivity
                }
            });
        }else
        {
            M.showToast(LoginActivity.this, getString(R.string.no_internet_error));
        }
    }

    private void forgotrestpwd(String nm, final Dialog dialog) {
        Log.d(TAG,"username: "+nm);
        if(connectionDetector.isConnectingToInternet()) {
            M.showLoadingDialog(this);
            AuthenticationAPI mAuthenticationAPI = APIServiceN.createService(LoginActivity.this, AuthenticationAPI.class);
            Call<SuccessPojo> call = mAuthenticationAPI.forgotRestpwd(nm);
            call.enqueue(new retrofit2.Callback<SuccessPojo>() {
                @Override
                public void onResponse(Call<SuccessPojo> call, Response<SuccessPojo> response) {
                    if (response.isSuccessful()) {
                        SuccessPojo loginModel = response.body();
                        if (loginModel != null) {
                            if(loginModel.getSuccess()==1) {
                                Toast.makeText(context,R.string.txt_check_email_pwd,Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }else
                                Toast.makeText(context,loginModel.getMessage(),Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        int statusCode = response.code();
                        ResponseBody errorBody = response.errorBody();
                        Log.d(TAG, "error:" + statusCode + " " + errorBody);
                    }
                    M.hideLoadingDialog();
                }

                @Override
                public void onFailure(Call<SuccessPojo> call, Throwable t) {
                    Log.d("response:", "fail:" + t.getMessage());
                    M.hideLoadingDialog();
                }
            });
        }else{
            M.showToast(LoginActivity.this, getString(R.string.no_internet_error));
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        finish();
        startActivity(intent);
    }
}
