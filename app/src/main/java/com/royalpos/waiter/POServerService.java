package com.royalpos.waiter;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.provider.Settings;
import androidx.annotation.Nullable;

import android.util.Log;

import com.royalpos.waiter.helper.WifiHelper;
import com.royalpos.waiter.model.M;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.util.Collections;

import java.util.List;



public class POServerService extends Service {

    private final IBinder mBinder = new LocalBinder();
    String TAG="POServerService";
    Context context;

    public final static String RECEIVE_MSG="receive_msg";
    Boolean isconnect=false;
    String ip,name;
    Socket clientSocket;
    PrintStream ps = null;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public class LocalBinder extends Binder {
        public POServerService getService() {
            // Return this instance of LocalService so clients can call public methods
            return POServerService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        context=this;
        ip=intent.getAction().replace("ip:","");
        name=M.getWaitername(context);
        //Log.d(TAG,"-----Start service: "+ip+" "+name);

        Thread sc = new Thread(new StartCommunication());
        sc.start();
        return super.onStartCommand(intent, flags, startId);
    }



    class StartCommunication implements Runnable {


        @Override
        public void run()
        {
            try {
                InetSocketAddress inetAddress = new InetSocketAddress(ip, 55555);
                clientSocket = new Socket();
                clientSocket.connect(inetAddress,7000);
                ps = new PrintStream(clientSocket.getOutputStream());
                //   sendChatMessage("Connected to "+ ip + " !!\n");
                ps.println("j01ne6:" + name);
                BufferedReader br = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

                while(true)
                {
                    final String str = br.readLine();
                    //Log.d(TAG,"while str:"+str);
                    if(str.equalsIgnoreCase("exit"))
                    {

                        sendChatMessage("Server Closed the Connection!");
                        ////Log.d(TAG,"1:"+str);

                        Intent it = new Intent(RECEIVE_MSG);
                        it.putExtra("stopService", "stopService");
                        sendBroadcast(it);

                        Thread.sleep(2000);
                        closeService();

                        break;
                    }
                    //Log.d(TAG,"2:"+str);
                    sendChatMessage(str);
                }
            }catch(final Exception e){
                //Log.d(TAG,"error:"+e.getMessage());
                try {Thread.sleep(2000);} catch (Exception exx){
                    //Log.d(TAG,"error1:"+e.getMessage());
                }
                closeService();
            }
        }
    }

    public void sendMsg(String msg){
        ps.println(msg);
        ps.flush();
    }

    private boolean sendChatMessage(String str) {

        if(str.startsWith("Connected to") || str.contains("Joined")) {

            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("device_model", Build.MODEL);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.CUPCAKE) {
                    jsonObject.put("device_id", Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID));
                }
                jsonObject.put("rest_id", M.getRestID(context));
                jsonObject.put("rest_uniq_id", M.getRestUniqueID(context));
                jsonObject.put("user_id", M.getWaiterid(context));
                jsonObject.put("user_name", M.getWaitername(context));
                jsonObject.put("user_ip", getIPAddress(true));
                jsonObject.put("key", WifiHelper.KEY_CONNECTION);
                sendMsg(jsonObject.toString());
            }catch (JSONException e){}

        }
        else {

            String res=str;
            if (res != null && !res.isEmpty() && !res.equals("null")) {
                if (res.equals(WifiHelper.KEY_EXIT_SERVER)) {
                    Intent it = new Intent(RECEIVE_MSG);
                    it.putExtra("stopService", "stopService");
                    sendBroadcast(it);
                    closeService();
                } else {
                    Intent it = new Intent(RECEIVE_MSG);
                    it.putExtra("response", res);
                    sendBroadcast(it);
                }
            }
        }
        return true;
    }

    public  String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':')<0;

                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                return delim<0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) { } // for now eat exceptions
        return "";
    }

    public void

    closeService(){
        stopSelf();
        isconnect=false;
    }

    public void updateConnect(Boolean res){
        isconnect=res;
    }



    @Override
    public void onDestroy() {
        //Log.d(TAG,"ondestroy");
        try {
            JSONObject dObj = new JSONObject();
            dObj.put("key", "Disconnect");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.CUPCAKE) {
                dObj.put("device_id", Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID));
            }
            sendMsg(dObj.toString());
        }catch (JSONException e){}
        Intent it=new Intent(RECEIVE_MSG);
        it.putExtra("stopService","stopService");
        sendBroadcast(it);

        isconnect=false;
        if (ps != null) {
            ps.println("Ex1+:" + M.getWaitername(context));
            ps.close();
        }
        stopSelf();
        super.onDestroy();
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        isconnect=false;
    }
}
