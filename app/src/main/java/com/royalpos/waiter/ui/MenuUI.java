package com.royalpos.waiter.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.royalpos.waiter.R;
import com.royalpos.waiter.model.M;

import de.greenrobot.event.EventBus;

public class MenuUI {

    public static String ui_vr="vertical";
    public static String ui_hr="horizontal";

    public MenuUI(final Context context, final TextView tv) {
        final Dialog dialog=new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (context.getResources().getBoolean(R.bool.portrait_only))
            dialog.setContentView(R.layout.dialog_set_menu_ui);
        else
            dialog.setContentView(R.layout.dialog_set_menu_ui_land);
        if(M.getMenuUI(context)==null)
            dialog.setCancelable(false);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final LinearLayout llvr=(LinearLayout)dialog.findViewById(R.id.llvr);
        llvr.setTag("");
        final LinearLayout llhr=(LinearLayout)dialog.findViewById(R.id.llhr);

        if(M.getMenuUI(context)!=null){
            llvr.setTag(M.getMenuUI(context));
            if(M.getMenuUI(context).equals(ui_vr))
                llvr.setBackgroundColor(context.getResources().getColor(R.color.yellow));
            else if(M.getMenuUI(context).equals(ui_hr))
                llhr.setBackgroundColor(context.getResources().getColor(R.color.yellow));
        }

        llvr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!llvr.getTag().toString().equals(ui_vr)) {
                    llvr.setBackgroundColor(context.getResources().getColor(R.color.yellow));
                    llhr.setBackgroundColor(context.getResources().getColor(android.R.color.transparent));
                    M.setMenuUI(ui_vr, context);
                    if(tv!=null)
                        tv.setText(context.getString(R.string.menu_type1));
                    dialog.dismiss();
                }
            }
        });
        llhr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!llvr.getTag().toString().equals(ui_hr)) {
                    llhr.setBackgroundColor(context.getResources().getColor(R.color.yellow));
                    llvr.setBackgroundColor(context.getResources().getColor(android.R.color.transparent));
                    M.setMenuUI(ui_hr, context);
                    if(tv!=null)
                        tv.setText(context.getString(R.string.menu_type2));
                    dialog.dismiss();
                }
            }
        });

        dialog.show();
    }
}
