package com.royalpos.waiter.ui.chips;

public interface ChipListener {
    void chipSelected(int index);

    void chipDeselected(int index);
}
