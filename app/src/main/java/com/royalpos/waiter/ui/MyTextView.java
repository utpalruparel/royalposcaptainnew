package com.royalpos.waiter.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.royalpos.waiter.R;
import com.royalpos.waiter.helper.AppConst;

public class MyTextView extends androidx.appcompat.widget.AppCompatTextView {

	public MyTextView(Context context) {
		super(context);
		if (isInEditMode()) return;
		parseAttributes(null);
	}

	public MyTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		if (isInEditMode()) return;
		parseAttributes(attrs);
	}

	public MyTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		if (isInEditMode()) return;
		parseAttributes(attrs);
	}
	
	private void parseAttributes(AttributeSet attrs) {
		int typeface;
		if (attrs == null) { //Not created from xml
			typeface = font.regular;
		} else {
		    TypedArray values = getContext().obtainStyledAttributes(attrs, R.styleable.MyTextView);
		    typeface = values.getInt(R.styleable.MyTextView_typeface, font.regular);
		    values.recycle();
		}
	    setTypeface(getfont(typeface));
	}
	
	public void setfontTypeface(int typeface) {
	    setTypeface(getfont(typeface));
	}
	
	private Typeface getfont(int typeface) {
		return getfont(getContext(), typeface);
	}
	
	public static Typeface getfont(Context context, int typeface) {
		switch (typeface) {
		case font.regular:
			if (font.fontRegular == null) {
				font.fontRegular = AppConst.font_regular(context);
			}
			return font.fontRegular;
		case font.medium:
			if (font.fontMedium == null) {
				font.fontMedium = AppConst.font_medium(context);
			}
			return font.fontMedium;
		case font.italic:
				if (font.fontItalic == null) {
					font.fontItalic = AppConst.font_italic(context);
				}
				return font.fontItalic;
		default:
			return font.fontRegular;
		}
	}
	
	public static class font {
		public static final int regular = 1;
		public static final int medium = 2;
		public static final int italic = 3;

		private static Typeface fontRegular;
		private static Typeface fontItalic;
		private static Typeface fontMedium;
	}
}
