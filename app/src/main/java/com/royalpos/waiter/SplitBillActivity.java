package com.royalpos.waiter;

import android.app.Dialog;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.util.Log;
import android.util.Printer;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.royalpos.waiter.database.DBDishes;
import com.royalpos.waiter.database.DBKP;
import com.royalpos.waiter.database.DBPaymentType;
import com.royalpos.waiter.database.DBPrinter;
import com.royalpos.waiter.helper.AppConst;
import com.royalpos.waiter.helper.GenerateToken;
import com.royalpos.waiter.helper.WifiHelper;
import com.royalpos.waiter.model.ComboModel;
import com.royalpos.waiter.model.DiscountPojo;
import com.royalpos.waiter.model.DishOrderPojo;
import com.royalpos.waiter.model.DishPojo;
import com.royalpos.waiter.model.KitchenPrinterPojo;
import com.royalpos.waiter.model.M;
import com.royalpos.waiter.model.OfflineOrder;
import com.royalpos.waiter.model.PaymentModePojo;
import com.royalpos.waiter.model.TaxData;
import com.royalpos.waiter.model.TaxInvoicePojo;
import com.royalpos.waiter.model.VarPojo;
import com.royalpos.waiter.print.KitchenPrintActivity;
import com.royalpos.waiter.print.OtherPrintReceipt;
import com.royalpos.waiter.print.PrintActivity;
import com.royalpos.waiter.print.PrintFormat;
import com.royalpos.waiter.print.StarPrintReceipt;
import com.royalpos.waiter.print.newbluetooth.AsyncNewPrintNew;
import com.royalpos.waiter.print.newbluetooth.GlobalsNew;
import com.royalpos.waiter.print.newbluetooth.KitchenGlobalsNew;
import com.royalpos.waiter.print.newbluetooth.KitchenPrintActivityNew;
import com.royalpos.waiter.print.newbluetooth.PrintActivityNew;
import com.royalpos.waiter.universalprinter.Globals;
import com.royalpos.waiter.universalprinter.KitchenGlobals;
import com.royalpos.waiter.utils.AidlUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import de.greenrobot.event.EventBus;
import dev.jokr.localnet.LocalClient;
import dev.jokr.localnet.models.Payload;

public class SplitBillActivity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView rv;
    LinearLayout llsplit,ll;
    TextView tvgrand,tvcount,tvsel,btnclose;
    Button btnnext,btnadd,btnremove;
    EditText ets_nm,ets_ph;
    View s_v;
    TextView s_tv;
    Context context;
    String TAG="SplitBillActivity",action,ser_per,dis_per;
    String ordertype,en_ordertype,orderno,token,tblid="0",tblnm="",order_id;
    SplitItemAdapter adapter;
    List<DishOrderPojo> dishList=new ArrayList<>();
    PrintFormat  pf;
    List<TextView> txtList=new ArrayList<>(),textViewList=new ArrayList<>();
    List<EditText> etnmist,etphlist;
    List<PaymentModePojo> plist;
    List<String> wlist=new ArrayList<>();
    public static DiscountPojo discountPojo;
    List<JSONObject> orderJSONList=new ArrayList<>();
    private Printer mPrinter = null;  // cash printer
    private Printer kitchenPrinter = null;  // kitchen printer
    String cashprintermodel, cashprintertarget, kitchenprintermodel, kitchenprintertarget;
    private SharedPreferences sharedPrefs = null;
    Dialog dialog,joinDialog;
    List<List<KitchenPrinterPojo>> kpList=new ArrayList<>();
    List<List<Integer>> totList=new ArrayList<>();
    Boolean mBound=false;
    POServerService mService;
    LocalClient localClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_split_bill);

        context=SplitBillActivity.this;
        pf=new PrintFormat(context);
        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        action=getIntent().getAction();
        if(getIntent().getExtras()!=null){
            dis_per=getIntent().getStringExtra("dis_per");
            ser_per=getIntent().getStringExtra("ser_per");
            ordertype=getIntent().getStringExtra("ot");
            en_ordertype=getIntent().getStringExtra("en_ot");
            orderno=getIntent().getStringExtra("order_no");
            token=getIntent().getStringExtra("token");
            if(getIntent().getExtras().containsKey("table_id"))
                tblid=getIntent().getStringExtra("table_id");
            if(getIntent().getExtras().containsKey("table_nm"))
                tblnm=getIntent().getStringExtra("table_nm");
            if(getIntent().getExtras().containsKey("order_id"))
                order_id=getIntent().getStringExtra("order_id");
        }
        TextView tvheading=(TextView)findViewById(R.id.tvheading);
        tvheading.setText("Select Items");
        rv=(RecyclerView)findViewById(R.id.rvitems);
        rv.setLayoutManager(new LinearLayoutManager(context));
        rv.setHasFixedSize(true);
        rv.setNestedScrollingEnabled(false);
        tvgrand=(TextView)findViewById(R.id.tvgrand);
        tvcount=(TextView)findViewById(R.id.tvcount);
        btnnext=(Button)findViewById(R.id.btnnext);
        btnnext.setTypeface(AppConst.font_regular(context));
        btnadd=(Button)findViewById(R.id.btnadd);
        btnremove=(Button)findViewById(R.id.btnremove);
        btnremove.setVisibility(View.GONE);
        llsplit=(LinearLayout)findViewById(R.id.llsplit);
        llsplit.setTag("0");

        dishList=AppConst.dishorederlist;
        if(discountPojo!=null && discountPojo.getDiscount_type().equalsIgnoreCase("complementary")){
            DBDishes dbDishes=new DBDishes(context);
            DishPojo model=dbDishes.getDishData(discountPojo.getItem_id(),context);
            DishOrderPojo custorder = new DishOrderPojo();
            custorder.setDishid(model.getDishid());
            custorder.setDishname(model.getDishname());
            custorder.setQty("1");
            custorder.setIsnew(true);
            custorder.setStatus("0");
            custorder.setPrefid("0");

            custorder.setPrice(pf.setFormat("0"));
            custorder.setPriceperdish(pf.setFormat("0"));
            custorder.setPriceper_without_tax("0");
            custorder.setPrice_without_tax("0");

            custorder.setDiscount(model.getPrice());
            custorder.setTot_disc(model.getPrice());
            custorder.setOffer(discountPojo.getId());
            custorder.setComment("");
            custorder.setCusineid(model.getCusineid());
            custorder.setDescription(model.getDescription());
            custorder.setDishimage(model.getDishimage());
            custorder.setPreflag(model.getPreflag());
            custorder.setPre(model.getPre());
            custorder.setInventory_item(model.getInventory_item());
            custorder.setDish_comment("");
            custorder.setHsn_no(model.getHsn_no());
            custorder.setTot_tax("0");
            dishList.add(custorder);
        }
        adapter=new SplitItemAdapter(context,dishList);
        rv.setAdapter(adapter);

        btnnext.setOnClickListener(this);
        btnadd.setOnClickListener(this);
        btnremove.setOnClickListener(this);

        btnadd.performClick();
        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        cashprintermodel = M.getCashPrinterModel(context);
        cashprintertarget = M.getCashPrinterIP(context);
        kitchenprintermodel = M.getKitchenPrinterModel(context);
        kitchenprintertarget = M.getKitchenPrinterIP(context);
        AidlUtil.getInstance().connectPrinterService(context);
        Globals.loadPreferences(sharedPrefs);
        KitchenGlobals.loadPreferences(sharedPrefs);
        AppConst.checkSame(context);

        registerReceiver(broadcastReceiver,new IntentFilter(POServerService.RECEIVE_MSG));

        if(AppConst.isMyServiceRunning(POServerService.class,context)) {
            M.showLoadingDialog(context);
            Intent intent = new Intent(this, POServerService.class);
            bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        }

    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, Intent intent) {
            if(intent.getExtras()!=null){
                if(intent.hasExtra("stopService")){
                    localClient=null;
                    mBound = false;
                    EventBus.getDefault().post("closescreen");
                    finish();
                }else if(AppConst.isMyServiceRunning(POServerService.class,context)){
                    String res = intent.getStringExtra("response");
                    try {
                        JSONObject jsonObject = new JSONObject(res);
                        if (jsonObject.has("key")) {
                            String msg_key = jsonObject.getString("key");
                            if (msg_key.equals(WifiHelper.KEY_CONNECTION)) {
                                if (jsonObject.getString("success").equals(WifiHelper.TXT_FAIL)) {
                                    mService.closeService();
                                } else {
                                    Intent it = new Intent(context, POServerService.class);
                                    bindService(it, mConnection, Context.BIND_AUTO_CREATE);
                                }
                            }else if(msg_key.equals(WifiHelper.api_split_order_response)){
                                if (jsonObject.getString("success").equals(WifiHelper.TXT_SUCCESS)){
                                    M.hideLoadingDialog();
                                    JSONObject msgObj=new JSONObject(jsonObject.getString("msg"));
                                    printBill(msgObj, msgObj.getInt("sel_pos"));
                                    etnmist.remove(ets_nm);
                                    etphlist.remove(ets_ph);
                                    textViewList.remove(s_tv);
                                    ll.removeView(s_v);
                                    btnclose.setVisibility(View.GONE);
                                    sDialog.setCancelable(false);
                                    if (ll.getChildCount() == 0) {
                                        sDialog.dismiss();
                                        EventBus.getDefault().post("closescreen");
                                        finish();
                                    } else {
                                        M.setToken(msgObj.getString("order_no"), context);
                                        modifyNo(textViewList);
                                    }
                                }
                            }
                        }
                    } catch (JSONException e) {
                        Log.d(TAG, "error:" + e.getMessage());
                    }
                }
            }
        }
    };

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            POServerService.LocalBinder binder = (POServerService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
            M.hideLoadingDialog();
            if(dialog!=null && dialog.isShowing())
                dialog.dismiss();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
            M.hideLoadingDialog();
        }
    };

    private void sendToServer(String reqObj){
        if(AppConst.isMyServiceRunning(POServerService.class,context)){
            if(localClient==null)
                localClient=new LocalClient(context);
            localClient.sendSessionMessage(new Payload<String>(reqObj+""));
        }else {
            if(M.pDialog!=null && M.pDialog.isShowing())
                M.hideLoadingDialog();
            EventBus.getDefault().post("serviceclose");
        }
    }

    public void cal(){
        String s1=tvsel.getTag().toString();
        List<String> sels=new ArrayList<>();
        if(s1.contains(","))
            sels=new ArrayList<String>(Arrays.asList(tvsel.getTag().toString().split(",")));
        else if(!s1.isEmpty())
            sels.add(s1);
        if(sels.size()>0){
            Double granttodal=0.0,discountamt=0.0;
            for(String s:sels){
                DishOrderPojo pojo=dishList.get(Integer.parseInt(s));
                if(!pojo.isnew())
                    granttodal = granttodal + Double.parseDouble(pojo.getPrice());
                else
                    granttodal = granttodal + (Integer.parseInt(pojo.getQty()) * Double.parseDouble(pojo.getPrice()));
                boolean isCamp=false;
                if(pojo.getOffer()!=null && !pojo.getOffer().isEmpty() && discountPojo!=null){
                    if(discountPojo.getId().equals(pojo.getOffer()))
                        isCamp=true;
                }
                if(!isCamp && pojo.getTot_disc()!=null && pojo.getTot_disc().trim().length()>0 &&
                        Double.parseDouble(pojo.getTot_disc())>0) {
                    discountamt = discountamt +Double.parseDouble(pojo.getTot_disc());
                }
            }
            if(discountamt>0) {
                if (discountamt > granttodal) {
                    discountamt = granttodal;
                    granttodal = 0.0;
                } else
                    granttodal = granttodal - discountamt;
            }
            Double dis_amt=0.0,ser_amt=0.0;
            if(dis_per!=null && !dis_per.isEmpty())
                dis_amt = (Double.parseDouble(dis_per) * granttodal) / 100f;
            if(discountPojo!=null && !discountPojo.getDiscount_type().equalsIgnoreCase("complementary")){
                if(Double.parseDouble(discountPojo.getBill_amt())<=granttodal){
                    if (discountPojo.getDiscount_type().equalsIgnoreCase("percentage"))
                        dis_amt = (Double.parseDouble(discountPojo.getDiscount_percentage()) * granttodal) / 100f;
                    else if (discountPojo.getDiscount_type().equalsIgnoreCase("amount"))
                        dis_amt=(Double.parseDouble(discountPojo.getDiscount_amt()));
                }
            }
            if(ser_per!=null && !ser_per.isEmpty())
                ser_amt = (Double.parseDouble(ser_per) * granttodal) / 100f;
            granttodal = granttodal+ser_amt-dis_amt;
            tvgrand.setText(pf.setFormat(granttodal+""));
        }else
            tvgrand.setText(pf.setFormat("0"));
        tvcount.setText(sels.size()+"");
    }

    @Override
    public void onClick(View view) {
        if(view==btnnext){
            int selcnt=adapter.getSelected().size();
            if(selcnt==dishList.size()){
                etnmist=new ArrayList<>();
                etphlist=new ArrayList<>();
                etnmist.clear();
                etphlist.clear();

                DBPaymentType dbPaymentType=new DBPaymentType(context);
                plist=dbPaymentType.getPaymenttype();
                wlist=new ArrayList<>();
                wlist.clear();
                if (plist.size() > 0) {
                    for (PaymentModePojo word : plist) {
                        String w = word.getType();
                        wlist.add(w);
                    }
                }
                orderJSONList.clear();
                textViewList.clear();
                showSplits();
            }else {
                Toast.makeText(context, "Please split all items",Toast.LENGTH_SHORT).show();
            }
        }else if(view==btnadd){
            int selcnt=adapter.getSelected().size();
            if(llsplit.getChildCount()==dishList.size()){
                Toast.makeText(context,"Not allow more than items",Toast.LENGTH_SHORT).show();
            }else if(selcnt<dishList.size()) {
                final TextView tv = new TextView(context);
                tv.setPadding(20, 10, 20, 10);
                LinearLayout.LayoutParams p=new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                p.setMargins(2,0,10,0);
                tv.setLayoutParams(p);
                int cnt = llsplit.getChildCount() + 1,cnttag=Integer.parseInt(llsplit.getTag().toString())+1;
                llsplit.setTag(cnttag);
                tv.setId(cnttag);
                tv.setTag("");
                tv.setText(cnt + "");
                tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
                tv.setBackgroundResource(R.drawable.chip);
                tv.setTextColor(context.getResources().getColor(R.color.primary_text));
                tv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (tvsel != null) {
                            tvsel.setBackgroundResource(R.drawable.chip);
                            tvsel.setTextColor(context.getResources().getColor(R.color.primary_text));
                        }
                        tv.setBackgroundResource(R.drawable.chip_selected);
                        tv.setTextColor(context.getResources().getColor(R.color.white));
                        tvsel = tv;
                        adapter.chipSelUpdate();
                        cal();
                    }
                });
                tv.performClick();
                llsplit.addView(tv);
                txtList.add(tv);
            }else
                Toast.makeText(context,"All Item selected",Toast.LENGTH_SHORT).show();
            if(llsplit.getChildCount()>1)
                btnremove.setVisibility(View.VISIBLE);
        }else if(view==btnremove){
            if(llsplit.getChildCount()==2)
                btnremove.setVisibility(View.GONE);
            if(tvsel!=null){
                txtList.remove(tvsel);
                adapter.removeChipUpdate(new ArrayList<String>(Arrays.asList(tvsel.getTag().toString().split(","))));
                llsplit.removeView(tvsel);
                llsplit.getChildAt(0).performClick();
                modifyNo(txtList);
            }
        }
    }
    Dialog sDialog;
    public void showSplits(){
        if(kpList==null)
            kpList=new ArrayList<>();
        kpList.clear();
        if(totList==null)
            totList=new ArrayList<>();
        totList.clear();
        sDialog=new Dialog(context);
        sDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        sDialog.setContentView(R.layout.dialog_split_items);
        sDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

        ll=sDialog.findViewById(R.id.ll);
        ll.setTag("-1");
        ll.removeAllViews();
        btnclose=sDialog.findViewById(R.id.btnclose);
        int i=0;
        for(TextView tv:txtList){
            try {
                String s1 = tv.getTag().toString();
                List<String> sels = new ArrayList<>();
                if (s1.contains(","))
                    sels = new ArrayList<String>(Arrays.asList(s1.split(",")));
                else if (!s1.isEmpty())
                    sels.add(s1);
                if (sels.size() > 0) {
                    orderJSONList.add(new JSONObject());
                    Double granttodal = 0.0, discountamt = 0.0, subtotal = 0.0,totamt=0.0;
                    Double dis_amt = 0.0, ser_amt = 0.0,totdisc=0.0,disper=0.0;
                    String offer_id="",offer_name="";
                    ArrayList<String> catlist=new ArrayList<>(), pidlist=new ArrayList<>(),taxidlist=new ArrayList<>(),txt = new ArrayList<>();
                    ArrayList<KitchenPrinterPojo> kplist=new ArrayList<>();
                    ArrayList<Integer> totlist=new ArrayList<>();
                    DBPrinter dbp=new DBPrinter(context);
                    ArrayList<TaxInvoicePojo> taxInvoiceList=new ArrayList<>(),taxDisList=new ArrayList<>();
                    List<DishOrderPojo> dlist = new ArrayList<>();
                    DishOrderPojo compItem=null;
                    JSONArray jsonArray=new JSONArray();
                    for (String s : sels) {
                        DishOrderPojo pojo = dishList.get(Integer.parseInt(s));
                        dlist.add(pojo);
                        JSONObject dishes = new JSONObject();
                        txt.add(pojo.getDishname() + " (X" + pojo.getQty() + ")");
                        double tax_tot = 0;
                        List<TaxData> tlist = pojo.getTax_data();
                        tax_tot = Double.parseDouble(pojo.getTot_tax());
                        double item_dis_per = 0;
                        boolean isComp=false;
                        if (pojo.getOffer() != null && !pojo.getOffer().isEmpty()) {
                            if(discountPojo!=null && pojo.getOffer().equals(discountPojo.getId()+"")){
                                compItem=pojo;
                                isComp=true;
                            }else if(pojo.getOffer().contains("{")){
                                JSONObject jdisc = new JSONObject(pojo.getOffer());
                                if (jdisc.has("discount_type")) {
                                    if (jdisc.getString("discount_type").equals("percentage") && jdisc.has("discount_percentage")) {
                                        String dp = jdisc.getString("discount_percentage");
                                        if (dp != null && dp.trim().length() > 0)
                                            item_dis_per = Double.parseDouble(dp);
                                    } else if (jdisc.getString("discount_type").equals("amount") && jdisc.has("discount_amt")) {
                                        if (!jdisc.getString("discount_amt").isEmpty())
                                            item_dis_per = (Double.parseDouble(jdisc.getString("discount_amt")) * 100) / Double.parseDouble(pojo.getPrice());
                                    }
                                }
                            }
                        } else if (pojo.getDis_per() != null && pojo.getDis_per().trim().length() > 0 && Double.parseDouble(pojo.getDis_per()) > 0) {
                            item_dis_per = Double.parseDouble(pojo.getDis_per());
                        }
                        long qty = Long.parseLong(pojo.getQty());
                        double price = Double.parseDouble(pojo.getPrice()),finalprice;
                        double pricewtax,finalpricewt;
                        if (pojo.getPrice_without_tax() != null && pojo.getPrice_without_tax().trim().length() > 0)
                            pricewtax = Double.parseDouble(pojo.getPrice_without_tax());
                        else {
                            pricewtax = price;
                            tlist = null;
                        }
                        String tt = pojo.getTax_amt();
                        if (tt == null || tt.trim().length() == 0)
                            tt = "0";
                        double diff = qty * (Double.parseDouble(tt)), tax_dis_amt = 0;
                        if (item_dis_per > 0 && diff > 0) {
                            tax_dis_amt = (item_dis_per * diff) / 100f;
                            diff = diff - tax_dis_amt;
                        }
                        double amt = qty * price;
                        totamt = totamt + amt;
                        if (!pojo.isnew()) {
                            finalprice=price;
                            finalpricewt=pricewtax;
                            granttodal = granttodal + price;
                        } else {
                            finalprice=qty * price;
                            finalpricewt=qty*pricewtax;
                            granttodal = granttodal + (qty * price);
                        }
                        if (tlist != null && tlist.size() > 0) {
                            if (!pojo.isnew()){
                                subtotal = subtotal + pricewtax;
                            }else {
                                subtotal = subtotal + (qty * pricewtax);
                            }
                            if(tax_dis_amt>0)
                                subtotal = subtotal +tax_dis_amt;
                            int tpos = 0;
                            for (TaxData tax : tlist) {
                                TaxInvoicePojo tp = new TaxInvoicePojo();
                                double d =0;
                                if(tax_tot!=0)
                                    d = (diff * Double.parseDouble(tax.getValue())) / tax_tot;
                                if (taxidlist.contains(tax.getId())) {
                                    int p = taxidlist.indexOf(tax.getId());
                                    tp = taxInvoiceList.get(p);
                                    double pt = Double.parseDouble(tp.getPer_total()) + Double.parseDouble(tax
                                            .getValue());
                                    tp.setPer_total(pt + "");
                                    double tdiff = Double.parseDouble(tp.getAmount_total()) + d;
                                    tax.setTax_value(d + "");
                                    tp.setAmount_total(tdiff+ "");
                                    taxInvoiceList.set(p, tp);
                                } else {
                                    tp.setId(tax.getId());
                                    tp.setName(tax.getText());
                                    tp.setType(tax.getTax_amount());
                                    tp.setPer(tax.getValue());
                                    tp.setPer_total(tax.getValue());
                                    tax.setTax_value(d + "");
                                    tp.setAmount_total(d+"");
                                    taxInvoiceList.add(tp);
                                    taxidlist.add(tax.getId());
                                }
                                tlist.set(tpos, tax);
                                tpos++;
                            }
                        } else {
                            if(!pojo.isnew())
                                subtotal=subtotal+price;
                            else
                                subtotal=subtotal+(qty*price);
                        }
                        if(pojo.getTot_disc()!=null && pojo.getTot_disc().trim().length()>0 &&
                                Double.parseDouble(pojo.getTot_disc())>0) {
                            if(!isComp)
                                discountamt = discountamt +Double.parseDouble(pojo.getTot_disc());
                            dishes.put("discount", pojo.getTot_disc());
                        }

                        if (pojo.getOffer() != null && pojo.getOffer().trim().length() > 0) {
                            String ofid,ofnm;
                            if(discountPojo!=null && discountPojo.getId().equals(pojo.getOffer())){
                                dishes.put("offer_id",pojo.getOffer());
                                ofid=discountPojo.getId();
                                ofnm=discountPojo.getName();
                            }else {
                                JSONObject jOffer = new JSONObject(pojo.getOffer());
                                dishes.put("offer_id", jOffer.getString("id"));
                                ofid=jOffer.getString("id");
                                ofnm=jOffer.getString("name");
                            }
                            if (offer_id != null && offer_id.trim().length() > 0) {
                                offer_id = offer_id + "," + ofid;
                                offer_name = offer_name + "," +ofnm ;
                            } else {
                                offer_id = ofid;
                                offer_name = ofnm;
                            }
                        }
                        String cid=pojo.getCusineid();
                        if(!catlist.contains(cid)) {
                            catlist.add(cid);
                            KitchenPrinterPojo kprinter=dbp.getCatPrinters(cid);
                            if(kprinter!=null) {
                                if(!pidlist.contains(kprinter.getId())) {
                                    pidlist.add(kprinter.getId());
                                    kplist.add(kprinter);
                                    totlist.add(1);
                                }else{
                                    int p=pidlist.indexOf(kprinter.getId());
                                    int val=totlist.get(p)+1;
                                    totlist.set(p,val);
                                }
                            }
                        }
                        String tot_weight="0";
                        if(pojo.getWeight()!=null && pojo.getWeight().trim().length()>0)
                            tot_weight=pojo.getWeight();
                        dishes.put("dishid", pojo.getDishid());
                        dishes.put("orderdetail_id",pojo.getOrderdetailid());
                        dishes.put("cuisine_id",pojo.getCusineid());
                        dishes.put("dishname", pojo.getDishname());
                        dishes.put("quantity", pojo.getQty());
                        dishes.put("rate", pf.setFormat(pojo.getPrice()));
                        dishes.put("price",pf.setFormat(finalprice+""));
                        dishes.put("priceperdish", pf.setFormat(pojo.getPriceperdish()+""));
                        dishes.put("priceperdish_wt",pf.setFormat( pojo.getPriceper_without_tax()+""));
                        dishes.put("price_wt",pf.setFormat(finalpricewt+""));
                        dishes.put("preferencesid", pojo.getPrefid());
                        dishes.put("prenm", pojo.getPrenm());
                        if(pojo.getVarPojoList()!=null && pojo.getVarPojoList().size()>0){
                            JSONArray preArray=new JSONArray();
                            for(VarPojo v:pojo.getVarPojoList()){
                                Double vamt=Double.parseDouble(v.getQuantity())*Double.parseDouble(v.getAmount());
                                Double vamtwt=Double.parseDouble(v.getQuantity())*Double.parseDouble(v.getAmount_wt());
                                JSONObject preObj=new JSONObject();
                                preObj.put("id",v.getId());
                                preObj.put("name",v.getName());
                                preObj.put("amount",pf.setFormat(vamt+""));
                                preObj.put("amount_wt",pf.setFormat(vamtwt+""));
                                preObj.put("quantity",v.getQuantity());
                                preArray.put(preObj);
                            }
                            dishes.put("pre_array",preArray);
                        }
                        dishes.put("weight",tot_weight+"");
                        dishes.put("unit_name",pojo.getUnitname());
                        dishes.put("unit_sort_name",pojo.getSort_nm());
                        if(pojo.getTax_data()!=null && pojo.getTax_data().size()>0) {
                            JSONArray ja = new JSONArray();
                            for (TaxData p : pojo.getTax_data()) {
                                double a ;
                                try {
                                    a = Double.parseDouble(p.getTax_value());
                                }catch (NumberFormatException e){
                                    a=0;
                                }
                                JSONObject jt = new JSONObject();
                                jt.put("tax_name", p.getText());
                                jt.put("tax_per", p.getValue());
                                jt.put("tax_value", pf.setFormat(a+ ""));
                                jt.put("tax_id",p.getId());
                                ja.put(jt);
                            }
                            dishes.put("taxes_data", ja);
                        }
                        if(pojo.getUnitid()!=null && pojo.getUnitid().trim().length()>0)
                            dishes.put("unit_id",pojo.getUnitid());
                        else
                            dishes.put("unit_id","0");
                        dishes.put("dish_comment", pojo.getDish_comment());
                        if (pojo.getCombo_list() != null) {
                            if (pojo.getCombo_list().size() > 0) {
                                List<String> cmbname = new ArrayList<>();
                                for (ComboModel comboModel : pojo.getCombo_list()) {
                                    cmbname.add(comboModel.getItem_name());
                                }
                                dishes.put("combo",TextUtils.join(",",cmbname));
                            }
                        }

                        if(pojo.getHsn_no()!=null && pojo.getHsn_no().trim().length()>0)
                            dishes.put("hsn_no",pojo.getHsn_no());
                        jsonArray.put(dishes);
                    }
                    kpList.add(kplist);
                    totList.add(totlist);
                    if(discountamt>0) {
                        if (discountamt > granttodal){
                            discountamt=granttodal;
                            granttodal = 0.0;
                        }else
                            granttodal=granttodal-discountamt;
                    }

                    if (dis_per != null && !dis_per.isEmpty()) {
                        disper=Double.parseDouble(dis_per);
                        dis_amt = (disper * granttodal) / 100f;
                        totdisc=discountamt+dis_amt;
                    } else if(discountPojo!=null && !discountPojo.getDiscount_type().equalsIgnoreCase("complementary")){
                        if(Double.parseDouble(discountPojo.getBill_amt())<=granttodal){
                            if (discountPojo.getDiscount_type().equalsIgnoreCase("percentage"))
                                dis_amt = (Double.parseDouble(discountPojo.getDiscount_percentage()) * granttodal) / 100f;
                            else if (discountPojo.getDiscount_type().equalsIgnoreCase("amount"))
                                dis_amt=(Double.parseDouble(discountPojo.getDiscount_amt()));
                            Double dper = (dis_amt * 100) / granttodal;
                            disper=dper;
                            totdisc=discountamt+dis_amt;
                            if(offer_id.isEmpty()) {
                                offer_id = discountPojo.getId();
                                offer_name=discountPojo.getName();
                            }else{
                                offer_id=offer_id+","+discountPojo.getId();
                                offer_name=offer_name+","+discountPojo.getName();
                            }
                        }
                    }

                    if(disper>0 && taxInvoiceList!=null && taxInvoiceList.size()>0) {
                        Double tot_tax_dis_amt=0.0,ttamt=0.0;
                        for (TaxInvoicePojo t : taxInvoiceList) {
                            TaxInvoicePojo tnew=new TaxInvoicePojo();
                            tnew.setType(t.getType());
                            tnew.setId(t.getId());
                            tnew.setName(t.getName());
                            tnew.setPer(t.getPer());
                            tnew.setPer_total(t.getPer_total());
                            double tamt=Double.parseDouble(t.getAmount_total());
                            double tax_dis_amt = (disper * tamt) / 100f;
                            tot_tax_dis_amt=tot_tax_dis_amt+tax_dis_amt;
                            tamt = tamt - tax_dis_amt;
                            tnew.setAmount_total(pf.setFormat(tamt+""));
                            ttamt=ttamt+tamt;
                            taxDisList.add(tnew);
                        }
                        Double samt=subtotal+tot_tax_dis_amt;
                        subtotal=samt;
                    }else {
                        totdisc = discountamt;
                        taxDisList=taxInvoiceList;
                    }
                    if (ser_per != null && !ser_per.isEmpty()) {
                        ser_amt = (Double.parseDouble(ser_per) * granttodal) / 100f;
                    }
                    granttodal = granttodal + ser_amt- dis_amt;
                    JSONObject jsonObject=new JSONObject();
                    if(orderJSONList!=null && orderJSONList.size()>i){
                        jsonObject=orderJSONList.get(i);
                    }
                    jsonObject.put("order", jsonArray);
                    jsonObject.put("offer_id", offer_id);
                    jsonObject.put("offer_name", offer_name);
                    jsonObject.put("total_amt",pf.setFormat(subtotal+""));
                    jsonObject.put("grand_total",pf.setFormat(granttodal+""));
                    if(compItem!=null){
                        totdisc=totdisc+Double.parseDouble(compItem.getTot_disc());
                    }
                    if(totdisc>0){
                        JSONObject j = new JSONObject();
                        j.put("discount_amount", pf.setFormat(totdisc+ ""));
                        j.put("discount_per", pf.setFormat(disper+""));
                        if(compItem!=null) {
                            j.put("discount_type", discountPojo.getDiscount_type());
                            dis_amt=dis_amt+Double.parseDouble(compItem.getTot_disc());
                        }else if(disper>0 && discountPojo!=null)
                            j.put("discount_type", discountPojo.getDiscount_type());
                        jsonObject.put("discount",j + "");
                        jsonObject.put("bill_amt_discount",pf.setFormat(dis_amt+""));
                    }
                    if(ser_amt>0)
                        jsonObject.put("service_charge",pf.setFormat(ser_amt+""));
                    JSONArray taxjson = new JSONArray();
                    for(TaxInvoicePojo p:taxDisList){
                        JSONObject jt=new JSONObject();
                        jt.put("tax_name",p.getName());
                        jt.put("tax_per",p.getPer());
                        jt.put("tax_value",pf.setFormat(p.getAmount_total()));
                        taxjson.put(jt);
                    }
                    jsonObject.put("taxes", taxjson);
                    orderJSONList.set(i,jsonObject);
                    add_split_row(granttodal + "", ll, tv, txt, i);
                    i++;
                }
            }catch (JSONException e){
                Log.d(TAG,"error:"+e.getMessage());
            }
        }

        btnclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sDialog.dismiss();
            }
        });

        sDialog.show();
    }

    private void modifyNo(List<TextView> tvList){
        for(int t=1;t<=tvList.size();t++){
            TextView tv=tvList.get(t-1);
            if(sDialog!=null && sDialog.isShowing())
                tv.setText(t+")");
            else
                tv.setText(t+"");
            //  tv.setId(t);
        }
    }

    public void add_split_row(String tot, final LinearLayout ll, TextView tv, List<String> txt, final int pos){
        final View v = LayoutInflater.from(context).inflate(R.layout.split_item_row, null);
        TextView tvamt=v.findViewById(R.id.tvamt);
        TextView tvname=v.findViewById(R.id.tvname);
        final TextView tvsrno=v.findViewById(R.id.tvsrno);
        final Spinner spn=(Spinner)v.findViewById(R.id.spnpaymode);
        final EditText etnm=(EditText)v.findViewById(R.id.etnm);
        etnm.setTypeface(AppConst.font_regular(context));
        final EditText etph=(EditText)v.findViewById(R.id.etphone);
        etph.setTypeface(AppConst.font_regular(context));
        Button btncharge=(Button)v.findViewById(R.id.btncharge);
        btncharge.setTypeface(AppConst.font_regular(context));
        int tagid=tv.getId()+10;
        spn.setId(tagid);
        tvamt.setText(pf.setFormat(tot));
        tvname.setText(TextUtils.join(", ",txt));
        tvsrno.setText(tv.getText().toString()+")");
        ArrayAdapter<String> pada=new ArrayAdapter<String>(context, R.layout.spn_category_row, R.id.txt,wlist);
        spn.setAdapter(pada);
        etnmist.add(etnm);
        etphlist.add(etph);
        textViewList.add(tvsrno);
        btncharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                M.showLoadingDialog(context);
                GenerateToken generateToken=new GenerateToken();
                JSONObject tokenJson=generateToken.generateToken(context);
                try {
                    SimpleDateFormat dtfmt=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date todt=new Date();
                    String tm = AppConst.arabicToEng(dtfmt.format(todt));
                    String timestamp= AppConst.arabicToEng(todt.getTime()+"");
                    String tp = plist.get(spn.getSelectedItemPosition()).getId();
                    String tp_text=plist.get(spn.getSelectedItemPosition()).getType();
                    JSONObject jsonObject=orderJSONList.get(pos);
                    jsonObject.put("user_id", M.getWaiterid(context));
                    jsonObject.put("user_name", M.getWaitername(context));
                    jsonObject.put("restaurant_id", M.getRestID(context));
                    jsonObject.put("rest_unique_id", M.getRestUniqueID(context));
                    jsonObject.put("order_type", en_ordertype);
                    jsonObject.put("order_type_lang", ordertype);
                    jsonObject.put("pay_mode", tp);
                    jsonObject.put("pay_mode_text",tp_text);
                    jsonObject.put("txn_id", "");
                    jsonObject.put("order_time", AppConst.arabicToEng(tm));
                    jsonObject.put("order_timestamp", timestamp);
                    jsonObject.put("order_no", tokenJson.getString("orderno"));
                    jsonObject.put("order_comment", "");
                    jsonObject.put("cust_name", etnm.getText().toString());
                    jsonObject.put("cust_phone", etph.getText().toString());
                    jsonObject.put("cust_email", "");
                    jsonObject.put("cust_address", "");
                    jsonObject.put("tax_no","");
                    jsonObject.put("total_split",0);
                    jsonObject.put("is_split",false);
                    jsonObject.put("split_data",new JSONArray());
                    jsonObject.put("token", tokenJson.getString("token"));
                    jsonObject.put("cash", "");
                    jsonObject.put("return_cash", "");
                    Log.d(TAG,"order json:"+jsonObject);
                    orderJSONList.set(pos,jsonObject);
                    ll.setTag(pos+"");
                    ets_nm=etnm;
                    ets_ph=etph;
                    s_v=v;
                    s_tv=tvsrno;
                    M.hideLoadingDialog();
                    paymentDinein(jsonObject, pos);
                }catch (JSONException e){

                }
            }
        });
        ll.addView(v);
    }

    private void paymentDinein(JSONObject jsonObject,int pos){
        try {
            jsonObject.put("userid", M.getWaiterid(context));
            jsonObject.put("usernm", M.getWaitername(context));
            jsonObject.put("rest_id", M.getRestID(context));
            jsonObject.put("rest_uniq_id", M.getRestUniqueID(context));
            if (ll.getChildCount() > 1) {
                jsonObject.put("order_no",jsonObject.getString("order_no"));
                jsonObject.put("token_number", jsonObject.getString("token"));

                if (tblid.equals("0")) {
                    jsonObject.put("cust_nm","");
                    jsonObject.put("cust_ph","");
                    jsonObject.put("sitting",tblnm);
                } else {
                    jsonObject.put("noppl","");
                }
                JSONArray jOrder=jsonObject.getJSONArray("order");
                List<String> oids=new ArrayList<>();
                for(int p=0;p<jOrder.length();p++){
                    JSONObject j=jOrder.getJSONObject(p);
                    String odid=j.getString("orderdetail_id");
                    oids.add(odid);
                }
                jsonObject.put("order_detail_ids",TextUtils.join(",",oids));
            }else {
                jsonObject.put("order_id", order_id);
                jsonObject.put("order_no",orderno);
                jsonObject.put("token_number", token);
            }
            jsonObject.put("cash","");
            jsonObject.put("cust_address","");
            jsonObject.put("cust_email","");
            jsonObject.put("cust_tax_no","");
            jsonObject.put("order_comment","");
            jsonObject.put("return_cash","");
            jsonObject.put("table_id",tblid);
            jsonObject.put("issplit","no");
            jsonObject.put("split_array",new JSONArray()+"");
            jsonObject.put("sel_pos",pos);
            M.showLoadingDialog(context);
            JSONObject apiObj=new JSONObject();
            apiObj.put("key",WifiHelper.api_split_order);
            apiObj.put("msg",jsonObject+"");
            apiObj.put("user_ip",getLocalIp());
            sendToServer(apiObj+"");
        } catch (Exception e) {
            Log.d(TAG,"error:"+e.getMessage());
            e.printStackTrace();
        }
    }

    private void printBill(final JSONObject jsonObject, int pos){
        if (M.isCashPrinter(context) && Globals.deviceType != 0) {
            createReceiptData(jsonObject,pos);
        }
    }

    public class SplitItemAdapter extends RecyclerView.Adapter<SplitItemAdapter.ContentViewHolder> {
        View view;
        ContentViewHolder contentviewholder;
        String TAG = "UpdateItemAdapter";
        List<DishOrderPojo> dishorederlist = new ArrayList<DishOrderPojo>();
        List<String> sellist = new ArrayList<String>(),slist=new ArrayList<>();
        Context mcontext;
        PrintFormat pf;

        public SplitItemAdapter(Context mcontext, List<DishOrderPojo> dishorederlist) {
            this.dishorederlist = dishorederlist;
            this.mcontext = mcontext;
            pf=new PrintFormat(mcontext);
        }

        public List<String> getSelected(){
            return sellist;
        }

        public void chipSelUpdate(){
            slist=new ArrayList<>();
            if(tvsel!=null){
                String tag=tvsel.getTag().toString();
                if(tag!=null && !tag.isEmpty())
                    slist=new ArrayList<String>(Arrays.asList(tag.split(",")));
            }
            notifyDataSetChanged();
        }

        public void removeChipUpdate(List<String> tags){
            for(String s:tags)
                sellist.remove(s);
        }

        public class ContentViewHolder extends RecyclerView.ViewHolder{

            TextView txtdishname, txtqty,txtpre,txtprice,txtdisc;
            CheckBox chk;
            LinearLayout ll;

            public ContentViewHolder(View itemView) {
                super(itemView);
                ll=(LinearLayout)itemView.findViewById(R.id.ll);
                txtpre=(TextView)itemView.findViewById(R.id.txtpre);
                txtdishname = (TextView) itemView.findViewById(R.id.txtdishname);
                txtqty = (TextView) itemView.findViewById(R.id.txtqty);
                txtprice=(TextView)itemView.findViewById(R.id.txtprice);
                txtdisc=(TextView)itemView.findViewById(R.id.tvdiscount);
                chk=(CheckBox)itemView.findViewById(R.id.chk);
                chk.setClickable(false);
            }

        }

        @Override
        public SplitItemAdapter.ContentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_order_item, parent, false);
            contentviewholder = new ContentViewHolder(view);
            return contentviewholder;
        }

        @Override
        public void onBindViewHolder(final SplitItemAdapter.ContentViewHolder holder, final int position) {

            final DishOrderPojo model=dishorederlist.get(position);

            if(model.getWeight()!=null && model.getSort_nm()!=null && model.getWeight().trim().length()>0 && model.getSort_nm().trim().length()>0) {
                holder.txtdishname.setText(model.getDishname()+" "+model.getWeight()+" "+model.getSort_nm());
            }else{
                holder.txtdishname.setText(model.getDishname());
            }

            String pq=model.getQty();
            Double amt=0.0;
            if(M.isPriceWT(mcontext)){
                amt=Double.parseDouble(model.getPrice_without_tax());
            }else if(model.getPriceperdish()!=null && model.getPriceperdish().trim().length()>0){
                amt=Double.parseDouble(model.getPrice());
            }
            if(model.isnew())
                amt=amt*Integer.parseInt(pq);
            holder.txtqty.setText(pq);
            holder.txtprice.setText(pf.setFormat(amt+""));

            if(model.getPrenm()!=null && model.getPrenm().trim().length()>0) {
                holder.txtpre.setVisibility(View.VISIBLE);
                holder.txtpre.setText(model.getPrenm());
            }else
                holder.txtpre.setVisibility(View.GONE);

            if(model.getTot_disc()!=null && model.getTot_disc().trim().length()>0 && Double.parseDouble(model.getTot_disc())>0) {
                holder.txtdisc.setVisibility(View.VISIBLE);
                holder.txtdisc.setText(mcontext.getString(R.string.txt_save)+": " + model.getDiscount());
            }else
                holder.txtdisc.setVisibility(View.GONE);

            if(sellist.contains(position+"")) {
                holder.chk.setChecked(true);
                holder.txtpre.setTextColor(mcontext.getResources().getColor(R.color.medium_grey));
                holder.txtdishname.setTextColor(mcontext.getResources().getColor(R.color.medium_grey));
                holder.txtqty.setTextColor(mcontext.getResources().getColor(R.color.medium_grey));
                holder.txtprice.setTextColor(mcontext.getResources().getColor(R.color.medium_grey));
                holder.txtdisc.setTextColor(mcontext.getResources().getColor(R.color.medium_grey));
                if(!slist.contains(position+"")){
                    holder.chk.setVisibility(View.INVISIBLE);
                }else {
                    holder.chk.setVisibility(View.VISIBLE);
                }
            }else {
                holder.chk.setVisibility(View.VISIBLE);
                holder.chk.setChecked(false);
                holder.txtpre.setTextColor(mcontext.getResources().getColor(R.color.primary_text));
                holder.txtdishname.setTextColor(mcontext.getResources().getColor(R.color.primary_text));
                holder.txtqty.setTextColor(mcontext.getResources().getColor(R.color.primary_text));
                holder.txtprice.setTextColor(mcontext.getResources().getColor(R.color.primary_text));
                holder.txtdisc.setTextColor(mcontext.getResources().getColor(R.color.primary_text));
            }
            holder.ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(holder.chk.getVisibility()==View.VISIBLE) {
                        if (sellist.contains(position + "")) {
                            sellist.remove(position + "");
                            slist.remove(position + "");
                        } else {
                            sellist.add(position + "");
                            slist.add(position + "");
                        }
                        tvsel.setTag(TextUtils.join(",", slist));
                        notifyDataSetChanged();
                        cal();
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return dishorederlist.size();
        }

        @Override
        public long getItemId(int position) {
            return super.getItemId(position);
        }
    }

    private boolean createReceiptData(JSONObject jsonObject,int pos) {
        Log.d(TAG,"==print==");
        Log.d(TAG,jsonObject+"");
        StringBuilder textData = new StringBuilder();
        String method = "";
        File myDir = new File(AppConst.folder_dir + "logo.png");
        pf.setPaperSize(M.retriveVal(M.key_bill_width,context));
        try {
            String tm = jsonObject.getString("order_time");
            String customername = jsonObject.getString("cust_name");
            String customernum=jsonObject.getString("cust_phone");
            String ordercomment="";
            if(jsonObject.has("order_comment"))
                ordercomment=jsonObject.getString("order_comment");
            String order_no = jsonObject.getString("order_no");
            String token_number = jsonObject.getString("token");
            if (token_number == null) {
                token_number = order_no.split("-")[1];
            }
            String subtotal = jsonObject.getString("total_amt");
            String finalprice = jsonObject.getString("grand_total");
            String payment_mode_text = "";
            if (jsonObject.has("pay_mode_text"))
                payment_mode_text = jsonObject.getString("pay_mode_text");
            String pay_mode_id = null;
            if (jsonObject.has("pay_mode"))
                pay_mode_id = jsonObject.getString("pay_mode");
            JSONArray j1 = null;
            if (jsonObject.has("taxes"))
                j1 = jsonObject.getJSONArray("taxes");
            JSONArray jorder = jsonObject.getJSONArray("order");
            if(M.isCashPrinter(context)) {

                if(Globals.deviceType<5) {
                    textData.append("$codepage3$");
                    textData.append("$al_center$$al_center$");
                    if (PrintFormat.setSize== PrintFormat.normalsize) {
                        textData.append("$bigh$" + " $al_center$");
                        textData.append(pf.padLine2(M.getBrandName(context), "") + " $big$ \n\n");
                    } else {
                        textData.append("$bold$" + "$bigl$" + " $al_center$");
                        textData.append(pf.padLine2(M.getBrandName(context), "") + " $big$ $unbold$\n\n");
                    }
                }else{
                    if (myDir.exists() && Globals.deviceType==5) {
                        Bitmap myBitmap = BitmapFactory.decodeFile(myDir.getAbsolutePath());
                        AidlUtil.getInstance().printBitmap(myBitmap);
                    }
                    textData.append(pf.padLine2(M.getBrandName(context), "") + "\n\n");
                }
                textData.append(pf.padLine2(M.getRestName(context),"") + "\n");
                textData.append(pf.padLine2(M.getRestAddress(context),"")+"\n");
                textData.append(pf.padLine2(getString(R.string.txt_phone) + ": " + M.getRestPhoneNumber(context),"") + "\n");
                if (M.getGST(context) != null && M.getGST(context).trim().length() > 0)
                    textData.append(pf.padLine2(getString(R.string.txt_tax) + " # " + M.getGST(context),"") + "\n");
                textData.append(pf.divider()+"\n");
                String tag_wnm=getString(R.string.txt_order_by);

                if(PrintFormat.setSize== PrintFormat.normalsize)
                    textData.append(pf.padLine2(tag_wnm + " : " + M.getWaitername(context),tm) + "\n");
                else {
                    textData.append(pf.padLine2(tag_wnm + " : " + M.getWaitername(context), "") + "\n");
                    textData.append(pf.padLine2(tm,"") + "\n");
                }

                boolean showpt=false;
                if(!pay_mode_id.equals("0") && payment_mode_text!=null && payment_mode_text.trim().length()>0)
                    showpt=true;
                if(jsonObject!=null && jsonObject.has("payment_status") && jsonObject.getString("payment_status").equals("unpaid"))
                    showpt=false;
                if(showpt)
                    textData.append(pf.padLine2(context.getString(R.string.payment_by) + " : " + payment_mode_text , "" ) + "\n");
                String tag_token="",tag_ot="";
                tag_token=getString(R.string.txt_token)+" # "+token_number;

                tag_ot=ordertype;

                    textData.append(pf.padLine2(getString(R.string.invoice_no)+" # "+order_no,"")+"\n");
                    textData.append(pf.divider()+"\n");
                    if(!tblid.equals("0"))
                        textData.append(pf.padLine2(tag_token,getString(R.string.table).toUpperCase()+" #"+tblnm) +"\n");
                    else
                        textData.append(pf.padLine2(tag_token,"#"+tblnm) +"\n");


                textData.append(pf.divider()+"\n");
                if (customername!=null && customername.length() > 0) {
                    textData.append(pf.padLine2(getString(R.string.txt_customer_name) + " "+customername,"")+ "\n");
                    textData.append(pf.divider()+"\n");
                }
                if(ordercomment!=null && ordercomment.trim().length()>0) {
                    textData.append(pf.padLine2(ordercomment,"") + "\n");
                    textData.append(pf.divider()+"\n");
                }
                if (jsonObject!=null && jsonObject.has("tax_no")) {
                    if(!jsonObject.getString("tax_no").isEmpty()) {
                        textData.append(pf.padLine2(jsonObject.getString("tax_no"),"") + "\n");
                        textData.append(pf.divider() + "\n");
                    }
                }

                if(PrintFormat.setSize== PrintFormat.normalsize)
                    textData.append(pf.padLine1(getString(R.string.item), getString(R.string.txt_qty), getString(R.string.txt_rate),getString(R.string.txt_amount)) + "\n");
                else
                    textData.append(pf.padLine1(getString(R.string.item), getString(R.string.txt_qty), "",getString(R.string.txt_amount)) + "\n");
                textData.append(pf.divider()+"\n");
                long tot_qty=0,tot_item=0;
                for (int i = 0; i < jorder.length(); i++) {
                    JSONObject obj = jorder.getJSONObject(i);
                    boolean print=true;
                    String st="";
                    if(obj.has("status"))
                        st=obj.getString("status");
                    if(st!=null && st.equals("4"))
                        print=true;
                    if(print) {
                        tot_item = tot_item + 1;
                        String qty = obj.getString("quantity");
                        tot_qty = (Long.parseLong(qty)) + tot_qty;
                        String name = obj.getString("dishname");
                        String finalpri = obj.getString("price");
                        String price = obj.getString("priceperdish");
                        if (M.isPriceWT(context)) {
                            if (obj.has("price_wt"))
                                finalpri = obj.getString("price_wt");
                            if (obj.has("priceperdish_wt"))
                                price = obj.getString("priceperdish_wt");
                        }
                        String we = "";
                        if (obj.has("weight")) {
                            we = obj.getString("weight");
                            if (we != null && we.trim().length() > 0 && Double.parseDouble(we) > 0) {
                                we = we + obj.getString("unit_sort_name");
                            } else
                                we = "";
                        }
                        textData.append(pf.padLine1(name + " " + we, qty + "", pf.setFormat(price), pf.setFormat(finalpri + "")) + "\n");
                        if (obj.has("pre_array")) {
                            JSONArray jPref = obj.getJSONArray("pre_array");
                            for (int p = 0; p < jPref.length(); p++) {
                                JSONObject prefObj = jPref.getJSONObject(p);
                                String pqty = prefObj.getString("quantity");
                                String pprice = prefObj.getString("amount");
                                if (M.isPriceWT(context) && prefObj.has("amount_wt"))
                                    pprice = prefObj.getString("amount_wt");
                                textData.append(pf.padLine2("    " + pqty+" X "+prefObj.getString("name") + "-" + pf.setFormat(pprice), "   ") + "\n");
                            }
                        } else if (obj.has("prenm")) {
                            String pnm = obj.getString("prenm");
                            if (pnm != null && pnm.length() > 0) {
                                textData.append(pf.padLine1("    " + pnm, "", "", "") + "\n");
                            }
                        }
                        String hsn=null;
                        if(obj.has("hsn_no"))
                            hsn=obj.getString("hsn_no");
                        if ( hsn != null && hsn.trim().length() > 0)
                            textData.append(pf.padLine1(context.getString(R.string.txt_hsn) + hsn, "", "", "") + "\n");
                        if (obj.has("discount")) {
                            String idisc = obj.getString("discount");
                            if (idisc != null && idisc.trim().length() > 0 && Double.parseDouble(idisc) > 0) {
                                textData.append(pf.padLine1("    ", " ", " ", context.getString(R.string.txt_discount) + " " + idisc) + "\n");
                            }
                        }

                        if (obj.has("combo")) {
                            String pnm = obj.getString("combo");
                            if (pnm != null && pnm.length() > 0) {
                                if (pnm.contains(",")) {
                                    String[] cs = pnm.split(",");
                                    for (String s : cs)
                                        textData.append(pf.padLine1(s, "", "", "") + "\n");
                                } else
                                    textData.append(pf.padLine1(pnm, "", "", "") + "\n");
                            }
                        }

                        if (obj.has("package")) {
                            String pnm = obj.getString("package");
                            if (pnm != null && pnm.length() > 0) {
                                if (pnm.contains(",")) {
                                    String[] cs = pnm.split(",");
                                    for (String s : cs)
                                        textData.append(pf.padLine1(s, "", "", "") + "\n");
                                } else
                                    textData.append(pf.padLine1(pnm, "", "", "") + "\n");
                            }
                        }
                    }
                }

                textData.append(pf.divider()+"\n");
                textData.append(pf.padLine2(context.getString(R.string.txt_items)+":"+tot_item ,context.getString(R.string.txt_qty)+":"+tot_qty) + "\n");
                textData.append(pf.divider()+"\n");
                //sub total
                textData.append(pf.padLine2(getString(R.string.txt_subtotal) , pf.setFormat(subtotal)) + "\n");
                //Discount
                JSONObject jdisc=null;
                if(jsonObject!=null && jsonObject.has("discount")){
                    jdisc=new JSONObject(jsonObject.getString("discount"));
                }
                if(jdisc!=null && jdisc.has("discount_amount")) {
                    String pdisc = jdisc.getString("discount_amount");
                    if (pdisc != null && pdisc.trim().length() > 0 && Double.parseDouble(pdisc) > 0) {
                        textData.append(pf.padLine2(getString(R.string.txt_discount), pf.setFormat(jdisc.getString("discount_amount") + "")) + "\n");
//                        if (offer_name != null && offer_name.trim().length() > 0)
//                            textData.append(pf.padLine2("(" + offer_name + ")", "") + "\n");
                    }
                }
                //Loyalty Points
//                if(jsonObject!=null && jsonObject.has("redeem_points")){
//                    textData.append(pf.padLine2(getString(R.string.redeem_point)+"("+pointJSON.getString("redeem_points")+")",pf.setFormat(pointJSON.getString("redeem_amount"))) + "\n");
//                    if(Globals.deviceType>=5 || billinfo.equals("share"))
//                        textData.append(pf.padLine2("("+pointJSON.getString("points_per_one_currency")+getString(R.string.txt_points)+"=1)","  " )+ "\n");
//                    else
//                        textData.append("$small$"+pf.padLine2("("+pointJSON.getString("points_per_one_currency")+getString(R.string.txt_points)+"=1)","  " )+ "$big$\n");
//                }
                //tax
                if (j1 != null && j1.length() > 0) {
                    for (int i = 0; i < j1.length(); i++) {
                        JSONObject j = j1.getJSONObject(i);
                        String nm = j.getString("tax_name");
                        String per = j.getString("tax_per");
                        String val = j.getString("tax_value");
                        textData.append(pf.padLine2(nm + " @" + per + "%", val) + "\n");
                    }
                }
                //Service Charge
                if (jsonObject!=null && jsonObject.has("service_charge")){
                    textData.append(pf.padLine2(M.retriveVal(M.key_custom_service_charge,context), pf.setFormat(jsonObject.getString("service_charge") + "")) + "\n");
                }
                //Packaging Charge
//                if(pack_charge!=null && pack_charge.trim().length()>0) {
//                    Double pa=Double.parseDouble(pack_charge);
//                    if(pa>0)
//                        textData.append(pf.padLine2(context.getString(R.string.txt_packaging_charge) ,pf.setFormat(pack_charge))+ "\n");
//                }
                //Cash Rounding
//                if(M.isQuickMode(context) && cc_payment_mode.getVisibility()==View.VISIBLE){
//                    if(Double.parseDouble(cc_payment_mode.getTag().toString())!=0)
//                        textData.append(pf.padLine2(getString(R.string.cash_rounding), pf.setFormat(cc_payment_mode.getTag()+"")) + "\n");
//                }else if(tvrounding!=null && tvrounding.getTag()!=null){
//                    if(Double.parseDouble(tvrounding.getTag().toString())!=0)
//                        textData.append(pf.padLine2(getString(R.string.cash_rounding), pf.setFormat(tvrounding.getTag()+"")) + "\n");
//                }
//                //Tip
//                if(jsonObject!=null && jsonObject.has("tip")){
//                    JSONObject tJ=new JSONObject(jsonObject.getString("tip"));
//                    if(tJ.has("tip_amount"))
//                        textData.append(pf.padLine2(context.getString(R.string.txt_tip), pf.setFormat(tJ.getString("tip_amount"))) + "\n");
//
//                }
                //Grand Total
                String billtotal="0.0";
                if(jsonObject!=null && jsonObject.has("grand_total")){
                    billtotal=jsonObject.getString("grand_total");
                }

                if(Globals.deviceType>=5)
                    textData.append(pf.padLine3(context.getString(R.string.txt_total), pf.setFormat(billtotal)) + "\n");
                else {
                    if (PrintFormat.setSize == PrintFormat.normalsize) {
                        textData.append("$bighw$" + " $al_center$");
                        textData.append(pf.padLine3(context.getString(R.string.txt_total), pf.setFormat(billtotal)) + " $big$ \n");
                    } else {
                        textData.append("$bold$" + "$bigh$" + " $al_center$");
                        textData.append(pf.padLine3(context.getString(R.string.txt_total), pf.setFormat(billtotal)) + " $big$ $unbold$\n");
                    }
                }

                textData.append(pf.divider()+"\n");
                if (M.getreceipt_footer(context).length() == 0) {
                    textData.append(pf.padLine2(getString(R.string.txt_thanku),"") + "\n");
                    textData.append(pf.padLine2(getString(R.string.txt_visitagain),"") + "\n");
                } else {
                    textData.append(pf.padLine2(M.getreceipt_footer(context),"") + "\n");
                }

                textData.append(pf.padLine2(getString(R.string.txt_powered_by) + " " + M.getfooterphone(context),"")+"\n");
                if(Globals.deviceType<5) {
                    textData.append("\n\n$intro$$intro$$intro$$intro$$cutt$$intro$");
                    textData.append("$drawer$");
                    textData.append("$drawer2$");
                }


                Log.d(TAG,"cash printer:");
                Log.d(TAG,textData+"");
                if(Globals.deviceType==5){
                    textData.append("\n\n\n");
                    AidlUtil.getInstance().printText(textData.toString(), PrintFormat.sunmi_font, true, false);
                    AidlUtil.getInstance().cuttpaper();
                    AidlUtil.getInstance().openDrawer();
                    AidlUtil.getInstance().lcdmessage(""+payment_mode_text,"Total : "+billtotal);
                }else if(Globals.deviceType==9){
                    JSONObject jdata=new JSONObject();
                    jdata.put("data",textData.toString());
                    StarPrintReceipt sp=new StarPrintReceipt(context,SplitBillActivity.this,jdata);
                    if(M.isadvanceprint(M.key_bill,context))
                        sp.printusingstar();
                    else
                        sp.printBill(textData.toString());
                }else {
                    Intent sendIntent;
                    if(M.isadvanceprint(M.key_bill,context)){
                        GlobalsNew.setReceipttype("2");
                        JSONObject jdata=new JSONObject();
                        jdata.put("data",textData.toString());
                        GlobalsNew.setJsonObject(jdata);
                        sendIntent = new Intent(context, PrintActivityNew.class);
                    }else{
                        sendIntent = new Intent(context, PrintActivity.class);
                    }
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, textData.toString());
                    sendIntent.putExtra("internal", "1");
                    sendIntent.setType("text/plain");
                    this.startActivity(sendIntent);
                }
            }

        }catch (JSONException e){
            Log.d(TAG,"json error:"+ e.getMessage());
        } catch (Exception e) {
            Log.d(TAG,"error:"+ e.getMessage());
            return false;
        }

        textData = null;
        return true;
    }

//    public String getLocalIp() {
//        WifiManager wm = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
//        return Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
//    }

    public String getLocalIp() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface networkInterface = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = networkInterface.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                        String host = inetAddress.getHostAddress();
                        if (!TextUtils.isEmpty(host)) {

                            return host;
                        }
                    }
                }

            }
        } catch (Exception ex) {
            Log.e("IP Address", "getLocalIpAddress", ex);
        }
        return null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        if(broadcastReceiver!=null)
            unregisterReceiver(broadcastReceiver);
        if(mBound && mConnection!=null)
            unbindService(mConnection);
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }
}
