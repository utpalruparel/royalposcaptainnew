package com.royalpos.waiter.model;

/**
 * Created by reeva on 14/5/18.
 */

public class AccountPojo {

    String success,status,membership_end_date;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMembership_end_date() {
        return membership_end_date;
    }

    public void setMembership_end_date(String membership_end_date) {
        this.membership_end_date = membership_end_date;
    }
}
