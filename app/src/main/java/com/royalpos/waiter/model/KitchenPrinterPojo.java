package com.royalpos.waiter.model;

/**
 * Created by reeva on 19/3/18.
 */

public class KitchenPrinterPojo {

    String id,categoryid,usbDeviceID,usbVendorID,usbProductID,blueToothDeviceAdress,deviceType,deviceIP,devicePort,link_code,picturePath,logoProcesed;
    String printer_name,printer_title;
    String categorynm,paper_width,star_settings;
    boolean isAdv;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(String categoryid) {
        this.categoryid = categoryid;
    }

    public String getCategorynm() {
        return categorynm;
    }

    public void setCategorynm(String categorynm) {
        this.categorynm = categorynm;
    }

    public String getPrinter_name() {
        return printer_name;
    }

    public void setPrinter_name(String printer_name) {
        this.printer_name = printer_name;
    }

    public String getUsbDeviceID() {
        return usbDeviceID;
    }

    public void setUsbDeviceID(String usbDeviceID) {
        this.usbDeviceID = usbDeviceID;
    }

    public String getUsbVendorID() {
        return usbVendorID;
    }

    public void setUsbVendorID(String usbVendorID) {
        this.usbVendorID = usbVendorID;
    }

    public String getUsbProductID() {
        return usbProductID;
    }

    public void setUsbProductID(String usbProductID) {
        this.usbProductID = usbProductID;
    }

    public String getBlueToothDeviceAdress() {
        return blueToothDeviceAdress;
    }

    public void setBlueToothDeviceAdress(String blueToothDeviceAdress) {
        this.blueToothDeviceAdress = blueToothDeviceAdress;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceIP() {
        return deviceIP;
    }

    public void setDeviceIP(String deviceIP) {
        this.deviceIP = deviceIP;
    }

    public String getDevicePort() {
        return devicePort;
    }

    public void setDevicePort(String devicePort) {
        this.devicePort = devicePort;
    }

    public String getLink_code() {
        return link_code;
    }

    public void setLink_code(String link_code) {
        this.link_code = link_code;
    }

    public String getPicturePath() {
        return picturePath;
    }

    public void setPicturePath(String picturePath) {
        this.picturePath = picturePath;
    }

    public String getLogoProcesed() {
        return logoProcesed;
    }

    public void setLogoProcesed(String logoProcesed) {
        this.logoProcesed = logoProcesed;
    }

    public String getPrinter_title() {
        return printer_title;
    }

    public void setPrinter_title(String printer_title) {
        this.printer_title = printer_title;
    }

    public boolean isAdv() {
        return isAdv;
    }

    public void setAdv(boolean adv) {
        isAdv = adv;
    }

    public String getPaper_width() {
        return paper_width;
    }

    public void setPaper_width(String paper_width) {
        this.paper_width = paper_width;
    }

    public String getStar_settings() {
        return star_settings;
    }

    public void setStar_settings(String star_settings) {
        this.star_settings = star_settings;
    }
}
