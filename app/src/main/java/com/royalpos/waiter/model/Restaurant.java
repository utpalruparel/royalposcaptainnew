package com.royalpos.waiter.model;

import java.util.HashMap;
import java.util.Map;

public class Restaurant {

    private Integer id;
    private String name,gst_no,brand_name,brand_user_id, username;
    private String address;
    private String phno;
    private Integer country;
    private Integer city;
    private String fax;
    private String email;
    private String google_place_id;
    private String website;
    private String logo;
    private Integer plan_id;
    private String membership_start_date;
    private String membership_end_date;
    private String rest_unique_id;
    private String status;
    private String created_at;
    private String updated_at;
    String currency,cash_drawer,reporting_emails,recipe_inventory,start_stock_deduct;
    String type_of_business,business_type_name,recipt_footer_phone;
    String packaging_charge,can_change_price;

    private String receipt_footer;
    private String is_rounding_on;
    private String rounding_id,rounding_rule,rounding_tag,interval_number;
    private String ezetap_status,show_item_price_without_tax;
    String pin;

    private String latitude;
    private String longitude;
    private Integer allowed_captain_per_outlet;
    private String outlet_opening_timing;
    private String outlet_closing_timing;
    private String outlet_current_status;
    private Integer delivery_distance;
    private String is_customer_app_enabled;
    private String check_pin_admin;
    private String check_pin_cashier;
    private String points_per_one_currency,service_charges,decimal_value;

    String barcode_receipt_payment;
    String check_foc,foc_amt;
    String allow_payment,allow_barcode_payment;//only for sync outlet data
    String advance_order,adv_order_duration;
    String square_enable,square_mapped_pay_mode;
    String allow_update_order,allow_item_wise_discount,item_master_on_app;
    String tip_calculation,tip_calculation_preference;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGst_no() {
        return gst_no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhno() {
        return phno;
    }

    public void setPhno(String phno) {
        this.phno = phno;
    }

    public Integer getCountry() {
        return country;
    }

    public void setCountry(Integer country) {
        this.country = country;
    }

    public Integer getCity() {
        return city;
    }

    public void setCity(Integer city) {
        this.city = city;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGoogle_place_id() {
        return google_place_id;
    }

    public void setGoogle_place_id(String google_place_id) {
        this.google_place_id = google_place_id;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public Integer getPlan_id() {
        return plan_id;
    }

    public void setPlan_id(Integer plan_id) {
        this.plan_id = plan_id;
    }

    public String getMembership_start_date() {
        return membership_start_date;
    }

    public void setMembership_start_date(String membership_start_date) {
        this.membership_start_date = membership_start_date;
    }

    public String getMembership_end_date() {
        return membership_end_date;
    }

    public void setMembership_end_date(String membership_end_date) {
        this.membership_end_date = membership_end_date;
    }

    public String getRest_unique_id() {
        return rest_unique_id;
    }

    public void setRest_unique_id(String rest_unique_id) {
        this.rest_unique_id = rest_unique_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getCurrency() {
        return currency;
    }

    public String getCash_drawer() {
        return cash_drawer;
    }

    public String getReporting_emails() {
        return reporting_emails;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getBrand_user_id() {
        return brand_user_id;
    }

    public void setBrand_user_id(String brand_user_id) {
        this.brand_user_id = brand_user_id;
    }

    public String getType_of_business() {
        return type_of_business;
    }

    public void setType_of_business(String type_of_business) {
        this.type_of_business = type_of_business;
    }

    public String getBusiness_type_name() {
        return business_type_name;
    }

    public void setBusiness_type_name(String business_type_name) {
        this.business_type_name = business_type_name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRecipt_footer_phone() {
        return recipt_footer_phone;
    }

    public String getRecipe_inventory() {
        return recipe_inventory;
    }

    public void setRecipe_inventory(String recipe_inventory) {
        this.recipe_inventory = recipe_inventory;
    }

    public String getStart_stock_deduct() {
        return start_stock_deduct;
    }

    public void setStart_stock_deduct(String start_stock_deduct) {
        this.start_stock_deduct = start_stock_deduct;
    }

    public String getPackaging_charge() {
        return packaging_charge;
    }

    public String getCan_change_price() {
        return can_change_price;
    }

    public void setCan_change_price(String can_change_price) {
        this.can_change_price = can_change_price;
    }

    public String getReceipt_footer() {
        return receipt_footer;
    }

    public String getIs_rounding_on() {
        return is_rounding_on;
    }

    public String getRounding_id() {
        return rounding_id;
    }

    public String getRounding_rule() {
        return rounding_rule;
    }

    public String getRounding_tag() {
        return rounding_tag;
    }

    public String getInterval_number() {
        return interval_number;
    }

    public String getEzetap_status() {
        return ezetap_status;
    }

    public String getShow_item_price_without_tax() {
        return show_item_price_without_tax;
    }

    public String getPin() {
        return pin;
    }

    public String getIs_customer_app_enabled() {
        return is_customer_app_enabled;
    }

    public String getPoints_per_one_currency() {
        return points_per_one_currency;
    }

    public String getService_charges() {
        return service_charges;
    }

    public String getCheck_pin_admin() {
        return check_pin_admin;
    }

    public String getCheck_pin_cashier() {
        return check_pin_cashier;
    }

    public String getDecimal_value() {
        return decimal_value;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public Integer getAllowed_captain_per_outlet() {
        return allowed_captain_per_outlet;
    }

    public String getOutlet_opening_timing() {
        return outlet_opening_timing;
    }

    public String getOutlet_closing_timing() {
        return outlet_closing_timing;
    }

    public String getOutlet_current_status() {
        return outlet_current_status;
    }

    public Integer getDelivery_distance() {
        return delivery_distance;
    }

    public String getBarcode_receipt_payment() {
        return barcode_receipt_payment;
    }

    public String getCheck_foc() {
        return check_foc;
    }

    public String getFoc_amt() {
        return foc_amt;
    }

    public String getAllow_payment() {
        return allow_payment;
    }

    public String getAllow_barcode_payment() {
        return allow_barcode_payment;
    }

    public String getAdvance_order() {
        return advance_order;
    }

    public String getAdv_order_duration() {
        return adv_order_duration;
    }

    public String getSquare_enable() {
        return square_enable;
    }

    public String getSquare_mapped_pay_mode() {
        return square_mapped_pay_mode;
    }

    public String getAllow_update_order() {
        return allow_update_order;
    }

    public String getAllow_item_wise_discount() {
        return allow_item_wise_discount;
    }

    public String getItem_master_on_app() {
        return item_master_on_app;
    }

    public String getTip_calculation() {
        return tip_calculation;
    }

    public String getTip_calculation_preference() {
        return tip_calculation_preference;
    }
}