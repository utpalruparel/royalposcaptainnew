package com.royalpos.waiter.model;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.widget.Toast;

import com.royalpos.waiter.R;
import com.royalpos.waiter.database.DBPrinter;
import com.royalpos.waiter.database.DBUnit;
import com.royalpos.waiter.database.DBWaiter;
import com.royalpos.waiter.helper.AppConst;
import com.royalpos.waiter.print.PrintFormat;
import com.royalpos.waiter.ui.MenuUI;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class M {
    public static ProgressDialog pDialog;
    private static SharedPreferences mSharedPreferences;
    private static String pref_name="royalpos_waiter_settings";
    public static String key_bill_width="paper_bill_width";
    public static String key_kitchen_width="paper_kitchen_width";
    public static String key_star_modelindex="star_modelindex";
    public static String key_star_setting="star_settings";
    public static String key_dinein_delitem="allow_delitem";
    public static String key_top_space="print_top_space";
    public static String key_phonepe_status="phonepe_status";
    public static String key_phonepe_paymode_qrcode="phonepe_paymode_qrcode";
    public static String key_phonepe_paymode="phonepe_paymode";
    public static String key_server_lasttime="server_lasttime";

    public static String key_kitchen="adv_kitchen";
    public static String key_bill="adv_bill";
    public static String key_tip_cal="tip_cal";//enable / disable
    public static String key_tip_pref="tip_pref";//calculate_tip_after_taxes/calculate_tip_before_taxes
    public static String key_dinein_separate_item="separate_item";
    public static String key_custom_service_charge="custom_service_charge";

    public static void showLoadingDialog(Context mContext) {

        if(!((Activity) mContext).isFinishing()) {
            pDialog = new ProgressDialog(mContext);
            pDialog.setMessage(mContext.getString(R.string.please_wait));
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }
    }

    public static void hideLoadingDialog() {
        if(pDialog!=null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }

    public static void showToast(Context mContext, String message) {
      Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
    }

    public static void T(Context mContext, String Message) {
        Toast.makeText(mContext, Message, Toast.LENGTH_SHORT).show();
    }

    public static void L(String Message) {
        Log.e(AppConst.TAG, Message);
    }

    public static boolean setPrintToken(Boolean printtoken, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean("printtoken", printtoken);
        return editor.commit();
    }

    public static Boolean isPrintToken(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getBoolean("printtoken", true);
    }

    public static boolean setPin(Boolean askpin, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean("askpin", askpin);
        return editor.commit();
    }

    public static Boolean getPin(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getBoolean("askpin", false);
    }

    public static boolean setcashierPin(Boolean askpin, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean("cashierPin", askpin);
        return editor.commit();
    }

    public static Boolean getcashierPin(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getBoolean("cashierPin", false);
    }

    public static boolean setExpiry(String expdate, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("expdate", expdate);
        return editor.commit();
    }

    public static String getExpiry(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("expdate", null);
    }

    public static boolean setWaitername(String username, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("username", username);
        return editor.commit();
    }

    public static String getWaitername(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("username", null);
    }

    public static boolean setPlanid(String planid, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("planid", planid);
        return editor.commit();
    }

    public static String getPlanid(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("planid", null);
    }

    public static boolean setWaiterid(String waiterid, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("waiterid", waiterid);
        return editor.commit();
    }

    public static String getWaiterid(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("waiterid", null);
    }

    public static boolean setWaiterPin(String wpin, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("waiterpin", wpin);
        return editor.commit();
    }

    public static String getWaiterPin(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("waiterpin", null);
    }

    public static boolean setToken(String order_token, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("order_token", order_token);
        return editor.commit();
    }

    public static String getToken(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("order_token", null);
    }

    public static boolean setUpdateTime(String updatetime, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("updatetime", updatetime);
        return editor.commit();
    }

    public static String getUpdateTime(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("updatetime", null);
    }

    public static boolean setCashDrawer(String cash_drawer, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("cash_drawer", cash_drawer);
        return editor.commit();
    }

    public static String getCashDrawer(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("cash_drawer", "");
    }

    public static boolean setBrandId(String brandid, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putString("brandid", brandid);
        return editor.commit();
    }

    public static String getBrandId(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("brandid", "");
    }

    public static boolean setType(String type, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("type", type);
        return editor.commit();
    }

    public static String getType(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("type", "");
    }

    public static boolean setCashPrinter(Boolean pcash, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean("pcash", pcash);
        return editor.commit();
    }

    public static Boolean isCashPrinter(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getBoolean("pcash", true);
    }


    public static boolean setKitchenPrinter(Boolean pkitchen, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean("pkitchen", pkitchen);
        return editor.commit();
    }

    public static Boolean isKitchenPrinter(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getBoolean("pkitchen",true);
    }

    public static boolean setKitchenCatPrinter(Boolean pcatkitchen, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean("pcatkitchen", pcatkitchen);
        return editor.commit();
    }

    public static Boolean isKitchenCatPrinter(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getBoolean("pcatkitchen",false);
    }

    public static boolean setPrintLogo(Boolean printlogo, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean("printlogo", printlogo);
        return editor.commit();
    }

    public static Boolean isPrintLogo(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getBoolean("printlogo", true);
    }

    public static boolean setAutoPrint(Boolean autoprint, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean("autoprint", autoprint);
        return editor.commit();
    }

    public static Boolean isAutoPrint(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getBoolean("autoprint", true);
    }


    public static boolean setLowStockAlert(Boolean lowstock, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean("lowstock", lowstock);
        return editor.commit();
    }

    public static Boolean isLowStockalert(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getBoolean("lowstock", true);
    }


    public static boolean setallowlowstockpunch(Boolean lowstockpunch, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean("lowstockpunch", lowstockpunch);
        return editor.commit();
    }

    public static Boolean islowstockpunch(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getBoolean("lowstockpunch", true);
    }

    public static boolean trackCustomer(Boolean tcust, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean("tcust", tcust);
        return editor.commit();
    }

    public static Boolean isTrackCust(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getBoolean("tcust", true);
    }

    public static boolean countrycode(String countrycode, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("countrycode", countrycode);
        return editor.commit();
    }

    public static String getcountrycode(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("countrycode", "");
    }

    public static boolean setSplitOrder(String sorder, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("sorder", sorder);
        return editor.commit();
    }

    public static String getSplitOrder(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("sorder", null);
    }

    public static void setAds(ArrayList<String> adlist, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putString("adlist", TextUtils.join(",",adlist));
        editor.commit();
    }

    public static boolean setChangePrice(Boolean changeprice, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean("changeprice", changeprice);
        return editor.commit();
    }

    public static Boolean getChangePrice(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getBoolean("changeprice", false);
    }

    public static void logOut(Context mContext) {
        setRestID("", mContext);
        setRestImg(null,mContext);
        setRestName(null,mContext);
        setWaiterid("",mContext);
        setWaitername("",mContext);
        setUpdateTime("",mContext);
        waiterLogin(false, mContext);
        setdaily_balance_id("", mContext);
        setCashDrawer("",mContext);
        setReportingemails("",mContext);
        setBrandName("",mContext);
        setBrandId("",mContext);
        setTrackInventory("",mContext);
        setType("",mContext);
        setPin(false,mContext);
        setPlanid("",mContext);
        setSplitOrder(null,mContext);
        setExpiry(null,mContext);
        setDecimalVal(null,mContext);
        setPackcharge(null,mContext);
        setChangePrice(false,mContext);
        setPrintToken(true,mContext);
        setRoundFormat(false,mContext);
        setRoundBoundary("",mContext);
        setOutletPin(null,mContext);
        setDeviceCode(null,mContext);
        qtyDialog(false,mContext);
        setItemDiscount(false,mContext);
        AppConst.waiters=null;
        AppConst.selidlist=null;
        if(AppConst.dishorederlist!=null)
            AppConst.dishorederlist.clear();
        AppConst.placelist=null;
        DBUnit dbUnit=new DBUnit(mContext);
        dbUnit.deleteall();
        DBWaiter dbWaiter=new DBWaiter(mContext);
        dbWaiter.deleteall();
    }

    public static void waiterlogOut(Context mContext) {
        setWaiterid("",mContext);
        setWaitername("",mContext);
        setToken("",mContext);
        setUpdateTime("",mContext);
        waiterLogin(false, mContext);
        setdaily_balance_id("", mContext);
        setView(null,mContext);
        setPrintLogo(false,mContext);
        trackCustomer(true,mContext);
        setSamePrinter(false,mContext);
        setWaiterPin(null,mContext);
        setCashierPermission(true,mContext);
        if(getDeviceCode(mContext)!=null)
            logOut(mContext);
        AppConst.waiters=null;
        AppConst.selidlist=null;
        if(AppConst.dishorederlist!=null)
            AppConst.dishorederlist.clear();
        AppConst.placelist=null;
    }

    public static boolean setPackcharge(String trow, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("packaging_charge", trow);
        return editor.commit();
    }

    public static String getPackCharge(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("packaging_charge", "0");
    }

    public static boolean setDecimalVal(String trow, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("dval", trow);
        return editor.commit();
    }

    public static String getDecimalVal(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("dval", "2");
    }

    public static boolean setTableRow(String trow, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("tbl_row", trow);
        return editor.commit();
    }

    public static String getTableRow(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("tbl_row", null);
    }

    public static boolean setTrackInventory(String cash_drawer, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("truck_inv", cash_drawer);
        return editor.commit();
    }

    public static String getTrackInventory(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("truck_inv", "");
    }

    public static boolean setDeduction(String deduct, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("deduct", deduct);
        return editor.commit();
    }

    public static String getDeduction(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("deduct", "");
    }

    public static boolean setBrandName(String brandname, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putString("brandname", brandname);
        return editor.commit();
    }

    public static String getBrandName(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("brandname", "");
    }

    public static boolean setAdminId(String ID, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putString("adminid", ID);
        return editor.commit();
    }

    public static String getAdminId(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("adminid", "");
    }


    public static boolean setPrinterpapersize(int size, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putInt("papersize", size);
        return editor.commit();
    }

    public static int getPapersize(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getInt("papersize", PrintFormat.normalsize);
    }

    public static boolean setdaily_balance_id(String ID, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putString("daily_balance_id", ID);
        return editor.commit();
    }

    public static String getdaily_balance_id(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("daily_balance_id", "");
    }


    public static boolean setlistType(String ID, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putString("listType", ID);
        return editor.commit();
    }

    public static String getListType(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("listType", "grid");
    }

    public static boolean setRestID(String ID, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putString("restid", ID);
        return editor.commit();
    }

    public static String getRestID(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("restid", "");
    }

    public static boolean setRestUniqueID(String ID, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putString("uniqueid", ID);
        return editor.commit();
    }

    public static String getRestUniqueID(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("uniqueid", "");
    }

    public static boolean setRestName(String name, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putString("restaurant_name", name);
        return editor.commit();
    }

    public static String getRestName(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("restaurant_name", "");
    }

    public static boolean setRestUserName(String name, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putString("restaurant_user_name", name);
        return editor.commit();
    }

    public static String getRestUserName(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("restaurant_user_name", "");
    }

    public static boolean setRestAddress(String name, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putString("restaurant_address", name);
        return editor.commit();
    }

    public static String getRestAddress(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("restaurant_address", "");
    }

    public static boolean setRestPhoneNumber(String name, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putString("restaurant_phone", name);
        return editor.commit();
    }

    public static String getRestPhoneNumber(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("restaurant_phone", "");
    }

    public static boolean setRestImg(String img, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putString("restaurant_img", img);
        return editor.commit();
    }

    public static String getRestImg(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("restaurant_img", "");
    }

    public static boolean setReportingemails(String emails, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putString("emails", emails);
        return editor.commit();
    }

    public static String getReportingemails(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("emails", "");
    }

    public static boolean setCurrency(String currency, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putString("currency", currency);
        return editor.commit();
    }

    public static String getCurrency(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("currency", "");
    }

    public static boolean setGST(String gstno, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putString("gstno", gstno);
        return editor.commit();
    }

    public static String getGST(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("gstno", "");
    }

    public static boolean setCashPrinterName(String gstno, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putString("cashpritnername", gstno);
        return editor.commit();
    }

    public static String getCashPrinterName(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("cashpritnername", "");
    }

    public static boolean setKitchenPrinterName(String gstno, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putString("kitchenpritnername", gstno);
        return editor.commit();
    }

    public static String getKitchenPrinterName(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("kitchenpritnername", "");
    }

    public static boolean setCashPrinterModel(String gstno, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putString("cashpritnermodel", gstno);
        return editor.commit();
    }

    public static String getCashPrinterModel(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("cashpritnermodel", "");
    }

    public static boolean setKitchenPrinterModel(String gstno, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putString("kitchenpritnermodel", gstno);
        return editor.commit();
    }

    public static String getKitchenPrinterModel(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("kitchenpritnermodel", "");
    }

    public static boolean setCashPrinterIP(String gstno, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putString("cashpritnerIP", gstno);
        return editor.commit();
    }

    public static String getCashPrinterIP(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("cashpritnerIP", "");
    }

    public static boolean setKitchenPrinterIP(String gstno, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putString("kitchenpritnerIP", gstno);
        return editor.commit();
    }

    public static String getKitchenPrinterIP(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("kitchenpritnerIP", "");
    }

    public static boolean setSamePrinter(Boolean sprinter, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putBoolean("same_printer", sprinter);
        return editor.commit();
    }

    public static boolean isSamePrinter(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getBoolean("same_printer", false);
    }

    public static boolean waiterLogin(boolean iswaiter, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putBoolean("iswaiter", iswaiter);
        return editor.commit();
    }

    public static boolean iswaiterloggedin(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getBoolean("iswaiter", false);
    }

    public static boolean setView(String st, Context mContext) {
        //Dashboard first time choose {dinein,take away,offline dinein}
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("selview", st);
        return editor.commit();
    }

    public static String getView(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("selview", null);
    }

    public static boolean setreceipt_footer(String receipt_footer, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putString("receipt_footer", receipt_footer);
        return editor.commit();
    }

    public static String getreceipt_footer(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("receipt_footer","Thank you\n" + "Visit Again\n");
    }

    public static boolean setlogo(String logo, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putString("logo", logo);
        return editor.commit();
    }

    public static String getlogo(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("logo", "");
    }

    public static boolean setCatAlpha(Boolean isAlpha, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putBoolean("isCatAlpha", isAlpha);
        return editor.commit();
    }

    public static Boolean isCatAlpha(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getBoolean("isCatAlpha", false);
    }

    public static boolean setItemAlpha(Boolean isAlpha, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putBoolean("isItemAlpha", isAlpha);
        return editor.commit();
    }

    public static Boolean isItemAlpha(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getBoolean("isItemAlpha", false);
    }

    public static boolean setfooterphone(String ph, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("footer_phone", ph);
        return editor.commit();
    }

    public static String getfooterphone(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("footer_phone", "https://royalpos.in");
    }

    public static boolean setKOTAlert(Boolean sprinter, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putBoolean("kotalert", sprinter);
        return editor.commit();
    }

    public static boolean isKOTAlert(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getBoolean("kotalert", false);
    }

    public static boolean setItemsAlert(Boolean sprinter, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putBoolean("showitems", sprinter);
        return editor.commit();
    }

    public static boolean isItemAlert(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getBoolean("showitems", false);
    }

    public static boolean setRoundFormat(Boolean isRoundFmt, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putBoolean("isRoundFmt", isRoundFmt);
        return editor.commit();
    }

    public static boolean getRoundFormat(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getBoolean("isRoundFmt", false);
    }

    public static boolean setRoundBoundary(String roundinfo, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putString("roundinfo", roundinfo);
        return editor.commit();
    }

    public static String getRoundBoundary(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("roundinfo", null);
    }

    public static boolean setPriceWT(Boolean pricewt, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putBoolean("show_pricewt", pricewt);
        return editor.commit();
    }

    public static boolean isPriceWT(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getBoolean("show_pricewt", false);
    }

    public static boolean setCashierPermission(Boolean cashier_per, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putBoolean("cashier_per", cashier_per);
        return editor.commit();
    }

    public static Boolean isCashierPermission(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getBoolean("cashier_per", true);
    }

    public static boolean setOutletPin(String outlet_pin, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putString("outlet_pin", outlet_pin);
        return editor.commit();
    }

    public static String getOutletPin(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getString("outlet_pin", "1234");
    }

    public static boolean setTokenInfo(String tm,String token, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("order_time", tm);
        editor.commit();

        SharedPreferences.Editor editor1 = mSharedPreferences.edit();
        editor1.putString("last_order_no", token);
        editor1.commit();

        return true;
    }

    public static JSONObject getTokenInfo(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        String tm= mSharedPreferences.getString("order_time", null);
        String t= mSharedPreferences.getString("last_order_no", null);

        try {
            JSONObject j=new JSONObject();
            j.put("order_no",t);
            j.put("order_time",tm);
            return j;
        }catch (JSONException e){}
        return null;
    }

    public static boolean setPointsAmt(String amtpoints, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("amount_point", 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("amount_point", amtpoints);
        return editor.commit();
    }

    public static String getPointsAmt(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("amount_point", 0);
        return mSharedPreferences.getString("amount_point", "0");
    }

    public static boolean setDeviceCode(String devicecode, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("device_code", 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("device_code", devicecode);
        return editor.commit();
    }

    public static String getDeviceCode(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("device_code", 0);
        return mSharedPreferences.getString("device_code", null);
    }

    public static boolean qtyDialog(Boolean dqty, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("qty_dialog", 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean("qty_dialog", dqty);
        return editor.commit();
    }

    public static Boolean isQtyDialog(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("qty_dialog", 0);
        return mSharedPreferences.getBoolean("qty_dialog", false);
    }

    public static boolean setItemDiscount(Boolean item_discount, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("item_discount", 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean("item_discount", item_discount);
        return editor.commit();
    }

    public static boolean isItemDiscount(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("item_discount", 0);
        return mSharedPreferences.getBoolean("item_discount", false);
    }

    public static boolean setTopic(String topic, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("topic", 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("topic", topic);
        return editor.commit();
    }

    public static String getTopic(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("topic", 0);
        return mSharedPreferences.getString("topic", null);
    }

    public static boolean setSitting(Boolean showSitting, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean("sitting", showSitting);
        return editor.commit();
    }

    public static boolean isSitting(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getBoolean("sitting", true);
    }

    public static boolean setServiceCharge(Boolean service, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean("service_charge", service);
        return editor.commit();
    }

    public static Boolean isServiceCharge(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getBoolean("service_charge", false);
    }

    public static boolean setadvanceprint(String key,boolean val, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(key, val);
        return editor.commit();
    }

    public static boolean isadvanceprint(String key,Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(pref_name, 0);
        return mSharedPreferences.getBoolean(key, false);
    }

    public static boolean saveVal(String key,String val, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(key, 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(key, val);
        return editor.commit();
    }

    public static String retriveVal(String key,Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(key, 0);
        String defVal=null;
        if(key.equals(key_custom_service_charge) && !mSharedPreferences.contains(key_custom_service_charge))
            defVal=mContext.getString(R.string.service_charge);
        else if(key.equals(key_bill_width))
            defVal=PrintFormat.normalsize+"";
        return mSharedPreferences.getString(key, defVal);
    }

    public static boolean setCustomAllow(String ca_txt,Boolean ca_val, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("custom_allow", 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(ca_txt, ca_val);
        return editor.commit();
    }

    public static boolean isCustomAllow(String ca_txt,Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("custom_allow", 0);
        if(mSharedPreferences.contains(ca_txt))
            return mSharedPreferences.getBoolean(ca_txt, false);
//        else if(ca_txt.equals(M.key_show_sitting))
//            return true;
        else
            return false;
    }

    public static boolean setMenuUI(String type, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("menu_ui", 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("menu_ui", type);
        return editor.commit();
    }

    public static String getMenuUI(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("menu_ui", 0);
        return mSharedPreferences.getString("menu_ui", MenuUI.ui_hr);
    }
}
