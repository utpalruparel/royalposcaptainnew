package com.royalpos.waiter.model;

/**
 * Created by SWIFT-3 on 26/04/17.
 */

public class SuccessPojo {

    int success;
    String message;
    String daily_balance_id,brand_user_id,op_time;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public String getDaily_balance_id() {
        return daily_balance_id;
    }

    public void setDaily_balance_id(String daily_balance_id) {
        this.daily_balance_id = daily_balance_id;
    }

    public String getBrand_user_id() {
        return brand_user_id;
    }

    public void setBrand_user_id(String brand_user_id) {
        this.brand_user_id = brand_user_id;
    }

    public String getOp_time() {
        return op_time;
    }
}


