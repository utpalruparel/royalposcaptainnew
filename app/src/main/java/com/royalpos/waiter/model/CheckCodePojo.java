package com.royalpos.waiter.model;

public class CheckCodePojo {

    String success,user_exist,message;

    public String getSuccess() {
        return success;
    }

    public String getUser_exist() {
        return user_exist;
    }

    public String getMessage() {
        return message;
    }
}
