package com.royalpos.waiter.model;

/**
 * Created by reeva on 20/7/18.
 */

public class TaxInvoicePojo {

    String id,name,per,type,per_total,amount_total;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPer() {
        return per;
    }

    public void setPer(String per) {
        this.per = per;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPer_total() {
        return per_total;
    }

    public void setPer_total(String per_total) {
        this.per_total = per_total;
    }

    public String getAmount_total() {
        return amount_total;
    }

    public void setAmount_total(String amount_total) {
        this.amount_total = amount_total;
    }
}
