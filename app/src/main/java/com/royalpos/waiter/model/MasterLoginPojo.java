package com.royalpos.waiter.model;

import java.util.List;

public class MasterLoginPojo {

    private String role;
    private String success;
    private String message;
    private String brand_user_id;
    private BrandPojo user_data;
    private Integer outlet_count;
    private Restaurant restaurant;
    private String kitchen_mngr_id,username,decimal_value;
    private List<Waiter> waiters = null;
    
    public String getRole() {
    return role;
    }
    
    public void setRole(String role) {
    this.role = role;
    }
    
    public String getSuccess() {
    return success;
    }
    
    public void setSuccess(String success) {
    this.success = success;
    }
    
    public String getMessage() {
    return message;
    }
    
    public void setMessage(String message) {
    this.message = message;
    }
    
    public String getBrand_user_id() {
    return brand_user_id;
    }
    
    public void setBrand_user_id(String brand_user_id) {
    this.brand_user_id = brand_user_id;
    }
    
    public BrandPojo getUser_data() {
    return user_data;
    }
    
    public void setUser_data(BrandPojo user_data) {
    this.user_data = user_data;
    }
    
    public Integer getOutlet_count() {
    return outlet_count;
    }
    
    public void setOutlet_count(Integer outlet_count) {
    this.outlet_count = outlet_count;
    }
    
    public Restaurant getRestaurant() {
    return restaurant;
    }
    
    public void setRestaurant(Restaurant restaurant) {
    this.restaurant = restaurant;
    }
    
    public String getKitchen_mngr_id() {
    return kitchen_mngr_id;
    }
    
    public void setKitchen_mngr_id(String kitchen_mngr_id) {
    this.kitchen_mngr_id = kitchen_mngr_id;
    }

    public String getUsername() {
        return username;
    }

    public String getDecimal_value() {
        return decimal_value;
    }

    public List<Waiter> getWaiters() {
    return waiters;
    }
    
    public void setWaiters(List<Waiter> waiters) {
    this.waiters = waiters;
    }

    public class BrandPojo {

        private String id;
        private String uniqueid;
        public String name;
        private String email;
        private String avatar_url;
        private String plan_id;
        private String no_of_outlets;
        private String phone_number;
        private String type_of_business;
        private String city;
        private String country;
        private String country_code;
        private String latitude;
        private String longitude;
        private String contact_person_name;
        private String contact_person_number;
        private String timezone_id;
        private String address;
        private String via;
        private String reseller_id;
        private String outlet_count;
        private String status;
        private String created_at;
        private String updated_at;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUniqueid() {
            return uniqueid;
        }

        public void setUniqueid(String uniqueid) {
            this.uniqueid = uniqueid;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getAvatar_url() {
            return avatar_url;
        }

        public void setAvatar_url(String avatar_url) {
            this.avatar_url = avatar_url;
        }

        public String getPlan_id() {
            return plan_id;
        }

        public void setPlan_id(String plan_id) {
            this.plan_id = plan_id;
        }

        public String getNo_of_outlets() {
            return no_of_outlets;
        }

        public void setNo_of_outlets(String no_of_outlets) {
            this.no_of_outlets = no_of_outlets;
        }

        public String getPhone_number() {
            return phone_number;
        }

        public void setPhone_number(String phone_number) {
            this.phone_number = phone_number;
        }

        public String getType_of_business() {
            return type_of_business;
        }

        public void setType_of_business(String type_of_business) {
            this.type_of_business = type_of_business;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getCountry_code() {
            return country_code;
        }

        public void setCountry_code(String country_code) {
            this.country_code = country_code;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getContact_person_name() {
            return contact_person_name;
        }

        public void setContact_person_name(String contact_person_name) {
            this.contact_person_name = contact_person_name;
        }

        public String getContact_person_number() {
            return contact_person_number;
        }

        public void setContact_person_number(String contact_person_number) {
            this.contact_person_number = contact_person_number;
        }

        public String getTimezone_id() {
            return timezone_id;
        }

        public void setTimezone_id(String timezone_id) {
            this.timezone_id = timezone_id;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getVia() {
            return via;
        }

        public void setVia(String via) {
            this.via = via;
        }

        public String getReseller_id() {
            return reseller_id;
        }

        public void setReseller_id(String reseller_id) {
            this.reseller_id = reseller_id;
        }

        public String getOutlet_count() {
            return outlet_count;
        }

        public void setOutlet_count(String outlet_count) {
            this.outlet_count = outlet_count;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

    }

}