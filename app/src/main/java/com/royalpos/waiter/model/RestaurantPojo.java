package com.royalpos.waiter.model;

/**
 * Created by reeva on 11/8/17.
 */

public class RestaurantPojo {
    String id,rest_unique_id,name,address,phno,logo,email,total_sales_amount,username;

    public String getId() {
        return id;
    }

    public String getRest_unique_id() {
        return rest_unique_id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getPhno() {
        return phno;
    }

    public String getLogo() {
        return logo;
    }

    public String getEmail() {
        return email;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setRest_unique_id(String rest_unique_id) {
        this.rest_unique_id = rest_unique_id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPhno(String phno) {
        this.phno = phno;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTotal_sales_amount() {
        return total_sales_amount;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
