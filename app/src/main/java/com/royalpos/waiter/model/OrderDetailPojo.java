package com.royalpos.waiter.model;

import java.util.List;

/**
 * Created by SWIFT-3 on 27/04/17.
 */

public class OrderDetailPojo {

    private String cusineid,dishid,dishname,quantity,priceperdish,price,orderdetailid,status;
    private String preflag,discount,comment;
    String weight,unit_id,unit_name,sold_by,display_name,pref;
    List<PreModel> pre;
    List<Tax> taxes_data;
    String offers,discount_amount;//ONLY FOR delivery order
    private String combo_flag;
    private List<ComboModel> combo_item = null;

    public OrderDetailPojo() {
    }

    public String getCusineid() {
        return cusineid;
    }

    public void setCusineid(String cusineid) {
        this.cusineid = cusineid;
    }

    public String getDishid() {
        return dishid;
    }

    public void setDishid(String dishid) {
        this.dishid = dishid;
    }

    public String getDishname() {
        return dishname;
    }

    public void setDishname(String dishname) {
        this.dishname = dishname;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getOrderdetailid() {
        return orderdetailid;
    }

    public void setOrderdetailid(String orderdetailid) {
        this.orderdetailid = orderdetailid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPreflag() {
        return preflag;
    }

    public void setPreflag(String preflag) {
        this.preflag = preflag;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getPriceperdish() {
        return priceperdish;
    }

    public void setPriceperdish(String priceperdish) {
        this.priceperdish = priceperdish;
    }

    public List<PreModel> getPre() {
        return pre;
    }

    public void setPre(List<PreModel> pre) {
        this.pre = pre;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getUnit_id() {
        return unit_id;
    }

    public void setUnit_id(String unit_id) {
        this.unit_id = unit_id;
    }

    public String getUnit_name() {
        return unit_name;
    }

    public void setUnit_name(String unit_name) {
        this.unit_name = unit_name;
    }

    public String getSold_by() {
        return sold_by;
    }

    public void setSold_by(String sold_by) {
        this.sold_by = sold_by;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public List<Tax> getTaxes_data() {
        return taxes_data;
    }

    public void setTaxes_data(List<Tax> taxes_data) {
        this.taxes_data = taxes_data;
    }

    public String getPref() {
        return pref;
    }

    public void setPref(String pref) {
        this.pref = pref;
    }

    public String getOffers() {
        return offers;
    }

    public void setOffers(String offers) {
        this.offers = offers;
    }

    public String getDiscount_amount() {
        return discount_amount;
    }

    public void setDiscount_amount(String discount_amount) {
        this.discount_amount = discount_amount;
    }

    public String getCombo_flag() {
        return combo_flag;
    }

    public void setCombo_flag(String combo_flag) {
        this.combo_flag = combo_flag;
    }

    public List<ComboModel> getCombo_item() {
        return combo_item;
    }

    public void setCombo_item(List<ComboModel> combo_item) {
        this.combo_item = combo_item;
    }
}
