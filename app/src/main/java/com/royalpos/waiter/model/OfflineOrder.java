package com.royalpos.waiter.model;

/**
 * Created by reeva on 26/2/18.
 */

public class OfflineOrder {

    String id,restaurantid, runiqueid,table_id,userid,username,order_no,token_number,send_status,order_status,starttime;
    String cash,cust_address,cust_email,cust_name,cust_phone,grand_total,order_comment,pay_mode,return_cash,total_amt,tax,endtime,order_discount;
    private String no_of_people,offerid,offernm,bill_amt_discount,issplit,splitarray;
    String roundingjson,service_charge,tip,ismerge,transjson,txn_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRestaurantid() {
        return restaurantid;
    }

    public void setRestaurantid(String restaurantid) {
        this.restaurantid = restaurantid;
    }

    public String getRuniqueid() {
        return runiqueid;
    }

    public void setRuniqueid(String runiqueid) {
        this.runiqueid = runiqueid;
    }

    public String getTable_id() {
        return table_id;
    }

    public void setTable_id(String table_id) {
        this.table_id = table_id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getOrder_no() {
        return order_no;
    }

    public void setOrder_no(String order_no) {
        this.order_no = order_no;
    }

    public String getToken_number() {
        return token_number;
    }

    public void setToken_number(String token_number) {
        this.token_number = token_number;
    }

    public String getSend_status() {
        return send_status;
    }

    public void setSend_status(String send_status) {
        this.send_status = send_status;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public String getCash() {
        return cash;
    }

    public void setCash(String cash) {
        this.cash = cash;
    }

    public String getCust_address() {
        return cust_address;
    }

    public void setCust_address(String cust_address) {
        this.cust_address = cust_address;
    }

    public String getCust_email() {
        return cust_email;
    }

    public void setCust_email(String cust_email) {
        this.cust_email = cust_email;
    }

    public String getCust_name() {
        return cust_name;
    }

    public void setCust_name(String cust_name) {
        this.cust_name = cust_name;
    }

    public String getCust_phone() {
        return cust_phone;
    }

    public void setCust_phone(String cust_phone) {
        this.cust_phone = cust_phone;
    }

    public String getGrand_total() {
        return grand_total;
    }

    public void setGrand_total(String grand_total) {
        this.grand_total = grand_total;
    }

    public String getOrder_comment() {
        return order_comment;
    }

    public void setOrder_comment(String order_comment) {
        this.order_comment = order_comment;
    }

    public String getPay_mode() {
        return pay_mode;
    }

    public void setPay_mode(String pay_mode) {
        this.pay_mode = pay_mode;
    }

    public String getReturn_cash() {
        return return_cash;
    }

    public void setReturn_cash(String return_cash) {
        this.return_cash = return_cash;
    }

    public String getTotal_amt() {
        return total_amt;
    }

    public void setTotal_amt(String total_amt) {
        this.total_amt = total_amt;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public String getOrder_discount() {
        return order_discount;
    }

    public void setOrder_discount(String order_discount) {
        this.order_discount = order_discount;
    }

    public String getNo_of_people() {
        return no_of_people;
    }

    public void setNo_of_people(String no_of_people) {
        this.no_of_people = no_of_people;
    }

    public String getOfferid() {
        return offerid;
    }

    public void setOfferid(String offerid) {
        this.offerid = offerid;
    }

    public String getOffernm() {
        return offernm;
    }

    public void setOffernm(String offernm) {
        this.offernm = offernm;
    }

    public String getBill_amt_discount() {
        return bill_amt_discount;
    }

    public void setBill_amt_discount(String bill_amt_discount) {
        this.bill_amt_discount = bill_amt_discount;
    }

    public String getIssplit() {
        return issplit;
    }

    public void setIssplit(String issplit) {
        this.issplit = issplit;
    }

    public String getSplitarray() {
        return splitarray;
    }

    public void setSplitarray(String splitarray) {
        this.splitarray = splitarray;
    }

    public String getRoundingjson() {
        return roundingjson;
    }

    public void setRoundingjson(String roundingjson) {
        this.roundingjson = roundingjson;
    }

    public String getService_charge() {
        return service_charge;
    }

    public void setService_charge(String service_charge) {
        this.service_charge = service_charge;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public String getIsmerge() {
        return ismerge;
    }

    public void setIsmerge(String ismerge) {
        this.ismerge = ismerge;
    }

    public String getTransjson() {
        return transjson;
    }

    public void setTransjson(String transjson) {
        this.transjson = transjson;
    }

    public String getTxn_id() {
        return txn_id;
    }

    public void setTxn_id(String txn_id) {
        this.txn_id = txn_id;
    }
}
