package com.royalpos.waiter.model;

import java.util.HashMap;
import java.util.Map;

public class Inventory_item {

    private String stock_item_id;
    private String qty;
    private Boolean isselect=true;
    private String alternate_stock_item_id;
    private String stock_item_name;
    private String alternate_stock_item_name;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getStock_item_id() {
        return stock_item_id;
    }

    public void setStock_item_id(String stock_item_id) {
        this.stock_item_id = stock_item_id;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getAlternate_stock_item_id() {
        return alternate_stock_item_id;
    }

    public void setAlternate_stock_item_id(String alternate_stock_item_id) {
        this.alternate_stock_item_id = alternate_stock_item_id;
    }

    public String getStock_item_name() {
        return stock_item_name;
    }

    public void setStock_item_name(String stock_item_name) {
        this.stock_item_name = stock_item_name;
    }

    public String getAlternate_stock_item_name() {
        return alternate_stock_item_name;
    }

    public void setAlternate_stock_item_name(String alternate_stock_item_name) {
        this.alternate_stock_item_name = alternate_stock_item_name;
    }

    public Boolean getIsselect() {
        return isselect;
    }

    public void setIsselect(Boolean isselect) {
        this.isselect = isselect;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}