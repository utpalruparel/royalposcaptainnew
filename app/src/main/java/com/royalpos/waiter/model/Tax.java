package com.royalpos.waiter.model;

public class Tax {

        private String tax_name;
        private String tax_per;
        private String tax_value,tax_id;

        public String getTax_name() {
            return tax_name;
        }

        public void setTax_name(String tax_name) {
            this.tax_name = tax_name;
        }

        public String getTax_per() {
            return tax_per;
        }

        public void setTax_per(String tax_per) {
            this.tax_per = tax_per;
        }

        public String getTax_value() {
            return tax_value;
        }

        public void setTax_value(String tax_value) {
            this.tax_value = tax_value;
        }

    public String getTax_id() {
        return tax_id;
    }

    public void setTax_id(String tax_id) {
        this.tax_id = tax_id;
    }
}