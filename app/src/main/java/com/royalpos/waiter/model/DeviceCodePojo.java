package com.royalpos.waiter.model;

public class DeviceCodePojo {

    private Integer success;
    private String allow_payment;
    private String pin;
    private String member_id;
    private String member_username;
    private String member_role_id;
    private String member_role_name;
    String kdk_show;
    private String admin_pin,allow_barcode_payment;
    private String daily_balance_addded;
    private String balance_id;
    private String opening_bal;
    private String op_time;
    private String last_order_no;
    private Restaurant restaurant;
    private String username;
    private String kitchen_mngr_id;
    String message;

    public Integer getSuccess() {
    return success;
    }

    public void setSuccess(Integer success) {
    this.success = success;
    }

    public String getAllow_payment() {
    return allow_payment;
    }

    public void setAllow_payment(String allow_payment) {
    this.allow_payment = allow_payment;
    }

    public String getPin() {
    return pin;
    }

    public void setPin(String pin) {
    this.pin = pin;
    }

    public String getAdmin_pin() {
    return admin_pin;
    }

    public void setAdmin_pin(String admin_pin) {
    this.admin_pin = admin_pin;
    }

    public String getDaily_balance_addded() {
    return daily_balance_addded;
    }

    public void setDaily_balance_addded(String daily_balance_addded) {
    this.daily_balance_addded = daily_balance_addded;
    }

    public String getBalance_id() {
    return balance_id;
    }

    public void setBalance_id(String balance_id) {
    this.balance_id = balance_id;
    }

    public String getOpening_bal() {
    return opening_bal;
    }

    public void setOpening_bal(String opening_bal) {
    this.opening_bal = opening_bal;
    }

    public String getOp_time() {
    return op_time;
    }

    public void setOp_time(String op_time) {
    this.op_time = op_time;
    }

    public String getLast_order_no() {
    return last_order_no;
    }

    public void setLast_order_no(String last_order_no) {
    this.last_order_no = last_order_no;
    }

    public Restaurant getRestaurant() {
    return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
    this.restaurant = restaurant;
    }

    public String getUsername() {
    return username;
    }

    public void setUsername(String username) {
    this.username = username;
    }

    public String getKitchen_mngr_id() {
    return kitchen_mngr_id;
    }

    public void setKitchen_mngr_id(String kitchen_mngr_id) {
    this.kitchen_mngr_id = kitchen_mngr_id;
    }

    public String getMember_id() {
        return member_id;
    }

    public String getMember_username() {
        return member_username;
    }

    public String getMember_role_id() {
        return member_role_id;
    }

    public String getMember_role_name() {
        return member_role_name;
    }

    public String getKdk_show() {
        return kdk_show;
    }

    public String getMessage() {
        return message;
    }

    public String getAllow_barcode_payment() {
        return allow_barcode_payment;
    }

}