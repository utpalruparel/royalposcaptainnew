package com.royalpos.waiter.model;

public class VarPojo {
    String id,name,amount,amount_wt,quantity;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAmount_wt() {
        return amount_wt;
    }

    public void setAmount_wt(String amount_wt) {
        this.amount_wt = amount_wt;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
}
