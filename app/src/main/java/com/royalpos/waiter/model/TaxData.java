package com.royalpos.waiter.model;

public class TaxData {

    private String id;
    private String text;
    private String value;
    private String tax_amount;
    String tax_value;//for calculation

    public String getId() {
    return id;
    }

    public void setId(String id) {
    this.id = id;
    }

    public String getText() {
    return text;
    }

    public void setText(String text) {
    this.text = text;
    }

    public String getValue() {
    return value;
    }

    public void setValue(String value) {
    this.value = value;
    }

    public String getTax_amount() {
    return tax_amount;
    }

    public void setTax_amount(String tax_amount) {
    this.tax_amount = tax_amount;
    }

    public String getTax_value() {
        return tax_value;
    }

    public void setTax_value(String tax_value) {
        this.tax_value = tax_value;
    }
}