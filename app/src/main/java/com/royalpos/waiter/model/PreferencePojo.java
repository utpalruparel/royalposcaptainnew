package com.royalpos.waiter.model;

/**
 * Created by reeva on 14/8/17.
 */

public class PreferencePojo {

    String text,value,tax_amount,id;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getValue() {


        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getTax_amount() {
        return tax_amount;
    }

    public void setTax_amount(String tax_amount) {
        this.tax_amount = tax_amount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
