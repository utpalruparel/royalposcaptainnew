package com.royalpos.waiter.model;

public class Topic_data {

    private String category_name;
    private String quote_text;
    private String image_name;

    public String getCategory_name() {
        return category_name;
    }

    public String getQuote_text() {
        return quote_text;
    }

    public String getImage_name() {
        return image_name;
    }
}