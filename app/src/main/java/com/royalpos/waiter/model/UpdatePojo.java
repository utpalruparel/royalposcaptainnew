package com.royalpos.waiter.model;

/**
 * Created by reeva on 6/10/17.
 */

public class UpdatePojo {

    String cuisine,dish,tax,payment_mode;

    public String getCuisine() {
        return cuisine;
    }

    public String getDish() {
        return dish;
    }

    public String getTax() {
        return tax;
    }

    public String getPayment_mode() {
        return payment_mode;
    }
}
