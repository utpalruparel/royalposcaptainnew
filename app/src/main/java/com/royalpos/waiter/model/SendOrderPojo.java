package com.royalpos.waiter.model;

import java.util.List;

public class SendOrderPojo {

    private List<Order> order = null;
    private String user_id;
    private String restaurant_id;
    private String rest_unique_id;
    private String pay_mode;
    private String order_time;
    private String order_type;
    private String cash;
    private String return_cash;
    private String total_amt;
    private String grand_total;
    private String order_no;
    private String order_comment;
    private String cust_name;
    private String cust_phone;
    private String cust_email;
    private String cust_address;
    private String cgst_amount;
    private String sgst_amount;
    private String token,discount,is_split,total_split;
    String order_status=null;
    String orderid=null,packaging_charge;//only for update delivery order
    private List<Tax> taxes = null;
    private List<SplitPojo> split_data = null;
    String part_payment_amt,part_payment_flag;
    String offer_id,bill_amt_discount,rounding_json;

    public List<Order> getOrder() {
    return order;
    }

    public void setOrder(List<Order> order) {
    this.order = order;
    }

    public String getUser_id() {
    return user_id;
    }

    public void setUser_id(String user_id) {
    this.user_id = user_id;
    }

    public String getRestaurant_id() {
    return restaurant_id;
    }

    public void setRestaurant_id(String restaurant_id) {
    this.restaurant_id = restaurant_id;
    }

    public String getRest_unique_id() {
    return rest_unique_id;
    }

    public void setRest_unique_id(String rest_unique_id) {
    this.rest_unique_id = rest_unique_id;
    }

    public String getPay_mode() {
    return pay_mode;
    }

    public void setPay_mode(String pay_mode) {
    this.pay_mode = pay_mode;
    }

    public String getOrder_time() {
    return order_time;
    }

    public void setOrder_time(String order_time) {
    this.order_time = order_time;
    }

    public String getOrder_type() {
    return order_type;
    }

    public void setOrder_type(String order_type) {
    this.order_type = order_type;
    }

    public String getCash() {
    return cash;
    }

    public void setCash(String cash) {
    this.cash = cash;
    }

    public String getReturn_cash() {
    return return_cash;
    }

    public void setReturn_cash(String return_cash) {
    this.return_cash = return_cash;
    }

    public String getTotal_amt() {
    return total_amt;
    }

    public void setTotal_amt(String total_amt) {
    this.total_amt = total_amt;
    }

    public String getGrand_total() {
    return grand_total;
    }

    public void setGrand_total(String grand_total) {
    this.grand_total = grand_total;
    }

    public String getOrder_no() {
    return order_no;
    }

    public void setOrder_no(String order_no) {
    this.order_no = order_no;
    }

    public String getOrder_comment() {
    return order_comment;
    }

    public void setOrder_comment(String order_comment) {
    this.order_comment = order_comment;
    }

    public String getCust_name() {
    return cust_name;
    }

    public void setCust_name(String cust_name) {
    this.cust_name = cust_name;
    }

    public String getCust_phone() {
    return cust_phone;
    }

    public void setCust_phone(String cust_phone) {
    this.cust_phone = cust_phone;
    }

    public String getCust_email() {
    return cust_email;
    }

    public void setCust_email(String cust_email) {
    this.cust_email = cust_email;
    }

    public String getCust_address() {
    return cust_address;
    }

    public void setCust_address(String cust_address) {
    this.cust_address = cust_address;
    }

    public String getCgst_amount() {
    return cgst_amount;
    }

    public void setCgst_amount(String cgst_amount) {
    this.cgst_amount = cgst_amount;
    }

    public String getSgst_amount() {
    return sgst_amount;
    }

    public void setSgst_amount(String sgst_amount) {
    this.sgst_amount = sgst_amount;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getToken() {
return token;
}

    public void setToken(String token) {
    this.token = token;
    }

    public List<Tax> getTaxes() {
    return taxes;
    }

    public void setTaxes(List<Tax> taxes) {
    this.taxes = taxes;
    }

    public String getIs_split() {
        return is_split;
    }

    public void setIs_split(String is_split) {
        this.is_split = is_split;
    }

    public String getTotal_split() {
        return total_split;
    }

    public void setTotal_split(String total_split) {
        this.total_split = total_split;
    }

    public List<SplitPojo> getSplit_data() {
        return split_data;
    }

    public void setSplit_data(List<SplitPojo> split_data) {
        this.split_data = split_data;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getPackaging_charge() {
        return packaging_charge;
    }

    public void setPackaging_charge(String packaging_charge) {
        this.packaging_charge = packaging_charge;
    }

    public String getPart_payment_amt() {
        return part_payment_amt;
    }

    public void setPart_payment_amt(String part_payment_amt) {
        this.part_payment_amt = part_payment_amt;
    }

    public String getPart_payment_flag() {
        return part_payment_flag;
    }

    public void setPart_payment_flag(String part_payment_flag) {
        this.part_payment_flag = part_payment_flag;
    }

    public String getOffer_id() {
        return offer_id;
    }

    public void setOffer_id(String offer_id) {
        this.offer_id = offer_id;
    }

    public String getBill_amt_discount() {
        return bill_amt_discount;
    }

    public void setBill_amt_discount(String bill_amt_discount) {
        this.bill_amt_discount = bill_amt_discount;
    }

    public String getRounding_json() {
        return rounding_json;
    }

    public void setRounding_json(String rounding_json) {
        this.rounding_json = rounding_json;
    }
}



