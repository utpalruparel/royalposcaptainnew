package com.royalpos.waiter.model;

import java.util.List;

/**
 * Created by reeva on 12/2/18.
 */

public class MembershipPojo {

    List<PreferencePojo> tax_data=null;
    String membership_expires,status,membership_end_date,version_number,decimal_value;
    String current_date,current_time;

    public List<PreferencePojo> getTax_data() {
        return tax_data;
    }

    public void setTax_data(List<PreferencePojo> tax_data) {
        this.tax_data = tax_data;
    }

    public String getMembership_expires() {
        return membership_expires;
    }

    public void setMembership_expires(String membership_expires) {
        this.membership_expires = membership_expires;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMembership_end_date() {
        return membership_end_date;
    }

    public void setMembership_end_date(String membership_end_date) {
        this.membership_end_date = membership_end_date;
    }

    public String getVersion_number() {
        return version_number;
    }

    public void setVersion_number(String version_number) {
        this.version_number = version_number;
    }

    public String getDecimal_value() {
        return decimal_value;
    }

    public String getCurrent_date() {
        return current_date;
    }

    public String getCurrent_time() {
        return current_time;
    }
}
