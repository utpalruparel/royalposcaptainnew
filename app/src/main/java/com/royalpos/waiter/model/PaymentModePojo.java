package com.royalpos.waiter.model;

/**
 * Created by reeva on 18/8/17.
 */

public class PaymentModePojo {

    String id,type,is_rounding_on;

    public PaymentModePojo() {
    }

    public PaymentModePojo(String id, String type) {
        this.id = id;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIs_rounding_on() {
        return is_rounding_on;
    }

    public void setIs_rounding_on(String is_rounding_on) {
        this.is_rounding_on = is_rounding_on;
    }
}
