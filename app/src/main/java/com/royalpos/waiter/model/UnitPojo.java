package com.royalpos.waiter.model;

/**
 * Created by reeva on 16/7/18.
 */

public class UnitPojo {

    String purchased_unit_id,purchased_unit_name,used_unit_id,used_unit_name,used_unit,purchased_unit_display_name,used_unit_display_name;

    public String getPurchased_unit_id() {
        return purchased_unit_id;
    }

    public void setPurchased_unit_id(String purchased_unit_id) {
        this.purchased_unit_id = purchased_unit_id;
    }

    public String getPurchased_unit_name() {
        return purchased_unit_name;
    }

    public void setPurchased_unit_name(String purchased_unit_name) {
        this.purchased_unit_name = purchased_unit_name;
    }

    public String getUsed_unit_id() {
        return used_unit_id;
    }

    public void setUsed_unit_id(String used_unit_id) {
        this.used_unit_id = used_unit_id;
    }

    public String getUsed_unit_name() {
        return used_unit_name;
    }

    public void setUsed_unit_name(String used_unit_name) {
        this.used_unit_name = used_unit_name;
    }

    public String getUsed_unit() {
        return used_unit;
    }

    public void setUsed_unit(String used_unit) {
        this.used_unit = used_unit;
    }

    public String getPurchased_unit_display_name() {
        return purchased_unit_display_name;
    }

    public void setPurchased_unit_display_name(String purchased_unit_display_name) {
        this.purchased_unit_display_name = purchased_unit_display_name;
    }

    public String getUsed_unit_display_name() {
        return used_unit_display_name;
    }

    public void setUsed_unit_display_name(String used_unit_display_name) {
        this.used_unit_display_name = used_unit_display_name;
    }
}
