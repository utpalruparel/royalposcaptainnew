package com.royalpos.waiter.model;

import java.util.List;

/**
 * Created by reeva on 22/1/18.
 */

public class CustomerPojo {

    private String id;
    private String name;
    private String email;
    private String address;
    private String phone_no;
    String no_person,status;//only for sqlite table
    private String restaurant_id;
    private String updated_at;//only for server api
    private String created_at;
    String points;
    private List<AddressPojo> address_json = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public String getRestaurant_id() {
        return restaurant_id;
    }

    public void setRestaurant_id(String restaurant_id) {
        this.restaurant_id = restaurant_id;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getNo_person() {
        return no_person;
    }

    public void setNo_person(String no_person) {
        this.no_person = no_person;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public List<AddressPojo> getAddress_json() {
        return address_json;
    }

    public void setAddress_json(List<AddressPojo> address_json) {
        this.address_json = address_json;
    }
}
