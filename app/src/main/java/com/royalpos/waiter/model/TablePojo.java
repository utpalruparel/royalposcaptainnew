package com.royalpos.waiter.model;

/**
 * Created by reeva on 26/1/18.
 */

public class TablePojo {

        String id, name, status;

    public TablePojo() {
    }

    public TablePojo(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
}
