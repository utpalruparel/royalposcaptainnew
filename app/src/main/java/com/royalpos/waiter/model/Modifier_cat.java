package com.royalpos.waiter.model;

import java.util.HashMap;
import java.util.Map;

public class Modifier_cat {

    private String cat_id;
    private String cat_name;
    private Integer modifier_selection,min_selection=0,tot_sel=0;

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }

    public Integer getModifier_selection() {
        return modifier_selection;
    }

    public void setModifier_selection(Integer modifier_selection) {
        this.modifier_selection = modifier_selection;
    }

    public Integer getMin_selection() {
        return min_selection;
    }

    public void setMin_selection(Integer min_selection) {
        this.min_selection = min_selection;
    }

    public Integer getTot_sel() {
        return tot_sel;
    }

    public void setTot_sel(Integer tot_sel) {
        this.tot_sel = tot_sel;
    }
}