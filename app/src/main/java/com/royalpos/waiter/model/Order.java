package com.royalpos.waiter.model;

import java.util.List;

public class Order {

        private String dishid;
        private String quantity;
        private String price;
        private String preferencesid;
        private String dish_comment;
    String weight,unit_id;
    private List<Tax> taxes_data = null;
    String order_detail_id;//only for deliver order
    String offer_id,discount;

        public String getDishid() {
            return dishid;
        }

        public void setDishid(String dishid) {
            this.dishid = dishid;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getPreferencesid() {
            return preferencesid;
        }

        public void setPreferencesid(String preferencesid) {
            this.preferencesid = preferencesid;
        }

        public String getDish_comment() {
            return dish_comment;
        }

        public void setDish_comment(String dish_comment) {
            this.dish_comment = dish_comment;
        }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getUnit_id() {
        return unit_id;
    }

    public void setUnit_id(String unit_id) {
        this.unit_id = unit_id;
    }

    public List<Tax> getTaxes_data() {
        return taxes_data;
    }

    public void setTaxes_data(List<Tax> taxes_data) {
        this.taxes_data = taxes_data;
    }

    public String getOrder_detail_id() {
        return order_detail_id;
    }

    public void setOrder_detail_id(String order_detail_id) {
        this.order_detail_id = order_detail_id;
    }

    public String getOffer_id() {
        return offer_id;
    }

    public void setOffer_id(String offer_id) {
        this.offer_id = offer_id;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }
}