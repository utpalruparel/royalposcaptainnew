package com.royalpos.waiter.model;

import java.util.List;

/**
 * Created by reeva on 11/8/17.
 */

public class WaiterLoginPojo {

    int success;
    String daily_balance_addded,last_order_no,balance_id,opening_bal,op_time;
    List<PreferencePojo> preference_data;
    String pin;
    private Topic_data topic_data;

    public int getSuccess() {
        return success;
    }

    public String getDaily_balance_addded() {
        return daily_balance_addded;
    }

    public String getLast_order_no() {
        return last_order_no;
    }

    public void setLast_order_no(String last_order_no) {
        this.last_order_no = last_order_no;
    }

    public String getBalance_id() {
        return balance_id;
    }

    public void setBalance_id(String balance_id) {
        this.balance_id = balance_id;
    }

    public String getOpening_bal() {
        return opening_bal;
    }

    public String getOp_time() {
        return op_time;
    }

    public List<PreferencePojo> getPreference_data() {
        return preference_data;
    }

    public void setPreference_data(List<PreferencePojo> preference_data) {
        this.preference_data = preference_data;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public Topic_data getTopic_data() {
        return topic_data;
    }
}
