package com.royalpos.waiter.model;

/**
 * Created by Reeva on 10/10/2015.
 */
public class CuisineListPojo {

    private String cuisine_id,cuisine_name,cuisine_image,user_id,username,role;
    String item_pricing,tax_id;

    public CuisineListPojo() {
    }

    public CuisineListPojo(String cuisine_id, String cuisine_name, String cuisine_image) {
        this.cuisine_id = cuisine_id;
        this.cuisine_name = cuisine_name;
        this.cuisine_image = cuisine_image;
    }

    public String getCuisine_id() {
        return cuisine_id;
    }

    public void setCuisine_id(String cuisine_id) {
        this.cuisine_id = cuisine_id;
    }

    public String getCuisine_name() {
        return cuisine_name;
    }

    public void setCuisine_name(String cuisine_name) {
        this.cuisine_name = cuisine_name;
    }

    public String getCuisine_image() {
        return cuisine_image;
    }

    public void setCuisine_image(String cuisine_image) {
        this.cuisine_image = cuisine_image;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getItem_pricing() {
        return item_pricing;
    }

    public void setItem_pricing(String item_pricing) {
        this.item_pricing = item_pricing;
    }

    public String getTax_id() {
        return tax_id;
    }

    public void setTax_id(String tax_id) {
        this.tax_id = tax_id;
    }
}
