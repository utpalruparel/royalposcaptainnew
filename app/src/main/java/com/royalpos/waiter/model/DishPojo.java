package com.royalpos.waiter.model;

import java.util.List;

/**
 * Created by Reeva on 10/10/2015.
 */
public class DishPojo {

    private String dishid;
    private String cusineid;
    private String dishname;
    private String description;
    private String price,costing_price;
    private String dishimage,dishimage_url;
    private String preflag;
    String composite_item_track_stock,in_stock,low_stock;
    String barcode_no, batch_no, prodct_no;
    private String sold_by,purchased_unit_id,purchased_unit_name,used_unit_id,used_unit_name,default_sale_value;
    private String price_without_tax,tax_amt;
    private List<TaxData> tax_data = null;
    List<PreModel> pre;
    List<Modifier_cat> modifier_cat=null;
    private List<Inventory_item> inventory_item = null;
    String combo_flag;
    List<ComboModel> combo_item;
    String offers,discount_amount,hsn_no,allow_open_price;

    public List<ComboModel> getCombo_item() {
        return combo_item;
    }

    public void setCombo_item(List<ComboModel> combo_item) {
        this.combo_item = combo_item;
    }

    public String getCombo_flag() {
        return combo_flag;
    }

    public void setCombo_flag(String combo_flag) {
        this.combo_flag = combo_flag;
    }

    public String getPreflag() {
        return preflag;
    }

    public void setPreflag(String preflag) {
        this.preflag = preflag;
    }

    public List<PreModel> getPre() {
        return pre;
    }

    public void setPre(List<PreModel> pre) {
        this.pre = pre;
    }

    public String getDishid() {
        return dishid;
    }

    public void setDishid(String dishid) {
        this.dishid = dishid;
    }

    public String getCusineid() {
        return cusineid;
    }

    public void setCusineid(String cusineid) {
        this.cusineid = cusineid;
    }

    public String getDishname() {
        return dishname;
    }

    public void setDishname(String dishname) {
        this.dishname = dishname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDishimage() {
        return dishimage;
    }

    public void setDishimage(String dishimage) {
        this.dishimage = dishimage;
    }

    public String getComposite_item_track_stock() {
        return composite_item_track_stock;
    }

    public void setComposite_item_track_stock(String composite_item_track_stock) {
        this.composite_item_track_stock = composite_item_track_stock;
    }

    public String getIn_stock() {
        return in_stock;
    }

    public void setIn_stock(String in_stock) {
        this.in_stock = in_stock;
    }

    public String getLow_stock() {
        return low_stock;
    }

    public void setLow_stock(String low_stock) {
        this.low_stock = low_stock;
    }

    public List<Modifier_cat> getModifier_cat() {
        return modifier_cat;
    }

    public void setModifier_cat(List<Modifier_cat> modifier_cat) {
        this.modifier_cat = modifier_cat;
    }

    public List<Inventory_item> getInventory_item() {
        return inventory_item;
    }

    public void setInventory_item(List<Inventory_item> inventory_item) {
        this.inventory_item = inventory_item;
    }

    public String getBarcode_no() {
        return barcode_no;
    }

    public void setBarcode_no(String barcode_no) {
        this.barcode_no = barcode_no;
    }

    public String getProdct_no() {
        return prodct_no;
    }

    public void setProdct_no(String prodct_no) {
        this.prodct_no = prodct_no;
    }

    public String getBatch_no() {
        return batch_no;
    }

    public void setBatch_no(String batch_no) {
        this.batch_no = batch_no;
    }

    public String getSold_by() {
        return sold_by;
    }

    public void setSold_by(String sold_by) {
        this.sold_by = sold_by;
    }

    public String getPurchased_unit_id() {
        return purchased_unit_id;
    }

    public void setPurchased_unit_id(String purchased_unit_id) {
        this.purchased_unit_id = purchased_unit_id;
    }

    public String getPurchased_unit_name() {
        return purchased_unit_name;
    }

    public void setPurchased_unit_name(String purchased_unit_name) {
        this.purchased_unit_name = purchased_unit_name;
    }

    public String getUsed_unit_id() {
        return used_unit_id;
    }

    public void setUsed_unit_id(String used_unit_id) {
        this.used_unit_id = used_unit_id;
    }

    public String getUsed_unit_name() {
        return used_unit_name;
    }

    public void setUsed_unit_name(String used_unit_name) {
        this.used_unit_name = used_unit_name;
    }

    public String getDefault_sale_value() {
        return default_sale_value;
    }

    public void setDefault_sale_value(String default_sale_value) {
        this.default_sale_value = default_sale_value;
    }

    public String getPrice_without_tax() {
        return price_without_tax;
    }

    public void setPrice_without_tax(String price_without_tax) {
        this.price_without_tax = price_without_tax;
    }

    public String getTax_amt() {
        return tax_amt;
    }

    public void setTax_amt(String tax_amt) {
        this.tax_amt = tax_amt;
    }

    public List<TaxData> getTax_data() {
        return tax_data;
    }

    public void setTax_data(List<TaxData> tax_data) {
        this.tax_data = tax_data;
    }

    public String getCosting_price() {
        return costing_price;
    }

    public void setCosting_price(String costing_price) {
        this.costing_price = costing_price;
    }

    public String getOffers() {
        return offers;
    }

    public void setOffers(String offers) {
        this.offers = offers;
    }

    public String getDiscount_amount() {
        return discount_amount;
    }

    public void setDiscount_amount(String discount_amount) {
        this.discount_amount = discount_amount;
    }

    public String getHsn_no() {
        return hsn_no;
    }

    public void setHsn_no(String hsn_no) {
        this.hsn_no = hsn_no;
    }

    public String getAllow_open_price() {
        return allow_open_price;
    }

    public void setAllow_open_price(String allow_open_price) {
        this.allow_open_price = allow_open_price;
    }

    public String getDishimage_url() {
        return dishimage_url;
    }

    public void setDishimage_url(String dishimage_url) {
        this.dishimage_url = dishimage_url;
    }
}
