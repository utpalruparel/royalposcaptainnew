package com.royalpos.waiter.model;

/**
 * Created by reeva on 22/12/15.
 */
public class PreModel {

    String preid, pretype, prenm, preprice;

    public String getPreid() {
        return preid;
    }

    public void setPreid(String preid) {
        this.preid = preid;
    }

    public String getPretype() {
        return pretype;
    }

    public void setPretype(String pretype) {
        this.pretype = pretype;
    }

    public String getPrenm() {
        return prenm;
    }

    public void setPrenm(String prenm) {
        this.prenm = prenm;
    }

    public String getPreprice() {
        return preprice;
    }

    public void setPreprice(String preprice) {
        this.preprice = preprice;
    }
}
