package com.royalpos.waiter.model;

public class DiscountPojo {

    private String id;
    private String name;
    private String discount_type;
    private String discount_percentage;
    private String discount_amt;
    private String bill_amt;
    private String item_id;

    public String getId() {
    return id;
    }

    public void setId(String id) {
    this.id = id;
    }

    public String getName() {
    return name;
    }

    public void setName(String name) {
    this.name = name;
    }

    public String getDiscount_type() {
    return discount_type;
    }

    public void setDiscount_type(String discount_type) {
    this.discount_type = discount_type;
    }

    public String getDiscount_percentage() {
    return discount_percentage;
    }

    public void setDiscount_percentage(String discount_percentage) {
    this.discount_percentage = discount_percentage;
    }

    public String getDiscount_amt() {
    return discount_amt;
    }

    public void setDiscount_amt(String discount_amt) {
    this.discount_amt = discount_amt;
    }

    public String getBill_amt() {
    return bill_amt;
    }

    public void setBill_amt(String bill_amt) {
    this.bill_amt = bill_amt;
    }

    public String getItem_id() {
    return item_id;
    }

    public void setItem_id(String item_id) {
    this.item_id = item_id;
    }

}
