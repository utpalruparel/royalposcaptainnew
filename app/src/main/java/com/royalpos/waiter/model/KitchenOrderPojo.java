package com.royalpos.waiter.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class KitchenOrderPojo {

    private List<OrderDetailPojo> order_details = null;
    private Integer order_id;
    private String order_no;
    private String token;
    private String order_time;
    private String status;
    private String username;
    private String name;
    private String status_text,table_id, table_name,userid;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public List<OrderDetailPojo> getOrder_details() {
    return order_details;
    }

    public void setOrder_details(List<OrderDetailPojo> order_details) {
        this.order_details = order_details;
    }

    public Integer getOrder_id() {
    return order_id;
    }

    public void setOrder_id(Integer order_id) {
    this.order_id = order_id;
    }

    public String getOrder_no() {
    return order_no;
    }

    public void setOrder_no(String order_no) {
    this.order_no = order_no;
    }

    public String getToken() {
    return token;
    }

    public void setToken(String token) {
    this.token = token;
    }

    public String getOrder_time() {
    return order_time;
    }

    public void setOrder_time(String order_time) {
    this.order_time = order_time;
    }

    public String getStatus() {
    return status;
    }

    public void setStatus(String status) {
    this.status = status;
    }

    public String getStatus_text() {
    return status_text;
    }

    public void setStatus_text(String status_text) {
    this.status_text = status_text;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTable_id() {
        return table_id;
    }

    public void setTable_id(String table_id) {
        this.table_id = table_id;
    }

    public String getTable_name() {
        return table_name;
    }

    public void setTable_name(String table_name) {
        this.table_name = table_name;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public Map<String, Object> getAdditionalProperties() {
    return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
    this.additionalProperties.put(name, value);
    }

}

