package com.royalpos.waiter.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Reeva on 9/24/2015.
 */
public class DishOrderPojo extends ArrayList<CharSequence> {

    private String dishid,dishname,qty,priceperdish,price,orderdetailid,status, prefid,prenm,discount,comment,dis_per;
    private String cusineid;
    private String dishtpe,hsn_no;
    private String description;
    private String dishimage;
    private String preflag;
    private String combo_flag;
    private String dish_comment;
    private String sold_by,purchased_unit_id,purchased_unit_name,used_unit_id,used_unit_name,default_sale_weight;
    String unitid,unitname,weight,sort_nm;
    String price_without_tax,priceper_without_tax,tax_amt,tot_tax;
    private List<TaxData> tax_data = null;
    private List<Inventory_item> inventory_item = null;
    String invid;
    List<PreModel> pre;
    List<ComboModel> combo_list;
    private boolean isnew;
    String offer,tot_disc;
    List<VarPojo> varPojoList=null;
    String tm,allow_open_price;

    public String getCombo_flag() {
        return combo_flag;
    }

    public void setCombo_flag(String combo_flag) {
        this.combo_flag = combo_flag;
    }

    public List<ComboModel> getCombo_list() {
        return combo_list;
    }

    public void setCombo_list(List<ComboModel> combo_list) {
        this.combo_list = combo_list;
    }

    public String getDish_comment() {
        return dish_comment;
    }

    public void setDish_comment(String dish_comment) {
        this.dish_comment = dish_comment;
    }

    public String getDishid() {
        return dishid;
    }

    public void setDishid(String dishid) {
        this.dishid = dishid;
    }

    public String getOrderdetailid() {
        return orderdetailid;
    }

    public void setOrderdetailid(String orderdetailid) {
        this.orderdetailid = orderdetailid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDishname() {
        return dishname;
    }

    public void setDishname(String dishname) {
        this.dishname = dishname;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public boolean isnew() {
        return isnew;
    }

    public void setIsnew(boolean isnew) {
        this.isnew = isnew;
    }

    public String getPrefid() {
        return prefid;
    }

    public void setPrefid(String prefid) {
        this.prefid = prefid;
    }

    public String getPriceperdish() {
        return priceperdish;
    }

    public void setPriceperdish(String priceperdish) {
        this.priceperdish = priceperdish;
    }

    public String getPrenm() {
        return prenm;
    }

    public void setPrenm(String prenm) {
        this.prenm = prenm;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCusineid() {
        return cusineid;
    }

    public void setCusineid(String cusineid) {
        this.cusineid = cusineid;
    }

    public String getDishtpe() {
        return dishtpe;
    }

    public void setDishtpe(String dishtpe) {
        this.dishtpe = dishtpe;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDishimage() {
        return dishimage;
    }

    public void setDishimage(String dishimage) {
        this.dishimage = dishimage;
    }

    public String getPreflag() {
        return preflag;
    }

    public void setPreflag(String preflag) {
        this.preflag = preflag;
    }

    public List<PreModel> getPre() {
        return pre;
    }

    public void setPre(List<PreModel> pre) {
        this.pre = pre;
    }

    public List<Inventory_item> getInventory_item() {
        return inventory_item;
    }

    public void setInventory_item(List<Inventory_item> inventory_item) {
        this.inventory_item = inventory_item;
    }

    public String getInvid() {
        return invid;
    }

    public void setInvid(String invid) {
        this.invid = invid;
    }

    public String getSold_by() {
        return sold_by;
    }

    public void setSold_by(String sold_by) {
        this.sold_by = sold_by;
    }

    public String getPurchased_unit_id() {
        return purchased_unit_id;
    }

    public void setPurchased_unit_id(String purchased_unit_id) {
        this.purchased_unit_id = purchased_unit_id;
    }

    public String getPurchased_unit_name() {
        return purchased_unit_name;
    }

    public void setPurchased_unit_name(String purchased_unit_name) {
        this.purchased_unit_name = purchased_unit_name;
    }

    public String getUsed_unit_id() {
        return used_unit_id;
    }

    public void setUsed_unit_id(String used_unit_id) {
        this.used_unit_id = used_unit_id;
    }

    public String getUsed_unit_name() {
        return used_unit_name;
    }

    public void setUsed_unit_name(String used_unit_name) {
        this.used_unit_name = used_unit_name;
    }

    public String getUnitid() {
        return unitid;
    }

    public void setUnitid(String unitid) {
        this.unitid = unitid;
    }

    public String getUnitname() {
        return unitname;
    }

    public void setUnitname(String unitname) {
        this.unitname = unitname;
    }

    public boolean isIsnew() {
        return isnew;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getDefault_sale_weight() {
        return default_sale_weight;
    }

    public void setDefault_sale_weight(String default_sale_weight) {
        this.default_sale_weight = default_sale_weight;
    }

    public String getSort_nm() {
        return sort_nm;
    }

    public void setSort_nm(String sort_nm) {
        this.sort_nm = sort_nm;
    }

    public String getPrice_without_tax() {
        return price_without_tax;
    }

    public void setPrice_without_tax(String price_without_tax) {
        this.price_without_tax = price_without_tax;
    }

    public String getTax_amt() {
        return tax_amt;
    }

    public void setTax_amt(String tax_amt) {
        this.tax_amt = tax_amt;
    }

    public String getPriceper_without_tax() {
        return priceper_without_tax;
    }

    public void setPriceper_without_tax(String priceper_without_tax) {
        this.priceper_without_tax = priceper_without_tax;
    }

    public String getTot_tax() {
        return tot_tax;
    }

    public void setTot_tax(String tot_tax) {
        this.tot_tax = tot_tax;
    }

    public List<TaxData> getTax_data() {
        return tax_data;
    }

    public void setTax_data(List<TaxData> tax_data) {
        this.tax_data = tax_data;
    }

    public String getOffer() {
        return offer;
    }

    public void setOffer(String offer) {
        this.offer = offer;
    }

    public String getTot_disc() {
        return tot_disc;
    }

    public void setTot_disc(String tot_disc) {
        this.tot_disc = tot_disc;
    }

    public List<VarPojo> getVarPojoList() {
        return varPojoList;
    }

    public void setVarPojoList(List<VarPojo> varPojoList) {
        this.varPojoList = varPojoList;
    }

    public String getDis_per() {
        return dis_per;
    }

    public void setDis_per(String dis_per) {
        this.dis_per = dis_per;
    }

    public String getHsn_no() {
        return hsn_no;
    }

    public void setHsn_no(String hsn_no) {
        this.hsn_no = hsn_no;
    }

    public String getTm() {
        return tm;
    }

    public void setTm(String tm) {
        this.tm = tm;
    }

    public String getAllow_open_price() {
        return allow_open_price;
    }

    public void setAllow_open_price(String allow_open_price) {
        this.allow_open_price = allow_open_price;
    }
}
