package com.royalpos.waiter.model;

public class RoundingPojo {

    private String is_rounding_on;
    private String rounding_id;
    private String interval_number;
    private String rounding_rule;
    private String rounding_tag;
    String show_item_price_without_tax;
    String pin,allow_payment;
    private String points_per_one_currency;
    String check_pin_admin,check_pin_cashier,service_charges;

    private String decimal_value;
    private String urbanpiper_status;
    private String allow_barcode_payment;
    private String brand_name;
    private String type_of_business;
    private String business_type_name;
    private String name;
    private String address;
    private String gst_no;
    private String receipt_footer;
    private String can_change_price;
    private String cash_drawer;
    private String barcode_receipt_payment;
    private String check_foc;
    private String foc_amt,is_customer_app_enabled;
    String phno,logo,currency,reporting_emails,recipe_inventory,start_stock_deduct,membership_end_date,packaging_charge;
    String plan_id,recipt_footer_phone;
    String advance_order,adv_order_duration;
    String square_enable,square_mapped_pay_mode;
    String allow_update_order,allow_item_wise_discount,item_master_on_app;
    String tip_calculation,tip_calculation_preference;
    String phonepe_enable,phonepe_mapped_pay_mode,phonepe_upi_dynamic_qrcode;

    public String getIs_rounding_on() {
        return is_rounding_on;
    }

    public String getRounding_id() {
        return rounding_id;
    }

    public String getInterval_number() {
        return interval_number;
    }

    public String getRounding_rule() {
        return rounding_rule;
    }

    public String getRounding_tag() {
        return rounding_tag;
    }

    public String getShow_item_price_without_tax() {
        return show_item_price_without_tax;
    }

    public void setShow_item_price_without_tax(String show_item_price_without_tax) {
        this.show_item_price_without_tax = show_item_price_without_tax;
    }

    public String getPin() {
        return pin;
    }

    public String getAllow_payment() {
        return allow_payment;
    }

    public String getPoints_per_one_currency() {
        return points_per_one_currency;
    }

    public String getCheck_pin_admin() {
        return check_pin_admin;
    }

    public String getCheck_pin_cashier() {
        return check_pin_cashier;
    }

    public String getService_charges() {
        return service_charges;
    }

    public String getDecimal_value() {
        return decimal_value;
    }

    public String getUrbanpiper_status() {
        return urbanpiper_status;
    }

    public String getAllow_barcode_payment() {
        return allow_barcode_payment;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public String getType_of_business() {
        return type_of_business;
    }

    public String getBusiness_type_name() {
        return business_type_name;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getGst_no() {
        return gst_no;
    }

    public String getReceipt_footer() {
        return receipt_footer;
    }

    public String getCan_change_price() {
        return can_change_price;
    }

    public String getCash_drawer() {
        return cash_drawer;
    }

    public String getBarcode_receipt_payment() {
        return barcode_receipt_payment;
    }

    public String getCheck_foc() {
        return check_foc;
    }

    public String getFoc_amt() {
        return foc_amt;
    }

    public String getIs_customer_app_enabled() {
        return is_customer_app_enabled;
    }

    public String getPhno() {
        return phno;
    }

    public String getLogo() {
        return logo;
    }

    public String getCurrency() {
        return currency;
    }

    public String getReporting_emails() {
        return reporting_emails;
    }

    public String getRecipe_inventory() {
        return recipe_inventory;
    }

    public String getStart_stock_deduct() {
        return start_stock_deduct;
    }

    public String getMembership_end_date() {
        return membership_end_date;
    }

    public String getPackaging_charge() {
        return packaging_charge;
    }

    public String getPlan_id() {
        return plan_id;
    }

    public String getRecipt_footer_phone() {
        return recipt_footer_phone;
    }

    public String getAdvance_order() {
        return advance_order;
    }

    public String getAdv_order_duration() {
        return adv_order_duration;
    }

    public String getSquare_enable() {
        return square_enable;
    }

    public String getSquare_mapped_pay_mode() {
        return square_mapped_pay_mode;
    }

    public String getAllow_update_order() {
        return allow_update_order;
    }

    public String getAllow_item_wise_discount() {
        return allow_item_wise_discount;
    }

    public String getItem_master_on_app() {
        return item_master_on_app;
    }

    public String getTip_calculation() {
        return tip_calculation;
    }

    public String getTip_calculation_preference() {
        return tip_calculation_preference;
    }

    public String getPhonepe_enable() {
        return phonepe_enable;
    }

    public String getPhonepe_mapped_pay_mode() {
        return phonepe_mapped_pay_mode;
    }

    public String getPhonepe_upi_dynamic_qrcode() {
        return phonepe_upi_dynamic_qrcode;
    }
}
