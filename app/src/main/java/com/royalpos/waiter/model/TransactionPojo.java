package com.royalpos.waiter.model;

/**
 * Created by reeva on 05/05/21.
 */

public class TransactionPojo {

    String checksum,order_id,success, amount;
    String code,message;
    DataPojo data;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getChecksum() {
        return checksum;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public DataPojo getData() {
        return data;
    }

    public class DataPojo{
        String qrString,transactionId,merchantId;

        public String getQrString() {
            return qrString;
        }

        public String getTransactionId() {
            return transactionId;
        }

        public String getMerchantId() {
            return merchantId;
        }
    }
}
