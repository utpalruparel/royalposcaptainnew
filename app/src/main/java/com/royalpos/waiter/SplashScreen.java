package com.royalpos.waiter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.royalpos.waiter.database.DBCombo;
import com.royalpos.waiter.database.DBCusines;
import com.royalpos.waiter.database.DBDishes;
import com.royalpos.waiter.database.DBModifier;
import com.royalpos.waiter.database.DBPreferences;
import com.royalpos.waiter.helper.AppConst;
import com.royalpos.waiter.helper.ConnectionDetector;
import com.royalpos.waiter.helper.GenerateToken;
import com.royalpos.waiter.helper.MemoryCache;
import com.royalpos.waiter.helper.SelectLanguage;
import com.royalpos.waiter.model.ComboModel;
import com.royalpos.waiter.model.DishPojo;
import com.royalpos.waiter.model.M;
import com.royalpos.waiter.model.MembershipPojo;
import com.royalpos.waiter.model.Modifier_cat;
import com.royalpos.waiter.model.PreModel;
import com.royalpos.waiter.model.UpdatePojo;
import com.royalpos.waiter.webservices.APIServiceHeader;
import com.royalpos.waiter.webservices.AuthenticationAPI;
import com.royalpos.waiter.webservices.CuisineDishAPI;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;

public class SplashScreen extends AppCompatActivity {

    ConnectionDetector connectionDetector;
    Context context;
    String TAG="SplashScreen",version="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        MemoryCache memoryCache = new MemoryCache();
        context=SplashScreen.this;
        connectionDetector=new ConnectionDetector(context);
       // AppConst.analytics(context, getLocalClassName());
        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
            // Log.d(TAG,"version:"+version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        if(AppConst.choose_lang && (SelectLanguage.getLocale(context)==null || SelectLanguage.getLocale(context).trim().length()==0)){
            Intent it=new Intent(context,ChooseLangActivity.class);
            startActivity(it);
        }else {
            new Handler().postDelayed(new Runnable() {
                public void run() {

                    if (AppConst.choose_lang)
                        SelectLanguage.changeLang(SelectLanguage.getLocale(context), context);
                    if (M.getBrandId(context) != null && M.getBrandId(context).trim().length() > 0 &&
                            M.getRestID(context) != null && M.getRestID(context).trim().length() > 0) {
                        getPreferncesData();
                    } else if (M.getBrandId(context) != null && M.getBrandId(context).trim().length() > 0 && M.getDeviceCode(context)==null) {
                        Intent it = new Intent(SplashScreen.this, RestaurantListActivity.class);
                        finish();
                        startActivity(it);
                        overridePendingTransition(0, 0);
                    } else {
                        Intent it = new Intent(SplashScreen.this, LoginActivity.class);
                        finish();
                        startActivity(it);
                        overridePendingTransition(0, 0);
                    }
                }
            }, 200);
        }
    }

    void getPreferncesData() {
        if(connectionDetector.isConnectingToInternet()){
            Log.d(TAG,M.getRestID(context)+" " +M.getRestUniqueID(context)+" "+M.getRestUserName(context));
            AuthenticationAPI mAuthenticationAPI = APIServiceHeader.createService(context, AuthenticationAPI.class);
            Call<MembershipPojo>call = mAuthenticationAPI.checkMembership
                    (M.getRestID(context),M.getRestUniqueID(context),null);//,M.getRestUserName(context));
            call.enqueue(new retrofit2.Callback<MembershipPojo>() {
                @Override
                public void onResponse(Call<MembershipPojo> call, retrofit2.Response<MembershipPojo> response) {
                    if (response.isSuccessful()) {
                        final MembershipPojo pojo=response.body();
                        M.setExpiry(pojo.getMembership_end_date(), context);
                        M.setDecimalVal(pojo.getDecimal_value(),context);
                        if(pojo.getStatus().equals("inactive")) {
                            final Dialog dialog=new Dialog(context);
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialog.setContentView(R.layout.dialog_inactive_rest);
                            dialog.setCancelable(false);
                            TextView btncancel=(TextView)dialog.findViewById(R.id.btncancel);
                            TextView btnok=(TextView)dialog.findViewById(R.id.btnok);
                            btncancel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialog.dismiss();
                                    M.logOut(context);
                                    finish();
                                }
                            });


                            btnok.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent emailIntent = new Intent(Intent.ACTION_SEND);
                                    emailIntent.setType("text/plain");
                                    emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {"admin@royalpos.in"});
                                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, M.getRestName(context)+" Inactive");
                                    emailIntent.putExtra(Intent.EXTRA_TEXT, "");
                                    startActivity(Intent.createChooser(emailIntent, getString(R.string.txt_choose)));
                                    dialog.dismiss();
                                    finish();
                                }
                            });
                            dialog.show();
                        }else if (pojo.getVersion_number().equals(version)) {
                            if (AppConst.dishorederlist != null)
                                AppConst.dishorederlist.clear();
                            if (AppConst.selidlist != null)
                                AppConst.selidlist.clear();
                            if (pojo.getMembership_expires() != null && pojo.getMembership_expires().equalsIgnoreCase("yes")) {
                                Intent it = new Intent(context, MyAccount.class);
                                finish();
                                startActivity(it);
                            } else
                                checkTime(pojo.getCurrent_date());
                        }else {
                            checkTime(pojo.getCurrent_date());
                            //redirectToMain();
                            //updateApk(pojo);
                        }

                    } else {
                        int statusCode = response.code();
                        ResponseBody errorBody = response.errorBody();
                        Log.d(TAG, "error:" + statusCode + " " + errorBody);
                        redirectToMain();
                    }
                }

                @Override
                public void onFailure(Call<MembershipPojo> call, Throwable t) {
                    Log.d(TAG, "fail:" + t.getMessage());
                    checkMembership(null);
                }
            });
        }else {
            Toast.makeText(context, R.string.no_internet_error, Toast.LENGTH_SHORT).show();
            checkMembership(null);
        }
    }

    private void checkMembership(String tm){
        String dt=M.getExpiry(context);
        if(dt!=null && dt.trim().length()>0) {
            try {
                Date d = new SimpleDateFormat("yyyy-MM-dd").parse(dt);
                if (new Date().before(d)) {
                    checkTime(tm);
                }else{
                    Intent it=new Intent(context,MyAccount.class);
                    finish();
                    startActivity(it);
                }
            } catch (ParseException e) {
                Log.d(TAG,"date error"+e.getMessage());
                e.printStackTrace();
            }
        }else{
            checkTime(tm);
        }
    }

    private void updateApk(final MembershipPojo pojo){
        final Dialog dialog=new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_update_apk);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        TextView tvok=(TextView)dialog.findViewById(R.id.btnok);
        TextView btnskip=(TextView)dialog.findViewById(R.id.btnskip);
        btnskip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if(AppConst.dishorederlist!=null)
                    AppConst.dishorederlist.clear();
                if(AppConst.selidlist!=null)
                    AppConst.selidlist.clear();
                M.setExpiry(pojo.getMembership_end_date(),context);
                if(pojo.getMembership_expires()!=null && pojo.getMembership_expires().equalsIgnoreCase("yes")){
                    Intent it=new Intent(context,MyAccount.class);
                    finish();
                    startActivity(it);
                }else
                    redirectToMain();
            }
        });


        tvok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });
        dialog.show();
    }

    private void checkTime(String tm){
        GenerateToken generateToken=new GenerateToken();
        Boolean isValid=generateToken.checkSysTime(tm,context);
        if(!isValid) {
            final Dialog d = new Dialog(context);
            d.requestWindowFeature(Window.FEATURE_NO_TITLE);
            d.setCancelable(false);
            d.setContentView(R.layout.dialog_custom_alert);
            TextView tvmsg=d.findViewById(R.id.tvmsg);
            tvmsg.setText("Please check your device date as it does not match with your Timezone. Wrong device date will effect in your sales reports.");
            TextView btncanel=d.findViewById(R.id.btncancel);
            btncanel.setVisibility(View.GONE);
            TextView btnok=d.findViewById(R.id.btnok);
            btnok.setText(R.string.ok);
            btnok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    d.dismiss();
                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    intent.addCategory(Intent.CATEGORY_HOME);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    finish();
                    startActivity(intent);
                }
            });
            d.show();
        }else
            redirectToMain();
    }

    private void redirectToMain(){

        if(M.iswaiterloggedin(context)) {
            DBCusines dbCusines=new DBCusines(context);
            int ccnt=dbCusines.getCuisineCount();
            DBDishes dbDishes=new DBDishes(context);
            int dcnt=dbDishes.getDishCount();
            if(ccnt>0 && dcnt==0) {
                getAllDishes(context, null);
            }else
                redirectToOrder();
        }else if(M.getDeviceCode(context)!=null){
            M.waiterlogOut(context);
            Intent mIntent = new Intent(context, LoginActivity.class);
            finish();
            startActivity(mIntent);
        }else{
            AppConst.currency = M.getCurrency(context);
            Intent mIntent = new Intent(context, WaiterListActivity.class);
            finish();
            startActivity(mIntent);
        }
    }

    private void redirectToOrder(){
        DBCusines dbCusines=new DBCusines(context);
        dbCusines.checkField();
        Intent it=new Intent(context,WaiterActivity.class);
        finish();
        startActivity(it);
    }

    public void getAllDishes(final Context context, final UpdatePojo resp) {
        connectionDetector=new ConnectionDetector(context);
        if(connectionDetector.isConnectingToInternet()) {
            CuisineDishAPI mAuthenticationAPI = APIServiceHeader.createService(context, CuisineDishAPI.class);
            Call<List<DishPojo>> call = mAuthenticationAPI.getDishes(M.getRestID(context), M.getRestUniqueID(context));
            call.enqueue(new retrofit2.Callback<List<DishPojo>>() {
                @Override
                public void onResponse(Call<List<DishPojo>> call, retrofit2.Response<List<DishPojo>> response) {
                    if (response.isSuccessful()) {
                        List<DishPojo> dishiesPojos = response.body();
                        DBDishes db = new DBDishes(context);
                        db.deleteallcuisine();
                        DBPreferences dbp = new DBPreferences(context);
                        dbp.deleteAllPre();
                        DBModifier dbm=new DBModifier(context);
                        dbm.deleteAllModi();
                        DBCombo dbCombo = new DBCombo(context);
                        dbCombo.deleteAllPre();
                        if (dishiesPojos != null) {
                            for (DishPojo dishesdata : dishiesPojos) {

                                String dishid = dishesdata.getDishid();
                                db.addDishes(dishesdata);
                                if (dishesdata.getPreflag().equals("true") && dishesdata.getPre() != null) {
                                    for (PreModel data : dishesdata.getPre()) {
                                        dbp.addPref(data, dishid);
                                    }
                                    for(Modifier_cat mdata:dishesdata.getModifier_cat()){
                                        dbm.check(mdata,dishid);
                                    }
                                }


                                if(dishesdata.getCombo_flag()!=null) {

                                    if (dishesdata.getCombo_flag().equals("true") && dishesdata.getCombo_item() !=
                                            null) {
                                        for (ComboModel combo : dishesdata.getCombo_item()) {
                                            dbCombo.addcombo(combo, dishid);
                                        }

                                    }
                                }
                            }
                        }
                        redirectToOrder();
                    } else {
                        int statusCode = response.code();
                        ResponseBody errorBody = response.errorBody();
                        Log.d(TAG, "error:" + statusCode + " " + errorBody);
                    }
                }

                @Override
                public void onFailure(Call<List<DishPojo>> call, Throwable t) {
                    Log.d(TAG, "fail:" + t.getMessage());
                }
            });
        }
    }
}
