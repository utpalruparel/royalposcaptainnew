package com.royalpos.waiter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.royalpos.waiter.database.DBCombo;
import com.royalpos.waiter.database.DBCusines;
import com.royalpos.waiter.database.DBDiscount;
import com.royalpos.waiter.database.DBDishes;
import com.royalpos.waiter.database.DBModifier;
import com.royalpos.waiter.database.DBPaymentType;
import com.royalpos.waiter.database.DBPreferences;
import com.royalpos.waiter.database.DBUnit;
import com.royalpos.waiter.helper.AppConst;
import com.royalpos.waiter.helper.ConnectionDetector;
import com.royalpos.waiter.model.ComboModel;
import com.royalpos.waiter.model.CuisineListPojo;
import com.royalpos.waiter.model.DiscountPojo;
import com.royalpos.waiter.model.DishPojo;
import com.royalpos.waiter.model.M;
import com.royalpos.waiter.model.Modifier_cat;
import com.royalpos.waiter.model.PaymentModePojo;
import com.royalpos.waiter.model.PreModel;
import com.royalpos.waiter.model.PreferencePojo;
import com.royalpos.waiter.model.RoundingPojo;
import com.royalpos.waiter.model.UnitPojo;
import com.royalpos.waiter.webservices.APIServiceHeader;
import com.royalpos.waiter.webservices.CuisineDishAPI;
import com.royalpos.waiter.webservices.OrderAPI;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * Created by reeva on 6/10/17.
 */

public class OutletData {

    public static ConnectionDetector connectionDetector;
    public static String TAG="OutletData";

    public static void getCuisine(final Context context) {
        Log.d("here---", M.getRestID(context)+" "+M.getBrandId(context)+ " "+M.getRestUniqueID
                (context));
        connectionDetector=new ConnectionDetector(context);
        if(connectionDetector.isConnectingToInternet()) {
            CuisineDishAPI mAuthenticationAPI = APIServiceHeader.createService(context, CuisineDishAPI.class);
            Call<List<CuisineListPojo>> call = mAuthenticationAPI.getCuisines(M.getRestID(context), M.getRestUniqueID(context));
            call.enqueue(new retrofit2.Callback<List<CuisineListPojo>>() {
                @Override
                public void onResponse(Call<List<CuisineListPojo>> call, retrofit2.Response<List<CuisineListPojo>> response) {
                    if (response.isSuccessful()) {
                        List<CuisineListPojo> custCuisineListPojos = response.body();
                        if (custCuisineListPojos != null) {
                            DBCusines db = new DBCusines(context);
                            db.deleteallcuisine();
                            for (CuisineListPojo pojo : custCuisineListPojos) {
                                db.addCusines(pojo);
                            }
                        }
                    } else {
                        int statusCode = response.code();
                        ResponseBody errorBody = response.errorBody();
                        Log.d(TAG, "error:" + statusCode + " " + errorBody);
                    }
                }

                @Override
                public void onFailure(Call<List<CuisineListPojo>> call, Throwable t) {
                    Log.d("response:", "fail:" + t.getMessage());
                }
            });
        }
    }

    public static void getAllDishes(final Context context) {
        Log.d("here---", M.getRestID(context)+" "+M.getBrandId(context)+ " "+M.getRestUniqueID
                (context));
        connectionDetector=new ConnectionDetector(context);
        if(connectionDetector.isConnectingToInternet()) {
            CuisineDishAPI mAuthenticationAPI = APIServiceHeader.createService(context, CuisineDishAPI.class);
            Call<List<DishPojo>> call = mAuthenticationAPI.getDishes(M.getRestID(context), M.getRestUniqueID(context));
            call.enqueue(new retrofit2.Callback<List<DishPojo>>() {
                @Override
                public void onResponse(Call<List<DishPojo>> call, retrofit2.Response<List<DishPojo>> response) {
                    if (response.isSuccessful()) {
                        List<DishPojo> dishiesPojos = response.body();
                        DBDishes db = new DBDishes(context);
                        db.deleteallcuisine();
                        DBPreferences dbp = new DBPreferences(context);
                        dbp.deleteAllPre();
                        DBModifier dbm=new DBModifier(context);
                        dbm.deleteAllModi();
                        DBCombo dbCombo = new DBCombo(context);
                        dbCombo.deleteAllPre();
                        if (dishiesPojos != null) {
                            for (DishPojo dishesdata : dishiesPojos) {

                                String dishid = dishesdata.getDishid();
                                db.addDishes(dishesdata);
                                if (dishesdata.getPreflag().equals("true") && dishesdata.getPre() != null) {
                                    for (PreModel data : dishesdata.getPre()) {
                                        dbp.addPref(data, dishid);
                                    }
                                    for(Modifier_cat mdata:dishesdata.getModifier_cat()){
                                        dbm.check(mdata,dishid);
                                    }
                                }


                                if(dishesdata.getCombo_flag()!=null) {

                                    if (dishesdata.getCombo_flag().equals("true") && dishesdata.getCombo_item() !=
                                            null) {
                                        for (ComboModel combo : dishesdata.getCombo_item()) {
                                            dbCombo.addcombo(combo, dishid);
                                        }

                                    }
                                }
                            }
                        }
                    } else {
                        int statusCode = response.code();
                        ResponseBody errorBody = response.errorBody();
                        Log.d(TAG, "error:" + statusCode + " " + errorBody);
                    }
                }

                @Override
                public void onFailure(Call<List<DishPojo>> call, Throwable t) {
                    Log.d("response:", "fail:" + t.getMessage());
                }
            });
        }
    }

    public static void getRoundingInfo(final Context context) {
        if(connectionDetector.isConnectingToInternet()){
            OrderAPI mAuthenticationAPI = APIServiceHeader.createService(context, OrderAPI.class);
            Call<RoundingPojo> call = mAuthenticationAPI.getRoundingInfo(M.getRestID(context), M.getRestUniqueID(context),M.getWaiterid(context));
            call.enqueue(new retrofit2.Callback<RoundingPojo>() {
                @Override
                public void onResponse(Call<RoundingPojo> call, retrofit2.Response<RoundingPojo> response) {
                    if (response.isSuccessful()) {
                        RoundingPojo resp = response.body();
                        if(resp!=null){
                            if(resp.getIs_rounding_on().equals("on") &&
                                    resp.getRounding_id()!=null && !resp.getRounding_id().equals("0")){
                                try {
                                    JSONObject jsonObject=new JSONObject();
                                    jsonObject.put("val_interval",resp.getInterval_number());
                                    jsonObject.put("rule_tag",resp.getRounding_tag());
                                    jsonObject.put("rounding_id",resp.getRounding_id());
                                    M.setRoundBoundary(jsonObject+"",context);
                                    M.setRoundFormat(true,context);
                                }catch (JSONException e){}
                            }else
                                M.setRoundFormat(false,context);
                            if(resp.getShow_item_price_without_tax()!=null){
                                M.setPriceWT(Boolean.parseBoolean(resp.getShow_item_price_without_tax()),context);
                            }
                            if(resp.getAllow_payment()!=null)
                                M.setCashierPermission(Boolean.parseBoolean(resp.getAllow_payment()),context);
                            M.setOutletPin(resp.getPin(),context);
                            M.setPointsAmt(resp.getPoints_per_one_currency(),context);
                            if(resp.getAllow_item_wise_discount()!=null && resp.getAllow_item_wise_discount().equals("enable"))
                                M.setItemDiscount(true,context);
                            else
                                M.setItemDiscount(false,context);
                            if(!resp.getService_charges().equals("disable"))
                                M.setServiceCharge(true,context);
                            else
                                M.setServiceCharge(false,context);
                            M.saveVal(M.key_tip_cal,resp.getTip_calculation(),context);
                            M.saveVal(M.key_tip_pref,resp.getTip_calculation_preference(),context);
                            M.saveVal(M.key_phonepe_status,resp.getPhonepe_enable(),context);
                            M.saveVal(M.key_phonepe_paymode,resp.getPhonepe_mapped_pay_mode(),context);
                            M.saveVal(M.key_phonepe_paymode_qrcode,resp.getPhonepe_upi_dynamic_qrcode(),context);
                        }
                        String tm=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
                        Log.d(TAG,"timing:"+tm);
                        M.setUpdateTime(tm,context);
                    } else {
                        int statusCode = response.code();
                        ResponseBody errorBody = response.errorBody();
                        Log.d(TAG, "rounding:" + statusCode + " " + errorBody);
                    }
                }

                @Override
                public void onFailure(Call<RoundingPojo> call, Throwable t) {
                    Log.d(TAG, "rounding fail:" + t.getMessage());
                }
            });
        }else
            Toast.makeText(context,R.string.no_internet_error, Toast.LENGTH_SHORT).show();
    }

    public static void getPaymentMode(final Context context) {
        connectionDetector=new ConnectionDetector(context);
        if(connectionDetector.isConnectingToInternet()){
            OrderAPI mAuthenticationAPI = APIServiceHeader.createService(context, OrderAPI.class);
            Call<List<PaymentModePojo>> call = mAuthenticationAPI.getPaymentMode(M.getRestID(context), M.getRestUniqueID(context));
            call.enqueue(new retrofit2.Callback<List<PaymentModePojo>>() {
                @Override
                public void onResponse(Call<List<PaymentModePojo>> call, retrofit2.Response<List<PaymentModePojo>> response) {
                    if (response.isSuccessful()) {
                        List<PaymentModePojo> resp = response.body();
                        if (resp != null) {
                            DBPaymentType dbPaymentType=new DBPaymentType(context);
                            dbPaymentType.deletealltype();
                            for (PaymentModePojo p:resp){
                                dbPaymentType.addType(p);
                            }
                        }
                        getRoundingInfo(context);
                    } else {
                        int statusCode = response.code();
                        ResponseBody errorBody = response.errorBody();
                        Log.d(TAG, "error:" + statusCode + " " + errorBody);
                    }
                }

                @Override
                public void onFailure(Call<List<PaymentModePojo>> call, Throwable t) {
                    Log.d("response:", "fail:" + t.getMessage());
                }
            });
        }
    }

    public static void getUnits(final Context context) {
        if(connectionDetector.isConnectingToInternet()){
            CuisineDishAPI mAuthenticationAPI = APIServiceHeader.createService(context, CuisineDishAPI.class);
            Call<List<UnitPojo>> call = mAuthenticationAPI.getUnitList(M.getRestID(context), M.getRestUniqueID(context));
            call.enqueue(new retrofit2.Callback<List<UnitPojo>>() {
                @Override
                public void onResponse(Call<List<UnitPojo>> call, retrofit2.Response<List<UnitPojo>> response) {
                    if (response.isSuccessful()) {
                        List<UnitPojo> resp = response.body();
                        if(resp!=null){
                            DBUnit dbUnit=new DBUnit(context);
                            dbUnit.deleteall();
                            for (UnitPojo p:resp){
                                dbUnit.addUnit(p);
                            }
                        }
                    } else {
                        int statusCode = response.code();
                        ResponseBody errorBody = response.errorBody();
                        Log.d(TAG, "error:" + statusCode + " " + errorBody);
                    }
                }

                @Override
                public void onFailure(Call<List<UnitPojo>> call, Throwable t) {
                    Log.d("response:", "payment fail:" + t.getMessage());
                }
            });
        }
    }

    public static void getDiscount(final Context context) {
        if(connectionDetector.isConnectingToInternet()){
            CuisineDishAPI mAuthenticationAPI = APIServiceHeader.createService(context, CuisineDishAPI.class);
            Call<List<DiscountPojo>> call = mAuthenticationAPI.billOffer(M.getRestID(context),M.getRestUniqueID(context));
            call.enqueue(new retrofit2.Callback<List<DiscountPojo>>() {
                @Override
                public void onResponse(Call<List<DiscountPojo>> call, retrofit2.Response<List<DiscountPojo>> response) {
                    if (response.isSuccessful()) {
                        List<DiscountPojo> resp = response.body();
                        if (resp != null) {
                            DBDiscount dbDiscount=new DBDiscount(context);
                            dbDiscount.deleteall();
                            for(DiscountPojo d:resp){
                                dbDiscount.addDiscount(d);
                            }
                        }
                    } else {
                        int statusCode = response.code();
                        ResponseBody errorBody = response.errorBody();
                        Log.d(TAG, "error:" + statusCode + " " + errorBody);
                    }
                }

                @Override
                public void onFailure(Call<List<DiscountPojo>> call, Throwable t) {
                    Log.d(TAG, "discount fail:" + t.getMessage());
                }
            });
        }
    }

    public static void get_rest_logo_footer(final Context context) {
        connectionDetector=new ConnectionDetector(context);
        if(connectionDetector.isConnectingToInternet()){
            OrderAPI mAuthenticationAPI = APIServiceHeader.createService(context, OrderAPI.class);
            Call<List<PreferencePojo>> call = mAuthenticationAPI.get_rest_logo_footer(M.getRestID(context), M.getRestUniqueID(context));
            call.enqueue(new retrofit2.Callback<List<PreferencePojo>>() {
                @Override
                public void onResponse(Call<List<PreferencePojo>> call, retrofit2.Response<List<PreferencePojo>> response) {
                    if (response.isSuccessful()) {
                        List<PreferencePojo> resp = response.body();
                        if(resp!=null)
                        {
                            String logo = resp.get(0).getValue();
                            String footer = resp.get(1).getValue();

                            M.setlogo(AppConst.restaurant_logo_url+M.getBrandId(context)+"/restaurant_logo/" +logo, context);
                            M.setreceipt_footer(footer, context);
                            if(logo.length()>0) {
                                downloadFile(AppConst.restaurant_logo_url+M.getBrandId(context)+"/restaurant_logo/"+ logo, context);
                            }
                        }
                    } else {
                        int statusCode = response.code();
                        ResponseBody errorBody = response.errorBody();
                        Log.d(TAG, "error:" + statusCode + " " + errorBody);
                    }
                }

                @Override
                public void onFailure(Call<List<PreferencePojo>> call, Throwable t) {
                    Log.d("response:", "fail:" + t.getMessage());
                }
            });
        }
    }

    public static void downloadFile(String ImageUrl, final Context mContext) {
          Log.d(TAG,"imgurl:"+ImageUrl);
        Picasso.get()
                .load(ImageUrl)
                .into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        try {
                            File myDir = new File(AppConst.folder_dir);

                            if (!myDir.exists()) {
                                myDir.mkdirs();
                            }
                            if (!myDir.exists()) {
                                myDir.mkdirs();
                            }

                            String name = "logo.png";
                            myDir = new File(myDir, name);
                            FileOutputStream out = new FileOutputStream(myDir);
                            bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);

                            out.flush();
                            out.close();

                        } catch(Exception e){
                            // some action
                            Log.d(TAG,"exp:"+e.getMessage());
                        }
                    }

                    @Override
                    public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });



    }
}
