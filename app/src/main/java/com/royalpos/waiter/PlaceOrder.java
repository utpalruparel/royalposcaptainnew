package com.royalpos.waiter;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;

import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.JsonObject;
import com.royalpos.waiter.adapter.ItemListAdapter;
import com.royalpos.waiter.adapter.TableItemAdapter;
import com.royalpos.waiter.database.DBCusines;
import com.royalpos.waiter.database.DBDiscount;
import com.royalpos.waiter.database.DBDishes;
import com.royalpos.waiter.database.DBKP;
import com.royalpos.waiter.database.DBPaymentType;
import com.royalpos.waiter.database.DBPrinter;
import com.royalpos.waiter.dialog.OnlineTransDialog;
import com.royalpos.waiter.dialog.alertdialog.PhonePeAlertDialog;
import com.royalpos.waiter.dialog.JoinServerDialog;
import com.royalpos.waiter.helper.AppConst;
import com.royalpos.waiter.helper.ConnectionDetector;
import com.royalpos.waiter.helper.MemoryCache;
import com.royalpos.waiter.helper.RoundHelper;
import com.royalpos.waiter.helper.WifiHelper;
import com.royalpos.waiter.model.AddressPojo;
import com.royalpos.waiter.model.ComboModel;
import com.royalpos.waiter.model.CuisineListPojo;
import com.royalpos.waiter.model.CustomerPojo;
import com.royalpos.waiter.model.DiscountPojo;
import com.royalpos.waiter.model.DishOrderPojo;
import com.royalpos.waiter.model.DishPojo;
import com.royalpos.waiter.model.KitchenPrinterPojo;
import com.royalpos.waiter.model.M;
import com.royalpos.waiter.model.OfflineOrder;
import com.royalpos.waiter.model.Order;
import com.royalpos.waiter.model.PaymentModePojo;
import com.royalpos.waiter.model.SendOrderPojo;
import com.royalpos.waiter.model.SuccessPojo;
import com.royalpos.waiter.model.Tax;
import com.royalpos.waiter.model.TaxData;
import com.royalpos.waiter.model.TaxInvoicePojo;
import com.royalpos.waiter.model.TransactionPojo;
import com.royalpos.waiter.model.VarPojo;
import com.royalpos.waiter.print.KitchenPrintActivity;
import com.royalpos.waiter.print.OtherPrintReceipt;
import com.royalpos.waiter.print.PrintActivity;
import com.royalpos.waiter.print.PrintFormat;
import com.royalpos.waiter.print.StarPrintReceipt;
import com.royalpos.waiter.print.newbluetooth.AsyncNewPrintNew;
import com.royalpos.waiter.print.newbluetooth.GlobalsNew;
import com.royalpos.waiter.print.newbluetooth.KitchenGlobalsNew;
import com.royalpos.waiter.print.newbluetooth.KitchenPrintActivityNew;
import com.royalpos.waiter.print.newbluetooth.PrintActivityNew;
import com.royalpos.waiter.ui.MenuUI;
import com.royalpos.waiter.ui.PagerSlidingTabStrip;
import com.royalpos.waiter.ui.PagerSlidingTabStripVertical;
import com.royalpos.waiter.ui.chips.ChipCloud;
import com.royalpos.waiter.ui.chips.ChipListener;
import com.royalpos.waiter.universalprinter.Globals;
import com.royalpos.waiter.universalprinter.KitchenGlobals;
import com.royalpos.waiter.utils.AidlUtil;
import com.royalpos.waiter.utils.BitmapUtil;
import com.royalpos.waiter.webservices.APIServiceHeader;
import com.royalpos.waiter.webservices.AuthenticationAPI;
import com.royalpos.waiter.webservices.CuisineDishAPI;
import com.royalpos.waiter.webservices.OrderAPI;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import de.greenrobot.event.EventBus;
import in.arjsna.passcodeview.PassCodeView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class PlaceOrder extends AppCompatActivity implements  View.OnClickListener{

    Context context;
    String TAG="PlaceOrder";
    ImageView ivback;
    TextView tvheading,dtvamt,dtvgrand,tvgstno,tvnodata,tvrounding,tvtip,txtreturnamt,tvno,btnpartial,tvitemdisc;
    TextView tvgrand,tvsub,btndone,btncancel,tvdiscount,btnsubmit,btndemo,tvg,tvd,tvsercharge;//dinein showfull order dialog
    TextView tvpoint,tvpay,tvredeempoint,tvselect;
    AutoCompleteTextView etsearch;
    RadioButton rbper,rbamt,rbscper,rbscamt;
    LinearLayout llhr,llvr,lltax,llgrand,llnodata,llcal,lldiscount,lldiscper,llsplit,llamount,llpmode,llcharge,lloffer,llitemdisc,llstd,llrounding,lladdress,llservice,ll_tip,llserper;
    LinearLayout lldata,llbtntbl;
    EditText editText,etdiscount,etdisamt,etpackcharge,etservicecharge,etscamt;
    Button btnsync,btnlogout,btnaddItems,btnphpestatus;
    Button buttonSeven,buttonEight, buttonNine, buttonFour, buttonFive, buttonSix, buttonOne, buttonTwo, buttonThree, buttonClear, buttonZero, buttonEqual;
    ChipCloud chip_cloud_preset,chipdiscount,cc_service_charge,cc_offer;
    EditText etname,etphone,etemail,etaddress,ettax;
    RecyclerView rvorders;
    Button btnupdate,btnfinish,btncanceltbl;
    ViewPager pager,vr_pager,hr_pager;
    PagerSlidingTabStrip tabs_hr;
    PagerSlidingTabStripVertical tabs_vr;

    Typeface fontregular,fontsemibold;

    List<CuisineListPojo> cuisinelist = new ArrayList<CuisineListPojo>();
    ArrayList<TaxInvoicePojo> taxInvoiceList=new ArrayList<>(),taxDisList;
    ArrayList<String> taxidlist=new ArrayList<>(),odlist=new ArrayList<>(),wlist = new ArrayList<String>();
    ArrayList<AddressPojo> addressList = new ArrayList<>();
    ArrayList<PaymentModePojo> plist=new ArrayList<>();
    ArrayList<KitchenPrinterPojo> kplist=new ArrayList<>();
    ArrayList<Integer> totlist=new ArrayList<>();
    JoinServerDialog jdialog;
    Dialog dialog,dialogcust,splitDialog,ssDialog,dialog2,joinDialog,phDialog,tipDialog;
    OnlineTransDialog onlineTransDialog;
    PhonePeAlertDialog phonePeAlertDialog;
    ConnectionDetector connectionDetector;
    public static Activity activity;
    private SharedPreferences sharedPrefs = null;
    PrintFormat pf;
    RoundHelper roundHelper;
    POServerService mService;

    String runiqueid,restaurantid,payment_mode="",payment_mode_text="",ordertype="",order_no="",en_ordertype="",pm_rounding="off";
    String tm=null,  cash="",return_cash="",finalprice, ordercomment, token_number, customernum="", customername;
    String cashprintermodel, cashprintertarget, kitchenprintermodel, kitchenprintertarget,action="dinein";
    String orderid,tblid,tblnm,custnm,custph,transid,offer_id=null,offer_name=null;
    String s_cmt, s_cust_nm, s_cust_ph, s_cust_mail, s_cust_address,s_tax;
    Boolean mBound=false,ishome=true,setitemOffer=false,is_portrait=false,is_split=false;
    double splite_amt=0,pointperamt=0,dis_per,dis_amt,granttodal=0 ,retamount = 0,subtotal = 0;
    int currpos=0,newitems=0;
    JSONArray splitArray=new JSONArray(),jsonArray;
    JSONObject pointJSON,jsonObject;
    public OfflineOrder orderData;
    DiscountPojo discountPojo;
    Bitmap qrbitmap;

    TableItemAdapter tableItemAdapter;
    PagerAdapter adapter;
    long refreshtime=1*60*1000;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_place_order);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);

        MemoryCache memoryCache = new MemoryCache();
        memoryCache.clear();
        context=PlaceOrder.this;
        activity=PlaceOrder.this;

        OutletData.get_rest_logo_footer(context);
        if(AppConst.dishorederlist!=null)
            AppConst.dishorederlist.clear();
        if(AppConst.selidlist!=null)
            AppConst.selidlist.clear();
        odlist.clear();

        if(M.getCurrency(context).length()>0){
            AppConst.currency = M.getCurrency(context)+ " ";
        }
        ishome=true;
        pf=new PrintFormat(context);
        roundHelper=new RoundHelper(context);
        cashprintermodel = M.getCashPrinterModel(context);
        cashprintertarget = M.getCashPrinterIP(context);
        kitchenprintermodel = M.getKitchenPrinterModel(context);
        kitchenprintertarget = M.getKitchenPrinterIP(context);
        AidlUtil.getInstance().connectPrinterService(context);
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);  // * load preference
        // manager
        Globals.loadPreferences(sharedPrefs);
        KitchenGlobals.loadPreferences(sharedPrefs);
        AppConst.checkSame(context);

        tvheading=(TextView)findViewById(R.id.tvheading);

        if(getIntent().getExtras()!=null) {
            //Log.d(TAG,"data:"+getIntent().getExtras());
            orderid=getIntent().getStringExtra("orderid");
            tblid=getIntent().getStringExtra("tbl_id");
            tblnm=getIntent().getStringExtra("tbl_nm");
            tm=getIntent().getStringExtra("ordertime");
            token_number=getIntent().getStringExtra("token");
            order_no=getIntent().getStringExtra("order_no");
            String ofid=getIntent().getStringExtra("offerid");
            String dis=getIntent().getStringExtra("discount");
            String billdis=getIntent().getStringExtra("bill_dis");
            String sercharge=getIntent().getStringExtra("service_charge");

            orderData=new OfflineOrder();
            orderData.setOfferid(ofid);
            orderData.setOrder_discount(dis);
            orderData.setBill_amt_discount(billdis);
            orderData.setService_charge(sercharge);
            if(getIntent().hasExtra("custnm"))
                custnm=getIntent().getStringExtra("custnm");
            if(getIntent().hasExtra("custph"))
                custph=getIntent().getStringExtra("custph");
            tvheading.setText(tblnm);
            tvheading.setTag(action);
            connectionDetector=new ConnectionDetector(context);
            restaurantid= M.getRestID(context);
            runiqueid=M.getRestUniqueID(context);
            fontregular = AppConst.font_regular(context);
            fontsemibold = AppConst.font_medium(context);
            pointperamt = Double.parseDouble(M.getPointsAmt(context));
            pager = (ViewPager)findViewById(R.id.viewpager);

            tvgrand=(TextView)findViewById(R.id.tvgrand);
            tvsub=(TextView)findViewById(R.id.tvamount) ;
            tvitemdisc=(TextView)findViewById(R.id.tvitemdisc);
            tabs_hr = (PagerSlidingTabStrip)findViewById(R.id.tab_hr);
            tabs_hr.setTypeface(fontregular,0);
            tabs_vr = (PagerSlidingTabStripVertical) findViewById(R.id.tab_vr);
            tabs_vr.setTypeface(fontregular,0);
            llvr=(LinearLayout)findViewById(R.id.llvr);
            llvr.setVisibility(View.GONE);
            llhr=(LinearLayout)findViewById(R.id.llhr);
            llhr.setVisibility(View.GONE);
            vr_pager=(ViewPager)findViewById(R.id.viewpager_vr);
            hr_pager=(ViewPager)findViewById(R.id.viewpager_hr);
            llgrand=(LinearLayout)findViewById(R.id.llgrand);
            llitemdisc=(LinearLayout)findViewById(R.id.llitemdisc);
            llitemdisc.setVisibility(View.GONE);
            tvnodata=(TextView)findViewById(R.id.tvnodata);
            lldata=(LinearLayout)findViewById(R.id.lldata);
            llnodata=(LinearLayout)findViewById(R.id.llnodata);
            btnsync=(Button)findViewById(R.id.btnsync);
            btnsync.setOnClickListener(this);
            btndemo=(Button)findViewById(R.id.btndemo);
            btndemo.setTypeface(AppConst.font_regular(context));
            btnlogout=(Button)findViewById(R.id.btnlogout);
            btnlogout.setOnClickListener(this);
            btnaddItems=(Button)findViewById(R.id.btnaddItems);
            btnaddItems.setOnClickListener(this);
            lltax=(LinearLayout)findViewById(R.id.lltax);
            llstd=(LinearLayout)findViewById(R.id.ll_std);
            rvorders=(RecyclerView)findViewById(R.id.rvdishes);
            rvorders.setLayoutManager(new LinearLayoutManager(context));
            rvorders.setHasFixedSize(true);
            llbtntbl=(LinearLayout)findViewById(R.id.llbtntbl);
            btncanceltbl=(Button)findViewById(R.id.btncanceltbl);
            btncanceltbl.setTypeface(AppConst.font_regular(context));
            btnfinish=(Button)findViewById(R.id.btnfinish);
            btnfinish.setTypeface(AppConst.font_regular(context));
            btnupdate=(Button)findViewById(R.id.btnupdate);
            btnupdate.setTypeface(AppConst.font_regular(context));

            updateUI();

            setMenuUI();

            btndemo.setOnClickListener(this);
            btnfinish.setOnClickListener(this);
            btncanceltbl.setOnClickListener(this);
            btnupdate.setOnClickListener(this);

            registerReceiver(broadcastReceiver,new IntentFilter(POServerService.RECEIVE_MSG));

            if(AppConst.isMyServiceRunning(POServerService.class,context)) {
                M.showLoadingDialog(context);
                Intent intent = new Intent(this, POServerService.class);
                bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
            }

            new Handler().postDelayed(new Runnable() {
                public void run() {
                    if(Globals.deviceType==5) {
                        AidlUtil.getInstance().lcdmessage("Welcome to ", "" + M.getBrandName(context));
                    }
                }
            }, 2000);
        }else {
            onBackPressed();
        }
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, Intent intent) {
            if(intent.getExtras()!=null){
                if(intent.hasExtra("stopService")){
                    mBound = false;
                    joinDialog();
                }else if(AppConst.isMyServiceRunning(POServerService.class,context)){
                    String res = intent.getStringExtra("response");
                    try {
                        JSONObject jsonObject = new JSONObject(res);
                        if (jsonObject.has("key")) {
                            M.saveVal(M.key_server_lasttime,new Date().getTime()+"",context);
                            String msg_key = jsonObject.getString("key");
                            if (msg_key.equals(WifiHelper.KEY_CONNECTION)) {
                                if (jsonObject.getString("success").equals(WifiHelper.TXT_FAIL)) {
                                    mService.closeService();
                                } else {
                                    if(joinDialog!=null && joinDialog.isShowing())
                                        joinDialog.dismiss();
                                    Intent it = new Intent(context, POServerService.class);
                                    bindService(it, mConnection, Context.BIND_AUTO_CREATE);
                                }
                            }else if (msg_key.equals(WifiHelper.api_get_order_detail_response)) {
                                if (jsonObject.getString("order_id").equals(orderid)) {
                                    JSONObject resObj = new JSONObject(jsonObject.getString("msg"));
                                    orderData = new OfflineOrder();
                                    if (resObj.has("bill_amt_dis"))
                                        orderData.setBill_amt_discount(resObj.getString("bill_amt_dis"));
                                    if (resObj.has("offer_id"))
                                        orderData.setOfferid(resObj.getString("offer_id"));
                                    if (resObj.has("discount"))
                                        orderData.setOrder_discount(resObj.getString("discount"));
                                    if(resObj.has("is_merge"))
                                        orderData.setIsmerge(resObj.getString("is_merge"));
                                    if(resObj.has("trans_json"))
                                        orderData.setTransjson(resObj.getString("trans_json"));
                                    getOrderData(resObj.getString("items"));
                                }
                            }
                            else if(msg_key.equals(WifiHelper.api_update_order_response)){
                                M.hideLoadingDialog();
                                if (jsonObject.getString("order_id").equals(orderid)) {
                                    if(!M.isKitchenCatPrinter(context) && !M.isKitchenPrinter(context)){
                                        Toast.makeText(context, R.string.order_place_success, Toast.LENGTH_SHORT).show();
                                        onBackPressed();
                                    }else if(M.isKitchenPrinter(context) && KitchenGlobals.deviceType == 0){
                                        Toast.makeText(context, R.string.order_place_success, Toast.LENGTH_SHORT).show();
                                        onBackPressed();
                                    }else {
                                        if (M.isKitchenCatPrinter(context) && kplist != null && kplist.size() > 0) {
                                            int k = 0, kms = 0;
                                            for (final KitchenPrinterPojo kp : kplist) {
                                                if (kp.getDeviceType() != null && kp.getDeviceType().trim().length() > 0 && !kp.getDeviceType().equals("0")) {
                                                    new Handler().postDelayed(new Runnable() {
                                                        public void run() {
                                                            kitchenreciept(kp.getCategoryid(), kp.getId(), Integer.parseInt(kp.getDeviceType()), true, kp.isAdv(), kp.getPaper_width(), kp.getStar_settings());
                                                        }
                                                    }, kms);
                                                }
                                                kms = kms + (totlist.get(k) * 1000);
                                                k++;
                                            }
                                        } else if (M.isKitchenPrinter(context) && KitchenGlobals.deviceType != 0)
                                            kitchenreciept(null, null, KitchenGlobals.deviceType, true, M.isadvanceprint(M.key_kitchen, context), M.retriveVal(M.key_kitchen_width, context), null);
                                        if (M.isKOTAlert(context)) {
                                            new Handler().postDelayed(new Runnable() {
                                                public void run() {
                                                    new AlertDialog.Builder(context)
                                                            .setTitle(R.string.other_kitchen_receipt)
                                                            .setCancelable(false)
                                                            .setMessage(context.getString(R.string.you_want_other_kitchen_receipt))
                                                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                                                @Override
                                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                                    Toast.makeText(context, R.string.order_place_success, Toast.LENGTH_SHORT).show();
                                                                    onBackPressed();
                                                                }
                                                            })
                                                            .setPositiveButton(android.R.string.yes,
                                                                    new DialogInterface.OnClickListener() {

                                                                        public void onClick(DialogInterface arg0, int arg1) {
                                                                            if (M.isKitchenCatPrinter(context) && kplist != null && kplist.size() > 0) {
                                                                                int k = 0, kms = 0;
                                                                                for (final KitchenPrinterPojo kp : kplist) {
                                                                                    if (kp.getDeviceType() != null && kp.getDeviceType().trim().length() > 0 && !kp.getDeviceType().equals("0")) {
                                                                                        new Handler().postDelayed(new Runnable() {
                                                                                            public void run() {
                                                                                                kitchenreciept(kp.getCategoryid(), kp.getId(), Integer.parseInt(kp.getDeviceType()), true, kp.isAdv(), kp.getPaper_width(), kp.getStar_settings());
                                                                                            }
                                                                                        }, kms);
                                                                                    }
                                                                                    kms = kms + (totlist.get(k) * 1000);
                                                                                    k++;
                                                                                }
                                                                            } else if (M.isKitchenPrinter(context) && KitchenGlobals.deviceType != 0)
                                                                                kitchenreciept(null, null, KitchenGlobals.deviceType, true, M.isadvanceprint(M.key_kitchen, context), M.retriveVal(M.key_kitchen_width, context), null);
                                                                            new Handler().postDelayed(new Runnable() {
                                                                                public void run() {
                                                                                    Toast.makeText(context, R.string.order_place_success, Toast.LENGTH_SHORT).show();
                                                                                    onBackPressed();
                                                                                }
                                                                            }, kplist.size() * 1000);
                                                                        }
                                                                    }).create().show();
                                                }
                                            }, kplist.size() * 1000);
                                        } else {
                                            Toast.makeText(context, R.string.order_place_success, Toast.LENGTH_SHORT).show();
                                            new Handler().postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    onBackPressed();
                                                }
                                            }, 2000);
                                        }
                                    }
                                }
                            }
                            else if (msg_key.equals(WifiHelper.api_cancel_order_item_response)) {
                                String odid = jsonObject.getString("orderdetail_id");
                                if (odlist.contains(odid)) {
                                    int pos = odlist.indexOf(odid);
                                    if (pos < odlist.size() && pos < AppConst.selidlist.size() && pos < AppConst.dishorederlist.size()) {
                                        AppConst.selidlist.remove(pos);
                                        AppConst.dishorederlist.remove(pos);
                                        odlist.remove(pos);
                                        tableItemAdapter.notifyDataSetChanged();
                                        cal();

                                        if (AppConst.selidlist.size() == 0) {
                                            btnfinish.setVisibility(View.GONE);
                                            btncanceltbl.setVisibility(View.VISIBLE);
                                        }
                                    }
                                }
                            } else if (msg_key.equals(WifiHelper.api_cancel_order_response)) {
                                String oid = jsonObject.getString("order_id");
                                if (orderid.equals(oid)) {
                                    ishome=true;
                                    onBackPressed();
                                }
                            } else if (msg_key.equals(WifiHelper.api_update_discount_offer_response)) {
                                String oid = jsonObject.getString("order_id");
                                if (orderid.equals(oid)) {
                                    orderData.setOfferid(jsonObject.getString("offer_id"));
                                    orderData.setOrder_discount(jsonObject.getString("disc_json"));
                                    if(jsonObject.has("service_charge"))
                                        orderData.setService_charge(jsonObject.getString("service_charge"));
                                    //  Log.d(TAG,"sc rec:"+orderData.getService_charge());
                                }
                            } else if (msg_key.equals(WifiHelper.api_finish_order_response)) {
                                String oid = jsonObject.getString("order_id");
                                if (orderid.equals(oid)) {
                                    M.hideLoadingDialog();
                                    successscreen();
                                }
                            }else if(msg_key.equals(WifiHelper.api_update_item_status_response) && jsonObject.getString("success").equals(WifiHelper.TXT_SUCCESS)){
                                JSONObject resObj=new JSONObject(jsonObject.getString("msg"));
                                if(orderid.equals(resObj.getString("order_id"))){
                                    DishOrderPojo data=AppConst.dishorederlist.get(resObj.getInt("position"));
                                    if(data.getOrderdetailid().equals(resObj.get("id"))) {
                                        data.setStatus(resObj.getString("status"));
                                        AppConst.dishorederlist.set(resObj.getInt("position"), data);
                                        EventBus.getDefault().post("updatetableorder");
                                    }
                                }
                            }else if(msg_key.equals(WifiHelper.api_update_payment_trans_response) && jsonObject.getString("success").equals(WifiHelper.TXT_SUCCESS)){
                                if(jsonObject.has("order_id") && jsonObject.has("trans_json")) {
                                    if (orderid.equals(jsonObject.getString("order_id"))) {
                                        orderData.setTransjson(jsonObject.getString("trans_json"));
                                    }
                                }
                            }
                        }
                    } catch (JSONException e) {
                        Log.d(TAG, "error:" + e.getMessage());
                    }
                }
            }
        }
    };

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            POServerService.LocalBinder binder = (POServerService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
            M.hideLoadingDialog();
            getDetail();
            if(jdialog!=null && jdialog.isShowing())
                jdialog.dismiss();
            if(!thread.isAlive())
                thread.start();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
            M.hideLoadingDialog();
            if(thread.isAlive())
                thread.interrupt();
        }
    };

    Thread thread = new Thread() {

        @Override
        public void run() {
            try {
                while (!thread.isInterrupted()) {
                    Thread.sleep(refreshtime);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String lasttime=M.retriveVal(M.key_server_lasttime,context);
                            if(lasttime!=null && !lasttime.isEmpty()){
                                long diffTm=new Date().getTime()-Long.parseLong(lasttime);
                                if(diffTm>60*1000){
                                    joinDialog();
                                }
                            }
                        }
                    });
                }
            } catch (InterruptedException e) {
            }
        }
    };

    private void getDetail(){
        try {
            JSONObject apiObj = new JSONObject(),paramObj=new JSONObject();
            paramObj.put("userid", M.getWaiterid(context));
            paramObj.put("orderid", orderid);

            apiObj.put("key",WifiHelper.api_get_order_detail);
            apiObj.put("msg",paramObj+"");
            apiObj.put("user_ip",getLocalIp());
            sendToServer(apiObj+"");

        }catch (JSONException e){
            Log.d(TAG,"wifi error:"+e.getMessage());
        }
    }

    private void getOrderData(String data) {
        if(AppConst.dishorederlist==null)
            AppConst.dishorederlist=new ArrayList<>();
        AppConst.dishorederlist.clear();
        if(AppConst.selidlist==null)
            AppConst.selidlist=new ArrayList<>();
        AppConst.selidlist.clear();
        if(AppConst.placelist==null)
            AppConst.placelist=new ArrayList<>();
        AppConst.placelist.clear();
        odlist.clear();
        if(data!=null && data.trim().length()>0){
            try {
                JSONArray resArray=new JSONArray(data);
                if(resArray!=null && resArray.length()>0){
                    for(int i=0;i<resArray.length();i++){
                        JSONObject j=resArray.getJSONObject(i);

                        String dishid=j.getString("dish_id");
                        DishOrderPojo dishOrderPojo = new DishOrderPojo();
                        dishOrderPojo.setDishid(dishid);
                        dishOrderPojo.setDishname(j.getString("dish_name"));

                        dishOrderPojo.setPrice(j.getString("price"));
                        dishOrderPojo.setPriceperdish(j.getString("priceperdish"));
                        dishOrderPojo.setPrice_without_tax(j.getString("price_without_tax"));
                        dishOrderPojo.setPriceper_without_tax(j.getString("priceper_without_tax"));

                        dishOrderPojo.setTot_tax(j.getString("tot_tax"));
                        dishOrderPojo.setCombo_flag(j.getString("combo_flag"));
                        dishOrderPojo.setStatus(j.getString("status"));
                        dishOrderPojo.setSold_by(j.getString("sold_by"));
                        dishOrderPojo.setQty(j.getString("qty"));
                        dishOrderPojo.setPrenm(j.getString("pre_nm"));
                        dishOrderPojo.setPrefid(j.getString("pre_id"));
                        if(j.has("tot_disc"))
                            dishOrderPojo.setTot_disc(j.getString("tot_disc"));
                        dishOrderPojo.setTax_amt(j.getString("tax_amt"));
                        dishOrderPojo.setPreflag(j.getString("pref_flag"));
                        if(j.has("comment")) {
                            dishOrderPojo.setComment(j.getString("comment"));
                            dishOrderPojo.setDish_comment(j.getString("comment"));
                        }
                        dishOrderPojo.setOrderdetailid(j.getString("orderdetail_id"));
                        odlist.add(dishOrderPojo.getOrderdetailid());
                        dishOrderPojo.setIsnew(false);

                        if(!dishOrderPojo.getSold_by().equals("each")) {
                            dishOrderPojo.setWeight(j.getString("weight"));
                            dishOrderPojo.setUsed_unit_name(j.getString("used_unit_name"));
                            dishOrderPojo.setUsed_unit_id(j.getString("used_unit_id"));
                            dishOrderPojo.setUnitname(j.getString("unitname"));
                            dishOrderPojo.setUnitid(j.getString("unitid"));
                            dishOrderPojo.setSort_nm(j.getString("sort_nm"));
                            dishOrderPojo.setPurchased_unit_name(j.getString("purchased_unit_name"));
                            dishOrderPojo.setPurchased_unit_id(j.getString("purchased_unit_id"));
                        }
                        if(j.has("offer"))
                            dishOrderPojo.setOffer(j.getString("offer"));
                        else if (dishOrderPojo.getTot_disc()!= null && dishOrderPojo.getTot_disc().trim().length() > 0
                                && Double.parseDouble(dishOrderPojo.getTot_disc()) > 0) {
                            if(dishOrderPojo.getOffer()!=null && dishOrderPojo.getOffer().trim().length()>0){}
                            else{
                                Double dis_per=(Double.parseDouble(dishOrderPojo.getTot_disc())*100)/Double.parseDouble(dishOrderPojo.getPrice());
                                dishOrderPojo.setDis_per(pf.setFormat(dis_per+""));
                            }
                        }

                        dishOrderPojo.setCusineid(j.getString("cuisine_id"));

                        ArrayList<TaxData> tlist=new ArrayList<>();
                        if(j.has("tax")) {
                            JSONObject taxObj=new JSONObject(j.getString("tax"));
                            if (taxObj.has("taxdata")) {
                                JSONArray j1 = new JSONArray(taxObj.getString("taxdata"));
                                for (int i1 = 0; i1 < j1.length(); i1++) {
                                    JSONObject a = j1.getJSONObject(i1);
                                    TaxData td = new TaxData();
                                    td.setId(a.getString("id"));
                                    td.setText(a.getString("txt"));
                                    td.setValue(a.getString("val"));
                                    td.setTax_amount(a.getString("tax_amt"));
                                    td.setTax_value(a.getString("tax_val"));
                                    tlist.add(td);
                                }
                                dishOrderPojo.setTax_data(tlist);
                            }
                        }
                        if(j.has("var_array")){
                            JSONArray jat = new JSONArray(j.getString("var_array"));
                            if(jat!=null && jat.length()>0){
                                List<VarPojo> varList=new ArrayList<>();
                                for(int i1=0;i1<jat.length();i1++){
                                    JSONObject ja1=jat.getJSONObject(i1);
                                    VarPojo v=new VarPojo();
                                    v.setId(ja1.getString("id"));
                                    v.setName(ja1.getString("name"));
                                    v.setAmount(ja1.getString("amount"));
                                    v.setAmount_wt(ja1.getString("amount_wt"));
                                    if(ja1.has("quantity"))
                                        v.setQuantity(ja1.getString("quantity"));
                                    else
                                        v.setQuantity("1");
                                    varList.add(v);
                                }
                                dishOrderPojo.setVarPojoList(varList);
                            }
                        }
                        dishOrderPojo.setTm(new Date().getTime()+"");
                        AppConst.dishorederlist.add(dishOrderPojo);
                        AppConst.selidlist.add(Integer.parseInt(dishid));
                    }
                    if(AppConst.dishorederlist!=null && AppConst.dishorederlist.size()>0){
                        btncanceltbl.setVisibility(View.GONE);
                        btnfinish.setVisibility(View.VISIBLE);
                    }else{
                        btncanceltbl.setVisibility(View.VISIBLE);
                        btnfinish.setVisibility(View.GONE);
                    }
                    cal();
                }
            } catch (JSONException e) {
                Log.d(TAG,"wifi error:"+e.getMessage());
                e.printStackTrace();
            }
        }
    }

    private void setMenuUI(){
        if(M.getMenuUI(context)==null){
            new MenuUI(context,null);
        }else if(M.getMenuUI(context).equals(MenuUI.ui_vr)){
            //vertical
            llvr.setVisibility(View.VISIBLE);
            llhr.setVisibility(View.GONE);
            pager=vr_pager;
            getCuisine();
        }else if(M.getMenuUI(context).equals(MenuUI.ui_hr)){
            //horizontal
            llvr.setVisibility(View.GONE);
            llhr.setVisibility(View.VISIBLE);
            pager=hr_pager;
            getCuisine();
        }else{
            //horizontal
            llvr.setVisibility(View.GONE);
            llhr.setVisibility(View.VISIBLE);
            pager=hr_pager;
            getCuisine();
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @SuppressWarnings("unused")
    public void onEventMainThread(String pusher) {
        if(pusher.equals("updatetableorder")) {
            tableItemAdapter.notifyDataSetChanged();
            cal();
        }else if(pusher.equals("resetorder")){
            btncanceltbl.performClick();
        }else if(pusher.equals("updateSorting"))
            getCuisine();
        else if(pusher.equals("closescreen")){
            onBackPressed();
        }else if(pusher.startsWith(WifiHelper.api_update_item_status)){
            try {
                String req = pusher.replaceFirst(WifiHelper.api_update_item_status, "");
                JSONObject reqObj = new JSONObject(),req1=new JSONObject(req);
                req1.put("order_id",orderid);
                reqObj.put("key", WifiHelper.api_update_item_status);
                reqObj.put("msg", req1+"");
                reqObj.put("user_ip", getLocalIp());
                sendToServer(reqObj + "");
            }catch (JSONException e){}
        }
    }

    private void cal(){
//        llstd.setVisibility(View.VISIBLE);
//        lltax.setVisibility(View.VISIBLE);
//        llitemdisc.setVisibility(View.VISIBLE);
        offer_id=null;offer_name=null;
        newitems=0;
        try {
            if (taxInvoiceList == null)
                taxInvoiceList = new ArrayList<>();
            if (taxidlist == null)
                taxidlist = new ArrayList<>();
            taxInvoiceList.clear();
            taxidlist.clear();
            subtotal = 0;
            granttodal = 0;
            double totamt = 0f, grand = 0,discountamt=0,totqty=0;
            if (AppConst.dishorederlist != null && AppConst.dishorederlist.size() > 0) {
                int dpos = 0;
                for (DishOrderPojo pojo : AppConst.dishorederlist) {
                    double tax_tot = 0;
                    List<TaxData> tlist = pojo.getTax_data();
                    tax_tot = Double.parseDouble(pojo.getTot_tax());
                    double item_dis_per=0;
                    if(pojo.getOffer()!=null && pojo.getOffer().trim().length()>0){//!action.equals("delivery") &&
                        JSONObject jdisc=new JSONObject(pojo.getOffer());
                        if(jdisc.has("discount_type")){
                            if(jdisc.getString("discount_type").equals("percentage") && jdisc.has("discount_percentage")){
                                String dp=jdisc.getString("discount_percentage");
                                if(dp!=null && dp.trim().length()>0)
                                    item_dis_per=Double.parseDouble(dp);
                            }
                        }
                    }else if(pojo.getDis_per()!=null && pojo.getDis_per().trim().length()>0 && Double.parseDouble(pojo.getDis_per())>0){
                        item_dis_per=Double.parseDouble(pojo.getDis_per());
                    }
                    long qty = Long.parseLong(pojo.getQty());
                    totqty=totqty+qty;
                    double price = Double.parseDouble(pojo.getPrice());
                    double pricewtax;
                    if (pojo.getPrice_without_tax() != null && pojo.getPrice_without_tax().trim().length() > 0)
                        pricewtax = Double.parseDouble(pojo.getPrice_without_tax());
                    else {
                        pricewtax = price;
                        tlist = null;
                    }
                    String tt=pojo.getTax_amt();
                    if(tt==null || tt.trim().length()==0)
                        tt="0";
                    double diff = qty * (Double.parseDouble(tt)),tax_dis_amt=0;
                    if(item_dis_per>0 && diff>0){
                        tax_dis_amt=(item_dis_per * diff) / 100f;
                        diff=diff-tax_dis_amt;
                    }
                    double amt = qty * price;
                    totamt = totamt + amt;
                    if(!pojo.isnew()) {
                        granttodal = granttodal + price;
                    }else {
                        granttodal = granttodal + (qty * price);
                        newitems=newitems+1;
                    }
                    if (tlist != null && tlist.size() > 0) {
                        if(!pojo.isnew())
                            subtotal=subtotal+pricewtax;
                        else
                            subtotal=subtotal+(qty*pricewtax);
                        if(tax_dis_amt>0)
                            subtotal = subtotal +tax_dis_amt;
                        int tpos = 0;
                        for (TaxData tax : tlist) {
                            TaxInvoicePojo tp = new TaxInvoicePojo();
                            double d = (diff * Double.parseDouble(tax.getValue())) / tax_tot;
                            if (taxidlist.contains(tax.getId())) {
                                int p = taxidlist.indexOf(tax.getId());
                                tp = taxInvoiceList.get(p);
                                double pt = Double.parseDouble(tp.getPer_total()) + Double.parseDouble(tax
                                        .getValue());
                                tp.setPer_total(pt + "");
                                double tdiff = Double.parseDouble(tp.getAmount_total()) + d;
                                tax.setTax_value(d + "");
                                tp.setAmount_total(pf.setFormat(tdiff+ ""));
                                taxInvoiceList.set(p, tp);
                            } else {
                                tp.setId(tax.getId());
                                tp.setName(tax.getText());
                                tp.setType(tax.getTax_amount());
                                tp.setPer(tax.getValue());
                                tp.setPer_total(tax.getValue());
                                tax.setTax_value(d + "");
                                tp.setAmount_total(pf.setFormat(d+""));
                                taxInvoiceList.add(tp);
                                taxidlist.add(tax.getId());
                            }
                            tlist.set(tpos, tax);
                            tpos++;
                        }
                        DishOrderPojo dishOrderPojo = AppConst.dishorederlist.get(dpos);
                        AppConst.dishorederlist.set(dpos, dishOrderPojo);
                    } else {
                        if(!pojo.isnew())
                            subtotal=subtotal+price;
                        else
                            subtotal=subtotal+(qty*price);
                    }
                    if(pojo.getTot_disc()!=null && pojo.getTot_disc().trim().length()>0 &&
                            Double.parseDouble(pojo.getTot_disc())>0) {
                        discountamt = discountamt +Double.parseDouble(pojo.getTot_disc());
                    }
                    if (pojo.getOffer() != null && pojo.getOffer().trim().length() > 0) {
                        JSONObject jOffer = new JSONObject(pojo.getOffer());
                        if (offer_id != null && offer_id.trim().length() > 0) {
                            offer_id = offer_id + "," + jOffer.getString("id");
                            offer_name = offer_name + "," + jOffer.getString("name");
                        } else {
                            offer_id = jOffer.getString("id");
                            offer_name = jOffer.getString("name");
                        }
                    }
                    dpos++;
                }

            }
            int t = 1;
            lltax.removeAllViews();
            JSONArray jtax=new JSONArray();
            if (taxInvoiceList != null && taxInvoiceList.size() > 0) {
                for (TaxInvoicePojo p : taxInvoiceList) {
                    View v = LayoutInflater.from(context).inflate(R.layout.tax_row, null);
                    LinearLayout ll = (LinearLayout) v.findViewById(R.id.ll);
                    ll.setId(t + 100);
                    TextView tvtxt = (TextView) v.findViewById(R.id.tvtext);
                    TextView tvval = (TextView) v.findViewById(R.id.tvvalue);
                    tvtxt.setTypeface(AppConst.font_regular(context));
                    tvtxt.setTextColor(context.getResources().getColor(R.color.medium_grey));
                    tvtxt.setTextSize(14f);
                    tvval.setTypeface(AppConst.font_regular(context));
                    tvval.setTextColor(context.getResources().getColor(R.color.medium_grey));
                    tvval.setTextSize(14f);
                    tvtxt.setText(p.getName() + "(" + p.getPer() + "%)");
                    tvval.setText(getroundednumber(Double.parseDouble(p.getAmount_total())) + "");
                    lltax.addView(ll);
                }
            }
            if(discountamt>0) {
                // llitemdisc.setVisibility(View.VISIBLE);
                if (discountamt > granttodal){
                    discountamt=granttodal;
                    granttodal = 0;
                }else
                    granttodal=granttodal-discountamt;
                tvitemdisc.setText("-"+AppConst.currency + " " + pf.setFormat(discountamt + ""));
                tvitemdisc.setTag(pf.setFormat(discountamt+""));
                setitemOffer=true;
            }else{
                tvitemdisc.setTag("0");
                llitemdisc.setVisibility(View.GONE);
                setitemOffer=false;
            }
            granttodal = getroundedtotal(granttodal);
            subtotal = getroundednumber(subtotal);
            tvsub.setText(AppConst.currency + " " + pf.setFormat(subtotal + ""));//totamt
            tvsub.setTag(pf.setFormat(subtotal + ""));
            llstd.setTag(pf.setFormat(subtotal+""));
            tvgrand.setText(AppConst.currency + " " + pf.setFormat(granttodal + ""));
            tvgrand.setTag(pf.setFormat(granttodal + "") + "");
            rvorders.scrollToPosition(AppConst.dishorederlist.size() - 1);
        }catch (JSONException e){}
    }

    private void dialog_cal(){

        dtvamt.setText(":"+AppConst.currency + " " + tvsub.getTag());
        dtvamt.setTag(tvsub.getTag()+"");
        llstd.setTag(tvsub.getTag()+"");
        if(dis_amt>0){
            double g=getroundedtotal(Double.parseDouble(tvgrand.getTag().toString())-dis_amt);
            dtvgrand.setText(AppConst.currency + " " + pf.setFormat(g+""));
            dtvgrand.setTag(pf.setFormat(g+""));
        }else {
            dtvgrand.setText(AppConst.currency + " " + pf.setFormat(tvgrand.getTag()+""));
            dtvgrand.setTag(pf.setFormat(tvgrand.getTag()+""));
        }
        rvorders.scrollToPosition(AppConst.dishorederlist.size()-1);
    }

    private void getCuisine() {
        if(cuisinelist==null)
            cuisinelist=new ArrayList<>();
        if(cuisinelist!=null && cuisinelist.size()>0)
            cuisinelist.clear();
        DBCusines dbCusines=new DBCusines(context);
        List<CuisineListPojo> cuilist=dbCusines.getcusines(context);
        if(cuilist!=null && cuilist.size()>0){
            cuisinelist.addAll(cuilist);

            llnodata.setVisibility(View.GONE);
            lldata.setVisibility(View.VISIBLE);
            llgrand.setVisibility(View.VISIBLE);
            adapter = new PagerAdapter(getSupportFragmentManager());
            pager.setAdapter(adapter);
            if (llhr.getVisibility() == View.VISIBLE) {
                tabs_hr.setViewPager(pager);
            } else {
                tabs_vr.setViewPager(pager);
            }
            final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2, getResources()
                    .getDisplayMetrics());
            pager.setPageMargin(pageMargin);
            pager.setCurrentItem(currpos);

        }else if(connectionDetector.isConnectingToInternet()) {
            M.showLoadingDialog(context);
            CuisineDishAPI mAuthenticationAPI = APIServiceHeader.createService(context, CuisineDishAPI.class);
            Call<List<CuisineListPojo>> call = mAuthenticationAPI.getCuisines(restaurantid, runiqueid);
            call.enqueue(new retrofit2.Callback<List<CuisineListPojo>>() {
                @Override
                public void onResponse(Call<List<CuisineListPojo>> call, Response<List<CuisineListPojo>> response) {
                    M.hideLoadingDialog();
                    if (response.isSuccessful()) {
                        List<CuisineListPojo> custCuisineListPojos = response.body();
                        if (custCuisineListPojos != null) {
                            if (custCuisineListPojos.size() > 0) {
                                cuisinelist.addAll(custCuisineListPojos);

                                llnodata.setVisibility(View.GONE);
                                lldata.setVisibility(View.VISIBLE);
                                llgrand.setVisibility(View.VISIBLE);

                                adapter = new PagerAdapter(getSupportFragmentManager());
                                pager.setAdapter(adapter);
                                if (llhr.getVisibility() == View.VISIBLE) {
                                    tabs_hr.setViewPager(pager);
                                } else {
                                    tabs_vr.setViewPager(pager);
                                }
                                final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2, getResources()
                                        .getDisplayMetrics());
                                pager.setPageMargin(pageMargin);
                                pager.setCurrentItem(currpos);

                            }else{
                                llnodata.setVisibility(View.VISIBLE);
                                lldata.setVisibility(View.GONE);
                                llgrand.setVisibility(View.GONE);
                            }
                        }
                    } else {
                        int statusCode = response.code();
                        ResponseBody errorBody = response.errorBody();
                        Log.d(TAG, "error:" + statusCode + " " + errorBody);
                    }
                }

                @Override
                public void onFailure(Call<List<CuisineListPojo>> call, Throwable t) {
                    Log.d(TAG, "fail:" + t.getMessage());
                    M.hideLoadingDialog();
                }
            });
        }else{
            llnodata.setVisibility(View.VISIBLE);
            lldata.setVisibility(View.GONE);
            llgrand.setVisibility(View.GONE);
        }
    }

    private void showFullOrder() {
        orderData.setTip("");
        dis_amt=0;
        final Dialog d=new Dialog(context);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setCancelable(false);
        d.setContentView(R.layout.dialog_show_order);
        d.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT);
        TextView tvtbl=(TextView)d.findViewById(R.id.tvtablenm);
        tvg=(TextView)d.findViewById(R.id.tv_grand);
        tvd=(TextView)d.findViewById(R.id.tv_discount);
        TextView tvclose=(TextView)d.findViewById(R.id.tvclose);
        tvtbl.setText(tblnm);
        RecyclerView rv=(RecyclerView)d.findViewById(R.id.rvitems);
        rv.setLayoutManager(new LinearLayoutManager(context));
        rv.setHasFixedSize(true);
        Button btncan=(Button)d.findViewById(R.id.btncancel);
        btncan.setTypeface(AppConst.font_medium(context));
        Button btnprint=(Button)d.findViewById(R.id.btnprint);
        btnprint.setTypeface(AppConst.font_medium(context));
        Button btnpay=(Button)d.findViewById(R.id.btnpay);
        btnpay.setTypeface(AppConst.font_medium(context));
        Button btndeldisc=(Button)d.findViewById(R.id.btnremovediscount);
        btndeldisc.setTypeface(AppConst.font_medium(context));
        btnphpestatus=(Button)d.findViewById(R.id.btnphpestatus);
        btnphpestatus.setTypeface(AppConst.font_medium(context));
        btnphpestatus.setVisibility(View.GONE);
        chipdiscount = (ChipCloud) d.findViewById(R.id.chip_cloud_discount);
        cc_offer=(ChipCloud)d.findViewById(R.id.chip_cloud_offer);
        cc_service_charge=(ChipCloud)d.findViewById(R.id.cc_service_charge);
        lldiscount=(LinearLayout)d.findViewById(R.id.lldiscount);
        final LinearLayout lldiscper=(LinearLayout)d.findViewById(R.id.lldiscper);
        lloffer=(LinearLayout)d.findViewById(R.id.lloffer);
        rbper=(RadioButton)d.findViewById(R.id.rbper);
        rbper.setTypeface(AppConst.font_regular(context));
        rbamt=(RadioButton)d.findViewById(R.id.rbamount);
        rbamt.setTypeface(AppConst.font_regular(context));
        etdiscount=(EditText)d.findViewById(R.id.etdis);
        etdiscount.setTypeface(AppConst.font_regular(context));
        etdiscount.setVisibility(View.GONE);
        etdisamt=(EditText)d.findViewById(R.id.etdisamt);
        etdisamt.setTypeface(AppConst.font_regular(context));
        etservicecharge=(EditText)d.findViewById(R.id.etservice);
        etservicecharge.setVisibility(View.GONE);
        TextView txtSC=(TextView)d.findViewById(R.id.txtSC);
        txtSC.setText(M.retriveVal(M.key_custom_service_charge,context));
        rbscper=(RadioButton)d.findViewById(R.id.rbSCper);
        rbscper.setTypeface(AppConst.font_regular(context));
        rbscamt=(RadioButton)d.findViewById(R.id.rbSCamount);
        rbscamt.setTypeface(AppConst.font_regular(context));
        etscamt=(EditText)d.findViewById(R.id.etserviceAmt);
        etscamt.setTypeface(AppConst.font_regular(context));
        tvsercharge=(TextView)d.findViewById(R.id.tvcharge);
        tvsercharge.setText(":"+ AppConst.currency+pf.setFormat("0"));
        tvsercharge.setTag("0");
        tvrounding=(TextView)d.findViewById(R.id.tvrounding);
        tvrounding.setTag("0");
        llservice=(LinearLayout)d.findViewById(R.id.llservice);
        llservice.setVisibility(View.GONE);
        llserper=(LinearLayout)d.findViewById(R.id.llSCper);
        llserper.setTag("0");
        llserper.setVisibility(View.GONE);
        if(dis_amt>0){
            lldiscount.setVisibility(View.GONE);
            if(orderData.getOfferid()!=null && orderData.getOfferid().trim().length()>0)
                btndeldisc.setVisibility(View.GONE);
            else
                btndeldisc.setVisibility(View.VISIBLE);
        }else {
            lldiscount.setVisibility(View.VISIBLE);
            btndeldisc.setVisibility(View.GONE);
        }
        if(M.isServiceCharge(context)) {
            if(orderData.getService_charge()!=null && !orderData.getService_charge().isEmpty()) {
                Double samt = Double.parseDouble(orderData.getService_charge());
                Double sper = (samt * 100) / Double.parseDouble(tvgrand.getTag().toString());
                //Log.d(TAG, orderData.getService_charge() + " " + sper + "%");
                etservicecharge.setText(pf.setFormat(sper + ""));
                etservicecharge.setSelection(etservicecharge.getText().length());
            }
            llservice.setVisibility(View.VISIBLE);
            etscamt.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if(rbscamt.isChecked()) {
                        setDiscount(0.0,"showorder");
                    }
                }
            });
            etservicecharge.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if(rbscper.isChecked())
                        setDiscount(0.0,"showorder");
                }
            });
        }
        if(orderData.getTransjson()!=null && !orderData.getTransjson().isEmpty())
            btnphpestatus.setVisibility(View.VISIBLE);
        setDiscount(0.0,"showorder");
        ItemListAdapter itemListAdapter=new ItemListAdapter(context,AppConst.dishorederlist);
        rv.setAdapter(itemListAdapter);
        rbamt.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b) {
                    lldiscper.setVisibility(View.GONE);
                    etdisamt.setVisibility(View.VISIBLE);
                    etdisamt.setText(dis_amt+"");
                    etdisamt.setSelection(etdisamt.getText().length());
                }
            }
        });

        rbper.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b) {
                    chipdiscount.chipSelected(0);
                    lldiscper.setVisibility(View.VISIBLE);
                    etdisamt.setVisibility(View.GONE);
                    chipdiscount.chipDeselected(0);
                }
            }
        });

        rbscamt.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(rbscamt.isChecked()) {
                    if(tvsercharge.getTag()!=null)
                        etscamt.setText(tvsercharge.getTag().toString());
                    etscamt.setVisibility(View.VISIBLE);
                    setDiscount(0.0,"showorder");
                }else {
                    etscamt.setVisibility(View.GONE);
                }
            }
        });

        rbscper.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(rbscper.isChecked()) {
                    if(cc_service_charge.getTag()!=null)
                        cc_service_charge.setSelectedChip(Integer.parseInt(cc_service_charge.getTag().toString()));
                    llserper.setVisibility(View.VISIBLE);
                    setDiscount(0.0,"showorder");
                }else {
                    llserper.setVisibility(View.GONE);
                }
            }
        });

        btncan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                d.dismiss();
            }
        });

        btnprint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    List<String> olist=new ArrayList<>();
                    if (discountPojo != null) {
                        if (offer_id != null && offer_id.trim().length() > 0) {
                            olist=Arrays.asList(offer_id.split(","));
                        }

                        if(olist!=null && olist.size()>0){
                            if(!olist.contains(discountPojo.getId())) {
                                offer_id = offer_id + "," + discountPojo.getId();
                                offer_name = offer_name + "," + discountPojo.getName();
                            }
                        } else {
                            offer_id = discountPojo.getId();
                            offer_name = discountPojo.getName();
                        }
                    }
                    if (tvd.getTag() != null && tvd.getTag().toString().trim().length() > 0) {
                        Double finaldisc = Double.parseDouble(tvd.getTag() + "");
                        JSONObject j = new JSONObject();
                        j.put("discount_amount", finaldisc + "");
                        j.put("discount_per", dis_per + "");
                        if (discountPojo != null)
                            j.put("discount_type", discountPojo.getDiscount_type());
                        updateDiscount(j + "", true);
                    }else {
                        checkOnlinePay();
                    }
                }catch (JSONException e){}
            }
        });

        btnpay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(M.isCashierPermission(context)){
                    payClick(d);
                }else{
                    final Dialog pinDialog=new Dialog(context, android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen);
                    pinDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    pinDialog.setContentView(R.layout.dialog_waiter_password);
                    pinDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                    TextView tv=(TextView)pinDialog.findViewById(R.id.tv);
                    tv.setText(R.string.type_outlet_pin);
                    PassCodeView circlePinField=(PassCodeView)pinDialog.findViewById(R.id.type_pin);
                    circlePinField.setOnTextChangeListener(new PassCodeView.TextChangeListener() {
                        @Override
                        public void onTextChanged(String pin) {
                            if(pin.length()==4) {
                                if (pin.equalsIgnoreCase(M.getOutletPin(context))) {
                                    pinDialog.dismiss();
                                    payClick(d);
                                } else
                                    Toast.makeText(context, R.string.invalid_pwd, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                    pinDialog.show();
                }
            }
        });

        btnphpestatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tid="";
                try{
                    JSONObject transJSON=new JSONObject(orderData.getTransjson());
                    tid=transJSON.getString("trans_id");
                }catch (JSONException e){}
                if(connectionDetector.isConnectingToInternet()){
                    M.showLoadingDialog(context);
                    OrderAPI mAuthenticationAPI = APIServiceHeader.createService(context, OrderAPI.class);
                    Call<JsonObject> call = mAuthenticationAPI.checkPhonePeStatus(M.getRestID(context),M.getRestUniqueID(context),tid);
                    call.enqueue(new retrofit2.Callback<JsonObject>(){
                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                            M.hideLoadingDialog();
                            JsonObject res=response.body();
                            String state="";
                            if(res!=null && res.has("data")){
                                JsonObject dj=res.get("data").getAsJsonObject();
                                if(dj.has("paymentState"))
                                    state=dj.get("paymentState").getAsString();
                            }
                            if(state!=null && res.get("success").getAsBoolean() && state.equalsIgnoreCase("COMPLETED")){
                                try {
                                    applyDiscountOnTax();
                                    dtvgrand=tvg;
                                    Log.d(TAG,dtvgrand.getTag().toString());
                                    JSONObject rJSON = new JSONObject(res.toString());
                                    JSONObject dJSON=rJSON.getJSONObject("data");
                                    rJSON.put("type","phonepe_upi");
                                    if(dJSON!=null && dJSON.has("providerReferenceId"))
                                        rJSON.put("token",dJSON.getString("providerReferenceId"));
                                    transid = rJSON.toString();
                                    qrbitmap=null;
                                    payment_mode=M.retriveVal(M.key_phonepe_paymode_qrcode,context);
                                    DBPaymentType dbPaymentType=new DBPaymentType(context);
                                    payment_mode_text=dbPaymentType.getName(payment_mode);
                                    finishOrder("",orderData.getCust_name(),orderData.getCust_phone(),"","","");
                                    d.dismiss();
                                }catch (JSONException e){}
                            }else if(res.has("message")) {
                                Toast.makeText(context, res.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                                if(res.has("code") && res.get("code").getAsString().equals("PAYMENT_PENDING"))
                                    btnphpestatus.setVisibility(View.VISIBLE);
                                else
                                    btnphpestatus.setVisibility(View.GONE);
                            }
                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {
                            M.hideLoadingDialog();
                        }
                    });
                }else
                    Toast.makeText(context,R.string.no_internet_alert,Toast.LENGTH_SHORT).show();
            }
        });

        etdiscount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                etdiscount.removeTextChangedListener(this);
                if(etdiscount.getText().toString().trim().length()==3){
                    double damt=Double.parseDouble(etdiscount.getText().toString());
                    if(damt>100)
                        etdiscount.setText("100");
                }
                if(etdiscount.getText().toString().trim().length()>0 && !etdiscount.getText().toString().equals(".")) {
                    dis_per=Double.parseDouble(etdiscount.getText().toString());
                    Double samt=Double.parseDouble(tvgrand.getTag().toString());
                    dis_amt = getroundednumber((dis_per * samt) / 100f);
                }else{
                    dis_per=0;
                    dis_amt=0.0;
                }
                setDiscount(0.0,"showorder");
                etdiscount.setSelection(etdiscount.getText().length());
                etdiscount.addTextChangedListener(this);
            }
        });
        etdisamt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                etdisamt.removeTextChangedListener(this);
                if(etdisamt.getText().toString().trim().length()>0 &&
                        !etdisamt.getText().toString().equals(".")){
                    Double samt=Double.parseDouble(tvgrand.getTag().toString());
                    Double damt=Double.parseDouble(etdisamt.getText().toString());
                    if(samt<damt)
                        etdisamt.setText(samt+"");
                }
                if(etdisamt.getText().toString().trim().length()>0 && !etdisamt.getText().toString().equals(".")) {
                    dis_amt=Double.parseDouble(etdisamt.getText().toString());
                    Double samt=Double.parseDouble(tvgrand.getTag().toString());
                    dis_per = getroundednumber((dis_amt * 100) / samt);
                }else{
                    dis_per=0;
                    dis_amt=0.0;
                }
                setDiscount(0.0,"showorder");
                etdisamt.setSelection(etdisamt.getText().length());
                etdisamt.addTextChangedListener(this);
            }
        });

        setDiscountChip();

        tvclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                d.dismiss();
            }
        });
        d.show();
    }

    private void checkOnlinePay(){
        String phonepe_qrcode=M.retriveVal(M.key_phonepe_paymode_qrcode,context);
        String phonpe_status=M.retriveVal(M.key_phonepe_status,context);
        if(phonpe_status!=null && phonpe_status.equals("true") && phonepe_qrcode!=null && !phonepe_qrcode.isEmpty()){
            phonePeAlertDialog=new PhonePeAlertDialog(context, orderData, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(phonePeAlertDialog.cbqrcode.isChecked()) {
                        phonePeAlertDialog.dismiss();
                        if(connectionDetector.isConnectingToInternet()) {
                            String spm = "phonepe";
                            String spt="dynamicqr";
                            String timestamp= AppConst.arabicToEng(new Date().getTime()+"");
                            M.showLoadingDialog(context);
                            OrderAPI mAuthenticationAPI = APIServiceHeader.createService(context, OrderAPI.class);
                            Call<TransactionPojo> call = mAuthenticationAPI.initTransPhonePe(restaurantid, runiqueid, M.getWaiterid(context), order_no, timestamp, tvg.getTag().toString(), spm, spt);
                            call.enqueue(new retrofit2.Callback<TransactionPojo>() {
                                @Override
                                public void onResponse(Call<TransactionPojo> call, Response<TransactionPojo> response) {
                                    M.hideLoadingDialog();
                                    TransactionPojo res = response.body();
                                    if (res != null) {
                                        if (res.getSuccess().equals("true")) {
                                            String txt = res.getData().getQrString();
                                            try {
                                                JSONObject tObj=new JSONObject();

                                                tObj.put("key",WifiHelper.api_update_payment_trans);
                                                tObj.put("order_id",orderid);
                                                tObj.put("trans_id",res.getData().getTransactionId());
                                                tObj.put("merchant_id",res.getData().getMerchantId());
                                                tObj.put("qr_string",res.getData().getQrString());
                                                tObj.put("user_ip",getLocalIp());
                                                sendToServer(tObj+"");
                                                orderData.setTransjson(tObj+"");
                                                qrbitmap = BitmapUtil.generateBitmap(txt, 9, 400, 400);
                                                if (qrbitmap != null) {
//                                                    OnlineTransDialog otd=new OnlineTransDialog(context,null,null);
//                                                    otd.setData(order_no,tvg.getTag().toString(),qrbitmap,res);
//                                                    otd.show();
                                                    if (M.isCashPrinter(context) && Globals.deviceType != 0)
                                                        createReceiptData("print");
                                                    btnphpestatus.setVisibility(View.VISIBLE);
                                                }
                                            } catch (Exception we) {
                                                we.printStackTrace();
                                            }
                                        } else if (res.getMessage() != null && !res.getMessage().isEmpty()) {
                                            Toast.makeText(context, res.getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }

                                @Override
                                public void onFailure(Call<TransactionPojo> call, Throwable t) {
                                    M.hideLoadingDialog();
                                }
                            });
                        }else
                            Toast.makeText(context,R.string.no_internet_alert,Toast.LENGTH_SHORT).show();
                    }else
                        createReceiptData("true");
                }
            });
            phonePeAlertDialog.show();
        }else
            createReceiptData("true");
    }

    private void payClick(Dialog d){
        try {
            List<String> olist=new ArrayList<>();
            if (discountPojo != null) {
                if (offer_id != null && offer_id.trim().length() > 0) {
                    olist=Arrays.asList(offer_id.split(","));
                }

                if(olist!=null && olist.size()>0){
                    if(!olist.contains(discountPojo.getId())) {
                        offer_id = offer_id + "," + discountPojo.getId();
                        offer_name = offer_name + "," + discountPojo.getName();
                    }
                } else {
                    offer_id = discountPojo.getId();
                    offer_name = discountPojo.getName();
                }
            }
            if (tvd.getTag() != null && tvd.getTag().toString().trim().length() > 0) {
                Double finaldisc = Double.parseDouble(tvd.getTag() + "");
                JSONObject j = new JSONObject();
                j.put("discount_amount", finaldisc + "");
                j.put("discount_per", dis_per + "");
                if (discountPojo != null)
                    j.put("discount_type", discountPojo.getDiscount_type());
                updateDiscount(j + "", false);
            }
        }catch (JSONException e){}
        d.dismiss();
        if(M.retriveVal(M.key_tip_cal,context)!=null && M.retriveVal(M.key_tip_cal,context).equals("enable")){
            tipDialog();
        }else
            paymentMode();
    }

    LinearLayout llseltip=null;
    TextView tvseltip = null;
    private void tipDialog(){
        applyDiscountOnTax();
        final String tip_pref=M.retriveVal(M.key_tip_pref,context);
        tipDialog=new Dialog(context);
        tipDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        tipDialog.setContentView(R.layout.dialog_tip);
        tipDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final TextView txtgrand=tipDialog.findViewById(R.id.txtgrand);
        txtgrand.setText(context.getString(R.string.txt_grand_total)+": "+AppConst.currency+pf.setFormat(tvg.getTag().toString()));
        txtgrand.setTag(tvg.getTag().toString());
        final TextView txtpay=tipDialog.findViewById(R.id.txtpay);
        txtpay.setText(AppConst.currency+pf.setFormat(tvg.getTag().toString()));
        txtpay.setTag(tvg.getTag().toString());
        Button btnpay=tipDialog.findViewById(R.id.btnpay);
        btnpay.setTypeface(AppConst.font_medium(context));
        Button btnclose=tipDialog.findViewById(R.id.btnclose);
        btnclose.setTypeface(AppConst.font_medium(context));
        LinearLayout llpretip=tipDialog.findViewById(R.id.llpretip);
        final TextView tvnotip=tipDialog.findViewById(R.id.tvnotip);
        final TextView tvcustomtip=tipDialog.findViewById(R.id.tvcustomtip);
        final TextInputLayout txt=(TextInputLayout)tipDialog.findViewById(R.id.txt);
        txt.setTypeface(AppConst.font_regular(context));
        final EditText ettip=(EditText)tipDialog.findViewById(R.id.ettipamt);
        ettip.setTypeface(AppConst.font_regular(context));
        ettip.setTag("0");

        tvcustomtip.setVisibility(View.VISIBLE);
        tvnotip.setVisibility(View.VISIBLE);
        llpretip.setVisibility(View.VISIBLE);
        String[] perList = new String[]{"10", "15", "20"};
//        Log.d(TAG,"std:"+llstd.getTag().toString());
//        Log.d(TAG,"grand:"+tvg.getTag().toString());
        for (int i = 0; i < perList.length; i++) {
            String p = perList[i];
            View v = LayoutInflater.from(context).inflate(R.layout.tip_row, null);
            final LinearLayout ll = (LinearLayout) v.findViewById(R.id.lltip);
            TextView tvper = (TextView) v.findViewById(R.id.tvper);
            TextView tvamt = (TextView) v.findViewById(R.id.tvamount);
            Double samt = Double.parseDouble(txtgrand.getTag().toString());
            if (tip_pref != null && tip_pref.equals("calculate_tip_before_taxes"))
                samt = Double.parseDouble(llstd.getTag().toString());
            final Double tamt = (Double.parseDouble(p) * samt) / 100f;
            tvper.setText(p + "%");
            tvamt.setText(AppConst.currency + pf.setFormat(tamt + ""));

            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1f);
            if (i != perList.length - 1)
                lp.setMargins(0, 0, 20, 0);
            else
                lp.setMargins(0, 0, 0, 0);
            ll.setLayoutParams(lp);

            ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    txt.setVisibility(View.GONE);
                    if (tvseltip != null)
                        tvseltip.setBackground(context.getResources().getDrawable(R.drawable.layout_inactive));
                    if (llseltip != null)
                        llseltip.setBackground(context.getResources().getDrawable(R.drawable.layout_inactive));
                    tvseltip = null;
                    llseltip = ll;
                    ll.setBackground(context.getResources().getDrawable(R.drawable.layout_active));
                    ettip.setText(tamt + "");
                }
            });
            llpretip.addView(ll);
        }

        ettip.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                ettip.removeTextChangedListener(this);
                Double cal_main_amt = Double.parseDouble(txtgrand.getTag().toString());
                Double tipamt = 0.0;
                if (s.toString().length() > 0) {
                    tipamt = Double.parseDouble(s.toString());
                    if (tip_pref != null && tip_pref.equals("calculate_tip_before_taxes"))
                        cal_main_amt = Double.parseDouble(llstd.getTag().toString());
                }
                if (tipamt != 0) {
                    Double tip_per = (Double.parseDouble(s.toString()) * 100) / cal_main_amt;
                    Double pamt = Double.parseDouble(s.toString()) + Double.parseDouble(txtgrand.getTag().toString());
                    txtpay.setText(AppConst.currency+pf.setFormat(pamt + ""));
                    txtpay.setTag(pamt + "");
                    ettip.setTag(tip_per + "");
                } else {
                    txtpay.setText(AppConst.currency+ pf.setFormat(txtgrand.getTag().toString()));
                    txtpay.setTag(txtgrand.getTag().toString());
                    ettip.setTag("0");
                }
                ettip.addTextChangedListener(this);
            }
        });

        tvnotip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt.setVisibility(View.GONE);
                if (tvseltip != null)
                    tvseltip.setBackground(context.getResources().getDrawable(R.drawable.layout_inactive));
                if (llseltip != null)
                    llseltip.setBackground(context.getResources().getDrawable(R.drawable.layout_inactive));
                tvnotip.setBackground(context.getResources().getDrawable(R.drawable.layout_active));
                tvseltip = tvnotip;
                llseltip = null;
                txtpay.setText(AppConst.currency + pf.setFormat(txtgrand.getTag().toString()));
                txtpay.setTag(txtgrand.getTag().toString());
                ettip.setText("0");
            }
        });

        tvcustomtip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt.setVisibility(View.VISIBLE);
                if (tvseltip != null)
                    tvseltip.setBackground(context.getResources().getDrawable(R.drawable.layout_inactive));
                if (llseltip != null)
                    llseltip.setBackground(context.getResources().getDrawable(R.drawable.layout_inactive));
                tvcustomtip.setBackground(context.getResources().getDrawable(R.drawable.layout_active));
                tvseltip = tvcustomtip;
                llseltip = null;
                ettip.setText("");
            }
        });

        btnpay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tipDialog.getCurrentFocus()!=null) {
                    InputMethodManager inputManager = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(tipDialog.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                }
                String tip_amt="";
                try {
                    Double ta=(ettip.getText().toString().length()!=0)?Double.parseDouble(ettip.getText().toString()):0.0;
                    if(ta!=0) {
                        JSONObject tipJson = new JSONObject();
                        tipJson.put("tip_amount",pf.setFormat(ettip.getText().toString()));
                        tipJson.put("tip_percentage", pf.setFormat(ettip.getTag().toString()));
                        tipJson.put("tip_preference", tip_pref);
                        tip_amt = tipJson + "";
                    }
                }catch (JSONException e){}
                String grand_tot=txtpay.getTag().toString();
                // Log.d(TAG,"SUB: "+sub_tot);
                Log.d(TAG,"GRAND: "+pf.setFormat(grand_tot));
                Log.d(TAG,"TIP: "+tip_amt);
                orderData.setTip(tip_amt);
                tipDialog.dismiss();
                paymentMode();
            }
        });

        btnclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tipDialog.dismiss();
            }
        });
        tipDialog.show();
    }

    private void updateDiscount(String discjson, final Boolean isprint) {
        String sc="";
        if(M.isServiceCharge(context)){
            if(tvsercharge!=null && tvsercharge.getTag()!=null) {
                sc=tvsercharge.getTag().toString();
            }
        }
        try {
            JSONObject disObj=new JSONObject();

            disObj.put("key",WifiHelper.api_update_discount_offer);
            disObj.put("order_id",orderid);
            disObj.put("disc_json",discjson);
            disObj.put("offer_id",offer_id);
            disObj.put("offer_name",offer_name);
            disObj.put("bill_disc_amt",dis_amt);
            disObj.put("service_charge",sc);
            disObj.put("user_ip",getLocalIp());
            sendToServer(disObj+"");
            if (isprint) {
                checkOnlinePay();
            }
        }catch (JSONException e){}
    }

    private void applyDiscountOnTax(){
        taxDisList=new ArrayList<>();
        taxDisList.clear();
        if(dis_per>0 && taxInvoiceList!=null && taxInvoiceList.size()>0) {
            Double tot_tax_dis_amt=0.0;
            for (TaxInvoicePojo t : taxInvoiceList) {
                TaxInvoicePojo tnew=new TaxInvoicePojo();
                tnew.setType(t.getType());
                tnew.setId(t.getId());
                tnew.setName(t.getName());
                tnew.setPer(t.getPer());
                tnew.setPer_total(t.getPer_total());
                double tamt=Double.parseDouble(t.getAmount_total());
                double tax_dis_amt = (dis_per * tamt) / 100f;
                tot_tax_dis_amt=tot_tax_dis_amt+tax_dis_amt;
                tamt = tamt - tax_dis_amt;
                tnew.setAmount_total(pf.setFormat(tamt+""));
                taxDisList.add(tnew);
            }
            Double samt=Double.parseDouble(tvsub.getTag().toString())+tot_tax_dis_amt;
            if(dtvamt!=null)
                dtvamt.setText(":" + AppConst.currency + " " + pf.setFormat(samt + ""));
            llstd.setTag(pf.setFormat(samt + ""));
        }else{
            taxDisList.addAll(taxInvoiceList);
            if(dtvamt!=null)
                dtvamt.setText(":"+AppConst.currency + " " + pf.setFormat(tvsub.getTag().toString()+""));
            llstd.setTag(pf.setFormat(tvsub.getTag().toString()+""));
        }
    }

    private void paymentMode(){
        payment_mode="";
        payment_mode_text="";
        pm_rounding="off";
        discountPojo=null;
        dialog=new Dialog(context,android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT);
        dialog.setContentView(R.layout.dialog_payment_mode);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        btndone=(TextView)dialog.findViewById(R.id.btndone);
        btncancel=(TextView)dialog.findViewById(R.id.ivclose);
        btnpartial=(TextView)dialog.findViewById(R.id.btnpartial);
        btnpartial.setTag("");
        btndone.setOnClickListener(this);
        btncancel.setOnClickListener(this);
        tvgstno=(TextView)dialog.findViewById(R.id.tvgstno);
        tvgstno.setText(getString(R.string.txt_gst_number)+M.getGST(context));
        dtvamt=(TextView)dialog.findViewById(R.id.tvdamt);
        dtvgrand=(TextView)dialog.findViewById(R.id.tvgrand);
        buttonSeven = (Button)dialog.findViewById(R.id.buttonSeven);
        buttonEight= (Button)dialog.findViewById(R.id.buttonEight);
        buttonNine= (Button)dialog.findViewById(R.id.buttonNine);
        buttonFour = (Button)dialog.findViewById(R.id.buttonFour);
        buttonFive= (Button)dialog.findViewById(R.id.buttonFive);
        buttonSix= (Button)dialog.findViewById(R.id.buttonSix);
        buttonOne= (Button)dialog.findViewById(R.id.buttonOne);
        buttonTwo= (Button)dialog.findViewById(R.id.buttonTwo);
        buttonThree = (Button)dialog.findViewById(R.id.buttonThree);
        buttonClear = (Button)dialog.findViewById(R.id.buttonClear);
        buttonZero = (Button)dialog.findViewById(R.id.buttonZero);
        buttonEqual = (Button)dialog.findViewById(R.id.buttonEqual);
        editText=(EditText)dialog.findViewById(R.id.editText);
        editText.setTypeface(AppConst.font_regular(context));
        etpackcharge=(EditText)dialog.findViewById(R.id.etcharge);
        etpackcharge.setTypeface(AppConst.font_regular(context));
        ll_tip=(LinearLayout)dialog.findViewById(R.id.lltip);
        ll_tip.setVisibility(View.GONE);
        llcal=(LinearLayout)dialog.findViewById(R.id.llcal);
        llcal.setVisibility(View.GONE);
        lldiscount=(LinearLayout)dialog.findViewById(R.id.lldiscount);
        lloffer=(LinearLayout)dialog.findViewById(R.id.lloffer);
        lldiscper=(LinearLayout)dialog.findViewById(R.id.lldiscper);
        llamount=(LinearLayout)dialog.findViewById(R.id.llamount);
        llpmode=(LinearLayout)dialog.findViewById(R.id.llpmode);
        llcharge=(LinearLayout)dialog.findViewById(R.id.llpackcharge);
        llrounding=(LinearLayout)dialog.findViewById(R.id.llrounding);
        llservice=(LinearLayout)dialog.findViewById(R.id.llsercharge);
        llservice.setVisibility(View.GONE);
        etdiscount=(EditText)dialog.findViewById(R.id.etdis);
        etdiscount.setTypeface(AppConst.font_regular(context));
        etdiscount.setVisibility(View.GONE);
        etdisamt=(EditText)dialog.findViewById(R.id.etdisamt);
        etdisamt.setTypeface(AppConst.font_regular(context));
        tvtip=(TextView)dialog.findViewById(R.id.tvtip);
        tvdiscount=(TextView)dialog.findViewById(R.id.tvdiscount);
        tvdiscount.setText(":"+AppConst.currency+" 0.00");
        tvrounding=(TextView)dialog.findViewById(R.id.tvrounding);
        tvrounding.setTag("0");
        tvsercharge=(TextView)dialog.findViewById(R.id.tvsercharge);
        TextView txtSC=(TextView)dialog.findViewById(R.id.txtSC);
        txtSC.setText(M.retriveVal(M.key_custom_service_charge,context));
        rbper=(RadioButton)dialog.findViewById(R.id.rbper);
        rbper.setTypeface(AppConst.font_regular(context));
        rbamt=(RadioButton)dialog.findViewById(R.id.rbamount);
        rbamt.setTypeface(AppConst.font_regular(context));
        RecyclerView rvinv=(RecyclerView)dialog.findViewById(R.id.rvinv);
        rvinv.setLayoutManager(new LinearLayoutManager(context));
        rvinv.setHasFixedSize(true);
        txtreturnamt = (TextView)dialog.findViewById(R.id.txtreturnamt);
        txtreturnamt.setText(AppConst.currency +" "+ 0.00);

        dialog_cal();
        getPaymentMode();
        rvinv.setVisibility(View.GONE);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        //   llrounding.setVisibility(View.GONE);

        if(action.equals("dinein")){
            btnpartial.setVisibility(View.GONE);
        }

        setDiscount(Double.parseDouble(etpackcharge.getText().toString()),"paymenttype");
        applyDiscountOnTax();
        rbamt.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b) {
                    lldiscper.setVisibility(View.GONE);
                    etdisamt.setVisibility(View.VISIBLE);
                    etdisamt.setText(dis_amt+"");
                }
                etdisamt.setSelection(etdisamt.getText().length());
            }
        });

        rbper.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b) {
                    lldiscper.setVisibility(View.VISIBLE);
                    etdisamt.setVisibility(View.GONE);
                }
            }
        });
        buttonZero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editText.setText(editText.getText() + "0");
            }
        });

        buttonOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editText.setText(editText.getText() + "1");
            }
        });

        buttonTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editText.setText(editText.getText() + "2");
            }
        });

        buttonThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editText.setText(editText.getText() + "3");
            }
        });

        buttonFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editText.setText(editText.getText() + "4");
            }
        });

        buttonFive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editText.setText(editText.getText() + "5");
            }
        });

        buttonSix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editText.setText(editText.getText() + "6");
            }
        });

        buttonSeven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editText.setText(editText.getText() + "7");
            }
        });

        buttonEight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editText.setText(editText.getText() + "8");
            }
        });

        buttonNine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editText.setText(editText.getText() + "9");
            }
        });



        buttonEqual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

        buttonClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editText.getText().length() > 0) {
                    CharSequence currentText = editText.getText();
                    editText.setText(currentText.subSequence(0, currentText.length()-1));
                }
                else {
                    editText.setText("");
                }
            }
        });
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                Double gm=Double.parseDouble(dtvgrand.getTag().toString());
                if(editText.getText().toString().length()>0 && !editText.getText().toString().equals(".")) {
                    double inputnumber = 0;
                    inputnumber = Double.parseDouble(editText.getText().toString());

                    retamount = inputnumber - gm;

                    txtreturnamt.setText(AppConst.currency +" "+ getroundednumber(retamount));
                    if(retamount<0){
                        txtreturnamt.setTextColor(getResources().getColor(R.color.red));
                    }else{
                        txtreturnamt.setTextColor(getResources().getColor(R.color.black));
                    }

                }else{
                    txtreturnamt.setText(AppConst.currency +" "+ 0.00);
                }

            }
        });
        etdiscount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                etdiscount.removeTextChangedListener(this);
                Double packamt=0.0;
                if(etdiscount.getText().toString().trim().length()==3){
                    double damt=Double.parseDouble(etdiscount.getText().toString());
                    if(damt>100)
                        etdiscount.setText("100");
                }
                if(etdiscount.getText().toString().trim().length()>0 && !etdiscount.getText().toString().equals(".")) {
                    dis_per=Double.parseDouble(etdiscount.getText().toString());
                    Double samt=Double.parseDouble(tvgrand.getTag().toString());
                    dis_amt = (dis_per * samt) / 100;
                }else{
                    dis_per=0;
                    dis_amt=0.0;
                }
                if(AppConst.isDelivered && etpackcharge.getText().toString().trim().length()>0){
                    packamt=Double.parseDouble(etpackcharge.getText().toString());
                }
                setDiscount(packamt,"paymenttype");
                applyDiscountOnTax();
                etdiscount.setSelection(etdiscount.getText().length());
                etdiscount.addTextChangedListener(this);
            }
        });
        etdisamt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                etdisamt.removeTextChangedListener(this);
                Double packamt=0.0;
                if(etdisamt.getText().toString().trim().length()>0 &&
                        !etdisamt.getText().toString().equals(".")){
                    Double samt=Double.parseDouble(tvgrand.getTag().toString());
                    Double damt=Double.parseDouble(etdisamt.getText().toString());
                    if(samt<damt)
                        etdisamt.setText(samt+"");
                }
                if(etdisamt.getText().toString().trim().length()>0 && !etdisamt.getText().toString().equals(".")) {
                    dis_amt=Double.parseDouble(etdisamt.getText().toString());
                    Double samt=Double.parseDouble(tvgrand.getTag().toString());
                    dis_per = getroundednumber((dis_amt * 100) / samt);
                }else{
                    dis_per=0;
                    dis_amt=0.0;
                }

                if(AppConst.isDelivered && etpackcharge.getText().toString().trim().length()>0){
                    packamt=Double.parseDouble(etpackcharge.getText().toString());
                }

                setDiscount(packamt,"paymenttype");
                etdisamt.setSelection(etdisamt.getText().length());
                etdisamt.addTextChangedListener(this);
            }
        });

        etpackcharge.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                etpackcharge.removeTextChangedListener(this);
                Double packamt=0.0;
                if(etpackcharge.getText().toString().trim().length()>0 &&
                        !etpackcharge.getText().toString().equals(".")){
                    packamt=Double.parseDouble(etpackcharge.getText().toString());
                }else{
                    packamt=0.0;
                }
                setDiscount(packamt,"paymenttype");
                etpackcharge.addTextChangedListener(this);
            }
        });

        dialog.show();
    }

    private void setDiscount(Double packamt,String dialognm){

        Double totdisc=0.0,itemdis=0.0;
        if(tvitemdisc.getTag()!=null && tvitemdisc.getTag().toString().length() >0)
            itemdis=Double.parseDouble(tvitemdisc.getTag().toString());

        Double a=Double.parseDouble(tvgrand.getTag().toString())+packamt-dis_amt;
        totdisc=dis_amt+itemdis;

        if(dialognm.equals("paymenttype")){
            if(orderData.getService_charge()!=null && !orderData.getService_charge().isEmpty()) {
                a = a + Double.parseDouble(orderData.getService_charge());
                tvsercharge.setText(":" + AppConst.currency + " " + pf.setFormat(orderData.getService_charge() + ""));
                tvsercharge.setTag(pf.setFormat(orderData.getService_charge() + ""));
                llservice.setVisibility(View.VISIBLE);
            }
            if(orderData.getTip()!=null && !orderData.getTip().isEmpty()){
                try {
                    JSONObject tJson=new JSONObject(orderData.getTip());
                    a=a+Double.parseDouble(tJson.getString("tip_amount"));
                    ll_tip.setVisibility(View.VISIBLE);
                    tvtip.setText(": " +AppConst.currency+" "+tJson.getString("tip_amount"));
                }catch (JSONException e){}
            }
        }else if(etservicecharge!=null){
            Double samt=Double.parseDouble(tvgrand.getTag().toString());
            double ser_amt=0.0;
            if(rbscper.isChecked() && etservicecharge.getText().toString().trim().length()>0  && !etservicecharge.getText().toString().equals(".")) {
                Double sc = Double.parseDouble(etservicecharge.getText().toString());
                ser_amt = (sc * samt) / 100;
                llserper.setTag(sc);
            }else if(rbscamt.isChecked() && etscamt.getText().toString().length()>0 && !etscamt.getText().toString().equals(".")){
                ser_amt=Double.parseDouble(etscamt.getText().toString());
                Double sper = (ser_amt * 100) / samt;
                etservicecharge.setText(pf.setFormat(sper + ""));
                llserper.setTag(sper);
            }
            a=a+ser_amt;
            tvsercharge.setText(":"+ AppConst.currency+" "+pf.setFormat(ser_amt+""));
            tvsercharge.setTag(pf.setFormat(ser_amt+""));
        }else if(tvsercharge!=null){
            tvsercharge.setTag("0");
        }

        if(dialognm.equals("showorder")){
            if(M.getRoundFormat(context)) {
                Double billgrand = a;
                String roundResult = roundHelper.applyRoundFormat(billgrand);
                a=Double.parseDouble(roundResult);

                Double roundiff=Double.parseDouble(roundResult)-billgrand;
                tvrounding.setText(":"+ AppConst.currency+" "+pf.setFormat(roundiff+""));
                tvrounding.setTag(roundiff+"");
            }
            tvd.setText(context.getString(R.string.txt_discount)+":" + AppConst.currency + " " + pf.setFormat(totdisc + ""));
            tvd.setTag(totdisc);
            tvg.setText(context.getString(R.string.txt_grand_total)+":"+AppConst.currency + " " + pf.setFormat(a + ""));
            tvg.setTag(pf.setFormat(a + ""));
        }else {
            tvdiscount.setText(":" + AppConst.currency + " " + pf.setFormat(totdisc + ""));
            tvdiscount.setTag(totdisc);

            String billgrand=a+"";

            if(!AppConst.isDelivered && M.getRoundFormat(context) && pm_rounding!=null && pm_rounding.equals("on")){
                String roundResult=roundHelper.applyRoundFormat(Double.parseDouble(billgrand+""));
                dtvgrand.setText(AppConst.currency + " " + roundResult);
                dtvgrand.setTag(roundResult);

                Double roundiff=Double.parseDouble(roundResult)-Double.parseDouble(billgrand);
                tvrounding.setText(":"+AppConst.currency+" "+pf.setFormat(roundiff+""));
                tvrounding.setTag(roundiff+"");
            }else{
                dtvgrand.setText(AppConst.currency + " " + pf.setFormat(a+""));
                dtvgrand.setTag(a);
            }
        }

        if(!AppConst.isDelivered && action.equals("takeaway"))
            setAmountChip(chip_cloud_preset,Double.parseDouble(dtvgrand.getTag().toString()));
    }

    public Double getroundednumber(double value){
        return AppConst.setdecimalfmt(value,context);
    }

    public Double getroundedtotal(double value){
        return AppConst.setdecimalfmt(value,context);
    }

    public void setchips() {
        chip_cloud_preset = (ChipCloud) dialog.findViewById(R.id.chip_cloud_preset);
        chip_cloud_preset.setTag(dtvgrand.getTag());
        final ChipCloud chipCloud = (ChipCloud) dialog.findViewById(R.id.chip_cloud);
        ChipCloud chipCloudOrderType = (ChipCloud) dialog.findViewById(R.id.chip_cloud_ordertype);
        chipdiscount = (ChipCloud) dialog.findViewById(R.id.chip_cloud_discount);
        cc_offer=(ChipCloud)dialog.findViewById(R.id.chip_cloud_offer);
        if (M.getType(context) != null && M.getType(context).equals("1"))
            chipCloudOrderType.setVisibility(View.VISIBLE);
        else
            chipCloudOrderType.setVisibility(View.GONE);
        final ArrayList<String> idlist = new ArrayList<String>();
        if (wlist == null)
            wlist = new ArrayList<>();
        wlist.clear();

        if (AppConst.isDelivered) {
            llpmode.setVisibility(View.GONE);
            llamount.setVisibility(View.GONE);
            editText.setVisibility(View.GONE);
        } else {
            llpmode.setVisibility(View.VISIBLE);
            llamount.setVisibility(View.VISIBLE);
            editText.setVisibility(View.VISIBLE);
        }

        if (plist.size() > 0) {
            PaymentModePojo p = new PaymentModePojo();
            p.setId("0");
            p.setType(getString(R.string.txt_split));
            p.setIs_rounding_on("off");
            //plist.add(p);
            p = new PaymentModePojo();
            p.setId("-5");
            p.setType("Split Items");
            p.setIs_rounding_on("off");
            plist.add(p);
            for (PaymentModePojo word : plist) {
                String id = word.getId();
                String w = word.getType();
                idlist.add(id);
                wlist.add(w);
            }
            String[] namesArr = new String[wlist.size()];
            for (int i = 0; i < wlist.size(); i++) {
                namesArr[i] = wlist.get(i);
            }

            new ChipCloud.Configure()
                    .chipCloud(chipCloud)
                    .selectedFontColor(getResources().getColor(R.color.white))
                    .deselectedFontColor(getResources().getColor(R.color.medium_grey))
                    .selectTransitionMS(500)
                    .deselectTransitionMS(250)
                    .mode(ChipCloud.Mode.SINGLE)
                    .labels(namesArr).typeface(AppConst.font_medium(context))
                    .allCaps(false)
                    .gravity(ChipCloud.Gravity.LEFT)
                    .textSize(getResources().getDimensionPixelSize(R.dimen.chip_textsize))
                    .verticalSpacing(getResources().getDimensionPixelSize(R.dimen.vertical_spacing))
                    .minHorizontalSpacing(getResources().getDimensionPixelSize(R.dimen.min_horizontal_spacing))
                    .chipListener(new ChipListener() {
                        @Override
                        public void chipSelected(int index) {
                            payment_mode = plist.get(index).getId();
                            payment_mode_text = plist.get(index).getType();
                            pm_rounding=plist.get(index).getIs_rounding_on();

                            if(M.getRoundFormat(context) && !AppConst.isDelivered){
                                Double billgrand=Double.parseDouble(dtvgrand.getTag().toString())-Double.parseDouble(tvrounding.getTag().toString());
                                if(pm_rounding!=null && pm_rounding.equals("on")) {
                                    //  llrounding.setVisibility(View.VISIBLE);
                                    String roundResult = roundHelper.applyRoundFormat(billgrand);
                                    dtvgrand.setText(AppConst.currency + " " + roundResult);
                                    dtvgrand.setTag(roundResult);

                                    Double roundiff = Double.parseDouble(roundResult) - billgrand;
                                    tvrounding.setText(":" + AppConst.currency + " " + pf.setFormat(roundiff + ""));
                                    tvrounding.setTag(roundiff + "");
                                }else{
                                    dtvgrand.setText(AppConst.currency + " " + pf.setFormat(billgrand+""));
                                    dtvgrand.setTag(pf.setFormat(billgrand+""));
                                    //  llrounding.setVisibility(View.INVISIBLE);
                                    tvrounding.setTag("0");
                                }
                            }else{
                                // llrounding.setVisibility(View.INVISIBLE);
                                tvrounding.setTag("0");
                            }

                            if (payment_mode_text.equalsIgnoreCase("cash")) {
                                chip_cloud_preset.setVisibility(View.VISIBLE);
                            }else if(payment_mode.equals("0")){
                                splitArray=new JSONArray();
                                openSplitDialog(chipCloud,index);
                            } else {
                                chip_cloud_preset.setVisibility(View.INVISIBLE);
                            }
                        }

                        @Override
                        public void chipDeselected(int index) {
                            payment_mode = "";
                            payment_mode_text = "";
                            pm_rounding="off";
                        }
                    })
                    .build();
            chipCloud.setSelectedChip(0);
        }
        if(!action.equals("dinein")) {
            final String[] typeval = new String[]{getString(R.string.dinein), getString(R.string.parcel)};
            final String[] en_typeval = new String[]{"Dinein", "Parcel"};
            new ChipCloud.Configure()
                    .chipCloud(chipCloudOrderType)
                    .selectedFontColor(getResources().getColor(R.color.white))
                    .deselectedFontColor(getResources().getColor(R.color.medium_grey))
                    .selectTransitionMS(500)
                    .deselectTransitionMS(250)
                    .mode(ChipCloud.Mode.SINGLE)
                    .labels(typeval).typeface(AppConst.font_medium(context))
                    .allCaps(false)
                    .gravity(ChipCloud.Gravity.LEFT)
                    .textSize(getResources().getDimensionPixelSize(R.dimen.chip_textsize))
                    .verticalSpacing(getResources().getDimensionPixelSize(R.dimen.vertical_spacing))
                    .minHorizontalSpacing(getResources().getDimensionPixelSize(R.dimen.min_horizontal_spacing))
                    .chipListener(new ChipListener() {
                        @Override
                        public void chipSelected(int index) {
                            ordertype = typeval[index];
                            en_ordertype = en_typeval[index];
                        }

                        @Override
                        public void chipDeselected(int index) {
                            ordertype = "";
                            en_ordertype = "";
                        }
                    })
                    .build();
            if (AppConst.isDelivered) {
                chipCloudOrderType.setVisibility(View.GONE);
                ordertype = getString(R.string.delivery);
                en_ordertype = "Delivery";
            } else
                chipCloudOrderType.setSelectedChip(0);
        }

        setAmountChip(chip_cloud_preset,granttodal);

        if(!action.equals("dinein")) {
            lldiscount.setVisibility(View.GONE);
            lloffer.setVisibility(View.GONE);
            setDiscountChip();
        }
    }

    private void setDiscountChip(){
        if(offer_id!=null && offer_id.trim().length()>0){
            lloffer.setVisibility(View.GONE);
            lldiscount.setVisibility(View.GONE);
        }else{
            DBDiscount dbDiscount=new DBDiscount(context);
            final List<DiscountPojo> disclist;
            if(action.equals("dinein"))
                disclist=dbDiscount.discountListDinIn(Double.parseDouble(tvgrand.getTag().toString()));
            else
                disclist=dbDiscount.discountList(Double.parseDouble(tvgrand.getTag().toString()));
            if(disclist!=null && disclist.size()>0){
                lloffer.setVisibility(View.VISIBLE);
                lldiscount.setVisibility(View.GONE);
                String[] offers=new String[disclist.size()];
                String[] offids=new String[disclist.size()];
                for(int d=0;d<disclist.size();d++) {
                    offers[d] = disclist.get(d).getName();
                    offids[d]=disclist.get(d).getId();
                }
                new ChipCloud.Configure()
                        .chipCloud(cc_offer)
                        .selectedFontColor(getResources().getColor(R.color.white))
                        .deselectedFontColor(getResources().getColor(R.color.medium_grey))
                        .selectTransitionMS(500)
                        .deselectTransitionMS(250)
                        .mode(ChipCloud.Mode.SINGLE)
                        .labels(offers).typeface(AppConst.font_medium(context))
                        .allCaps(false)
                        .gravity(ChipCloud.Gravity.LEFT)
                        .textSize(getResources().getDimensionPixelSize(R.dimen.chip_textsize))
                        .verticalSpacing(getResources().getDimensionPixelSize(R.dimen.vertical_spacing))
                        .minHorizontalSpacing(getResources().getDimensionPixelSize(R.dimen.min_horizontal_spacing))
                        .chipListener(new ChipListener() {
                            @Override
                            public void chipSelected(int index) {
                                discountPojo=disclist.get(index);
                                if(discountPojo.getDiscount_type().equalsIgnoreCase("percentage")){
                                    etdiscount.setText(disclist.get(index).getDiscount_percentage());
                                }else if(discountPojo.getDiscount_type().equalsIgnoreCase("amount")){
                                    etdisamt.setText(disclist.get(index).getDiscount_amt());
                                }else{
                                    etdiscount.setText("");
                                }
                            }
                            @Override
                            public void chipDeselected(int index) {
                                etdiscount.setText("");
                                discountPojo=null;
                            }
                        })
                        .build();

                if(action.equals("dinein") && orderData.getOfferid()!=null && orderData.getOfferid().trim().length()>0 && offids!=null){
                    int o=0,seloff=-1;
                    for(String ofi:offids){
                        if(Arrays.asList(orderData.getOfferid().split(",")).contains(ofi))
                            seloff=o;
                        o++;
                    }
                    if(seloff!=-1) {
                        cc_offer.setSelectedChip(seloff);
                    }
                }

            }
            else {
                discountPojo = null;
                lloffer.setVisibility(View.GONE);
                lldiscount.setVisibility(View.VISIBLE);

                final String[] dilist = new String[]{"5", "10", "20", "25", getString(R.string.custom)};
                new ChipCloud.Configure()
                        .chipCloud(chipdiscount)
                        .selectedFontColor(getResources().getColor(R.color.white))
                        .deselectedFontColor(getResources().getColor(R.color.medium_grey))
                        .selectTransitionMS(500)
                        .deselectTransitionMS(250)
                        .mode(ChipCloud.Mode.SINGLE)
                        .labels(dilist).typeface(AppConst.font_medium(context))
                        .allCaps(true)
                        .gravity(ChipCloud.Gravity.LEFT)
                        .textSize(getResources().getDimensionPixelSize(R.dimen.chip_textsize))
                        .verticalSpacing(getResources().getDimensionPixelSize(R.dimen.vertical_spacing))
                        .minHorizontalSpacing(getResources().getDimensionPixelSize(R.dimen.min_horizontal_spacing))
                        .chipListener(new ChipListener() {
                            @Override
                            public void chipSelected(int index) {

                                if (dilist[index].equalsIgnoreCase(getString(R.string.custom))) {
                                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                                    etdiscount.setVisibility(View.VISIBLE);
                                    etdisamt.setVisibility(View.GONE);
                                    if (!action.equals("takeaway") && dis_per > 0)
                                        etdiscount.setText(dis_per + "");
                                    else
                                        etdiscount.setText("");
                                } else {
                                    String disper = dilist[index];
                                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                                    etdiscount.setText(disper);
                                    etdiscount.setVisibility(View.GONE);
                                }
                                etdiscount.setSelection(etdiscount.getText().length());
                            }

                            @Override
                            public void chipDeselected(int index) {
                                if (dilist[index].equalsIgnoreCase(getString(R.string.custom)))
                                    etdiscount.setVisibility(View.GONE);
                                etdiscount.setText("");
                            }
                        })
                        .build();
                if (!action.equals("takeaway")) {
                    String disjson = "";
                    if (action.equals("dinein")) {
                        disjson = orderData.getOrder_discount();
                    }

                    if (disjson != null && disjson.trim().length() > 0 && !disjson.equals("0")) {
                        try {
                            JSONObject obj = new JSONObject(disjson);
                            String diper = "0";
                            if (obj.has("discount_per")) {
                                diper = obj.getString("discount_per");
                                List<String> dl = new ArrayList<String>(Arrays.asList(dilist));
                                String dp = diper + "";
                                if (dp.endsWith(".0"))
                                    dp = dp.replace(".0", "");
                                if (dl.contains(dp)) {
                                    int pos = dl.indexOf(dp);
                                    chipdiscount.setSelectedChip(pos);
                                    rbper.setChecked(true);
                                    etdiscount.setText(Double.parseDouble(dl.get(pos) + "") + "");
                                } else {
                                    if (Double.parseDouble(diper + "") > 0) {
                                        rbper.setChecked(true);
                                        dis_per = Double.parseDouble(diper + "");
                                        chipdiscount.setSelectedChip(4);
                                    }
                                }
                            } else if (obj.has("discount_amount")) {
                                dis_amt = Double.parseDouble(obj.getString("discount_amount") + "");
                                if (action.equals("dinein"))
                                    etdisamt.setText(orderData.getBill_amt_discount() + "");
                                else
                                    etdisamt.setText(dis_amt + "");
                                etdisamt.setSelection(etdisamt.getText().length());
                                rbamt.setChecked(true);
                            }

                        } catch (Throwable t) {

                        }
                    }
                }
            }
        }

        if(M.isServiceCharge(context)) {
            final String[] dilist = new String[]{"5", "10", "20", "25", getString(R.string.custom)};
            new ChipCloud.Configure()
                    .chipCloud(cc_service_charge)
                    .selectedFontColor(getResources().getColor(R.color.white))
                    .deselectedFontColor(getResources().getColor(R.color.medium_grey))
                    .selectTransitionMS(500)
                    .deselectTransitionMS(250)
                    .mode(ChipCloud.Mode.SINGLE)
                    .labels(dilist).typeface(AppConst.font_medium(context))
                    .allCaps(true)
                    .gravity(ChipCloud.Gravity.LEFT)
                    .textSize(getResources().getDimensionPixelSize(R.dimen.chip_textsize))
                    .verticalSpacing(getResources().getDimensionPixelSize(R.dimen.vertical_spacing))
                    .minHorizontalSpacing(getResources().getDimensionPixelSize(R.dimen.min_horizontal_spacing))
                    .chipListener(new ChipListener() {
                        @Override
                        public void chipSelected(int index) {

                            if (dilist[index].equalsIgnoreCase(getString(R.string.custom))) {
                                cc_service_charge.setTag("-1");
                                etservicecharge.setVisibility(View.VISIBLE);
                            } else {
                                etservicecharge.setVisibility(View.GONE);
                                String disper = dilist[index];
                                cc_service_charge.setTag(disper);
                                etservicecharge.setText(disper);
                            }
                        }

                        @Override
                        public void chipDeselected(int index) {
                            etservicecharge.setVisibility(View.GONE);
                            cc_service_charge.setTag("0");
                            etservicecharge.setText("0");
                        }
                    })
                    .build();

            if(action.equals("dinein") && orderData.getService_charge()!=null && !orderData.getService_charge().isEmpty()){
                String sper=etservicecharge.getText().toString();
                if(sper.contains(".")) {
                    String[] ss=sper.split("\\.");
                    if(ss.length==2 && Double.parseDouble(ss[1])==0){
                        sper=ss[0];
                    }
                }
                List<String> dl = new ArrayList<String>(Arrays.asList(dilist));
                if(dl.contains(sper)){
                    cc_service_charge.setSelectedChip(dl.indexOf(sper));
                }else
                    cc_service_charge.setSelectedChip(dilist.length-1);
            }
        }
    }

    int id=0;
    ArrayList<EditText> etsplit=new ArrayList<>();
    ArrayList<EditText> etnmlist=new ArrayList<>();
    ArrayList<EditText> etphlist=new ArrayList<>();
    ArrayList<TextView> tvamtlist=new ArrayList<>();
    ArrayList<Spinner> spnlist=new ArrayList<>();

    private void openSplitDialog(final ChipCloud chipCloud, final int index) {
        if(etsplit!=null)
            etsplit.clear();
        if(spnlist!=null)
            spnlist.clear();
        if(etnmlist!=null)
            etnmlist.clear();
        if(etphlist!=null)
            etphlist.clear();
        if(tvamtlist!=null)
            tvamtlist.clear();
        id=0;
        splitDialog=new Dialog(context,android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen);
        splitDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        splitDialog.setContentView(R.layout.dialog_split);
        splitDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        ivback=(ImageView)splitDialog.findViewById(R.id.ivback);
        llsplit=(LinearLayout)splitDialog.findViewById(R.id.llsplit);
        final TextView tvamount=(TextView)splitDialog.findViewById(R.id.tvamount);
        TextView tvinc=(TextView)splitDialog.findViewById(R.id.imgdinc);
        TextView tvdec=(TextView)splitDialog.findViewById(R.id.imgddec);
        tvno=(TextView)splitDialog.findViewById(R.id.tvno);
        if(wlist==null)
            wlist=new ArrayList<>();
        tvno.setText("2");
        addSplitRow();
        addSplitRow();

        tvamount.setText(getString(R.string.remaining)+" "+dtvgrand.getTag().toString());

        tvinc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int q=Integer.parseInt(tvno.getText().toString());
                q++;
                tvno.setText(q+"");
                addSplitRow();
                id++;
            }
        });

        tvdec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int q=Integer.parseInt(tvno.getText().toString());
                if(q>1){
                    q--;
                    tvno.setText(q+"");
                    removeSplitRow();
                }
            }
        });

        ivback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chipCloud.chipDeselected(index);
                splitDialog.dismiss();
            }
        });
        splitDialog.show();
    }

    double charge_amt=0.0;
    TextView seltv;
    EditText selet;

    private void addSplitRow(){
        final View v = LayoutInflater.from(context).inflate(R.layout.add_split_row, null);
        LinearLayout ll = (LinearLayout) v.findViewById(R.id.ll);
        final Spinner spn=(Spinner)v.findViewById(R.id.spnpaymode);
        spn.setId(id);
        final TextView tvsamt=(TextView)v.findViewById(R.id.tvamt);
        tvsamt.setTypeface(AppConst.font_regular(context));
        tvsamt.setId(id);
        tvsamt.setTag("static");
        final EditText etamt=(EditText)v.findViewById(R.id.etamt);
        etamt.setTypeface(AppConst.font_regular(context));
        etamt.setId(id);
        etamt.setTag("static");
        final EditText etnm=(EditText)v.findViewById(R.id.etnm);
        etamt.setTypeface(AppConst.font_regular(context));
        final EditText etph=(EditText)v.findViewById(R.id.etphone);
        etamt.setTypeface(AppConst.font_regular(context));
        Button btncharge=(Button)v.findViewById(R.id.btncharge);
        btncharge.setTypeface(AppConst.font_regular(context));
        btncharge.setId(id);
        ImageView ivdel=(ImageView)v.findViewById(R.id.ivdel);

        if(wlist!=null){
            ArrayList<String> data=wlist;
            data.remove(getString(R.string.txt_split));
            ArrayAdapter<String> pada=new ArrayAdapter<String>(context,R.layout.spn_category_row,R.id.txt,data);
            spn.setAdapter(pada);
        }

        tvsamt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(selet!=null){
                    selet.setEnabled(false);
                    selet.setVisibility(View.GONE);
                    seltv.setVisibility(View.VISIBLE);
                }
                tvsamt.setVisibility(View.GONE);
                etamt.setVisibility(View.VISIBLE);
                etamt.setEnabled(true);
                etamt.setText(tvsamt.getText().toString());
                etamt.setSelection(etamt.getText().length());
                tvsamt.setTag("manual");
                etamt.setTag("manual");
                etamt.requestFocus();
                selet=etamt;
                seltv=tvsamt;
            }
        });

        etamt.addTextChangedListener(new TextWatcher() {
            double d,m_amt=0.0,diff_amt=0.0;
            String beforeval;

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                beforeval=etamt.getText().toString();
                if(beforeval.equals("."))
                    beforeval="0";
                d=Double.parseDouble(dtvgrand.getTag().toString());
                m_amt=0.0;
                for(EditText e:etsplit){
                    if(etamt.getId()!=e.getId() && e.getTag().equals("manual")) {
                        m_amt = m_amt + Double.parseDouble(e.getText().toString());
                    }
                }
                diff_amt=d-m_amt;
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                etamt.removeTextChangedListener(this);
                String txt = etamt.getText().toString();
                if (etamt.getText().toString().trim().length() == 0){
                    etamt.setText("0.0");
                    txt="0.0";
                }else if(charSequence.toString().endsWith("."))
                    txt=charSequence+"0";
                Double c_amt = Double.parseDouble(txt);
                if (c_amt > diff_amt) {
                    etamt.setText(beforeval + "");
                    tvsamt.setText(beforeval);
                }else{
                    tvsamt.setText(txt);
                }
                etamt.setSelection(etamt.getText().length());
                etamt.addTextChangedListener(this);
            }

            @Override
            public void afterTextChanged(Editable editable) {
                setSplitAmount();
            }
        });
        ll.setTag(id+"");

        ivdel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etsplit.remove(etamt);
                tvamtlist.remove(tvsamt);
                etphlist.remove(etph);
                etnmlist.remove(etnm);
                spnlist.remove(spn);
                ((LinearLayout)v.getParent()).removeView(v);
                int q=Integer.parseInt(tvno.getText().toString());
                if(q>1){
                    q--;
                    tvno.setText(q+"");
                    setSplitAmount();
                }
            }
        });
        btncharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                is_split=true;
                double gamt=Double.parseDouble(dtvgrand.getTag().toString());
                if (splite_amt == gamt) {
                    String amt = tvsamt.getText().toString();
                    if(amt.endsWith("."))
                        amt=amt+"0";
                    charge_amt = charge_amt + Double.parseDouble(amt);
                    String tp = plist.get(spn.getSelectedItemPosition()).getId();

                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("payment_mode", tp);
                        jsonObject.put("payment_mode_text", plist.get(spn.getSelectedItemPosition()).getType());
                        jsonObject.put("split_amount", getroundednumber(Double.parseDouble(amt))+"");
                        jsonObject.put("cust_name", etnm.getText().toString());
                        jsonObject.put("cust_phone", etph.getText().toString());
                        splitArray.put(jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    String cmt = M.getWaitername(context);
                    buildJsonSplit(cmt, etnm.getText().toString(), etph.getText().toString(),amt+"");

                    ssDialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                    ssDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    ssDialog.setContentView(R.layout.dialog_split_success);
                    ssDialog.setCancelable(false);
                    TextView txtcharge = (TextView) ssDialog.findViewById(R.id.txtcharge);
                    TextView tvspamt = (TextView) ssDialog.findViewById(R.id.tvsplitamt);
                    TextView tvcust = (TextView) ssDialog.findViewById(R.id.tvcust);
                    Button btnc = (Button) ssDialog.findViewById(R.id.btncontinue);
                    btnc.setTypeface(AppConst.font_regular(context));
                    Button btnn = (Button) ssDialog.findViewById(R.id.btnnew);
                    btnn.setTypeface(AppConst.font_regular(context));
                    txtcharge.setText(dtvgrand.getTag().toString());
                    tvspamt.setText("" + amt);
                    tvcust.setText(etnm.getText().toString()+" "+etph.getText().toString());
                    if (charge_amt == Double.parseDouble(dtvgrand.getTag().toString())) {
                        btnc.setVisibility(View.GONE);
                        btnn.setVisibility(View.VISIBLE);
                    } else {
                        btnc.setVisibility(View.VISIBLE);
                        btnn.setVisibility(View.GONE);
                    }

                    btnc.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            etsplit.remove(etamt);
                            etphlist.remove(etph);
                            etnmlist.remove(etnm);
                            spnlist.remove(spn);
                            tvamtlist.remove(tvsamt);
                            ((LinearLayout) v.getParent()).removeView(v);
                            ssDialog.dismiss();
                        }
                    });
                    btnn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            is_split = true;
                            ssDialog.dismiss();
                            splitDialog.dismiss();
                            String cmt ="";
                            finishOrder(cmt, "", "", "", "","");
                        }
                    });
                    ivback.setVisibility(View.GONE);
                    ssDialog.show();
                }else{
                    double da=gamt-splite_amt;
                    Toast.makeText(context,da+" "+getString(R.string.txt_amount_remaining),Toast.LENGTH_SHORT).show();
                }
            }
        });
        etsplit.add(etamt);
        tvamtlist.add(tvsamt);
        etphlist.add(etph);
        etnmlist.add(etnm);
        spnlist.add(spn);
        llsplit.addView(ll);
        setSplitAmount();
    }

    private void setSplitAmount() {
        splite_amt=0;
        int tot_st=0;
        Double damt=Double.parseDouble(dtvgrand.getTag().toString());
        double diff_amt=damt;
        for(int i=0;i<etsplit.size();i++){
            TextView tv=tvamtlist.get(i);
            EditText et=etsplit.get(i);
            if(tv.getTag().equals("static"))
                tot_st++;
            else if(et.getText().toString().endsWith("."))
                diff_amt=diff_amt-Double.parseDouble(et.getText().toString()+"0");
            else
                diff_amt=diff_amt-Double.parseDouble(et.getText().toString());
        }
        double persplit=diff_amt/tot_st;
        if(persplit<0)
            persplit=0;
        for(int i=0;i<etsplit.size();i++){
            TextView tvamt=tvamtlist.get(i);
            if(tvamt.getTag().equals("static"))
                tvamt.setText(AppConst.setdecimalfmt(persplit,context)+"");
            splite_amt=splite_amt+Double.parseDouble(tvamt.getText().toString());
        }
    }

    private void removeSplitRow(){
        etsplit.remove(etsplit.size()-1);
        tvamtlist.remove(tvamtlist.size()-1);
        llsplit.removeViewAt(llsplit.getChildCount()-1);
        setSplitAmount();
    }

    void setAmountChip(ChipCloud chip_cloud_preset,Double granttodal){
        txtreturnamt.setText(AppConst.currency +" "+ 0.00);
        DecimalFormat format = new DecimalFormat();
        format.setDecimalSeparatorAlwaysShown(false);
        final ArrayList<String> presetlist= new ArrayList<>();
        presetlist.add(Math.round(granttodal)+"");
        double numberten= Math.round((granttodal + 5)/ 10.0) * 10.0;
        double numbertwentyfive= Math.round((granttodal + 24) / 25) * 25;
        double numberfifty= Math.round((granttodal + 49) / 50) * 50;
        double numberhundred= Math.round((granttodal + 99) / 100) * 100;

        presetlist.add(String.valueOf(format.format(numberten)));
        presetlist.add(String.valueOf(format.format(numbertwentyfive)));
        presetlist.add(String.valueOf(format.format(numberfifty)));
        presetlist.add(String.valueOf(format.format(numberhundred)));

        presetlist.add(String.valueOf(granttodal));
        if(granttodal<200) {
            presetlist.add(String.valueOf(200));
        }
        if(granttodal<500) {
            presetlist.add(String.valueOf(500));
        }
        if(granttodal<1000)
            presetlist.add(String.valueOf(1000));
        if(granttodal<1500)
            presetlist.add(String.valueOf(1500));
        if(granttodal<2000)
            presetlist.add(String.valueOf(2000));
        if(presetlist.size()==0)
        {
            presetlist.add("100");
        }
        double[] presetarray = new double[presetlist.size()];

        for (int i = 0; i < presetlist.size(); i++) {
            String txtval=AppConst.arabicToEng(presetlist.get(i));
            String a=txtval.replaceAll(",", "");
            a=a.replaceAll(" ","");
            a=a.replaceAll("٬","");
            if(a.endsWith("."))
                a=a.substring(0,a.length()-1);
            presetarray[i] = Double.parseDouble(a);
        }
        double current = presetarray[0];
        boolean found = false;

        for (int i = 0; i < presetarray.length; i++) {
            if (current == presetarray[i] && !found) {
                found = true;
            } else if (current != presetarray[i]) {
                current = presetarray[i];
                found = false;
            }
        }

        Arrays.sort(presetarray);
        double[] finalarray = removeDuplicates(presetarray);

        List<String> intList = new ArrayList<String>();
        for (int index = 0; index < finalarray.length; index++)
        {
            intList.add(String.valueOf(finalarray[index]));
        }
        intList.add(getString(R.string.custom));

        final String[] a=intList.toArray(new String[0]);

        new ChipCloud.Configure()
                .chipCloud(chip_cloud_preset)
                .selectedFontColor(getResources().getColor(R.color.white))
                .deselectedFontColor(getResources().getColor(R.color.medium_grey))
                .selectTransitionMS(500)
                .deselectTransitionMS(250)
                .mode(ChipCloud.Mode.SINGLE)
                .labels(a).typeface(AppConst.font_medium(context))
                .allCaps(true)
                .gravity(ChipCloud.Gravity.LEFT)
                .textSize(getResources().getDimensionPixelSize(R.dimen.chip_textsize))
                .verticalSpacing(getResources().getDimensionPixelSize(R.dimen.vertical_spacing))
                .minHorizontalSpacing(getResources().getDimensionPixelSize(R.dimen.min_horizontal_spacing))
                .chipListener(new ChipListener() {
                    @Override
                    public void chipSelected(int index) {

                        if(a[index].equalsIgnoreCase(getString(R.string.custom)))
                        {
                            llcal.setVisibility(View.VISIBLE);
                            editText.setEnabled(false);
                            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                            editText.setText("");
                        }else {
                            llcal.setVisibility(View.GONE);
                            String amountselected = a[index];
                            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                            editText.setEnabled(false);
                            editText.setText("" + amountselected);
                        }
                        editText.setSelection(editText.getText().length());
                    }
                    @Override
                    public void chipDeselected(int index) {
                        txtreturnamt.setText(AppConst.currency +" "+ 0.00);

                    }
                })
                .build();
    }

    public static double[] removeDuplicates(double[] input){

        int j = 0;
        int i = 1;
        //return if the array length is less than 2
        if(input.length < 2){
            return input;
        }
        while(i < input.length){
            if(input[i] == input[j]){
                i++;
            }else{
                input[++j] = input[i++];
            }
        }
        double[] output = new double[j+1];
        for(int k=0; k<output.length; k++){
            output[k] = input[k];
        }

        return output;
    }

    @Override
    public void onClick(View view) {
        if(view==btnlogout){
            if(dialogcust!=null && dialogcust.isShowing())
                dialogcust.dismiss();
            if(dialog!=null && dialog.isShowing())
                dialog.dismiss();
            Intent mIntent = new Intent(context, WaiterListActivity.class);
            if(M.getDeviceCode(context)!=null)
                mIntent = new Intent(context, LoginActivity.class);
            M.waiterlogOut(context);
            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            finish();
            startActivity(mIntent);
        }else if(view==btnsync){
            EventBus.getDefault().post("syncdata");
        }else if(view==btnaddItems){
            EventBus.getDefault().post("addItems");
        }else if(view==btndone){
            if(payment_mode==null || payment_mode.trim().length()<=0){
                Toast.makeText(context,R.string.empty_payment_mode,Toast.LENGTH_SHORT).show();
            }else if(!action.equals("dinein") && (ordertype==null || ordertype.trim().length()<=0)){
                Toast.makeText(context,R.string.empty_order_type,Toast.LENGTH_SHORT).show();
            }else{
                if (M.getcashierPin(context) && (setitemOffer || discountPojo != null)) {
                    final Dialog dialog1 = new Dialog(context, android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen);
                    dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog1.setContentView(R.layout.dialog_waiter_password);
                    dialog1.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                    PassCodeView circlePinField=(PassCodeView)dialog1.findViewById(R.id.type_pin);
                    circlePinField.setOnTextChangeListener(new PassCodeView.TextChangeListener() {
                        @Override
                        public void onTextChanged(String pin) {
                            if(pin.length()==4) {
                                if (pin.equalsIgnoreCase(M.getWaiterPin(context))) {
                                    dialog1.dismiss();
                                    paymentDone();
                                } else
                                    Toast.makeText(context, R.string.invalid_pwd, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                    dialog1.show();
                } else
                    paymentDone();
            }
        }else if(view==btncancel){
            dialog.dismiss();
        }else if(view==btndemo){
            if (connectionDetector.isConnectingToInternet()) {
                M.showLoadingDialog(context);
                CuisineDishAPI mAuthenticationAPI = APIServiceHeader.createService(context, CuisineDishAPI.class);
                Call<SuccessPojo> call = mAuthenticationAPI.addDemoData(restaurantid,runiqueid);
                call.enqueue(new retrofit2.Callback<SuccessPojo>() {
                    @Override
                    public void onResponse(Call<SuccessPojo> call, Response<SuccessPojo> response) {
                        if (response.isSuccessful()) {
                            SuccessPojo pojo = response.body();
                            if (pojo != null) {
                                M.hideLoadingDialog();
                                //     Log.d(TAG,"demodata:"+pojo.getSuccess());
                                if(pojo.getSuccess()==1)
                                    btnsync.performClick();
                                else if(pojo.getSuccess()==0){
                                    Toast.makeText(context, R.string.data_avaliable,Toast.LENGTH_SHORT).show();
                                    btnsync.performClick();
                                }
                            }else{
                                M.hideLoadingDialog();
                            }
                        } else {
                            M.hideLoadingDialog();
                            int statusCode = response.code();
                            ResponseBody errorBody = response.errorBody();
                            Log.d(TAG, "error:" + statusCode + " " + errorBody);
                        }
                    }

                    @Override
                    public void onFailure(Call<SuccessPojo> call, Throwable t) {
                        Log.d(TAG, "fail:" + t.getMessage());
                        M.hideLoadingDialog();
                    }
                });

            } else {
                M.showToast(context, getString(R.string.no_internet_error));
                M.hideLoadingDialog();
            }
        }else if(view==btnupdate){
            if(newitems>0 && M.isItemAlert(context)){
                final Dialog d=new Dialog(context);
                d.requestWindowFeature(Window.FEATURE_NO_TITLE);
                d.setCancelable(false);
                d.setContentView(R.layout.dialog_confirm_order);
                d.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT);

                LinearLayout llitem=(LinearLayout)d.findViewById(R.id.llitems);
                Button btnconfirm=(Button)d.findViewById(R.id.btnconfirm);
                btnconfirm.setTypeface(AppConst.font_regular(context));
                TextView tvtbl=(TextView)d.findViewById(R.id.tvtablenm);
                tvtbl.setText(tblnm);
                TextView tvclose=(TextView)d.findViewById(R.id.tvclose);

                for (DishOrderPojo dp:AppConst.dishorederlist){
                    if(dp.isnew()) {
                        View v = LayoutInflater.from(context).inflate(R.layout.confirm_order_row, null);
                        LinearLayout ll = (LinearLayout) v.findViewById(R.id.ll);
                        ll.setPadding(0,0,0,20);
                        TextView tvtxt = (TextView) v.findViewById(R.id.tvtext);
                        TextView tvval = (TextView) v.findViewById(R.id.tvvalue);

                        String txt = dp.getDishname();
                        if (dp.getPrenm() != null && dp.getPrenm().trim().length() > 0)
                            txt = txt + "\n\t\t" + dp.getPrenm();
                        tvtxt.setText(txt);
                        tvval.setText(dp.getQty());
                        llitem.addView(ll);
                    }
                }

                btnconfirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        d.dismiss();
                        updateTableItem();
                    }
                });

                tvclose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        d.dismiss();
                    }
                });
                if(llitem.getChildCount()>0)
                    d.show();
            }else
                updateTableItem();
        }else if(view==btncanceltbl){
            if(M.isCustomAllow(M.key_dinein_delitem,context)){
                new AlertDialog.Builder(context)
                        .setTitle(getString(R.string.txt_are_sure))
                        .setMessage(getString(R.string.que_cancel_order))
                        .setNegativeButton(android.R.string.no, null)
                        .setPositiveButton(android.R.string.yes,
                                new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface arg0, int arg1) {
                                        cancelTblOrder();
                                    }
                                }).create().show();
            }else{
                final Dialog dialog1 = new Dialog(context, android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen);
                dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog1.setContentView(R.layout.dialog_waiter_password);
                TextView tvtitle=(TextView)dialog1.findViewById(R.id.tvtitle);
                tvtitle.setVisibility(View.VISIBLE);
                tvtitle.setText(context.getString(R.string.txt_are_sure));
                TextView tvmsg=(TextView)dialog1.findViewById(R.id.tvmsg);
                tvmsg.setVisibility(View.VISIBLE);
                tvmsg.setText(context.getString(R.string.que_cancel_order));
                TextView tv=(TextView)dialog1.findViewById(R.id.tv);
                tv.setText(R.string.type_outlet_pin);

                final PassCodeView circlePinField=(PassCodeView)dialog1.findViewById(R.id.type_pin);
                circlePinField.setOnTextChangeListener(new PassCodeView.TextChangeListener() {
                    @Override
                    public void onTextChanged(String pin) {
                        if(pin.length()==4) {
                            if (pin.equalsIgnoreCase(M.getOutletPin(context))) {
                                circlePinField.removeOnTextChangeListener();
                                dialog1.dismiss();
                                cancelTblOrder();
                            } else
                                Toast.makeText(context, R.string.invalid_pwd, Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                dialog1.show();
            }
        }else if(view==btnfinish){
            if(tableItemAdapter.newOrderCount()>0){
                new AlertDialog.Builder(context)
                        .setTitle(R.string.dialog_title_finish_order)
                        .setMessage(getString(R.string.txt1)+tableItemAdapter.newOrderCount()+getString(R.string.txt2))
                        .setNegativeButton(android.R.string.no, null)
                        .setPositiveButton(android.R.string.yes,
                                new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface arg0, int arg1) {
                                        btnupdate.performClick();
                                    }
                                }).create().show();
            }else{
                showFullOrder();
            }
        }
    }

    private void cancelTblOrder(){
        try {
            JSONObject cancelObj=new JSONObject();
            cancelObj.put("key",WifiHelper.api_cancel_order);
            cancelObj.put("order_id",orderid);
            cancelObj.put("table_id",tblid);
            cancelObj.put("user_ip",getLocalIp());
            sendToServer(cancelObj+"");
        }catch (JSONException e){}
    }

    private void updateTableItem(){
        M.showLoadingDialog(context);
        int newcnt=0;
        ArrayList<String> catlist=new ArrayList<>();
        ArrayList<String> pidlist=new ArrayList<>();
        if(totlist==null)
            totlist=new ArrayList<>();
        totlist.clear();
        DBPrinter dbp=new DBPrinter(context);
        if(kplist==null)
            kplist=new ArrayList<>();
        kplist.clear();
        JSONArray dishArray=new JSONArray();
        JSONObject updateObj=new JSONObject();
        for (int i = 0; i < AppConst.dishorederlist.size(); i++) {
            if (AppConst.dishorederlist.get(i).isnew()) {
                DishOrderPojo dop=AppConst.dishorederlist.get(i);
                String cid=AppConst.dishorederlist.get(i).getCusineid();
                if(!catlist.contains(cid)) {
                    catlist.add(cid);
                    KitchenPrinterPojo kprinter=dbp.getCatPrinters(cid);
                    if(kprinter!=null) {
                        if(!pidlist.contains(kprinter.getId())) {
                            pidlist.add(kprinter.getId());
                            kplist.add(kprinter);
                            totlist.add(1);
                        }else{
                            int p=pidlist.indexOf(kprinter.getId());
                            int val=totlist.get(p)+1;
                            totlist.set(p,val);
                        }
                    }
                }
                newcnt++;
                try {
                    String tm=dop.getTm();
                    JSONObject dishObj = new JSONObject();
                    dishObj.put("oi_time",tm+"");
                    dishObj.put("dish_id", dop.getDishid());
                    dishObj.put("dish_name", dop.getDishname());
                    dishObj.put("qty", dop.getQty());

                    dishObj.put("priceperdish", dop.getPriceperdish());
                    dishObj.put("price_wt", dop.getPrice_without_tax());
                    dishObj.put("priceper_wt", dop.getPriceper_without_tax());
                    dishObj.put("price", dop.getPrice());

                    dishObj.put("pre_id", dop.getPrefid());
                    dishObj.put("pre_nm", dop.getPrenm());
                    dishObj.put("discount", dop.getDiscount());
                    dishObj.put("cmt", dop.getDish_comment());

                    dishObj.put("soldby", dop.getSold_by());
                    dishObj.put("tot_tax", dop.getTot_tax());
                    dishObj.put("tax_amt", dop.getTax_amt());
                    dishObj.put("pu_id", dop.getPurchased_unit_id());
                    dishObj.put("pu_nm", dop.getPurchased_unit_name());
                    dishObj.put("uu_id", dop.getUsed_unit_id());
                    dishObj.put("uu_nm", dop.getUsed_unit_name());
                    dishObj.put("sortnm", dop.getSort_nm());
                    dishObj.put("u_id", dop.getUnitid());
                    dishObj.put("u_nm", dop.getUnitname());
                    dishObj.put("weight", dop.getWeight());
                    dishObj.put("offer", dop.getOffer());
                    dishObj.put("tot_disc", dop.getTot_disc());
                    dishObj.put("cuisine_id",dop.getCusineid());
                    dishObj.put("pref_flag",dop.getPreflag());
                    JSONArray jtax=new JSONArray();
                    JSONObject jt=new JSONObject();
                    if(dop.getTax_data()!=null && dop.getTax_data().size()>0){
                        for(TaxData t:dop.getTax_data()){
                            JSONObject j=new JSONObject();
                            j.put("id",t.getId());
                            j.put("txt",t.getText());
                            j.put("val",t.getValue());
                            j.put("tax_amt",t.getTax_amount());
                            j.put("tax_val",t.getTax_value());
                            jtax.put(j);
                        }
                        jt.put("taxdata",jtax);
                    }
                    dishObj.put("tax", jt + "");
                    JSONArray jVar=new JSONArray();
                    if(dop.getVarPojoList()!=null && dop.getVarPojoList().size()>0){
                        for(VarPojo p:dop.getVarPojoList()){
                            Double vamt=Double.parseDouble(p.getQuantity())*Double.parseDouble(p.getAmount());
                            Double vamtwt=Double.parseDouble(p.getQuantity())*Double.parseDouble(p.getAmount_wt());
                            JSONObject preObj=new JSONObject();
                            preObj.put("id",p.getId());
                            preObj.put("name",p.getName());
                            preObj.put("amount",vamt);
                            preObj.put("amount_wt",vamtwt);
                            preObj.put("quantity",p.getQuantity());
                            jVar.put(preObj);
                        }
                        dishObj.put("var_array",jVar);
                    }
                    dishArray.put(dishObj);
                }catch (JSONException e){}
            }
        }
        M.hideLoadingDialog();
        if(newcnt>0){
            if(AppConst.isMyServiceRunning(POServerService.class,context)) {
                try {
                    updateObj.put("key", WifiHelper.api_update_order);
                    updateObj.put("oid", orderid);
                    updateObj.put("userid", M.getWaiterid(context));
                    updateObj.put("msg", dishArray + "");
                    updateObj.put("user_ip",getLocalIp());
                    //  Log.d(TAG,"updateitems:"+updateObj);
                    M.showLoadingDialog(context);
                    sendToServer(updateObj + "");
//                    long delayTm=2000;
//                    if (M.isKitchenCatPrinter(context) && kplist != null && kplist.size() > 0) {
//                        int k = 0, kms = 0;
//                        for (final KitchenPrinterPojo kp : kplist) {
//                            if (kp.getDeviceType() != null && kp.getDeviceType().trim().length() > 0 && !kp.getDeviceType().equals("0")) {
//                                new Handler().postDelayed(new Runnable() {
//                                    public void run() {
//                                        kitchenreciept(kp.getCategoryid(), kp.getId(), Integer.parseInt(kp.getDeviceType()), true,kp.isAdv(),kp.getPaper_width(),kp.getStar_settings());
//                                    }
//                                }, kms);
//                            }
//                            kms = kms + (totlist.get(k) * 1000);
//                            k++;
//                        }
//                        delayTm=kms;
//                    } else if (M.isKitchenPrinter(context) && KitchenGlobals.deviceType != 0)
//                        kitchenreciept(null, null, KitchenGlobals.deviceType, true,M.isadvanceprint(M.key_kitchen,context),M.retriveVal(M.key_kitchen_width,context),null);
//                    if (M.isKOTAlert(context)) {
//                        new Handler().postDelayed(new Runnable() {
//                            public void run() {
//                                new AlertDialog.Builder(context)
//                                        .setTitle(R.string.other_kitchen_receipt)
//                                        .setCancelable(false)
//                                        .setMessage(context.getString(R.string.you_want_other_kitchen_receipt))
//                                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
//                                            @Override
//                                            public void onClick(DialogInterface dialogInterface, int i) {
//                                                Toast.makeText(context, R.string.order_place_success, Toast.LENGTH_SHORT).show();
//                                                onBackPressed();
//                                            }
//                                        })
//                                        .setPositiveButton(android.R.string.yes,
//                                                new DialogInterface.OnClickListener() {
//
//                                                    public void onClick(DialogInterface arg0, int arg1) {
//                                                        if (M.isKitchenCatPrinter(context) && kplist != null && kplist.size() > 0) {
//                                                            int k = 0, kms = 0;
//                                                            for (final KitchenPrinterPojo kp : kplist) {
//                                                                if (kp.getDeviceType() != null && kp.getDeviceType().trim().length() > 0 && !kp.getDeviceType().equals("0")) {
//                                                                    new Handler().postDelayed(new Runnable() {
//                                                                        public void run() {
//                                                                            kitchenreciept(kp.getCategoryid(), kp.getId(), Integer.parseInt(kp.getDeviceType()), true,kp.isAdv(),kp.getPaper_width(),kp.getStar_settings());
//                                                                        }
//                                                                    }, kms);
//                                                                }
//                                                                kms = kms + (totlist.get(k) * 1000);
//                                                                k++;
//                                                            }
//                                                        } else if (M.isKitchenPrinter(context) && KitchenGlobals.deviceType != 0)
//                                                            kitchenreciept(null, null, KitchenGlobals.deviceType, true,M.isadvanceprint(M.key_kitchen,context),M.retriveVal(M.key_kitchen_width,context),null);
//                                                        new Handler().postDelayed(new Runnable() {
//                                                            public void run() {
//                                                                Toast.makeText(context, R.string.order_place_success, Toast.LENGTH_SHORT).show();
//                                                                onBackPressed();
//                                                            }
//                                                        }, kplist.size() * 1000);
//                                                    }
//                                                }).create().show();
//                            }
//                        }, kplist.size() * 1000);
//                    } else {
//                        Toast.makeText(context, R.string.order_place_success, Toast.LENGTH_SHORT).show();
//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                onBackPressed();
//                            }
//                        },delayTm);
//                    }
                } catch (JSONException e) {
                    onBackPressed();
                }
            }
        }else {
            Toast.makeText(context, R.string.select_item, Toast.LENGTH_SHORT).show();
        }
    }

    private void paymentDone(){
        if(payment_mode.equals("-5")){
            SplitBillActivity.discountPojo=discountPojo;
            Intent it =new Intent(context,SplitBillActivity.class);
            it.setAction(action);
            it.putExtra("ot",ordertype);
            it.putExtra("en_ot",en_ordertype);
            if(discountPojo!=null)
                it.putExtra("dis_per","");
            else
                it.putExtra("dis_per",dis_per+"");
            Double sper=0.0;
            if(orderData.getService_charge()!=null && !orderData.getService_charge().isEmpty()) {
                sper = (Double.parseDouble(orderData.getService_charge()) * 100) / Double.parseDouble(tvgrand.getTag().toString());
            }
            it.putExtra("ser_per",sper+"");
            it.putExtra("order_no",order_no);
            it.putExtra("token",token_number);
            it.putExtra("table_id",tblid);
            it.putExtra("table_nm",tblnm);
            it.putExtra("order_id",orderid);
            startActivity(it);
        } else if(pointperamt!=0){
            searchPhone();
        } else if(M.isTrackCust(context))
            cust_detail(null);
        else {
            checkPayment("", "", "", "", "","");
        }
    }

    private void searchPhone(){
        pointJSON=null;
        phDialog=new Dialog(context);
        phDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        phDialog.setContentView(R.layout.dialog_loyalty_point);
        phDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);

        etsearch=(AutoCompleteTextView)phDialog.findViewById(R.id.etsearch);
        etsearch.setTypeface(AppConst.font_regular(context));
        etname=(EditText)phDialog.findViewById(R.id.etcustomername);
        etname.setTypeface(AppConst.font_regular(context));
        etname.setFocusable(false);
        etphone=(EditText)phDialog.findViewById(R.id.etphone);
        etphone.setTypeface(AppConst.font_regular(context));
        etemail=(EditText)phDialog.findViewById(R.id.etemail);
        etemail.setTypeface(AppConst.font_regular(context));
        etaddress=(EditText)phDialog.findViewById(R.id.etaddress);
        etaddress.setTypeface(AppConst.font_regular(context));
        ettax=(EditText)phDialog.findViewById(R.id.etgstno);
        ettax.setTypeface(AppConst.font_regular(context));
        TextInputLayout til=(TextInputLayout)phDialog.findViewById(R.id.til_gst);
        til.setTypeface(AppConst.font_regular(context));
        til.setVisibility(View.VISIBLE);
        LinearLayout ivsearch=(LinearLayout)phDialog.findViewById(R.id.ivsearch);
        final LinearLayout llpoint=(LinearLayout)phDialog.findViewById(R.id.llpoint);
        llpoint.setVisibility(View.GONE);
        lladdress=(LinearLayout)phDialog.findViewById(R.id.lladdress);
        final LinearLayout btnredeem=(LinearLayout) phDialog.findViewById(R.id.llredeem);
        Button btnpay=(Button)phDialog.findViewById(R.id.btnpay);
        btnpay.setTypeface(AppConst.font_regular(context));
        btnpay.setText("Pay "+AppConst.currency+" "+pf.setFormat(dtvgrand.getTag().toString()));
        Button btnskip=(Button)phDialog.findViewById(R.id.btnskip);
        btnskip.setTypeface(AppConst.font_regular(context));
        tvpoint=(TextView)phDialog.findViewById(R.id.tvpoints);
        tvpay=(TextView)phDialog.findViewById(R.id.tvpay);
        tvpay.setTag(dtvgrand.getTag().toString());
        tvpay.setText("Pay "+AppConst.currency+" "+pf.setFormat(dtvgrand.getTag().toString()));
        tvredeempoint=(TextView)phDialog.findViewById(R.id.tvredeem);
        tvredeempoint.setTag("0");
        tvselect=(TextView)phDialog.findViewById(R.id.tvselect);

        ivsearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etname.setText("");
                etphone.setText("");
                etemail.setText("");
                etaddress.setText("");
                tvpoint.setText("0");
                tvpay.setTag(dtvgrand.getTag().toString());
                tvredeempoint.setTag("0");
                tvselect.setVisibility(View.GONE);
                pointJSON=null;
                if(etsearch.getText().toString().trim().length()>0) {
                    InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    search_cust(etsearch.getText().toString());
                }else
                    etsearch.setError("Empty Search");
            }
        });

        btnredeem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(etphone.getText().toString().trim().length()>0) {
                    return_cash = "";
                    cash = "";
                    try {
                        Double pamt = Double.parseDouble(tvredeempoint.getTag().toString()) / Double.parseDouble(M.getPointsAmt(context));
                        pointJSON = new JSONObject();
                        pointJSON.put("redeem_points", tvredeempoint.getTag().toString());
                        pointJSON.put("redeem_amount", pamt + "");
                        pointJSON.put("points_per_one_currency", M.getPointsAmt(context));
                    } catch (JSONException e) {
                    }
                    phDialog.dismiss();
                    dtvgrand.setTag(tvpay.getTag().toString());
                    checkPayment("", etname.getText().toString(), etphone.getText().toString(), etemail.getText().toString(), etaddress.getText().toString(),ettax.getText().toString());
                }else
                    Toast.makeText(context,"Customer Detail required",Toast.LENGTH_SHORT).show();
            }
        });

        tvredeempoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnredeem.performClick();
            }
        });

        tvpay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnredeem.performClick();
            }
        });

        btnskip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pointJSON=null;
                phDialog.dismiss();
                checkPayment("", "", "", "", "","");
            }
        });

        btnpay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    pointJSON=new JSONObject();
                    pointJSON.put("redeem_points","0");
                    pointJSON.put("redeem_amount","0");
                    pointJSON.put("points_per_one_currency",M.getPointsAmt(context));
                }catch (JSONException e){}
                phDialog.dismiss();
                checkPayment("", etname.getText().toString(), etphone.getText().toString(), etemail.getText().toString(), etaddress.getText().toString(),ettax.getText().toString());
            }
        });

        tvpoint.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                double currpoint = Double.parseDouble(tvpoint.getText().toString());
                double pointperamt = Double.parseDouble(M.getPointsAmt(context));
                if(currpoint>0 && pointperamt!=0) {
                    llpoint.setVisibility(View.VISIBLE);
                    btnredeem.setVisibility(View.VISIBLE);
                    double paypointamt = Double.parseDouble(dtvgrand.getTag().toString()) * pointperamt;

                    if (currpoint >= paypointamt) {
                        tvredeempoint.setText("Redeem "+paypointamt + " Points");
                        tvredeempoint.setTag(paypointamt + "");
                        tvpay.setText("Pay "+AppConst.currency+pf.setFormat("0"));
                        tvpay.setTag("0");
                    }else{
                        double rpay=Double.parseDouble(dtvgrand.getTag().toString())-(currpoint/pointperamt);
                        tvredeempoint.setText("Redeem "+currpoint + " Points");
                        tvredeempoint.setTag(currpoint + "");
                        tvpay.setText("Pay "+AppConst.currency+" "+pf.setFormat(rpay+""));
                        tvpay.setTag(pf.setFormat(rpay+""));
                    }

                }else {
                    llpoint.setVisibility(View.GONE);
                    btnredeem.setVisibility(View.GONE);
                    tvredeempoint.setText("Redeem 0 Points");
                    tvredeempoint.setTag("0");
                    tvpay.setText("Pay "+AppConst.currency+" "+pf.setFormat(dtvgrand.getTag().toString()));
                    tvpay.setTag(dtvgrand.getTag().toString());
                }
            }
        });

        phDialog.show();
        Log.d(TAG,tblid+"--"+orderData.getCust_name()+"-"+orderData.getCust_phone());
        if(tblid.equals("0")){
            if(custnm!=null && custnm.trim().length()>0) {
                etname.setText(custnm);
                etsearch.setText(etname.getText().toString());
            }
            if(custph!=null && custph.trim().length()>0) {
                etphone.setText(custph);
                etsearch.setText(etphone.getText().toString());
            }
            if(etsearch.getText().toString().length()>0)
                ivsearch.performClick();
        }else if(AppConst.selCust!=null) {
            etsearch.setText(AppConst.selCust.getPhone_no());
            if(etsearch.getText().toString().trim().length()>0)
                ivsearch.performClick();
        }

    }

    public void cust_detail(String search_phone){
        dialogcust=new Dialog(context);
        dialogcust.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogcust.setContentView(R.layout.dialog_customer);
        dialogcust.setCancelable(false);
        dialogcust.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialogcust.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        LinearLayout ivsearch=(LinearLayout)dialogcust.findViewById(R.id.ivsearch);
        LinearLayout llsearch=(LinearLayout)dialogcust.findViewById(R.id.llsearch);
        TextView custTitle=(TextView)dialogcust.findViewById(R.id.tvtitle);
        TextView ivclose=(TextView)dialogcust.findViewById(R.id.ivclose);
        etsearch=(AutoCompleteTextView)dialogcust.findViewById(R.id.etsearch);
        etsearch.setTypeface(AppConst.font_regular(context));
        etname=(EditText)dialogcust.findViewById(R.id.etcustomername);
        etname.setTypeface(AppConst.font_regular(context));
        etphone=(EditText)dialogcust.findViewById(R.id.etphone);
        etphone.setTypeface(AppConst.font_regular(context));
        etemail=(EditText)dialogcust.findViewById(R.id.etemail);
        etemail.setTypeface(AppConst.font_regular(context));
        etaddress=(EditText)dialogcust.findViewById(R.id.etaddress);
        etaddress.setTypeface(AppConst.font_regular(context));
        ettax=(EditText)dialogcust.findViewById(R.id.etgstno);
        ettax.setTypeface(AppConst.font_regular(context));
        TextInputLayout til=(TextInputLayout)dialogcust.findViewById(R.id.til_gst);
        til.setTypeface(AppConst.font_regular(context));
        til.setVisibility(View.VISIBLE);
        final EditText etnote=(EditText)dialogcust.findViewById(R.id.etnote);
        etnote.setTypeface(AppConst.font_regular(context));
        etnote.setVisibility(View.VISIBLE);
        btnsubmit=(TextView)dialogcust.findViewById(R.id.btnsubmit);
        btnsubmit.setText(context.getString(R.string.complete_checkout));
        tvselect=(TextView)dialogcust.findViewById(R.id.tvselect);

        if(custnm!=null && !custnm.isEmpty())
            etname.setText(custnm);
        if(custph!=null && !custph.isEmpty())
            etphone.setText(custph);

        if(search_phone!=null){
            custTitle.setText(getString(R.string.add_new_customer));
            llsearch.setVisibility(View.GONE);
            try {
                Long num = Long.parseLong(search_phone);
                etphone.setText(num + "");
            } catch (NumberFormatException e) {
                String regexStr = "^(?:(?:\\+?1\\s*(?:[.-]\\s*)?)?(?:\\(\\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\\s*\\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\\s*(?:[.-]\\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\\s*(?:[.-]\\s*)?([0-9]{4})(?:\\s*(?:#|x\\.?|ext\\.?|extension)\\s*(\\d+))?$";
                if(etsearch.getText().toString().matches(regexStr))
                    etphone.setText(search_phone);
                else
                    etname.setText(search_phone);
            }
            etphone.setSelection(etphone.getText().length());
            etname.setSelection(etname.getText().length());
        }

        ivsearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etphone.setText("");
                etname.setText("");
                if(etsearch.getText().toString().trim().length()>0) {
                    search_cust(etsearch.getText().toString());
                    try {
                        Long num = Long.parseLong(etsearch.getText().toString());
                        etphone.setText(num + "");
                    } catch (NumberFormatException e) {
                        String regexStr = "^(?:(?:\\+?1\\s*(?:[.-]\\s*)?)?(?:\\(\\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\\s*\\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\\s*(?:[.-]\\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\\s*(?:[.-]\\s*)?([0-9]{4})(?:\\s*(?:#|x\\.?|ext\\.?|extension)\\s*(\\d+))?$";
                        if(etsearch.getText().toString().matches(regexStr))
                            etphone.setText(etsearch.getText().toString());
                        else
                            etname.setText(etsearch.getText().toString());
                    }
                    etphone.setSelection(etphone.getText().length());
                    etname.setSelection(etname.getText().length());
                }else
                    M.hideLoadingDialog();
            }
        });

        ivclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogcust.dismiss();
            }
        });

        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String nm = "";
                if (etname.getText().toString().trim().length() > 0)
                    nm = etname.getText().toString();
                String email = "";
                if (etemail.getText().toString().trim().length() > 0)
                    email = etemail.getText().toString();
                String address = "";
                if (etaddress.getText().toString().trim().length() > 0)
                    address = etaddress.getText().toString();
                String phone = "";
                if (etphone.getText().toString().trim().length() > 0) {
                    phone = etphone.getText().toString();
                    customernum = phone;
                }
                String cmt ="";
                if (etnote.getText().toString().trim().length() > 0)
                    cmt = etnote.getText().toString();

                dialog.dismiss();
                btnsubmit.setEnabled(false);

                checkPayment(cmt, nm, phone, email, address,ettax.getText().toString());

            }
        });
        dialogcust.show();
    }

    private void phonepeIntegrate(){
        if(connectionDetector.isConnectingToInternet()){
            Date todt=new Date();
            String timestamp= AppConst.arabicToEng(todt.getTime()+""),spm="",spt="";
            String phpe_qrcode=M.retriveVal(M.key_phonepe_paymode_qrcode,context);
            String phpe=M.retriveVal(M.key_phonepe_paymode,context);
            if(phpe_qrcode!=null && payment_mode.equalsIgnoreCase(phpe_qrcode)) {
                spm = "phonepe";
                spt="dynamicqr";
                M.showLoadingDialog(context);
                OrderAPI mAuthenticationAPI = APIServiceHeader.createService(context, OrderAPI.class);
                Call<TransactionPojo> call = mAuthenticationAPI.initTransPhonePe(restaurantid,runiqueid,M.getWaiterid(context),order_no,timestamp,dtvgrand.getTag().toString(),spm,spt);
                call.enqueue(new retrofit2.Callback<TransactionPojo>(){
                    @Override
                    public void onResponse(Call<TransactionPojo> call, Response<TransactionPojo> response) {
                        M.hideLoadingDialog();
                        TransactionPojo res=response.body();
                        if(res!=null) {
                            if (res.getSuccess().equals("true")) {
                                String txt = res.getData().getQrString();
                                try {
                                    qrbitmap = BitmapUtil.generateBitmap(txt, 9, 400, 400);
                                    if (qrbitmap != null) {
                                        onlineTransDialog = new OnlineTransDialog(context, new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {//print click
                                                if (Globals.deviceType != 0 && M.isCashPrinter(context)) {
                                                    createQRReceipt();
                                                }
                                            }
                                        }, new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {//success click
                                                transid = onlineTransDialog.transJson;
                                                finishOrder(s_cmt,s_cust_nm,s_cust_ph,s_cust_mail,s_cust_address,s_tax);
                                                onlineTransDialog.dismiss();
                                            }
                                        });
                                        onlineTransDialog.setData(order_no, pf.setFormat(dtvgrand.getTag().toString()), qrbitmap, res);
                                        onlineTransDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                            @Override
                                            public void onDismiss(DialogInterface dialog) {
                                                onlineTransDialog.onDismiss();
                                            }
                                        });
                                        onlineTransDialog.show();
                                    }
                                } catch (Exception we) {
                                    we.printStackTrace();
                                }
                            } else if (res.getMessage() != null && !res.getMessage().isEmpty()) {
                                Toast.makeText(context, res.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<TransactionPojo> call, Throwable t) {
                        M.hideLoadingDialog();
                    }
                });
            }else if(phpe!=null && payment_mode.equalsIgnoreCase(phpe)){
                if(s_cust_ph!=null && !s_cust_ph.isEmpty()) {
                    spm = "phonepe";
                    spt = "collectphonepe";
                    M.showLoadingDialog(context);
                    OrderAPI mAuthenticationAPI = APIServiceHeader.createService(context, OrderAPI.class);
                    Call<TransactionPojo> call = mAuthenticationAPI.collectPayment(restaurantid, runiqueid, M.getWaiterid(context), order_no, timestamp, dtvgrand.getTag().toString(), spm, spt, s_cust_ph);
                    String finalSpm = spm;
                    String finalSpt = spt;
                    call.enqueue(new retrofit2.Callback<TransactionPojo>() {
                        @Override
                        public void onResponse(Call<TransactionPojo> call, Response<TransactionPojo> response) {
                            M.hideLoadingDialog();
                            TransactionPojo res = response.body();
                            if (res != null) {
                                if (res.getSuccess().equals("true")) {
                                    onlineTransDialog = new OnlineTransDialog(context,null, new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {//success click
                                            transid = onlineTransDialog.transJson;
                                            finishOrder(s_cmt,s_cust_nm,s_cust_ph,s_cust_mail,s_cust_address,s_tax);
                                            onlineTransDialog.dismiss();
                                        }
                                    });
                                    onlineTransDialog.setData(order_no, pf.setFormat(dtvgrand.getTag().toString()), null, res);
                                    onlineTransDialog.show();
                                } else if (res.getMessage() != null && !res.getMessage().isEmpty()) {
                                    Toast.makeText(context, res.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<TransactionPojo> call, Throwable t) {
                            M.hideLoadingDialog();
                        }
                    });
                }else
                    Toast.makeText(context,getString(R.string.empty_phone_no),Toast.LENGTH_SHORT).show();
            }
        }else
            Toast.makeText(context,R.string.no_internet_alert,Toast.LENGTH_SHORT).show();
    }

    private void checkPayment(String comment, String custName, String custphone, final String custemail, String custaddress,String taxno){
        s_cmt=comment;
        s_cust_nm=custName;
        s_cust_ph=custphone;
        s_cust_address=custaddress;
        s_cust_mail=custemail;
        s_tax=taxno;
        if(payment_mode_text!=null && M.retriveVal(M.key_phonepe_paymode_qrcode,context)!=null && payment_mode.equalsIgnoreCase(M.retriveVal(M.key_phonepe_paymode_qrcode,context))){
            phonepeIntegrate();
        }else if(payment_mode_text!=null && M.retriveVal(M.key_phonepe_paymode,context)!=null && payment_mode.equalsIgnoreCase(M.retriveVal(M.key_phonepe_paymode,context))){
            phonepeIntegrate();
        }else
            finishOrder(comment, custName, custphone, custemail, custaddress,taxno);
    }

    public void finishOrder(String comment, String custName, String custphone, final String custemail, String custaddress,String taxno){
        M.showLoadingDialog(context);
        if(payment_mode_text.equalsIgnoreCase("cash")){
            cash=editText.getText().toString();
            return_cash=retamount+"";
            if(cash.trim().length()<=0) {
                cash = "";
                return_cash="";
            }
        }
        customername = custName ;
        JSONObject jObj=new JSONObject();
        jsonObject=new JSONObject();
        try {
            jObj.put("cash",cash);
            jObj.put("cust_address",custaddress);
            jObj.put("cust_email",custemail);
            jObj.put("cust_name",customername);
            jObj.put("cust_phone",custphone);
            jObj.put("cust_tax_no",taxno);
            jObj.put("grand_total",dtvgrand.getTag().toString());
            jObj.put("order_comment",comment);
            jObj.put("pay_mode",payment_mode);
            jObj.put("pay_mode_text",payment_mode_text);
            jObj.put("return_cash",return_cash);
            jObj.put("total_amt",pf.setFormat(llstd.getTag().toString()));
            jObj.put("tableid",tblid+"");
            jObj.put("tip",orderData.getTip());
            if(transid!=null && !transid.isEmpty())
                jObj.put("trans_id",transid);
            ArrayList<Tax> tlist=new ArrayList<>();
            JSONArray jArray=new JSONArray();
            for(TaxInvoicePojo p:taxDisList){
                JSONObject jt=new JSONObject();
                jt.put("tax_id",p.getId());
                jt.put("tax_name",p.getName());
                jt.put("tax_per",p.getPer());
                jt.put("tax_value",pf.setFormat(p.getAmount_total()));
                jArray.put(jt);

                Tax t=new Tax();
                t.setTax_id(p.getId());
                t.setTax_per(p.getPer());
                t.setTax_value(pf.setFormat(p.getAmount_total()));
                t.setTax_name(p.getName());
                tlist.add(t);
            }

            jObj.put("taxes",jArray);
            if(is_split)
                jObj.put("issplit","yes");
            else
                jObj.put("issplit","no");
            jObj.put("split_array",splitArray+"");
            if(M.getRoundFormat(context)) {
                //Rounding JSON
                Double billgrand=Double.parseDouble(dtvgrand.getTag().toString())-Double.parseDouble(tvrounding.getTag().toString());
                JSONObject roundingJson = new JSONObject();
                roundingJson.put("rounding_id",RoundHelper.rounding_id);
                roundingJson.put("cash_rounding",pf.setFormat(tvrounding.getTag()+""));
                roundingJson.put("interval_val",RoundHelper.interval_val);
                roundingJson.put("original_total",pf.setFormat(billgrand+""));
                jObj.put("rounding_json",roundingJson+"");
            }
            if(pointJSON!=null){
                jObj.put("redeem_points",pointJSON+"");
            }
            if(orderData.getIsmerge()!=null && !orderData.getIsmerge().isEmpty())
                jObj.put("is_merge",orderData.getIsmerge());
            jObj.put("order_id",orderid);
            jObj.put("tabel_id",tblid);

            JSONObject finishObj=new JSONObject();
            finishObj.put("key",WifiHelper.api_finish_order);
            finishObj.put("msg",jObj+"");
            finishObj.put("user_ip",getLocalIp());
            sendToServer(finishObj+"");
            jsonObject=jObj;
            M.setSplitOrder(null,context);
            if(dialogcust!=null && dialogcust.isShowing())
                dialogcust.dismiss();
            qrbitmap=null;
        } catch (Exception e) {
            Log.d(TAG,"error:"+e.getMessage());
            e.printStackTrace();
        }
    }

    public void buildJsonSplit(String comment, String custName, String custphone,String amt_due) {
        DishOrderPojo compItem=null;
        if(discountPojo!=null){
            if(discountPojo.getDiscount_type().equalsIgnoreCase("complementary")){
                DBDishes dbDishes=new DBDishes(context);
                DishPojo model=dbDishes.getDishData(discountPojo.getItem_id(),context);
                DishOrderPojo custorder = new DishOrderPojo();
                custorder.setDishid(model.getDishid());
                custorder.setDishname(model.getDishname());
                custorder.setQty("1");
                custorder.setIsnew(true);
                custorder.setStatus("0");
                custorder.setPrefid("0");
                custorder.setPrice(pf.setFormat("0"));
                custorder.setDiscount(model.getPrice());
                custorder.setTot_disc(model.getPrice());
                custorder.setOffer(discountPojo.getId());
                custorder.setComment("");
                custorder.setPriceperdish(model.getPrice());
                custorder.setCusineid(model.getCusineid());
                custorder.setDescription(model.getDescription());
                custorder.setDishimage(model.getDishimage());
                custorder.setPreflag(model.getPreflag());
                custorder.setPre(model.getPre());
                custorder.setInventory_item(model.getInventory_item());
                custorder.setDish_comment("");
                custorder.setTm(new Date().getTime()+"");
                AppConst.dishorederlist.add(custorder);
                AppConst.selidlist.add(Integer.parseInt(model.getDishid()));
                compItem=custorder;
                dis_amt=dis_amt+Double.parseDouble(custorder.getTot_disc());
            }
        }
        ordercomment = comment;
        String userid = M.getWaiterid(context);
        jsonObject = new JSONObject();
        jsonArray = new JSONArray();
        tm = AppConst.arabicToEng(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        if(totlist==null)
            totlist=new ArrayList<>();
        totlist.clear();

        cash = "";
        return_cash = "";
        customername = custName;
        try {
            for (int i = 0; i < AppConst.dishorederlist.size(); i++) {
                DishOrderPojo dishOP=AppConst.dishorederlist.get(i);
                JSONObject dishes = new JSONObject();
                if (AppConst.dishorederlist.get(i).isnew()) {
                    int quan = Integer.valueOf(AppConst.dishorederlist.get(i).getQty());
                    float priceperdish = Float.valueOf(AppConst.dishorederlist.get(i).getPrice());
                    String tot_weight="0";
                    if(AppConst.dishorederlist.get(i).getWeight()!=null && AppConst.dishorederlist.get(i).getWeight().trim().length()>0)
                        tot_weight=AppConst.dishorederlist.get(i).getWeight();
                    priceperdish = quan * priceperdish;
                    finalprice = String.valueOf(priceperdish);
                    dishes.put("dishid", AppConst.dishorederlist.get(i).getDishid());
                    dishes.put("cusineid", AppConst.dishorederlist.get(i).getCusineid());
                    dishes.put("dishname", AppConst.dishorederlist.get(i).getDishname());
                    dishes.put("quantity", AppConst.dishorederlist.get(i).getQty());
                    dishes.put("price", finalprice);
                    dishes.put("rate", AppConst.dishorederlist.get(i).getPrice());
                    dishes.put("weight",tot_weight+"");
                    dishes.put("unit_name",AppConst.dishorederlist.get(i).getUnitname());
                    dishes.put("unit_sort_name",AppConst.dishorederlist.get(i).getSort_nm());
                    if(AppConst.dishorederlist.get(i).getTax_data()!=null && AppConst.dishorederlist.get(i).getTax_data().size()>0){
                        JSONArray ja=new JSONArray();
                        for(TaxData p:AppConst.dishorederlist.get(i).getTax_data()){
                            JSONObject jt=new JSONObject();
                            jt.put("tax_name",p.getText());
                            jt.put("tax_per",p.getValue());
                            jt.put("tax_value",pf.setFormat(p.getTax_value())+"");
                            jt.put("tax_id",p.getId());
                            ja.put(jt);
                        }
                        dishes.put("taxes_data",ja);
                    }
                    if(AppConst.dishorederlist.get(i).getUnitid()!=null && AppConst.dishorederlist.get(i).getUnitid().trim().length()>0)
                        dishes.put("unit_id",AppConst.dishorederlist.get(i).getUnitid());
                    else
                        dishes.put("unit_id","0");

                    dishes.put("preferencesid", AppConst.dishorederlist.get(i).getPrefid());
                    dishes.put("prenm", AppConst.dishorederlist.get(i).getPrenm());
                    dishes.put("dish_comment", AppConst.dishorederlist.get(i).getDish_comment());
                    if (dishOP.getCombo_list() != null) {
                        if (dishOP.getCombo_list().size() > 0) {
                            String cmbname = "";
                            for (ComboModel comboModel : dishOP.getCombo_list()) {
                                String itemname = comboModel.getItem_name();
                                if(cmbname.trim().length()==0)
                                    cmbname=itemname;
                                else
                                    cmbname = cmbname + "," +itemname;
                            }
                            dishes.put("combo",cmbname);
                        }
                    }
                    if(dishOP.getOffer()!=null && dishOP.getOffer().trim().length()>0){
                        if(discountPojo!=null && discountPojo.getId().equals(dishOP.getOffer())){
                            dishes.put("offer_id",dishOP.getOffer());
                        }else {
                            JSONObject jOffer = new JSONObject(dishOP.getOffer());
                            dishes.put("offer_id", jOffer.getString("id"));
                        }
                    }
                    if(dishOP.getTot_disc()!=null && dishOP.getTot_disc().trim().length()>0){
                        dishes.put("discount", dishOP.getTot_disc());
                    }
                    jsonArray.put(dishes);

                }
            }
            jsonObject.put("order", jsonArray);
            jsonObject.put("user_id", userid);
            jsonObject.put("restaurant_id", restaurantid);
            jsonObject.put("rest_unique_id", runiqueid);
            jsonObject.put("pay_mode", payment_mode);
            jsonObject.put("pay_mode_text", payment_mode_text);
            jsonObject.put("order_time", tm);
            jsonObject.put("order_type", en_ordertype);
            jsonObject.put("order_type_lang",ordertype);
            jsonObject.put("cash", cash);
            jsonObject.put("return_cash", return_cash);
            jsonObject.put("total_amt", dtvamt.getTag().toString());
            jsonObject.put("grand_total", dtvgrand.getTag().toString());
            jsonObject.put("order_no", order_no);
            jsonObject.put("order_comment", comment);
            jsonObject.put("cust_name", custName);
            jsonObject.put("amount_due", amt_due);
            jsonObject.put("cust_phone", custphone);
            jsonObject.put("cust_email", "");
            jsonObject.put("cust_address", "");
            jsonObject.put("cgst_amount", "");
            jsonObject.put("sgst_amount", "");
            jsonObject.put("token", token_number);
            if(action.equals("dinein")){
                jsonObject.put("tableid",tblid);
                jsonObject.put("tablenm",tblnm);
                jsonObject.put("orderid",orderid);
                jsonObject.put("view","dineinoffline");
            }
            if(discountPojo!=null) {
                if(offer_id!=null && offer_id.trim().length()>0){
                    offer_id=offer_id+","+discountPojo.getId();
                    offer_name=offer_name+","+discountPojo.getName();
                }else{
                    offer_id=discountPojo.getId();
                    offer_name=discountPojo.getName();
                }
            }
            if(offer_id!=null && offer_id.trim().length()>0){
                jsonObject.put("offer_id", offer_id);
                jsonObject.put("offer_name", offer_name);
            }

            if(dis_per>0 || jsonObject.has("offer_id")) {
                Double finaldisc=Double.parseDouble(tvdiscount.getTag() + "");
                if(compItem!=null)
                    finaldisc=finaldisc+Double.parseDouble(compItem.getTot_disc());
                JSONObject j = new JSONObject();
                j.put("discount_amount", finaldisc+ "");
                j.put("discount_per", dis_per + "");
                if(discountPojo!=null)
                    j.put("discount_type", discountPojo.getDiscount_type());
                jsonObject.put("discount",j + "");
                jsonObject.put("bill_amt_discount",dis_amt);
            }

            jsonObject.put("split_data",splitArray);
            JSONArray taxjson = new JSONArray();
            for(TaxInvoicePojo p:taxInvoiceList){
                JSONObject jt=new JSONObject();
                jt.put("tax_name",p.getName());
                jt.put("tax_per",p.getPer());
                jt.put("tax_value",pf.setFormat(p.getAmount_total()));
                taxjson.put(jt);
            }
            jsonObject.put("taxes", taxjson);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //   Log.e(TAG,"json:"+ jsonObject.toString());
        M.setSplitOrder(jsonObject.toString(),context);
        M.hideLoadingDialog();
        if (M.isCashPrinter(context) && Globals.deviceType != 0){// && M.isAutoPrint(context)){
            createReceiptData("print");
        }
    }

    public void successscreen() {
        AppConst.selCust=null;
        dialog2=new Dialog(context,android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog2.setContentView(R.layout.dialog_success);
        dialog2.setCancelable(false);
        TextView txtcharge=(TextView)dialog2.findViewById(R.id.txtcharge);
        Button btn=(Button)dialog2.findViewById(R.id.btncontinue);
        btn.setTypeface(AppConst.font_regular(context));
        TextView btnsend=(TextView)dialog2.findViewById(R.id.btnsend);
        TextView btnprint=(TextView)dialog2.findViewById(R.id.btnprint);
        btnprint.setVisibility(View.VISIBLE);
        TextView btnkitchenprint=(TextView)dialog2.findViewById(R.id.btnkitchenprint);
        Button btnsharebill=(Button) dialog2.findViewById(R.id.btnsharebill);
        Button btnwhatsapp = (Button)dialog2.findViewById(R.id.btnwhatsapp);
        ImageView btnyes=(ImageView)dialog2.findViewById(R.id.ivback);
        dialog2.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        txtcharge.setText(AppConst.currency+" "+pf.setFormat(dtvgrand.getTag().toString()));
        TextView tv=(TextView)dialog2.findViewById(R.id.tv);
        tv.setTypeface(AppConst.font_regular(context));
        TextView tvreturn=(TextView)dialog2.findViewById(R.id.tvreturn);

        tv.setText(context.getString(R.string.order_completed));

        btnkitchenprint.setVisibility(View.VISIBLE);

        btnsend.setVisibility(View.GONE);

        if(return_cash!=null && return_cash.trim().length()>0 && Double.parseDouble(return_cash)>0){
            tvreturn.setVisibility(View.VISIBLE);
            tvreturn.setText(getString(R.string.txt_change)+": "+AppConst.currency+" "+pf.setFormat(return_cash));
        }else
            tvreturn.setVisibility(View.GONE);

        btnyes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog2.dismiss();
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        onBackPressed();
                    }
                }, 100);
            }
        });

        btnprint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                is_split=false;
                if(M.isCashPrinter(context) && Globals.deviceType!=0) {
                    createReceiptData("print");
                }
            }
        });

        btnkitchenprint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(M.isKitchenCatPrinter(context) && kplist!=null && kplist.size()>0){
                    int k=0,kms=0;
                    for(final KitchenPrinterPojo kp:kplist){
                        if(kp.getDeviceType()!=null && kp.getDeviceType().trim().length()>0 && !kp.getDeviceType().equals("0")) {
                            new Handler().postDelayed(new Runnable() {
                                public void run() {
                                    kitchenreciept(kp.getCategoryid(), kp.getId(),Integer.parseInt(kp.getDeviceType()),false,kp.isAdv(),kp.getPaper_width(),kp.getStar_settings());
                                }
                            },kms);
                        }
                        kms=kms+(totlist.get(k)*1000);
                        k++;
                    }
                }else if(M.isKitchenPrinter(context) && KitchenGlobals.deviceType!=0)
                    kitchenreciept(null,null,KitchenGlobals.deviceType,false,M.isadvanceprint(M.key_kitchen,context),M.retriveVal(M.key_kitchen_width,context),null);
            }
        });

        btnsharebill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                is_split=false;
                if(action.equals("dinein")){
                    createReceiptData("share");
                }else {
                    OtherPrintReceipt otherPrintReceipt = new OtherPrintReceipt(context, jsonObject);
                    otherPrintReceipt.share();
                }
            }
        });

        btnwhatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                is_split=false;

                if(M.getcountrycode(context)==null){

                }else if(M.getcountrycode(context).equals("")){

                }else {
                    if(customernum.length()>0) {
                        customernum = M.getcountrycode(context)+customernum;
                        OtherPrintReceipt otherPrintReceipt = new OtherPrintReceipt(context, jsonObject);
                        otherPrintReceipt.shareonwhatsapp(customernum);
                    }else{
                        sendwhatsapp();
                    }

                }

            }
        });

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog2.dismiss();
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        onBackPressed();
                    }
                }, 100);
            }
        });
        btnsend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendEmail(dialog2);
            }
        });

        if(dialog2!=null && !dialog2.isShowing())
            dialog2.show();
    }

    void sendwhatsapp(){

        final Dialog d=new Dialog(context);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.dialog_cust_phone);
        d.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
        TextView tvtitle=(TextView)d.findViewById(R.id.tvtitle);
        tvtitle.setTypeface(AppConst.font_regular(context));
        TextView btn=(TextView)d.findViewById(R.id.btnsubmit);
        btn.setTypeface(AppConst.font_regular(context));
        final EditText etphone=(EditText)d.findViewById(R.id.etphone);
        ImageView ivclose=(ImageView)d.findViewById(R.id.ivclose);


        ivclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                d.dismiss();
            }
        });
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(etphone.getText().toString().trim().length()<=0){
                    etphone.setError(context.getString(R.string.empty_phone));
                }else{
                    String phone = M.getcountrycode(context)+""+etphone.getText().toString().trim();
                    OtherPrintReceipt otherPrintReceipt = new OtherPrintReceipt(context, jsonObject);
                    otherPrintReceipt.shareonwhatsapp(phone);
                    d.dismiss();
                }
            }
        });
        d.show();

    }

    void sendEmail(final Dialog dialog2){
        try {
            final Dialog d=new Dialog(context);
            d.requestWindowFeature(Window.FEATURE_NO_TITLE);
            d.setContentView(R.layout.dialog_report_emails);
            d.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
            TextView tvtitle=(TextView)d.findViewById(R.id.tvtitle);
            tvtitle.setTypeface(AppConst.font_regular(context));
            tvtitle.setText(getString(R.string.txt_send_email));
            TextView btn=(TextView)d.findViewById(R.id.btnsubmit);
            btn.setTypeface(AppConst.font_regular(context));
            final EditText etemail=(EditText)d.findViewById(R.id.etemail);
            ImageView ivclose=(ImageView)d.findViewById(R.id.ivclose);
            String emails=jsonObject.getString("cust_email");
            if(emails!=null && emails.trim().length()>0)
                etemail.setText(emails);
            ivclose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    d.dismiss();
                }
            });
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(etemail.getText().toString().trim().length()<=0){
                        etemail.setError(context.getString(R.string.empty_email));
                    }else  if(!etemail.getText().toString().contains("@") || !etemail.getText().toString().contains(".")){
                        etemail.setError(context.getString(R.string.invalid_email));
                    }else{
                        if (view != null) {
                            InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }
                        M.showLoadingDialog(context);
                        String email=etemail.getText().toString();
                        SendOrderPojo sendOrderPojo=new SendOrderPojo();
                        try {
                            sendOrderPojo.setCash(jsonObject.getString("cash"));
                            sendOrderPojo.setCust_address(jsonObject.getString("cust_address"));
                            sendOrderPojo.setCust_email(email);
                            sendOrderPojo.setCust_name(jsonObject.getString("cust_name"));
                            sendOrderPojo.setCust_phone(jsonObject.getString("cust_phone"));
                            sendOrderPojo.setGrand_total(jsonObject.getString("grand_total"));
                            sendOrderPojo.setOrder_comment(jsonObject.getString("order_comment"));
                            sendOrderPojo.setOrder_no(jsonObject.getString("order_no"));
                            sendOrderPojo.setOrder_time(jsonObject.getString("order_time"));
                            sendOrderPojo.setOrder_type(jsonObject.getString("order_type"));
                            sendOrderPojo.setPay_mode(jsonObject.getString("pay_mode"));
                            sendOrderPojo.setRest_unique_id(jsonObject.getString("rest_unique_id"));
                            sendOrderPojo.setRestaurant_id(jsonObject.getString("restaurant_id"));
                            sendOrderPojo.setReturn_cash(jsonObject.getString("return_cash"));
                            sendOrderPojo.setUser_id(jsonObject.getString("user_id"));
                            sendOrderPojo.setTotal_amt(jsonObject.getString("total_amt"));
                            sendOrderPojo.setToken(jsonObject.getString("token"));
                            if(jsonObject.has("discount")) {
                                sendOrderPojo.setDiscount(jsonObject.getString("discount"));
                            }else {
                                sendOrderPojo.setDiscount("");
                            }
                            if(jsonObject.has("offer_id"))
                                sendOrderPojo.setOffer_id(jsonObject.getString("offer_id"));
                            else
                                sendOrderPojo.setOffer_id(null);
                            if(jsonObject.has("bill_amt_discount"))
                                sendOrderPojo.setBill_amt_discount(jsonObject.getString("bill_amt_discount"));
                            else
                                sendOrderPojo.setBill_amt_discount("0");
                            JSONArray j1=jsonObject.getJSONArray("taxes");
                            JSONArray j2=jsonObject.getJSONArray("order");
                            if(j1!=null && j1.length()>0){
                                List<Tax> taxList=new ArrayList<>();
                                for(int i=0;i<j1.length();i++){
                                    JSONObject j=j1.getJSONObject(i);
                                    Tax tax=new Tax();
                                    tax.setTax_name(j.getString("tax_name"));
                                    tax.setTax_per(j.getString("tax_per"));
                                    tax.setTax_value(j.getString("tax_value"));
                                    taxList.add(tax);
                                }
                                sendOrderPojo.setTaxes(taxList);
                            }
                            if(j2!=null && j2.length()>0){
                                List<Order> taxList=new ArrayList<>();
                                for(int i=0;i<j2.length();i++){
                                    JSONObject j=j2.getJSONObject(i);
                                    Order tax=new Order();
                                    if(j.has("dish_comment"))
                                        tax.setDish_comment(j.getString("dish_comment"));
                                    else
                                        tax.setDish_comment("");
                                    tax.setDishid(j.getString("dishid"));
                                    tax.setPreferencesid(j.getString("preferencesid"));
                                    tax.setPrice(j.getString("price"));
                                    tax.setQuantity(j.getString("quantity"));
                                    if(j.has("unit_id"))
                                        tax.setUnit_id(j.getString("unit_id"));
                                    else
                                        tax.setUnit_id("0");

                                    if(j.has("weight"))
                                        tax.setWeight(j.getString("weight"));
                                    else
                                        tax.setWeight("0");
                                    if(j.has("offer_id"))
                                        tax.setOffer_id(j.getString("offer_id"));
                                    else
                                        tax.setOffer_id("");
                                    if(j.has("discount"))
                                        tax.setDiscount(j.getString("discount"));
                                    else
                                        tax.setDiscount("");
                                    taxList.add(tax);
                                }
                                sendOrderPojo.setOrder(taxList);

                                //Rounding
                                if(jsonObject.has("rounding_json"))
                                    sendOrderPojo.setRounding_json(jsonObject.getString("rounding_json"));
                            }
                            if(connectionDetector.isConnectingToInternet()) {
                                OrderAPI mAuthenticationAPI = APIServiceHeader.createService(context, OrderAPI.class);
                                Call<SuccessPojo> call = mAuthenticationAPI.sendEmailTakeAway(sendOrderPojo);
                                call.enqueue(new retrofit2.Callback<SuccessPojo>() {
                                    @Override
                                    public void onResponse(Call<SuccessPojo> call, Response<SuccessPojo> response) {
                                        if (response.isSuccessful()) {
                                            SuccessPojo pojo = response.body();
                                            M.hideLoadingDialog();
                                            if (pojo != null) {
                                                if (pojo.getSuccess() == 1) {
                                                    dialog2.dismiss();
                                                    d.dismiss();
                                                    Toast.makeText(context,getString(R.string.email_sent), Toast.LENGTH_SHORT).show();
                                                    onBackPressed();
                                                }
                                            }

                                        } else {
                                            M.hideLoadingDialog();
                                            int statusCode = response.code();
                                            ResponseBody errorBody = response.errorBody();
                                            Log.d(TAG, "error:" + statusCode + " " + errorBody);
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<SuccessPojo> call, Throwable t) {
                                        M.hideLoadingDialog();
                                        Log.d(TAG, "fail:" + t.getMessage());
                                    }
                                });
                            }else{
                                M.hideLoadingDialog();
                                M.showToast(context,context.getString(R.string.no_internet_error));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            M.hideLoadingDialog();
                        }
                    }
                }
            });
            d.show();
        } catch (JSONException e) {
            e.printStackTrace();
            M.hideLoadingDialog();
        }
    }

    private boolean createQRReceipt() {
        StringBuilder textData = new StringBuilder();
        String method = "";

        pf.setPaperSize(M.retriveVal(M.key_bill_width,context));
        try {

            if(Globals.deviceType<5) {
                textData.append("$codepage3$");
                textData.append("$al_center$$al_center$");
                if (PrintFormat.setSize!= PrintFormat.smallsize) {
                    textData.append("$bigh$" + " $al_center$");
                    textData.append(pf.padLine2(M.getBrandName(context), "") + " $big$ \n\n");
                } else {
                    textData.append("$bold$" + "$bigl$" + " $al_center$");
                    textData.append(pf.padLine2(M.getBrandName(context), "") + " $big$ $unbold$\n\n");
                }
            }else{
                textData.append(pf.padLine2(M.getBrandName(context), "") + "\n\n");
            }

            textData.append(pf.padLine2(M.getRestName(context), "") + "\n");
            textData.append("\n" + pf.divider() + "\n");
            textData.append(pf.padLine2(getString(R.string.txt_token) + " # " + token_number, "") + "\n");
            textData.append(pf.padLine2(getString(R.string.invoice_no) + " # " + order_no, "") + "\n");
            textData.append(pf.padLine2("Total : " + dtvgrand.getTag().toString(), "") + "\n");
            textData.append("\n" + pf.divider() + "\n");

            if(qrbitmap!=null){
                if(Globals.deviceType==5){
                    AidlUtil.getInstance().printBitmap(qrbitmap);
                }else if(Globals.deviceType<5){
                    if (PrintFormat.setSize != PrintFormat.smallsize) {
                        textData.append("$al_center$ " + Globals.getImageDataPosPrinterLogo(qrbitmap));
                    } else {
                        textData.append(Globals.getImageDataPosPrinterLogo(qrbitmap));
                    }
                }
            }

            textData.append("\n"+pf.divider()+"\n");

            textData.append(pf.padLine2(getString(R.string.txt_powered_by) + " " + M.getfooterphone(context),"")+"\n");
            if( Globals.deviceType<5) {
                textData.append("\n\n$intro$$intro$$intro$$cutt$$intro$");
            }


            Log.d(TAG,"cash printer:");
            Log.d(TAG,textData+"");
            if(Globals.deviceType==5){
                textData.append("\n\n\n");
                AidlUtil.getInstance().printText(textData.toString(), PrintFormat.sunmi_font, true, false);
                AidlUtil.getInstance().cuttpaper();
            }else if(Globals.deviceType==9){
                JSONObject jdata=new JSONObject();
                jdata.put("data",textData.toString());
                StarPrintReceipt sp=new StarPrintReceipt(context,activity,jdata);
                if(M.isadvanceprint(M.key_bill,context))
                    sp.printusingstar();
                else
                    sp.printBill(textData.toString());
            }else {
                Intent sendIntent;
                if(M.isadvanceprint(M.key_bill,context)){
                    GlobalsNew.setReceipttype("2");
                    JSONObject jdata=new JSONObject();
                    jdata.put("data",textData.toString());
                    GlobalsNew.setJsonObject(jdata);
                    sendIntent = new Intent(context, PrintActivityNew.class);
                }else{
                    sendIntent = new Intent(context, PrintActivity.class);
                }
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, textData.toString());
                sendIntent.putExtra("internal", "1");
                sendIntent.setType("text/plain");
                this.startActivity(sendIntent);
            }

        }catch (JSONException e){
            Log.d(TAG,"json error:"+ e.getMessage());
        }

        textData = null;
        return true;
    }

    private boolean createReceiptData(String billinfo) {//billinfo : print,share

        StringBuilder textData = new StringBuilder();
        pf.setPaperSize(M.retriveVal(M.key_bill_width,context));
        try {

            if(M.isCashPrinter(context) || billinfo.equals("share")) {
                if(billinfo.equals("print") && Globals.deviceType!=5) {
                    textData.append("$codepage3$");
                    textData.append("$al_center$$al_center$");
                    textData.append("$bighw$" + " $al_center$");
                    textData.append(pf.padLine2(M.getBrandName(context), "") + " $big$ \n");
                }else {
                    if(M.isPrintLogo(context) && Globals.deviceType==5) {
                        String root = Environment.getExternalStorageDirectory().toString();
                        File myDir = new File(root + "/RoyalPOS/logo.png");
                        if (myDir.exists()) {
                            Bitmap myBitmap = BitmapFactory.decodeFile(myDir.getAbsolutePath());
                            AidlUtil.getInstance().printBitmap(myBitmap);
                        }
                    }
                    textData.append(pf.padLine2(M.getBrandName(context), "") + "\n\n");
                }
                textData.append(pf.padLine2(M.getRestName(context),"") + "\n");
                textData.append(pf.padLine2(M.getRestAddress(context) , "")+"\n");
                textData.append(pf.padLine2(getString(R.string.txt_phone) + ": " + M.getRestPhoneNumber(context),"") + "\n");
                if (M.getGST(context) != null && M.getGST(context).trim().length() > 0)
                    textData.append(pf.padLine2(getString(R.string.txt_tax) + " # " + M.getGST(context),"") + "\n");
                textData.append(pf.divider()+"\n");
                String orderTime= AppConst.arabicToEng(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                if(PrintFormat.setSize==PrintFormat.normalsize)
                    textData.append(pf.padLine2(getString(R.string.txt_order_by) + " : " + M.getWaitername(context),orderTime) + "\n");
                else {
                    textData.append(pf.padLine2(getString(R.string.txt_order_by) + " : " + M.getWaitername(context), "") + "\n");
                    textData.append(pf.padLine2(orderTime,"") + "\n");
                }

                if(action.equals("dinein")) {
                    textData.append(pf.padLine2(getString(R.string.txt_order)+" # "+order_no,"")+"\n");

                    textData.append(pf.divider()+"\n");
                    if(tblid.equals("0"))
                        textData.append(pf.padLine2(getString(R.string.txt_token)+" # "+token_number," #"+tblnm) +"\n");
                    else
                        textData.append(pf.padLine2(getString(R.string.txt_token)+" # "+token_number,getString(R.string.table).toUpperCase()+" #"+tblnm) +"\n");

                }else{
                    if(billinfo.equals("print") && Globals.deviceType!=5) {
                        textData.append("$bigw$" + " $al_center$");
                        textData.append(pf.padLine2(context.getResources().getString(R.string.txt_token) + " # " + token_number, "") + " $big$\n");
                    }else{
                        textData.append(pf.padLine2(context.getResources().getString(R.string.txt_token) + " # " + token_number, "") + "\n");
                    }

                    textData.append(pf.divider() + "\n");
                    textData.append(pf.padLine2(getString(R.string.txt_order) + " # " + order_no, "" + ordertype) + "\n");
                }

                textData.append(pf.divider()+"\n");
                if (customername!=null && customername.length() > 0) {
                    textData.append(pf.padLine2(getString(R.string.txt_customer_name) + "\t",customername)+ "\n");
                    textData.append(pf.divider()+"\n");
                }
                if (jsonObject!=null && jsonObject.has("cust_tax_no")) {
                    if(!jsonObject.getString("cust_tax_no").isEmpty()) {
                        textData.append(pf.padLine2(jsonObject.getString("cust_tax_no"),"") + "\n");
                        textData.append(pf.divider() + "\n");
                    }
                }
                if(jsonObject!=null && jsonObject.has("trans_id")){
                    String tid=jsonObject.getString("trans_id");
                    try {
                        JSONObject jt=new JSONObject(tid);
                        if(jt.has("type")){
                            if(jt.has("token")){
                                tid=jt.getString("token");
                            }
                        }
                        textData.append(pf.padLine2(tid, "") + "\n");
                    }catch (JSONException e){
                        textData.append(pf.padLine2(tid, "") + "\n");
                    }
                    textData.append(pf.divider() + "\n");
                }
                if(PrintFormat.setSize==PrintFormat.normalsize)
                    textData.append(pf.padLine1(getString(R.string.item), getString(R.string.txt_qty), getString(R.string.txt_rate),getString(R.string.txt_amount)) + "\n");
                else
                    textData.append(pf.padLine1(getString(R.string.item), getString(R.string.txt_qty), "",getString(R.string.txt_amount)) + "\n");
                textData.append(pf.divider()+"\n");
                int tot_qty=0,tot_item=0;
                for (int i = 0; i < AppConst.dishorederlist.size(); i++) {
                    tot_item = tot_item + 1;
                    String qty = AppConst.dishorederlist.get(i).getQty();
                    tot_qty=(Integer.parseInt(qty))+tot_qty;
                    String name = AppConst.dishorederlist.get(i).getDishname();

                    String price = AppConst.dishorederlist.get(i).getPrice();
                    String pricewt=AppConst.dishorederlist.get(i).getPrice_without_tax();
                    double finalpri=0.0;
                    if(!action.equals("dinein")) {
                        if(M.isPriceWT(context)){
                            finalpri = Double.valueOf(qty) * Double.valueOf(pricewt);
                            price = AppConst.dishorederlist.get(i).getPriceper_without_tax();
                        }else {
                            finalpri = Double.valueOf(qty) * Double.valueOf(price);
                            price = AppConst.dishorederlist.get(i).getPriceperdish();
                        }
                    }else {
                        if(M.isPriceWT(context)) {
                            finalpri = Double.valueOf(pricewt);
                            price = AppConst.dishorederlist.get(i).getPriceper_without_tax();
                        }else {
                            finalpri = Double.valueOf(price);
                            price = AppConst.dishorederlist.get(i).getPriceperdish();
                        }
                    }
                    String we="";
                    if(AppConst.dishorederlist.get(i).getWeight()!=null && AppConst.dishorederlist.get(i).getWeight().trim().length()>0
                            && Double.parseDouble(AppConst.dishorederlist.get(i).getWeight())>0){
                        we=AppConst.dishorederlist.get(i).getWeight()+AppConst.dishorederlist.get(i).getSort_nm();
                    }
                    textData.append(pf.padLine1(name+" "+we, qty+"", pf.setFormat(price), pf.setFormat(finalpri+"")) + "\n");

                    List<VarPojo> vlist=AppConst.dishorederlist.get(i).getVarPojoList();
                    if(vlist!=null && vlist.size()>0){
                        for(VarPojo v:vlist) {
                            if(M.isPriceWT(context))
                                textData.append(pf.padLine2("    " + v.getQuantity()+" X "+v.getName() + "-" + pf.setFormat(v.getAmount_wt()), "   ") + "\n");
                            else
                                textData.append(pf.padLine2("    " + v.getQuantity()+" X "+v.getName() + "-" + pf.setFormat(v.getAmount()), "   ") + "\n");
                        }
                    }
                    if (AppConst.dishorederlist.get(i).getCombo_list() != null) {
                        if (AppConst.dishorederlist.get(i).getCombo_list().size() > 0) {
                            for (ComboModel comboModel : AppConst.dishorederlist.get(i).getCombo_list()) {
                                String itemname = comboModel.getItem_name();
                                textData.append(pf.padLine1(itemname,  "", "","") + "\n");
                            }
                        }
                    }
                    String idisc=AppConst.dishorederlist.get(i).getTot_disc();
                    if(idisc!=null && idisc.trim().length()>0 && Double.parseDouble(idisc)>0){
                        textData.append(pf.padLine1("    " ,  " ", " ",getString(R.string.txt_save)+" "+pf.setFormat(idisc)) + "\n");
                    }
                }

                textData.append(pf.divider()+"\n");
                textData.append(pf.padLine2(context.getString(R.string.txt_items)+":"+tot_item ,context.getString(R.string.txt_qty)+":"+tot_qty) + "\n");
                textData.append(pf.divider()+"\n");
                //sub total
                textData.append(pf.padLine2(getString(R.string.txt_subtotal) , pf.setFormat(tvsub.getTag().toString())) + "\n");
                //discount
                JSONObject jdisc=null;
                if(action.equals("dinein") && orderData.getOrder_discount()!=null && orderData.getOrder_discount().trim().length()>0){
                    jdisc=new JSONObject(orderData.getOrder_discount());

                }else if(jsonObject!=null && jsonObject.has("discount")){
                    jdisc=new JSONObject(jsonObject.getString("discount"));
                }
                if(jdisc!=null && jdisc.has("discount_amount")) {
                    String pdisc = jdisc.getString("discount_amount");
                    if (pdisc != null && pdisc.trim().length() > 0 && Double.parseDouble(pdisc) > 0) {
                        textData.append(pf.padLine2(getString(R.string.txt_discount), pf.setFormat(jdisc.getString("discount_amount") + "")) + "\n");
                        if (offer_name != null && offer_name.trim().length() > 0)
                            textData.append(pf.padLine2("(" + offer_name + ")", "") + "\n");
                    }
                }
                //loyalty point
                if(jsonObject!=null && jsonObject.has("redeem_points")){
                    textData.append(pf.padLine2(getString(R.string.redeem_point)+"("+pointJSON.getString("redeem_points")+")",pf.setFormat(pointJSON.getString("redeem_amount"))) + "\n");
                    if(Globals.deviceType==5)
                        textData.append(pf.padLine2("("+pointJSON.getString("points_per_one_currency")+" Points=1)","  " )+ "\n");
                    else
                        textData.append("$small$"+pf.padLine2("("+pointJSON.getString("points_per_one_currency")+" Points=1)","  " )+ "$big$\n");
                }
                //tax
                if(taxInvoiceList!=null && taxInvoiceList.size()>0){
                    for(TaxInvoicePojo t:taxInvoiceList)
                        textData.append(pf.padLine2(t.getName()+" @"+t.getPer()+"%" ,pf.setFormat(t.getAmount_total())) + "\n");
                }
                if(orderData.getTip()!=null && !orderData.getTip().isEmpty()){
                    JSONObject tJ=new JSONObject(orderData.getTip());
                    if(tJ.has("tip_amount"))
                        textData.append(pf.padLine2(context.getString(R.string.txt_tip), pf.setFormat(tJ.getString("tip_amount"))) + "\n");

                }
                //Service Charge
                if (orderData.getService_charge()!=null && !orderData.getService_charge().isEmpty()){
                    textData.append(pf.padLine2(M.retriveVal(M.key_custom_service_charge,context), pf.setFormat(orderData.getService_charge())) + "\n");
                }
                //cash rounding
                if(tvrounding!=null && tvrounding.getTag()!=null){
                    if(Double.parseDouble(tvrounding.getTag().toString())!=0)
                        textData.append(pf.padLine2(getString(R.string.cash_rounding), pf.setFormat(tvrounding.getTag()+"")) + "\n");
                }
                //grand total
                String billtotal="0.0";
                if(dtvgrand!=null && dtvgrand.getTag()!=null)
                    billtotal=dtvgrand.getTag().toString();
                else if(tvg!=null && tvg.getTag()!=null)
                    billtotal=tvg.getTag().toString();

                if(billinfo.equals("share") || Globals.deviceType==5)
                    textData.append(pf.padLine3(context.getString(R.string.txt_total), pf.setFormat(billtotal)) + "\n");
                else {
                    if (PrintFormat.setSize == PrintFormat.normalsize) {
                        textData.append("$bighw$" + " $al_center$");
                        textData.append(pf.padLine3(context.getString(R.string.txt_total), pf.setFormat(billtotal)) + " $big$ \n");
                    } else {
                        textData.append("$bold$" + "$bigh$" + " $al_center$");
                        textData.append(pf.padLine3(context.getString(R.string.txt_total), pf.setFormat(billtotal)) + " $big$ $unbold$\n");
                    }
                }

                if(jsonObject!=null && jsonObject.has("amount_due"))
                    textData.append(pf.padLine2(getString(R.string.txt_amount_paid) ,pf.setFormat(jsonObject.getString("amount_due"))) + "\n");
                else if(jsonObject!=null && jsonObject.has("part_payment_amt")){
                    JSONArray ja=new JSONArray(jsonObject.getString("part_payment_amt"));
                    for(int a=0;a<ja.length();a++){
                        JSONObject jao=ja.getJSONObject(a);
                        textData.append(pf.padLine2(context.getString(R.string.txt_amount_paid), pf.setFormat(jao.getString("amount"))) + "\n");
                        textData.append(pf.padLine2(context.getString(R.string.payment_type),jao.getString("payment_mode_text")) + "\n");
                    }

                    if(jsonObject.has("part_amt_due"))
                        textData.append(pf.padLine2(getString(R.string.amount_due) ,pf.setFormat(jsonObject.getString("part_amt_due"))) + "\n");
                }

                if( return_cash!=null && return_cash.trim().length()>0 && Double.parseDouble(return_cash)!=0) {
                    textData.append("\n");
                    textData.append(pf.padLine2(getString(R.string.txt_cash), pf.setFormat(cash)) + "\n");
                    textData.append(pf.padLine2(getString(R.string.txt_change), pf.setFormat(return_cash)) + "\n");
                }

                if(qrbitmap!=null){
                    if(Globals.deviceType==5){
                        AidlUtil.getInstance().printBitmap(qrbitmap);
                    }else if(Globals.deviceType<5){
                        textData.append("Scan & Pay");
                        if (PrintFormat.setSize != PrintFormat.smallsize) {
                            textData.append("$al_center$ " + Globals.getImageDataPosPrinterLogo(qrbitmap));
                        } else {
                            textData.append(Globals.getImageDataPosPrinterLogo(qrbitmap));
                        }
                    }
                }

                textData.append(pf.divider()+"\n");
                if (M.getreceipt_footer(context).length() == 0) {
                    textData.append(getString(R.string.txt_thanku) +"\n"+ getString(R.string.txt_visitagain) + "\n");
                } else {
                    textData.append(pf.padLine2( M.getreceipt_footer(context),"") + "\n");
                }

                textData.append(pf.padLine2(getString(R.string.txt_powered_by) + " " + M.getfooterphone(context),"")+"\n");
                if(billinfo.equals("print") && Globals.deviceType!=5) {
                    textData.append("\n\n$intro$$intro$$intro$$intro$$cutt$$intro$");
                    textData.append("$drawer$");
                    textData.append("$drawer2$");
                }
                Log.d(TAG,"cash printer:");
                Log.d(TAG,textData+"");
                if(billinfo.equals("print")) {
                    if(Globals.deviceType==5){
                        textData.append("\n\n\n");
                        AidlUtil.getInstance().printText(textData.toString(), 24, true, false);

                        // AidlUtil.getInstance().openDrawer();
                        AidlUtil.getInstance().lcdmessage(""+payment_mode_text,"Total : "+billtotal);
                    }else if(Globals.deviceType==9){
                        JSONObject jdata=new JSONObject();
                        jdata.put("data",textData.toString());
                        StarPrintReceipt sp=new StarPrintReceipt(context,PlaceOrder.this,jdata);
                        if(M.isadvanceprint(M.key_bill,context))
                            sp.printusingstar();
                        else
                            sp.printBill(textData.toString());
                    }else {
                        Intent sendIntent;
                        if(M.isadvanceprint(M.key_bill,context)){
                            GlobalsNew.setReceipttype("2");
                            JSONObject jdata=new JSONObject();
                            jdata.put("data",textData.toString());
                            GlobalsNew.setJsonObject(jdata);
                            sendIntent = new Intent(context, PrintActivityNew.class);
                        }else{
                            sendIntent = new Intent(context, PrintActivity.class);
                        }
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_TEXT, textData.toString());
                        sendIntent.putExtra("internal", "1");
                        sendIntent.setType("text/plain");
                        this.startActivity(sendIntent);
                    }
                }else if(billinfo.equals("share")){
                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    sharingIntent.putExtra(Intent.EXTRA_TEXT, textData.toString());
                    context.startActivity(Intent.createChooser(sharingIntent, "Share using"));
                }

            }

        }catch (JSONException e){
            Log.d(TAG,"json error:"+ e.getMessage());
        }
//        catch (Exception e) {
//            Log.d(TAG,"error:"+ e.getMessage());
//            return false;
//        }

        textData = null;
        return true;
    }

    private boolean kitchenreciept(String catid,String pid,int devicetype,Boolean onlyNew,Boolean isAdv,String wdt,String star) {

        StringBuilder textData = new StringBuilder();
        if(devicetype<5)
            textData.append("$al_center$$al_center$");
        JSONObject jprint=new JSONObject();
        pf.setPaperSize(wdt);
        try {
            if(M.isCustomAllow(M.key_top_space,context))
                textData.append("\n\n\n");
            if(devicetype<5) {
                textData.append("$bigw$" + " $al_center$");
                textData.append(pf.padLine2(context.getResources().getString(R.string.txt_token) + " # " + token_number, "") + " $big$\n");
            }else{
                textData.append(pf.padLine2(context.getResources().getString(R.string.txt_token) + " # " + token_number, "") + "\n");
            }
            textData.append(pf.padLine2(M.getRestName(context),"")+"\n");
            jprint.put("restname",M.getRestName(context));
            textData.append(pf.divider()+"\n");
            String orderTime= AppConst.arabicToEng(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
            if(PrintFormat.setSize==PrintFormat.normalsize)
                textData.append(pf.padLine2(getString(R.string.txt_order_by)+" : "+M.getWaitername(context),orderTime)+"\n");
            else{
                textData.append(pf.padLine2(getString(R.string.txt_order_by)+" : "+M.getWaitername(context),"")+"\n");
                textData.append(pf.padLine2(orderTime,"")+"\n");
            }
            jprint.put("orderby",M.getWaitername(context));
            jprint.put("time",orderTime);
            if(action.equals("dinein")) {
                if(devicetype<5)
                    textData.append("$bold$");
                if(tblid.equals("0")){
                    textData.append(pf.padLine2(""," #"+tblnm)+"\n");
                    jprint.put("table"," #"+tblnm);
                }else{
                    textData.append(pf.padLine2("",getString(R.string.table).toUpperCase()+" #"+tblnm)+"\n");
                    jprint.put("table",getString(R.string.table).toUpperCase()+" #"+tblnm);
                }
                if(devicetype<5)
                    textData.append("$unbold$");
            }else {
                textData.append(pf.padLine2("", ordertype) + "\n");
                jprint.put("ordertype",ordertype +"");
            }
            jprint.put("token",getString(R.string.txt_token)+" # "+token_number);
            textData.append(pf.divider()+"\n");
            if(customername!=null && customername.length()>0) {
                textData.append(pf.padLine2(getString(R.string.customer),customername)+" \n");
                jprint.put("custnm",getString(R.string.customer)+" \t"+customername);
                textData.append(pf.divider()+"\n");
            }
            JSONArray ja=new JSONArray();

            for (int i = 0; i < AppConst.dishorederlist.size(); i++) {
                Boolean isprint=false;
                if(catid!=null && catid.trim().length()>0){
                    List<String> items = Arrays.asList(catid.split("\\s*,\\s*"));
                    if(items.contains(AppConst.dishorederlist.get(i).getCusineid()))
                        isprint=true;
                    else
                        isprint=false;
                }else{
                    isprint=true;
                }

                if(onlyNew && isprint && !AppConst.dishorederlist.get(i).isnew())
                    isprint=false;

                if(isprint) {
                    JSONObject jd=new JSONObject();
                    String qty = AppConst.dishorederlist.get(i).getQty();
                    jd.put("qty",qty);
                    String name = AppConst.dishorederlist.get(i).getDishname();
                    jd.put("name",name);
                    jd.put("cusineid",AppConst.dishorederlist.get(i).getCusineid());
                    String price = AppConst.dishorederlist.get(i).getPrice();
                    String we="";
                    if(AppConst.dishorederlist.get(i).getWeight()!=null && AppConst.dishorederlist.get(i).getWeight().trim().length()>0
                            && Double.parseDouble(AppConst.dishorederlist.get(i).getWeight())>0){
                        we="X"+AppConst.dishorederlist.get(i).getWeight()+AppConst.dishorederlist.get(i).getSort_nm();
                        jd.put("weight",we);
                    }
                    if(devicetype<5)
                        textData.append("$bold$"+pf.padLine2(qty+" X "+name+" "+we, "  ") + "$unbold$\n");
                    else
                        textData.append(pf.padLine2(qty+" X "+name+" "+we, "  ") + "\n");
                    String prefname = AppConst.dishorederlist.get(i).getPrenm();
                    if (prefname!=null && prefname.length() > 0) {
                        textData.append(pf.padLine2( "    " + prefname,  " ") + "\n");
                        jd.put("prenm",prefname);
                    }
                    if (AppConst.dishorederlist.get(i).getDish_comment() != null) {
                        if (AppConst.dishorederlist.get(i).getDish_comment().length() > 0) {
                            String dish_comment = AppConst.dishorederlist.get(i).getDish_comment();
                            textData.append(pf.padLine2( "    " + dish_comment," ") + "\n");
                            jd.put("dish_comment",dish_comment);
                        }
                    }

                    if(AppConst.dishorederlist.get(i).getCombo_list()!=null) {

                        if (AppConst.dishorederlist.get(i).getCombo_list().size() > 0) {
                            String cmbname = "";
                            for (ComboModel comboModel : AppConst.dishorederlist.get(i).getCombo_list()) {
                                String itemname = comboModel.getItem_name();
                                textData.append(pf.padLine2(itemname,  "  ") + "\n");
                                if(cmbname.trim().length()==0)
                                    cmbname=itemname;
                                else
                                    cmbname = cmbname +","+itemname;
                            }
                            jd.put("combo",cmbname);
                        }
                    }
                    ja.put(jd);
                }
            }
            jprint.put("item",ja);
            textData.append(pf.divider()+"\n");
            if(devicetype!=5)
                textData.append("$intro$$intro$$intro$$intro$$cutt$$intro$");
            DBKP dbkp=new DBKP(context);
            dbkp.addData(order_no,tm,jprint+"");
            Log.d(TAG,"Kitchen Data:"+pid);
            Log.d(TAG,textData+"");
            if(devicetype!=5) {
                Intent sendIntent;
                if (M.isSamePrinter(context))
                    sendIntent = new Intent(context, PrintActivity.class);
                else {
                    AsyncNewPrintNew.isKitchenAdv=isAdv;
                    if(isAdv){
                        KitchenGlobalsNew.setJsonObject(jprint);
                        sendIntent = new Intent(context, KitchenPrintActivityNew.class);
                        sendIntent.putExtra(Intent.EXTRA_TEXT,jprint+"");
                    }else {
                        sendIntent = new Intent(context, KitchenPrintActivity.class);
                        sendIntent.putExtra(Intent.EXTRA_TEXT, textData.toString());
                    }
                }
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, textData.toString());
                sendIntent.putExtra("printerid", pid);
                sendIntent.putExtra("internal", "1");
                sendIntent.setType("text/plain");
                this.startActivity(sendIntent);
            }else if(devicetype==9){
                StarPrintReceipt sp=new StarPrintReceipt(context,PlaceOrder.this,jprint);
                if(catid!=null) {
                    if(star!=null && !star.isEmpty()) {
                        JSONObject jstar=new JSONObject(star);
                        sp.printKitchen(context, textData.toString(), jstar.getString("model"),jstar.getString("settings"),Integer.parseInt(jstar.getString("modelIndex")));
                    }
                }else
                    sp.printKitchen(context,textData.toString(),M.getKitchenPrinterModel(context),M.retriveVal(M.key_star_setting,context),Integer.parseInt(M.retriveVal(M.key_star_modelindex,context)));
            }else{
                textData.append("\n\n");
                AidlUtil.getInstance().printText(textData.toString(), 20, true,false);
            }

        }
        catch (Exception e) {
            Log.d(TAG,"Exception printer----"+e.getMessage());

            return false;
        }

        textData = null;

        return true;
    }

    void getPaymentMode() {
        DBPaymentType dbPaymentType=new DBPaymentType(context);
        List<PaymentModePojo> resp=dbPaymentType.getPaymenttype();
        if(resp!=null && resp.size()>0) {
            if(plist!=null)
                plist.clear();
            else
                plist=new ArrayList<>();
            plist.addAll(resp);
            setchips();
        }else if(connectionDetector.isConnectingToInternet()){
            M.showLoadingDialog(context);
            OrderAPI mAuthenticationAPI = APIServiceHeader.createService(context, OrderAPI.class);
            Call<List<PaymentModePojo>> call = mAuthenticationAPI.getPaymentMode(restaurantid,runiqueid);
            call.enqueue(new retrofit2.Callback<List<PaymentModePojo>>() {
                @Override
                public void onResponse(Call<List<PaymentModePojo>> call, Response<List<PaymentModePojo>> response) {
                    M.hideLoadingDialog();
                    if (response.isSuccessful()) {
                        List<PaymentModePojo> resp = response.body();
                        if (resp != null) {
                            if(plist!=null)
                                plist.clear();
                            else
                                plist=new ArrayList<>();
                            plist.addAll(resp);
                            setchips();
                        }
                    } else {
                        int statusCode = response.code();
                        ResponseBody errorBody = response.errorBody();
                        Log.d(TAG, "error:" + statusCode + " " + errorBody);
                    }
                }

                @Override
                public void onFailure(Call<List<PaymentModePojo>> call, Throwable t) {
                    Log.d(TAG, "fail:" + t.getMessage());
                    M.hideLoadingDialog();
                }
            });
        }

    }

    private void setAddress(){
        final ArrayList<String> alist=new ArrayList<>();
        if(addressList!=null && addressList.size()>0){
            for(AddressPojo a: addressList){
                String address=a.getAddress();
                String hno=a.getHome_no();
                String landmark=a.getLandmark();
                String fieldadd="";
                if(hno!=null && hno.trim().length()>0)
                    fieldadd=hno+", ";
                if(landmark!=null && landmark.trim().length()>0)
                    fieldadd=fieldadd+landmark+", ";
                fieldadd=fieldadd+address;
                alist.add(fieldadd);
            }
        }
        if(alist.size()>0) {
            final Dialog addressD = new Dialog(context);
            addressD.requestWindowFeature(Window.FEATURE_NO_TITLE);
            addressD.setContentView(R.layout.dialog_address);
            addressD.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            ListView lv = (ListView) addressD.findViewById(R.id.lv);
            ArrayAdapter<String> ada = new ArrayAdapter<>(context, R.layout.spn_category_row, R.id.txt, alist);
            lv.setAdapter(ada);

            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    addressD.dismiss();
                    etaddress.setText(alist.get(i));
                }
            });

            addressD.show();
        }
    }

    private void fillAddress(CustomerPojo pojo){
        if(pojo!=null) {
            Boolean isNew = false;
            addressList = new ArrayList<>();
            if (pojo.getAddress_json() == null) {
                isNew = false;
            } else if (pojo.getAddress_json().size() == 0) {
                isNew = false;
            } else {
                isNew = true;
                addressList = (ArrayList<AddressPojo>) pojo.getAddress_json();
            }

            if (!isNew) {
                etaddress.setText(pojo.getAddress());
                tvselect.setVisibility(View.GONE);
            } else {
                tvselect.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        setAddress();
                    }
                });
                tvselect.setVisibility(View.VISIBLE);
                String address = addressList.get(0).getAddress();
                String hno = addressList.get(0).getHome_no();
                String landmark = addressList.get(0).getLandmark();
                String fieldadd = "";
                if (hno != null && hno.trim().length() > 0)
                    fieldadd = hno + ", ";
                if (landmark != null && landmark.trim().length() > 0)
                    fieldadd = fieldadd + landmark + ", ";
                fieldadd = fieldadd + address;
                etaddress.setText(fieldadd);
            }
            etaddress.setSelection(etaddress.getText().length());
        }
    }

    private void search_cust(final String txt) {
        if(connectionDetector.isConnectingToInternet()) {
            M.showLoadingDialog(context);
            AuthenticationAPI mAuthenticationAPI = APIServiceHeader.createService(context, AuthenticationAPI.class);
            Call<List<CustomerPojo>> call = mAuthenticationAPI.searchCustomer(restaurantid, txt);
            call.enqueue(new retrofit2.Callback<List<CustomerPojo>>() {
                @Override
                public void onResponse(Call<List<CustomerPojo>> call, Response<List<CustomerPojo>> response) {
                    if (response.isSuccessful()) {
                        M.hideLoadingDialog();
                        final List<CustomerPojo> custCustomerPojos = response.body();
                        if (custCustomerPojos != null) {
                            if(custCustomerPojos.size()==0 && phDialog != null && phDialog.isShowing()){
                                final Dialog dnew=new Dialog(context);
                                dnew.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dnew.setContentView(R.layout.dialog_alert_nocust);
                                dnew.setCancelable(false);
                                dnew.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                TextView tvyes=(TextView)dnew.findViewById(R.id.tvyes);
                                TextView tvno=(TextView)dnew.findViewById(R.id.tvno);

                                tvno.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        dnew.dismiss();
                                        phDialog.dismiss();
                                    }
                                });

                                tvyes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        dnew.dismiss();
                                        phDialog.dismiss();
                                        cust_detail(txt);

                                    }
                                });
                                dnew.show();
                            }else {
                                ArrayList<String> list = new ArrayList<>();
                                for (CustomerPojo c : custCustomerPojos) {
                                    String txt = "";
                                    txt = c.getName() + "\n" + c.getPhone_no();
                                    list.add(txt);
                                }
                                if (list != null && list.size() > 0) {
                                    if (phDialog != null && phDialog.isShowing() && list.size() == 1) {
                                        fillAddress(custCustomerPojos.get(0));
                                        tvpoint.setText(custCustomerPojos.get(0).getPoints());
                                        etemail.setText(custCustomerPojos.get(0).getEmail());
                                        etemail.setSelection(etemail.getText().length());
                                        etphone.setText(custCustomerPojos.get(0).getPhone_no());
                                        etphone.setSelection(etphone.getText().length());
                                        etname.setText(custCustomerPojos.get(0).getName());
                                        etname.setSelection(etname.getText().length());
                                    } else {
                                        final Dialog dcust = new Dialog(context);
                                        dcust.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                        dcust.setContentView(R.layout.dialog_customer_list);
                                        dcust.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                        ListView lv = (ListView) dcust.findViewById(R.id.lv);
                                        ArrayAdapter<String> ada = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, list);
                                        lv.setAdapter(ada);
                                        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                            public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
                                                fillAddress(custCustomerPojos.get(i));
                                                etname.setText(custCustomerPojos.get(i).getName());
                                                etname.setSelection(etname.getText().length());
                                                etemail.setText(custCustomerPojos.get(i).getEmail());
                                                etemail.setSelection(etemail.getText().length());
                                                etphone.setText(custCustomerPojos.get(i).getPhone_no());
                                                etphone.setSelection(etphone.getText().length());
                                                if (phDialog != null && phDialog.isShowing()) {
                                                    tvpoint.setText(custCustomerPojos.get(i).getPoints());
                                                }
                                                dcust.dismiss();
                                            }
                                        });
                                        dcust.show();
                                    }
                                }
                            }
                        }
                    } else {
                        M.hideLoadingDialog();
                        int statusCode = response.code();
                        ResponseBody errorBody = response.errorBody();
                        Log.d(TAG, "error:" + statusCode + " " + errorBody);
                    }
                }

                @Override
                public void onFailure(Call<List<CustomerPojo>> call, Throwable t) {
                    Log.d(TAG, "fail:" + t.getMessage());
                    M.hideLoadingDialog();
                }
            });
        }else {
            M.hideLoadingDialog();
            Toast.makeText(context, R.string.no_internet_error, Toast.LENGTH_SHORT).show();
        }
    }

    public class PagerAdapter extends FragmentPagerAdapter {

        private final ArrayList<String> tabNames = new ArrayList<String>() {{
            for (CuisineListPojo item:cuisinelist){
                add(item.getCuisine_name());
            }
        }};

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabNames.get(position);
        }

        @Override
        public int getCount() {
            return tabNames.size();
        }

        @Override
        public Fragment getItem(int position) {
            int currpos=position;
            return DishFragment.newInstance(cuisinelist.get(position).getCuisine_id(),false);
        }
    }

    public void updateUI(){
        llbtntbl.setVisibility(View.VISIBLE);
        tableItemAdapter=new TableItemAdapter(context);
        rvorders.setAdapter(tableItemAdapter);

        if(AppConst.dishorederlist!=null && AppConst.dishorederlist.size()>0){
            btncanceltbl.setVisibility(View.GONE);
            btnfinish.setVisibility(View.VISIBLE);
        }else{
            btncanceltbl.setVisibility(View.VISIBLE);
            btnfinish.setVisibility(View.GONE);
        }
    }

    private void sendToServer(String reqObj){
        if(AppConst.isMyServiceRunning(POServerService.class,context)){
            mService.sendMsg(reqObj);
        }else {
            if(M.pDialog!=null && M.pDialog.isShowing())
                M.hideLoadingDialog();
            joinDialog();
        }
    }

    private void joinDialog(){

        mBound = false;
        if(joinDialog!=null && joinDialog.isShowing()){}
        else {
            jdialog=new JoinServerDialog(context, PlaceOrder.this,null);
            jdialog.show();
//            joinDialog = new Dialog(context);
//            joinDialog.setCancelable(false);
//            joinDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//            joinDialog.setContentView(R.layout.dialog_server_connection);
//
//            Button btnjoin = (Button) joinDialog.findViewById(R.id.btnjoin);
//            btnjoin.setTypeface(AppConst.font_regular(context));
//            Button btnlogout = (Button) joinDialog.findViewById(R.id.btnlogout);
//            btnlogout.setTypeface(AppConst.font_regular(context));
//            btnlogout.setVisibility(View.GONE);
//            final EditText etip=(EditText)dialog.findViewById(R.id.etip);
//            etip.setTypeface(AppConst.font_regular(context));
//
//
//            btnlogout.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                }
//            });
//
//            btnjoin.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    M.showLoadingDialog(context);
//                    Boolean isPOS=AppConst.isMyServiceRunning(POServerService.class,context);
//                    if(isPOS)
//                        stopService(new Intent(context, POServerService.class));
//                    if(etip.getText().toString().isEmpty()){
//                            etip.setError("IP Address required");
//                        }else {
//                        new Handler().postDelayed(new Runnable() {
//                            public void run() {
//                                M.hideLoadingDialog();
//                                Intent it = new Intent(context, POServerService.class);
//                                it.setAction("ip:" + etip.getText().toString());
//                                startService(it);
//                            }
//                        }, 2000);
//                    }
//                }
//            });
//
//            joinDialog.show();
        }
    }

    private void unRegister(){
        try {
            if(broadcastReceiver!=null)
                unregisterReceiver(broadcastReceiver);
        }catch (IllegalArgumentException e){}
    }

    @Override
    public void onDestroy() {
        if(joinDialog!=null && joinDialog.isShowing())
            joinDialog.dismiss();
        if(dialog2!=null && dialog2.isShowing())
            dialog2.dismiss();
        if(dialogcust!=null && dialogcust.isShowing())
            dialogcust.dismiss();
        if(dialog!=null && dialog.isShowing())
            dialog.dismiss();
        if(splitDialog!=null && splitDialog.isShowing())
            splitDialog.dismiss();
        if(ssDialog!=null && ssDialog.isShowing())
            ssDialog.dismiss();
        if(onlineTransDialog!=null && onlineTransDialog.isShowing())
            onlineTransDialog.dismiss();
        if(M.pDialog!=null && M.pDialog.isShowing())
            M.hideLoadingDialog();
        if(thread.isAlive())
            thread.interrupt();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_dinein_order,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home) {
            // ishome=true;
            onBackPressed();
        }else if(item.getItemId()==R.id.item_cancel) {
            btncanceltbl.performClick();
        }else if(item.getItemId()==R.id.item_search){
            Intent it = new Intent(context, SearchActivity.class);
            it.putExtra("table",true);
            startActivity(it);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if(mBound)
            unbindService(mConnection);
        unRegister();
        if(EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
        if(joinDialog!=null && joinDialog.isShowing())
            joinDialog.dismiss();
        if(dialog2!=null && dialog2.isShowing())
            dialog2.dismiss();
        if(dialogcust!=null && dialogcust.isShowing())
            dialogcust.dismiss();
        if(dialog!=null && dialog.isShowing())
            dialog.dismiss();
        if(splitDialog!=null && splitDialog.isShowing())
            splitDialog.dismiss();
        if(ssDialog!=null && ssDialog.isShowing())
            ssDialog.dismiss();
        if(phDialog!=null && phDialog.isShowing())
            phDialog.dismiss();
        Intent it=new Intent(context,WaiterActivity.class);
        finish();
        startActivity(it);
    }

//    public String getLocalIp() {
//        WifiManager wm = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
//        return Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
//    }

    public String getLocalIp() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface networkInterface = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = networkInterface.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                        String host = inetAddress.getHostAddress();
                        if (!TextUtils.isEmpty(host)) {

                            return host;
                        }
                    }
                }

            }
        } catch (Exception ex) {
            Log.e("IP Address", "getLocalIpAddress", ex);
        }
        return null;
    }
}
