package com.royalpos.waiter;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.royalpos.waiter.helper.ConnectionDetector;
import com.royalpos.waiter.model.AccountPojo;
import com.royalpos.waiter.model.M;
import com.royalpos.waiter.webservices.APIServiceHeader;
import com.royalpos.waiter.webservices.AuthenticationAPI;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.ResponseBody;
import retrofit2.Call;

public class MyAccountExpire extends AppCompatActivity{

    TextView tv;
    Context context;
    ConnectionDetector connectionDetector;
    String TAG="MyAccount";
    SimpleDateFormat defaultfmt=new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat dtfmt=new SimpleDateFormat("dd MMM yyyy");



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);
        if(getResources().getBoolean(R.bool.portrait_only)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        }
        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);

        context=MyAccountExpire.this;
        connectionDetector=new ConnectionDetector(context);

        tv=(TextView)findViewById(R.id.tv);


        getData();


    }

    private void getData() {
        if(connectionDetector.isConnectingToInternet()) {
            M.showLoadingDialog(context);
                AuthenticationAPI mAuthenticationAPI = APIServiceHeader.createService(context, AuthenticationAPI.class);
                Call<AccountPojo> call = mAuthenticationAPI.getACStatus(M.getRestID(context));
                call.enqueue(new retrofit2.Callback<AccountPojo>() {
                    @Override
                    public void onResponse(Call<AccountPojo> call, retrofit2.Response<AccountPojo> response) {
                        M.hideLoadingDialog();
                        if (response.isSuccessful()) {
                            AccountPojo pojo=response.body();
                            String txt="";
                            if(pojo.getSuccess().equals("1")){
                                txt=pojo.getStatus();
                                if(pojo.getStatus().equals("expired")){
                                    try {
                                        Date dt=defaultfmt.parse(pojo.getMembership_end_date());
                                        txt=getString(R.string.txt_membership_expire)+dtfmt.format(dt);
                                        tv.setText(txt);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }

                                }else if(pojo.getStatus().equals("purchased")){

                                    try {
                                        Date dt=defaultfmt.parse(pojo.getMembership_end_date());
                                        txt=getString(R.string.Membership_Expires_on)+"\n"+dtfmt.format(dt);
                                        tv.setText(txt);

                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    return;
                                }else if(pojo.getStatus().equals("free")){


                                    try {
                                        Date dt=defaultfmt.parse(pojo.getMembership_end_date());
                                        txt=getString(R.string.free_trial)+" \n"+getString(R.string.expires_on)+" \n"+dtfmt.format(dt);
                                        tv.setText(txt);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        } else {
                            int statusCode = response.code();
                            ResponseBody errorBody = response.errorBody();
                            Log.d(TAG, "error:" + statusCode + " " + errorBody);
                        }
                    }

                    @Override
                    public void onFailure(Call<AccountPojo> call, Throwable t) {
                        M.hideLoadingDialog();
                        Log.d("response:", "fail:" + t.getMessage());
                    }
                });
        }else{
            M.showToast(context,getString(R.string.no_internet_error));
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }




}
