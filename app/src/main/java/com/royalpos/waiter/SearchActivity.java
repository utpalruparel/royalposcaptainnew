package com.royalpos.waiter;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import com.google.android.material.textfield.TextInputEditText;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import com.royalpos.waiter.adapter.TableDishesListAdapter;
import com.royalpos.waiter.database.DBDishes;
import com.royalpos.waiter.helper.AppConst;
import com.royalpos.waiter.helper.MemoryCache;
import com.royalpos.waiter.model.DishPojo;

import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends AppCompatActivity {

    TextInputEditText etsearch;
    ImageView ivclear;
    RecyclerView rv;
    Context context;
    String TAG="SearchActivity";
    TableDishesListAdapter ada;
    ArrayList<DishPojo> dlist=new ArrayList<>();
    Boolean istable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        if(getResources().getBoolean(R.bool.portrait_only)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        }
        MemoryCache memoryCache = new MemoryCache();
        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);

        context=SearchActivity.this;
        istable=getIntent().getBooleanExtra("table",false);
        etsearch=(TextInputEditText)findViewById(R.id.search_input);
        etsearch.setTypeface(AppConst.font_regular(context));
        ivclear=(ImageView)findViewById(R.id.clear_btn_search_view);
        rv=(RecyclerView)findViewById(R.id.rvresults);
        rv.setLayoutManager(new LinearLayoutManager(context));
        rv.setHasFixedSize(true);
        if(dlist==null)
            dlist=new ArrayList<>();
        dlist.clear();

        etsearch.setFocusable(true);
        initializerSearchView(etsearch, ivclear);

        ivclear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etsearch.getText() != null) {
                    etsearch.setText("");
                }
            }
        });
        getDishes();
    }

    private void getDishes() {
        ada=new TableDishesListAdapter(context,new ArrayList<DishPojo>(),true);
        rv.setAdapter(ada);
        rv.scrollToPosition(0);
    }

    public void initializerSearchView(TextInputEditText searchInput, final ImageView clearSearchBtn) {

        final Context context = this;
        searchInput.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                }
            }
        });

        searchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                clearSearchBtn.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                clearSearchBtn.setVisibility(View.VISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    clearSearchBtn.setVisibility(View.GONE);
                }else{
                    filter(s.toString());
                }
            }
        });
    }

    void filter(String text){
        DBDishes dbDishes = new DBDishes(context);
        List<DishPojo> temp = dbDishes.filterItems(text,context);
//        List<DishPojo> temp = new ArrayList();
//        for(DishPojo d: dlist){
//            String dnm=d.getDishname();
//            String batchno=d.getBatch_no();
//            String barcode=d.getBarcode_no();
//            String pro=d.getProdct_no();
//            if(dnm.toLowerCase().contains(text.toLowerCase()) || batchno.toLowerCase().contains(text.toLowerCase())
//                   || barcode.toLowerCase().contains(text.toLowerCase()) || pro.toLowerCase().contains(text.toLowerCase())){
//                temp.add(d);
//            }
//        }
        if(temp!=null) {
            ada.updateList(temp);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

}
