package com.royalpos.waiter.print;

import android.content.Context;
import android.util.Log;

import com.royalpos.waiter.model.M;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by reeva on 21/7/18.
 */
public class PrintFormatNew {

    Context context;
    public static int selSize=42;
    String TAG="PrintFormatNew";

    public PrintFormatNew(Context context) {
        this.context=context;
    }

    public void setPaperSize(String size){
        if(size==null || size.isEmpty())
            selSize=M.getPapersize(context);
        else if(Integer.parseInt(size)==PrintFormat.smallsize)
            selSize=PrintFormat.smallsize;
        else if(Integer.parseInt(size)==PrintFormat.size79)
            selSize=PrintFormat.size79;
        else if(Integer.parseInt(size)==PrintFormat.size80)
            selSize=PrintFormat.size80;
        else
            selSize=PrintFormat.normalsize;
    }

    public String padLine1(String txt1, String txt2, String txt3, String txt4){
        //txt1-item name,txt2-quantity,txt3-amount,txt4-total
        txt1=txt1.replaceAll("\\s+$", "");
        txt2=" "+txt2;
        String concat="",extra="";
        Boolean isSmall=false,chk3=true;
        int columnsPerLine = selSize;
        //size-40 (78)
        int  fcol = 20,scol=4,tcol=8,frcol=8;
        if(columnsPerLine==PrintFormat.smallsize){
            //size-30 (58)
            fcol=16;
            scol=5;
            frcol=9;
            tcol=0;
            isSmall=true;
        }else if(columnsPerLine==PrintFormat.size79){
            //size-42 (79)
            fcol = 23;
            scol=4;
            tcol=9;
            frcol=9;
        }else if(columnsPerLine==PrintFormat.size80){
            //size-44 (80)
            fcol = 25;
            scol=5;
            tcol=9;
            frcol=9;
        }
        int maxLines=1,fcharLine=txt1.length()/fcol,scharLine=txt2.length()/scol,frcl=txt4.length()/frcol,tcharLine=0;
        if(!isSmall)
            tcharLine=txt3.length()/tcol;
        else if(tcol>txt3.length())
            chk3=false;
        maxLines=fcharLine;
        if(scharLine>maxLines) {
            maxLines = scharLine;
        }
        if(!isSmall && tcharLine>maxLines) {
            maxLines = tcharLine;
        }
        if(frcl>maxLines) {
            maxLines = frcl;
        }

        if(fcol>=txt1.length() && scol>=txt2.length() && frcol>=txt4.length() && chk3){
            concat = setLength(txt1, fcol, "left");
            concat = concat + setLength(txt2, scol, "left");
            if(!isSmall)
                concat = concat + setLength(txt3, tcol, "right");
            concat = concat + setLength(txt4, frcol, "right");
        }else{
            ArrayList<String> fResult=new ArrayList<>();
            if(fcol>=txt1.length()){
                fResult.add(txt1);
            }else{
                fResult=PrintFormat.splitTxt(txt1);
            }
            if(maxLines<fResult.size()-1)
                maxLines=fResult.size()-1;
            for (int i = 0; i <= maxLines; i++) {
                if(fResult.size()>i){
                    concat = concat+setLength(fResult.get(i), fcol, "left") ;
                }else{
                    concat=concat+repeat(" ",fcol);
                }
                int s2 = i * scol;
                int e2 = s2 + scol;
                if (e2 > txt2.length())
                    e2 = txt2.length();
                if(s2>txt2.length())
                    concat=concat+repeat(" ",scol);
                else
                    concat = concat+setLength(txt2.substring(s2, e2), scol, "left") ;

                if(!isSmall) {
                    int s3 = i * tcol;
                    int e3 = s3 + tcol;
                    if (e3 > txt3.length())
                        e3 = txt3.length();
                    if (s3 > txt3.length())
                        concat = concat + repeat(" ", tcol);
                    else
                        concat = concat + setLength(txt3.substring(s3, e3), tcol, "right");
                }

                int s4 = i * frcol;
                int e4 = s4 + frcol;
                if (e4 > txt4.length())
                    e4 = txt4.length();
                if(s4>txt4.length())
                    concat=concat+repeat(" ",frcol);
                else
                    concat = concat+setLength(txt4.substring(s4, e4), frcol, "right") ;

                concat=concat+"\n";
            }
            if(concat.endsWith("\n"))
                concat=concat.substring(0,concat.length()-1);
        }

//        if(concat.endsWith("\n"))
//            concat=concat.substring(0,concat.length()-1);
        Log.d(TAG,concat);
        return concat;
    }

    public String padLine2(String txt1,String txt2){
        String concat="",extra="";
        int s=0;
        if(txt1!=null)
            s=txt1.length();
        if(txt2!=null)
            s=s+txt2.length();
        if(selSize!=PrintFormat.smallsize) {
            int columnsPerLine =selSize;;
            if (s <= columnsPerLine) {
                if (txt1.length() == 0 || txt2.length() == 0)
                    concat = txt1 + txt2;
                else
                    concat = txt1 + setLength(txt2, columnsPerLine - txt1.length(), "right");
            } else {
                if (txt1.length() > columnsPerLine) {
                    int b = txt1.length() / columnsPerLine;
                    for (int i = 0; i <= b; i++) {
                        int start = i * columnsPerLine;
                        int end = start + columnsPerLine;
                        if (end > txt1.length())
                            end = txt1.length();
                        concat = concat + setLength(txt1.substring(start, end), columnsPerLine, "center") + "\n";
                    }
                }else{
                    concat=txt1+"\n"+txt2;
                }
            }
        }else{
            int columnsPerLine = PrintFormat.smallsize;
            if (s < columnsPerLine) {
                if (txt1.length() == 0 || txt2.length() == 0)
                    concat = txt1 + txt2;
                else
                    concat = txt1 + setLength(txt2, columnsPerLine - txt1.length(), "right");
            } else {
                if (txt1.length() > columnsPerLine && txt2.length() == 0) {
                    int b = txt1.length() / columnsPerLine;
                    for (int i = 0; i <= b; i++) {
                        int start = i * columnsPerLine;
                        int end = start + columnsPerLine;
                        if (end > txt1.length())
                            end = txt1.length();
                        concat = concat + setLength(txt1.substring(start, end), columnsPerLine, "center") + "\n";
                    }
                }else{
                    if(s==columnsPerLine)
                        txt2=setLength(txt2,txt2.length()+1,"right");
                    int fcol=s-txt2.length();
                    if(fcol>txt1.length())
                        concat = txt1 + setLength(txt2, columnsPerLine - txt1.length(), "right");
                    else{
                        int b = txt1.length() / fcol;
                        for (int i = 0; i <= b; i++) {
                            int start = i * fcol;
                            int end = start + fcol;
                            if (end > txt1.length())
                                end = txt1.length();
                            if(i==0)
                                concat = concat + setLength(txt1.substring(start, end), fcol, "center")+txt2+"\n";
                            else
                                extra = extra + setLength(txt1.substring(start, end), fcol, "center") +repeat(" ",txt2.length())+ "\n";
                        }
                        concat=concat+extra;
                    }
                }
            }
        }
//        if(concat.endsWith("\n"))
//            concat=concat.substring(0,concat.length()-1);
        Log.d(TAG,concat);
        return concat;
    }

    public String padLine3(String txt1,String txt2){
        if(selSize!=PrintFormat.smallsize)
            return txt1+setLength(txt2,15,"right");
        else {
            if(txt1.length()+txt2.length()>15)
                return padLine2(txt1,txt2);
            else
                return txt1 + setLength(txt2, 8, "right");
        }
    }

    public String alignLeft(String txt){
        String concat="";
        int columnsPerLine =selSize;
        if(txt.contains("\n")) {
            txt=txt.trim();
            String[] a = txt.split("\n");
            for (int i = 0; i < a.length; i++) {
                String t=a[i];
                if(t.length()<=columnsPerLine)
                    concat = concat + setLength(t, columnsPerLine, "left")+"\n";
                else{
                    int b = t.length() / columnsPerLine;
                    String extra="";
                    for (int i1 = 0; i1 <= b; i1++) {
                        int start = i1 * columnsPerLine;
                        int end = start + columnsPerLine;
                        if (end > t.length())
                            end = t.length();
                        if(i1==0)
                            concat = concat + setLength(t.substring(start, end), columnsPerLine, "left")+"\n";
                        else
                            extra = extra + setLength(t.substring(start, end), columnsPerLine, "left") + "\n";
                    }
                    concat=concat+extra;
                }

            }
        }else {
            String t=txt;
            int b = t.length() / columnsPerLine;
            String extra="";
            for (int i1 = 0; i1 <= b; i1++) {
                int start = i1 * columnsPerLine;
                int end = start + columnsPerLine;
                if (end > t.length())
                    end = t.length();
                if(i1==0)
                    concat = concat + setLength(t.substring(start, end), columnsPerLine, "left")+"\n";
                else
                    extra = extra + setLength(t.substring(start, end), columnsPerLine, "left") + "\n";
            }
            concat=concat+extra;
        }
//        if(concat.endsWith("\n"))
//            concat=concat.substring(0,concat.length()-1);
        return concat;
    }

    public String setLength(String txt,int size,String side){
        int padding=size-txt.length();
        if(padding>0) {
            if (side.equals("center")) {
                int p = padding / 2;
                int ep = padding - p;
                return repeat(" ", p) + txt + repeat(" ", ep);
            } else if (side.equals("right")) {
                return repeat(" ", padding) + txt;
            } else {
                return txt + repeat(" ", padding);
            }
        }else
            return txt.substring(0,size);

    }

    public String divider(){
        if(selSize==PrintFormat.smallsize)
            return "________________________________";
        else
            return "__________________________________________";
    }

    protected String repeat(String str, int i){
        return new String(new char[i]).replace("\0", str);
    }

    public String setFormat(String value){
        if(value!=null && value.trim().length()>0) {
            try {
                return String.format(Locale.ENGLISH, "%."+ M.getDecimalVal(context)+"f", Double.parseDouble(value));
            }catch (NumberFormatException e){
                return value;
            }

        }else
            return "";
    }
}