package com.royalpos.waiter.print.newbluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import androidx.annotation.Nullable;

import com.royalpos.waiter.R;
import com.royalpos.waiter.model.M;
import com.royalpos.waiter.print.PrintFormat;
import com.royalpos.waiter.print.PrintFormatNew;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static java.lang.Math.abs;


public class KitchenGlobalsNew {
    // TODO IMPORTANT CONFIG CHANGES
    // ** IMPORTANT CONFIGURATION
    private static String serverRealUrl = "http://www.myserver.com/api";    //Real Server configuration
    private static String serverLocalUrl = "http://localhost:80/api";             //local configuration
    /**
     * Substitute you own sender ID here. This is the project number you got
     * from the API Console, as described in "Getting Started."
     */

    public static String SENDER_ID = "000000";
    public static String base64EncodedPublicKey = "xxxxx";
    //DEBUG MODO
    public static boolean debugModeBoolean = true;  //USE in DEBUG MODE
    //public static boolean debugModeBoolean = false;  //USE in REAL  MODE


    //PRINT FROM WEB ENABLES
    //public static boolean printFronWebEnabled = true;  //USE to activate print fron web
    public static boolean printFronWebEnabled = false;  //USE  to deactivate print fron web

    //GCM Register
    //public static boolean gcmRegisterEnabled = true;  //USE to register device in GCM
    public static boolean gcmRegisterEnabled = false;  // not use gcm services


    //IN APP BILLING
    //public static boolean inAppBillingModeON = true;  //USE in pay app modo on
    public static boolean inAppBillingModeON = false;  //USE in pay app modo off


    private static String internalClassName = "Globals";    // * Tag used on log messages.
    public static int deviceType = 0;                       // * Device type
    //0 Pending config (Default as start)
    //1 NET Print
    //2 RS232   ->Pending
    //3 USB
    //4 BT

    //Google subscriptions
    public static boolean mSubscribedToYearlyUsed = false;


    // Bluetooth  variable Config
    public static BluetoothAdapter mBluetoothAdapter;
    public static BluetoothSocket mmSocket;
    public static BluetoothDevice mmDevice;


    public static String logoProcesed = "";

    public static OutputStream mmOutputStream;
    public static InputStream mmInputStream;


    // NET Printer Config
    public static String deviceIP = "";
    public static int devicePort = 0;
    public static int usbDeviceID = 0;//
    public static int usbVendorID = 0;//
    public static int usbProductID = 0;//
    public static String blueToothDeviceAdress = "";
    public static int blueToothSpinnerSelected = -1;

    public static String tmpDeviceIP = "";
    public static int tmpDevicePort = 0;
    public static int tmpUsbDeviceID = 0;
    public static int tmpUsbVendorID = 0;
    public static int tmpUsbProductID = 0;
    public static String link_code = "";
    public static String tmpBlueToothDeviceAdress = "";
    public static JSONObject jsonObject =null;
    String TAG="KitchenGlobals";
    public static PrintFormatNew pf;

    public static int size = 27;
    public static int medsize = 30;
    public static int bigsize = 35;
    public static boolean isbold = false;
    public static boolean isnormal = false;

    public static String getServerUrl() {
        String actualServer = "";
        if (debugModeBoolean) {
            actualServer = serverLocalUrl;
        } else {
            actualServer = serverRealUrl;
        }

        return actualServer;
    }

    public static String picturePath = "";

    public static JSONObject getJsonObject() {
        return jsonObject;
    }

    public static void setJsonObject(JSONObject jsonObject) {
        KitchenGlobalsNew.jsonObject = jsonObject;
    }

    public static void savePreferences(SharedPreferences sharedPrefs) {
        Log.i(internalClassName, "savePreferences");

        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putString("kusbDeviceID", String.valueOf(usbDeviceID));
        editor.putString("kusbVendorID", String.valueOf(usbVendorID));//
        editor.putString("kusbProductID", String.valueOf(usbProductID));//


        editor.putString("kblueToothDeviceAdress", String.valueOf(blueToothDeviceAdress));


        editor.putString("kdeviceType", String.valueOf(deviceType));
        //Ethernet
        editor.putString("kdeviceIP", String.valueOf(deviceIP));
        editor.putString("kdevicePort", String.valueOf(devicePort));

        editor.putString("klink_code", String.valueOf(link_code));
        editor.putString("kpicturePath", String.valueOf(picturePath));
        editor.putString("klogoProcesed", logoProcesed);

        Log.i(internalClassName, "               deviceType=" + String.valueOf(deviceType));
        Log.i(internalClassName, "               deviceIP=" + String.valueOf(deviceIP));
        Log.i(internalClassName, "               devicePort=" + String.valueOf(devicePort));
        Log.i(internalClassName, "               usbDeviceID=" + String.valueOf(usbDeviceID));
        Log.i(internalClassName, "               blueToothDeviceAdress=" + String.valueOf(blueToothDeviceAdress));


        editor.commit();

    }

    public static void loadPreferences(SharedPreferences sharedPrefs) {
        Log.i(internalClassName, "loadPreferences");

        usbDeviceID = mathIntegerFromString(sharedPrefs.getString("kusbDeviceID", "0"), 0);
        usbVendorID = mathIntegerFromString(sharedPrefs.getString("kusbVendorID", "0"), 0);
        usbProductID = mathIntegerFromString(sharedPrefs.getString("kusbProductID", "0"), 0);


        link_code = sharedPrefs.getString("klink_code", "");
        picturePath = sharedPrefs.getString("kpicturePath", "");
        blueToothDeviceAdress = sharedPrefs.getString("kblueToothDeviceAdress", "x:x:x:x");

        deviceType = mathIntegerFromString(sharedPrefs.getString("kdeviceType", "0"), 0);
        //Ethernet
        deviceIP = sharedPrefs.getString("kdeviceIP", "0.0.0.0");
        devicePort = mathIntegerFromString(sharedPrefs.getString("kdevicePort", "9100"), 9100);
        logoProcesed = sharedPrefs.getString("klogoProcesed", "");


        Log.i(internalClassName, "               deviceType=" + String.valueOf(deviceType));
        Log.i(internalClassName, "               deviceIP=" + String.valueOf(deviceIP));
        Log.i(internalClassName, "               devicePort=" + String.valueOf(devicePort));
        Log.i(internalClassName, "               usbDeviceID=" + String.valueOf(usbDeviceID));
        Log.i(internalClassName, "               blueToothDeviceAdress=" + String.valueOf(blueToothDeviceAdress));


    }

    public static Integer mathIntegerFromString(String inti, Integer defi) {
        //used for preferences
        Integer returi = defi;
        if (!inti.equals("")) {
            try {
                returi = Integer.valueOf(inti);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }

        return returi;

    }

    protected static String padLine(@Nullable String partOne, @Nullable String partTwo, int columnsPerLine, String str){
        if(partOne == null) {partOne = "";}
        if(partTwo == null) {partTwo = "";}
        if(str == null) {str = " ";}
        String concat;
        if((partOne.length() + partTwo.length()) > columnsPerLine) {
            concat = partOne + str + partTwo;
        } else {
            int padding = columnsPerLine - (partOne.length() + partTwo.length());
            concat = partOne + repeat(str, padding) + partTwo;
        }
        return concat;
    }

    protected static String repeat(String str, int i){
        return new String(new char[i]).replace("\0", str);
    }

    public static String getImageDataPosPrinterLogo(Resources resources) {
        String INITIALIZE_PRINTER = "·27··64·";//Chr(&H1B) & Chr(&H40);
        String PRINT_AND_FEED_PAPER = "·10·";//Chr(&HA);
        String CMDSELECT_BIT_IMAGE_MODE = "·27··42·";//Chr(&H1B) & Chr(&H2A);
        String SET_LINE_SPACING = "·27··51·";//Chr(&H1B) & Chr(&H33);
        String SETLINESPACING24 = SET_LINE_SPACING + "·24·";

        BitmapFactory.Options options_load = new BitmapFactory.Options();
        options_load.inScaled = false;
        Bitmap miImagetest = null;
        Bitmap miImage = null;

        miImage = BitmapFactory.decodeResource(resources, R.drawable.rpbanner, options_load);


        int image_h = miImage.getHeight();
        int p_Width = miImage.getWidth();


        String widthLSB = "";
        String widthMSB = "";
        String hexdata = "0000" + Integer.toHexString(p_Width);
        widthLSB = hexdata.substring(hexdata.length() - 2, hexdata.length());
        widthMSB = hexdata.substring(hexdata.length() - 4, hexdata.length() - 2);


        String SELECTBITIMAGEMODE = CMDSELECT_BIT_IMAGE_MODE + "·33··" + Integer.parseInt(widthLSB, 16) + "··" + Integer.parseInt(widthMSB, 16) + "·";// & Chr(33) & Chr(widthLSB) & Chr(widthMSB)


        String p_TxtPrint = INITIALIZE_PRINTER;
        p_TxtPrint = p_TxtPrint + SETLINESPACING24;
        String p_DatatoPrint = "";
        int offset = 0;
        while (offset < image_h) {
            int imageDataLineIndex = 0;
            int p_BytesAncho = 3 * p_Width - 1;
            int[] imageDataLine = new int[p_BytesAncho + 10];//(3 * (image_w + 10))];
            for (int X = 0; X < p_Width; X++) {
                //' Remember, 24 dots = 24 bits = 3 bytes.
                //' The ' k ' variable keeps track of which of those
                //' three bytes that we' re currently scribbling into.
                for (int K = 0; K <= 2; K++) {
                    int slice = 0;
                    String strSlice = "";
                    //' A byte is 8 bits. The ' b ' variable keeps track
                    //' of which bit in the byte we' re recording.
                    for (int b = 0; b <= 7; b++) {
                        //' Calculate the y position that we' re currently
                        //' trying to draw. We take our offset, divide it
                        //' by 8 so we' re talking about the y offset in
                        //' terms of bytes, add our current ' k ' byte
                        //' offset to that, multiple by 8 to get it in terms
                        //' of bits again, and add our bit offset to it.
                        int zz2 = abs(offset / 8);
                        int y = ((zz2 + K) * 8) + b;
                        int I = (y * (p_Width)) + (X);
                        //int y = (((abs(offset / 8)) + K) * 8) + b;
                        //' Calculate the location of the pixel we want in the bit array  It' ll be at(y * width) + x.
                        // int I = (y * p_Width) + X;
                        // ' If the image (or this stripe of the image)                         ' is shorter than 24 dots, pad with zero.
                        boolean v = false;

                        if (I <= (image_h * p_Width)) {
                            //Log.e(internalClassName, "IMAGEN= getImageData x,y:" + String.valueOf(X) + "," + String.valueOf(y));
                            if (y < image_h) {
                                int midato = miImage.getPixel(X, y);
                                int alfa = (midato >> 24) & 0xff;     //bitwise shifting

                                int R = (midato >> 16) & 0xff;     //bitwise shifting
                                int G = (midato >> 8) & 0xff;
                                int B = midato & 0xff;
                                double total = abs(0.2126 * R + 0.7152 * G + 0.0722 * B);

                                // Log.e(internalClassName, "valores:  total===" + String.valueOf(total)  + "->%="  );
                                if (total > 80) {
                                    v = true;
                                }
                            } else {
                                v = true;
                            }
                        } else {
                            v = true;
                        }
                        if (v == true) {
                            strSlice = strSlice + "0";
                        } else {
                            strSlice = strSlice + "1";
                        }
                    }

                    imageDataLine[imageDataLineIndex + K] = Integer.parseInt(strSlice, 2);
                }
                imageDataLineIndex = imageDataLineIndex + 3;
            }
            String p_dataBytes = "";
            for (int XX = 0; XX < imageDataLineIndex; XX++) {
                p_dataBytes = p_dataBytes + String.valueOf(((char) imageDataLine[XX]));
            }
            p_DatatoPrint = p_DatatoPrint + SELECTBITIMAGEMODE + p_dataBytes + PRINT_AND_FEED_PAPER;

            offset = offset + 24;
        }

        p_TxtPrint = p_TxtPrint + p_DatatoPrint + SETLINESPACING24 + INITIALIZE_PRINTER;
        p_TxtPrint = p_TxtPrint + "·29··60·";//& Chr( & H1D)&Chr( & H3C)

        return p_TxtPrint;
    }

    public static boolean printadvance(Context context) {
        if(M.getPapersize(context)== PrintFormat.normalsize){
            isnormal = true;
        }
        if(KitchenGlobalsNew.getJsonObject()!=null){
            return kitchenreciept(context);
        }else {
            return createTestReceiptData(context);
        }
    }

    public static boolean createTestReceiptData(Context context) {

        pf=new PrintFormatNew(context);
        try {
            initprint();

            printText(pf.padLine2(M.getBrandName(context),""),25,true);
            printline();
            printText(pf.padLine2("Order By : "+ M.getWaitername(context),"")+"\t", size, isbold);
//            textData.append("\n");
            printText(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"", size, isbold);
            printText(pf.padLine2("ORDER # 1025555555555",""), size, isbold);
            printline();
            printText(pf.padLine2("TOKEN # "+203,"") +"", size, isbold);

            printline();

            printText(pf.padLine2("طلب تعليق",""), size, isbold);
            printText(pf.padLine2("need spicy",""), size, isbold);

            printline();

            String qty = "1";
            printText(padLine(qty + "   અમેરિકન મકાઈ", "",40, " ") + "", size, isbold);
            printText(padLine(qty + "    બર્ગર",  "", 40, " ") + "", size, isbold);
            printText(padLine(qty + "    પિઝા",  "", 40, " ") + "", size, isbold);
            printText(padLine(qty + "    بيتزا    ",  "", 40, " ") + "", size, isbold);
            printText(padLine(qty + "    برغر    ",  "", 40, " ") + "", size, isbold);

            printText(padLine(qty + "    الذرة الأمريكية    ",  "", 40, " ") + "", size, isbold);

            printline();

            printNewLine();

            cutpaper();
            flush();
        }
        catch (Exception e) {
            Log.d("Exception printer----", e.getMessage()+"");
            return false;
        }



        return true;
    }

    public static void flush(){
//        try {
//            mmOutputStream.flush();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

    public static boolean kitchenreciept(Context context) {
        pf=new PrintFormatNew(context);

        try {
            initprint();

            if(M.isCustomAllow(M.key_top_space,context)) {
                printNewLine();
                printNewLine();
                printNewLine();
                printNewLine();
            }


            String tok="",tm="";
            JSONObject jsonObject= KitchenGlobalsNew.getJsonObject();
            String catid=null;
            if(jsonObject.has("kitchen_cat_id"))
                catid=jsonObject.getString("kitchen_cat_id");

                tok=jsonObject.getString("token");

            if(jsonObject.has("status")){
                printText(pf.padLine2(jsonObject.getString("status"), "") , size,
                        true);
            }
            if(jsonObject.has("order_time"))
                tm=jsonObject.getString("order_time");
            else if(jsonObject.has("time"))
                tm=jsonObject.getString("time");
            printText(pf.padLine2(tok, "") , size, true);

            printText(pf.padLine2(M.getRestName(context),""), size, isbold);
            printline();
            if(jsonObject.has("order_no")) {
                printText(pf.padLine2(jsonObject.getString("order_no"), "") , size, isbold);
                printline();
            }
            String tag_wnm=context.getString(R.string.txt_order_by);
            printText(pf.padLine2(tag_wnm+" : "+M.getWaitername(context),tm), size, isbold);
            if(jsonObject.has("table") && !jsonObject.getString("table").isEmpty()){
                printText(pf.padLine2("   ",jsonObject.getString("table")), size, isbold);
            }else if(jsonObject.has("sitting") && !jsonObject.getString("sitting").isEmpty()){
                printText(pf.padLine2("   ",jsonObject.getString("sitting")), size, isbold);
            }else if(jsonObject.has("ordertype") && !jsonObject.getString("ordertype").isEmpty()) {
                printText(pf.padLine2("   ", jsonObject.getString("ordertype")), size, isbold);
            }
            printline();
            String customername=null,customerph=null;
            if(jsonObject.has("custnm"))
                customername=jsonObject.getString("custnm");
            if(jsonObject.has("custph"))
                customerph=jsonObject.getString("custph");
            String cust="";
            if(customername!=null && customername.length()>0)
                cust=customername;
            if(customerph!=null && customerph.length()>0)
                cust=cust+"\t"+customerph;
            if(cust!=null && cust.trim().length()>0){
                printText(pf.padLine2(context.getString(R.string.customer)+'\t'+cust,""), size, isbold);
                printline();
            }
            if(jsonObject.has("ordercmt") && !jsonObject.getString("ordercmt").isEmpty()){
                printText(pf.padLine2(jsonObject.getString("ordercmt"),""), size, isbold);
                printline();
            }
            JSONArray ja=jsonObject.getJSONArray("item");
            for (int i = 0; i < ja.length(); i++) {
                JSONObject jd=ja.getJSONObject(i);
                Boolean isprint=false;
                if(catid!=null && catid.trim().length()>0 && jd.has("cusineid")){
                    List<String> items = Arrays.asList(catid.split("\\s*,\\s*"));
                    if(items.contains(jd.getString("cusineid")))
                        isprint=true;
                    else
                        isprint=false;
                }else{
                    isprint=true;
                }
                if(isprint && !jd.toString().equals("{}")) {
                    String qty = jd.getString("qty");
                    String name = jd.getString("name");
                    String we="";
                    if(jd.has("weight"))
                        we=jd.getString("weight");
                    String txt=qty+" X "+name+" "+we;
                    if(jd.has("status"))
                        txt=txt+" "+jd.getString("status");
//                    printText(pf.padLine3(txt, "") , size, isbold);
                    printText(pf.alignLeft(txt), size, true);
                    if (jd.has("prenm")) {
                        String prefname = jd.getString("prenm");
                        if(prefname!=null && prefname.length()>0)
                            printText(pf.padLine2(  "    " + prefname, " "), size, isbold);

                    }
                    if (jd.has("dish_comment")) {
                        if (jd.getString("dish_comment").length() > 0) {
                            String dish_comment = jd.getString("dish_comment");
                            printText(pf.padLine2("    " +dish_comment, " ") , size, isbold);
                        }
                    }

                    if (jd.has("combo")) {
                        if (jd.getString("combo").length() > 0) {
                            String combo = jd.getString("combo");
                            printText(pf.padLine2(combo,  ""), size, isbold);
                        }
                    }
                }
            }
            printline();
            printNewLine();
            cutpaper();

            mmOutputStream.flush();
        }catch (JSONException e){
            Log.d("KitchenGlobals","error:"+e.getMessage());
        }
        catch (Exception e) {
            Log.d("KitchenGlobals","Exception printer----"+e.getMessage());

            return false;
        }
        return true;
    }

    public static void initprint(){
        try {
            mmOutputStream.write(com.royalpos.waiter.universalprinter.PrinterCommands.INIT);
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    //print photo
    public static void printPhoto(Bitmap logo) {
        try {

            if(logo!=null){
                byte[] command = Utils.decodeBitmap(logo);
                mmOutputStream.write(PrinterCommands.ESC_ALIGN_LEFT);
                printText(command);
            }else{
                Log.e("Print Photo error", "the file isn't exists");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("PrintTools", "the file isn't exists");
        }
    }

    //print new line
    public static void printNewLine() {
        try {
            mmOutputStream.write(PrinterCommands.FEED_LINE);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void cutpaper() {
        try {
            mmOutputStream.write(PrinterCommands.FEED_PAPER_AND_CUT);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //print text
    public static void printText(String msg, int size, boolean isbold) {
//        try {
        String[] txt=msg.split("\n");
        ReceiptBitmap receiptBitmap = new ReceiptBitmap().getInstance();
        int l=(txt.length+1)*25;

        if(isnormal) {
            receiptBitmap.init(525,l);
        }else{
            receiptBitmap.init(400,l);
        }
        Log.d("here----", "size---"+l );
        for(String t:txt)
            receiptBitmap.drawadvCenterText(t, size, isbold);

        Bitmap canvasbitmap = receiptBitmap.getReceiptBitmap();


        Bitmap croppedBmp = Bitmap.createBitmap(canvasbitmap, 0, 0, canvasbitmap.getWidth(), canvasbitmap.getHeight());

        printPhoto(croppedBmp);
    }

    public static void printline() {
        printNewLine();
        ReceiptBitmap receiptBitmap = new ReceiptBitmap().getInstance();
        int    l = 5;    if(isnormal) {
            receiptBitmap.init(500,l);
            receiptBitmap.drawNewLine(450);
        }else{
            receiptBitmap.init(384,l);
            receiptBitmap.drawNewLine(350);
        }    Bitmap canvasbitmap = receiptBitmap.getReceiptBitmap();
        printPhoto(canvasbitmap);
    }
    //print byte[]
    public static void printText(byte[] msg) {
        try {

            mmOutputStream.write(msg);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
