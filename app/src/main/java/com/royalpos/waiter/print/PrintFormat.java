package com.royalpos.waiter.print;

import android.content.Context;
import android.text.BidiFormatter;
import android.util.Log;

import com.royalpos.waiter.model.M;

import java.text.Bidi;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by reeva on 21/7/18.
 */
public class PrintFormat {

    Context context;
    public static int sunmi_font=24;
    public static int normalsize=40,smallsize=30,setSize=40,size79=45,size80=48;
    String TAG="PrintFormat";

    public PrintFormat(Context context) {
        this.context=context;
        if(M.retriveVal(M.key_bill_width, context)!=null)
            setSize= Integer.valueOf(M.retriveVal(M.key_bill_width, context));
    }

    public static boolean isProbablyArabic(String s) {
        for (int i = 0; i < s.length();) {
            int c = s.codePointAt(i);
            if (c >= 0x0600 && c <= 0x06E0)
                return true;
            i += Character.charCount(c);
        }
        return false;
    }

    public void setPaperSize(String size){
        if(size==null || size.isEmpty())
            setSize= Integer.valueOf(M.retriveVal(M.key_bill_width, context));
        else if(Integer.parseInt(size)==smallsize)
            setSize=smallsize;
        else if(Integer.parseInt(size)==size79)
            setSize=size79;
        else if(Integer.parseInt(size)==size80)
            setSize=size80;
        else
            setSize=normalsize;
        PrintFormatNew.selSize=setSize;
    }

    public static ArrayList<String> splitTxt(String txt){
        int fcol=20;
        int columnsPerLine = setSize;
        Log.d("Paper size---", "Paper size---"+setSize);
        if(columnsPerLine==smallsize)
            fcol=16;
        else if(columnsPerLine==size79)
            fcol=23;
        else if(columnsPerLine==size80)
            fcol=25;
        String[] s=txt.split(" ");
        ArrayList<String> result=new ArrayList<>();
        int maxLines=s.length,index=0;
        while (index<maxLines){
            String rtxt=s[index]+" ";
            if(rtxt.length()<=fcol){
                if(result.isEmpty()) {
                    result.add(rtxt);
                }else{
                    String m_txt=result.get(result.size()-1)+rtxt;
                    if(m_txt.length()<=fcol) {
                        result.set(result.size() - 1, m_txt);
                    }else {
                        result.add(rtxt);
                    }
                }
            }else{
                boolean isUpdate=false;
                if(!result.isEmpty()){
                    int lastEle=result.get(result.size()-1).length();
                    if(lastEle<fcol){
                        rtxt=result.get(result.size()-1)+" "+rtxt;
                        isUpdate=true;
                    }
                }
                int maxNo=rtxt.length()/fcol;
                if(rtxt.length()%fcol!=0) maxNo=maxNo+1;
                for(int i=0;i<maxNo;i++){
                    int s1 = i * fcol;
                    int e1 = s1 + fcol;
                    if (e1 > rtxt.length())
                        e1 = rtxt.length();
                    if(isUpdate && i==0)
                        result.set(result.size()-1,rtxt.substring(s1,e1));
                    else
                        result.add(rtxt.substring(s1,e1));
                    //Log.d(TAG,"len:"+rtxt.substring(s1,e1).length());
                }
            }
            index++;
        }
        //Log.d(TAG,TextUtils.join("\n",result));
        return result;
    }


    public String padLine1(String txt1, String txt2, String txt3, String txt4){

        txt1 = getBidiString(txt1, Bidi.DIRECTION_LEFT_TO_RIGHT);

        //txt1-item name,txt2-quantity,txt3-amount,txt4-total
        txt1=txt1.replaceAll("\\s+$", "");
        txt2=" "+txt2;
        String concat="",extra="";
        Boolean isSmall=false,chk3=true;
        int columnsPerLine = setSize;
        //size-40 (78)
        Log.d("Paper columnsPerLine---", "Paper columnsPerLine---"+columnsPerLine);
        int  fcol = 20,scol=4,tcol=8,frcol=8;
        if(columnsPerLine==smallsize){
            //size-30 (58)
            fcol=16;
            scol=5;
            frcol=9;
            tcol=0;
            isSmall=true;
        }else if(columnsPerLine==size79){
            //size-45 (79)
            Log.d("Paper size79---", "Paper size---"+setSize);
            fcol = 23;
            scol=4;
            tcol=9;
            frcol=9;
        }else if(columnsPerLine==size80){
            //size-48 (80)
            Log.d("Paper size80---", "Paper size---"+setSize);
            fcol = 25;
            scol=5;
            tcol=9;
            frcol=9;
        }
        int maxLines=1,fcharLine=txt1.length()/fcol,scharLine=txt2.length()/scol,frcl=txt4.length()/frcol,tcharLine=0;
        if(!isSmall)
            tcharLine=txt3.length()/tcol;
        else if(tcol>txt3.length())
            chk3=false;
        maxLines=fcharLine;
        if(scharLine>maxLines) {
            maxLines = scharLine;
        }
        if(!isSmall && tcharLine>maxLines) {
            maxLines = tcharLine;
        }
        if(frcl>maxLines) {
            maxLines = frcl;
        }

        if(fcol>=txt1.length() && scol>=txt2.length() && frcol>=txt4.length() && chk3){
            concat = setLength(txt1, fcol, "left");
            concat = concat + setLength(txt2, scol, "left");
            if(!isSmall)
                concat = concat + setLength(txt3, tcol, "right");
            concat = concat + setLength(txt4, frcol, "right");
        }else{
            ArrayList<String> fResult=new ArrayList<>();
            if(fcol>=txt1.length()){
                fResult.add(txt1);
            }else{
                fResult=splitTxt(txt1);
            }
            if(maxLines<fResult.size()-1)
                maxLines=fResult.size()-1;
            for (int i = 0; i <= maxLines; i++) {
                if(fResult.size()>i){
                    concat = concat+setLength(fResult.get(i), fcol, "left") ;
                }else{
                    concat=concat+repeat(" ",fcol);
                }
                int s2 = i * scol;
                int e2 = s2 + scol;
                if (e2 > txt2.length())
                    e2 = txt2.length();
                if(s2>txt2.length())
                    concat=concat+repeat(" ",scol);
                else
                    concat = concat+setLength(txt2.substring(s2, e2), scol, "left") ;

                if(!isSmall) {
                    int s3 = i * tcol;
                    int e3 = s3 + tcol;
                    if (e3 > txt3.length())
                        e3 = txt3.length();
                    if (s3 > txt3.length())
                        concat = concat + repeat(" ", tcol);
                    else
                        concat = concat + setLength(txt3.substring(s3, e3), tcol, "right");
                }

                int s4 = i * frcol;
                int e4 = s4 + frcol;
                if (e4 > txt4.length())
                    e4 = txt4.length();
                if(s4>txt4.length())
                    concat=concat+repeat(" ",frcol);
                else
                    concat = concat+setLength(txt4.substring(s4, e4), frcol, "right") ;

                concat=concat+"\n";
            }
            if(concat.endsWith("\n"))
                concat=concat.substring(0,concat.length()-1);
        }

        concat = getBidiString(concat, Bidi.DIRECTION_LEFT_TO_RIGHT);

        return concat;
    }

//    public String padLine1(String txt1, String txt2, String txt3, String txt4){
//
//            txt1 = getBidiString(txt1, Bidi.DIRECTION_LEFT_TO_RIGHT);
//
////        txt2 = getBidiString(txt2, Bidi.DIRECTION_LEFT_TO_RIGHT);
////        txt3 = getBidiString(txt3, Bidi.DIRECTION_LEFT_TO_RIGHT);
////        txt4 = getBidiString(txt4, Bidi.DIRECTION_LEFT_TO_RIGHT);
//        //txt1-item name,txt2-quantity,txt3-amount,txt4-total
//        txt1=txt1.replaceAll("\\s+$", "");
//        txt2=" "+txt2;
//        String concat="",extra="";
//        Boolean isSmall=false,chk3=true;
//        int columnsPerLine = setSize, fcol = 21,scol=4,tcol=8,frcol=9;
////        if(isProbablyArabic(txt1)) {
////            fcol = 20;
////        }
//
//        if(columnsPerLine!=normalsize){
//            fcol=16;
//            scol=5;
//            frcol=9;
//            tcol=0;
//            isSmall=true;
//        }
//        int maxLines=1,fcharLine=txt1.length()/fcol,scharLine=txt2.length()/scol,frcl=txt4.length()/frcol,tcharLine=0;
//        if(!isSmall)
//            tcharLine=txt3.length()/tcol;
//        else if(tcol>txt3.length())
//            chk3=false;
//        maxLines=fcharLine;
//        if(scharLine>maxLines) {
//            maxLines = scharLine;
//        }
//        if(!isSmall && tcharLine>maxLines) {
//            maxLines = tcharLine;
//        }
//        if(frcl>maxLines) {
//            maxLines = frcl;
//        }
//
//        if(fcol>=txt1.length() && scol>=txt2.length() && frcol>=txt4.length() && chk3){
//            concat = setLength(txt1, fcol, "left");
//            concat = concat + setLength(txt2, scol, "left");
//            if(!isSmall)
//                concat = concat + setLength(txt3, tcol, "right");
//            concat = concat + setLength(txt4, frcol, "right");
//        }else{
//            int lastpos=txt1.length();
//            for (int i = 0; i <= maxLines; i++) {
//                int s1 = i * fcol;
//
//                int e1 = s1 + fcol;
//                if (e1 > txt1.length())
//                    e1 = txt1.length();
//
////                if(maxLines>=1){
////                    if(i==maxLines-1){
////                        String lastosec=txt1.substring(s1, e1);
////                        if(lastosec.contains("-")){
////                            if(txt1.substring(e1,txt1.length()).length()<=fcol)
////                                e1=lastosec.indexOf("-")+1;
////                        }
////                    }else if(i==maxLines){
////                        s1=lastpos;
////                    }
////                }
//
//                if(s1>txt1.length())
//                    concat=concat+repeat(" ",fcol);
//                else
//                    concat = concat+setLength(txt1.substring(s1, e1), fcol, "left") ;
//                lastpos=e1;
//                int s2 = i * scol;
//                int e2 = s2 + scol;
//                if (e2 > txt2.length())
//                    e2 = txt2.length();
//                if(s2>txt2.length())
//                    concat=concat+repeat(" ",scol);
//                else
//                    concat = concat+setLength(txt2.substring(s2, e2), scol, "left") ;
//
//                if(!isSmall) {
//                    int s3 = i * tcol;
//                    int e3 = s3 + tcol;
//                    if (e3 > txt3.length())
//                        e3 = txt3.length();
//                    if (s3 > txt3.length())
//                        concat = concat + repeat(" ", tcol);
//                    else
//                        concat = concat + setLength(txt3.substring(s3, e3), tcol, "right");
//                }
//
//                int s4 = i * frcol;
//                int e4 = s4 + frcol;
//                if (e4 > txt4.length())
//                    e4 = txt4.length();
//                if(s4>txt4.length())
//                    concat=concat+repeat(" ",frcol);
//                else
//                    concat = concat+setLength(txt4.substring(s4, e4), frcol, "right") ;
//
//                concat=concat+"\n";
//            }
//            if(concat.endsWith("\n"))
//                concat=concat.substring(0,concat.length()-1);
//        }
//
//        concat = getBidiString(concat, Bidi.DIRECTION_LEFT_TO_RIGHT);
////        if(concat.endsWith("\n"))
////            concat=concat.substring(0,concat.length()-1);
//
//        return concat;
//    }

    public String padLine2(String txt1,String txt2){
//        txt1 = getBidiString(txt1, Bidi.DIRECTION_LEFT_TO_RIGHT);
        String concat="",extra="";
        int s=0;
        if(txt1!=null)
            s=txt1.length();
        if(txt2!=null)
            s=s+txt2.length();

        Log.d("padline2 --- 1 --set size---", "paper size---"+setSize);
        if(setSize!=smallsize) {
            Log.d("padline2 --- set size---", "paper size---"+setSize);
            int columnsPerLine = setSize;
            if (s <= columnsPerLine) {
                if (txt1.length() == 0 || txt2.length() == 0)
                    concat = txt1 + txt2;
                else
                    concat = txt1 + setLength(txt2, columnsPerLine - txt1.length(), "right");
            } else {
                if (txt1.length() > columnsPerLine) {
                    int b = txt1.length() / columnsPerLine;
                    for (int i = 0; i <= b; i++) {
                        int start = i * columnsPerLine;
                        int end = start + columnsPerLine;
                        if (end > txt1.length())
                            end = txt1.length();
                        concat = concat + setLength(txt1.substring(start, end), columnsPerLine, "center") + "\n";
                    }
                }else{
                    concat=txt1+"\n"+txt2;
                }
            }
        }else{
            int columnsPerLine = smallsize;
            if (s < columnsPerLine) {
                if (txt1.length() == 0 || txt2.length() == 0)
                    concat = txt1 + txt2;
                else
                    concat = txt1 + setLength(txt2, columnsPerLine - txt1.length(), "right");
            } else {
                if (txt1.length() > columnsPerLine && txt2.length() == 0) {
                    int b = txt1.length() / columnsPerLine;
                    for (int i = 0; i <= b; i++) {
                        int start = i * columnsPerLine;
                        int end = start + columnsPerLine;
                        if (end > txt1.length())
                            end = txt1.length();
                        concat = concat + setLength(txt1.substring(start, end), columnsPerLine, "center") + "\n";
                    }
                }else{
                    if(s==columnsPerLine)
                        txt2=setLength(txt2,txt2.length()+1,"right");
                    int fcol=s-txt2.length();
                    if(fcol>txt1.length())
                        concat = txt1 + setLength(txt2, columnsPerLine - txt1.length(), "right");
                    else{
                        int b = txt1.length() / fcol;
                        for (int i = 0; i <= b; i++) {
                            int start = i * fcol;
                            int end = start + fcol;
                            if (end > txt1.length())
                                end = txt1.length();
                            if(i==0)
                                concat = concat + setLength(txt1.substring(start, end), fcol, "center")+txt2+"\n";
                            else
                                extra = extra + setLength(txt1.substring(start, end), fcol, "center") +repeat(" ",txt2.length())+ "\n";
                        }
                        concat=concat+extra;
                    }
                }
            }
        }
        if(concat.endsWith("\n"))
            concat=concat.substring(0,concat.length()-1);
        return concat;
    }

    public String padLine3(String txt1,String txt2){
        //     txt1 = getBidiString(txt1, Bidi.DIRECTION_LEFT_TO_RIGHT);
        if(setSize!=smallsize)
            return txt1+setLength(txt2,15,"right");
        else {
            if(txt1.length()+txt2.length()>14)
                return padLine2(txt1,txt2);
            else
                return txt1 + setLength(txt2, 8, "right");
        }
    }

    public String padLine4(String txt1,String txt2){
        //     txt1 = getBidiString(txt1, Bidi.DIRECTION_LEFT_TO_RIGHT);
        if(setSize!=smallsize)
            return txt1+setLength(txt2,15,"center");
        else {
            if(txt1.length()+txt2.length()>16)
                return padLine2(txt1,txt2);
            else
                return txt1 + setLength(txt2, 12, "center");
        }
    }


    public String alignLeft(String txt){
        String concat="";
        int columnsPerLine =setSize;
        if(txt.contains("\n")) {
            txt=txt.trim();
            String[] a = txt.split("\n");
            for (int i = 0; i < a.length; i++) {
                String t=a[i];
                if(t.length()<=columnsPerLine)
                    concat = concat + setLength(t, columnsPerLine, "left")+"\n";
                else{
                    int b = t.length() / columnsPerLine;
                    String extra="";
                    for (int i1 = 0; i1 <= b; i1++) {
                        int start = i1 * columnsPerLine;
                        int end = start + columnsPerLine;
                        if (end > t.length())
                            end = t.length();
                        if(i1==0)
                            concat = concat + setLength(t.substring(start, end), columnsPerLine, "left")+"\n";
                        else
                            extra = extra + setLength(t.substring(start, end), columnsPerLine, "left") + "\n";
                    }
                    concat=concat+extra;
                }

            }
        }else {
            String t=txt;
            int b = t.length() / columnsPerLine;
            String extra="";
            for (int i1 = 0; i1 <= b; i1++) {
                int start = i1 * columnsPerLine;
                int end = start + columnsPerLine;
                if (end > t.length())
                    end = t.length();
                if(i1==0)
                    concat = concat + setLength(t.substring(start, end), columnsPerLine, "left")+"\n";
                else
                    extra = extra + setLength(t.substring(start, end), columnsPerLine, "left") + "\n";
            }
            concat=concat+extra;
        }
        if(concat.endsWith("\n"))
            concat=concat.substring(0,concat.length()-1);
        return concat;
    }

    public String setLength(String txt,int size,String side){

        int padding=size-txt.length();
        if(padding>0) {
            if (side.equals("center")) {
                int p = padding / 2;
                int ep = padding - p;
                return repeat(" ", p) + txt + repeat(" ", ep);
            } else if (side.equals("right")) {
                return repeat(" ", padding) + txt;
            } else {
                return txt + repeat(" ", padding);
            }
        }else
            return txt.substring(0,size);

    }

    public String divider(){
        if(setSize==smallsize)
            return repeat("-",smallsize);
        else
            return repeat("-", setSize);
    }

    protected String repeat(String str, int i){
        return new String(new char[i]).replace("\0", str);
    }

    public String setFormat(String value){
        if(value!=null && value.trim().length()>0) {
            try {
                return String.format(Locale.ENGLISH, "%."+M.getDecimalVal(context)+"f", Double.parseDouble(value));
            }catch (NumberFormatException e){
                return value;
            }

        }else
            return "";
    }


    public static String getBidiString(String input, int direction) {

        if(!isProbablyArabic(input)){
            return input;
        }
        boolean rtlContext;
        int defaultBidiDirection;
        if (direction == Bidi.DIRECTION_LEFT_TO_RIGHT) {
            rtlContext = false;
            defaultBidiDirection = Bidi.DIRECTION_DEFAULT_LEFT_TO_RIGHT;
        } else /* if (lang == Util.Lang.HE) */{
            rtlContext = true;
            defaultBidiDirection = Bidi.DIRECTION_DEFAULT_LEFT_TO_RIGHT;
        }


        Bidi bidi = new Bidi(input,defaultBidiDirection);



        BidiFormatter bidiFormatter = BidiFormatter.getInstance(rtlContext);
        StringBuilder bidiTestBuilder = new StringBuilder();
        for (int i = 0; i < bidi.getRunCount(); i++)
        {
            int start = bidi.getRunStart(i);
            int level = bidi.getRunLevel(i);
            int limit = bidi.getRunLimit(i);
            String run = input.substring(start, limit);

            if (level != direction) {
                run = bidiFormatter.unicodeWrap(run) + " ";
            }
            bidiTestBuilder.append(run);
        }
        return bidiTestBuilder.toString().trim();
    }


}