package com.royalpos.waiter.print;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.royalpos.waiter.print.newbluetooth.ReceiptBitmap;
import com.starmicronics.starioextension.ICommandBuilder;
import com.starmicronics.starioextension.StarIoExt;
import com.starmicronics.starprntsdk.CommonAlertDialogFragment;
import com.starmicronics.starprntsdk.Communication;
import com.starmicronics.starprntsdk.ModelCapability;
import com.starmicronics.starprntsdk.PrinterSettingManager;
import com.starmicronics.starprntsdk.PrinterSettings;
import com.royalpos.waiter.helper.AppConst;
import com.royalpos.waiter.helper.MemoryCache;
import com.royalpos.waiter.helper.RoundHelper;
import com.royalpos.waiter.model.M;
import com.royalpos.waiter.print.newbluetooth.GlobalsNew;

import org.json.JSONObject;

import java.io.File;

/**
 * Created by reeva on 20/06/19.
 */

public class StarPrintReceipt implements CommonAlertDialogFragment.Callback {
    ICommandBuilder builder;
    Context context;
    Activity activity;
    String cashprintermodel, cashprintertarget;
    JSONObject jsonObject;
    String TAG = "STARPrintReceipt";
    RoundHelper roundHelper;
    PrintFormat pf;
    public static boolean isnormal = false;
    PrinterSettingManager settingManager;
    PrinterSettings settings;
    public static int size = 25;
    public static int medsize = 30;
    public static int bigsize = 35;
    public StarPrintReceipt(Context context, Activity activity, JSONObject jsonObject) {
        MemoryCache memoryCache = new MemoryCache();
        memoryCache.clear();
        this.context = context;
        this.activity = activity;
        this.jsonObject = jsonObject;
        roundHelper = new RoundHelper(context);
        pf = new PrintFormat(context);
        cashprintermodel = M.getCashPrinterModel(context);
        cashprintertarget = M.getCashPrinterIP(context);
    }


    @Override
    public void onDialogResult(String tag, Intent data) {

    }

    public void print() {

        byte[] data;

        settingManager = new PrinterSettingManager(context);
        settings = settingManager.getPrinterSettings();

        data = "".getBytes();

        Communication.sendCommands(this, data, settings.getPortName(),
                settings.getPortSettings(), 10000, 30000,
                context, mCallback);     // 10000mS!!!

    }


    public void printusingstar(){

        Log.d(TAG, "STAR Print---");
        try {
            String txt=jsonObject.getString("data");
            String[] data=txt.split("\n");
            File myDir = new File(AppConst.folder_dir + "logo.png");
            StarIoExt.Emulation emulation = ModelCapability.getEmulation(settings.getModelIndex());
            builder = StarIoExt.createCommandBuilder(emulation);


            builder.beginDocument();

            if (myDir.exists() ) {
                Bitmap myBitmap = BitmapFactory.decodeFile(myDir.getAbsolutePath());

                builder.appendBitmap(myBitmap, false);
            }
            for(String s:data){
                String s1= GlobalsNew.prepareDataToPrint1(s);
                if(s1.trim().length()>0) {
                    if (pf.divider().equals(s)) {

                        printline();
                    }else if (s.contains("$cutt")) {

                        printNewLine();
                        cutpaper();
                        printNewLine();
                    }
                    else if (s.contains("$big")) {
                        if (PrintFormat.setSize == PrintFormat.normalsize) {
                            printText(s1, bigsize, true, context);
                        } else {
                            printText(s1, medsize, true, context);
                        }

                    } else{
                        printText(s1, size, false, context);
                    }

                }
            }
            builder.endDocument();

        } catch (Exception e) {
            Log.d(TAG, "error:" + e.getMessage());

        }

    }

    public void printBill(String textData){
        byte[] data;

        PrinterSettingManager settingManager = new PrinterSettingManager(context);
        PrinterSettings settings       = settingManager.getPrinterSettings();


        StarIoExt.Emulation emulation = ModelCapability.getEmulation(settings.getModelIndex());
        data = PrintMainActivity.createGenericData(textData, emulation);

        Communication.sendCommands(this, data, settings.getPortName(),
                settings.getPortSettings(), 10000, 30000,
                context, mCallback);     // 10000mS!!!
    }

    public void printKitchen(Context context, String textData,String portnm,String portsetting,int modelIndex){
        byte[] data;
        StarIoExt.Emulation emulation = ModelCapability.getEmulation(modelIndex);
        data = PrintMainActivity.createGenericData(textData, emulation);

        Communication.sendCommands(this, data,portnm,
                portsetting, 10000, 30000,
                context, mCallback);     // 10000mS!!!
    }

    private final Communication.SendCallback mCallback = new Communication.SendCallback() {
        @Override
        public void onStatus(Communication.CommunicationResult communicationResult) {



        }
    };


    //print new line
    public void printNewLine() {

        builder.append("\n".getBytes());
        sendcommand(builder);

    }



    public void printline() {
//        printNewLine();
        ReceiptBitmap receiptBitmap = new ReceiptBitmap().getInstance();
        int    l = 5;    if(isnormal) {
            receiptBitmap.init(500,l);
            receiptBitmap.drawNewLine(450);
        }else{
            receiptBitmap.init(384,l);
            receiptBitmap.drawNewLine(350);
        }    Bitmap canvasbitmap = receiptBitmap.getReceiptBitmap();
        builder.appendBitmap(canvasbitmap, true);

        sendcommand(builder);
    }

    //print text
    public void printText(String msg, int size, boolean isbold, Context context) {
//        try {
        String[] txt=msg.split("\n");
        ReceiptBitmap receiptBitmap = new ReceiptBitmap().getInstance();
        int l=(txt.length+1)*25;
        int width = 384;
        if(isnormal) {
            width = 584;
            receiptBitmap.init(500,l);
        }else{
            receiptBitmap.init(384,l);
        }
        Log.d("here----", "size---"+l );
        for(String t:txt)
            receiptBitmap.drawadvCenterText(t, size, isbold);

        Bitmap canvasbitmap = receiptBitmap.getReceiptBitmap();


        Bitmap croppedBmp = Bitmap.createBitmap(canvasbitmap, 0, 0, canvasbitmap.getWidth(), canvasbitmap.getHeight());



        builder.appendBitmap(croppedBmp, false);
        builder.append((byte) 0x0a);

        sendcommand(builder);
    }

    public void sendcommand(ICommandBuilder builder){

        Communication.sendCommands(context, builder.getCommands(), settings.getPortName(),
                settings.getPortSettings(), 10000, 30000,
                context, mCallback);

    }

    public void cutpaper()
    {
        builder.appendCutPaper(ICommandBuilder.CutPaperAction.PartialCutWithFeed);
        sendcommand(builder);
    }

    public void opendrawer(){
        builder.appendPeripheral(ICommandBuilder.PeripheralChannel.No1);
        builder.appendPeripheral(ICommandBuilder.PeripheralChannel.No2);
        sendcommand(builder);
    }
}
