package com.royalpos.waiter.print.netprint;

public interface AsyncPrintCallBack {
	
	void statusChange();
    void printErrorResult(String output);
    void printResult(String output);
}
