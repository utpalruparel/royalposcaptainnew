package com.royalpos.waiter.print.newbluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.Log;

import androidx.annotation.Nullable;

import com.royalpos.waiter.R;
import com.royalpos.waiter.helper.AppConst;
import com.royalpos.waiter.helper.RoundHelper;
import com.royalpos.waiter.model.M;
import com.royalpos.waiter.print.PrintFormat;
import com.royalpos.waiter.print.PrintFormatNew;
import com.royalpos.waiter.receipt.ReceiptBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.greenrobot.event.EventBus;

import static java.lang.Math.abs;


public class GlobalsNew {
    // TODO IMPORTANT CONFIG CHANGES
    // ** IMPORTANT CONFIGURATION
    private static String serverRealUrl = "http://www.myserver.com/api";    //Real Server configuration
    private static String serverLocalUrl = "http://localhost:80/api";             //local configuration
    /**
     * Substitute you own sender ID here. This is the project number you got
     * from the API Console, as described in "Getting Started."
     */
    public static String TAG = "GLOBAL";
    public static String SENDER_ID = "000000";
    public static String base64EncodedPublicKey = "xxxxx";
    //DEBUG MODO
    public static boolean debugModeBoolean = true;  //USE in DEBUG MODE
    //public static boolean debugModeBoolean = false;  //USE in REAL  MODE


    //PRINT FROM WEB ENABLES
    //public static boolean printFronWebEnabled = true;  //USE to activate print fron web
    public static boolean printFronWebEnabled = false;  //USE  to deactivate print fron web

    //GCM Register
    //public static boolean gcmRegisterEnabled = true;  //USE to register device in GCM
    public static boolean gcmRegisterEnabled = false;  // not use gcm services


    //IN APP BILLING
    //public static boolean inAppBillingModeON = true;  //USE in pay app modo on
    public static boolean inAppBillingModeON = false;  //USE in pay app modo off


    private static String internalClassName = "Globals";    // * Tag used on log messages.
    public static int deviceType = 0;                       // * Device type
    //0 Pending config (Default as start)
    //1 NET Print
    //2 RS232   ->Pending
    //3 USB
    //4 BT
    //5 SUNMI
    //6 MSWIPE (wise pos plus)
    //7 EPSON
    //8 mswipe (wise pos neo)

    //Google subscriptions
    public static boolean mSubscribedToYearlyUsed = false;


    // Bluetooth  variable Config
    public static BluetoothAdapter mBluetoothAdapter;
    public static BluetoothSocket mmSocket;
    public static BluetoothDevice mmDevice;


    public static String logoProcesed = "";

    public static OutputStream mmOutputStream;
    public static InputStream mmInputStream;
    public static Typeface arabicfont ;

    // NET Printer Config
    public static String deviceIP = "";
    public static int devicePort = 0;
    public static int usbDeviceID = 0;
    public static int usbVendorID = 0;
    public static int usbProductID = 0;
    public static String blueToothDeviceAdress = "";
    public static int blueToothSpinnerSelected = -1;

    public static String tmpDeviceIP = "";
    public static int tmpDevicePort = 0;
    public static int tmpUsbDeviceID = 0;
    public static int tmpUsbVendorID = 0;
    public static int tmpUsbProductID = 0;
    public static String link_code = "";
    public static String tmpBlueToothDeviceAdress = "";
    public static  ReceiptBuilder receiptBuilder = null;
    public static PrintFormatNew pf;
    public static RoundHelper roundHelper;
    public static JSONObject jsonObject =null;
    public static int size = 25;
    public static int medsize = 30;
    public static int bigsize = 35;
    public static boolean isbold = false;
    public static boolean isnormal = false;

    public static String getReceipttype() {
        return receipttype;
    }

    public static void setReceipttype(String receipttype) {
        GlobalsNew.receipttype = receipttype;
    }

    public static String receipttype= null;

    // 1 = restaurants - not used
    // 2 = retail & service,restaurants
    // 3 = end day recipts
    // 4 = stock prints
    // 5 = test print

    public static JSONObject getJsonObject() {
        return jsonObject;
    }

    public static void setJsonObject(JSONObject jsonObject) {
        GlobalsNew.jsonObject = jsonObject;
    }

    public static String getServerUrl() {
        String actualServer = "";
        if (debugModeBoolean) {
            actualServer = serverLocalUrl;
        } else {
            actualServer = serverRealUrl;
        }

        return actualServer;
    }

    public static String server_registerPrinter = "registerPrinter";
    public static String server_getData = "getData";
    public static String regid = "";
    public static String picturePath = "";

    public static Bitmap imgbm = null;

    public static void savePreferences(SharedPreferences sharedPrefs) {
        Log.i(internalClassName, "savePreferences");


        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putString("usbDeviceID", String.valueOf(usbDeviceID));
        editor.putString("usbVendorID", String.valueOf(usbVendorID));
        editor.putString("usbProductID", String.valueOf(usbProductID));


        editor.putString("blueToothDeviceAdress", String.valueOf(blueToothDeviceAdress));


        editor.putString("deviceType", String.valueOf(deviceType));
        //Ethernet
        editor.putString("deviceIP", String.valueOf(deviceIP));
        editor.putString("devicePort", String.valueOf(devicePort));

        editor.putString("link_code", String.valueOf(link_code));
        editor.putString("picturePath", String.valueOf(picturePath));
        editor.putString("logoProcesed", logoProcesed);

        Log.i(internalClassName, "               deviceType=" + String.valueOf(deviceType));
        Log.i(internalClassName, "               deviceIP=" + String.valueOf(deviceIP));
        Log.i(internalClassName, "               devicePort=" + String.valueOf(devicePort));
        Log.i(internalClassName, "               usbDeviceID=" + String.valueOf(usbDeviceID));
        Log.i(internalClassName, "               blueToothDeviceAdress=" + String.valueOf(blueToothDeviceAdress));


        editor.commit();

    }

    public static void savePreferenceskitchen(SharedPreferences sharedPrefs) {
        Log.i(internalClassName, "savePreferences");


        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putString("usbDeviceID", String.valueOf(usbDeviceID));
        editor.putString("usbVendorID", String.valueOf(usbVendorID));
        editor.putString("usbProductID", String.valueOf(usbProductID));


        editor.putString("blueToothDeviceAdress", String.valueOf(blueToothDeviceAdress));


        editor.putString("deviceType", String.valueOf(deviceType));
        //Ethernet
        editor.putString("deviceIP", String.valueOf(deviceIP));
        editor.putString("devicePort", String.valueOf(devicePort));

        editor.putString("link_code", String.valueOf(link_code));
        editor.putString("picturePath", String.valueOf(picturePath));
        editor.putString("logoProcesed", logoProcesed);

        Log.i(internalClassName, "               deviceType=" + String.valueOf(deviceType));
        Log.i(internalClassName, "               deviceIP=" + String.valueOf(deviceIP));
        Log.i(internalClassName, "               devicePort=" + String.valueOf(devicePort));
        Log.i(internalClassName, "               usbDeviceID=" + String.valueOf(usbDeviceID));
        Log.i(internalClassName, "               blueToothDeviceAdress=" + String.valueOf(blueToothDeviceAdress));


        editor.commit();

    }

    public static void loadPreferences(SharedPreferences sharedPrefs) {
        Log.i(internalClassName, "loadPreferences");

        usbDeviceID = mathIntegerFromString(sharedPrefs.getString("usbDeviceID", "0"), 0);
        usbVendorID = mathIntegerFromString(sharedPrefs.getString("usbVendorID", "0"), 0);
        usbProductID = mathIntegerFromString(sharedPrefs.getString("usbProductID", "0"), 0);


        link_code = sharedPrefs.getString("link_code", "");
        picturePath = sharedPrefs.getString("picturePath", "");
        blueToothDeviceAdress = sharedPrefs.getString("blueToothDeviceAdress", "x:x:x:x");

        deviceType = mathIntegerFromString(sharedPrefs.getString("deviceType", "0"), 0);
        //Ethernet
        deviceIP = sharedPrefs.getString("deviceIP", "192.168.0.98");
        devicePort = mathIntegerFromString(sharedPrefs.getString("devicePort", "9100"), 9100);
        logoProcesed = sharedPrefs.getString("logoProcesed", "");


        Log.i(internalClassName, "               deviceType=" + String.valueOf(deviceType));
        Log.i(internalClassName, "               deviceIP=" + String.valueOf(deviceIP));
        Log.i(internalClassName, "               devicePort=" + String.valueOf(devicePort));
        Log.i(internalClassName, "               usbDeviceID=" + String.valueOf(usbDeviceID));
        Log.i(internalClassName, "               blueToothDeviceAdress=" + String.valueOf(blueToothDeviceAdress));


    }

    public static void loadPreferenceskitchen(SharedPreferences sharedPrefs) {
        Log.i(internalClassName, "loadPreferences");


        usbDeviceID = mathIntegerFromString(sharedPrefs.getString("usbDeviceID", "0"), 0);
        usbVendorID = mathIntegerFromString(sharedPrefs.getString("usbVendorID", "0"), 0);
        usbProductID = mathIntegerFromString(sharedPrefs.getString("usbProductID", "0"), 0);


        link_code = sharedPrefs.getString("link_code", "");
        picturePath = sharedPrefs.getString("picturePath", "");
        blueToothDeviceAdress = sharedPrefs.getString("blueToothDeviceAdress", "x:x:x:x");

        deviceType = mathIntegerFromString(sharedPrefs.getString("deviceType", "0"), 0);
        //Ethernet
        deviceIP = sharedPrefs.getString("deviceIP", "192.168.0.98");
        devicePort = mathIntegerFromString(sharedPrefs.getString("devicePort", "9100"), 9100);
        logoProcesed = sharedPrefs.getString("logoProcesed", "");


        Log.i(internalClassName, "               deviceType=" + String.valueOf(deviceType));
        Log.i(internalClassName, "               deviceIP=" + String.valueOf(deviceIP));
        Log.i(internalClassName, "               devicePort=" + String.valueOf(devicePort));
        Log.i(internalClassName, "               usbDeviceID=" + String.valueOf(usbDeviceID));
        Log.i(internalClassName, "               blueToothDeviceAdress=" + String.valueOf(blueToothDeviceAdress));


    }

    public static Integer mathIntegerFromString(String inti, Integer defi) {
        //used for preferences
        Integer returi = defi;
        if (!inti.equals("")) {
            try {
                returi = Integer.valueOf(inti);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }

        return returi;

    }

    public static String prepareDataToPrint(String data) {
        //ESC POS Comamnds convertions


        data = data.replace("$bold$", "·27·E·1·");
        data = data.replace("$unbold$", "·27·E·0·");


        data = data.replace("$intro$", "·13··10·");
        data = data.replace("$cut$", "·27·m");
        data = data.replace("$cutt$", "·27·i");

        data = data.replace("$al_left$", "·27·a·0·");
        data = data.replace("$al_center$", "·27·a·1·");
        data = data.replace("$al_right$", "·27·a·2·");

        data = data.replace("$small$", "·27·!·1·");
        data = data.replace("$smallh$", "·27·!·17·");
        data = data.replace("$smallw$", "·27·!·33·");
        data = data.replace("$smallhw$", "·27·!·49·");

        data = data.replace("$smallu$", "·27·!·129·");
        data = data.replace("$smalluh$", "·27·!·145·");
        data = data.replace("$smalluw$", "·27·!·161·");
        data = data.replace("$smalluhw$", "·27·!·177·");

        data = data.replace("$big$", "·27·!·0·");
        data = data.replace("$bigh$", "·27·!·16·");
        data = data.replace("$bigw$", "·27·!·32·");
        data = data.replace("$bighw$", "·27·!·48·");
        data = data.replace("$bigl$", "·27·!·12·");
        data = data.replace("$bigu$", "·27·!·128·");
        data = data.replace("$biguh$", "·27·!·144·");
        data = data.replace("$biguw$", "·27·!·160·");
        data = data.replace("$biguhw$", "·27·!·176·");

        data = data.replace("$drawer$", "·27··112··48··200··100·");
        data = data.replace("$drawer2$", "·27··112··49··200··100·");


        data = data.replace("$enter$", "·13··10·");

        data = data.replace("$letrap$", "·27·!·1·");
        data = data.replace("$letraph$", "·27·!·17·");
        data = data.replace("$letrapw$", "·27·!·33·");
        data = data.replace("$letraphw$", "·27·!·49·");

        data = data.replace("$letrapu$", "·27·!·129·");
        data = data.replace("$letrapuh$", "·27·!·145·");
        data = data.replace("$letrapuw$", "·27·!·161·");
        data = data.replace("$letrapuhw$", "·27·!·177·");


        data = data.replace("$letrag$", "·27·!·0·");
        data = data.replace("$letragh$", "·27·!·16·");
        data = data.replace("$letragw$", "·27·!·32·");
        data = data.replace("$letraghw$", "·27·!·48·");

        data = data.replace("$letragu$", "·27·!·128·");
        data = data.replace("$letraguh$", "·27·!·144·");
        data = data.replace("$letraguw$", "·27·!·160·");
        data = data.replace("$letraguhw$", "·27·!·176·");


        data = data.replace("$letracorte$", "·27·m");
        data = data.replace("$LETRACORTE$", "·27·m");


        data = data.replace("$ENTER$", "·13··10·");

        data = data.replace("$LETRAP$", "·27·!·1·");
        data = data.replace("$LETRAPH$", "·27·!·17·");
        data = data.replace("$LETRAPW$", "·27·!·33·");
        data = data.replace("$LETRAPHW$", "·27·!·49·");

        data = data.replace("$LETRAPU$", "·27·!·129·");
        data = data.replace("$LETRAPUH$", "·27·!·145·");
        data = data.replace("$LETRAPUW$", "·27·!·161·");
        data = data.replace("$LETRAPUHW$", "·27·!·177·");


        data = data.replace("$LETRAG$", "·27·!·0·");
        data = data.replace("$LETRAGH$", "·27·!·16·");
        data = data.replace("$LETRAGW$", "·27·!·32·");
        data = data.replace("$LETRAGHW$", "·27·!·48·");

        data = data.replace("$LETRAGU$", "·27·!·128·");
        data = data.replace("$LETRAGUH$", "·27·!·144·");
        data = data.replace("$LETRAGUW$", "·27·!·160·");
        data = data.replace("$LETRAGUHW$", "·27·!·176·");



        data = data.replace("á", "·160·");
        data = data.replace("à", "·133·");
        data = data.replace("â", "·131·");
        data = data.replace("ä", "·132·");
        data = data.replace("å", "·134·");

        data = data.replace("Á", "·193·");
        data = data.replace("À", "·192·");
        data = data.replace("Â", "·194·");
        data = data.replace("Ä", "·142·");
        data = data.replace("Å", "·143·");


        data = data.replace("é", "·130·");
        data = data.replace("è", "·138·");
        data = data.replace("ê", "·136·");
        data = data.replace("ë", "·137·");
        //data = data.replace("","··");


        data = data.replace("É", "·144·");
        data = data.replace("È", "··");
        data = data.replace("Ê", "··");
        data = data.replace("Ë", "··");

        data = data.replace("ñ", "·164·");
        data = data.replace("Ñ", "·165·");

        //data = data.replace("","··");


        data = data.replace("í", "··");
        data = data.replace("ì", "·141·");
        data = data.replace("î", "·140·");
        data = data.replace("ï", "·139·");
        //data = data.replace("","··");


        data = data.replace("ó", "·149·");
        data = data.replace("ò", "··");
        data = data.replace("ô", "·147·");
        data = data.replace("ö", "·148·");
        //data = data.replace("","··");

        data = data.replace("Ó", "··");
        data = data.replace("Ò", "··");
        data = data.replace("Ô", "··");
        data = data.replace("Ö", "·153·");
        //data = data.replace("","··");

        data = data.replace("ú", "··");
        data = data.replace("ù", "·151·");
        data = data.replace("û", "··");
        data = data.replace("ü", "·129·");
        //data = data.replace("","··");


        data = data.replace("ú", "··");
        data = data.replace("ù", "·151·");
        data = data.replace("û", "··");
        data = data.replace("ü", "·129·");

        //data = data.replace("..", "");

        for (int l = 0; l <= 255; l++) {
            data = data.replace("·" + String.valueOf(l) + "·", String.valueOf(((char) l)));
            data = data.replace("$codepage" + String.valueOf(l) + "$",  String.valueOf(((char) 27)) + "t" + String.valueOf(((char) l)));
        }
        Log.d("here----", ""+data);

        return data;
    }

    public static String prepareDataToPrint1(String data) {
        //ESC POS Comamnds convertions


        data = data.replace("$bold$", "");
        data = data.replace("$unbold$", "");


        data = data.replace("$intro$", "");
        data = data.replace("$cut$", "");
        data = data.replace("$cutt$", "");

        data = data.replace("$al_left$", "");
        data = data.replace("$al_center$", "");
        data = data.replace("$al_right$", "");

        data = data.replace("$small$", "");
        data = data.replace("$smallh$", "");
        data = data.replace("$smallw$", "");
        data = data.replace("$smallhw$", "");

        data = data.replace("$smallu$", "");
        data = data.replace("$smalluh$", "");
        data = data.replace("$smalluw$", "");
        data = data.replace("$smalluhw$", "");

        data = data.replace("$big$", "");
        data = data.replace("$bigh$", "");
        data = data.replace("$bigw$", "");
        data = data.replace("$bighw$", "");
        data = data.replace("$bigl$", "");
        data = data.replace("$bigu$", "");
        data = data.replace("$biguh$", "");
        data = data.replace("$biguw$", "");
        data = data.replace("$biguhw$", "");

        data = data.replace("$drawer$", "");
        data = data.replace("$drawer2$", "");


        data = data.replace("$enter$", "");

        data = data.replace("$letrap$", "");
        data = data.replace("$letraph$", "");
        data = data.replace("$letrapw$", "");
        data = data.replace("$letraphw$", "");

        data = data.replace("$letrapu$", "");
        data = data.replace("$letrapuh$", "");
        data = data.replace("$letrapuw$", "");
        data = data.replace("$letrapuhw$", "");


        data = data.replace("$letrag$", "");
        data = data.replace("$letragh$", "");
        data = data.replace("$letragw$", "");
        data = data.replace("$letraghw$", "");

        data = data.replace("$letragu$", "");
        data = data.replace("$letraguh$", "");
        data = data.replace("$letraguw$", "");
        data = data.replace("$letraguhw$", "");


        data = data.replace("$letracorte$", "");
        data = data.replace("$LETRACORTE$", "");


        data = data.replace("$ENTER$", "");

        data = data.replace("$LETRAP$", "");
        data = data.replace("$LETRAPH$", "");
        data = data.replace("$LETRAPW$", "");
        data = data.replace("$LETRAPHW$", "");

        data = data.replace("$LETRAPU$", "");
        data = data.replace("$LETRAPUH$", "");
        data = data.replace("$LETRAPUW$", "");
        data = data.replace("$LETRAPUHW$", "");


        data = data.replace("$LETRAG$", "");
        data = data.replace("$LETRAGH$", "");
        data = data.replace("$LETRAGW$", "");
        data = data.replace("$LETRAGHW$", "");

        data = data.replace("$LETRAGU$", "");
        data = data.replace("$LETRAGUH$", "");
        data = data.replace("$LETRAGUW$", "");
        data = data.replace("$LETRAGUHW$", "");
        data = data.replace("$codepage3$", "");


//        data = data.replace("á", "·160·");
//        data = data.replace("à", "·133·");
//        data = data.replace("â", "·131·");
//        data = data.replace("ä", "·132·");
//        data = data.replace("å", "·134·");
//
//        data = data.replace("Á", "·193·");
//        data = data.replace("À", "·192·");
//        data = data.replace("Â", "·194·");
//        data = data.replace("Ä", "·142·");
//        data = data.replace("Å", "·143·");
//
//
//        data = data.replace("é", "·130·");
//        data = data.replace("è", "·138·");
//        data = data.replace("ê", "·136·");
//        data = data.replace("ë", "·137·");
//        //data = data.replace("","··");
//
//
//        data = data.replace("É", "·144·");
//        data = data.replace("È", "··");
//        data = data.replace("Ê", "··");
//        data = data.replace("Ë", "··");
//
//        data = data.replace("ñ", "·164·");
//        data = data.replace("Ñ", "·165·");
//
//        //data = data.replace("","··");
//
//
//        data = data.replace("í", "··");
//        data = data.replace("ì", "·141·");
//        data = data.replace("î", "·140·");
//        data = data.replace("ï", "·139·");
//        //data = data.replace("","··");
//
//
//        data = data.replace("ó", "·149·");
//        data = data.replace("ò", "··");
//        data = data.replace("ô", "·147·");
//        data = data.replace("ö", "·148·");
//        //data = data.replace("","··");
//
//        data = data.replace("Ó", "··");
//        data = data.replace("Ò", "··");
//        data = data.replace("Ô", "··");
//        data = data.replace("Ö", "·153·");
//        //data = data.replace("","··");
//
//        data = data.replace("ú", "··");
//        data = data.replace("ù", "·151·");
//        data = data.replace("û", "··");
//        data = data.replace("ü", "·129·");
//        //data = data.replace("","··");
//
//
//        data = data.replace("ú", "··");
//        data = data.replace("ù", "·151·");
//        data = data.replace("û", "··");
//        data = data.replace("ü", "·129·");

        //data = data.replace("..", "");

//        for (int l = 0; l <= 255; l++) {
//            data = data.replace("·" + String.valueOf(l) + "·", String.valueOf(((char) l)));
//            data = data.replace("$codepage" + String.valueOf(l) + "$",  String.valueOf(((char) 27)) + "t" + String.valueOf(((char) l)));
//        }
        Log.d("here----", ""+data);

        return data;
    }

    protected static String padLine(@Nullable String partOne, @Nullable String partTwo, int columnsPerLine, String str){
        if(partOne == null) {partOne = "";}
        if(partTwo == null) {partTwo = "";}
        if(str == null) {str = " ";}
        String concat;
        if((partOne.length() + partTwo.length()) > columnsPerLine) {
            concat = partOne + str + partTwo;
        } else {
            int padding = columnsPerLine - (partOne.length() + partTwo.length());
            concat = partOne + repeat(str, padding) + partTwo;
        }
        return concat;
    }

    protected static String repeat(String str, int i){
        return new String(new char[i]).replace("\0", str);
    }

    public static String getImageDataPosPrinterLogo(Bitmap bm) {
        String INITIALIZE_PRINTER = "·27··64·";//Chr(&H1B) & Chr(&H40);
        String PRINT_AND_FEED_PAPER = "·10·";//Chr(&HA);
        String CMDSELECT_BIT_IMAGE_MODE = "·27··42·";//Chr(&H1B) & Chr(&H2A);
        String SET_LINE_SPACING = "·27··51·";//Chr(&H1B) & Chr(&H33);
        String SETLINESPACING24 = SET_LINE_SPACING + "·24·";

        BitmapFactory.Options options_load = new BitmapFactory.Options();
        options_load.inScaled = false;
        Bitmap miImagetest = null;
        Bitmap miImage = bm;



        int image_h = miImage.getHeight();
        int p_Width = miImage.getWidth();


        String widthLSB = "";
        String widthMSB = "";
        String hexdata = "0000" + Integer.toHexString(p_Width);
        widthLSB = hexdata.substring(hexdata.length() - 2, hexdata.length());
        widthMSB = hexdata.substring(hexdata.length() - 4, hexdata.length() - 2);


        String SELECTBITIMAGEMODE = CMDSELECT_BIT_IMAGE_MODE + "·33··" + Integer.parseInt(widthLSB, 16) + "··" + Integer.parseInt(widthMSB, 16) + "·";// & Chr(33) & Chr(widthLSB) & Chr(widthMSB)


        String p_TxtPrint = INITIALIZE_PRINTER;
        p_TxtPrint = p_TxtPrint + SETLINESPACING24;
        String p_DatatoPrint = "";
        int offset = 0;
        while (offset < image_h) {
            int imageDataLineIndex = 0;
            int p_BytesAncho = 3 * p_Width - 1;
            int[] imageDataLine = new int[p_BytesAncho + 10];//(3 * (image_w + 10))];
            for (int X = 0; X < p_Width; X++) {
                //' Remember, 24 dots = 24 bits = 3 bytes.
                //' The ' k ' variable keeps track of which of those
                //' three bytes that we' re currently scribbling into.
                for (int K = 0; K <= 2; K++) {
                    int slice = 0;
                    String strSlice = "";
                    //' A byte is 8 bits. The ' b ' variable keeps track
                    //' of which bit in the byte we' re recording.
                    for (int b = 0; b <= 7; b++) {
                        //' Calculate the y position that we' re currently
                        //' trying to draw. We take our offset, divide it
                        //' by 8 so we' re talking about the y offset in
                        //' terms of bytes, add our current ' k ' byte
                        //' offset to that, multiple by 8 to get it in terms
                        //' of bits again, and add our bit offset to it.
                        int zz2 = abs(offset / 8);
                        int y = ((zz2 + K) * 8) + b;
                        int I = (y * (p_Width)) + (X);
                        //int y = (((abs(offset / 8)) + K) * 8) + b;
                        //' Calculate the location of the pixel we want in the bit array  It' ll be at(y * width) + x.
                        // int I = (y * p_Width) + X;
                        // ' If the image (or this stripe of the image)                         ' is shorter than 24 dots, pad with zero.
                        boolean v = false;

                        if (I <= (image_h * p_Width)) {
                            //Log.e(internalClassName, "IMAGEN= getImageData x,y:" + String.valueOf(X) + "," + String.valueOf(y));
                            if (y < image_h) {
                                int midato = miImage.getPixel(X, y);
                                int alfa = (midato >> 24) & 0xff;     //bitwise shifting

                                int R = (midato >> 16) & 0xff;     //bitwise shifting
                                int G = (midato >> 8) & 0xff;
                                int B = midato & 0xff;
                                double total = abs(0.2126 * R + 0.7152 * G + 0.0722 * B);

                                // Log.e(internalClassName, "valores:  total===" + String.valueOf(total)  + "->%="  );
                                if (total > 80) {
                                    v = true;
                                }
                            } else {
                                v = true;
                            }
                        } else {
                            v = true;
                        }
                        if (v == true) {
                            strSlice = strSlice + "0";
                        } else {
                            strSlice = strSlice + "1";
                        }
                    }

                    imageDataLine[imageDataLineIndex + K] = Integer.parseInt(strSlice, 2);
                }
                imageDataLineIndex = imageDataLineIndex + 3;
            }
            String p_dataBytes = "";
            for (int XX = 0; XX < imageDataLineIndex; XX++) {
                p_dataBytes = p_dataBytes + String.valueOf(((char) imageDataLine[XX]));
            }
            p_DatatoPrint = p_DatatoPrint + SELECTBITIMAGEMODE + p_dataBytes + PRINT_AND_FEED_PAPER;

            offset = offset + 24;
        }

        p_TxtPrint = p_TxtPrint + p_DatatoPrint + SETLINESPACING24 + INITIALIZE_PRINTER;
        p_TxtPrint = p_TxtPrint + "·29··60·";//& Chr( & H1D)&Chr( & H3C)

        return p_TxtPrint;
    }

    public static List<String> binaryListToHexStringList(List<String> list) {
        List<String> hexList = new ArrayList<String>();
        for (String binaryStr : list) {
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < binaryStr.length(); i += 8) {
                String str = binaryStr.substring(i, i + 8);

                String hexString = myBinaryStrToHexString(str);
                sb.append(hexString);
            }
            hexList.add(sb.toString());
        }
        return hexList;

    }

    private static String[] binaryArray = { "0000", "0001", "0010", "0011",
            "0100", "0101", "0110", "0111", "1000", "1001", "1010", "1011",
            "1100", "1101", "1110", "1111" };

    private static String hexStr = "0123456789ABCDEF";
    public static String myBinaryStrToHexString(String binaryStr) {
        String hex = "";
        String f4 = binaryStr.substring(0, 4);
        String b4 = binaryStr.substring(4, 8);
        for (int i = 0; i < binaryArray.length; i++) {
            if (f4.equals(binaryArray[i]))
                hex += hexStr.substring(i, i + 1);
        }
        for (int i = 0; i < binaryArray.length; i++) {
            if (b4.equals(binaryArray[i]))
                hex += hexStr.substring(i, i + 1);
        }

        return hex;
    }

    public static boolean printadvance(Context context)
    {
        pf = new PrintFormatNew(context);
        if(M.retriveVal(M.key_bill_width, context)==null){
            M.saveVal(M.key_bill_width,PrintFormat.normalsize+"", context);
        }
        pf.setPaperSize(M.retriveVal(M.key_bill_width,context));
        if(PrintFormatNew.selSize== PrintFormat.normalsize){
            isnormal = true;
        }
        if(getReceipttype().equals("2")){
            return createOtherReceiptData(context);
        }else if(getReceipttype().equals("3")){
            return endDayRecipt(context);
        }else if(getReceipttype().equals("4")){
            return stockRecipt(context);
        }else {
            return createTestReceiptData(context);
        }
    }

    public static void flush(){
        PrintActivityNew.isprinting = false;
//        try {
//
//            mmOutputStream.flush();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }


    }


    public static boolean createTestReceiptData(Context context) {

        pf=new PrintFormatNew(context);
        try {
            initprint();
            File myDir = new File(AppConst.folder_dir + "logo.png");
//            if (myDir.exists()) {
//                Bitmap myBitmap = BitmapFactory.decodeFile(myDir.getAbsolutePath());
//                mmOutputStream.write(PrinterCommands.ESC_ALIGN_CENTER);
//                mmOutputStream.write(PrinterCommands.SELECT_BIT_IMAGE_MODE);
//                printPhoto(myBitmap);
//                printNewLine();
//            }else{
            printCustom(M.getBrandName(context)+"\n",2,1);
//            }

            int size = 20;
            boolean isbold = false;

            printText(pf.padLine2(M.getBrandName(context),""),25,true);
            printText(pf.padLine2(M.getRestAddress(context),""), size, isbold);
            printText(pf.padLine2("Phone: "+ M.getRestPhoneNumber(context),""), size, isbold);
            printText(pf.padLine2("GST# "+ M.getGST(context),""), size, isbold);
            GlobalsNew.printline();
            printText(pf.padLine2("Order By : "+ M.getWaitername(context),"")+"\t", size, isbold);
//            textData.append("\n");
            printText(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"", size, isbold);
            printText(pf.padLine2("ORDER # 1025555555555",""), size, isbold);
            GlobalsNew.printline();
            printText(pf.padLine2("TOKEN # "+203,"") +"", size, isbold);

            GlobalsNew.printline();

            printText(pf.padLine2("طلب تعليق",""), size, isbold);
            printText(pf.padLine2("need spicy",""), size, isbold);

            GlobalsNew.printline();

            String qty = "1";
            printText(padLine(qty + "    Amrican Corn", 120 + "", 40, " ") + "", size, isbold);
            printText(padLine(qty + "    Burger", 120 + "", 40, " ") + "", size, isbold);
            printText(padLine(qty + "    Pizza", 120 + "", 40, " ") + "", size, isbold);
            printText(padLine(qty + "    بيتزا    ", 120 + "", 40, " ") + "", size, isbold);
            printText(padLine(qty + "    برغر    ", 120 + "", 40, " ") + "", size, isbold);

            printText(padLine(qty + "    الذرة الأمريكية    ", 120 + "", 40, " ") + "", size, isbold);

            GlobalsNew.printline();


            printText(pf.padLine2("SUBTOTAL",480+""), size, isbold);
            printText(pf.padLine2("CGST @"+6+"%",29+""), size, isbold);
            printText(pf.padLine2("SGST @"+6+"%",29+""), size, isbold);
            printNewLine();
            if (PrintFormatNew.selSize == PrintFormat.normalsize) {
                printText(pf.padLine3(context.getString(R.string.txt_total), pf.setFormat("538")) , bigsize, true);
            } else {
                printText(pf.padLine3(context.getString(R.string.txt_total), pf.setFormat("538")) , medsize, true);
            }
            printNewLine();
            printText(pf.padLine2("CASH","600"), size, isbold);
            printText(pf.padLine2("CHANGE","62"), size, isbold);

            GlobalsNew.printline();

            printText(pf.padLine2("Thank you "+"Visit Again ",""), size, isbold);

            printNewLine();
            printNewLine();

            cutpaper();
            cashdrawer();
            flush();

        }
        catch (Exception e) {
            Log.d("Exception printer----", e.getMessage()+"");
            return false;
        }



        return true;
    }

    public static void initprint(){
        try {
            mmOutputStream.write(com.royalpos.waiter.universalprinter.PrinterCommands.INIT);
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public static void printCustom(String msg, int size, int align) {
        //Print config "mode"
        byte[] cc = new byte[]{0x1B,0x21,0x03};  // 0- normal size text
        //byte[] cc1 = new byte[]{0x1B,0x21,0x00};  // 0- normal size text
        byte[] bb = new byte[]{0x1B,0x21,0x08};  // 1- only bold text
        byte[] bb2 = new byte[]{0x1B,0x21,0x20}; // 2- bold with medium text
        byte[] bb3 = new byte[]{0x1B,0x21,0x10}; // 3- bold with large text
        try {
            switch (size){
                case 0:
                    mmOutputStream.write(cc);
                    break;
                case 1:
                    mmOutputStream.write(bb);
                    break;
                case 2:
                    mmOutputStream.write(bb2);
                    break;
                case 3:
                    mmOutputStream.write(bb3);
                    break;
            }

            switch (align){
                case 0:
                    //left align
                    mmOutputStream.write(PrinterCommands.ESC_ALIGN_LEFT);
                    break;
                case 1:
                    //center align
                    mmOutputStream.write(PrinterCommands.ESC_ALIGN_CENTER);
                    break;
                case 2:
                    //right align
                    mmOutputStream.write(PrinterCommands.ESC_ALIGN_RIGHT);
                    break;
            }
            mmOutputStream.write(msg.getBytes());
            mmOutputStream.write(PrinterCommands.LF);
            //mmOutputStream.write(cc);
            //printNewLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //print photo
    public static void printPhoto(Bitmap logo) {
        try {

            if(logo!=null){
                byte[] command = Utils.decodeBitmap(logo);
//                mmOutputStream.write(PrinterCommands.ESC_ALIGN_CENTER);
                printText(command);
            }else{
                Log.e("Print Photo error", "the file isn't exists");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("PrintTools", "the file isn't exists");
        }
    }

    //print new line
    public static void printNewLine() {
        try {
            mmOutputStream.write(PrinterCommands.FEED_LINE);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void cutpaper() {
        try {
            mmOutputStream.write(PrinterCommands.FEED_PAPER_AND_CUT);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void cashdrawer() {
        try {
            mmOutputStream.write(PrinterCommands.CASH_DRAWER);
            mmOutputStream.write(PrinterCommands.CASH_DRAWER1);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void resetPrint() {
        try{
            mmOutputStream.write(PrinterCommands.ESC_FONT_COLOR_DEFAULT);
            mmOutputStream.write(PrinterCommands.FS_FONT_ALIGN);
            mmOutputStream.write(PrinterCommands.ESC_ALIGN_LEFT);
            mmOutputStream.write(PrinterCommands.ESC_CANCEL_BOLD);
            mmOutputStream.write(PrinterCommands.LF);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //print text
    public static void printText(String msg, int size, boolean isbold) {
//        try {
        String[] txt=msg.split("\n");
        ReceiptBitmap receiptBitmap = new ReceiptBitmap().getInstance();
        int l=(txt.length+1)*25;
        int width = 384;
        if(isnormal) {
            width = 584;
            receiptBitmap.init(500,l);
        }else{
            receiptBitmap.init(384,l);
        }
        Log.d("here----", "size---"+l );
        for(String t:txt)
            receiptBitmap.drawadvCenterText(t, size, isbold);

        Bitmap canvasbitmap = receiptBitmap.getReceiptBitmap();


        Bitmap croppedBmp = Bitmap.createBitmap(canvasbitmap, 0, 0, canvasbitmap.getWidth(), canvasbitmap.getHeight());

        printPhoto(croppedBmp);


    }


    static public Bitmap createBitmapFromText(String printText, int textSize, int printWidth, Typeface typeface) {
        Paint paint = new Paint();
        Bitmap bitmap;
        Canvas canvas;

        paint.setTextSize(textSize);
        paint.setTypeface(typeface);

        paint.getTextBounds(printText, 0, printText.length(), new Rect());

        TextPaint textPaint = new TextPaint(paint);
        StaticLayout staticLayout = new StaticLayout(printText, textPaint,
                printWidth, Layout.Alignment.ALIGN_CENTER,
                1, 0, false);

        // Create bitmap
        bitmap = Bitmap.createBitmap(staticLayout.getWidth(), staticLayout.getHeight(), Bitmap.Config.ARGB_8888);

        // Create canvas
        canvas = new Canvas(bitmap);
        canvas.drawColor(Color.WHITE);
        canvas.translate(0, 0);
        staticLayout.draw(canvas);
        printPhoto(bitmap);
        return bitmap;
    }

    public static void printTextalignleft(String msg, int size, boolean isbold) {

        String[] txt=msg.split("\n");
        ReceiptBitmap receiptBitmap = new ReceiptBitmap().getInstance();
        int l=(txt.length+1)*20;

        if(isnormal) {
            receiptBitmap.init(500,l);
        }else{
            receiptBitmap.init(384,l);
        }
        for(String t:txt)
            receiptBitmap.drawLeftText(t,size, isbold);
//        receiptBitmap.drawLeftText(msg, size, isbold);

        Bitmap canvasbitmap = receiptBitmap.getReceiptBitmap();


        Bitmap croppedBmp = Bitmap.createBitmap(canvasbitmap, 0, 0, canvasbitmap.getWidth(), canvasbitmap.getHeight());

        printPhoto(croppedBmp);


    }

    public static void printline() {
//        printNewLine();
        ReceiptBitmap receiptBitmap = new ReceiptBitmap().getInstance();
        int    l = 5;    if(isnormal) {
            receiptBitmap.init(500,l);
            receiptBitmap.drawNewLine(450);
        }else{
            receiptBitmap.init(384,l);
            receiptBitmap.drawNewLine(350);
        }    Bitmap canvasbitmap = receiptBitmap.getReceiptBitmap();
        printPhoto(canvasbitmap);
    }
    //print byte[]
    public static void printText(byte[] msg) {
        try {
            // Print normal text
            mmOutputStream.write(msg);
//            printNewLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    public static boolean createOtherReceiptData(Context context) {

        arabicfont = AppConst.font_arabic(context);
        PrintFormat pf1=new PrintFormat(context);
        pf1.setPaperSize(M.retriveVal(M.key_bill_width,context));
        jsonObject = getJsonObject();
        try {
            String txt=jsonObject.getString("data");
            String[] data=txt.split("\n");
            File myDir = new File(AppConst.folder_dir + "logo.png");
//            if (myDir.exists() && M.getCustomPrint(M.key_logo,context)) {
//                Bitmap myBitmap = BitmapFactory.decodeFile(myDir.getAbsolutePath());
//                mmOutputStream.write(com.royalpos.waiter.print.newbluetooth.PrinterCommands.ESC_ALIGN_CENTER);
//
//                printPhoto(myBitmap);
//                printNewLine();
//            }
            for(String s:data){
                String s1=prepareDataToPrint1(s);
                if(s1.trim().length()>0) {
                    if (pf1.divider().equals(s))
                        printline();
                    else if (s.contains("$cutt")) {
                        printNewLine();
                        cutpaper();
                        printNewLine();

                    }
                    else if (s.contains("$big")) {
                        if (PrintFormat.setSize == PrintFormat.normalsize) {
                            printText(s1, bigsize, true);
                        } else {
                            printText(s1, medsize, true);
                        }
                    } else
                        printText(s1, size, isbold);
                }
            }
            printNewLine();
            cutpaper();
            cashdrawer();
            flush();

            EventBus.getDefault().post("Kitchenprint");
        } catch (Exception e) {
            Log.d(TAG, "error:" + e.getMessage());
            return false;
        }

        return true;
    }

    public static boolean endDayRecipt(Context context) {
        jsonObject = getJsonObject();
        pf=new PrintFormatNew(context);
        pf.setPaperSize(M.retriveVal(M.key_bill_width,context));
        String tm=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        try {
            String otime="",ctime="",obal="";
            if(jsonObject.has("op_time"))
                otime=jsonObject.getString("op_time");
            if(jsonObject.has("cl_time"))
                otime=jsonObject.getString("cl_time");
            if(jsonObject.has("op_bal"))
                obal=jsonObject.getString("op_bal");
            initprint();
            if (PrintFormatNew.selSize == PrintFormat.normalsize) {
                printText(pf.padLine2(M.getBrandName(context), ""), bigsize, true);
            } else {
                printText(pf.padLine2(M.getBrandName(context), ""), medsize, true);
            }

            printText(pf.padLine2(M.getRestName(context), "") , size, isbold);
            if (PrintFormatNew.selSize == PrintFormat.normalsize)
                printText(pf.padLine2(M.getWaitername(context), tm), size, isbold);
            else {
                printText(pf.padLine2(M.getWaitername(context), ""), size, isbold);
                printText(pf.padLine2(tm, "") , size, isbold);
                printline();
            }
            printline();
            if(jsonObject!=null && jsonObject.has("item_data")){
                JSONArray ja=jsonObject.getJSONArray("item_data");
                for(int i=0;i<ja.length();i++){
                    JSONObject ja1=ja.getJSONObject(i);
                    printText(pf.padLine1(ja1.getString("dishnm"),ja1.getString("qty"),"",pf.setFormat(ja1.getString("amt"))), size, isbold);
                }
            }else if(jsonObject!=null) {
                printText(pf.padLine2(context.getString(R.string.opening_time) + " " + otime, "") , size, isbold);
                if (ctime != null && ctime.trim().length() > 0)
                    printText(pf.padLine2(context.getString(R.string.closing_time) + " " + ctime, "") , size, isbold);
                printText(pf.padLine2(context.getString(R.string.txt_opening_balance), pf.setFormat(obal)) , size, isbold);
                if(jsonObject.has("pay_sales")){
                    JSONArray jp=jsonObject.getJSONArray("pay_sales");
                    for(int i=0;i<jp.length();i++){
                        JSONObject jp1=jp.getJSONObject(i);
                        printText(pf.padLine2(jp1.getString("type"), pf.setFormat(jp1.getString("sales"))) , size, isbold);
                    }
                }
                printText(pf.padLine2(context.getString(R.string.total_sales), pf.setFormat(jsonObject.getString("sales"))) , size, isbold);
                printText(pf.padLine2(context.getString(R.string.txt_cash_in), pf.setFormat(jsonObject.getString("cashin"))), size, isbold);
                printText(pf.padLine2(context.getString(R.string.txt_cash_out), pf.setFormat(jsonObject.getString("cashout"))) , size, isbold);
                printText(pf.padLine2(context.getString(R.string.expected_cash), pf.setFormat(jsonObject.getString("expected"))) , size, isbold);
                if (jsonObject.has("actual")) {
                    printText(pf.padLine2(context.getString(R.string.actual_cash), pf.setFormat(jsonObject.getString("actual"))) , size, isbold);
                }

                if (jsonObject.has("diffamt")) {
                    printText(pf.padLine2(context.getString(R.string.txt_difference), pf.setFormat(jsonObject.getString("diffamt"))), size, isbold);
                }
            }
            printline();
            printText(pf.padLine2(context.getString(R.string.txt_powered_by) + " " + M.getfooterphone(context), ""), size, isbold);
            printNewLine();
            printNewLine();
            cutpaper();
            flush();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static boolean stockRecipt(Context context) {
        jsonObject = getJsonObject();
        pf=new PrintFormatNew(context);
        pf.setPaperSize(M.retriveVal(M.key_bill_width,context));
        try {
            initprint();
            if (PrintFormatNew.selSize == PrintFormat.normalsize) {
                printText(pf.padLine2(M.getBrandName(context), ""), bigsize, true);
            } else {
                printText(pf.padLine2(M.getBrandName(context), ""), medsize, true);
            }

            printText(pf.padLine2(M.getRestName(context), "") , size, isbold);
            printText(pf.padLine2(M.getWaitername(context), ""), size, isbold);
            printline();

            if(jsonObject!=null && jsonObject.has("stock_items")){
                JSONArray ja=jsonObject.getJSONArray("stock_items");
                for(int i=0;i<ja.length();i++){
                    JSONObject ja1=ja.getJSONObject(i);
                    printText(pf.padLine2(ja1.getString("dishnm"),ja1.getString("stock")), size, isbold);
                }
            }
            printline();
            printText(pf.padLine2(context.getString(R.string.txt_powered_by) + " " + M.getfooterphone(context), ""), size, isbold);
            printNewLine();
            printNewLine();
            cutpaper();
            flush();
        }catch (JSONException e){

        }
        return true;
    }

    public static final byte[] PRINTER_SELECT_BIT_IMAGE_MODE = {0x1B, 0x2A, 33};
    public static byte ESC_CHAR = 0x1B;
    public static byte[] PRINTER_SET_LINE_SPACE_24 = new byte[]{ESC_CHAR, 0x33, 24};
    public static byte[] PRINTER_DARKER_PRINTING = {0x1B, 0x37, 11, 0x7F, 50};

    public static void printImage(Bitmap bitmap) {

        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        writeToPrinterBuffer( PRINTER_DARKER_PRINTING);
        final byte[] controlByte = {(byte) (0x00ff & width), (byte) ((0xff00 & width) >> 8)};
        int[] pixels = new int[width * height];

        bitmap.getPixels(pixels, 0, width, 0, 0, width, height);

        final int BAND_HEIGHT = 24;

        // Bands of pixels are sent that are 8 pixels high.  Iterate through bitmap
        // 24 rows of pixels at a time, capturing bytes representing vertical slices 1 pixel wide.
        // Each bit indicates if the pixel at that position in the slice should be dark or not.
        for (int row = 0; row < height; row += BAND_HEIGHT) {

            writeToPrinterBuffer( PRINTER_SET_LINE_SPACE_24);

            // Need to send these two sets of bytes at the beginning of each row.
            writeToPrinterBuffer( PRINTER_SELECT_BIT_IMAGE_MODE);
            writeToPrinterBuffer( controlByte);

            // Columns, unlike rows, are one at a time.
            for (int col = 0; col < width; col++) {

                byte[] bandBytes = {0x0, 0x0, 0x0};

                // Ugh, the nesting of forloops.  For each starting row/col position, evaluate
                // each pixel in a column, or "band", 24 pixels high.  Convert into 3 bytes.
                for (int rowOffset = 0; rowOffset < 8; rowOffset++) {

                    // Because the printer only maintains correct height/width ratio
                    // at the highest density, where it takes 24 bit-deep slices, process
                    // a 24-bit-deep slice as 3 bytes.
                    int[] pixelSlice = new int[3];
                    int pixel2Row = row + rowOffset + 8;
                    int pixel3Row = row + rowOffset + 16;

                    // If we go past the bottom of the image, just send white pixels so the printer
                    // doesn't do anything.  Everything still needs to be sent in sets of 3 rows.
                    pixelSlice[0] = bitmap.getPixel(col, row + rowOffset);
                    pixelSlice[1] = (pixel2Row >= bitmap.getHeight()) ?
                            Color.WHITE : bitmap.getPixel(col, pixel2Row);
                    pixelSlice[2] = (pixel3Row >= bitmap.getHeight()) ?
                            Color.WHITE : bitmap.getPixel(col, pixel3Row);

                    boolean[] isDark = {pixelSlice[0] == Color.BLACK,
                            pixelSlice[1] == Color.BLACK,
                            pixelSlice[2] == Color.BLACK};

                    // Towing that fine line between "should I forloop or not".  This will only
                    // ever be 3 elements deep.
                    if (isDark[0]) bandBytes[0] |= 1 << (7 - rowOffset);
                    if (isDark[1]) bandBytes[1] |= 1 << (7 - rowOffset);
                    if (isDark[2]) bandBytes[2] |= 1 << (7 - rowOffset);
                }
                writeToPrinterBuffer(bandBytes);
            }

        }
    }

    static  void writeToPrinterBuffer(byte[] command) {
        try {
            mmOutputStream.write(command);
        } catch (IOException e) {
            Log.d(TAG, "IO Exception while writing printer data to buffer.", e);
        }
    }




    public static byte[] stringToBytesASCIIOK(String str) {
        String TAG = "posprinterdriver_AsyncNetPrint";

        byte[] b = new byte[0];
        try {
            b = str.getBytes("US-ASCII");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < b.length; i++) {

            b[i] = (byte) str.charAt(i);
            Log.i("", "                CAdena encontrada=" + i + " ->parte1=" + str.charAt(i) + " ->parte1=" + (int) str.charAt(i));

        }

        return b;

    }



}

