package com.royalpos.waiter.print;

import android.app.Activity;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.royalpos.waiter.R;
import com.royalpos.waiter.database.DBPrinter;
import com.royalpos.waiter.helper.MemoryCache;
import com.royalpos.waiter.model.KitchenPrinterPojo;
import com.royalpos.waiter.print.netprint.AsyncNetPrint;
import com.royalpos.waiter.print.netprint.AsyncPrintCallBack;
import com.royalpos.waiter.print.newbluetooth.PrintActivityNew;
import com.royalpos.waiter.universalprinter.KitchenGlobals;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.UUID;

import asyncProcess.AsyncResponse;
import asyncProcess.AsyncUrlGet;


public class KitchenPrintActivity extends Activity implements AsyncPrintCallBack, AsyncResponse {
    //private Handler mHandler = new Handler(Looper.getMainLooper());

    private static String internalClassName = "KitchenPrintActivity";// * Tag used on log messages.
    private static AsyncNetPrint asyncnetprint = null;      //Used to print in net printer
    private static AsyncPrintCallBack delegatePrintServices = null;
    private static AsyncResponse delegateAsyncResponse = null;
    private static final int REQUEST_ENABLE_BT = 1;
    private boolean internal = false;
    private boolean fromWeb = false;
    String lastIdFromServer = "0";
    private Activity activity = null;


    String dataToPrint = "";
    String internalInfo = "";

    SharedPreferences sharedPrefs = null;

    //USB DATA
    private static final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";
    PendingIntent mPermissionIntent;

    UsbInterface usbInterface;
    UsbDeviceConnection usbDeviceConnection;
    UsbDevice usbDeviceFound = null;
    UsbInterface usbInterfaceFound = null;
    UsbEndpoint usbEndPointIn = null;
    UsbEndpoint usbEndPointOut = null;


    int directPrinterDeviceType = 0;
    Boolean iscategory=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(internalClassName, "onCreate");
        if(getResources().getBoolean(R.bool.portrait_only)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        }
        delegatePrintServices = this;
        delegateAsyncResponse = this;
        activity = this;
        requestWindowFeature(Window.FEATURE_NO_TITLE);                                                                      // * Start with no title
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);  // * Start with full screen page
        setContentView(R.layout.print_layout);                      // * Start with main_layout
        MemoryCache memoryCache = new MemoryCache();

        if(PrintActivityNew.isprinting){
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    setContentView(R.layout.print_layout);
                    init();

                }
            }, 1000);
        }else{
            setContentView(R.layout.print_layout);
            init();
        }

    }

    void init(){
        Intent intent = getIntent();// Get intent, action and MIME type

        if(intent.getStringExtra("printerid")!=null){
            iscategory=true;
            String pid=intent.getStringExtra("printerid");
            DBPrinter dbp=new DBPrinter(KitchenPrintActivity.this);
            KitchenPrinterPojo kp=dbp.getPrintersData(pid);
            if(kp!=null){
                KitchenGlobals.usbDeviceID = KitchenGlobals.mathIntegerFromString(kp.getUsbDeviceID(), 0);
                KitchenGlobals.usbVendorID = KitchenGlobals.mathIntegerFromString(kp.getUsbVendorID(), 0);
                KitchenGlobals.usbProductID = KitchenGlobals.mathIntegerFromString(kp.getUsbProductID(), 0);

                if(kp.getLink_code()==null)
                    KitchenGlobals.link_code="";
                else
                    KitchenGlobals.link_code = kp.getLink_code();
                if(kp.getPicturePath()==null)
                    KitchenGlobals.picturePath="";
                else
                    KitchenGlobals.picturePath = kp.getPicturePath();
                if(kp.getBlueToothDeviceAdress()==null)
                    KitchenGlobals.blueToothDeviceAdress="";
                else
                    KitchenGlobals.blueToothDeviceAdress =kp.getBlueToothDeviceAdress();

                KitchenGlobals.deviceType = KitchenGlobals.mathIntegerFromString(kp.getDeviceType(), 0);
                //Ethernet
                if(kp.getDeviceIP()==null)
                    KitchenGlobals.deviceIP="";
                else
                    KitchenGlobals.deviceIP = kp.getDeviceIP();
                KitchenGlobals.devicePort = KitchenGlobals.mathIntegerFromString(kp.getDevicePort(), 9100);
                if(kp.getLogoProcesed()==null)
                    KitchenGlobals.logoProcesed="";
                else
                    KitchenGlobals.logoProcesed =kp.getLogoProcesed();
            }
        }else {
            iscategory=false;
            sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
            KitchenGlobals.loadPreferences(sharedPrefs);
        }

        // register the broadcast receiver
        mPermissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);
        IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
        registerReceiver(mUsbDeviceReceiver, new IntentFilter(UsbManager.ACTION_USB_DEVICE_ATTACHED));
        registerReceiver(mUsbDeviceReceiver, new IntentFilter(UsbManager.ACTION_USB_DEVICE_DETACHED));


        internal = false;
        if (getIntent().hasExtra("internal")) {
            internal = true;
        }
        fromWeb = false;
        if (getIntent().hasExtra("fromWeb")) {
            fromWeb = true;
        }

        //Obtain data to print
        dataToPrint = "";
        if (getIntent().hasExtra(Intent.EXTRA_TEXT)) {
            Log.i(internalClassName, "Print from Intent");
            dataToPrint = getIntent().getStringExtra(Intent.EXTRA_TEXT);
            Log.i(internalClassName, "dataToPrint from intent=" + dataToPrint);


        } else {

            Log.i(internalClassName, "Print from web ");
            Uri data = getIntent().getData();
            try {
                //dataToPrint = URLDecoder.decode(data.toString().substring(28), "UTF-8");
                dataToPrint = URLDecoder.decode(data.toString(), "UTF-8");//
            } catch (UnsupportedEncodingException e) {
                Log.i(internalClassName, "Received  error on decode url : " + dataToPrint);
                dataToPrint = data.toString().substring(28);
                e.printStackTrace();
            }
        }

        Log.d("KitchenPrintActivity","data:"+dataToPrint);
        directPrinterDeviceType = 0;
        if (intent.hasExtra("printer_type_id")) {
            directPrinterDeviceType = KitchenGlobals.mathIntegerFromString(intent.getStringExtra("printer_type_id").toString(), 0);
            Log.i(internalClassName, "directPrinterDeviceType" + directPrinterDeviceType);

        } else {
            if (dataToPrint.contains("$printer_type_id")) {
                Log.i(internalClassName, "$printer_type_id FOUND");
                directPrinterDeviceType = Integer.valueOf(KitchenGlobals.getVble(dataToPrint, "printer_type_id", "0"));

            }
        }
        //select type to print
        if (directPrinterDeviceType == 0) {
            //no special printer selected, get default configured printer
            directPrinterDeviceType = KitchenGlobals.deviceType;
            KitchenGlobals.tmpDeviceIP = KitchenGlobals.deviceIP;
            KitchenGlobals.tmpDevicePort = KitchenGlobals.devicePort;
            KitchenGlobals.tmpUsbVendorID = KitchenGlobals.usbVendorID;
            KitchenGlobals.tmpUsbProductID = KitchenGlobals.usbProductID;
            KitchenGlobals.tmpUsbDeviceID = KitchenGlobals.usbDeviceID;
            KitchenGlobals.tmpBlueToothDeviceAdress = KitchenGlobals.blueToothDeviceAdress;


        } else {
            //special printer selected, ->get config printer data to print

            if (dataToPrint.contains("$printer_type_id")) {
                KitchenGlobals.tmpDeviceIP = KitchenGlobals.getVble(dataToPrint, "printer_ip", "192.168.0.150");
                KitchenGlobals.tmpDevicePort = Integer.valueOf(KitchenGlobals.getVble(dataToPrint, "printer_port", "9100"));

                KitchenGlobals.tmpBlueToothDeviceAdress = KitchenGlobals.getVble(dataToPrint, "printer_bt_adress", "0");

                KitchenGlobals.tmpUsbVendorID = Integer.valueOf(KitchenGlobals.getVble(dataToPrint, "printer_usb_vendor_id", "0"));
                KitchenGlobals.tmpUsbProductID = Integer.valueOf(KitchenGlobals.getVble(dataToPrint, "printer_usb_product_id", "0"));
                KitchenGlobals.tmpUsbDeviceID = Integer.valueOf(KitchenGlobals.getVble(dataToPrint, "printer_usb_device_id", "0"));
                //clear config data in data to print
                dataToPrint = dataToPrint.replace("$printer_type_id=" + String.valueOf(directPrinterDeviceType) + "$", "");
                dataToPrint = dataToPrint.replace("$printer_ip=" + String.valueOf(KitchenGlobals.tmpDeviceIP) + "$", "");
                dataToPrint = dataToPrint.replace("$printer_port=" + String.valueOf(KitchenGlobals.tmpDevicePort) + "$", "");
                dataToPrint = dataToPrint.replace("$printer_bt_adress=" + String.valueOf(KitchenGlobals.tmpBlueToothDeviceAdress) + "$", "");
                dataToPrint = dataToPrint.replace("$printer_usb_vendor_id=" + String.valueOf(KitchenGlobals.tmpUsbVendorID) + "$", "");
                dataToPrint = dataToPrint.replace("$printer_usb_product_id=" + String.valueOf(KitchenGlobals.tmpUsbProductID) + "$", "");
                dataToPrint = dataToPrint.replace("$printer_usb_device_id=" + String.valueOf(KitchenGlobals.tmpUsbDeviceID) + "$", "");


            } else {
                if (getIntent().hasExtra("printer_ip"))
                    KitchenGlobals.tmpDeviceIP = intent.getStringExtra("printer_ip").toString();
                if (getIntent().hasExtra("printer_port"))
                    KitchenGlobals.tmpDevicePort = KitchenGlobals.mathIntegerFromString(intent.getStringExtra("printer_port").toString(), 9100);
                if (getIntent().hasExtra("printer_bt_adress"))
                    KitchenGlobals.tmpBlueToothDeviceAdress = intent.getStringExtra("printer_bt_adress").toString();
                if (getIntent().hasExtra("printer_usb_vendor_id"))
                    KitchenGlobals.tmpUsbVendorID = KitchenGlobals.mathIntegerFromString(intent.getStringExtra("printer_usb_vendor_id").toString(), 9100);
                if (getIntent().hasExtra("printer_usb_product_id"))
                    KitchenGlobals.tmpUsbProductID = KitchenGlobals.mathIntegerFromString(intent.getStringExtra("printer_usb_product_id").toString(), 9100);
                if (getIntent().hasExtra("printer_usb_device_id"))
                    KitchenGlobals.tmpUsbDeviceID = KitchenGlobals.mathIntegerFromString(intent.getStringExtra("printer_usb_device_id").toString(), 9100);

            }
        }
        if (KitchenGlobals.picturePath!=null && KitchenGlobals.picturePath.trim().length()>0) {
            if (KitchenGlobals.inAppBillingModeON) {
                if (KitchenGlobals.get_status_licence() == true) {
                    dataToPrint = KitchenGlobals.getImageData() + dataToPrint;
                } else {
                    dataToPrint = KitchenGlobals.getImageDataPosPrinterLogo(this.getResources()) + dataToPrint;
                }
            } else {
                dataToPrint = KitchenGlobals.getImageData() + dataToPrint;
            }
        }
        // Start config activity
        if (intent.hasExtra("config")) {
            loadCondigActivity();

        } else {
            if (fromWeb == true) {
                getPendingDataToPrintFromWeb("0");
            } else {
                //real sen data to printer
                if (tryToPrint()) {
                    finish();
                }
            }
        }

        try {

            // we are goin to have three buttons for specific functions
            Button sendButton = (Button) findViewById(R.id.send);
            Button closeButton = (Button) findViewById(R.id.close);
            Button configButton = (Button) findViewById(R.id.config);
            if (internal) {
                configButton.setVisibility(View.GONE);

            }
            // send data typed by the user to be printed
            configButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    loadCondigActivity();

                }
            });


            // send data typed by the user to be printed
            sendButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (tryToPrint()) {
                        finish();
                    }

                }
            });

            // close bluetooth connection
            closeButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    finish();

                }
            });

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadCondigActivity() {
        Intent sendIntent = new Intent(getApplicationContext(), PrintMainActivity.class);
        sendIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getApplicationContext().startActivity(sendIntent);
    }

    //function to print in device
    private boolean tryToPrint() {
        Log.i(internalClassName, "tryToPrint");
        Log.i(internalClassName, "directPrinterDeviceType=" + String.valueOf(directPrinterDeviceType));
        Log.e(internalClassName, "DATA:" + dataToPrint);

        boolean success = false;
        if (directPrinterDeviceType == 0) {
            //no configured default device...
        }
        if (directPrinterDeviceType == 1)  //net printer configured
        {
            sendAsyncDataToPrinter(KitchenGlobals.tmpDeviceIP, KitchenGlobals.tmpDevicePort, dataToPrint);
            dataToPrint = "";//clera actual data to print
        }
        if (directPrinterDeviceType == 2)  //RS232
        {
            //not implemented yet
        }
        if (directPrinterDeviceType == 3)  //USB
        {
            String tmp_vendor_product = "";
            tmp_vendor_product = String.valueOf(KitchenGlobals.tmpUsbVendorID) + "_" + String.valueOf(KitchenGlobals.tmpUsbProductID);
            success = printInUsbDevice(KitchenGlobals.tmpUsbDeviceID, tmp_vendor_product);
        }
        if (directPrinterDeviceType == 4)  //BT
        {
            success = printInBTDevice(KitchenGlobals.tmpBlueToothDeviceAdress);
        }
        return success;
    }

    private boolean printInUsbDevice(int deviceNumer, String vendor_product) {
        Log.i(internalClassName, "printInUsbDevice");
        boolean success = false;
        if (connectToUsbDevice(deviceNumer, vendor_product)) {
            if (usbDeviceFound != null) {
                byte[] bytesOut = KitchenGlobals.stringToBytesASCII(KitchenGlobals.prepareDataToPrint(dataToPrint));//.getBytes();	//convert String to byte[]
                if (usbDeviceConnection != null && usbEndPointOut!=null) {
                    int usbResult = usbDeviceConnection.bulkTransfer(usbEndPointOut, bytesOut, bytesOut.length, 0);
                    Log.i(internalClassName, "printInUsbDevice: OK" + String.valueOf(usbResult));
                    success = true;
                }
            } else {
                Log.i(internalClassName, "printInUsbDevice: ERROR " + dataToPrint);
            }
        }
        return success;
    }


    private boolean printInBTDevice(String deviceAdrees) {
        Log.i(internalClassName, "printInBTDevice= ");

        boolean success = true;
        boolean closeDevice = false;
        boolean openDevice = false;


        if (KitchenGlobals.mmDevice == null) {
            Log.i(internalClassName, "       ->mmDevice=null");
            openDevice = true;
            success = false;
        } else {
            if (deviceAdrees.equals(KitchenGlobals.mmDevice.getAddress().toString())) {

            } else {
                Log.i(internalClassName, "->mmdevice= " + KitchenGlobals.mmDevice.getAddress().toString() + " ??=?? " + deviceAdrees);
                closeDevice = true;
                openDevice = true;
                success = false;
            }
        }


        if (closeDevice) {
            try {
                bluetoothCloseDevice();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        if (openDevice) {
            if (bluetoothFindDevice(deviceAdrees)) {
                if (bluetoothOpenDevice()) {
                    success = true;
                }
            }
        }
        if (success) {
            bluetoothSendData(dataToPrint);
        } else {
            Log.i("datos aintent=", "error3");

        }


        return success;
    }
    // XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX BT XXXXXXXXXXXXXXXXXXXXXXXXXXX

    boolean bluetoothFindDevice(String Printer_name) {
        boolean value = true;
        Log.i(internalClassName, "bluetoothFindDevice");
        try {
            KitchenGlobals.mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            if (KitchenGlobals.mBluetoothAdapter == null) {
                value = false;
                internalInfo = internalInfo + "No bluetooth adapter available";
                Log.e(internalClassName, "mBluetoothAdapter NULL");

            }

            if (!KitchenGlobals.mBluetoothAdapter.isEnabled()) {
                Intent enableBluetooth = new Intent(
                        BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBluetooth, 0);
            }

            Set<BluetoothDevice> pairedDevices = KitchenGlobals.mBluetoothAdapter
                    .getBondedDevices();
            if (pairedDevices.size() > 0) {
                for (BluetoothDevice device : pairedDevices) {

                    if (device.getAddress().equals(Printer_name)) {
                        KitchenGlobals.mmDevice = device;
                        Log.i(internalClassName, "ENCONTRADO = " + device.getName());
                        value = true;
                        break;
                    } else {
                        Log.i(internalClassName, "         = " + device.getName());

                    }
                }
            }

        } catch (NullPointerException e) {
            Log.e(internalClassName, "NullPointerException = " + e.toString());
            e.printStackTrace();
            value = false;


        } catch (Exception e) {
            Log.e(internalClassName, "Exception = " + e.toString());
            e.printStackTrace();
            value = false;

        }
        return value;

    }

    boolean bluetoothOpenDevice() {
        Log.i(internalClassName, "bluetoothOpenDevice");
        boolean value = true;
        try {
            // Standard SerialPortService ID
            UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
            KitchenGlobals.mmSocket = KitchenGlobals.mmDevice.createRfcommSocketToServiceRecord(uuid);
            KitchenGlobals.mmSocket.connect();
            KitchenGlobals.mmOutputStream = KitchenGlobals.mmSocket.getOutputStream();
            KitchenGlobals.mmInputStream = KitchenGlobals.mmSocket.getInputStream();
            Log.i(internalClassName, "OpenBluet OK = ");


            //myLabel.setText("Bluetooth Opened");
        } catch (NullPointerException e) {
            Log.e(internalClassName, "NullPointerException = " + e.toString());
            e.printStackTrace();
            value = false;
        } catch (Exception e) {
            Log.e(internalClassName, "Exception = " + e.toString());

            e.printStackTrace();
            value = false;
        }
        return value;
    }

    boolean bluetoothSendData(String dataToPrint) {
        Log.i(internalClassName, "bluetoothSendData");

        boolean value = true;

        try {
            String data = KitchenGlobals.prepareDataToPrint(dataToPrint);
            DataOutputStream out = new DataOutputStream(KitchenGlobals.mmOutputStream);
            byte[] dataToSend = KitchenGlobals.stringToBytesASCII(data);
            out.write(dataToSend, 0, dataToSend.length);
            out.flush();
            Log.i(internalClassName, "bluetoothSendData" + data);

        } catch (NullPointerException e) {
            Log.e(internalClassName, "NullPointerException: " + e.toString());
            e.printStackTrace();
            value = false;
        } catch (Exception e) {
            Log.i(internalClassName, "Exception: " + e.toString());
            KitchenGlobals.mmDevice = null;

            e.printStackTrace();
            value = false;
        }
        return value;
    }

    void bluetoothCloseDevice() throws IOException {
        Log.i(internalClassName, "bluetoothCloseDevice");
        try {
            KitchenGlobals.mmOutputStream.close();
            KitchenGlobals.mmInputStream.close();
            KitchenGlobals.mmSocket.close();
            Log.i(internalClassName, "Cerrado OK");
        } catch (NullPointerException e) {
            Log.e(internalClassName, "NullPointerException: " + e.toString());
            e.printStackTrace();
        } catch (Exception e) {
            Log.e(internalClassName, "Exception: " + e.toString());
            e.printStackTrace();
        }
    }

    // XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX BT XXXXXXXXXXXXXXXXXXXXXXXXXXX

    private void searchUsbDevice(int deviceNumer, String vendor_product) {
        Log.i(internalClassName, "searchUsbDevice");


        //		textInfo.setText("");
        //		textSearchedEndpoint.setText("");

        usbInterfaceFound = null;
        usbEndPointOut = null;
        usbEndPointIn = null;
        UsbManager manager = (UsbManager) getSystemService(Context.USB_SERVICE);
        HashMap<String, UsbDevice> deviceList = manager.getDeviceList();
        Iterator<UsbDevice> deviceIterator = deviceList.values().iterator();

        while (deviceIterator.hasNext()) {
            UsbDevice device = deviceIterator.next();
            Log.i("Buscndo =", "???=" + device.getDeviceId());
            if (deviceNumer > 0) {
                if (deviceNumer == device.getDeviceId()) {
                    Log.i("envontrados=", "OKKKKKK=" + device.getDeviceId());
                    usbDeviceFound = device;
                }
            } else {
                String tmp_vendor_product = "";
                tmp_vendor_product = String.valueOf(device.getVendorId()) + "_" + String.valueOf(device.getProductId());
                if (vendor_product.equals(tmp_vendor_product)) {
                    Log.i("envontrados=", "OKKKKKK=" + device.getDeviceId());
                    usbDeviceFound = device;
                }
            }
            //
        }

        if (usbDeviceFound == null) {
            Toast.makeText(KitchenPrintActivity.this, R.string.no_device,
                    Toast.LENGTH_LONG).show();
            //	textStatus.setText("device not found");
        } else {
            String s = usbDeviceFound.toString() + "\n" + "DeviceID: "
                    + usbDeviceFound.getDeviceId() + "\n" + "DeviceName: "
                    + usbDeviceFound.getDeviceName() + "\n" + "DeviceClass: "
                    + usbDeviceFound.getDeviceClass() + "\n" + "DeviceSubClass: "
                    + usbDeviceFound.getDeviceSubclass() + "\n" + "VendorID: "
                    + usbDeviceFound.getVendorId() + "\n" + "ProductID: "
                    + usbDeviceFound.getProductId() + "\n" + "InterfaceCount: "
                    + usbDeviceFound.getInterfaceCount();
            //	textInfo.setText(s);

            // Search for UsbInterface with Endpoint of USB_ENDPOINT_XFER_BULK,
            // and direction USB_DIR_OUT and USB_DIR_IN
            Log.i(internalClassName, "=" + s);
            for (int i = 0; i < usbDeviceFound.getInterfaceCount(); i++) {
                UsbInterface usbif = usbDeviceFound.getInterface(i);

                UsbEndpoint tOut = null;
                UsbEndpoint tIn = null;

                int tEndpointCnt = usbif.getEndpointCount();
                if (tEndpointCnt >= 2) {
                    for (int j = 0; j < tEndpointCnt; j++) {
                        if (usbif.getEndpoint(j).getType() == UsbConstants.USB_ENDPOINT_XFER_BULK) {
                            if (usbif.getEndpoint(j).getDirection() == UsbConstants.USB_DIR_OUT) {
                                tOut = usbif.getEndpoint(j);
                            } else if (usbif.getEndpoint(j).getDirection() == UsbConstants.USB_DIR_IN) {
                                tIn = usbif.getEndpoint(j);
                            }
                        }
                    }

                    if (tOut != null && tIn != null) {
                        // This interface have both USB_DIR_OUT
                        // and USB_DIR_IN of USB_ENDPOINT_XFER_BULK
                        usbInterfaceFound = usbif;
                        usbEndPointOut = tOut;
                        usbEndPointIn = tIn;
                    }
                }

            }

            if (usbInterfaceFound == null) {
                Log.i(internalClassName, "Error ");

                //	textSearchedEndpoint.setText("No suitable interface found!");
            } else {
                String data = "UsbInterface found: "
                        + usbInterfaceFound.toString() + "\n\n"
                        + "Endpoint OUT: " + usbEndPointOut.toString() + "\n\n"
                        + "Endpoint IN: " + usbEndPointIn.toString();

                Log.i(internalClassName, "Datos=" + data);

            }
        }
    }


    private boolean connectToUsbDevice(int deviceNumber, String vendorProduct) {
        Log.i(internalClassName, "connectToUsbDevice");

        boolean success = false;
        Log.i(internalClassName, "deviceNumber=" + String.valueOf(deviceNumber) + "vendorProduct=" + String.valueOf(vendorProduct));
        searchUsbDevice(deviceNumber, "");
        if (usbInterfaceFound != null) {
            success = setupUsbComm();
        } else {
            searchUsbDevice(0, vendorProduct);
            if (usbInterfaceFound != null) {
                success = setupUsbComm();
            }
        }
        return success;
    }

    private boolean setupUsbComm() {
        boolean success = false;
        Log.i(internalClassName, "setupUsbComm");
        // for more info, search SET_LINE_CODING and
        // SET_CONTROL_LINE_STATE in the document:
        // "Universal Serial Bus Class Definitions for Communication Devices"
        // at http://adf.ly/dppFt
        final int RQSID_SET_LINE_CODING = 0x20;
        final int RQSID_SET_CONTROL_LINE_STATE = 0x22;
        UsbManager manager = (UsbManager) getSystemService(Context.USB_SERVICE);
        Boolean permitToRead = manager.hasPermission(usbDeviceFound);
        Log.i(internalClassName, "setupUsbComm 1");
        //permitToRead=false;
        if (permitToRead) {
            success = true;
            usbDeviceConnection = manager.openDevice(usbDeviceFound);
            if (usbDeviceConnection != null) {
                usbDeviceConnection.claimInterface(usbInterfaceFound, true);
            }
        } else {
            manager.requestPermission(usbDeviceFound, mPermissionIntent);
            Toast.makeText(KitchenPrintActivity.this, getString(R.string.permission_to_read) + permitToRead,
                    Toast.LENGTH_LONG).show();
        }
        return success;
    }


    /*private void releaseUsb() {
        Log.i("envontrados=", "releaseUsb");

        Toast.makeText(PrintActivity.this, "releaseUsb()", Toast.LENGTH_LONG)
                .show();
        //textStatus.setText("releaseUsb()");

        if (usbDeviceConnection != null) {
            if (usbInterface != null) {
                usbDeviceConnection.releaseInterface(usbInterface);
                usbInterface = null;
            }
            usbDeviceConnection.close();
            usbDeviceConnection = null;
        }

        usbDeviceFound = null;
        usbInterfaceFound = null;
        usbEndPointIn = null;
        usbEndPointOut = null;
    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_BT) {

        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mUsbDeviceReceiver);
    }

    private final BroadcastReceiver mUsbDeviceReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i("BroadcastReceiver=", "onReceive");
        }
    };

    public static void sendAsyncDataToPrinter(String printerIP, int printerPort, String dataToPrint) {

        Log.e(internalClassName, "sendAsyncUrlData :" + printerIP);
        Log.e(internalClassName, "port             :" + printerPort);
        Log.e(internalClassName, "datos            :" + dataToPrint);

        if (asyncnetprint == null) {
            Log.e(internalClassName, "Start AsyncNetPrint:");
            asyncnetprint = new AsyncNetPrint();
        } else {
            Log.e(internalClassName, "AsyncNetPrint Actual Status :");
            Log.e(internalClassName, "        getInitialicing:" + asyncnetprint.getInitialicing());
            Log.e(internalClassName, "        getOperative   :" + asyncnetprint.getOperative());
            Log.e(internalClassName, "        getError       :" + asyncnetprint.getError());


        }
        asyncnetprint.printerCallBack = delegatePrintServices;
        asyncnetprint.initialice(dataToPrint, printerIP, printerPort);
    }
    @Override
    public void statusChange() {
    }

    @Override
    public void printErrorResult(String output) {
    }

    @Override
    public void printResult(String output) {
        boolean finalicenow = true;

        if (fromWeb == true) {
            if (!lastIdFromServer.equals("0")) {
                Looper.prepare();
                finalicenow = false;
                getPendingDataToPrintFromWeb(lastIdFromServer);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Looper.myLooper().quit();
                Looper.loop();
            }
        }
        //if (finalicenow == true) {
        finish();
        //}
    }

    private void getPendingDataToPrintFromWeb(String string_id) {
        Log.e(internalClassName, "getPendingDataToPrintFromWeb string_id=" + string_id);
        JSONObject jsonData = new JSONObject();
        try {
            jsonData.put("linkcode", KitchenGlobals.link_code);
            jsonData.put("id", string_id);//para borrar el anterior
            sendAsyncUrlData(jsonData, "getdata", KitchenGlobals.server_getData);
        } catch (JSONException e1) {
            e1.printStackTrace();
        }
    }

    //****************************************************************************
    //  SEND ASYNC DATA
    //****************************************************************************
    public void sendAsyncUrlData(JSONObject jsonData, String functionCmd, String urlPath) {
        Log.e(internalClassName, "sendAsyncUrlData :" + functionCmd + " url=" + KitchenGlobals.getServerUrl() + urlPath);
        //delegatePrintServices=this;
        AsyncUrlGet asyncgetUrlData = new AsyncUrlGet();
        asyncgetUrlData.initialice(delegateAsyncResponse, activity, KitchenGlobals.getServerUrl() + urlPath, jsonData);
        asyncgetUrlData.processName = functionCmd;
        asyncgetUrlData.execute();
    }


    @Override
    public void processFinish(String output) {
        Log.d(internalClassName, "processFinish= : " + output);
        JSONObject jsonObjectTotal;


        try {
            jsonObjectTotal = new JSONObject(output);
            String asyncProcess = jsonObjectTotal.getString("async");
            String processName = jsonObjectTotal.getString("process");
            String internalData = jsonObjectTotal.getString("data");

            Boolean asyncResult = jsonObjectTotal.getString("result").equals("ok");

            if (asyncResult) {

                if (asyncProcess.equals("geturl")) {
                    if (processName.equals("getdata")) {
                        JSONObject jsonInternalData = new JSONObject(internalData);
                        dataToPrint = jsonInternalData.getString("data");
                        lastIdFromServer = "0";
                        if (jsonInternalData.has("id")) {

                            lastIdFromServer = jsonInternalData.getString("id");

                        }
                        if (!dataToPrint.equals("")) {
                            dataToPrint = dataToPrint + "$intro$";
                            tryToPrint();
                        } else {
                            if (!lastIdFromServer.equals("0")) {
                                //lo mato..
                                getPendingDataToPrintFromWeb(lastIdFromServer);

                            }
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void processUpdate(int parseInt) {

    }

    @Override
    public void processStringCallBack(String stringText) {

    }
}
