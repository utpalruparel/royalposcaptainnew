package com.royalpos.waiter.print;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.royalpos.waiter.POServerService;
import com.royalpos.waiter.R;
import com.royalpos.waiter.database.DBDishes;
import com.royalpos.waiter.helper.AppConst;
import com.royalpos.waiter.helper.ConnectionDetector;
import com.royalpos.waiter.helper.MemoryCache;
import com.royalpos.waiter.helper.WifiHelper;
import com.royalpos.waiter.model.DishPojo;
import com.royalpos.waiter.model.M;
import com.royalpos.waiter.ui.MenuUI;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import androidx.core.app.ActivityCompat;
import de.greenrobot.event.EventBus;

public class PrinterSettings extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {

    LinearLayout btn_cash_printer, btn_kitchen_printer,llkitchen,llkitchencat, btncatKitchen, llpapersize,lltblrow,llcustomSC,
                lldinein,lljoin,lldownload;
    TextView txt_cash_pritner, txt_kitchen_pritner,txtpapersize,tvcustomSC,tv_ui_type,
    tvcols, tvserverstatus,tvs_conn,tvs_disconn;
    SwitchCompat swcash,swkitchen,swcatkitchen, swautoprint, swlsale, swlsalert,swcat,switem,swkot,swplaceorder,swqty,swdelitem,switemseparate,swtopspace;
    String TAG="PrinterSettings",selected = "",cd=null;
    Context context;
    ConnectionDetector connectionDetector;
    Boolean ischangecol=false;
    POServerService mService;
    Boolean iscat,isitem,isMainDineIn=false,mBound=false;
    int cnt=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_printer_settings);
        if(getResources().getBoolean(R.bool.portrait_only)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        }
        requestForSpecificPermission();
        MemoryCache  memoryCache = new MemoryCache();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        context = this;
        connectionDetector=new ConnectionDetector(context);
        if(getIntent().getAction()!=null && getIntent().getAction().equals("dinein"))
            isMainDineIn=true;
        else
            isMainDineIn=false;

        btn_cash_printer = (LinearLayout)findViewById(R.id.btn_cash_printer);
        llpapersize= (LinearLayout)findViewById(R.id.llpapersize);
        btn_kitchen_printer = (LinearLayout)findViewById(R.id.btn_kitchen_printer);
        btncatKitchen=(LinearLayout)findViewById(R.id.btn_cat_kitchen_printer);
        llkitchen=(LinearLayout)findViewById(R.id.llkitchen);
        llkitchencat=(LinearLayout)findViewById(R.id.llcatkitchen);
        lldinein=(LinearLayout)findViewById(R.id.lldinein);
        lldownload=(LinearLayout)findViewById(R.id.lldownload);
        txtpapersize = (TextView)findViewById(R.id.txtpapersize);
        txt_cash_pritner= (TextView) findViewById(R.id.txt_cash_pritner);
        txt_kitchen_pritner= (TextView) findViewById(R.id.txt_kitchen_pritner);
        swcash=(SwitchCompat)findViewById(R.id.swcash);
        swkitchen=(SwitchCompat)findViewById(R.id.swkitchen);
        swlsalert=(SwitchCompat)findViewById(R.id.swlsalert);
        swlsale =(SwitchCompat)findViewById(R.id.swlsale);
        swautoprint=(SwitchCompat)findViewById(R.id.swautoprint);
        swcatkitchen=(SwitchCompat)findViewById(R.id.swcatkitchen);
        swcat=(SwitchCompat)findViewById(R.id.swcat);
        switem=(SwitchCompat)findViewById(R.id.switem);
        swkot=(SwitchCompat)findViewById(R.id.swkot);
        swqty=(SwitchCompat)findViewById(R.id.swqty);
        swplaceorder=(SwitchCompat)findViewById(R.id.swplaceorder);
        swdelitem=(SwitchCompat)findViewById(R.id.swdelitem);
        switemseparate=(SwitchCompat)findViewById(R.id.switemseparate);
        swtopspace=(SwitchCompat)findViewById(R.id.swtopspace);
        lltblrow=(LinearLayout)findViewById(R.id.lltblrow);
        tvcols=(TextView)findViewById(R.id.tvcols);
        tv_ui_type=(TextView)findViewById(R.id.tvtype);
        tvcustomSC=(TextView)findViewById(R.id.tvcustomSC);
        tvcustomSC.setText(M.retriveVal(M.key_custom_service_charge,context));
        llcustomSC=(LinearLayout)findViewById(R.id.llcustomSC);
        llcustomSC.setOnClickListener(this);
        if(M.isServiceCharge(context))
            llcustomSC.setVisibility(View.VISIBLE);
        else
            llcustomSC.setVisibility(View.GONE);
        lldinein.setVisibility(View.VISIBLE);

        if(M.getPapersize(this)==25) {
            txtpapersize.setText(R.string.paper_width_58);
        }else{
            txtpapersize.setText(R.string.paper_width_80);
        }

            llkitchen.setVisibility(View.VISIBLE);
            llkitchencat.setVisibility(View.VISIBLE);

        if(M.getCashPrinterName(this).length()>0){
            if(M.isadvanceprint(M.key_bill,context))
                txt_cash_pritner.setText(M.getCashPrinterName(this)+" "+context.getString(R.string.txt_advance));
            else
                txt_cash_pritner.setText(""+ M.getCashPrinterName(this));
        }

        if(M.getKitchenPrinterName(this).length()>0){
            if(M.isadvanceprint(M.key_kitchen,context))
                txt_kitchen_pritner.setText(M.getKitchenPrinterName(this)+" "+context.getString(R.string.txt_advance));
            else
                txt_kitchen_pritner.setText(""+ M.getKitchenPrinterName(this));
        }

        btn_cash_printer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selected = "cash";
                Intent i = new Intent(PrinterSettings.this, PrintMainActivity.class);
                startActivityForResult(i, 101);
            }
        });

        btn_kitchen_printer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selected = "kitchen";
                Intent i = new Intent(PrinterSettings.this, KitchenPrintMainActivity.class);
                startActivityForResult(i, 102);
            }
        });

        btncatKitchen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent it=new Intent(context,KitchencategoryActivity.class);
                startActivity(it);
            }
        });

        if(M.isCashPrinter(context))
           swcash.setChecked(true);
        else
           swcash.setChecked(false);

        if(M.isKitchenPrinter(context))
            swkitchen.setChecked(true);
        else
            swkitchen.setChecked(false);

        if(M.isKitchenCatPrinter(context))
            swcatkitchen.setChecked(true);
        else
            swcatkitchen.setChecked(false);

        if(M.isLowStockalert(context))
            swlsalert.setChecked(true);
        else
            swlsalert.setChecked(false);


        if(M.isAutoPrint(context))
            swautoprint.setChecked(true);
        else
            swautoprint.setChecked(false);


        if(M.islowstockpunch(context))
            swlsale.setChecked(true);
        else
            swlsale.setChecked(false);

        swcash.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
               M.setCashPrinter(b,context);
            }
        });

        swkitchen.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                M.setKitchenPrinter(b,context);
                if(b){
                    swcatkitchen.setChecked(false);
                    M.setKitchenCatPrinter(false,context);
                }
            }
        });

        swcatkitchen.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                M.setKitchenCatPrinter(b,context);
                if(b){
                    swkitchen.setChecked(false);
                    M.setKitchenPrinter(false,context);
                }
            }
        });

        llpapersize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showdialog();
            }
        });

        if(M.getTableRow(context)!=null)
            tvcols.setText(M.getTableRow(context));

        lltblrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog pd = new Dialog(context);
                pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
                pd.setContentView(R.layout.dialog_inactive_table);
                pd.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                TextView tvtitle=(TextView)pd.findViewById(R.id.tvtitle);
                tvtitle.setText(getString(R.string.sel_tbl_col_cnt));
                ListView lv=(ListView)pd.findViewById(R.id.lvtable);
                final ArrayList<String> pnmlist=new ArrayList();
                pnmlist.add("1");pnmlist.add("2");pnmlist.add("3");
                CustomListAdapter tada=new CustomListAdapter(context,pnmlist);
                lv.setAdapter(tada);
                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        ischangecol=true;
                        M.setTableRow(pnmlist.get(i),context);
                        tvcols.setText(M.getTableRow(context));
                        pd.dismiss();
                    }
                });
                pd.show();
            }
        });


        swcat.setChecked(M.isCatAlpha(context));
        iscat=M.isCatAlpha(context);
        switem.setChecked(M.isItemAlpha(context));
        isitem=M.isItemAlpha(context);
        swkot.setChecked(M.isKOTAlert(context));
        swplaceorder.setChecked(M.isItemAlert(context));
        swqty.setChecked(M.isQtyDialog(context));
        swdelitem.setChecked(M.isCustomAllow(M.key_dinein_delitem,context));
        switemseparate.setChecked(M.isCustomAllow(M.key_dinein_separate_item,context));
        swtopspace.setChecked(M.isCustomAllow(M.key_top_space,context));

        swplaceorder.setOnCheckedChangeListener(this);
        swkot.setOnCheckedChangeListener(this);
        switem.setOnCheckedChangeListener(this);
        swcat.setOnCheckedChangeListener(this);
        swlsale.setOnCheckedChangeListener(this);
        swautoprint.setOnCheckedChangeListener(this);
        swlsalert.setOnCheckedChangeListener(this);
        swqty.setOnCheckedChangeListener(this);
        swdelitem.setOnCheckedChangeListener(this);
        switemseparate.setOnCheckedChangeListener(this);
        swtopspace.setOnCheckedChangeListener(this);

        lljoin=(LinearLayout)findViewById(R.id.lljoin);
        tvserverstatus=(TextView)findViewById(R.id.tvserverstatus);
        tvs_conn=(TextView)findViewById(R.id.tvconnect);
        tvs_disconn=(TextView)findViewById(R.id.tvdisconnect);

        registerReceiver(broadcastReceiver,new IntentFilter(POServerService.RECEIVE_MSG));

        checkServer();

        tvs_disconn.setOnClickListener(this);
        tvs_conn.setOnClickListener(this);
        lldownload.setOnClickListener(this);

        tv_ui_type.setTag(M.getMenuUI(context));
        if(M.getMenuUI(context)!=null && M.getMenuUI(context).equals(MenuUI.ui_vr))
            tv_ui_type.setText(context.getString(R.string.menu_type1));
        else if(M.getMenuUI(context)!=null && M.getMenuUI(context).equals(MenuUI.ui_hr))
            tv_ui_type.setText(context.getString(R.string.menu_type2));

        tv_ui_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new MenuUI(context,tv_ui_type);
            }
        });
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getExtras()!=null){
                if(intent.hasExtra("stopService")) {
                    mBound = false;
                    tvserverstatus.setText(getString(R.string.txt_disconnect));
                    tvs_disconn.setVisibility(View.GONE);
                    tvs_conn.setVisibility(View.VISIBLE);
                }else if(intent.hasExtra("response")){
                    try {
                        JSONObject jsonObject = new JSONObject(intent.getStringExtra("response"));
                        if (jsonObject.has("key")) {
                            String msg_key = jsonObject.getString("key");
                            if (msg_key.equals(WifiHelper.KEY_CONNECTION)) {
                               // Toast.makeText(context, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                                if (jsonObject.getString("success").equals(WifiHelper.TXT_FAIL)) {
                                    Boolean isPOS=AppConst.isMyServiceRunning(POServerService.class,context);
                                    if(isPOS)
                                        stopService(new Intent(context, POServerService.class));
                                }
                                checkServer();
                            }
                        }
                    }catch (JSONException e){}
                }
            }
        }
    };

    /** Defines callbacks for service binding, passed to bindService() */
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            POServerService.LocalBinder binder = (POServerService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if(compoundButton==swplaceorder)
            M.setItemsAlert(b,context);
        else if(compoundButton==swkot)
            M.setKOTAlert(b,context);
        else if(compoundButton==switem)
            M.setItemAlpha(b,context);
        else if(compoundButton==swcat)
            M.setCatAlpha(b,context);
        else if(compoundButton==swlsalert)
            M.setLowStockAlert(b,context);
        else if(compoundButton==swautoprint)
            M.setAutoPrint(b,context);
        else if(compoundButton==swlsale)
            M.setallowlowstockpunch(b,context);
        else if(compoundButton==swqty)
            M.qtyDialog(b,context);
        else if(compoundButton==swdelitem)
            M.setCustomAllow(M.key_dinein_delitem, b, context);
        else if(compoundButton==switemseparate)
            M.setCustomAllow(M.key_dinein_separate_item,b,context);
        else if (compoundButton==swtopspace)
            M.setCustomAllow(M.key_top_space,b,context);
    }

    public void showdialog()
    {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(PrinterSettings.this);
        builderSingle.setTitle("Select One Name:-");

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(PrinterSettings.this, android.R.layout.select_dialog_singlechoice);
        arrayAdapter.add("58 mm");
        arrayAdapter.add("78 mm");
        arrayAdapter.add("79 mm");
        arrayAdapter.add("80 mm");


        builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strName = arrayAdapter.getItem(which);

                txtpapersize.setText(getString(R.string.paper_width)+strName);

                if(strName.equals("58 mm")){
                    M.setPrinterpapersize(PrintFormat.smallsize, context);
                }else {
                    M.setPrinterpapersize(PrintFormat.normalsize, context);
                }


            }
        });
        builderSingle.show();
    }

    @Override
    protected void onActivityResult(int requestCode, final int resultCode, final Intent data) {

        if (resultCode == RESULT_OK && requestCode==101) {
            String DeviceType= data.getStringExtra(getString(R.string.DeviceType)).replace("*","");
            txt_cash_pritner.setText(""+DeviceType);
            M.setCashPrinterName(DeviceType, PrinterSettings.this);
        }else if (resultCode == RESULT_OK && requestCode==102) {
            String DeviceType= data.getStringExtra(getString(R.string.DeviceType)).replace("*","");
            txt_kitchen_pritner.setText(""+DeviceType);
            M.setKitchenPrinterName(DeviceType, PrinterSettings.this);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
            finish();
    }

    @Override
    protected void onDestroy() {
        AppConst.checkSame(context);
        if(ischangecol)
            EventBus.getDefault().post("updateColCount");
        if(broadcastReceiver!=null)
            unregisterReceiver(broadcastReceiver);
        if(mBound && mConnection!=null)
            unbindService(mConnection);
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        if(view==tvs_conn){
            Intent it = new Intent(context, POServerService.class);
            startService(it);
        }else if(view==tvs_disconn){
            if(mBound)
                mService.closeService();

            tvserverstatus.setText(getString(R.string.txt_disconnect));
            tvs_disconn.setVisibility(View.GONE);
            tvs_conn.setVisibility(View.VISIBLE);
        }else if(view==lldownload){
            new AlertDialog.Builder(context)
                    .setTitle(getString(R.string.alert_download))
                    .setMessage("Items Image from Server will be Saved In Device for Menu. Do You Wish To Continue?")
                    .setNegativeButton(android.R.string.no, null)
                    .setPositiveButton(android.R.string.yes,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface arg0, int arg1) {
                                    if (connectionDetector.isConnectingToInternet()) {
                                        final File myDir = new File(AppConst.item_img_dir);
                                        if (!myDir.exists()) {
                                            myDir.mkdirs();
                                        }
                                        if (myDir.isDirectory()) {
                                            String[] children = myDir.list();
                                            for (int i = 0; i < children.length; i++)
                                                new File(myDir, children[i]).delete();
                                        }
                                        DBDishes dbDishes=new DBDishes(context);
                                        List<DishPojo> dlist=dbDishes.getDishes("-1",context);
                                        cnt=0;
                                        startDownload(dlist,myDir);
                                    }
                                }
                            }).create().show();
        }else if(llcustomSC==view){
            final Dialog dialogSC=new Dialog(context);
            dialogSC.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogSC.setContentView(R.layout.dialog_ip_address);
            dialogSC.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            TextView tvhint=(TextView)dialogSC.findViewById(R.id.tvhint);
            tvhint.setText("Custom Label");
            final EditText etip=(EditText)dialogSC.findViewById(R.id.deviceIP);
            etip.setTypeface(AppConst.font_regular(context));
            Button btncancel=(Button)dialogSC.findViewById(R.id.btncancel);
            btncancel.setTypeface(AppConst.font_medium(context));
            Button btnsubmit=(Button)dialogSC.findViewById(R.id.btnsubmit);
            btnsubmit.setTypeface(AppConst.font_medium(context));

            etip.setHint(context.getString(R.string.enter_value));
            etip.setText(M.retriveVal(M.key_custom_service_charge,context));

            btncancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogSC.dismiss();
                }
            });

            btnsubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(etip.getText().toString().isEmpty())
                        etip.setError(context.getString(R.string.empty_field));
                    else{
                        tvcustomSC.setText(etip.getText().toString());
                        M.saveVal(M.key_custom_service_charge,etip.getText().toString(),context);
                        dialogSC.dismiss();
                    }
                }
            });
            dialogSC.show();
        }
    }
    private void requestForSpecificPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                    checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

            } else {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 101);
            }
        }
    }
    private void checkServer(){
        if(AppConst.isMyServiceRunning(POServerService.class,context)) {
            Intent intent = new Intent(this, POServerService.class);
            bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
            tvserverstatus.setText(getString(R.string.txt_connect));
            tvs_disconn.setVisibility(View.VISIBLE);
            tvs_conn.setVisibility(View.GONE);
        }else {
            tvserverstatus.setText(getString(R.string.txt_disconnect));
            tvs_disconn.setVisibility(View.GONE);
            tvs_conn.setVisibility(View.VISIBLE);
        }
    }

    public class CustomListAdapter extends BaseAdapter {
        private Context context;
        private ArrayList<String> items;

        public CustomListAdapter(Context context, ArrayList<String> items) {
            this.context = context;
            this.items = items;
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(context).
                        inflate(R.layout.pt_row, parent, false);
            }
            String currentItem = items.get(position);
            ImageView iv=(ImageView)convertView.findViewById(R.id.ivcheck);
            TextView textViewItemName = (TextView)convertView.findViewById(R.id.txt);
            textViewItemName.setText(currentItem);
            if(M.getTableRow(context)!=null && currentItem.equals(M.getTableRow(context))){
                textViewItemName.setTextColor(context.getResources().getColor(R.color.blue1));
                iv.setVisibility(View.VISIBLE);
            }else{
                textViewItemName.setTextColor(context.getResources().getColor(R.color.primary_text));
                iv.setVisibility(View.GONE);
            }

            return convertView;
        }
    }

    private void startDownload(List<DishPojo> dlist,File myDir){
        Log.d(TAG,"chk:"+cnt);
        if(cnt<dlist.size()){
            DishPojo d=dlist.get(cnt);
            cnt++;
            if (d.getDishimage() != null && d.getDishimage().trim().length() > 0) {
                Log.d(TAG,cnt+" "+d.getDishname()+"\n"+d.getDishimage());
                downloadImg(d.getDishimage(),dlist,myDir);
            }else
                startDownload(dlist,myDir);
        }else
            M.hideLoadingDialog();
    }

    private void downloadImg(final String ImageUrl, final List<DishPojo> dlist, final File myDir){
        Target target=new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                Log.d(TAG,"onBitmapLoaded");
                try {
                    String[] split = ImageUrl.split("/");
                    String name =split[split.length - 1];
                    File myDir1 = new File(myDir, name);
                    Log.d(TAG,"desti:"+myDir1.getPath());
                    FileOutputStream out = new FileOutputStream(myDir1);
                    bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);

                    out.flush();
                    out.close();
                    Log.d(TAG,"Image downloaded---");
                    startDownload(dlist,myDir);
                } catch(Exception e){
                    // some action
                    Log.d(TAG,"exp:"+e.getMessage());
                    startDownload(dlist,myDir);
                }
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                Log.d(TAG,"onBitmapFailed\n"+e.getMessage());
                startDownload(dlist,myDir);
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                Log.d(TAG,"onPrepareLoad");
            }
        };
        Picasso.get()
                .load(ImageUrl)
                .into(target);
    }
}
