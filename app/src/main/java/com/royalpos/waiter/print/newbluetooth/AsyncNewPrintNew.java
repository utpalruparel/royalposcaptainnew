package com.royalpos.waiter.print.newbluetooth;

import android.content.Context;
import android.util.Log;

import com.royalpos.waiter.model.M;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class AsyncNewPrintNew implements AsyncPrintNewCallBack{

    public static Socket printerSocket;
    public AsyncPrintNewCallBack printerCallBack = null;
    public static String internalClassName = "posprinterdriver_AsyncNetPrint";
    private static Boolean initialicing = false;
    private static Boolean operative = false;
    private static Boolean error = false;
    static String key;
    public  static int configuredPrinterPort = 9100;
    public static String configuredPrinterIp = "192.168.9.150";
    public static Boolean isKitchenAdv=false;
    public static String dataToPrint = "";
    public static Context context;
    // constructor stub
    public AsyncNewPrintNew() {
    }

    public void initialice(String key,String txt_texto, String tmpSrvIp, int tmpSrvPort, Context ctx) {
        Log.i(internalClassName, "initialice");
        setDataToPrint(getDataToPrint() + GlobalsNew.prepareDataToPrint(txt_texto));
        setConfiguredPrinterIp(tmpSrvIp);
        setConfiguredPrinterPort(tmpSrvPort);
        context = ctx;
        this.key=key;
        if (getInitialicing() == false) {
            Log.i(internalClassName, "");
            SocketThread clientTask = new SocketThread();
            clientTask.socketCallBack = this;
            new Thread(clientTask).start();
        } else {
            Log.i(internalClassName, "Socket is Initialicing");
        }
    }


    public static void printNow() {
        Log.i(internalClassName, "printNow");
        Log.i(internalClassName, getDataToPrint());
        Boolean printNow = false;
        if (getOperative()) {
            Log.i(internalClassName, "Socket data isConnected" + String.valueOf(printerSocket.isConnected()) + " isClosed" + String.valueOf(printerSocket.isClosed()));

            if (printerSocket.isConnected()) {
                printNow = true;
            }
        }
        if (printNow)

            try {
                //  String charset = "ISO-8859-1";

                if(M.isadvanceprint(M.key_bill,context) && key.equals(M.key_bill)){
                    GlobalsNew.mmOutputStream =
                            new DataOutputStream(printerSocket.getOutputStream());
                    GlobalsNew.printadvance(context);
                            try {
                                printerSocket.close();
                                setInitialicing(false);
                                setOperative(false);
                                setError(false);
                                setDataToPrint("");
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                }else if(isKitchenAdv && key.equals(M.key_kitchen)){
                    KitchenGlobalsNew.mmOutputStream =
                            new DataOutputStream(printerSocket.getOutputStream());
                    KitchenGlobalsNew.printadvance(context);
                            try {
                                printerSocket.close();
                                setInitialicing(false);
                                setOperative(false);
                                setError(false);
                                setDataToPrint("");
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.i(internalClassName, "printErrorResult A3" + e.toString());
                setInitialicing(false);
                setOperative(false);
                setError(true);
            }
        else {

        }
    }

    public static Boolean getInitialicing() {
        return initialicing;
    }

    public static void setInitialicing(Boolean initialicing) {
        AsyncNewPrintNew.initialicing = initialicing;
    }

    public static Boolean getOperative() {
        return operative;
    }

    public static void setOperative(Boolean operative) {
        AsyncNewPrintNew.operative = operative;
    }

    public static Boolean getError() {
        return error;
    }

    public static void setError(Boolean error) {
        AsyncNewPrintNew.error = error;
    }

    public static int getConfiguredPrinterPort() {
        return configuredPrinterPort;
    }

    public static void setConfiguredPrinterPort(int configuredPrinterPort) {
        AsyncNewPrintNew.configuredPrinterPort = configuredPrinterPort;
    }

    public static String getConfiguredPrinterIp() {
        return configuredPrinterIp;
    }

    public static void setConfiguredPrinterIp(String configuredPrinterIp) {
        AsyncNewPrintNew.configuredPrinterIp = configuredPrinterIp;
    }

    public static String getDataToPrint() {
        return dataToPrint;
    }

    public static void setDataToPrint(String dataToPrint) {
        dataToPrint = dataToPrint;
    }

    @Override
    public void printErrorResult(String output) {
        Log.i(internalClassName, "printErrorResult");

    }

    @Override
    public void printResult(String output) {
        Log.i(internalClassName, "printResult");

    }

    @Override
    public void statusChange() {
        Log.i(internalClassName, "statusChange");
        if (getOperative()) {

            Log.i(internalClassName, "socket.isConnected=" + String.valueOf(printerSocket.isConnected()));
            printNow();
            if (printerCallBack != null) {
                printerCallBack.printResult("OK");
            }
        }
        if (getError()) {
            Log.i(internalClassName, "Error detected in socked");
        }
    }

    class SocketThread implements Runnable {
        public AsyncPrintNewCallBack socketCallBack = null;

        @Override
        public void run() {
            Log.i(internalClassName, "run 1");
            setInitialicing(true);
            setOperative(false);
            setError(false);

            try {
                Log.i(internalClassName, "run 2");
                InetAddress serverAddr = InetAddress.getByName(getConfiguredPrinterIp());
                printerSocket = new Socket(serverAddr, getConfiguredPrinterPort());

                setInitialicing(false);
                setOperative(true);
                setError(false);
                socketCallBack.statusChange();
            } catch (UnknownHostException e1) {
                Log.i(internalClassName, "run ER 1");
                setInitialicing(false);
                setOperative(false);
                setError(true);
                socketCallBack.statusChange();
                e1.printStackTrace();
            } catch (IOException e1) {
                Log.i(internalClassName, "run ER 2");
                setInitialicing(false);
                setOperative(false);
                setError(true);
                e1.printStackTrace();
                socketCallBack.statusChange();
            }
        }
    }
}
