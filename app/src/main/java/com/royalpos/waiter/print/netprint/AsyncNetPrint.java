package com.royalpos.waiter.print.netprint;

import android.util.Log;

import com.royalpos.waiter.universalprinter.Globals;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class AsyncNetPrint implements AsyncPrintCallBack {

    private Socket printerSocket;
    public AsyncPrintCallBack printerCallBack = null;
    private String internalClassName = "posprinterdriver_AsyncNetPrint";
    private static Boolean initialicing = false;
    private static Boolean operative = false;
    private static Boolean error = false;

    private static int configuredPrinterPort = 9100;
    private static String configuredPrinterIp = "192.168.9.150";

    private String dataToPrint = "";

    // constructor stub
    public AsyncNetPrint() {
    }

    public void initialice(String txt_texto, String tmpSrvIp, int tmpSrvPort) {
        Log.i(internalClassName, "initialice");
        setDataToPrint(getDataToPrint() + Globals.prepareDataToPrint(txt_texto));
        setConfiguredPrinterIp(tmpSrvIp);
        setConfiguredPrinterPort(tmpSrvPort);
        if (getInitialicing() == false) {
            Log.i(internalClassName, "");
            SocketThread clientTask = new SocketThread();
            clientTask.socketCallBack = this;
            new Thread(clientTask).start();
        } else {
            Log.i(internalClassName, "Socket is Initialicing");
        }
    }


    public void printNow() {
        Log.i(internalClassName, "printNow");
        Log.i(internalClassName, getDataToPrint());
        Boolean printNow = false;
        if (getOperative()) {
            Log.i(internalClassName, "Socket data isConnected" + String.valueOf(printerSocket.isConnected()) + " isClosed" + String.valueOf(printerSocket.isClosed()));

            if (printerSocket.isConnected()) {
                printNow = true;
            }
        }
        if (printNow) try {
            //  String charset = "ISO-8859-1";

            DataOutputStream out = new DataOutputStream(printerSocket.getOutputStream());
            String data = getDataToPrint();

            byte[] dataToSend = Globals.stringToBytesASCII(data);
            out.write(dataToSend, 0, dataToSend.length);
            out.flush();


            Log.i(internalClassName, "Send data OK:" + getDataToPrint());
            printerSocket.close();
            setInitialicing(false);
            setOperative(false);
            setError(false);
            setDataToPrint("");

        } catch (UnknownHostException e) {
            Log.i(internalClassName, "printErrorResult A1");
            setInitialicing(false);
            setOperative(false);
            setError(true);

            e.printStackTrace();
        } catch (IOException e) {
            Log.i(internalClassName, "printErrorResult A2");
            setInitialicing(false);
            setOperative(false);
            setError(true);
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(internalClassName, "printErrorResult A3" + e.toString());
            setInitialicing(false);
            setOperative(false);
            setError(true);
        }
        else {

        }
    }

    public static Boolean getInitialicing() {
        return initialicing;
    }

    public static void setInitialicing(Boolean initialicing) {
        AsyncNetPrint.initialicing = initialicing;
    }

    public static Boolean getOperative() {
        return operative;
    }

    public static void setOperative(Boolean operative) {
        AsyncNetPrint.operative = operative;
    }

    public static Boolean getError() {
        return error;
    }

    public static void setError(Boolean error) {
        AsyncNetPrint.error = error;
    }

    public static int getConfiguredPrinterPort() {
        return configuredPrinterPort;
    }

    public static void setConfiguredPrinterPort(int configuredPrinterPort) {
        AsyncNetPrint.configuredPrinterPort = configuredPrinterPort;
    }

    public static String getConfiguredPrinterIp() {
        return configuredPrinterIp;
    }

    public static void setConfiguredPrinterIp(String configuredPrinterIp) {
        AsyncNetPrint.configuredPrinterIp = configuredPrinterIp;
    }

    public String getDataToPrint() {
        return dataToPrint;
    }

    public void setDataToPrint(String dataToPrint) {
        this.dataToPrint = dataToPrint;
    }

    @Override
    public void printErrorResult(String output) {
        Log.i(internalClassName, "printErrorResult");

    }

    @Override
    public void printResult(String output) {
        Log.i(internalClassName, "printResult");

    }

    @Override
    public void statusChange() {
        Log.i(internalClassName, "statusChange");
        if (getOperative()) {
            Log.i(internalClassName, "socket.isConnected=" + String.valueOf(printerSocket.isConnected()));
            printNow();
            if (printerCallBack != null) {
                printerCallBack.printResult("OK");
            }
        }
        if (getError()) {
            Log.i(internalClassName, "Error detected in socked");
        }
    }

    class SocketThread implements Runnable {
        public AsyncPrintCallBack socketCallBack = null;

        @Override
        public void run() {
            Log.i(internalClassName, "run 1");
            setInitialicing(true);
            setOperative(false);
            setError(false);

            try {
                Log.i(internalClassName, "run 2");
                InetAddress serverAddr = InetAddress.getByName(getConfiguredPrinterIp());
                printerSocket = new Socket(serverAddr, getConfiguredPrinterPort());
                setInitialicing(false);
                setOperative(true);
                setError(false);
                socketCallBack.statusChange();
            } catch (UnknownHostException e1) {
                Log.i(internalClassName, "run ER 1");
                setInitialicing(false);
                setOperative(false);
                setError(true);
                socketCallBack.statusChange();
                e1.printStackTrace();
            } catch (IOException e1) {
                Log.i(internalClassName, "run ER 2");
                setInitialicing(false);
                setOperative(false);
                setError(true);
                e1.printStackTrace();
                socketCallBack.statusChange();
            }
        }
    }
}