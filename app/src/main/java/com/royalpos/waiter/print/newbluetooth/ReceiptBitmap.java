package com.royalpos.waiter.print.newbluetooth;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;

public class ReceiptBitmap {


	int ypos = 0;
	int xpos = 0;
	int yoffset = 6;

	Bitmap canvasbitmap = null;
	Canvas canvas = null;
	Paint paintText = null;
	double scale = 0.80;
	ReceiptBitmap mReceiptBitmap = null;
	Typeface tf = Typeface.MONOSPACE;
	public ReceiptBitmap getInstance() {
		if(mReceiptBitmap == null){
			mReceiptBitmap = new ReceiptBitmap();
		}
		return mReceiptBitmap;
	}

	public void init(int size) {

		canvasbitmap = Bitmap.createBitmap(384, size, Bitmap.Config.ARGB_8888);
		canvas = new Canvas(canvasbitmap);
		canvas.drawARGB(255, 255, 255, 255);

		paintText = new Paint();
		paintText = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.LINEAR_TEXT_FLAG);

		paintText.setStyle(Paint.Style.FILL);
		paintText.setColor(Color.parseColor("#FF000000"));
		Typeface tf = Typeface.MONOSPACE;
		paintText.setTypeface(Typeface.create(tf, Typeface.BOLD));
		paintText.setTextAlign(Paint.Align.CENTER);
		paintText.setAntiAlias(true);

		paintText.setTextSize(40);

	}

	public void init(int width, int size) {


		canvasbitmap = Bitmap.createBitmap(width, size, Bitmap.Config.ARGB_8888);
		canvas = new Canvas(canvasbitmap);
		canvas.drawARGB(255, 255, 255, 255);

		paintText = new Paint();
		paintText = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.LINEAR_TEXT_FLAG);

		paintText.setStyle(Paint.Style.FILL);
		paintText.setColor(Color.parseColor("#FF000000"));
		paintText.setTextScaleX((float) 0.80);
		paintText.setTypeface(Typeface.create(tf, Typeface.BOLD));
		paintText.setTextAlign(Paint.Align.CENTER);
		paintText.setAntiAlias(true);

		paintText.setTextSize(40);

	}

	public void setTextSize(int size) {
		paintText.setTextSize(size);
	}

	public void setTypeface(Typeface typeface) {
		paintText.setTypeface(typeface);
	}

	public Bitmap getReceiptBitmap() {

		return canvasbitmap;
	}

	public int getReceiptHeight() {
		return ypos;
	}

	public void drawCenterText(String text)
	{
		paintText.setTextAlign(Paint.Align.LEFT);

		Rect bounds = new Rect();

		paintText.getTextBounds(text, 0, text.length(), bounds);

		ypos = ypos +  bounds.height() + yoffset;
		canvas.drawText(text, canvas.getWidth() / 2, ((paintText.descent() + paintText.ascent()) / 2) + ypos, paintText);

	}

	public void drawadvCenterText(String text, int size, boolean isbold)
	{
		paintText.setTextAlign(Paint.Align.CENTER);

		Rect bounds = new Rect();

		paintText.getTextBounds(text, 0, text.length(), bounds);
		paintText.setTextSize(size);
		paintText.setTextScaleX((float) scale);
		if(isbold) {
			paintText.setTypeface(Typeface.create(tf, Typeface.BOLD));
		}else{
			paintText.setTypeface(Typeface.create(tf, Typeface.NORMAL));
		}
		ypos = ypos +  bounds.height() + yoffset;

		canvas.drawText(text, canvas.getWidth() / 2, ((paintText.descent() + paintText.ascent()) / 2) + ypos, paintText);


	}

	public void drawNewLine(int endpoint)
	{
		Paint paint = new Paint();
		paint.setStyle(Paint.Style.FILL_AND_STROKE);

		paint.setTextSize(5);
		canvas.drawLine(20, 0, endpoint, 0, paint);
	}

	public void drawImage(Bitmap bitmap)
	{
		canvas.drawBitmap(bitmap, 100, ((paintText.descent() + paintText.ascent()) / 2) + ypos, paintText);
		ypos = ypos +  bitmap.getHeight() + yoffset;

	}

	public void drawSignaturImage(Bitmap bitmap)
	{
		Bitmap scaledbitmap = Bitmap.createScaledBitmap(bitmap, 400, 200, false);
		canvas.drawBitmap(scaledbitmap, xpos - 10, ypos, null);
		ypos = ypos +  bitmap.getHeight() + (yoffset + 30);

	}
	public boolean isPersian(String persianText)
	{
		return persianText.matches("/[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FF]/");
	}
	public void drawLineText(String text)
	{
		paintText.setTextAlign(Paint.Align.CENTER);

		Rect bounds = new Rect();

		paintText.getTextBounds(text, 0, text.length(), bounds);

		ypos = ypos +  bounds.height() + yoffset + 20;
		canvas.drawText(text, canvas.getWidth() / 2, ((paintText.descent() + paintText.ascent()) / 2) + ypos, paintText);

	}

	public void drawLeftText(String text)
	{
		paintText.setTextAlign(Paint.Align.LEFT);

		Rect bounds = new Rect();



		paintText.getTextBounds(text, 0, text.length(), bounds);

		ypos = ypos +  bounds.height() + yoffset;
		canvas.drawText(text, xpos, ypos, paintText);

	}

	public void drawLeftText(String text, int size, boolean isbold)
	{
		paintText.setTextAlign(Paint.Align.LEFT);
		Rect bounds = new Rect();

		if(isbold) {
			paintText.setTypeface(Typeface.create(tf, Typeface.BOLD));
		}else{
			paintText.setTypeface(Typeface.create(tf, Typeface.NORMAL));
		}
		paintText.setTextScaleX((float) 0.80);

		paintText.setTextSize(size);
		paintText.getTextBounds(text, 0, text.length(), bounds);

		ypos = ypos +  bounds.height() + yoffset;
		canvas.drawText(text, xpos, ypos, paintText);

	}

	public void drawRightText(String text)
	{
		paintText.setTextAlign(Paint.Align.LEFT);

		Rect bounds = new Rect();

		paintText.getTextBounds(text, 0, text.length(), bounds);

		ypos = ypos +  bounds.height() + yoffset;
		xpos = 384 - bounds.width();
		canvas.drawText(text, xpos, ypos, paintText);
		xpos = 0;

	}

	public void drawLeftAndRightText(String lefttext, String righttext)
	{
		paintText.setTextAlign(Paint.Align.LEFT);
		Rect bounds = new Rect();
		paintText.getTextBounds(lefttext, 0, lefttext.length(), bounds);

		ypos = ypos +  bounds.height() + yoffset;

		xpos = 0;
		canvas.drawText(lefttext, xpos, ypos, paintText);

		paintText.getTextBounds(righttext, 0, righttext.length(), bounds);

		xpos = 384 - bounds.width();
		canvas.drawText(righttext, xpos, ypos, paintText);

		xpos = 0;

	}
}