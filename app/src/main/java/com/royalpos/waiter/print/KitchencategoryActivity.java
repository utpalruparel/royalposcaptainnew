package com.royalpos.waiter.print;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.royalpos.waiter.R;
import com.royalpos.waiter.adapter.CatPrinterAdapter;
import com.royalpos.waiter.adapter.CategoryAdapter;
import com.royalpos.waiter.database.DBPrinter;
import com.royalpos.waiter.helper.AppConst;
import com.royalpos.waiter.model.CuisineListPojo;
import com.royalpos.waiter.model.KitchenPrinterPojo;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

public class KitchencategoryActivity extends AppCompatActivity implements View.OnClickListener {

    TextView tvheading;
    RecyclerView rv,rvprinters;
    Button btnadd;
    LinearLayout llkp;
    Context context;
    String TAG="KitchencategoryActivity";
    public static ArrayList<String> selcategory=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kitchencategory);
        if(getResources().getBoolean(R.bool.portrait_only)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        }
        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
        context=KitchencategoryActivity.this;
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        tvheading=(TextView)findViewById(R.id.tvheading);
        tvheading.setText(R.string.txt_category_kitchen);
        tvheading.setGravity(Gravity.CENTER_VERTICAL|Gravity.LEFT);
        llkp=(LinearLayout)findViewById(R.id.llkp);
        rv=(RecyclerView)findViewById(R.id.rvcat);
        rv.setLayoutManager(new LinearLayoutManager(context));
        rv.setHasFixedSize(true);
        rv.setNestedScrollingEnabled(false);
        rvprinters=(RecyclerView)findViewById(R.id.rvprinters);
        rvprinters.setLayoutManager(new LinearLayoutManager(context));
        rvprinters.setHasFixedSize(true);
        rvprinters.setNestedScrollingEnabled(false);
        btnadd=(Button)findViewById(R.id.btnadd);
        btnadd.setTypeface(AppConst.font_regular(context));

        getCategory();

        btnadd.setOnClickListener(this);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @SuppressWarnings("unused")
    public void onEventMainThread(String pusher) {
        if(pusher.equals("updateprinter")) {
            getCategory();
        }
    }

    private void getCategory() {
        if(selcategory==null)
            selcategory=new ArrayList<>();
        selcategory.clear();
        DBPrinter dbp=new DBPrinter(context);
        ArrayList<CuisineListPojo> clist=new ArrayList<>();
        clist.clear();
        clist= (ArrayList<CuisineListPojo>) dbp.getcusines();
        if(clist!=null && clist.size()>0){
            btnadd.setVisibility(View.VISIBLE);
            rv.setVisibility(View.VISIBLE);
            CategoryAdapter cadapter=new CategoryAdapter(clist,context);
            rv.setAdapter(cadapter);
        }else{
            rv.setVisibility(View.GONE);
            btnadd.setVisibility(View.GONE);
        }

        ArrayList<KitchenPrinterPojo> kplist=new ArrayList<>();
        kplist.clear();
        kplist= (ArrayList<KitchenPrinterPojo>) dbp.getPrinters();
        if(kplist!=null && kplist.size()>0){
            llkp.setVisibility(View.VISIBLE);
            CatPrinterAdapter kpadapter=new CatPrinterAdapter(kplist,context);
            rvprinters.setAdapter(kpadapter);
        }else{
            llkp.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {
        if(view==btnadd){
            String allIds = TextUtils.join(",", selcategory);
            Log.d(TAG,"sel:"+allIds);
            if(allIds!=null && allIds.trim().length()>0) {
                KPCategoryMainActivity.kpPojo=null;
                Intent it = new Intent(context, KPCategoryMainActivity.class);
                it.putExtra("category", allIds);
                startActivityForResult(it, 201);
            }else{
                Toast.makeText(context,R.string.select_min_1_category,Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode==201) {
            selcategory.clear();
            KPCategoryMainActivity.kpPojo=null;
            getCategory();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
