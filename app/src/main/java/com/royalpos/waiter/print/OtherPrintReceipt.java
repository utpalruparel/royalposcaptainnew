package com.royalpos.waiter.print;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;

//import com.itextpdf.text.BaseColor;
//import com.itextpdf.text.Document;
//import com.itextpdf.text.DocumentException;
//import com.itextpdf.text.Element;
//import com.itextpdf.text.Font;
//import com.itextpdf.text.PageSize;
//import com.itextpdf.text.Paragraph;
//import com.itextpdf.text.Rectangle;
//import com.itextpdf.text.pdf.PdfPCell;
//import com.itextpdf.text.pdf.PdfPTable;
//import com.itextpdf.text.pdf.PdfWriter;
import com.royalpos.waiter.R;
import com.royalpos.waiter.helper.AppConst;
import com.royalpos.waiter.helper.RoundHelper;
import com.royalpos.waiter.model.M;
import com.royalpos.waiter.universalprinter.Globals;
import com.royalpos.waiter.utils.AidlUtil;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by reeva on 10/7/18.
 */

public class OtherPrintReceipt {

    Context context;
    JSONObject jsonObject;
    String TAG="OtherPrintReceipt";
    PrintFormat pf;
    RoundHelper roundHelper;

    public OtherPrintReceipt(Context context,JSONObject jsonObject){
        this.context=context;
        pf=new PrintFormat(context);
        roundHelper=new RoundHelper(context);
        this.jsonObject=jsonObject;
        Log.d(TAG,"json:"+jsonObject);
    }

    public boolean shareonwhatsapp(String phonenumber){{
        StringBuilder textData = new StringBuilder();
        try {

            String order_no=jsonObject.getString("order_no");
            String finalprice=jsonObject.getString("grand_total");
            String usepoint=null;
            if(jsonObject.has("redeem_points")){
                String point=jsonObject.getString("redeem_points");
                JSONObject j=new JSONObject(point);
                String redeempoint=j.getString("redeem_points");
                if(redeempoint!=null && redeempoint.trim().length()>0){
                    if(Double.parseDouble(redeempoint)>0)
                        usepoint=redeempoint;
                }
            }

            String smstxt = AppConst.sendBillFormat;
            if(usepoint!=null) {
                smstxt = AppConst.sendBillFormat_loyalty;
                smstxt = smstxt.replace("{{loyalty_points}}",usepoint);
            }
            smstxt = smstxt.replace("{{brandname}}", M.getBrandName(context));
            smstxt = smstxt.replace("{{order_no}}", order_no);
            smstxt = smstxt.replace("{{amount}}", finalprice);

            String phonenum = phonenumber;
            phonenum = phonenum.replaceAll("/+", "");
            String url = "https://api.whatsapp.com/send?phone="+phonenum+ "&text="+smstxt;
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            context.startActivity(browserIntent);

        } catch (Exception e) {
            Log.d(TAG,"error:"+e.getMessage());
            return false;
        }
        return true;
    }
    }

    public boolean share() {
        StringBuilder textData = new StringBuilder();
        try {
            String tm=jsonObject.getString("order_time");
            String customername=jsonObject.getString("cust_name");
//            String customernumber = jsonObject.getString("cust_phone");
            String order_no=jsonObject.getString("order_no");
            String token_number=jsonObject.getString("token");
            String ordertype=jsonObject.getString("order_type");
            String subtotal=jsonObject.getString("total_amt");
            String finalprice=jsonObject.getString("grand_total");
            String payment_mode="";
            if(jsonObject.has("pay_mode_text"))
                payment_mode=jsonObject.getString("pay_mode_text");
            String pay_mode_id=null;
            if(jsonObject.has("pay_mode"))
                pay_mode_id=jsonObject.getString("pay_mode");
            String cash=jsonObject.getString("cash");
            String return_cash=jsonObject.getString("return_cash");
            JSONArray j1=null;
            if(jsonObject.has("taxes"))
                j1=jsonObject.getJSONArray("taxes");
            JSONArray jorder=jsonObject.getJSONArray("order");
            String ordercomment=jsonObject.getString("order_comment");
            String part_amt=null,pack_charge=null;
            if(jsonObject.has("part_payment_amt"))
                part_amt=jsonObject.getString("part_payment_amt");

            if(jsonObject.has("pack_charge"))
                pack_charge=jsonObject.getString("pack_charge");
            String status="";
            if(jsonObject.has("order_status"))
                status=jsonObject.getString("order_status");
            String cashrounding=null;
            if(jsonObject.has("rounding_json")) {
                cashrounding=roundHelper.extractRoundJSON(jsonObject.getString("rounding_json"));
                if(cashrounding!=null && Double.parseDouble(cashrounding)==0) {
                    cashrounding=null;
                }
            }
            String point=null;
            if(jsonObject.has("redeem_point")){
                point=jsonObject.getString("redeem_point");
            }

            textData.append(pf.padLine2(M.getBrandName(context),""+"\n"));
            textData.append(pf.padLine2(M.getRestName(context),"") + "\n");
            textData.append(pf.padLine2(M.getRestAddress(context) , "")+"\n");
            textData.append(pf.padLine2(context.getString(R.string.txt_phone) + ": " + M.getRestPhoneNumber(context),"") + "\n");
            if (M.getGST(context) != null && M.getGST(context).trim().length() > 0)
                textData.append(pf.padLine2(context.getString(R.string.txt_tax) + " # " + M.getGST(context),"") + "\n");
            textData.append(pf.divider()+"\n");
            if(M.getPapersize(context)==PrintFormat.normalsize)
                textData.append(pf.padLine2(context.getString(R.string.txt_order_by) + " : " + M.getWaitername(context),tm) + "\n");
            else {
                textData.append(pf.padLine2(context.getString(R.string.txt_order_by) + " : " + M.getWaitername(context), "") + "\n");
                textData.append(pf.padLine2(tm,"") + "\n");
            }
            textData.append(context.getString(R.string.txt_order)+" # " + order_no + "\n");
            textData.append(pf.divider()+"\n");
            textData.append(pf.padLine2(context.getResources().getString(R.string.txt_token) + " # " + token_number , "" ) + "\n");//ordertype
            textData.append(pf.divider()+"\n");

            if (customername!=null && customername.length() > 0) {
                textData.append(pf.padLine2(context.getString(R.string.txt_customer_name) + "\t"+customername,"")+ "\n");
                textData.append(pf.divider()+"\n");
            }

            //    textData.append("$small$" + " $al_left$");

            textData.append(pf.padLine1(context.getString(R.string.item), context.getString(R.string.txt_qty), "",context.getString(R.string.txt_amount)) + "\n");
            textData.append(pf.divider()+"\n");
            int tot_qty=0,tot_item=0;
            for (int i = 0; i < jorder.length(); i++) {
                tot_item = tot_item + 1;
                JSONObject obj=jorder.getJSONObject(i);
                String qty = obj.getString("quantity");
                tot_qty=(Integer.parseInt(qty))+tot_qty;
                String name =  obj.getString("dishname");
                String finalpri = obj.getString("price");
                String we="";
                if(obj.has("weight")) {
                    we=obj.getString("weight");
                    if (we!=null && we.trim().length() > 0 && Double.parseDouble(we) > 0){
                        we = we + obj.getString("unit_sort_name");
                    }else
                        we="";
                }
                textData.append(pf.padLine1(name+" "+we, qty+"" , "", pf.setFormat(finalpri+"")) + "\n");
                if(obj.has("prenm")) {
                    String pnm = obj.getString("prenm");
                    if (pnm != null && pnm.length() > 0) {
                        textData.append(pf.padLine1("    " + pnm,  "","","") + "\n");
                    }
                }

                if(obj.has("discount")) {
                    String idisc = obj.getString("discount");
                    if (idisc != null && idisc.trim().length() > 0 && Double.parseDouble(idisc) > 0) {
                        textData.append(pf.padLine1("    ", " ", " ", context.getString(R.string.txt_save) + " " + idisc) + "\n");
                    }
                }

                if(obj.has("combo")) {
                    String pnm = obj.getString("combo");
                    if (pnm != null && pnm.length() > 0) {
                        if(pnm.contains(",")){
                            String[] cs=pnm.split(",");
                            for(String s:cs)
                                textData.append(pf.padLine1(s,  "","","") + "\n");
                        }else
                            textData.append(pf.padLine1(pnm,  "","","") + "\n");
                    }
                }
            }
            textData.append(pf.divider()+"\n");
            textData.append(pf.padLine2(context.getString(R.string.txt_items)+":"+tot_item ,context.getString(R.string.txt_qty)+":"+tot_qty) + "\n");
            textData.append(pf.divider()+"\n");
            //sub total
            textData.append(pf.padLine2(context.getString(R.string.txt_subtotal),subtotal )+ "\n");
            //discount
            if(jsonObject.has("discount")){
                String dis=jsonObject.getString("discount");
                if(dis!=null && dis.trim().length()>0) {
                    JSONObject jdisc = new JSONObject(dis);
                    textData.append(pf.padLine2(context.getString(R.string.txt_discount), pf.setFormat(jdisc.getString("discount_amount") + "")) + "\n");
                    if (jsonObject.has("offer_name"))
                        textData.append(pf.padLine2("(" + jsonObject.getString("offer_name") + ")", "") + "\n");
                }
            }
            //loyalty point

            if(point!=null && point.trim().length()>0){
                JSONObject j=new JSONObject(point);
                textData.append(pf.padLine2(context.getString(R.string.redeem_point)+"("+j.getString("redeem_points")+")",j.getString("redeem_amount")) + "\n");
                textData.append(pf.padLine2("("+j.getString("points_per_one_currency")+" Points=1)","  " )+ "\n");
            }
            //tax
            if(j1!=null && j1.length()>0) {
                for (int i = 0; i < j1.length(); i++) {
                    JSONObject j = j1.getJSONObject(i);
                    String nm = j.getString("tax_name");
                    String per = j.getString("tax_per");
                    String val = j.getString("tax_value");
                    textData.append(pf.padLine2(nm + " @" + per + "%", val) + "\n");
                }
            }
            //packaging charge

            if(pack_charge!=null && pack_charge.trim().length()>0) {
                Double pa=Double.parseDouble(pack_charge);
                if(pa>0)
                    textData.append(pf.padLine2(context.getString(R.string.txt_packaging_charge) ,pf.setFormat(pack_charge))+ "\n");
            }
            //cash rounding
            if(cashrounding!=null){
                if(Double.parseDouble(cashrounding)!=0)
                    textData.append(pf.padLine2(context.getString(R.string.cash_rounding), pf.setFormat(cashrounding+"")) + "\n");
            }
            //grand total
            textData.append(pf.padLine2(context.getString(R.string.txt_total) ,pf.setFormat(finalprice)) + "\n\n");
            if(status!=null && status.trim().length()>0 && status.equals("4")){
                textData.append("$bighw$" +pf.padLine3(context.getString(R.string.refund) ,"") + "$big$"+"\n\n");
            }
            if(jsonObject.has("amount_due")) {
                textData.append(pf.padLine2(context.getString(R.string.amount_due), jsonObject.getString("amount_due")) + "\n");
            }else if(jsonObject.has("split_data")){
                JSONArray sarray=jsonObject.getJSONArray("split_data");
                if(sarray!=null && sarray.length()>0){
                    for(int i=0;i<sarray.length();i++){
                        JSONObject j=sarray.getJSONObject(i);
                        textData.append(pf.padLine2(context.getString(R.string.txt_amount_paid), j.getString("split_amount") ) + "\n");
                        textData.append(pf.padLine2(context.getString(R.string.payment_type), j.getString("payment_mode")) + "\n");
                    }
                }
            }
            if(pay_mode_id!=null && pay_mode_id.equals("-2") && part_amt!=null && part_amt.length()>0){
                JSONArray ja=new JSONArray(part_amt);
                for(int a=0;a<ja.length();a++){
                    JSONObject jao=ja.getJSONObject(a);
                    textData.append(pf.padLine2(context.getString(R.string.txt_amount_paid), pf.setFormat(jao.getString("amount"))) + "\n");
                    textData.append(pf.padLine2(context.getString(R.string.payment_type),jao.getString("payment_mode_text")) + "\n");
                }

                if(status==null)
                    status="";
                if(jsonObject.has("part_amt_due") && !status.equals("4"))
                    textData.append(pf.padLine2(context.getString(R.string.amount_due) ,pf.setFormat(jsonObject.getString("part_amt_due"))) + "\n");
            }
            if (cash!=null && return_cash!=null && cash.trim().length()>0 && return_cash.trim().length()>0) {

                textData.append(pf.padLine2(context.getString(R.string.txt_cash),pf.setFormat(cash)+"   ") + "\n");
                textData.append(pf.padLine2(context.getString(R.string.txt_change), pf.setFormat(return_cash)+"   ") + "\n");


            }

            textData.append(pf.divider()+"\n");

            if (M.getreceipt_footer(context).length() == 0) {
                textData.append(context.getString(R.string.txt_thanku)+"\n" + context.getString(R.string.txt_visitagain)+"\n");
            } else {
                textData.append(pf.padLine2( M.getreceipt_footer(context),"") + "\n");
            }

            textData.append(pf.padLine2(context.getString(R.string.txt_powered_by) + " " + M.getfooterphone(context),""));

            Log.d(TAG,"other data");
            Log.d(TAG,textData+"");

            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, textData.toString());
            context.startActivity(Intent.createChooser(sharingIntent, "Share using"));

        } catch (Exception e) {
            Log.d(TAG,"error:"+e.getMessage());
            return false;
        }
        return true;
    }

}
