package com.royalpos.waiter.print;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.royalpos.waiter.R;
import com.royalpos.waiter.database.DBPrinter;
import com.royalpos.waiter.helper.AppConst;
import com.royalpos.waiter.model.KitchenPrinterPojo;
import com.royalpos.waiter.universalprinter.KitchenGlobals;
import com.starmicronics.starprntsdk.CommonActivity;
import com.starmicronics.starprntsdk.ModelCapability;
import com.starmicronics.starprntsdk.PrinterSettingKitchenManager;
import com.starmicronics.starprntsdk.PrinterSettings;
import com.starmicronics.starprntsdk.SearchPortFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;


public class KPCategoryMainActivity extends AppCompatActivity{
    String internalClassName = "KPCategoryMainActivity";   // * Tag used on log messages.
  //  private SharedPreferences sharedPrefs = null;               // Preferences variables
    private String internalInfo = "";                           // *  Back info

    // Device data
    private static final int REQUEST_ENABLE_BT = 1;             // Used to request all BT devices
    private BluetoothAdapter mBluetoothAdapter;         // BlueTooth adacter to use
    private static final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";
    PendingIntent mPermissionIntent;
    // USB data


    // ************************
    // Objects in main activity
    private Button testPrinterButton = null;
    private Button saveDataButton = null;
    private Button closeButton = null;
    private Button btnEthOk = null;
    private CheckBox cbadvance =  null;
    EditText ettitle;
    private EditText deviceIP = null;
    private EditText devicePort = null;
    private TextView txt_info = null;

    private RadioButton deviceSelectEthernet = null;
    private RadioButton deviceSelectUsb = null;
    private RadioButton deviceSelectBT = null;
    RadioButton rbsunmi,rbstar;

    private LinearLayout layoutNet = null;
    private LinearLayout layoutUsb = null;
    private LinearLayout layoutBt = null,llstars;
    TextView tvpapersize,tvstarprinter;

    private Spinner allUsbDevices = null;
    private Spinner allBlueToothDevices = null;
    private ArrayList<HashMap<String, String>> uspDevicesMap;
    LinkedList<ObjectsClassData> blueToothDevicesItemscls = new LinkedList<ObjectsClassData>();
    private ArrayList<HashMap<String, String>> blueToothDevicesMap;
    LinkedList<ObjectsClassData> usbDevicesItemscls = new LinkedList<ObjectsClassData>();
    DBPrinter dbPrinter;
    Context context;
    int blueToothSpinnerSelected = -1;
    String catid,blueToothDeviceAdress="",deviceIPtxt="0.0.0.0",link_code="",picturePath="",logoProcesed="";
    int devicePorttxt = 9100,usbDeviceID = 0,usbVendorID = 0,usbProductID = 0,deviceType=0;
    public static KitchenPrinterPojo kpPojo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kitchencat_main_layout);                      // * Start with main_layout
        if(getResources().getBoolean(R.bool.portrait_only)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
        context=KPCategoryMainActivity.this;
        ettitle=(EditText)findViewById(R.id.ettitle);
        cbadvance= (CheckBox) findViewById(R.id.cbadvanced);
        cbadvance.setTypeface(AppConst.font_regular(context));
        tvstarprinter=(TextView)findViewById(R.id.tvstarprinter);
        tvstarprinter.setTag("0");
        llstars=(LinearLayout)findViewById(R.id.llstar);
        llstars.setTag("");
        if(getIntent().getStringExtra("category")==null)
            finish();
        else
            catid=getIntent().getStringExtra("category");
        getSupportActionBar().setTitle("Category Kitchen");
        dbPrinter=new DBPrinter(context);
//        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);  // * load preference manager
        //KitchenGlobals.loadPreferences(sharedPrefs);                               // * load preference data
        tvpapersize=(TextView)findViewById(R.id.tvsize);
        tvpapersize.setText(context.getString(R.string.paper_width_78));
        tvpapersize.setTag(PrintFormat.normalsize+"");
        if(kpPojo!=null){
            usbDeviceID = KitchenGlobals.mathIntegerFromString(kpPojo.getUsbDeviceID()+"", 0);
            usbVendorID = KitchenGlobals.mathIntegerFromString(kpPojo.getUsbVendorID()+"", 0);
            usbProductID = KitchenGlobals.mathIntegerFromString(kpPojo.getUsbProductID(), 0);
            link_code = kpPojo.getLink_code();
            picturePath = kpPojo.getPicturePath();
            blueToothDeviceAdress = kpPojo.getBlueToothDeviceAdress();
            deviceType = KitchenGlobals.mathIntegerFromString(kpPojo.getDeviceType()+"", 0);
            deviceIPtxt = kpPojo.getDeviceIP();
            devicePorttxt = KitchenGlobals.mathIntegerFromString(kpPojo.getDevicePort(), 9100);
            logoProcesed = kpPojo.getLogoProcesed();
            ettitle.setText(kpPojo.getPrinter_title());
            ettitle.setTag("Kitchen Printer "+kpPojo.getId());
            cbadvance.setChecked(kpPojo.isAdv());
            if(kpPojo.getPaper_width()!=null && kpPojo.getPaper_width().equals(PrintFormat.smallsize+"")) {
                tvpapersize.setText(context.getString(R.string.paper_width_58));
                tvpapersize.setTag(PrintFormat.smallsize+"");
            }else if(kpPojo.getPaper_width()!=null && kpPojo.getPaper_width().equals(PrintFormat.size79+"")) {
                tvpapersize.setText(context.getString(R.string.paper_width_79));
                tvpapersize.setTag(PrintFormat.size79+"");
            }else if(kpPojo.getPaper_width()!=null && kpPojo.getPaper_width().equals(PrintFormat.size80+"")) {
                tvpapersize.setText(context.getString(R.string.paper_width_80));
                tvpapersize.setTag(PrintFormat.size80+"");
            }
            if(deviceType==9){
                String star= kpPojo.getStar_settings();
                if(star!=null && !star.isEmpty()){
                    try {
                        JSONObject jstar=new JSONObject(star);
                        tvstarprinter.setText(jstar.getString("model"));
                        tvstarprinter.setTag(jstar.getString("modelIndex"));
                        llstars.setTag(jstar.getString("settings"));
                    }catch (JSONException e){}
                }
            }
        }else{
            DBPrinter dbPrinter=new DBPrinter(context);
            int cnt=dbPrinter.getPrinterCount()+1;
            ettitle.setTag("Kitchen Printer "+cnt);
            ettitle.setText("Kitchen Printer "+cnt);
        }
        ettitle.setSelection(ettitle.getText().length());

        txt_info = (TextView) findViewById(R.id.txt_info);

        mPermissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);
        IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
        //registerReceiver(mUsbReceiver, filter);
        registerReceiver(mUsbDeviceReceiver, new IntentFilter(UsbManager.ACTION_USB_DEVICE_ATTACHED));
        registerReceiver(mUsbDeviceReceiver, new IntentFilter(UsbManager.ACTION_USB_DEVICE_DETACHED));

        refreshUsbDevices();
        if (KitchenGlobals.deviceType == 4) {
            refreshBlueToothDevices();
        }

        try {
            testPrinterButton = (Button) findViewById(R.id.printTest);
            saveDataButton = (Button) findViewById(R.id.saveAndClose);
            closeButton = (Button) findViewById(R.id.closeButton);
            btnEthOk = (Button) findViewById(R.id.btnEthOk);

            deviceSelectEthernet = (RadioButton) findViewById(R.id.deviceSelectEthernet);
            deviceSelectEthernet.setTypeface(AppConst.font_regular(context));
            deviceSelectUsb = (RadioButton) findViewById(R.id.deviceSelectUsb);
            deviceSelectUsb.setTypeface(AppConst.font_regular(context));
            deviceSelectBT = (RadioButton) findViewById(R.id.deviceSelectBT);
            deviceSelectBT.setTypeface(AppConst.font_regular(context));
            rbsunmi = (RadioButton) findViewById(R.id.rbSunmi);
            rbsunmi.setTypeface(AppConst.font_regular(context));
            rbstar= (RadioButton) findViewById(R.id.rbStar);
            rbstar.setTypeface(AppConst.font_regular(context));

            layoutNet = (LinearLayout) findViewById(R.id.lly_net);
            layoutUsb = (LinearLayout) findViewById(R.id.lly_usb);
            layoutBt = (LinearLayout) findViewById(R.id.lly_bt);
            deviceIP = (EditText) findViewById(R.id.deviceIP);
            devicePort = (EditText) findViewById(R.id.devicePort);

            updateUi();
            //SELECTOR FOR BlueTooth DEVICES
            allBlueToothDevices = (Spinner) findViewById(R.id.blueToothDevices);
            allBlueToothDevices.setOnItemSelectedListener(new OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                    Log.d(internalClassName, "Click spinner onItemSelected 1 BT");
                    if (parent.getId() == R.id.blueToothDevices) {
                        Log.d(internalClassName, "Click BlueTooth Spinner");
                        if (blueToothDevicesMap.size() > 0 && blueToothDevicesMap.size()>pos) {
                            Log.d(internalClassName, "Click en device--XX.XX.XX.XX " + blueToothDevicesMap.get(pos).get("DeviceAddress").toString());
                            blueToothDeviceAdress = blueToothDevicesMap.get(pos).get("DeviceAddress").toString();       // *  save un global config
                        }
                    }
                    //other spinners. (not need)
                }

                public void onNothingSelected(AdapterView<?> parent) {
                    // Do nothing.
                }
            });
            //SELECTOR FOR USB DEVICES
            allUsbDevices = (Spinner) findViewById(R.id.usbDevices);
            allUsbDevices.setOnItemSelectedListener(new OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                    Log.d(internalClassName, "Click en onItemSelected 2 USB");
                    if (parent.getId() == R.id.usbDevices) {
                        Log.d(internalClassName, "Click on device>>> " + KitchenGlobals.usbDeviceID);
                        if (uspDevicesMap.size() > 0) {
                            Log.d(internalClassName, "Click en device ID " + uspDevicesMap.get(pos).get("usbDeviceID").toString());
                            HashMap<String, String> mapa = uspDevicesMap.get(pos);

                            usbDeviceID = KitchenGlobals.mathIntegerFromString(mapa.get("usbDeviceID").toString(), 0);
                            usbProductID = KitchenGlobals.mathIntegerFromString(mapa.get("ProductID").toString(), 0);
                            usbVendorID = KitchenGlobals.mathIntegerFromString(mapa.get("VendorID").toString(), 0);
                        }
                    }
                    //other spinners. (not need)
                }

                public void onNothingSelected(AdapterView<?> parent) {
                    // Do nothing.
                }
            });

            tvpapersize.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showdialog();
                }
            });

            tvstarprinter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SearchPortFragment.printer_type="kitchen";
                    Intent intent = new Intent(KPCategoryMainActivity.this, CommonActivity.class);
                    intent.putExtra(CommonActivity.BUNDLE_KEY_ACTIVITY_LAYOUT, R.layout.activity_printer_search);
                    intent.putExtra(CommonActivity.BUNDLE_KEY_TOOLBAR_TITLE, "Search Port");
                    intent.putExtra(CommonActivity.BUNDLE_KEY_SHOW_HOME_BUTTON, true);
                    intent.putExtra(CommonActivity.BUNDLE_KEY_SHOW_RELOAD_BUTTON, true);
                    intent.putExtra(CommonActivity.BUNDLE_KEY_PRINTER_SETTING_INDEX, 0);    // Index of the backup printer

                    startActivityForResult(intent,PrintMainActivity. PRINTER_SET_REQUEST_CODE);
                }
            });

            testPrinterButton.setVisibility(View.GONE);
            // we are goin to have three buttons for specific functions
            // send data typed by the user to be printed
//            testPrinterButton.setOnClickListener(new View.OnClickListener() {
//                public void onClick(View v) {
//                    Log.i(internalClassName, "testPrinterButton PRESSED");
//                    updateNetCondigFronTextData();
//                    KitchenGlobals.savePreferences(sharedPrefs);
//                    testPrint();
//
//                }
//            });

            btnEthOk.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Log.i(internalClassName, "btnEthOk PRESSED");
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);//(Hide keyboard)
                    imm.hideSoftInputFromWindow(deviceIP.getWindowToken(), 0);
                    updateNetCondigFronTextData();
                    addPrinter(deviceSelectEthernet.getText().toString());
                    //KitchenGlobals.savePreferences(sharedPrefs);
                    Intent intent = new Intent();
//                    intent.putExtra(getString(R.string.title_target), KitchenGlobals.deviceType);
//                    intent.putExtra(getString(R.string.printername), "selected");
//                    intent.putExtra(getString(R.string.DeviceType), deviceSelectEthernet.getText().toString());
                    setResult(RESULT_OK, intent);

                    finish();
                }
            });


            closeButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Log.i(internalClassName, "closeButton -> finish ACTIVITY");
                    finish();
                }
            });

            saveDataButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    String cprinter;
                    if(deviceSelectBT.isChecked())
                        cprinter=deviceSelectBT.getText().toString();
                    else if(deviceSelectEthernet.isChecked())
                        cprinter=deviceSelectEthernet.getText().toString();
                    else if(deviceSelectUsb.isChecked())
                        cprinter=deviceSelectUsb.getText().toString();
                    else if(rbsunmi.isChecked())
                        cprinter=rbsunmi.getText().toString();
                    else if(rbstar.isChecked()){
                        cprinter=rbstar.getText().toString();
                    }
                    else
                        cprinter="Select Printer";
                    Log.i(internalClassName, "saveDataButton -> Save data ");
                    updateNetCondigFronTextData();
                    addPrinter(cprinter);
                    Intent intent = new Intent();
                    setResult(RESULT_OK, intent);

                    finish();
               }
            });



        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        setDefaultData();
    }

    public void showdialog() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(context);
        builderSingle.setTitle("Select One Name:-");

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.select_dialog_singlechoice);
        arrayAdapter.add("58 mm");
        arrayAdapter.add("78 mm");
        arrayAdapter.add("79 mm");
        arrayAdapter.add("80 mm");

        builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strName = arrayAdapter.getItem(which);

                tvpapersize.setText(getString(R.string.paper_width)+strName);

                if(strName.equals("58 mm")){
                    tvpapersize.setTag(PrintFormat.smallsize+"");
                }else if(strName.equals("79 mm")){
                    tvpapersize.setTag(PrintFormat.size79+"");
                }else if(strName.equals("80 mm")){
                    tvpapersize.setTag(PrintFormat.size80+"");
                }else {
                    tvpapersize.setTag(PrintFormat.normalsize+"");
                }
            }

        });
        builderSingle.show();
    }

    private void addPrinter(String pnm){
        KitchenPrinterPojo kp=new KitchenPrinterPojo();
        kp.setPrinter_name(pnm.replace("*",""));
        kp.setUsbVendorID(usbVendorID+"");
        kp.setUsbProductID(usbProductID+"");
        kp.setUsbDeviceID(usbDeviceID+"");
        kp.setPicturePath(picturePath);
        kp.setLogoProcesed(logoProcesed);
        kp.setBlueToothDeviceAdress(blueToothDeviceAdress);
        kp.setLink_code(link_code);
        kp.setCategoryid(catid);
        kp.setDeviceIP(deviceIPtxt);
        kp.setDevicePort(devicePorttxt+"");
        kp.setDeviceType(deviceType+"");
        kp.setAdv(cbadvance.isChecked());
        if(tvpapersize.getTag()!=null)
            kp.setPaper_width(tvpapersize.getTag().toString());
        else
            kp.setPaper_width(PrintFormat.normalsize+"");
        String t=ettitle.getTag().toString();
        if(ettitle.getText().toString().trim().length()>0)
            t=ettitle.getText().toString();
        kp.setPrinter_title(t);
        if(deviceType==9){
            try {
                JSONObject jstar=new JSONObject();
                jstar.put("model",tvstarprinter.getText().toString());
                jstar.put("modelIndex",tvstarprinter.getTag().toString());
                jstar.put("settings",llstars.getTag().toString());
                kp.setStar_settings(jstar+"");
            }catch (JSONException e){}
        }
        if(kpPojo==null)
            dbPrinter.addPrinter(kp);
        else{
            kp.setId(kpPojo.getId());
            dbPrinter.updatePrinterCategory(kp);
        }
    }

    @Override
    protected void onResume() {
        Log.i(internalClassName, "onResume");
        super.onResume();
//        checkPlayServices();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(internalClassName, "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(internalClassName, "onStop");
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(mUsbDeviceReceiver);
        super.onDestroy();
        Log.i(internalClassName, "onDestroy");
    }


    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.deviceSelectEthernet:
                if (checked)
                    deviceType = 1;
                updateUi();
                break;
            case R.id.deviceSelectUsb:
                if (checked)
                    deviceType = 3;
                updateUi();
                break;
            case R.id.deviceSelectBT:
                if (checked)
                    deviceType = 4;
                refreshBlueToothDevices();
                updateUi();
                break;
            case R.id.rbSunmi:
                if(checked)
                    deviceType = 5;
                updateUi();
                break;
            case R.id.rbStar:
                if(checked)
                    deviceType = 9;
                updateUi();
                break;
        }
        setDefaultData();
      //  KitchenGlobals.savePreferences(sharedPrefs);

    }

    public void testPrint() {
        Log.i(internalClassName, "testPrint");

        String dataToPrint = "";
        dataToPrint = KitchenGlobals.getDemoText(this.getResources(), this);
        Log.e(internalClassName, "testPrint:" + dataToPrint);

        Intent sendIntent = new Intent(this, PrintActivity.class);
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, dataToPrint);
        sendIntent.putExtra("internal", "1");
        sendIntent.setType("text/plain");
        this.startActivity(sendIntent);
    }

    public void updateNetCondigFronTextData() {
        Log.i(internalClassName, "updateNetCondigFronTextData");
        deviceIPtxt = deviceIP.getText().toString();
        devicePorttxt = KitchenGlobals.mathIntegerFromString(devicePort.getText().toString(), 9100);
    }

    public void setDefaultData() {
        Log.i(internalClassName, "setDefaultData");

        //Set option from config
        deviceSelectEthernet.setChecked(deviceType==1);
        deviceSelectUsb.setChecked(deviceType==3);
        deviceSelectBT.setChecked(deviceType==4);
        rbsunmi.setChecked(deviceType==5);
        rbstar.setChecked(deviceType==9);
        deviceIP.setEnabled(deviceType==1);
        devicePort.setEnabled(deviceType==1);

        deviceIP.setText(deviceIPtxt+"");
        devicePort.setText(devicePorttxt+"");

        deviceIP.setSelection(deviceIP.getText().length());
        devicePort.setSelection(devicePort.getText().length());

        testPrinterButton.setEnabled(deviceType!=0);
        allUsbDevices.setEnabled(deviceType==3);
        if (deviceType==3) {
            ArrayAdapter<ObjectsClassData> spinner_adapter = new ArrayAdapter<ObjectsClassData>(this, R.layout.business_type_row,R.id.txt, usbDevicesItemscls);
            allUsbDevices.setAdapter(spinner_adapter);
        }

        allBlueToothDevices.setEnabled(deviceType==4);
        if (deviceType==4) {
            ArrayAdapter<ObjectsClassData> spinnerAdapterBT = new ArrayAdapter<ObjectsClassData>(this, R.layout.business_type_row,R.id.txt, blueToothDevicesItemscls);
            Log.i(internalClassName, "BTSpinner");
            allBlueToothDevices.setAdapter(spinnerAdapterBT);
            if (blueToothSpinnerSelected != -1) {
                Log.i(internalClassName, "BTSpinner SET");
                allBlueToothDevices.setSelection(blueToothSpinnerSelected);
            }
        }

        if(deviceType==9){
            llstars.setVisibility(View.VISIBLE);
        }else
            llstars.setVisibility(View.GONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(internalClassName, "onActivityResult(" + requestCode + "," + resultCode + "," + data);
        if (requestCode == REQUEST_ENABLE_BT) {
            Log.i(internalClassName, "onActivityResult BlueTooth ENABLED OK> refresh Devices");
            refreshBlueToothDevices();
        }else if (requestCode ==PrintMainActivity. PRINTER_SET_REQUEST_CODE) {
            updateList();
        }

    }
    public void updateList(){
        PrinterSettingKitchenManager settingManager = new PrinterSettingKitchenManager(context);
        PrinterSettings settings       = settingManager.getPrinterSettings();

        boolean isDeviceSelected     = false;
        int     modelIndex           = ModelCapability.NONE;
        String  modelName            = "";
        boolean isBluetoothInterface = false;
        boolean isUsbInterface       = false;

        if (settings != null) {
            isDeviceSelected     = true;
            modelIndex           = settings.getModelIndex();
            modelName            = settings.getModelName();
            isBluetoothInterface = settings.getPortName().toUpperCase().startsWith("BT:");
            isUsbInterface       = settings.getPortName().toUpperCase().startsWith("USB:");
            tvstarprinter.setText(""+modelName);
            tvstarprinter.setTag(modelIndex+"");
            llstars.setTag(settings.getPortSettings());
        }


    }

    private final BroadcastReceiver mUsbDeviceReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i("BroadcastReceiver=", "onReceive 1 1");
            refreshUsbDevices();
        }
    };


    private boolean refreshBlueToothDevices() {
        Log.i(internalClassName, "refreshBlueToothDevices  ");

        boolean success = false;
        blueToothDevicesMap = new ArrayList<HashMap<String, String>>();
        try {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (mBluetoothAdapter == null) {
                success = false;
                internalInfo = internalInfo + "No bluetooth adapter available";
            }
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBluetooth, 0);
            }

            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
            HashMap<String, String> map = null;

            int position = 0;
            if (pairedDevices.size() > 0) {
                if(blueToothDevicesMap!=null)
                    blueToothDevicesMap.clear();
                if(blueToothDevicesItemscls!=null)
                    blueToothDevicesItemscls.clear();
                for (BluetoothDevice device : pairedDevices) {
                    Log.i(internalClassName, "refreshBlueToothDevices pairedDevices:: " + device.getAddress());
                    map = new HashMap<String, String>();
                    map.put("DeviceAddress", String.valueOf(device.getAddress()));
                    map.put("DeviceName", String.valueOf(device.getName()));
                    if (blueToothDeviceAdress.equals(String.valueOf(device.getAddress()))) {
                        blueToothSpinnerSelected = position;
                    }
                    map.put("posicion", String.valueOf(position));
                    blueToothDevicesMap.add(map);
                    blueToothDevicesItemscls.add(new ObjectsClassData(position, device.getAddress() + " " + String.valueOf(device.getName())));
                    success = true;
                    position = position + 1;
                }
                updateUi();
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
            success = false;
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }
        return success;
    }

    private void refreshUsbDevices() {
        Log.i(internalClassName, "refreshUsbDevices");

        uspDevicesMap = new ArrayList<HashMap<String, String>>();

        UsbManager manager = (UsbManager) getSystemService(Context.USB_SERVICE);
        HashMap<String, UsbDevice> deviceList = manager.getDeviceList();
        Iterator<UsbDevice> deviceIterator = deviceList.values().iterator();
        HashMap<String, String> map = null;
        usbDevicesItemscls = new LinkedList<ObjectsClassData>();
        List<String> usbDevicesItems = new ArrayList<String>();
        int position = 0;

        while (deviceIterator.hasNext()) {
            position++;

            UsbDevice device = deviceIterator.next();
            Log.i(internalClassName, "VendorId=" + device.getVendorId() + " ProductId=" + device.getProductId() + " DeviceName=" + device.getDeviceName());
            map = new HashMap<String, String>();
            map.put("usbDeviceID", String.valueOf(device.getDeviceId()));
            map.put("DeviceName", String.valueOf(device.getDeviceName()));
            map.put("DeviceClass", String.valueOf(device.getDeviceClass()));
            map.put("DeviceSubclass", String.valueOf(device.getDeviceSubclass()));
            map.put("VendorID", String.valueOf(device.getVendorId()));
            map.put("ProductID", String.valueOf(device.getProductId()));
            map.put("InterfaceCount", String.valueOf(device.getInterfaceCount()));
            map.put("posicion", String.valueOf(-1));
            uspDevicesMap.add(map);
            usbDevicesItems.add(String.valueOf(device.getDeviceId()));
            usbDevicesItemscls.add(new ObjectsClassData(position, String.valueOf(device.getDeviceId()) + " " + device.getDeviceName()));
        }
    }



    public class ObjectsClassData {
        int id;
        String name;

        //Constructor
        public ObjectsClassData(int id, String name) {
            super();
            this.id = id;
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }

        public int getId() {
            return id;
        }
    }

    // updates UI to reflect model
    public void updateUi() {

        layoutNet.setVisibility(View.GONE);
        layoutUsb.setVisibility(View.GONE);
        layoutBt.setVisibility(View.GONE);
        llstars.setVisibility(View.GONE);
        if (deviceType==1) layoutNet.setVisibility(View.VISIBLE);
        if (deviceType==3) layoutUsb.setVisibility(View.VISIBLE);
        if (deviceType==4) layoutBt.setVisibility(View.VISIBLE);
        if(deviceType==9){
            llstars.setVisibility(View.VISIBLE);
        }else
            llstars.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}