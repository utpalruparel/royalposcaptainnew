package com.royalpos.waiter.print;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.royalpos.waiter.R;
import com.royalpos.waiter.helper.AppConst;
import com.royalpos.waiter.helper.MemoryCache;
import com.royalpos.waiter.model.M;
import com.royalpos.waiter.print.newbluetooth.GlobalsNew;
import com.royalpos.waiter.print.newbluetooth.PrintActivityNew;
import com.royalpos.waiter.universalprinter.Globals;
import com.royalpos.waiter.utils.AidlUtil;
import com.starmicronics.starioextension.ICommandBuilder;
import com.starmicronics.starioextension.StarIoExt;
import com.starmicronics.starprntsdk.CommonActivity;
import com.starmicronics.starprntsdk.Communication;
import com.starmicronics.starprntsdk.ModelCapability;
import com.starmicronics.starprntsdk.PrinterSettingManager;
import com.starmicronics.starprntsdk.PrinterSettings;
import com.starmicronics.starprntsdk.SearchPortFragment;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class PrintMainActivity extends AppCompatActivity{
    private static String internalClassName = "MainActivity";   // * Tag used on log messages.

    private SharedPreferences sharedPrefs = null;               // Preferences variables
    private String internalInfo = "";                           // *  Back info
    public static final int PRINTER_SET_REQUEST_CODE = 15;
    private static final int REQUEST_ENABLE_BT = 1;             // Used to request all BT devices
    private BluetoothAdapter mBluetoothAdapter;         // BlueTooth adacter to use
    private static final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";
    PendingIntent mPermissionIntent;
    // USB data
    // ************************
    // Objects in main activity
    private Button testPrinterButton = null;
    private Button saveDataButton = null;
    private Button closeButton = null;
    private Button btnEthOk = null;
    TextView tvpapersize,tvstarprinter;

    private EditText deviceIP = null;
    private EditText devicePort = null;

    private RadioButton deviceSelectEthernet = null;
    private RadioButton deviceSelectUsb = null;
    private RadioButton deviceSelectBT = null;
    RadioButton rbsunmi,rbstar;
    private LinearLayout layoutNet = null;
    private LinearLayout layoutUsb = null;
    private LinearLayout layoutBt = null,llstar;
    private CheckBox cbadvance =  null;

    private Spinner allUsbDevices = null;
    private Spinner allBlueToothDevices = null;
    private ArrayList<HashMap<String, String>> uspDevicesMap;
    LinkedList<ObjectsClassData> blueToothDevicesItemscls = new LinkedList<ObjectsClassData>();
    private ArrayList<HashMap<String, String>> blueToothDevicesMap;
    LinkedList<ObjectsClassData> usbDevicesItemscls = new LinkedList<ObjectsClassData>();

    String cprinter="Select Printer";
    Context context;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);
        if(getResources().getBoolean(R.bool.portrait_only)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        }
        MemoryCache  memoryCache = new MemoryCache();
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);// * Start with main_layout
        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);  // * load preference manager
        Globals.loadPreferences(sharedPrefs);                               // * load preference data

        context=PrintMainActivity.this;

        mPermissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);
        IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
        //registerReceiver(mUsbReceiver, filter);
        registerReceiver(mUsbDeviceReceiver, new IntentFilter(UsbManager.ACTION_USB_DEVICE_ATTACHED));
        registerReceiver(mUsbDeviceReceiver, new IntentFilter(UsbManager.ACTION_USB_DEVICE_DETACHED));

        refreshUsbDevices();
        if (Globals.deviceType == 4) {
            refreshBlueToothDevices();
        }

        try {
            testPrinterButton = (Button) findViewById(R.id.printTest);
            testPrinterButton.setTypeface(AppConst.font_regular(context));
            saveDataButton = (Button) findViewById(R.id.saveAndClose);
            saveDataButton.setTypeface(AppConst.font_regular(context));
            closeButton = (Button) findViewById(R.id.closeButton);
            closeButton.setTypeface(AppConst.font_regular(context));
            btnEthOk = (Button) findViewById(R.id.btnEthOk);
            btnEthOk.setTypeface(AppConst.font_regular(context));
            deviceSelectEthernet = (RadioButton) findViewById(R.id.deviceSelectEthernet);
            deviceSelectEthernet.setTypeface(AppConst.font_regular(context));
            deviceSelectUsb = (RadioButton) findViewById(R.id.deviceSelectUsb);
            deviceSelectUsb.setTypeface(AppConst.font_regular(context));
            deviceSelectBT = (RadioButton) findViewById(R.id.deviceSelectBT);
            deviceSelectBT.setTypeface(AppConst.font_regular(context));
            tvpapersize=(TextView)findViewById(R.id.tvsize);
            rbsunmi = (RadioButton) findViewById(R.id.rbSunmi);
            rbsunmi.setTypeface(AppConst.font_regular(context));
            rbstar= (RadioButton) findViewById(R.id.rbStar);
            rbstar.setTypeface(AppConst.font_regular(context));
            llstar=(LinearLayout)findViewById(R.id.llstar);
            layoutNet = (LinearLayout) findViewById(R.id.lly_net);
            layoutUsb = (LinearLayout) findViewById(R.id.lly_usb);
            layoutBt = (LinearLayout) findViewById(R.id.lly_bt);
            deviceIP = (EditText) findViewById(R.id.deviceIP);
            deviceIP.setTypeface(AppConst.font_regular(context));
            devicePort = (EditText) findViewById(R.id.devicePort);
            devicePort.setTypeface(AppConst.font_regular(context));
            cbadvance= (CheckBox) findViewById(R.id.cbadvanced);
            cbadvance.setTypeface(AppConst.font_regular(context));
            tvstarprinter = (TextView)findViewById(R.id.tvstarprinter);
            layoutUsb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    allUsbDevices.performClick();
                }
            });

            layoutBt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    allBlueToothDevices.performClick();
                }
            });

            updateUi();
            //SELECTOR FOR BlueTooth DEVICES
            allBlueToothDevices = (Spinner) findViewById(R.id.blueToothDevices);
            allBlueToothDevices.setOnItemSelectedListener(new OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                    Log.d(internalClassName, "Click spinner onItemSelected 1 BT");
                    if (parent.getId() == R.id.blueToothDevices) {
                        Log.d(internalClassName, "Click BlueTooth Spinner");
                        if (blueToothDevicesMap.size() > 0) {
                            Log.d(internalClassName, "Click en device--XX.XX.XX.XX " + blueToothDevicesMap.get(pos).get("DeviceAddress").toString());
                            Globals.blueToothDeviceAdress = blueToothDevicesMap.get(pos).get
                                    ("DeviceAddress").toString();       // *  ave un global config
                        }
                    }
                    //other spinners. (not need)
                }

                public void onNothingSelected(AdapterView<?> parent) {
                    // Do nothing.
                }
            });
            //SELECTOR FOR USB DEVICES
            allUsbDevices = (Spinner) findViewById(R.id.usbDevices);
            allUsbDevices.setOnItemSelectedListener(new OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                    Log.d(internalClassName, "Click en onItemSelected 2 USB");
                    if (parent.getId() == R.id.usbDevices) {
                        Log.d(internalClassName, "Click on device>>> " + Globals.usbDeviceID);
                        if (uspDevicesMap.size() > 0) {
                            Log.d(internalClassName, "Click en device ID " + uspDevicesMap.get(pos).get("usbDeviceID").toString());
                            HashMap<String, String> mapa = uspDevicesMap.get(pos);

                            Globals.usbDeviceID = Globals.mathIntegerFromString(mapa.get("usbDeviceID").toString(), 0);
                            Globals.usbProductID = Globals.mathIntegerFromString(mapa.get("ProductID").toString(), 0);
                            Globals.usbVendorID = Globals.mathIntegerFromString(mapa.get("VendorID").toString(), 0);
                        }
                    }
                    //other spinners. (not need)
                }

                public void onNothingSelected(AdapterView<?> parent) {
                    // Do nothing.
                }
            });


            // we are goin to have three buttons for specific functions
            // send data typed by the user to be printed
            testPrinterButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Log.i(internalClassName, "testPrinterButton PRESSED");
                    updateNetCondigFronTextData();
                    Globals.savePreferences(sharedPrefs);
                    testPrint();

                }
            });

            btnEthOk.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Log.i(internalClassName, "btnEthOk PRESSED");
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);//(Hide keyboard)
                    imm.hideSoftInputFromWindow(deviceIP.getWindowToken(), 0);
                    updateNetCondigFronTextData();
                    Globals.savePreferences(sharedPrefs);
                    Intent intent = new Intent();
                    intent.putExtra(getString(R.string.title_target), Globals.deviceType);
                    intent.putExtra(getString(R.string.printername), "selected");
                    intent.putExtra(getString(R.string.DeviceType), deviceSelectEthernet.getText().toString());
                    setResult(RESULT_OK, intent);

                    finish();
                }
            });

            tvpapersize.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showdialog();
                }
            });

            tvstarprinter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SearchPortFragment.printer_type="bill";
                    Intent intent = new Intent(PrintMainActivity.this, CommonActivity.class);
                    intent.putExtra(CommonActivity.BUNDLE_KEY_ACTIVITY_LAYOUT, R.layout.activity_printer_search);
                    intent.putExtra(CommonActivity.BUNDLE_KEY_TOOLBAR_TITLE, "Search Port");
                    intent.putExtra(CommonActivity.BUNDLE_KEY_SHOW_HOME_BUTTON, true);
                    intent.putExtra(CommonActivity.BUNDLE_KEY_SHOW_RELOAD_BUTTON, true);
                    intent.putExtra(CommonActivity.BUNDLE_KEY_PRINTER_SETTING_INDEX, 0);    // Index of the backup printer

                    startActivityForResult(intent, PRINTER_SET_REQUEST_CODE);
                }
            });

            closeButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Log.i(internalClassName, "closeButton -> finish ACTIVITY");
                    finish();
                }
            });

            saveDataButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if(deviceSelectBT.isChecked())
                        cprinter=deviceSelectBT.getText().toString();
                    else if(deviceSelectEthernet.isChecked())
                        cprinter=deviceSelectEthernet.getText().toString();
                    else if(deviceSelectUsb.isChecked())
                        cprinter=deviceSelectUsb.getText().toString();
                    else if(rbsunmi.isChecked())
                        cprinter=rbsunmi.getText().toString();
                    else if(rbstar.isChecked()){
                        cprinter=rbstar.getText().toString();
                        M.setCashPrinterModel(tvstarprinter.getText().toString(),context);
                    }
                    else
                        cprinter="Select Printer";
                    Log.i(internalClassName, "saveDataButton -> Save data ");
                    updateNetCondigFronTextData();
                    Globals.savePreferences(sharedPrefs);
                    Intent intent = new Intent();
                    intent.putExtra(getString(R.string.title_target), Globals.deviceType);
                    intent.putExtra(getString(R.string.printername), "selected");
                    intent.putExtra(getString(R.string.DeviceType), cprinter);
                    setResult(RESULT_OK, intent);

                    finish();
               }
            });



        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        setDefaultData();

    }

    public void showdialog() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(context);
        builderSingle.setTitle("Select One Name:-");

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.select_dialog_singlechoice);
        arrayAdapter.add("58 mm");
        arrayAdapter.add("78 mm");
        arrayAdapter.add("79 mm");
        arrayAdapter.add("80 mm");

        builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strName = arrayAdapter.getItem(which);

                tvpapersize.setText(getString(R.string.paper_width)+strName);

                if(strName.equals("58 mm")){
                    M.saveVal(M.key_bill_width,PrintFormat.smallsize+"", context);
                }else if(strName.equals("79 mm")){
                    M.saveVal(M.key_bill_width,PrintFormat.size79+"", context);
                }else if(strName.equals("80 mm")){
                    M.saveVal(M.key_bill_width,PrintFormat.size80+"", context);
                }else {
                    M.saveVal(M.key_bill_width,PrintFormat.normalsize+"", context);
                }
            }
        });
        builderSingle.show();
    }

    @Override
    protected void onResume() {
        Log.i(internalClassName, "onResume");
        super.onResume();
//        checkPlayServices();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(internalClassName, "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(internalClassName, "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(internalClassName, "onDestroy");
        unregisterReceiver(mUsbDeviceReceiver);
    }


    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.deviceSelectEthernet:
                if (checked)
                    Globals.deviceType = 1;
                updateUi();
                break;
            case R.id.deviceSelectUsb:
                if (checked)
                    Globals.deviceType = 3;
                updateUi();
                break;
            case R.id.deviceSelectBT:
                if (checked)
                    Globals.deviceType = 4;
                refreshBlueToothDevices();
                updateUi();
                break;
            case R.id.rbSunmi:
                if(checked)
                    Globals.deviceType = 5;
                updateUi();
                break;
            case R.id.rbStar:
                if(checked)
                    Globals.deviceType = 9;
                updateUi();
                break;
        }
        setDefaultData();
        Globals.savePreferences(sharedPrefs);

    }

    public void testPrint() {
        Log.i(internalClassName, "testPrint");

        if(Globals.deviceType==5){
            String dataToPrint = "";
            dataToPrint = Globals.getSunmiDemoText(this);
            Log.e(internalClassName, "testPrint:" + dataToPrint);
            String root = Environment.getExternalStorageDirectory().toString();
            File myDir = new File(root + "/RoyalPOS/logo.png");
            if (myDir.exists()) {
                Bitmap myBitmap = BitmapFactory.decodeFile(myDir.getAbsolutePath());
                AidlUtil.getInstance().printBitmap(myBitmap);
            }
            AidlUtil.getInstance().printText(dataToPrint, 24, true,false);
        }else if(Globals.deviceType==9){

            Log.d("Star Printer---", "star printer---");
            Globals.getSunmiDemoText(this);

            byte[] data;

            PrinterSettingManager settingManager = new PrinterSettingManager(context);
            PrinterSettings settings       = settingManager.getPrinterSettings();


            StarIoExt.Emulation emulation = ModelCapability.getEmulation(settings.getModelIndex());
            data = createGenericData( Globals.getSunmiDemoText(this), emulation);

            Communication.sendCommands(this, data, settings.getPortName(),
                    settings.getPortSettings(), 10000, 30000,
                    context, mCallback);     // 10000mS!!!


        }else{
            String dataToPrint = "";

            dataToPrint = Globals.getDemoText(this.getResources(), this,M.retriveVal(M.key_bill_width,context));
            Intent sendIntent;
            if(M.isadvanceprint(M.key_bill,context) && Globals.deviceType<=4) {
                GlobalsNew.setReceipttype("5");
                sendIntent = new Intent(this, PrintActivityNew.class);
            }else{
                sendIntent = new Intent(this, PrintActivity.class);
            }
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, dataToPrint);
            sendIntent.putExtra("internal", "1");
            sendIntent.setType("text/plain");
            this.startActivity(sendIntent);
        }


    }

    public void updateNetCondigFronTextData() {
        Log.i(internalClassName, "updateNetCondigFronTextData");
        Globals.deviceIP = deviceIP.getText().toString();
        Globals.devicePort = Globals.mathIntegerFromString(devicePort.getText().toString(), 9100);
    }

    public void setDefaultData() {
        Log.i(internalClassName, "setDefaultData");
        cbadvance.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                M.setadvanceprint(M.key_bill,b, context);
            }
        });

        cbadvance.setChecked(M.isadvanceprint(M.key_bill,context));
        //Set option from config
        deviceSelectEthernet.setChecked(Globals.deviceType == 1);
        deviceSelectUsb.setChecked(Globals.deviceType == 3);
        deviceSelectBT.setChecked(Globals.deviceType == 4);
        rbsunmi.setChecked(Globals.deviceType == 5);
        rbstar.setChecked(Globals.deviceType==9);

        deviceIP.setEnabled(Globals.deviceType == 1);
        devicePort.setEnabled(Globals.deviceType == 1);

        deviceIP.setText(Globals.deviceIP);
        deviceIP.setSelection(deviceIP.getText().length());
        devicePort.setText(String.valueOf(Globals.devicePort));
        devicePort.setSelection(devicePort.getText().length());

        testPrinterButton.setEnabled(Globals.deviceType != 0);
        allUsbDevices.setEnabled(Globals.deviceType == 3);
        if (Globals.deviceType == 3) {
            ArrayAdapter<ObjectsClassData> spinner_adapter = new ArrayAdapter<ObjectsClassData>(this, R.layout.business_type_row,R.id.txt, usbDevicesItemscls);
           // spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            allUsbDevices.setAdapter(spinner_adapter);
        }

        allBlueToothDevices.setEnabled(Globals.deviceType == 4);
        if (Globals.deviceType == 4) {
            ArrayAdapter<ObjectsClassData> spinnerAdapterBT = new ArrayAdapter<ObjectsClassData>(this, R.layout.business_type_row,R.id.txt, blueToothDevicesItemscls);
          //  spinnerAdapterBT.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            Log.i(internalClassName, "BTSpinner");
            allBlueToothDevices.setAdapter(spinnerAdapterBT);
            if (Globals.blueToothSpinnerSelected != -1) {
                Log.i(internalClassName, "BTSpinner SET");
                allBlueToothDevices.setSelection(Globals.blueToothSpinnerSelected);
            }
        }

        if(Globals.deviceType==9){
            PrinterSettingManager settingManager = new PrinterSettingManager(context);
            PrinterSettings       settings       = settingManager.getPrinterSettings();
            if(settings!=null && settings.getModelName()!=null)
                tvstarprinter.setText(settings.getModelName());
            llstar.setVisibility(View.VISIBLE);
        }else
            llstar.setVisibility(View.GONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(internalClassName, "onActivityResult(" + requestCode + "," + resultCode + "," + data);
        if (requestCode == REQUEST_ENABLE_BT) {
            Log.i(internalClassName, "onActivityResult BlueTooth ENABLED OK> refresh Devices");
            refreshBlueToothDevices();
        } else if (requestCode == PRINTER_SET_REQUEST_CODE) {
            updateList();
        }

    }

    public void updateList(){
        PrinterSettingManager settingManager = new PrinterSettingManager(context);
        PrinterSettings settings       = settingManager.getPrinterSettings();

        boolean isDeviceSelected     = false;
        int     modelIndex           = ModelCapability.NONE;
        String  modelName            = "";
        boolean isBluetoothInterface = false;
        boolean isUsbInterface       = false;

        if (settings != null) {
            isDeviceSelected     = true;
            modelIndex           = settings.getModelIndex();
            modelName            = settings.getModelName();
            isBluetoothInterface = settings.getPortName().toUpperCase().startsWith("BT:");
            isUsbInterface       = settings.getPortName().toUpperCase().startsWith("USB:");
            tvstarprinter.setText(""+modelName);
        }


    }

    private final BroadcastReceiver mUsbDeviceReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i("BroadcastReceiver=", "onReceive 1 1");
            refreshUsbDevices();
        }
    };


    private boolean refreshBlueToothDevices() {
        Log.i(internalClassName, "refreshBlueToothDevices  ");

        boolean success = false;
        blueToothDevicesMap = new ArrayList<HashMap<String, String>>();
        try {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (mBluetoothAdapter == null) {
                success = false;
                internalInfo = internalInfo + "No bluetooth adapter available";
            }
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBluetooth, 0);
            }

            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
            HashMap<String, String> map = null;

            int position = 0;
            if (pairedDevices.size() > 0) {
                if(blueToothDevicesMap!=null)
                    blueToothDevicesMap.clear();
                if(blueToothDevicesItemscls!=null)
                    blueToothDevicesItemscls.clear();
                for (BluetoothDevice device : pairedDevices) {
                    Log.i(internalClassName, "refreshBlueToothDevices pairedDevices:: " + device.getAddress());
                    map = new HashMap<String, String>();
                    map.put("DeviceAddress", String.valueOf(device.getAddress()));
                    map.put("DeviceName", String.valueOf(device.getName()));
                    if (Globals.blueToothDeviceAdress.equals(String.valueOf(device.getAddress()))) {
                        Globals.blueToothSpinnerSelected = position;
                    }
                    map.put("posicion", String.valueOf(position));
                    blueToothDevicesMap.add(map);
                    blueToothDevicesItemscls.add(new ObjectsClassData(position, device.getAddress() + " " + String.valueOf(device.getName())));
                    success = true;
                    position = position + 1;
                }
                updateUi();
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
            success = false;
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }
        return success;
    }

    private final Communication.SendCallback mCallback = new Communication.SendCallback() {
        @Override
        public void onStatus(Communication.CommunicationResult communicationResult) {


        }
    };

    public static byte[] createGenericData(String str, StarIoExt.Emulation emulation) {
        byte[] data = str.getBytes();

        ICommandBuilder builder = StarIoExt.createCommandBuilder(emulation);

        builder.beginDocument();

        builder.append(data);
        builder.append((byte) 0x0a);

        builder.appendCutPaper(ICommandBuilder.CutPaperAction.PartialCutWithFeed);
        builder.appendPeripheral(ICommandBuilder.PeripheralChannel.No1);
        builder.appendPeripheral(ICommandBuilder.PeripheralChannel.No2);
        builder.endDocument();

        return builder.getCommands();
    }

    private void refreshUsbDevices() {
        Log.i(internalClassName, "refreshUsbDevices");

        uspDevicesMap = new ArrayList<HashMap<String, String>>();

        UsbManager manager = (UsbManager) getSystemService(Context.USB_SERVICE);
        HashMap<String, UsbDevice> deviceList = manager.getDeviceList();
        Iterator<UsbDevice> deviceIterator = deviceList.values().iterator();
        HashMap<String, String> map = null;
        usbDevicesItemscls = new LinkedList<ObjectsClassData>();
        List<String> usbDevicesItems = new ArrayList<String>();
        int position = 0;

        while (deviceIterator.hasNext()) {
            position++;

            UsbDevice device = deviceIterator.next();
            Log.i(internalClassName, "VendorId=" + device.getVendorId() + " ProductId=" + device.getProductId() + " DeviceName=" + device.getDeviceName());
            map = new HashMap<String, String>();
            map.put("usbDeviceID", String.valueOf(device.getDeviceId()));
            map.put("DeviceName", String.valueOf(device.getDeviceName()));
            map.put("DeviceClass", String.valueOf(device.getDeviceClass()));
            map.put("DeviceSubclass", String.valueOf(device.getDeviceSubclass()));
            map.put("VendorID", String.valueOf(device.getVendorId()));
            map.put("ProductID", String.valueOf(device.getProductId()));
            map.put("InterfaceCount", String.valueOf(device.getInterfaceCount()));
            map.put("posicion", String.valueOf(-1));
            uspDevicesMap.add(map);
            usbDevicesItems.add(String.valueOf(device.getDeviceId()));
            usbDevicesItemscls.add(new ObjectsClassData(position, String.valueOf(device.getDeviceId()) + " " + device.getDeviceName()));
        }
    }

    public class ObjectsClassData {
        int id;
        String name;

        //Constructor
        public ObjectsClassData(int id, String name) {
            super();
            this.id = id;
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }

        public int getId() {
            return id;
        }
    }

    // updates UI to reflect model
    public void updateUi() {

        layoutNet.setVisibility(View.GONE);
        layoutUsb.setVisibility(View.GONE);
        layoutBt.setVisibility(View.GONE);
        llstar.setVisibility(View.GONE);
        if (Globals.deviceType == 1) layoutNet.setVisibility(View.VISIBLE);
        if (Globals.deviceType == 3) layoutUsb.setVisibility(View.VISIBLE);
        if (Globals.deviceType == 4) layoutBt.setVisibility(View.VISIBLE);

        if(M.retriveVal(M.key_bill_width,context)!=null && M.retriveVal(M.key_bill_width,context).equals(PrintFormat.smallsize+"")) {
            tvpapersize.setText(R.string.paper_width_58);
        }else if(M.retriveVal(M.key_bill_width,context)!=null && M.retriveVal(M.key_bill_width,context).equals(PrintFormat.size79+"")){
            tvpapersize.setText(R.string.paper_width_79);
        }else if(M.retriveVal(M.key_bill_width,context)!=null && M.retriveVal(M.key_bill_width,context).equals(PrintFormat.normalsize+"")){
            tvpapersize.setText(R.string.paper_width_78);
        }else{
            tvpapersize.setText(R.string.paper_width_80);
        }
        if(Globals.deviceType==9){
            llstar.setVisibility(View.VISIBLE);
            if(M.getCashPrinterModel(context)!=null)
                tvstarprinter.setText(M.getCashPrinterModel(context));
        }else
            llstar.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }
}