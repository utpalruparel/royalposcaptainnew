package com.royalpos.waiter.print.newbluetooth;

public interface AsyncPrintNewCallBack {


        void statusChange();
        void printErrorResult(String output);
        void printResult(String output);


}
