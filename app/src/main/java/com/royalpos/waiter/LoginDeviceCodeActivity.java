package com.royalpos.waiter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;

import android.text.Html;
import android.text.InputType;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.royalpos.waiter.helper.AppConst;
import com.royalpos.waiter.helper.ConnectionDetector;
import com.royalpos.waiter.model.DeviceCodePojo;
import com.royalpos.waiter.database.DBCombo;
import com.royalpos.waiter.database.DBCusines;
import com.royalpos.waiter.database.DBDiscount;
import com.royalpos.waiter.database.DBDishes;
import com.royalpos.waiter.database.DBModifier;
import com.royalpos.waiter.database.DBPaymentType;
import com.royalpos.waiter.database.DBPreferences;
import com.royalpos.waiter.database.DBUnit;
import com.royalpos.waiter.model.ComboModel;
import com.royalpos.waiter.model.CuisineListPojo;
import com.royalpos.waiter.model.DiscountPojo;
import com.royalpos.waiter.model.DishPojo;
import com.royalpos.waiter.model.M;
import com.royalpos.waiter.model.Modifier_cat;
import com.royalpos.waiter.model.PaymentModePojo;
import com.royalpos.waiter.model.PreModel;
import com.royalpos.waiter.model.RoundingPojo;
import com.royalpos.waiter.model.SuccessPojo;
import com.royalpos.waiter.model.UnitPojo;
import com.royalpos.waiter.webservices.APIServiceHeader;
import com.royalpos.waiter.webservices.APIServiceN;
import com.royalpos.waiter.webservices.AuthenticationAPI;
import com.royalpos.waiter.webservices.CuisineDishAPI;
import com.royalpos.waiter.webservices.OrderAPI;
import com.royalpos.waiter.webservices.WaiterAPI;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class LoginDeviceCodeActivity extends AppCompatActivity {

    TextInputLayout txt1;
    EditText etcode;
    Button btnlogin;
    Context context;
    ConnectionDetector connectionDetector;
    String TAG="LoginDeviceCodeActivity";
    Dialog dialog;
    String restaurantid,runiqueid,code;
    DeviceCodePojo loginModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_device_code);

        context=LoginDeviceCodeActivity.this;
        connectionDetector=new ConnectionDetector(context);

        TextView tvheading=(TextView)findViewById(R.id.tvheading);
        tvheading.setText(R.string.activity_title_login);
        txt1=(TextInputLayout)findViewById(R.id.txt1);
        txt1.setTypeface(AppConst.font_regular(context));
        etcode=(EditText)findViewById(R.id.etcode);
        etcode.setTypeface(AppConst.font_regular(context));
        etcode.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
        btnlogin=(Button)findViewById(R.id.btnsignin);
        btnlogin.setTypeface(AppConst.font_regular(context));

        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(etcode.getText().toString().trim().length()==0)
                    etcode.setError(getString(R.string.empty_device_code));
                else if(etcode.getText().toString().trim().length()!=6)
                    etcode.setError(getString(R.string.error_device_code_validation));
                else{
                    login();
                }
            }
        });
    }

    private void login() {
        if(connectionDetector.isConnectingToInternet()) {
            String deviceid= Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
            code=etcode.getText().toString().toUpperCase();
            M.showLoadingDialog(this);
            AuthenticationAPI mAuthenticationAPI = APIServiceN.createService(context, AuthenticationAPI.class);
            Call<DeviceCodePojo> call = mAuthenticationAPI.deviceCodeLogin(code,deviceid,"android");
            call.enqueue(new retrofit2.Callback<DeviceCodePojo>() {
                @Override
                public void onResponse(Call<DeviceCodePojo> call, Response<DeviceCodePojo> response) {
                    if (response.isSuccessful()) {
                        loginModel = response.body();
                        if (loginModel != null) {
                            if(loginModel.getSuccess()==1) {
                                M.setDeviceCode(code,context);
                                M.setAdminId("", context);
                                M.setDecimalVal(loginModel.getRestaurant().getDecimal_value(), context);
                                AppConst.currency = loginModel.getRestaurant().getCurrency();
                                M.setRestID(loginModel.getRestaurant().getId() + "", context);
                                restaurantid = M.getRestID(context);
                                M.setRestName(loginModel.getRestaurant().getName(), context);
                                M.setRestUserName(loginModel.getUsername(), context);
                                M.setRestAddress(loginModel.getRestaurant().getAddress(), context);
                                M.setRestPhoneNumber(loginModel.getRestaurant().getPhno(), context);
                                M.setRestImg(loginModel.getRestaurant().getLogo(), context);
                                M.setRestUniqueID(loginModel.getRestaurant().getRest_unique_id(), context);
                                runiqueid = M.getRestUniqueID(context);
                                M.setGST(loginModel.getRestaurant().getGst_no(), context);
                                M.setCurrency(loginModel.getRestaurant().getCurrency(), context);
                                M.setCashDrawer(loginModel.getRestaurant().getCash_drawer(), context);
                                M.setReportingemails(loginModel.getRestaurant().getReporting_emails(), context);
                                M.setBrandName(loginModel.getRestaurant().getBrand_name(), context);
                                M.setBrandId(loginModel.getRestaurant().getBrand_user_id(), context);
                                M.setTrackInventory(loginModel.getRestaurant().getRecipe_inventory(), context);
                                M.setDeduction(loginModel.getRestaurant().getStart_stock_deduct(), context);
                                M.setType(loginModel.getRestaurant().getType_of_business(), context);
                                M.setPlanid(loginModel.getRestaurant().getPlan_id() + "", context);
                                M.setfooterphone(loginModel.getRestaurant().getRecipt_footer_phone(), context);
                                M.setExpiry(loginModel.getRestaurant().getMembership_end_date(), context);
                                M.setPackcharge(loginModel.getRestaurant().getPackaging_charge(), context);
                                M.setPointsAmt(loginModel.getRestaurant().getPoints_per_one_currency(), context);

                                M.setChangePrice(Boolean.parseBoolean(loginModel.getRestaurant().getCan_change_price()), context);
                                if (loginModel.getRestaurant().getShow_item_price_without_tax() != null) {
                                    M.setPriceWT(Boolean.parseBoolean(loginModel.getRestaurant().getShow_item_price_without_tax()), context);
                                }
                                if(!loginModel.getRestaurant().getService_charges().equals("disable"))
                                    M.setServiceCharge(true,context);
                                else
                                    M.setServiceCharge(false,context);
                                if (loginModel.getRestaurant().getIs_rounding_on() != null && loginModel.getRestaurant().getIs_rounding_on().equals("on") &&
                                        loginModel.getRestaurant().getRounding_id() != null && !loginModel.getRestaurant().getRounding_id().equals("0")) {
                                    try {
                                        JSONObject jsonObject = new JSONObject();
                                        jsonObject.put("val_interval", loginModel.getRestaurant().getInterval_number());
                                        jsonObject.put("rule_tag", loginModel.getRestaurant().getRounding_tag());
                                        jsonObject.put("rounding_id", loginModel.getRestaurant().getRounding_id());
                                        M.setRoundBoundary(jsonObject + "", context);
                                        M.setRoundFormat(true, context);
                                    } catch (JSONException e) {
                                    }
                                } else
                                    M.setRoundFormat(false, context);
                                M.setOutletPin(loginModel.getRestaurant().getPin(), context);

                                if (loginModel.getRestaurant().getCheck_pin_admin() != null &&
                                        loginModel.getRestaurant().getCheck_pin_admin().equals("true"))
                                    M.setPin(true, context);
                                else
                                    M.setPin(false, context);

                                if (loginModel.getRestaurant().getCheck_pin_cashier() != null &&
                                        loginModel.getRestaurant().getCheck_pin_cashier().equals("true"))
                                    M.setcashierPin(true, context);
                                else
                                    M.setcashierPin(false, context);

                                M.hideLoadingDialog();
                                M.setWaiterid(loginModel.getMember_id() + "", context);
                                M.setWaitername(loginModel.getMember_username(), context);
                                M.setWaiterPin(loginModel.getPin(), context);
                                if(loginModel.getRestaurant().getAllow_item_wise_discount()!=null && loginModel.getRestaurant().getAllow_item_wise_discount().equals("enable"))
                                    M.setItemDiscount(true,context);
                                else
                                    M.setItemDiscount(false,context);
                                if (loginModel.getAllow_payment() != null)
                                    M.setCashierPermission(Boolean.parseBoolean(loginModel.getAllow_payment()), context);
                                M.setToken(loginModel.getLast_order_no(), context);
                                M.saveVal(M.key_tip_cal,loginModel.getRestaurant().getTip_calculation(),context);
                                M.saveVal(M.key_tip_pref,loginModel.getRestaurant().getTip_calculation_preference(),context);
                                if (M.getCashDrawer(context) != null && M.getCashDrawer(context).equals("on") &&
                                        loginModel.getDaily_balance_addded() != null && loginModel.getDaily_balance_addded().equals("false")) {
                                    final Dialog dialog = new Dialog(context);
                                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                    dialog.setContentView(R.layout.dialog_opening_bal);
                                    dialog.setCancelable(false);
                                    final EditText etbal = (EditText) dialog.findViewById(R.id.etopeningbal);
                                    ImageView ivclose = (ImageView) dialog.findViewById(R.id.ivclose);
                                    Button btn = (Button) dialog.findViewById(R.id.btnsubmit);
                                    etbal.setText("0");
                                    etbal.setSelection(etbal.getText().toString().length());
                                    btn.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            if (etbal.getText().toString().trim().length() <= 0)
                                                etbal.setError(context.getString(R.string.empty_balance));
                                            else {
                                                String bal = etbal.getText().toString();
                                                dialog.dismiss();
                                                updateBal(bal, context);
                                            }
                                        }
                                    });

                                    ivclose.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            dialog.dismiss();
                                        }
                                    });
                                    dialog.show();
                                } else {
                                    M.hideLoadingDialog();
                                    String daily_balance_id = loginModel.getBalance_id() + "";
                                    M.setdaily_balance_id(daily_balance_id, context);
                                    dialog = new Dialog(context, android.R.style.Theme_DeviceDefault_NoActionBar_Fullscreen);
                                    dialog.setContentView(R.layout.dialog_progress);
                                    ImageView ivtopic=(ImageView)dialog.findViewById(R.id.ivtopic);
                                    TextView tvtopic=(TextView)dialog.findViewById(R.id.tvtopic);
                                    TextView tvcat=(TextView)dialog.findViewById(R.id.tvcat);
                                    TextView tvtxt=(TextView)dialog.findViewById(R.id.tvtxt);
                                    LinearLayout lltopic=(LinearLayout)dialog.findViewById(R.id.lltopic);
                                    lltopic.setVisibility(View.GONE);
                                    AppConst.animation(context.getString(R.string.progress_dialog_txt), tvtxt,context);
                                    if(M.getTopic(context)!=null) {
                                        try {
                                            JSONObject j=new JSONObject(M.getTopic(context));
                                            if (j != null) {
                                                if (j.has("text") && !j.getString("text").isEmpty()) {
                                                    tvtopic.setText(Html.fromHtml(j.getString("text")));
                                                    tvtopic.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
                                                    lltopic.setVisibility(View.VISIBLE);
                                                    tvtxt.setVisibility(View.VISIBLE);
                                                }
                                                if (j.has("cat") && !j.getString("cat").isEmpty()) {
                                                    tvcat.setText(j.getString("cat"));
                                                    tvcat.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
                                                    lltopic.setVisibility(View.VISIBLE);
                                                    tvtxt.setVisibility(View.VISIBLE);
                                                }
                                                if (j.has("img") && !j.getString("img").isEmpty()) {
                                                    Picasso.get()
                                                            .load(AppConst.topic_img_url + j.getString("img"))
                                                            .into(ivtopic);
                                                    ivtopic.setVisibility(View.VISIBLE);
                                                    lltopic.setVisibility(View.VISIBLE);
                                                    tvtxt.setVisibility(View.VISIBLE);
                                                }
                                            }
                                        }catch (JSONException e){}
                                    }
                                    dialog.show();
                                    getCuisine();
                                }
                            }else if(loginModel.getMessage()!=null && loginModel.getMessage().trim().length()>0){
                                M.hideLoadingDialog();
                                Toast.makeText(context,loginModel.getMessage(),Toast.LENGTH_SHORT).show();
                            }
                        }

                    } else {
                        M.hideLoadingDialog();
                        int statusCode = response.code();
                        ResponseBody errorBody = response.errorBody();
                        Log.d(TAG, "error:" + statusCode + " " + errorBody);
                    }
                }

                @Override
                public void onFailure(Call<DeviceCodePojo> call, Throwable t) {
                    Log.d("response:", "fail:" + t.getMessage());
                    M.hideLoadingDialog();
                    // handle execution failures like no internet connectivity
                }
            });
        }else {
            M.showToast(context, getString(R.string.no_internet_error));
        }
    }

    private void updateBal(final String bal, final Context context) {
        if(connectionDetector.isConnectingToInternet()) {
            M.showLoadingDialog(context);
            String restaurantid = M.getRestID(context);
            String runiqueid = M.getRestUniqueID(context);
            WaiterAPI mAuthenticationAPI = APIServiceHeader.createService(context, WaiterAPI.class);
            Call<SuccessPojo> call = mAuthenticationAPI.updateBalance(restaurantid, runiqueid, "opening", M.getWaiterid(context), bal, "",null,null,null);
            call.enqueue(new retrofit2.Callback<SuccessPojo>() {
                @Override
                public void onResponse(Call<SuccessPojo> call, Response<SuccessPojo> response) {
                    if (response.isSuccessful()) {
                        SuccessPojo pojo = response.body();
                        M.hideLoadingDialog();
                        if (pojo != null) {
                            if (pojo.getSuccess() == 1) {

                                String daily_balance_id = pojo.getDaily_balance_id();
                                M.setdaily_balance_id(daily_balance_id, context);
                                dialog = new Dialog(context, android.R.style.Theme_DeviceDefault_NoActionBar_Fullscreen);
                                dialog.setContentView(R.layout.dialog_progress);
                                ImageView ivtopic=(ImageView)dialog.findViewById(R.id.ivtopic);
                                TextView tvtopic=(TextView)dialog.findViewById(R.id.tvtopic);
                                TextView tvcat=(TextView)dialog.findViewById(R.id.tvcat);
                                TextView tvtxt=(TextView)dialog.findViewById(R.id.tvtxt);
                                LinearLayout lltopic=(LinearLayout)dialog.findViewById(R.id.lltopic);
                                lltopic.setVisibility(View.GONE);
                                AppConst.animation(context.getString(R.string.progress_dialog_txt), tvtxt,context);
                                if(M.getTopic(context)!=null) {
                                    try {
                                        JSONObject j=new JSONObject(M.getTopic(context));
                                        if (j != null) {
                                            if (j.has("text") && !j.getString("text").isEmpty()) {
                                                tvtopic.setText(Html.fromHtml(j.getString("text")));
                                                tvtopic.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
                                                lltopic.setVisibility(View.VISIBLE);
                                                tvtxt.setVisibility(View.VISIBLE);
                                            }
                                            if (j.has("cat") && !j.getString("cat").isEmpty()) {
                                                tvcat.setText(j.getString("cat"));
                                                tvcat.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
                                                lltopic.setVisibility(View.VISIBLE);
                                                tvtxt.setVisibility(View.VISIBLE);
                                            }
                                            if (j.has("img") && !j.getString("img").isEmpty()) {
                                                Picasso.get()
                                                        .load(AppConst.topic_img_url + j.getString("img"))
                                                        .into(ivtopic);
                                                ivtopic.setVisibility(View.VISIBLE);
                                                lltopic.setVisibility(View.VISIBLE);
                                                tvtxt.setVisibility(View.VISIBLE);
                                            }
                                        }
                                    }catch (JSONException e){}
                                }
                                dialog.show();
                                getCuisine();
                            }
                        }
                    } else {
                        int statusCode = response.code();
                        ResponseBody errorBody = response.errorBody();
                        Log.d(TAG, "error:" + statusCode + " " + errorBody);
                        M.hideLoadingDialog();
                    }
                }

                @Override
                public void onFailure(Call<SuccessPojo> call, Throwable t) {
                    M.hideLoadingDialog();
                    Log.d(TAG, "fail:" + t.getMessage());
                }
            });
        }else
            M.showToast(context,context.getString(R.string.no_internet_error));
    }

    private void getCuisine() {
        if(connectionDetector.isConnectingToInternet()) {
            CuisineDishAPI mAuthenticationAPI = APIServiceHeader.createService(context, CuisineDishAPI.class);
            Call<List<CuisineListPojo>> call = mAuthenticationAPI.getCuisines(restaurantid, runiqueid);
            call.enqueue(new retrofit2.Callback<List<CuisineListPojo>>() {
                @Override
                public void onResponse(Call<List<CuisineListPojo>> call, Response<List<CuisineListPojo>> response) {
                    if (response.isSuccessful()) {
                        List<CuisineListPojo> custCuisineListPojos = response.body();
                        if (custCuisineListPojos != null) {
                            DBCusines db = new DBCusines(context);
                            db.deleteallcuisine();
                            for (CuisineListPojo pojo : custCuisineListPojos) {
                                db.addCusines(pojo);
                            }
                        }
                        String tm=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
                        Log.d(TAG,"timing:"+tm);
                        M.setUpdateTime(tm,context);
                        getAllDishes();
                    } else {
                        int statusCode = response.code();
                        ResponseBody errorBody = response.errorBody();
                        Log.d(TAG, "error:" + statusCode + " " + errorBody);
                        if(dialog!=null && dialog.isShowing())
                            dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<List<CuisineListPojo>> call, Throwable t) {
                    if(dialog!=null && dialog.isShowing())
                        dialog.dismiss();
                    Log.d(TAG, "cuisine fail:" + t.getMessage());
                }
            });
        }else {
            Toast.makeText(context, R.string.no_internet_error, Toast.LENGTH_SHORT).show();
            if(dialog!=null && dialog.isShowing())
                dialog.dismiss();
        }
    }

    private void getAllDishes() {
        if(connectionDetector.isConnectingToInternet()){
            CuisineDishAPI mAuthenticationAPI = APIServiceHeader.createService(context, CuisineDishAPI.class);
            Call<List<DishPojo>> call = mAuthenticationAPI.getDishes(restaurantid, runiqueid);
            call.enqueue(new retrofit2.Callback<List<DishPojo>>() {
                @Override
                public void onResponse(Call<List<DishPojo>> call, Response<List<DishPojo>> response) {
                    if (response.isSuccessful()) {
                        List<DishPojo> dishiesPojos = response.body();
                        DBDishes db = new DBDishes(context);
                        db.deleteallcuisine();
                        DBPreferences dbp=new DBPreferences(context);
                        dbp.deleteAllPre();
                        DBModifier dbm=new DBModifier(context);
                        dbm.deleteAllModi();
                        DBCombo dbCombo = new DBCombo(context);
                        dbCombo.deleteAllPre();
                        if (dishiesPojos != null) {
                            for (DishPojo dishesdata : dishiesPojos) {
                                String dishid = dishesdata.getDishid();
                                db.addDishes(dishesdata);
                                if(dishesdata.getPreflag().equals("true") && dishesdata.getPre()!=null) {
                                    for (PreModel data : dishesdata.getPre()) {
                                        dbp.addPref(data, dishid);
                                    }
                                    for(Modifier_cat mdata:dishesdata.getModifier_cat()){
                                        dbm.check(mdata,dishid);
                                    }
                                }

                                if(dishesdata.getCombo_flag()!=null) {
                                    if (dishesdata.getCombo_flag().equals("true") && dishesdata.getCombo_item() != null) {
                                        for (ComboModel combo : dishesdata.getCombo_item()) {
                                            dbCombo.addcombo(combo, dishid);
                                        }

                                    }
                                }

                            }
                        }
                        String tm=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
                        Log.d(TAG,"timing:"+tm);
                        M.setUpdateTime(tm,context);
                        getRoundingInfo();
                    } else {
                        int statusCode = response.code();
                        ResponseBody errorBody = response.errorBody();
                        Log.d(TAG, "error:" + statusCode + " " + errorBody);
                        if(dialog!=null && dialog.isShowing())
                            dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<List<DishPojo>> call, Throwable t) {
                    Log.d(TAG, "dish fail:" + t.getMessage());
                    if(dialog!=null && dialog.isShowing())
                        dialog.dismiss();
                }
            });
        }else {
            Toast.makeText(context, R.string.no_internet_error, Toast.LENGTH_SHORT).show();
            if(dialog!=null && dialog.isShowing())
                dialog.dismiss();
        }
    }

    void getRoundingInfo() {
        if(connectionDetector.isConnectingToInternet()){
            OrderAPI mAuthenticationAPI = APIServiceHeader.createService(context, OrderAPI.class);
            Call<RoundingPojo> call = mAuthenticationAPI.getRoundingInfo(restaurantid,runiqueid,M.getWaiterid(context));
            call.enqueue(new retrofit2.Callback<RoundingPojo>() {
                @Override
                public void onResponse(Call<RoundingPojo> call, Response<RoundingPojo> response) {
                    if (response.isSuccessful()) {
                        RoundingPojo resp = response.body();
                        if(resp!=null){
                            if(resp.getIs_rounding_on().equals("on") &&
                                    resp.getRounding_id()!=null && !resp.getRounding_id().equals("0")){
                                try {
                                    JSONObject jsonObject=new JSONObject();
                                    jsonObject.put("val_interval",resp.getInterval_number());
                                    jsonObject.put("rule_tag",resp.getRounding_tag());
                                    jsonObject.put("rounding_id",resp.getRounding_id());
                                    M.setRoundBoundary(jsonObject+"",context);
                                    M.setRoundFormat(true,context);
                                }catch (JSONException e){}
                            }else
                                M.setRoundFormat(false,context);
                            if(resp.getShow_item_price_without_tax()!=null){
                                M.setPriceWT(Boolean.parseBoolean(resp.getShow_item_price_without_tax()),context);
                            }
                            if(resp.getAllow_payment()!=null)
                                M.setCashierPermission(Boolean.parseBoolean(resp.getAllow_payment()),context);
                            M.setOutletPin(resp.getPin(),context);
                            M.setPointsAmt(resp.getPoints_per_one_currency(),context);

                            if (resp.getCheck_pin_admin() != null &&
                                    resp.getCheck_pin_admin().equals("true"))
                                M.setPin(true, context);
                            else
                                M.setPin(false, context);

                            if (resp.getCheck_pin_cashier() != null &&
                                    resp.getCheck_pin_cashier().equals("true"))
                                M.setcashierPin(true, context);
                            else
                                M.setcashierPin(false, context);
                            if(resp.getAllow_item_wise_discount()!=null && resp.getAllow_item_wise_discount().equals("enable"))
                                M.setItemDiscount(true,context);
                            else
                                M.setItemDiscount(false,context);
                            if(!resp.getService_charges().equals("disable"))
                                M.setServiceCharge(true,context);
                            else
                                M.setServiceCharge(false,context);
                            M.saveVal(M.key_tip_cal,resp.getTip_calculation(),context);
                            M.saveVal(M.key_tip_pref,resp.getTip_calculation_preference(),context);
                            M.saveVal(M.key_phonepe_status,resp.getPhonepe_enable(),context);
                            M.saveVal(M.key_phonepe_paymode,resp.getPhonepe_mapped_pay_mode(),context);
                            M.saveVal(M.key_phonepe_paymode_qrcode,resp.getPhonepe_upi_dynamic_qrcode(),context);
                        }

                        String tm=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
                        Log.d(TAG,"timing:"+tm);
                        M.setUpdateTime(tm,context);
                        getPaymentMode();
                    } else {
                        int statusCode = response.code();
                        ResponseBody errorBody = response.errorBody();
                        Log.d(TAG, "error:" + statusCode + " " + errorBody);
                        if(dialog!=null && dialog.isShowing())
                            dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<RoundingPojo> call, Throwable t) {
                    Log.d(TAG, "payment fail:" + t.getMessage());
                    if(dialog!=null && dialog.isShowing())
                        dialog.dismiss();
                }
            });
        }else {
            Toast.makeText(context, R.string.no_internet_error, Toast.LENGTH_SHORT).show();
            if(dialog!=null && dialog.isShowing())
                dialog.dismiss();
        }
    }

    void getPaymentMode() {
        if(connectionDetector.isConnectingToInternet()){
            OrderAPI mAuthenticationAPI = APIServiceHeader.createService(context, OrderAPI.class);
            Call<List<PaymentModePojo>> call = mAuthenticationAPI.getPaymentMode(restaurantid,runiqueid);
            call.enqueue(new retrofit2.Callback<List<PaymentModePojo>>() {
                @Override
                public void onResponse(Call<List<PaymentModePojo>> call, Response<List<PaymentModePojo>> response) {
                    if (response.isSuccessful()) {
                        List<PaymentModePojo> resp = response.body();
                        if(resp!=null){
                            DBPaymentType dbPaymentType=new DBPaymentType(context);
                            dbPaymentType.deletealltype();
                            for (PaymentModePojo p:resp){
                                dbPaymentType.addType(p);
                            }
                        }
                        String tm=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
                        Log.d(TAG,"timing:"+tm);
                        M.setUpdateTime(tm,context);
                        getUnits();
                    } else {
                        int statusCode = response.code();
                        ResponseBody errorBody = response.errorBody();
                        Log.d(TAG, "error:" + statusCode + " " + errorBody);
                        if(dialog!=null && dialog.isShowing())
                            dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<List<PaymentModePojo>> call, Throwable t) {
                    Log.d(TAG, "payment fail:" + t.getMessage());
                    if(dialog!=null && dialog.isShowing())
                        dialog.dismiss();
                }
            });
        }else {
            Toast.makeText(context, R.string.no_internet_error, Toast.LENGTH_SHORT).show();
            if(dialog!=null && dialog.isShowing())
                dialog.dismiss();
        }
    }

    void getUnits() {
        if(connectionDetector.isConnectingToInternet()){
            CuisineDishAPI mAuthenticationAPI = APIServiceHeader.createService(context, CuisineDishAPI.class);
            Call<List<UnitPojo>> call = mAuthenticationAPI.getUnitList(restaurantid,runiqueid);
            call.enqueue(new retrofit2.Callback<List<UnitPojo>>() {
                @Override
                public void onResponse(Call<List<UnitPojo>> call, Response<List<UnitPojo>> response) {
                    if (response.isSuccessful()) {
                        List<UnitPojo> resp = response.body();
                        if(resp!=null){
                            DBUnit dbUnit=new DBUnit(context);
                            dbUnit.deleteall();
                            for (UnitPojo p:resp){
                                dbUnit.addUnit(p);
                            }
                        }
                        String tm=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
                        Log.d(TAG,"timing:"+tm);
                        M.setUpdateTime(tm,context);
                        getDiscount();
                    } else {
                        int statusCode = response.code();
                        ResponseBody errorBody = response.errorBody();
                        Log.d(TAG, "error:" + statusCode + " " + errorBody);
                        if(dialog!=null && dialog.isShowing())
                            dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<List<UnitPojo>> call, Throwable t) {
                    Log.d(TAG, "payment fail:" + t.getMessage());
                    if(dialog!=null && dialog.isShowing())
                        dialog.dismiss();
                }
            });
        }else {
            Toast.makeText(context, R.string.no_internet_error, Toast.LENGTH_SHORT).show();
            if(dialog!=null && dialog.isShowing())
                dialog.dismiss();
        }
    }

    void getDiscount() {
        if(connectionDetector.isConnectingToInternet()){
            CuisineDishAPI mAuthenticationAPI = APIServiceHeader.createService(context, CuisineDishAPI.class);
            Call<List<DiscountPojo>> call = mAuthenticationAPI.billOffer(restaurantid,runiqueid);
            call.enqueue(new retrofit2.Callback<List<DiscountPojo>>() {
                @Override
                public void onResponse(Call<List<DiscountPojo>> call, Response<List<DiscountPojo>> response) {
                    if (response.isSuccessful()) {
                        List<DiscountPojo> resp = response.body();
                        if (resp != null) {
                            DBDiscount dbDiscount=new DBDiscount(context);
                            dbDiscount.deleteall();
                            for(DiscountPojo d:resp){
                                dbDiscount.addDiscount(d);
                            }
                        }
                        String tm=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
                        Log.d(TAG,"timing:"+tm);
                        M.setUpdateTime(tm,context);

                            M.waiterLogin(true, context);

                            if(AppConst.dishorederlist!=null)
                                AppConst.dishorederlist.clear();
                            if(AppConst.selidlist!=null)
                                AppConst.selidlist.clear();
                            if(dialog!=null && dialog.isShowing())
                                dialog.dismiss();
                            redirectScreen();
                    } else {
                        int statusCode = response.code();
                        ResponseBody errorBody = response.errorBody();
                        Log.d(TAG, "error:" + statusCode + " " + errorBody);
                        if(dialog!=null && dialog.isShowing())
                            dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<List<DiscountPojo>> call, Throwable t) {
                    Log.d(TAG, "discount fail:" + t.getMessage());
                    if(dialog!=null && dialog.isShowing())
                        dialog.dismiss();
                }
            });
        }else {
            Toast.makeText(context, R.string.no_internet_error, Toast.LENGTH_SHORT).show();
            if(dialog!=null && dialog.isShowing())
                dialog.dismiss();
        }
    }

    private void redirectScreen(){
        if(dialog!=null && dialog.isShowing())
            dialog.dismiss();
        Intent it = new Intent(context, WaiterActivity.class);
        finish();
        context.startActivity(it);
    }

}
