package com.royalpos.waiter.adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.royalpos.waiter.R;
import com.royalpos.waiter.database.DBPrinter;
import com.royalpos.waiter.helper.AppConst;
import com.royalpos.waiter.model.CuisineListPojo;
import com.royalpos.waiter.model.KitchenPrinterPojo;
import com.royalpos.waiter.print.KPCategoryMainActivity;
import com.royalpos.waiter.print.KitchencategoryActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.greenrobot.event.EventBus;

public class CatPrinterAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_ITEM = 1;
    private List<KitchenPrinterPojo> list;
    Context context;
    String TAG="CatPrinterAdapter";

    public CatPrinterAdapter(List<KitchenPrinterPojo> list, Context mcontext) {
        context = mcontext;
        this.list = list;
    }

    @Override
    public int getItemViewType(int position) {
        return TYPE_ITEM;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.printer_row, parent, false);
        return new ViewHolderPosts(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        final KitchenPrinterPojo model=list.get(position);
        final ViewHolderPosts itemview = (ViewHolderPosts) holder;

        itemview.tvprinterno.setText(model.getPrinter_title());
        if(model.isAdv()){
            itemview.tvprinterno.setText(model.getPrinter_title()+" "+context.getString(R.string.txt_advance));
        }
        if(model.getDeviceType().equals("1"))
            itemview.tvprinternm.setText(model.getPrinter_name()+"("+model.getDeviceIP()+")");
        else
            itemview.tvprinternm.setText(model.getPrinter_name());
        itemview.tvcat.setText(model.getCategorynm());

        itemview.tvedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editPrinterGroup(position);
            }
        });

        itemview.llprinter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                KPCategoryMainActivity.kpPojo=model;
                Intent it = new Intent(context, KPCategoryMainActivity.class);
                it.putExtra("category", model.getCategoryid());
                ((KitchencategoryActivity)context).startActivityForResult(it, 201);
            }
        });

        itemview.tvdelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(context)
                        .setTitle(R.string.txt_are_sure)
                        .setMessage(context.getString(R.string.you_want_delete)+model.getPrinter_title()+"?")
                        .setNegativeButton(android.R.string.no, null)
                        .setPositiveButton(android.R.string.yes,
                                new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface arg0, int arg1) {
                                        DBPrinter dbp=new DBPrinter(context);
                                        dbp.deletePrinter(model.getId());
                                        list.remove(position);
                                        EventBus.getDefault().post("updateprinter");
                                        notifyDataSetChanged();
                                    }
                                }).create().show();
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolderPosts extends RecyclerView.ViewHolder {
        TextView tvcat,tvprinterno,tvprinternm,tvedit,tvdelete;
        LinearLayout llprinter;

        public ViewHolderPosts(View itemView) {
            super(itemView);
            tvcat = (TextView) itemView.findViewById(R.id.tvcat);
            tvprinterno = (TextView) itemView.findViewById(R.id.tvprinter);
            tvprinternm = (TextView) itemView.findViewById(R.id.tvprinternm);
            tvedit=(TextView)itemView.findViewById(R.id.tvedit);
            tvdelete=(TextView)itemView.findViewById(R.id.tvdelete);
            llprinter=(LinearLayout)itemView.findViewById(R.id.llprinter);
        }
    }


    private void editPrinterGroup(final int pos) {
        final DBPrinter dbp=new DBPrinter(context);
        ArrayList<CuisineListPojo> clist=new ArrayList<>();
        clist= (ArrayList<CuisineListPojo>) dbp.updateCategory(list.get(pos).getId());
        if(clist!=null && clist.size()>0){
            final Dialog dialog=new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_update_printer_category);
            RecyclerView rv=(RecyclerView)dialog.findViewById(R.id.rv);
            rv.setLayoutManager(new LinearLayoutManager(context));
            rv.setHasFixedSize(true);
            Button btnedit=(Button)dialog.findViewById(R.id.btnedit);
            btnedit.setTypeface(AppConst.font_regular(context));
            List<String> items = Arrays.asList(list.get(pos).getCategoryid().split("\\s*,\\s*"));
            if(KitchencategoryActivity.selcategory==null)
                KitchencategoryActivity.selcategory=new ArrayList<>();
            KitchencategoryActivity.selcategory.clear();
            KitchencategoryActivity.selcategory.addAll(items);
            CategoryAdapter categoryAdapter=new CategoryAdapter(clist,context);
            rv.setAdapter(categoryAdapter);

            btnedit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String allIds = TextUtils.join(",",KitchencategoryActivity.selcategory);
                    if(allIds!=null && allIds.trim().length()>0) {
                        KitchenPrinterPojo pojo=list.get(pos);
                        pojo.setCategoryid(allIds);
                        dbp.updatePrinterCategory(pojo);
                        EventBus.getDefault().post("updateprinter");
                        dialog.dismiss();
                    }else{
                        Toast.makeText(context, R.string.select_min_1_category,Toast.LENGTH_SHORT).show();
                    }
                }
            });
            dialog.show();
        }
    }
}
