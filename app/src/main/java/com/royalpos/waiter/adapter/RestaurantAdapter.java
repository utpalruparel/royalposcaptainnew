package com.royalpos.waiter.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.royalpos.waiter.R;
import com.royalpos.waiter.RestaurantListActivity;
import com.royalpos.waiter.WaiterListActivity;
import com.royalpos.waiter.helper.AppConst;
import com.royalpos.waiter.helper.ConnectionDetector;
import com.royalpos.waiter.model.LoginPojo;
import com.royalpos.waiter.model.M;
import com.royalpos.waiter.model.RestaurantPojo;
import com.royalpos.waiter.model.Waiter;
import com.royalpos.waiter.webservices.APIServiceN;
import com.royalpos.waiter.webservices.AuthenticationAPI;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;


public class RestaurantAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_ITEM = 1;
    private List<RestaurantPojo> list;
    Context context;
    String TAG="RestaurantAdapter";

    public RestaurantAdapter(List<RestaurantPojo> list, Context mcontext) {

        context = mcontext;
        this.list = list;
    }

    @Override
    public int getItemViewType(int position) {
        return TYPE_ITEM;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.restaurant_row, parent, false);
        return new ViewHolderPosts(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final RestaurantPojo model=list.get(position);
        final ViewHolderPosts itemview = (ViewHolderPosts) holder;
        final String username=model.getName();
        itemview.tvname.setText(username);
        if(model.getAddress()!=null && model.getAddress().trim().length()>0) {
            itemview.tvaddress.setVisibility(View.VISIBLE);
            itemview.tvaddress.setText(model.getAddress());
        }else{
            itemview.tvaddress.setVisibility(View.GONE);
        }
        itemview.tvusername.setText(context.getString(R.string.txt_username)+":"+model.getUsername());

        itemview.ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_rest_login);
                dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                TextView tvtitle = (TextView) dialog.findViewById(R.id.tvtitle);
                tvtitle.setText(model.getName());
                tvtitle.setAllCaps(false);
                final EditText etpwd = (EditText) dialog.findViewById(R.id.etpwd);
                etpwd.setTypeface(AppConst.font_regular(context));
                ImageView ivclose = (ImageView) dialog.findViewById(R.id.ivclose);
                TextView btn = (TextView) dialog.findViewById(R.id.btnsubmit);
                btn.setTypeface(AppConst.font_regular(context));
                ivclose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (etpwd.getText().toString().trim().length() <= 0)
                            etpwd.setError(context.getString(R.string.empty_password));
                        else {
                            login(model.getUsername(),etpwd.getText().toString(), dialog);
                        }
                    }
                });
                dialog.show();
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolderPosts extends RecyclerView.ViewHolder {
        TextView tvname,tvaddress,tvphone,tvusername;
        LinearLayout ll;
        ImageView img;

        public ViewHolderPosts(View itemView) {
            super(itemView);
            tvname = (TextView) itemView.findViewById(R.id.tvnm);
            tvaddress=(TextView)itemView.findViewById(R.id.tvaddress);
            tvusername=(TextView)itemView.findViewById(R.id.tvusername);
            ll=(LinearLayout)itemView.findViewById(R.id.llrest);
        }

    }

    private void login(final String nm, final String pwd, final Dialog dialog) {
        ConnectionDetector connectionDetector=new ConnectionDetector(context);
        if(connectionDetector.isConnectingToInternet()) {
            M.showLoadingDialog(context);
            AuthenticationAPI mAuthenticationAPI = APIServiceN.createService(context, AuthenticationAPI.class);
            Call<LoginPojo> call = mAuthenticationAPI.login(nm, pwd);
            call.enqueue(new retrofit2.Callback<LoginPojo>() {
                @Override
                public void onResponse(Call<LoginPojo> call, Response<LoginPojo> response) {
                    if (response.isSuccessful()) {
                        LoginPojo loginModel = response.body();
                        if (loginModel != null) {
                            if (loginModel.getSuccess()==1) {
                                M.setAdminId("",context);
                                String rid=loginModel.getRestaurant().getId()+"";
                                String rnm=loginModel.getRestaurant().getName();
                                String logo=loginModel.getRestaurant().getLogo();
                                String uid=loginModel.getRestaurant().getRest_unique_id();
                                String bid=loginModel.getRestaurant().getBrand_user_id();

                                AppConst.currency=loginModel.getRestaurant().getCurrency();
                                M.setRestID(rid, context);
                                M.setRestName(rnm,context);
                                M.setRestUserName(nm,context);
                                M.setRestAddress(loginModel.getRestaurant().getAddress(), context);
                                M.setRestPhoneNumber(loginModel.getRestaurant().getPhno(), context);
                                M.setRestImg(logo,context);
                                M.setRestUniqueID(uid,context);
                                M.setGST(loginModel.getRestaurant().getGst_no(),context);
                                M.setCurrency(loginModel.getRestaurant().getCurrency(),context);
                                M.setCashDrawer(loginModel.getRestaurant().getCash_drawer(),context);
                                M.setReportingemails(loginModel.getRestaurant().getReporting_emails(),context);
                                M.setBrandName(loginModel.getRestaurant().getBrand_name(),context);
                                M.setBrandId(loginModel.getRestaurant().getBrand_user_id(),context);
                                M.setTrackInventory(loginModel.getRestaurant().getRecipe_inventory(),context);
                                M.setDeduction(loginModel.getRestaurant().getStart_stock_deduct(),context);
                                M.setType(loginModel.getRestaurant().getType_of_business(),context);
                                M.setPlanid(loginModel.getRestaurant().getPlan_id()+"",context);
                                M.setfooterphone(loginModel.getRestaurant().getRecipt_footer_phone(),context);
                                M.setExpiry(loginModel.getRestaurant().getMembership_end_date(),context);
                                M.setPackcharge(loginModel.getRestaurant().getPackaging_charge(),context);
                                M.setChangePrice(Boolean.parseBoolean(loginModel.getRestaurant().getCan_change_price()),context);
                                if(loginModel.getRestaurant().getShow_item_price_without_tax()!=null){
                                    M.setPriceWT(Boolean.parseBoolean(loginModel.getRestaurant().getShow_item_price_without_tax()),context);
                                }
                                if(loginModel.getWaiters()!=null){
                                    if(AppConst.waiters==null) {
                                        AppConst.waiters = new ArrayList<Waiter>();
                                    }
                                    AppConst.waiters.clear();
                                    AppConst.waiters= (ArrayList<Waiter>) loginModel.getWaiters();
                                }
                                if(loginModel.getRestaurant().getIs_rounding_on().equals("on") &&
                                        loginModel.getRestaurant().getRounding_id()!=null && !loginModel.getRestaurant().getRounding_id().equals("0")){
                                    try {
                                        JSONObject jsonObject=new JSONObject();
                                        jsonObject.put("val_interval",loginModel.getRestaurant().getInterval_number());
                                        jsonObject.put("rule_tag",loginModel.getRestaurant().getRounding_tag());
                                        jsonObject.put("rounding_id",loginModel.getRestaurant().getRounding_id());
                                        M.setRoundBoundary(jsonObject+"",context);
                                        M.setRoundFormat(true,context);
                                    }catch (JSONException e){}
                                }else
                                    M.setRoundFormat(false,context);
                                M.setPointsAmt(loginModel.getRestaurant().getPoints_per_one_currency(),context);
                                M.saveVal(M.key_tip_cal,loginModel.getRestaurant().getTip_calculation(),context);
                                M.saveVal(M.key_tip_pref,loginModel.getRestaurant().getTip_calculation_preference(),context);
                                M.hideLoadingDialog();
                                Intent it=new Intent(context,WaiterListActivity.class);
                                dialog.dismiss();
                                ((RestaurantListActivity)context).finish();
                                context.startActivity(it);
                            } else if (loginModel.getSuccess()==2){
                                M.hideLoadingDialog();
                                M.showToast(context, ""+loginModel.getMessage());
                            }else {
                                M.hideLoadingDialog();
                                M.showToast(context, ""+loginModel.getMessage());
                            }
                        }

                    } else {
                        M.hideLoadingDialog();
                        int statusCode = response.code();
                        ResponseBody errorBody = response.errorBody();
                        Log.d(TAG, "error:" + statusCode + " " + errorBody);
                    }
                }

                @Override
                public void onFailure(Call<LoginPojo> call, Throwable t) {
                    Log.d(TAG, "fail:" + t.getMessage());
                    M.hideLoadingDialog();
                    // handle execution failures like no internet connectivity
                }
            });
        }else {
            M.showToast(context,context.getString(R.string.no_internet_error));
        }
    }
}
