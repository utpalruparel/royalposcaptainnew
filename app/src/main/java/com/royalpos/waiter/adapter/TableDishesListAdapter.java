package com.royalpos.waiter.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.royalpos.waiter.PlaceOrder;
import com.royalpos.waiter.R;
import com.royalpos.waiter.SearchActivity;
import com.royalpos.waiter.database.DBModifier;
import com.royalpos.waiter.database.DBUnit;
import com.royalpos.waiter.helper.AppConst;
import com.royalpos.waiter.helper.ConnectionDetector;
import com.royalpos.waiter.model.ComboModel;
import com.royalpos.waiter.model.DailyBalPojo;
import com.royalpos.waiter.model.DishOrderPojo;
import com.royalpos.waiter.model.DishPojo;
import com.royalpos.waiter.model.M;
import com.royalpos.waiter.model.Modifier_cat;
import com.royalpos.waiter.model.PreModel;
import com.royalpos.waiter.model.SuccessPojo;
import com.royalpos.waiter.model.TaxData;
import com.royalpos.waiter.model.UnitPojo;
import com.royalpos.waiter.model.VarPojo;
import com.royalpos.waiter.print.PrintFormat;
import com.royalpos.waiter.ui.AutoResizeTextView;
import com.royalpos.waiter.webservices.APIServiceHeader;
import com.royalpos.waiter.webservices.WaiterAPI;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import de.greenrobot.event.EventBus;
import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * Created by Reeva on 10/12/2015.
 */
public class TableDishesListAdapter extends RecyclerView.Adapter<TableDishesListAdapter.DataObjectHolder> {
    View view;
    DishOrderPojo custorder;
    long quan;
    Context mcontext;
    Activity activity;
    public List<DishPojo> custdisheslist = new ArrayList<DishPojo>();
    private static String TAG = "TableDishesListAdapter";
    int i =0, s=0;
    String selectedid = "",selectednm="",unit_id="",unit_name="";
    Typeface fontregular;
    ConnectionDetector connectionDetector;
    Boolean ismob=false,issearch=false,isweight=false;
    PrintFormat pf;
    double tax_per_tot=0f;
    String taxtype="";

    public TableDishesListAdapter(Context mcontext, List<DishPojo> custdisheslist, Boolean issearch) {
        this.mcontext = mcontext;
        pf=new PrintFormat(mcontext);
        activity = PlaceOrder.activity;
        this.custdisheslist = custdisheslist;
        connectionDetector=new ConnectionDetector(mcontext);
        fontregular = AppConst.font_regular(mcontext);
        if(AppConst.dishorederlist==null) {
            AppConst.dishorederlist = new ArrayList<DishOrderPojo>();
        }
        if(AppConst.selidlist==null) {
            AppConst.selidlist = new ArrayList<Integer>();
        }
        this.issearch=issearch;
        if(mcontext.getResources().getBoolean(R.bool.portrait_only))
            ismob=true;
        else
            ismob=false;
    }

    public void updateList(List<DishPojo> custdisheslist){
        this.custdisheslist = custdisheslist;
        notifyDataSetChanged();
    }

    public static class DataObjectHolder extends RecyclerView.ViewHolder {
        AutoResizeTextView txtdname, tvamount;
        LinearLayout lladd, llcolor;
        ImageView ivdish;
        int[] androidColors;

        public DataObjectHolder(View itemView) {
            super(itemView);
            txtdname = (AutoResizeTextView) itemView.findViewById(R.id.tvdishname);
            tvamount = (AutoResizeTextView) itemView.findViewById(R.id.tvamount);
            lladd=(LinearLayout) itemView.findViewById(R.id.lladd);
            androidColors = itemView.getResources().getIntArray(R.array.androidcolors);
            llcolor =(LinearLayout) itemView.findViewById(R.id.lladd);
            ivdish = (ImageView)itemView.findViewById(R.id.ivdish);
        }

    }

    public String replace(String str) {
        if (str.length() > 0 && str.charAt(str.length()-1)==',') {
            str = str.substring(0, str.length()-1);
        }
        return str;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(issearch){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dish_row, parent, false);
        }else if(mcontext.getResources().getBoolean(R.bool.portrait_only)) {
            if(M.getListType(mcontext).equals("gridimg"))
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dish_item_image_portrait, parent, false);
            else if(M.getListType(mcontext).equals("list"))
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dish_row, parent, false);
            else
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dish_item_portrait, parent, false);
        }else {
            if(M.getListType(mcontext).equals("gridimg")) {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dish_item_image, parent, false);
            }else if(M.getListType(mcontext).equals("list"))
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dish_row, parent, false);
            else {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dish_item, parent, false);
            }
        }
        DataObjectHolder dataobjectholder = new DataObjectHolder(view);
        return dataobjectholder;
    }

    @Override
    public void onBindViewHolder(final DataObjectHolder holder, final int position) {
        final DishPojo model=custdisheslist.get(position);
        String img=model.getDishimage();
        final String dishid=model.getDishid();
        final int dish_id= Integer.valueOf(dishid);
        final String dishname=model.getDishname();
        String desc=model.getDescription();
        final String price=model.getPrice();
        final String preflag=model.getPreflag();
        List<PreModel> preflist=model.getPre();
        String track_stock=model.getComposite_item_track_stock();
        String current_stock=model.getIn_stock();

        if(M.getListType(mcontext).equals("gridimg") && holder.ivdish!=null) {
            if (model.getDishimage().trim().length() > 0) {
                File f=new File(AppConst.item_img_dir+model.getDishimage());
                if(f!=null && f.exists())
                    Picasso.get()
                            .load(f)//(AppConst.dish_img_url+ M.getBrandId(mcontext)+"/dish/"+img)
                            .into(holder.ivdish);
            }
        }
        if(track_stock.equalsIgnoreCase("true") && current_stock!=null){
            if(current_stock.equals("")){
                current_stock="0";
            }
            if(Double.parseDouble(current_stock)>0) {
                holder.txtdname.setTextColor(mcontext.getResources().getColor(R.color.white));
                holder.txtdname.setTag("no");
            }else {
                holder.txtdname.setTextColor(mcontext.getResources().getColor(R.color.white));
                holder.txtdname.setTag("yes");
            }
        }else{
            holder.txtdname.setTextColor(mcontext.getResources().getColor(R.color.white));
            holder.txtdname.setTag("no");
        }
        holder.txtdname.setText(dishname);
        holder.txtdname.setTypeface(AppConst.font_regular(mcontext));
        if(M.isPriceWT(mcontext) && model.getPrice_without_tax()!=null && model.getPrice_without_tax().trim().length()>0)
            holder.tvamount.setText(pf.setFormat(model.getPrice_without_tax()));
        else
            holder.tvamount.setText(pf.setFormat(price));
        holder.tvamount.setTypeface(AppConst.font_regular(mcontext));
        holder.lladd.setTag(model);

        holder.lladd.setTag("" + position);
        holder.lladd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unit_id="";
                unit_name="";
                Boolean ispunch=false;

                if (holder.txtdname.getTag().toString().equalsIgnoreCase("no")){
                    ispunch=true;
                }else if(M.isLowStockalert(mcontext) && M.islowstockpunch(mcontext)){
                    new AlertDialog.Builder(mcontext)
                            .setTitle(R.string.dialog_title_low_stock)
                            .setMessage(mcontext.getString(R.string.txt_want_add_item))
                            .setNegativeButton(android.R.string.no, null)
                            .setPositiveButton(android.R.string.yes,
                                    new DialogInterface.OnClickListener() {

                                        public void onClick(DialogInterface arg0, int arg1) {
                                            if (connectionDetector.isConnectingToInternet() &&
                                                    M.getCashDrawer(mcontext) != null && M.getCashDrawer(mcontext).equals("on") &&
                                                    (M.getdaily_balance_id(mcontext) == null || M.getdaily_balance_id(mcontext).trim().length() <= 0)) {
                                                check_opening_bal(model);
                                            } else {
                                                checkSoldType(model);
                                            }
                                        }
                                    }).create().show();
                }else if(M.isLowStockalert(mcontext) && !M.islowstockpunch(mcontext)){
                    new AlertDialog.Builder(mcontext)
                            .setTitle(R.string.dialog_title_low_stock)
                            .setMessage(mcontext.getString(R.string.out_of_stock))
                            .setPositiveButton(android.R.string.yes,
                                    new DialogInterface.OnClickListener() {

                                        public void onClick(DialogInterface arg0, int arg1) {

                                        }
                                    }).create().show();
                }else if(!M.isLowStockalert(mcontext) && M.islowstockpunch(mcontext)){
                    ispunch=true;
                }else{
                    M.showToast(mcontext, mcontext.getString(R.string.out_of_stock));
                }

                if(ispunch){
                    if (connectionDetector.isConnectingToInternet() &&
                            M.getCashDrawer(mcontext) != null && M.getCashDrawer(mcontext).equals("on") &&
                            (M.getdaily_balance_id(mcontext) == null || M.getdaily_balance_id(mcontext).trim().length() <= 0)) {
                        check_opening_bal(model);
                    } else {
                        checkSoldType(model);
                    }
                }

            }
        });

        int randomAndroidColor = holder.androidColors[position % holder.androidColors.length];

        holder.lladd.setBackgroundColor(randomAndroidColor);

    }

    @Override
    public int getItemCount() {
        return custdisheslist.size();
    }

    private void checkSoldType(final DishPojo model){
        if(model.getSold_by().equals("each"))
            addDish(model);
        else {
            final DBUnit dbUnit = new DBUnit(mcontext);
            final UnitPojo unitPojo = dbUnit.getUnitInfo(model.getPurchased_unit_id(), mcontext);

            final Dialog wd = new Dialog(mcontext, R.style.MyDialogTheme);
            wd.requestWindowFeature(Window.FEATURE_NO_TITLE);
            LayoutInflater inflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.dialog_weight, null);
            wd.setContentView(layout);
            wd.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            final TextView tvweight = (TextView) wd.findViewById(R.id.tvweight);
            final TextView tvprice = (TextView) wd.findViewById(R.id.tvprice);
            final TextView tvamt = (TextView) wd.findViewById(R.id.tvpriceamount);
            final TextView tvcntweight = (TextView) wd.findViewById(R.id.tvcntweight);
            final TextView tvpurchase = (TextView) wd.findViewById(R.id.tvpurchase);
            tvpurchase.setText(model.getPurchased_unit_name());
            final TextView tvused = (TextView) wd.findViewById(R.id.tvused);
            tvused.setText(model.getUsed_unit_name());
            if (model.getPurchased_unit_id().equals(model.getUsed_unit_id())){
                tvused.setVisibility(View.GONE);
                tvpurchase.setTag("1");
                setunitChip(tvpurchase,tvused);
            }else {
                tvused.setVisibility(View.VISIBLE);
                tvpurchase.setTag("0");
            }
            final EditText etweight=(EditText)wd.findViewById(R.id.etweight);
            etweight.setTypeface(AppConst.font_regular(mcontext));
            final EditText etamt=(EditText)wd.findViewById(R.id.etamount);
            etamt.setTypeface(AppConst.font_regular(mcontext));
            ImageView imginc=(ImageView)wd.findViewById(R.id.imgdinc);
            ImageView imgdec=(ImageView)wd.findViewById(R.id.imgddec);
            final EditText edtqty=(EditText) wd.findViewById(R.id.edtqty);
            edtqty.setTypeface(AppConst.font_regular(mcontext));
            Button buttonSeven = (Button)wd.findViewById(R.id.buttonSeven);
            Button buttonEight= (Button)wd.findViewById(R.id.buttonEight);
            Button buttonNine= (Button)wd.findViewById(R.id.buttonNine);
            Button buttonFour = (Button)wd.findViewById(R.id.buttonFour);
            Button buttonFive= (Button)wd.findViewById(R.id.buttonFive);
            Button buttonSix= (Button)wd.findViewById(R.id.buttonSix);
            Button buttonOne= (Button)wd.findViewById(R.id.buttonOne);
            Button buttonTwo= (Button)wd.findViewById(R.id.buttonTwo);
            Button buttonThree = (Button)wd.findViewById(R.id.buttonThree);
            Button buttonClear = (Button)wd.findViewById(R.id.buttonClear);
            Button buttonZero = (Button)wd.findViewById(R.id.buttonZero);
            Button buttonEqual = (Button)wd.findViewById(R.id.buttonEqual);
            buttonEqual.setText(".");
            final LinearLayout llw=(LinearLayout)wd.findViewById(R.id.llweight);
            final LinearLayout llp=(LinearLayout)wd.findViewById(R.id.llprice);
            llp.setTag("0");
            Button btnadd=(Button)wd.findViewById(R.id.btnadd);
            btnadd.setTypeface(AppConst.font_regular(mcontext));
            Button btnclose=(Button)wd.findViewById(R.id.btnclose);
            btnclose.setTypeface(AppConst.font_regular(mcontext));
            isweight=true;

            Double pamt=0.0,ptamt=0.0;
            if(model.getPrice().trim().length()>0 && !model.getPrice().equals(".")){
                pamt= AppConst.setdecimalfmt(Double.parseDouble(model.getPrice()),mcontext);
            }
            if(model.getPrice_without_tax().trim().length()>0 && !model.getPrice_without_tax().equals(".")){
                ptamt=Double.parseDouble(model.getPrice_without_tax());
            }

            final Double finalPamt = pamt;
            final Double finalPtamt = ptamt;
            etweight.addTextChangedListener(new TextWatcher() {
                Boolean ispurchase;
                Double we=0.0,wet=0.0;
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if(tvpurchase.getTag().toString().equals("1")) {
                        ispurchase = true;
                    }else {
                        ispurchase = false;
                    }
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    etweight.setSelection(etweight.length());
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String w = etweight.getText().toString();
                    if(w.equals("."))
                        w=null;
                    if (w != null && w.trim().length() > 0) {
                        if (ispurchase) {
                            we = finalPamt * Double.parseDouble(w);
                            wet = finalPtamt * Double.parseDouble(w);
                            unit_id = model.getPurchased_unit_id();
                            unit_name = model.getPurchased_unit_name();
                        } else if(unitPojo!=null){
                            we = (Double.parseDouble(w) * finalPamt) / (Double.parseDouble(unitPojo.getUsed_unit()));
                            wet = (Double.parseDouble(w) * finalPtamt) / (Double.parseDouble(unitPojo.getUsed_unit()));
                            unit_id = model.getUsed_unit_id();
                            unit_name = model.getUsed_unit_name();
                        }
                    } else {
                        we = 0.0;
                        wet = 0.0;
                    }
                    we = AppConst.setdecimalfmt(we, mcontext);
                    wet = AppConst.setdecimalfmt(wet, mcontext);
                    if(M.isPriceWT(mcontext))
                        tvamt.setText(mcontext.getString(R.string.txt_amount)+" : " + AppConst.currency + " " + wet);
                    else
                        tvamt.setText(mcontext.getString(R.string.txt_amount)+" : " + AppConst.currency + " " + we);
                    tvamt.setTag(we + "");
                    llp.setTag(wet + "");

                }
            });

            final Double finalPamt1 = pamt;
            etamt.addTextChangedListener(new TextWatcher() {
                Double we=0.0,wet=0.0;
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    unit_name=model.getPurchased_unit_name();
                    unit_id=model.getPurchased_unit_id();
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    etamt.setSelection(etamt.length());
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String w = etamt.getText().toString();
                    if(w.equals("."))
                        w=null;
                    if(w!=null && w.trim().length()>0) {
                        we=Double.parseDouble(w)/ finalPamt1;
                        if(we<1 && !model.getPurchased_unit_id().equals(model.getUsed_unit_id()) &&
                                unitPojo!=null) {
                            Double weu=we*(Double.parseDouble(unitPojo.getUsed_unit()));
                            we = weu;
                            unit_name=model.getUsed_unit_name();
                            unit_id=model.getUsed_unit_id();
                            tvpurchase.setTag("0");
                        }else
                            tvpurchase.setTag("1");
                    }else{
                        we=0.0;
                        tvpurchase.setTag("1");
                    }
                    we= AppConst.setdecimalfmt1(we,"#.###");
                    tvcntweight.setText("Weight : "+we+" "+unit_name);
                    tvcntweight.setTag(we+"");
                    etweight.setText(we+"");
                    etweight.setSelection(etweight.getText().length());
                }
            });

            if(model.getDefault_sale_value()!=null && model.getDefault_sale_value().trim().length()>0) {
                etweight.setText(model.getDefault_sale_value());
                etweight.setSelection(etweight.getText().length());
            }

            tvweight.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    isweight=true;
                    tvweight.setTextColor(mcontext.getResources().getColor(R.color.white));
                    tvweight.setTypeface(AppConst.font_medium(mcontext));
                    tvprice.setTextColor(mcontext.getResources().getColor(R.color.primary_text));
                    tvprice.setTypeface(AppConst.font_regular(mcontext));
                    llw.setVisibility(View.VISIBLE);
                    llp.setVisibility(View.GONE);
                    setunitChip(tvpurchase,tvused);
                    if(tvcntweight.getTag()!=null && tvcntweight.getTag().toString().trim().length()>0){
                        etweight.setText(tvcntweight.getTag()+"");
                        etweight.setSelection(etweight.getText().length());
                    }
                }
            });

            tvprice.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    isweight=false;
                    tvprice.setTextColor(mcontext.getResources().getColor(R.color.white));
                    tvprice.setTypeface(AppConst.font_medium(mcontext));
                    tvweight.setTextColor(mcontext.getResources().getColor(R.color.primary_text));
                    tvweight.setTypeface(AppConst.font_regular(mcontext));
                    llw.setVisibility(View.GONE);
                    llp.setVisibility(View.VISIBLE);
                    if(tvamt.getTag()!=null && tvamt.getTag().toString().trim().length()>0)
                        etamt.setText(tvamt.getTag()+"");
                    etamt.setSelection(etamt.getText().length());
                }
            });

            tvpurchase.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    tvpurchase.setTag("1");
                    etweight.setText("");
                    setunitChip(tvpurchase,tvused);
                    etweight.setSelection(etweight.getText().length());

                }
            });

            tvused.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    tvpurchase.setTag("0");
                    etweight.setText("");
                    setunitChip(tvpurchase,tvused);
                    etweight.setSelection(etweight.getText().length());
                }
            });

            btnadd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String weight="",amt="",amtwithouttax="",taxamt="",snm="";
                    float tax_tot=0f;
                    if(isweight){
                        weight=etweight.getText()+"";
                        amt=tvamt.getTag()+"";
                    }else{
                        amt=etamt.getText()+"";
                        weight=tvcntweight.getTag()+"";
                    }

                    if(weight.equals("."))
                        weight=null;
                    if(weight!=null && weight.trim().length()>0 && Double.parseDouble(weight)>0) {
                        if(llp.getTag()!=null)
                            amtwithouttax=llp.getTag()+"";
                        float tamt=Float.parseFloat(amt)-Float.parseFloat(amtwithouttax);
                        if(unit_id.equals(model.getPurchased_unit_id())) {
                            snm = dbUnit.getSortName(model.getPurchased_unit_id(), null);
                            if(snm==null && model.getPurchased_unit_id().equals(model.getUsed_unit_id()))
                                snm=dbUnit.getSortName(null,model.getUsed_unit_id());
                        }else
                            snm=dbUnit.getSortName(null,model.getUsed_unit_id());
                        if(snm==null)
                            snm=unit_name;
                        if(model.getTax_data()!=null && model.getTax_data().size()>0) {
                            for (TaxData t : model.getTax_data())
                                tax_tot = tax_tot + Float.parseFloat(t.getValue());
                        }
                        Log.d(TAG, weight + " " + amt + " " + unit_id + " " + unit_name);
                        wd.dismiss();
                        custorder = new DishOrderPojo();
                        custorder.setDishid(model.getDishid());
                        custorder.setDishname(model.getDishname());
                        custorder.setQty(edtqty.getText().toString());
                        custorder.setIsnew(true);
                        custorder.setStatus("0");
                        custorder.setPrefid("0");
                        custorder.setPrenm("");
                        custorder.setOrderdetailid(null);
                        custorder.setDiscount(model.getDiscount_amount());
                        custorder.setDish_comment("");
                        custorder.setCusineid(model.getCusineid());
                        custorder.setDescription(model.getDescription());
                        custorder.setDishimage(model.getDishimage());
                        custorder.setPreflag(model.getPreflag());
                        custorder.setPre(model.getPre());
                        custorder.setInventory_item(model.getInventory_item());
                        custorder.setSold_by(model.getSold_by());
                        custorder.setUnitid(unit_id);
                        custorder.setUnitname(unit_name);
                        custorder.setPurchased_unit_id(model.getPurchased_unit_id());
                        custorder.setPurchased_unit_name(model.getPurchased_unit_name());
                        custorder.setUsed_unit_id(model.getUsed_unit_id());
                        custorder.setUsed_unit_name(model.getUsed_unit_name());
                        custorder.setWeight(weight);
                        custorder.setPrice(pf.setFormat(amt));
                        custorder.setPriceperdish(model.getPrice());
                        custorder.setPriceper_without_tax(model.getPrice_without_tax());
                        custorder.setPrice_without_tax(amtwithouttax);
                        custorder.setDefault_sale_weight(model.getDefault_sale_value());
                        custorder.setSort_nm(snm);
                        custorder.setTax_data(model.getTax_data());
                        custorder.setTax_amt(tamt+"");
                        custorder.setTot_tax(tax_tot+"");
                        custorder.setHsn_no(model.getHsn_no());
                        custorder.setAllow_open_price(model.getAllow_open_price());
                        if(model.getDiscount_amount()!=null && model.getDiscount_amount().trim().length()>0){
                            Double damt=Double.parseDouble(custorder.getQty())*Double.parseDouble(model.getDiscount_amount());
                            Double pamt=Double.parseDouble(custorder.getQty())*Double.parseDouble(custorder.getPrice());
                            if (damt > pamt)
                                custorder.setTot_disc(pf.setFormat(pamt+""));
                            else
                                custorder.setTot_disc(pf.setFormat(damt+""));
                        }
                        custorder.setOffer(model.getOffers());
                        custorder.setTm(new Date().getTime()+"");
                        AppConst.dishorederlist.add(custorder);
                        AppConst.selidlist.add(Integer.parseInt(model.getDishid()));
                        EventBus.getDefault().post("updatetableorder");
                    }else{
                        Toast.makeText(mcontext, R.string.txt_fill_data,Toast.LENGTH_SHORT).show();
                    }
                }
            });

            btnclose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    wd.dismiss();
                }
            });

            imginc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    quan = Long.parseLong(edtqty.getText().toString());
                    quan++;
                    edtqty.setText(quan + "");
                    edtqty.setSelection(edtqty.getText().length());

                }

            });

            imgdec.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    quan = Long.parseLong(edtqty.getText().toString());
                    if (quan == 1) {
                        return;
                    } else {
                        quan--;
                        edtqty.setText(quan + "");
                    }
                    edtqty.setSelection(edtqty.getText().length());
                }
            });

            buttonZero.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(isweight)
                        etweight.setText(etweight.getText() + "0");
                    else
                        etamt.setText(etamt.getText() + "0");
                }
            });

            buttonOne.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(isweight)
                        etweight.setText(etweight.getText() + "1");
                    else
                        etamt.setText(etamt.getText() + "1");
                }
            });

            buttonTwo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(isweight)
                        etweight.setText(etweight.getText() + "2");
                    else
                        etamt.setText(etamt.getText() + "2");
                }
            });

            buttonThree.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(isweight)
                        etweight.setText(etweight.getText() + "3");
                    else
                        etamt.setText(etamt.getText() + "3");
                }
            });

            buttonFour.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(isweight)
                        etweight.setText(etweight.getText() + "4");
                    else
                        etamt.setText(etamt.getText() + "4");
                }
            });

            buttonFive.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(isweight)
                        etweight.setText(etweight.getText() + "5");
                    else
                        etamt.setText(etamt.getText() + "5");
                }
            });

            buttonSix.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(isweight)
                        etweight.setText(etweight.getText() + "6");
                    else
                        etamt.setText(etamt.getText() + "6");
                }
            });

            buttonSeven.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(isweight)
                        etweight.setText(etweight.getText() + "7");
                    else
                        etamt.setText(etamt.getText() + "7");
                }
            });

            buttonEight.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(isweight)
                        etweight.setText(etweight.getText() + "8");
                    else
                        etamt.setText(etamt.getText() + "8");
                }
            });

            buttonNine.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(isweight)
                        etweight.setText(etweight.getText() + "9");
                    else
                        etamt.setText(etamt.getText() + "9");
                }
            });



            buttonEqual.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(isweight) {
                        if(!etweight.getText().toString().contains("."))
                            etweight.setText(etweight.getText() + ".");
                    }else {
                        if(!etamt.getText().toString().contains("."))
                            etamt.setText(etamt.getText() + ".");
                    }
                }
            });

            buttonClear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(isweight) {
                        etweight.setText("");
                    }else {
                        etamt.setText("");
                    }

                }
            });
            wd.show();
        }
    }

    public void displayDetail(final DishPojo model){
        tax_per_tot=0;
        taxtype="";
        final Dialog dialog=new Dialog(mcontext, R.style.MyDialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = (LayoutInflater)mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.dialog_dish_detail, null);
        dialog.setContentView(layout);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ImageView ivclose=(ImageView)dialog.findViewById(R.id.ivclose);
        final TextView txtdname = (TextView) dialog.findViewById(R.id.txtcustdishname);
        final TextView edtqty=(TextView)dialog.findViewById(R.id.edtqty);
        final EditText etnote=(EditText)dialog.findViewById(R.id.etnote);
        etnote.setTypeface(AppConst.font_regular(mcontext));
        ImageView imginc=(ImageView)dialog.findViewById(R.id.imgdinc);
        ImageView imgdec=(ImageView)dialog.findViewById(R.id.imgddec);
        final Button btnadd=(Button)dialog.findViewById(R.id.custadd);
        final TextView txtprice = (TextView) dialog.findViewById(R.id.txtcustdishprice);
        final LinearLayout llmodifier=(LinearLayout)dialog.findViewById(R.id.llmodifier);
        final EditText etchange=(EditText)dialog.findViewById(R.id.etchangeprice);
        etchange.setTypeface(AppConst.font_regular(mcontext));
        LinearLayout llchangeprice=(LinearLayout)dialog.findViewById(R.id.llchangeprice);
        if(model.getAllow_open_price().equals("true"))
            llchangeprice.setVisibility(View.VISIBLE);
        else
            llchangeprice.setVisibility(View.GONE);
        ScrollView sv=(ScrollView)dialog.findViewById(R.id.sv);
        llmodifier.setOrientation(LinearLayout.VERTICAL);
        TableLayout tbl=new TableLayout(mcontext);
        tbl.setStretchAllColumns(true);
        TableRow tr=null;
        final String dishid=model.getDishid();
        final int dish_id= Integer.valueOf(dishid);
        final String dishname=model.getDishname();
        final String price=model.getPrice();
        String preflag=model.getPreflag();
        List<PreModel> preflist=model.getPre();
        final ArrayList<VarPojo> vlist=new ArrayList<>();
        final ArrayList<String> vidlist=new ArrayList<>();

        txtdname.setText(dishname);
        if(M.isPriceWT(mcontext))
            txtprice.setText(AppConst.currency + " " + pf.setFormat(model.getPrice_without_tax()));
        else
            txtprice.setText(AppConst.currency + " " + pf.setFormat(price));

        if(model.getTax_data()!=null && model.getTax_data().size()>0) {
            for (TaxData t : model.getTax_data()) {
                tax_per_tot = tax_per_tot + Float.parseFloat(t.getValue());
                taxtype=t.getTax_amount();
            }
        }

        if (AppConst.selidlist!=null && AppConst.selidlist.contains(dish_id)) {
            int pos= AppConst.selidlist.indexOf(dish_id);
            DishOrderPojo selpos= AppConst.dishorederlist.get(pos);
            edtqty.setText(selpos.getQty());
        }

        imginc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                quan = Long.parseLong(edtqty.getText().toString());
                quan++;
                edtqty.setText(quan + "");

            }

        });
        imgdec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                quan = Long.parseLong(edtqty.getText().toString());
                if (quan == 1) {
                    return;
                } else {
                    quan--;
                    edtqty.setText(quan + "");
                }
            }
        });
        btnadd.setTypeface(fontregular);

        btnadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                custorder = new DishOrderPojo();
                quan = Long.parseLong(edtqty.getText().toString());
                String poscustdisid = dishid;
                String poscustdishname = dishname;
                if (txtdname.getTag() == null)
                    txtdname.setTag("0");

                double priceperdish =Double.parseDouble(model.getPrice());
                double pwtax=Double.parseDouble(model.getPrice_without_tax());
                Double newgst=Double.parseDouble(model.getTax_amt());
                String finalid = "", finalnm = "";

                if(vlist!=null && vlist.size()>0){
                    double vtot=0,vtot_wt=0;
                    for(VarPojo v1:vlist) {
                        Double vamt=Integer.parseInt(v1.getQuantity())*Double.parseDouble(v1.getAmount());
                        Double vamtwt=Integer.parseInt(v1.getQuantity())*Double.parseDouble(v1.getAmount_wt());
                        vtot=vtot+vamt;
                        vtot_wt=vtot_wt+vamtwt;
                        if(finalid.trim().length()==0) {
                            finalid = v1.getId();
                            finalnm=v1.getQuantity()+" X "+v1.getName();
                        }else {
                            finalid = finalid + "," + v1.getId();
                            finalnm=finalnm+","+v1.getQuantity()+" X "+v1.getName();
                        }
                    }
                    newgst=newgst+(vtot-vtot_wt);
                    priceperdish=priceperdish+vtot;
                    pwtax=pwtax+vtot_wt;
                    custorder.setVarPojoList(vlist);
                }else
                    custorder.setVarPojoList(new ArrayList<VarPojo>());

                if(etchange.getText().toString().trim().length()>0) {
                    if (etchange.getText().toString().equalsIgnoreCase(".")){
                        newgst=0.0;
                        priceperdish=0.0;
                        pwtax=0.0;
                    }else {
                        Double cprice=Double.parseDouble(etchange.getText().toString());
                        Log.d(TAG,"===change price===");
                        if (taxtype != null && taxtype.trim().length() > 0) {
                            if (taxtype.equals("1")) {
                                priceperdish=cprice;
                                newgst = cprice - (cprice * (100 / (100 + tax_per_tot)));
                                pwtax = cprice - newgst;
                            } else if (taxtype.equals("0")) {
                                pwtax=cprice;
                                newgst = (cprice * tax_per_tot) / 100;
                                priceperdish = cprice + newgst;
                            }
                            Log.d(TAG,"tax amount:"+newgst);
                            Log.d(TAG,"price:"+priceperdish);
                            Log.d(TAG,"price without tax:"+pwtax);
                        } else {
                            priceperdish=cprice;
                            pwtax=cprice;
                            newgst=0.0;
                        }
                    }
                }

                custorder.setDishid(poscustdisid);
                custorder.setDishname(poscustdishname);
                custorder.setQty(quan + "");
                custorder.setIsnew(true);
                custorder.setStatus("0");
                custorder.setPrefid(finalid);

                custorder.setPrice(pf.setFormat("" + priceperdish));
                custorder.setPriceperdish(model.getPrice());
                custorder.setPrice_without_tax(pwtax+"");
                custorder.setPriceper_without_tax(model.getPrice_without_tax()+"");

                custorder.setPrenm(finalnm);
                custorder.setOrderdetailid(null);
                custorder.setDiscount(model.getDiscount_amount());
                custorder.setComment("");
                custorder.setDish_comment(etnote.getText().toString());
                custorder.setCusineid(model.getCusineid());
                custorder.setDescription(model.getDescription());
                custorder.setDishimage(model.getDishimage());
                custorder.setPreflag(model.getPreflag());
                custorder.setPre(model.getPre());
                custorder.setInventory_item(model.getInventory_item());
                custorder.setSold_by(model.getSold_by());
                custorder.setUnitid("");
                custorder.setUnitname("");
                custorder.setPurchased_unit_id(model.getPurchased_unit_id());
                custorder.setPurchased_unit_name(model.getPurchased_unit_name());
                custorder.setUsed_unit_id(model.getUsed_unit_id());
                custorder.setUsed_unit_name(model.getUsed_unit_name());
                custorder.setDefault_sale_weight(model.getDefault_sale_value());
                custorder.setTax_data(model.getTax_data());
                custorder.setTax_amt(newgst+"");
                custorder.setTot_tax(tax_per_tot+"");
                custorder.setHsn_no(model.getHsn_no());
                custorder.setAllow_open_price(model.getAllow_open_price());
                if(model.getDiscount_amount()!=null && model.getDiscount_amount().trim().length()>0){
                    Double damt=quan*Double.parseDouble(model.getDiscount_amount());
                    Double pamt=quan*Double.parseDouble(custorder.getPrice());
                    if (damt > pamt)
                        custorder.setTot_disc(pf.setFormat(pamt+""));
                    else
                        custorder.setTot_disc(pf.setFormat(damt+""));
                }
                custorder.setOffer(model.getOffers());
                if (AppConst.selidlist != null && AppConst.selidlist.contains(dish_id) && !M.isCustomAllow(M.key_dinein_separate_item,mcontext)) {
                    DishOrderPojo selpos = new DishOrderPojo();
                    int noOfOccurs = Collections.frequency(AppConst.selidlist, dish_id), pos = -1;
                    Boolean isupdatePref = false;
                    List<String> selpid = new ArrayList<String>(Arrays.asList(finalid.split(",")));
                    selpid.remove("0");
                    if (noOfOccurs == 1) {
                        pos = AppConst.selidlist.indexOf(dish_id);
                        selpos = AppConst.dishorederlist.get(pos);
                        if (selpos.isnew()) {
                            List<String> plist = new ArrayList<String>(Arrays.asList(selpos.getPrefid().split(",")));
                            plist.remove("0");
                            isupdatePref = false;
                            if(selpid.size()!=plist.size()){
                                isupdatePref = true;
                            }else {
                                for (String id : plist) {
                                    if (!selpid.contains(id))
                                        isupdatePref = true;
                                }
                            }
                        } else {
                            isupdatePref = true;
                        }
                    } else {
                        isupdatePref = true;
                        for (int d = 0; d < AppConst.dishorederlist.size(); d++) {
                            if (AppConst.selidlist.get(d) == dish_id) {
                                DishOrderPojo selpos1 = AppConst.dishorederlist.get(d);
                                if (selpos1.isnew()) {
                                    List<String> plist = new ArrayList<String>(Arrays.asList(selpos1.getPrefid().split(",")));
                                    plist.remove("0");
                                    int pi = 0;
                                    for (String id : plist) {
                                        if (selpid.contains(id.trim())) {
                                            pi++;
                                        }
                                    }
                                    if (pi == plist.size() && pi==selpid.size()) {
                                        isupdatePref = false;
                                        pos = d;
                                        selpos = selpos1;
                                    }
                                }
                            }
                        }
                    }
                    if (isupdatePref) {
                        custorder.setTm(new Date().getTime()+"");
                        AppConst.dishorederlist.add(custorder);
                        AppConst.selidlist.add(dish_id);
                    } else {
                        if (pos > -1) {
                            long q = quan;
                            selpos.setQty(q + "");
                            if(model.getDiscount_amount()!=null && model.getDiscount_amount().trim().length()>0){
                                Double damt=q*Double.parseDouble(model.getDiscount_amount());
                                Double pamt=q*Double.parseDouble(selpos.getPrice());
                                Log.d(TAG,damt+" "+pamt+"");
                                if (damt > pamt)
                                    selpos.setTot_disc(pf.setFormat(pamt+""));
                                else
                                    selpos.setTot_disc(pf.setFormat(damt+""));
                            }
                            selpos.setOffer(model.getOffers());
                            AppConst.dishorederlist.set(pos, selpos);
                        }
                    }
                } else {
                    custorder.setTm(new Date().getTime()+"");
                    AppConst.dishorederlist.add(custorder);
                    AppConst.selidlist.add(dish_id);
                }
                EventBus.getDefault().post("updatetableorder");
                if(issearch && mcontext instanceof SearchActivity){
                    ((SearchActivity)mcontext).finish();
                }
                dialog.dismiss();
            }

        });

        llmodifier.setTag("");
        sv.setVisibility(View.GONE);
        llmodifier.setOrientation(LinearLayout.VERTICAL);
//        TableLayout tbl=new TableLayout(mcontext);
//        tbl.setStretchAllColumns(true);
//        TableRow tr=null;
        if(preflag.equals("true")) {
            sv.setVisibility(View.VISIBLE);
            ArrayList<String> ml=new ArrayList<>();
            final ArrayList<Modifier_cat> mlnm=new ArrayList<>();
            final ArrayList<LinearLayout> mlinear=new ArrayList<>();
            int mcatI=0;
            for(PreModel pre : preflist)
            {
                String modifier_name="";
                int mpos=0;
                if(!ml.contains(pre.getPretype())) {
                    ml.add(pre.getPretype());
                    DBModifier dbm=new DBModifier(mcontext);
                    Modifier_cat modi_cat=dbm.getModifierName(pre.getPretype());
                    if(modi_cat!=null) {
                        modifier_name = modi_cat.getCat_name();
                        if(modi_cat.getModifier_selection()!=0)
                            modifier_name=modifier_name+" ("+mcontext.getString(R.string.txt_max)+modi_cat.getModifier_selection()+" "+mcontext.getString(R.string.txt_allowd)+")";
                        mlnm.add(modi_cat);
                    }
                    LinearLayout llmodi=new LinearLayout(mcontext);
                    if(mcatI%2==0 || ismob){
                        tr=new TableRow(mcontext);
                        tr.addView(llmodi,new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT, 1f));
                        tbl.addView(tr);
                    }else{
                        tr.addView(llmodi,new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT, 1f));
                    }
                    mcatI++;
                    TextView tvmodi=new TextView(mcontext);
                    tvmodi.setTextSize(18f);
                    tvmodi.setTextColor(mcontext.getResources().getColor(R.color.dark_grey));
                    tvmodi.setTypeface(AppConst.font_medium(mcontext));
                    tvmodi.setText(modifier_name);
                    llmodi.addView(tvmodi);
                    llmodi.setTag("0");
                    mlinear.add(llmodi);
                }

                i++;
                final String prefid = pre.getPreid().trim();
                final String Prenm = pre.getPrenm().trim();
                final String preprice = pre.getPreprice().trim();
                Typeface font = AppConst.font_regular(mcontext);
                if(ml.contains(pre.getPretype())) {
                    mpos = ml.indexOf(pre.getPretype());
                    View v = LayoutInflater.from(mcontext).inflate(R.layout.variation_row, null);
                    LinearLayout ll = (LinearLayout) v.findViewById(R.id.ll);
                    ll.setId(Integer.parseInt(pre.getPreid()));
                    final TextView txtqty=v.findViewById(R.id.tvqty);
                    ImageView iminc=v.findViewById(R.id.imgdinc);
                    ImageView imdec=v.findViewById(R.id.imgddec);
                    final CheckBox cb =v.findViewById(R.id.cb);
                    cb.setText(" " + Prenm + "\n "+ AppConst.currency + preprice);
                    cb.setTag(prefid+","+preprice);
                    cb.setTypeface(font);
                    cb.setHighlightColor(mcontext.getResources().getColor(R.color.colorPrimary));
                    mlinear.get(mpos).setOrientation(LinearLayout.VERTICAL);
                    mlinear.get(mpos).addView(v);
                    final int finalMpos = mpos;
                    txtqty.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {

                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                            txtqty.removeTextChangedListener(this);
                            String[] tokens = cb.getTag().toString().split(",", -1);
                            String id = tokens[0];
                            if(vidlist!=null && vidlist.contains(id)){
                                int vpos=vidlist.indexOf(id);
                                VarPojo selv=vlist.get(vpos);
                                selv.setQuantity(s.toString());
                                vlist.set(vpos,selv);
                            }
                            txtqty.addTextChangedListener(this);
                        }
                    });
                    iminc.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int q=Integer.parseInt(txtqty.getText().toString());
                            q++;
                            txtqty.setText(q+"");
                        }
                    });
                    imdec.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int q=Integer.parseInt(txtqty.getText().toString());
                            if(q>1) {
                                q--;
                                txtqty.setText(q+"");
                            }
                        }
                    });
                    cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                            //String currentprice =txtprice.getTag().toString();
                            String tag = buttonView.getTag().toString();
                            String[] tokens = tag.split(",", -1);
                            String id = tokens[0];
                            double extraprice = Double.parseDouble(tokens[1]);
                            int sel=0;
                            if(mlinear.get(finalMpos).getTag()!=null)
                                sel=Integer.parseInt(mlinear.get(finalMpos).getTag().toString());
                            if (isChecked) {
                                if(!vidlist.contains(id)) {
                                    Boolean allowadd=true;
                                    int ms=mlnm.get(finalMpos).getModifier_selection();
                                    if(ms==0)
                                        allowadd=true;
                                    else if(sel<ms)
                                        allowadd=true;
                                    else
                                        allowadd=false;
                                    if(allowadd) {
                                        vidlist.add(id);
                                        VarPojo v = new VarPojo();
                                        v.setId(id);
                                        v.setName(Prenm);
                                        v.setQuantity(txtqty.getText().toString());
                                        if (taxtype != null && taxtype.trim().length() > 0) {
                                            if (taxtype.equals("1")) {
                                                v.setAmount(extraprice + "");
                                                double newgst = extraprice - (extraprice * (100 / (100 + tax_per_tot)));
                                                double prewtax = extraprice - newgst;
                                                v.setAmount_wt(prewtax + "");
                                            } else if (taxtype.equals("0")) {
                                                double newgst = (extraprice * tax_per_tot) / 100;
                                                double priceperdish = extraprice + newgst;
                                                v.setAmount(priceperdish + "");
                                                v.setAmount_wt(preprice);
                                            }
                                        } else {
                                            v.setAmount(preprice);
                                            v.setAmount_wt(preprice);
                                        }
                                        vlist.add(v);
                                        sel++;
                                        mlinear.get(finalMpos).setTag(sel+"");
                                    }else{
                                        cb.setChecked(false);
                                        Toast.makeText(mcontext,"Max."+ms+" Allowed",Toast.LENGTH_SHORT).show();
                                    }
                                }
                            } else if (!isChecked) {
                                if(vidlist.contains(id)) {
                                    int vpos = vidlist.indexOf(id);
                                    vidlist.remove(vpos);
                                    vlist.remove(vpos);
                                    if(sel>0){
                                        sel--;
                                        mlinear.get(finalMpos).setTag(sel+"");
                                    }
                                }
                            }

                        }
                    });
                }

            }
            llmodifier.addView(tbl);
        }else{
            sv.setVisibility(View.GONE);
        }
        ivclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void addDish(final DishPojo model) {
        if(model.getPreflag().equals("true")) {
            displayDetail(model);
        }else if(model.getCombo_flag().equals("true")) {
            displaycombo(model);
        }else {
            if(M.isQtyDialog(mcontext) || model.getAllow_open_price().equals("true")){
                final int dish_id= Integer.valueOf(model.getDishid());
                final Dialog dialogqty=new Dialog(mcontext, R.style.MyDialogTheme);
                dialogqty.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogqty.setContentView(R.layout.dialog_dish_qty);
                dialogqty.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                TextView tvnm=(TextView)dialogqty.findViewById(R.id.txtcustdishname);
                TextView tvprice=(TextView)dialogqty.findViewById(R.id.txtcustdishprice);
                final EditText etnote=(EditText)dialogqty.findViewById(R.id.etnote);
                etnote.setTypeface(AppConst.font_regular(mcontext));
                final EditText etqty=(EditText)dialogqty.findViewById(R.id.edtqty);
                etqty.setTypeface(AppConst.font_regular(mcontext));
                ImageView ivclose=(ImageView)dialogqty.findViewById(R.id.ivclose);
                final Button btnadd=(Button)dialogqty.findViewById(R.id.custadd);
                btnadd.setTypeface(AppConst.font_regular(mcontext));
                Button buttonSeven=(Button)dialogqty.findViewById(R.id.buttonSeven);
                Button buttonEight=(Button)dialogqty.findViewById(R.id.buttonEight);
                Button buttonNine=(Button)dialogqty.findViewById(R.id.buttonNine);
                Button buttonFour=(Button)dialogqty.findViewById(R.id.buttonFour);
                Button buttonFive=(Button)dialogqty.findViewById(R.id.buttonFive);
                Button buttonSix=(Button)dialogqty.findViewById(R.id.buttonSix);
                Button buttonOne=(Button)dialogqty.findViewById(R.id.buttonOne);
                Button buttonTwo=(Button)dialogqty.findViewById(R.id.buttonTwo);
                Button buttonThree=(Button)dialogqty.findViewById(R.id.buttonThree);
                Button buttonClear=(Button)dialogqty.findViewById(R.id.buttonClear);
                Button buttonZero=(Button)dialogqty.findViewById(R.id.buttonZero);
                Button buttonEqual=(Button)dialogqty.findViewById(R.id.buttonEqual);
                Button btnaddcart=(Button)dialogqty.findViewById(R.id.btnadd);
                btnaddcart.setTypeface(AppConst.font_regular(mcontext));
                btnaddcart.setVisibility(View.GONE);
                final EditText etchange=(EditText)dialogqty.findViewById(R.id.etchangeprice);
                etchange.setTypeface(AppConst.font_regular(mcontext));
                LinearLayout llchangeprice=(LinearLayout)dialogqty.findViewById(R.id.llchangeprice);
                LinearLayout llqty=(LinearLayout)dialogqty.findViewById(R.id.llqty);
                if(model.getAllow_open_price().equals("true")) {
                    llchangeprice.setVisibility(View.VISIBLE);
                    dialogqty.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                    etchange.requestFocus();
                }else {
                    dialogqty.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                    llchangeprice.setVisibility(View.GONE);
                }
                if(M.isQtyDialog(mcontext)) {
                    llqty.setVisibility(View.VISIBLE);
                    btnaddcart.setVisibility(View.GONE);
                }else {
                    llqty.setVisibility(View.GONE);
                    btnaddcart.setVisibility(View.VISIBLE);
                }
                tvnm.setText(model.getDishname());
                if(M.isPriceWT(mcontext))
                    tvprice.setText(model.getPrice_without_tax());
                else
                    tvprice.setText(pf.setFormat(model.getPrice()));
                if (AppConst.selidlist!=null && AppConst.selidlist.contains(dish_id)){
                    final int pos= AppConst.selidlist.indexOf(dish_id);
                    DishOrderPojo selpos= AppConst.dishorederlist.get(pos);
                    etqty.setText(selpos.getQty());
                }

                btnadd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        hideKeyboard(dialogqty);
                        if(etqty.getText().toString().trim().length()<=0)
                            etqty.setText("1");
                        setDish(model,etqty.getText().toString(),etchange.getText().toString());
                        dialogqty.dismiss();
                    }
                });

                btnaddcart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        btnadd.performClick();
                    }
                });

                buttonZero.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        etqty.setText(etqty.getText() + "0");
                    }
                });

                buttonOne.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        etqty.setText(etqty.getText() + "1");
                    }
                });

                buttonTwo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        etqty.setText(etqty.getText() + "2");
                    }
                });

                buttonThree.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        etqty.setText(etqty.getText() + "3");
                    }
                });

                buttonFour.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        etqty.setText(etqty.getText() + "4");
                    }
                });

                buttonFive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        etqty.setText(etqty.getText() + "5");
                    }
                });

                buttonSix.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        etqty.setText(etqty.getText() + "6");
                    }
                });

                buttonSeven.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        etqty.setText(etqty.getText() + "7");
                    }
                });

                buttonEight.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        etqty.setText(etqty.getText() + "8");
                    }
                });

                buttonNine.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        etqty.setText(etqty.getText() + "9");
                    }
                });

                buttonEqual.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                });

                buttonClear.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(etqty.getText().length() > 0) {
                            CharSequence currentText = etqty.getText();
                            etqty.setText(currentText.subSequence(0, currentText.length()-1));
                        }else {
                            etqty.setText("");
                        }
                    }
                });

                ivclose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        hideKeyboard(dialogqty);
                        dialogqty.dismiss();
                    }
                });
                dialogqty.show();
            }else
                setDish(model,"1",null);
        }
    }

    private void setDish(DishPojo model, String qty, String change_price){
        tax_per_tot=0;
        final String dishid=model.getDishid();
        final int dish_id= Integer.valueOf(dishid);
        final String dishname=model.getDishname();
        final String price=model.getPrice();
        Double newgst=Double.parseDouble(model.getTax_amt());
        double priceperdish =Double.parseDouble(model.getPrice());
        double pwtax=Double.parseDouble(model.getPrice_without_tax());
        if(model.getTax_data()!=null && model.getTax_data().size()>0) {
            for (TaxData t : model.getTax_data()) {
                tax_per_tot = tax_per_tot + Float.parseFloat(t.getValue());
                taxtype=t.getTax_amount();
            }
        }
        if(change_price!=null && change_price.trim().length()>0) {
            if (change_price.equalsIgnoreCase(".")){
                newgst=0.0;
                priceperdish=0.0;
                pwtax=0.0;
            }else {
                Double cprice=Double.parseDouble(change_price);
                if (taxtype != null && taxtype.trim().length() > 0) {
                    if (taxtype.equals("1")) {
                        priceperdish=cprice;
                        newgst = cprice - (cprice * (100 / (100 + tax_per_tot)));
                        pwtax = cprice - newgst;
                    } else if (taxtype.equals("0")) {
                        pwtax=cprice;
                        newgst = (cprice * tax_per_tot) / 100;
                        priceperdish = cprice + newgst;
                    }
                } else {
                    priceperdish=cprice;
                    pwtax=cprice;
                    newgst=0.0;
                }
            }
        }
        custorder = new DishOrderPojo();
        Boolean isadd=true;
        if (AppConst.selidlist!=null && AppConst.selidlist.contains(dish_id) && !M.isCustomAllow(M.key_dinein_separate_item,mcontext)) {
            int noOfOccurs = Collections.frequency(AppConst.selidlist, dish_id);
            if(noOfOccurs==1) {
                final int pos = AppConst.selidlist.indexOf(dish_id);
                DishOrderPojo selpos = AppConst.dishorederlist.get(pos);
                if(selpos.isnew()) {
                    isadd=false;
                    long q = Long.parseLong(qty);
                    if(M.isQtyDialog(mcontext))
                        q= Long.parseLong(qty);
                    else
                        q= Long.parseLong(selpos.getQty())+1;
                    selpos.setQty(q + "");
                    if(model.getDiscount_amount()!=null && model.getDiscount_amount().trim().length()>0){
                        Double damt=q*Double.parseDouble(model.getDiscount_amount());
                        Double pamt=q*Double.parseDouble(selpos.getPrice());
                        if (damt > pamt)
                            selpos.setTot_disc(pf.setFormat(pamt+""));
                        else
                            selpos.setTot_disc(pf.setFormat(damt+""));
                    }
                    selpos.setOffer(model.getOffers());
                    selpos.setPrice(pf.setFormat(priceperdish+""));
                    selpos.setPrice_without_tax(pf.setFormat(pwtax+""));
                    selpos.setTax_amt(newgst+"");
                    AppConst.dishorederlist.set(pos, selpos);
                    EventBus.getDefault().post("updatetableorder");
                    if (issearch && mcontext instanceof SearchActivity) {
                        ((SearchActivity) mcontext).finish();
                    }
                }else
                    isadd=true;
            }else{
                for(int i = 0; i< AppConst.selidlist.size(); i++){
                    DishOrderPojo dishOrderPojo= AppConst.dishorederlist.get(i);
                    if(dishOrderPojo.getDishid().equals(dish_id+"") && dishOrderPojo.isnew()){
                        isadd=false;
                        long q= 0;
                        if(M.isQtyDialog(mcontext) || model.getAllow_open_price().equals("true"))
                            q= Long.parseLong(qty);
                        else
                            q= Long.parseLong(dishOrderPojo.getQty())+1;
                        dishOrderPojo.setQty(q + "");
                        if(model.getDiscount_amount()!=null && model.getDiscount_amount().trim().length()>0){
                            Double damt=q*Double.parseDouble(model.getDiscount_amount());
                            Double pamt=q*Double.parseDouble(dishOrderPojo.getPrice());
                            if (damt > pamt)
                                dishOrderPojo.setTot_disc(pf.setFormat(pamt+""));
                            else
                                dishOrderPojo.setTot_disc(pf.setFormat(damt+""));
                        }
                        dishOrderPojo.setOffer(model.getOffers());
                        dishOrderPojo.setPrice(pf.setFormat(priceperdish+""));
                        dishOrderPojo.setPrice_without_tax(pf.setFormat(pwtax+""));
                        dishOrderPojo.setTax_amt(newgst+"");
                        AppConst.dishorederlist.set(i, dishOrderPojo);
                        EventBus.getDefault().post("updatetableorder");
                        if (issearch && mcontext instanceof SearchActivity) {
                            ((SearchActivity) mcontext).finish();
                        }
                        break;
                    }
                }
            }
        } else {
            isadd=true;
        }
        if(isadd){
            custorder.setDishid(dishid);
            custorder.setDishname(dishname);
            custorder.setQty(qty);
            custorder.setIsnew(true);
            custorder.setStatus("0");
            custorder.setPrefid("0");
            custorder.setPrenm("");
            custorder.setOrderdetailid(null);
            custorder.setDiscount(model.getDiscount_amount());
            custorder.setDish_comment("");

            custorder.setPriceperdish(model.getPrice());
            custorder.setPrice(pf.setFormat(priceperdish+""));
            custorder.setPrice_without_tax(pf.setFormat(pwtax+""));
            custorder.setPriceper_without_tax(model.getPrice_without_tax());

            custorder.setCusineid(model.getCusineid());
            custorder.setDescription(model.getDescription());
            custorder.setDishimage(model.getDishimage());
            custorder.setPreflag(model.getPreflag());
            custorder.setPre(model.getPre());
            custorder.setInventory_item(model.getInventory_item());
            custorder.setSold_by(model.getSold_by());
            custorder.setTax_data(model.getTax_data());
            custorder.setTax_amt(newgst+"");
            custorder.setTot_tax(tax_per_tot+"");
            custorder.setHsn_no(model.getHsn_no());
            custorder.setAllow_open_price(model.getAllow_open_price());
            if(model.getDiscount_amount()!=null && model.getDiscount_amount().trim().length()>0){
                Double damt=Double.parseDouble(custorder.getQty())*Double.parseDouble(model.getDiscount_amount());
                Double pamt=Double.parseDouble(custorder.getQty())*Double.parseDouble(price);
                if (damt > pamt)
                    custorder.setTot_disc(pf.setFormat(pamt+""));
                else
                    custorder.setTot_disc(pf.setFormat(damt+""));
            }
            custorder.setOffer(model.getOffers());
            custorder.setTm(new Date().getTime()+"");
            AppConst.dishorederlist.add(custorder);
            AppConst.selidlist.add(dish_id);
            EventBus.getDefault().post("updatetableorder");
            if(issearch && mcontext instanceof SearchActivity){
                ((SearchActivity)mcontext).finish();
            }
        }
    }

    private void check_opening_bal(final DishPojo model) {
        if(connectionDetector.isConnectingToInternet()) {
            M.showLoadingDialog(mcontext);
            String restaurantid = M.getRestID(mcontext);
            String runiqueid = M.getRestUniqueID(mcontext);
            WaiterAPI mAuthenticationAPI = APIServiceHeader.createService(mcontext, WaiterAPI.class);
            Call<DailyBalPojo> call = mAuthenticationAPI.dailybalanceInfo(restaurantid, runiqueid);
            call.enqueue(new retrofit2.Callback<DailyBalPojo>() {
                @Override
                public void onResponse(Call<DailyBalPojo> call, retrofit2.Response<DailyBalPojo> response) {
                    if (response.isSuccessful()) {
                        DailyBalPojo pojo = response.body();
                        M.hideLoadingDialog();
                        if (pojo != null) {
                            if (pojo.getDaily_balance_addded().equals("false")) {
                                final Dialog dialog = new Dialog(mcontext);
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                dialog.setContentView(R.layout.dialog_opening_bal);
                                dialog.setCancelable(false);
                                final EditText etbal = (EditText) dialog.findViewById(R.id.etopeningbal);
                                ImageView ivclose=(ImageView)dialog.findViewById(R.id.ivclose);
                                Button btn = (Button) dialog.findViewById(R.id.btnsubmit);
                                etbal.setText("0");
                                etbal.setSelection(etbal.getText().toString().length());
                                btn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if (etbal.getText().toString().trim().length() <= 0)
                                            etbal.setError(mcontext.getString(R.string.empty_balance));
                                        else {
                                            String bal = etbal.getText().toString();
                                            dialog.dismiss();
                                            updateBal(model,bal, mcontext);
                                        }
                                    }
                                });

                                ivclose.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        dialog.dismiss();
                                    }
                                });
                                dialog.show();
                            } else {
                                String daily_balance_id = pojo.getBalance_id();
                                M.setdaily_balance_id(daily_balance_id, mcontext);
                                checkSoldType(model);
                            }
                        }
                    } else {
                        int statusCode = response.code();
                        ResponseBody errorBody = response.errorBody();
                        Log.d(TAG, "error:" + statusCode + " " + errorBody);
                        M.hideLoadingDialog();
                    }
                }

                @Override
                public void onFailure(Call<DailyBalPojo> call, Throwable t) {
                    M.hideLoadingDialog();
                    Log.d("response:", "fail:" + t.getMessage());
                }
            });
        }else{

        }
    }

    private void updateBal(final DishPojo model, String bal, final Context context) {
        M.showLoadingDialog(context);
        String restaurantid= M.getRestID(context);
        String runiqueid= M.getRestUniqueID(context);
        WaiterAPI mAuthenticationAPI = APIServiceHeader.createService(context, WaiterAPI.class);
        Call<SuccessPojo> call = mAuthenticationAPI.updateBalance(restaurantid,runiqueid,"opening", M.getWaiterid(context),bal, "",null,null,null);
        call.enqueue(new retrofit2.Callback<SuccessPojo>() {
            @Override
            public void onResponse(Call<SuccessPojo> call, retrofit2.Response<SuccessPojo> response) {
                if (response.isSuccessful()) {
                    SuccessPojo pojo = response.body();
                    M.hideLoadingDialog();
                    if(pojo!=null){
                        if(pojo.getSuccess()==1){
                            String daily_balance_id = pojo.getDaily_balance_id();
                            M.setdaily_balance_id(daily_balance_id, context);
                            checkSoldType(model);
                        }
                    }
                } else {
                    int statusCode = response.code();
                    ResponseBody errorBody = response.errorBody();
                    Log.d(TAG, "error:" + statusCode + " " + errorBody);
                    M.hideLoadingDialog();
                }
            }

            @Override
            public void onFailure(Call<SuccessPojo> call, Throwable t) {
                M.hideLoadingDialog();
                Log.d("response:", "fail:" + t.getMessage());
            }
        });
    }

    public void displaycombo(final DishPojo model){
        final Dialog dialog=new Dialog(mcontext, R.style.MyDialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = (LayoutInflater)mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.dialog_dish_detail, null);
        dialog.setContentView(layout);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ImageView ivclose=(ImageView)dialog.findViewById(R.id.ivclose);
        final TextView txtdname = (TextView) dialog.findViewById(R.id.txtcustdishname);
        final EditText edtqty=(EditText)dialog.findViewById(R.id.edtqty);
        ImageView imginc=(ImageView)dialog.findViewById(R.id.imgdinc);
        ImageView imgdec=(ImageView)dialog.findViewById(R.id.imgddec);
        final EditText etnote=(EditText)dialog.findViewById(R.id.etnote);
        final Button btnadd=(Button)dialog.findViewById(R.id.custadd);
        final TextView txtprice = (TextView) dialog.findViewById(R.id.txtcustdishprice);
        final EditText etchange=(EditText)dialog.findViewById(R.id.etchangeprice);
        etchange.setTypeface(AppConst.font_regular(mcontext));
        final LinearLayout llmodifier=(LinearLayout)dialog.findViewById(R.id.llmodifier);
        LinearLayout llchangeprice=(LinearLayout)dialog.findViewById(R.id.llchangeprice);
        if(model.getAllow_open_price().equals("true"))
            llchangeprice.setVisibility(View.VISIBLE);
        else
            llchangeprice.setVisibility(View.GONE);
        ScrollView sv=(ScrollView)dialog.findViewById(R.id.sv);
        if(ismob)
            llmodifier.setOrientation(LinearLayout.VERTICAL);
        else
            llmodifier.setOrientation(LinearLayout.HORIZONTAL);

        final String dishid=model.getDishid();
        final int dish_id= Integer.valueOf(dishid);
        final String dishname=model.getDishname();
        String price=model.getPrice();
        String comboflag=model.getCombo_flag();
        List<ComboModel> preflist=model.getCombo_item();

        txtdname.setText(dishname);
        if(M.isPriceWT(mcontext))
            txtprice.setText(AppConst.currency + " " + pf.setFormat(model.getPrice_without_tax()));
        else
            txtprice.setText(AppConst.currency + " " + pf.setFormat(price));
        txtprice.setTag(price);

        if (AppConst.selidlist!=null && AppConst.selidlist.contains(dish_id)) {
            int pos= AppConst.selidlist.indexOf(dish_id);
            DishOrderPojo selpos= AppConst.dishorederlist.get(pos);
            edtqty.setText(selpos.getQty());
        }

        imginc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                quan = Long.parseLong(edtqty.getText().toString());
                quan++;
                edtqty.setText(quan + "");
                edtqty.setSelection(edtqty.getText().length());

            }

        });
        imgdec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                quan =Long.parseLong(edtqty.getText().toString());
                if (quan == 1) {
                    return;
                } else {
                    quan--;
                    edtqty.setText(quan + "");
                }
                edtqty.setSelection(edtqty.getText().length());
            }
        });
        edtqty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(edtqty.getText()!=null && (edtqty.getText().toString().equalsIgnoreCase("0") || edtqty.getText().toString().trim().length()==0))
                    edtqty.setText("1");
                else if(edtqty.getText()!=null && edtqty.getText().toString().trim().length()>1)
                    edtqty.setMinimumWidth(50);
                edtqty.setSelection(edtqty.getText().length());
            }
        });
        btnadd.setTypeface(fontregular);

        btnadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float tax_tot=0f;
                if(model.getTax_data()!=null && model.getTax_data().size()>0) {
                    for (TaxData t : model.getTax_data()) {
                        tax_tot = tax_tot + Float.parseFloat(t.getValue());
                        taxtype=t.getTax_amount();
                    }
                }
                custorder = new DishOrderPojo();
                quan = Long.parseLong(edtqty.getText().toString());
                if (txtdname.getTag() == null)
                    txtdname.setTag("0");
                Double newgst=Double.parseDouble(model.getTax_amt());
                double priceperdish =Double.parseDouble(model.getPrice());
                double pwtax=Double.parseDouble(model.getPrice_without_tax());
                if(etchange.getText().toString().trim().length()>0) {
                    if (etchange.getText().toString().equalsIgnoreCase(".")){
                        newgst=0.0;
                        priceperdish=0.0;
                        pwtax=0.0;
                    }else {
                        Double cprice=Double.parseDouble(etchange.getText().toString());
                        if (taxtype != null && taxtype.trim().length() > 0) {
                            if (taxtype.equals("1")) {
                                priceperdish=cprice;
                                newgst = cprice - (cprice * (100 / (100 + tax_per_tot)));
                                pwtax = cprice - newgst;
                            } else if (taxtype.equals("0")) {
                                pwtax=cprice;
                                newgst = (cprice * tax_per_tot) / 100;
                                priceperdish = cprice + newgst;
                            }
                        } else {
                            priceperdish=cprice;
                            pwtax=cprice;
                            newgst=0.0;
                        }
                    }
                }

                custorder.setDishid(dishid);
                custorder.setDishname(dishname);
                custorder.setQty(""+quan);
                custorder.setIsnew(true);
                custorder.setCombo_flag("true");
                custorder.setCombo_list(model.getCombo_item());
                custorder.setStatus("0");
                custorder.setPrefid("0");

                custorder.setPrice(pf.setFormat(""+priceperdish));
                custorder.setPriceperdish(model.getPrice());
                custorder.setPrice_without_tax(pf.setFormat(pwtax+""));
                custorder.setPriceper_without_tax(model.getPrice_without_tax());

                custorder.setPrenm("");
                custorder.setOrderdetailid(null);
                custorder.setDiscount(model.getDiscount_amount());
                custorder.setDish_comment(etnote.getText().toString());
                custorder.setCusineid(model.getCusineid());
                custorder.setDescription(model.getDescription());
                custorder.setDishimage(model.getDishimage());
                custorder.setPreflag(model.getPreflag());
                custorder.setPre(model.getPre());
                custorder.setInventory_item(model.getInventory_item());
                custorder.setSold_by(model.getSold_by());
                custorder.setTax_data(model.getTax_data());
                custorder.setTot_tax(tax_tot+"");
                custorder.setSold_by(model.getSold_by());
                custorder.setTax_amt(newgst+"");
                if(model.getDiscount_amount()!=null && model.getDiscount_amount().trim().length()>0){
                    Double damt=quan*Double.parseDouble(model.getDiscount_amount());
                    Double pamt=quan*Double.parseDouble(custorder.getPrice());
                    if (damt > pamt)
                        custorder.setTot_disc(pf.setFormat(pamt+""));
                    else
                        custorder.setTot_disc(pf.setFormat(damt+""));
                }
                custorder.setOffer(model.getOffers());
                custorder.setHsn_no(model.getHsn_no());
                custorder.setAllow_open_price(model.getAllow_open_price());
                custorder.setTm(new Date().getTime()+"");
                AppConst.dishorederlist.add(custorder);
                AppConst.selidlist.add(dish_id);
                EventBus.getDefault().post("updatetableorder");
                if(issearch && mcontext instanceof SearchActivity){
                    View v1=((SearchActivity)mcontext).getCurrentFocus();
                    InputMethodManager inputManager = (InputMethodManager) mcontext.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(v1.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    ((SearchActivity)mcontext).finish();
                }
                hideKeyboard(dialog);
                dialog.dismiss();
            }

        });
        if(comboflag.equals("true")) {
            sv.setVisibility(View.VISIBLE);
            final ArrayList<LinearLayout> mlinear=new ArrayList<>();
            LinearLayout llmodi=new LinearLayout(mcontext);
            if(!ismob) {
                LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1f);
                llmodi.setLayoutParams(param);
            }else{
                LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, 1f);
                llmodi.setLayoutParams(param);
            }
            llmodifier.addView(llmodi);
            TextView tvmodi=new TextView(mcontext);
            tvmodi.setTextSize(18f);
            tvmodi.setTextColor(mcontext.getResources().getColor(R.color.dark_grey));
            tvmodi.setTypeface(AppConst.font_medium(mcontext));
            tvmodi.setText(dishname);
            llmodi.addView(tvmodi);
            llmodi.setTag("0");
            mlinear.add(llmodi);
            for(ComboModel pre : preflist)
            {
                int mpos=0;

                i++;
                final String prefid = pre.getItem_id().trim();
                final String Prenm = pre.getItem_name().trim();
                String preprice = pre.getQty().trim();
                Typeface font = AppConst.font_regular(mcontext);

                final TextView cb = new TextView(mcontext);
                cb.setText(" " + Prenm + "\tQty :"+pre.getQty() );
                cb.setTag(prefid+","+preprice);
                cb.setTextColor(mcontext.getResources().getColor(R.color.medium_grey));
                cb.setTextSize(15);
                LinearLayout.LayoutParams p=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                p.setMargins(0,7,0,7);
                cb.setLayoutParams(p);
                cb.setTypeface(font);
                cb.setHighlightColor(mcontext.getResources().getColor(R.color.colorPrimary));
                mlinear.get(mpos).setOrientation(LinearLayout.VERTICAL);
                mlinear.get(mpos).addView(cb);

            }
        }
        llmodifier.setTag("");

        ivclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(dialog);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void hideKeyboard(Dialog d){
        View view = d.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)mcontext.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void setunitChip(TextView tvpurchase,TextView tvused){
        int padding=tvpurchase.getPaddingLeft();
        if(tvpurchase.getTag().equals("1")){

            tvpurchase.setTextColor(mcontext.getResources().getColor(R.color.white));
            tvpurchase.setBackground(mcontext.getResources().getDrawable(R.drawable.chip_selected));

            tvused.setTextColor(mcontext.getResources().getColor(R.color.primary_text));
            tvused.setBackground(mcontext.getResources().getDrawable(R.drawable.chip));
        }else{
            tvused.setTextColor(mcontext.getResources().getColor(R.color.white));
            tvused.setBackground(mcontext.getResources().getDrawable(R.drawable.chip_selected));

            tvpurchase.setTextColor(mcontext.getResources().getColor(R.color.primary_text));
            tvpurchase.setBackground(mcontext.getResources().getDrawable(R.drawable.chip));
        }
        tvpurchase.setPadding(padding,padding,padding,padding);
        tvused.setPadding(padding,padding,padding,padding);
    }
}


