package com.royalpos.waiter.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.github.thunder413.datetimeutils.DateTimeStyle;
import com.github.thunder413.datetimeutils.DateTimeUtils;
import com.royalpos.waiter.POServerService;
import com.royalpos.waiter.PlaceOrder;
import com.royalpos.waiter.R;
import com.royalpos.waiter.WaiterActivity;
import com.royalpos.waiter.helper.AppConst;
import com.royalpos.waiter.helper.ConnectionDetector;
import com.royalpos.waiter.print.PrintFormat;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import de.greenrobot.event.EventBus;

public class ActiveCustomTableAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_ITEM = 1;
    private JSONArray list;
    Context context;
    String TAG="ActiveCustomTableAdapter";
    ConnectionDetector connectionDetector;
    PrintFormat pf;

    public ActiveCustomTableAdapter(JSONArray list, Context mcontext) {
        context = mcontext;
        this.list = list;
        connectionDetector=new ConnectionDetector(context);
        pf=new PrintFormat(context);
    }

    @Override
    public int getItemViewType(int position) {
        return TYPE_ITEM;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.active_table_row, parent, false);
        return new ViewHolderPosts(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        final JSONObject model;
        final ViewHolderPosts itemview = (ViewHolderPosts) holder;
        try {
            model = list.getJSONObject(position);
            final String tblnm=model.getString("sitting");
            final String tblid="0";
            final String otime=model.getString("order_time");
            final String oid=model.getString("order_id");
            final String ono=model.getString("order_no");
            final String token=model.getString("token");
            String unm=model.getString("usernm");
            final String uid=model.getString("userid");
            String amount=model.getString("amount");
            String offerid="",disc="",billdisc="",custnm="",custph="",sc="";
            if(model.has("offer_id"))
                offerid=model.getString("offer_id");
            if(model.has("discount"))
                disc=model.getString("discount");
            if(model.has("bill_amt_dis"))
                billdisc=model.getString("bill_amt_dis");
            if(model.has("cust_nm"))
                custnm=model.getString("cust_nm");
            if(model.has("cust_ph"))
                custph=model.getString("cust_ph");
            if(model.has("service_charge"))
                sc=model.getString("service_charge");

            itemview.tvnm.setText(""+tblnm);
            itemview.tvno.setText(token);
            String amt="0";
            if(amount==null)
                amt="0";
            else
                amt=amount;
            itemview.tvamount.setText(""+AppConst.currency+" "+pf.setFormat(amt));
            itemview.tvuser.setText(""+unm);
            if(otime!=null && otime.trim().length()>0) {
                try {
                    String timeAgo = DateTimeUtils.getTimeAgo(context, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(otime), DateTimeStyle.AGO_SHORT_STRING);
                    itemview.tvtime.setText(timeAgo);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            itemview.tvpeople.setVisibility(View.GONE);

            if(custnm.length()>0 || custph.length()>0) {
                itemview.tvcust.setVisibility(View.VISIBLE);
                itemview.tvcust.setText(custnm+" "+custph);
            }else
                itemview.tvcust.setVisibility(View.GONE);

            if(model.has("pending_count")) {
                itemview.llcnt.setVisibility(View.VISIBLE);
                itemview.tvcnt.setText(context.getString(R.string.pending_item_count) + ": " + model.getInt("pending_count"));
            }else
                itemview.llcnt.setVisibility(View.GONE);

            final String finalOfferid = offerid;
            final String finalDisc = disc;
            final String finalBilldisc = billdisc;
            final String finalCustnm = custnm;
            final String finalCustph = custph;
            final String finalSc = sc;
            itemview.lltable.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(AppConst.isMyServiceRunning(POServerService.class,context)) {
                        if (AppConst.dishorederlist == null)
                            AppConst.dishorederlist = new ArrayList<>();
                        AppConst.dishorederlist.clear();
                        if (AppConst.selidlist == null)
                            AppConst.selidlist = new ArrayList<>();
                        AppConst.selidlist.clear();
                        if (AppConst.placelist == null)
                            AppConst.placelist = new ArrayList<>();
                        AppConst.placelist.clear();

                        Intent it = new Intent(context, PlaceOrder.class);
                        it.putExtra("orderid", oid);
                        it.putExtra("tbl_id", tblid + "");
                        it.putExtra("tbl_nm", tblnm);
                        it.putExtra("token", token);
                        it.putExtra("order_no", ono);
                        it.putExtra("ordertime", otime);
                        it.putExtra("offerid", finalOfferid);
                        it.putExtra("discount", finalDisc);
                        it.putExtra("bill_dis", finalBilldisc);
                        it.putExtra("custnm", finalCustnm);
                        it.putExtra("custph", finalCustph);
                        it.putExtra("service_charge", finalSc);
                        ((WaiterActivity) context).finish();
                        ((WaiterActivity) context).startActivity(it);
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return list.length();
    }

    public class ViewHolderPosts extends RecyclerView.ViewHolder {
        TextView tvnm,tvamount,tvuser,tvtime,tvpeople,tvno,tvcust,txtsitting,tvcnt;
        Button btnchange;
        LinearLayout lltable,llcnt;

        public ViewHolderPosts(View itemView) {
            super(itemView);
            tvnm = (TextView) itemView.findViewById(R.id.tvtablenm);
            tvnm.setGravity(Gravity.CENTER_HORIZONTAL);
            tvamount = (TextView) itemView.findViewById(R.id.tvamount);
            tvuser = (TextView) itemView.findViewById(R.id.tvuser);
            tvtime=(TextView)itemView.findViewById(R.id.tvtime);
            tvpeople = (TextView) itemView.findViewById(R.id.tvpeople);
            tvno=(TextView)itemView.findViewById(R.id.tvno);
            tvcust=(TextView)itemView.findViewById(R.id.tvcust);
            txtsitting=(TextView)itemView.findViewById(R.id.txtsitting);
            txtsitting.setVisibility(View.VISIBLE);
            btnchange=(Button)itemView.findViewById(R.id.btnchange);
            btnchange.setTypeface(AppConst.font_regular(context));
            btnchange.setVisibility(View.GONE);
            lltable=(LinearLayout)itemView.findViewById(R.id.lltable);
            llcnt=(LinearLayout)itemView.findViewById(R.id.llitemcnt);
            tvcnt=(TextView)itemView.findViewById(R.id.tvcnt);
        }
    }
}
