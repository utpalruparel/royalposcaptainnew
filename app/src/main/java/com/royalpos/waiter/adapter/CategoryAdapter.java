package com.royalpos.waiter.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.royalpos.waiter.R;
import com.royalpos.waiter.helper.AppConst;
import com.royalpos.waiter.model.CuisineListPojo;
import com.royalpos.waiter.print.KitchencategoryActivity;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private static final int TYPE_ITEM = 1;
        private List<CuisineListPojo> list;
        Context context;
        String TAG="CategoryAdapter",selcat="";

        public CategoryAdapter(List<CuisineListPojo> list, Context mcontext) {
            context = mcontext;
            this.list = list;
        }

        @Override
        public int getItemViewType(int position) {
            return TYPE_ITEM;
        }


        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_row, parent, false);
            return new ViewHolderPosts(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

            final CuisineListPojo model=list.get(position);
            final ViewHolderPosts itemview = (ViewHolderPosts) holder;

            itemview.cb.setText(model.getCuisine_name());
            itemview.cb.setTag(model.getCuisine_id());
            itemview.cb.setTypeface(AppConst.font_regular(context));

            if(KitchencategoryActivity.selcategory.contains(model.getCuisine_id()))
                itemview.cb.setChecked(true);

            itemview.cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    String cid=compoundButton.getTag().toString();
                    if(b){
                        if(!KitchencategoryActivity.selcategory.contains(cid))
                            KitchencategoryActivity.selcategory.add(cid);
                    }else{
                        if(KitchencategoryActivity.selcategory.contains(cid))
                            KitchencategoryActivity.selcategory.remove(cid);
                    }
                }
            });
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public class ViewHolderPosts extends RecyclerView.ViewHolder {
            CheckBox cb;

            public ViewHolderPosts(View itemView) {
                super(itemView);
                cb = (CheckBox) itemView.findViewById(R.id.cb);
            }

        }

    }