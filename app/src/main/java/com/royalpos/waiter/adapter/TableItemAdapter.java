package com.royalpos.waiter.adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.net.wifi.WifiManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.Formatter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.royalpos.waiter.ItemStatusDialog;
import com.royalpos.waiter.POServerService;
import com.royalpos.waiter.PlaceOrder;
import com.royalpos.waiter.R;
import com.royalpos.waiter.database.DBDishes;
import com.royalpos.waiter.database.DBModifier;
import com.royalpos.waiter.database.DBUnit;
import com.royalpos.waiter.helper.AppConst;
import com.royalpos.waiter.helper.WifiHelper;
import com.royalpos.waiter.model.DishOrderPojo;
import com.royalpos.waiter.model.DishPojo;
import com.royalpos.waiter.model.M;
import com.royalpos.waiter.model.Modifier_cat;
import com.royalpos.waiter.model.PreModel;
import com.royalpos.waiter.model.TaxData;
import com.royalpos.waiter.model.UnitPojo;
import com.royalpos.waiter.model.VarPojo;
import com.royalpos.waiter.print.PrintFormat;
import com.royalpos.waiter.ui.chips.ChipCloud;
import com.royalpos.waiter.ui.chips.ChipListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;

import de.greenrobot.event.EventBus;
import dev.jokr.localnet.LocalClient;
import dev.jokr.localnet.models.Payload;
import in.arjsna.passcodeview.PassCodeView;

public class TableItemAdapter extends RecyclerView.Adapter<TableItemAdapter.ContentViewHolder> {

    View view;
    ContentViewHolder contentviewholder;
    private String TAG = "TableItemAdapter";
    long quan;
    public List<DishOrderPojo> dishorederlist = new ArrayList<DishOrderPojo>();
    Context mcontext;
    Typeface fontregular;
    String selectedid = "",unit_id="",unit_name="";
    Boolean ismob=false,isweight=false;
    PrintFormat pf;
    LocalClient localClient;
    double tax_per_tot=0f;
    String taxtype="";
    List<VarPojo> vlist=new ArrayList<>();
    List<String> vidlist=new ArrayList<>();

    public TableItemAdapter(Context mcontext) {
        if(AppConst.dishorederlist==null) {
            AppConst.dishorederlist = new ArrayList<DishOrderPojo>();
        }
        if(AppConst.selidlist==null) {
            AppConst.selidlist = new ArrayList<Integer>();
        }
        this.dishorederlist = AppConst.dishorederlist;
        this.mcontext = mcontext;
        fontregular = AppConst.font_regular(mcontext);
        if(mcontext.getResources().getBoolean(R.bool.portrait_only))
            ismob=true;
        else
            ismob=false;
        pf=new PrintFormat(mcontext);

        if(AppConst.isMyServiceRunning(POServerService.class,mcontext))
            localClient=new LocalClient(mcontext);
    }

    public class ContentViewHolder extends RecyclerView.ViewHolder{

        TextView txtdishname,tvqty,tvpref,tvprice,tvdiscount,tvstatus;
        ImageView tvinc,tvdec;
        LinearLayout lldish,llinv;
        ImageView ivcancel;

        public ContentViewHolder(View itemView) {
            super(itemView);
            txtdishname = (TextView) itemView.findViewById(R.id.tvdishname);
            tvpref=(TextView)itemView.findViewById(R.id.tvpref);
            tvqty=(TextView)itemView.findViewById(R.id.tvqty);
            tvprice=(TextView)itemView.findViewById(R.id.tvprice);
            tvdiscount=(TextView)itemView.findViewById(R.id.tvdiscount);
            tvinc=(ImageView)itemView.findViewById(R.id.imgdinc);
            tvdec=(ImageView)itemView.findViewById(R.id.imgddec);
            tvstatus=(TextView)itemView.findViewById(R.id.tvstatus);
            tvstatus.setVisibility(View.VISIBLE);
            lldish=(LinearLayout)itemView.findViewById(R.id.lldish);
            llinv=(LinearLayout)itemView.findViewById(R.id.llinv);
            ivcancel=(ImageView)itemView.findViewById(R.id.ivdel);
        }
    }


    @Override
    public TableItemAdapter.ContentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tbl_order_row, parent, false);
        contentviewholder = new ContentViewHolder(view);
        return contentviewholder;
    }

    public int newOrderCount(){
        int cnt=0;
        for(DishOrderPojo d:AppConst.dishorederlist){
            if(d.isnew())
                cnt++;
        }
        return cnt;
    }

    @Override
    public void onBindViewHolder(final TableItemAdapter.ContentViewHolder holder, final int position) {

        final DishOrderPojo model=dishorederlist.get(position);

        if(model.getSold_by().equals("each"))
            holder.txtdishname.setText(model.getDishname());
        else
            holder.txtdishname.setText(model.getDishname()+" X "+model.getWeight()+" "+model.getSort_nm());
        holder.txtdishname.setTypeface(fontregular);

        if(model.getPrenm()!=null && model.getPrenm().trim().length()>0) {
            holder.tvpref.setVisibility(View.VISIBLE);
            holder.tvpref.setText(model.getPrenm());
            holder.tvpref.setTypeface(fontregular);
        }else
            holder.tvpref.setVisibility(View.GONE);

        holder.tvqty.setText(model.getQty());
        holder.tvqty.setTypeface(fontregular);

        if(M.isPriceWT(mcontext) && model.getPrice_without_tax()!=null)
            holder.tvprice.setText(AppConst.currency + " " +pf.setFormat(model.getPrice_without_tax()));
        else
            holder.tvprice.setText(AppConst.currency + " " +model.getPrice());
        holder.tvprice.setTypeface(fontregular);

        holder.lldish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayDetail(model, position);
            }
        });
        holder.llinv.setVisibility(View.GONE);
        if(model.isnew()) {
            holder.tvstatus.setVisibility(View.GONE);
            holder.ivcancel.setVisibility(View.GONE);
            holder.tvinc.setVisibility(View.VISIBLE);
            holder.tvdec.setVisibility(View.VISIBLE);
            holder.lldish.setClickable(true);
            holder.lldish.setFocusable(true);
        }else {
            holder.tvstatus.setVisibility(View.VISIBLE);
            holder.ivcancel.setVisibility(View.VISIBLE);
            holder.tvinc.setVisibility(View.INVISIBLE);
            holder.tvdec.setVisibility(View.INVISIBLE);
            holder.lldish.setClickable(false);
            holder.lldish.setFocusable(false);
        }

        if(model.getTot_disc()!=null && model.getTot_disc().trim().length()>0 &&
                Double.parseDouble(model.getTot_disc())>0){
            holder.tvdiscount.setVisibility(View.VISIBLE);
            holder.tvdiscount.setText(mcontext.getString(R.string.txt_save)+": "+ AppConst.currency+" "+pf.setFormat(model.getTot_disc()));
        }else
            holder.tvdiscount.setVisibility(View.GONE);

        String status="";
        int clr=mcontext.getResources().getColor(R.color.primary_text);
        if(model.getStatus().equals("0")){
            status=mcontext.getString(R.string.status_0);
            clr=mcontext.getResources().getColor(R.color.status_new);
        }else if(model.getStatus().equals("1")){
            status=mcontext.getString(R.string.status_1);
            clr=mcontext.getResources().getColor(R.color.status_preparing);
        }else if(model.getStatus().equals("2")){
            status=mcontext.getString(R.string.status_2);
            clr=mcontext.getResources().getColor(R.color.status_served);
        }else if(model.getStatus().equals("3")){
            status=mcontext.getString(R.string.status_3);
        }else if(model.getStatus().equals("4")){
            status=mcontext.getString(R.string.status_4);
            clr=mcontext.getResources().getColor(R.color.status_prepared);
        }
        holder.tvstatus.setText(status.toUpperCase());
        holder.tvstatus.setBackgroundColor(clr);

        holder.tvstatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!model.getStatus().equals("3")){
                    ItemStatusDialog dialog=new ItemStatusDialog(mcontext,model,position);
                    dialog.show();
                }
            }
        });

        holder.tvinc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long quan = Long.parseLong(holder.tvqty.getText().toString());
                quan++;
                holder.tvqty.setText(quan + "");
                model.setQty(quan+"");
                if(model.getDiscount()!=null && model.getDiscount().trim().length()>0){
                    Double damt=quan*Double.parseDouble(model.getDiscount());
                    Double pamt=quan*Double.parseDouble(model.getPrice());
                    if (damt > pamt)
                        model.setTot_disc(pf.setFormat(pamt+""));
                    else
                        model.setTot_disc(pf.setFormat(damt+""));
                }else if(model.getDis_per()!=null && model.getDis_per().trim().length()>0){
                    Double dis_per=Double.parseDouble(model.getDis_per());
                    Double dis_amt = (dis_per * Double.parseDouble(model.getPrice())) / 100f;

                    Double damt=quan*dis_amt;
                    model.setTot_disc(pf.setFormat(damt+""));
                }
                AppConst.dishorederlist.set(position,model);
                EventBus.getDefault().post("updatetableorder");
            }
        });

        holder.tvdec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long quan = Long.parseLong(holder.tvqty.getText().toString());
                if (quan == 1) {
                    if(AppConst.dishorederlist.size()>position)
                        AppConst.dishorederlist.remove(position);
                    if(AppConst.selidlist.size()>position)
                        AppConst.selidlist.remove(position);
                    EventBus.getDefault().post("updatetableorder");
                } else {
                    quan--;
                    holder.tvqty.setText(quan + "");
                    model.setQty(quan+"");
                    if(model.getDiscount()!=null && model.getDiscount().trim().length()>0){
                        Double damt=quan*Double.parseDouble(model.getDiscount());
                        Double pamt=quan*Double.parseDouble(model.getPrice());
                        if (damt > pamt)
                            model.setTot_disc(pf.setFormat(pamt+""));
                        else
                            model.setTot_disc(pf.setFormat(damt+""));
                    }else if(model.getDis_per()!=null && model.getDis_per().trim().length()>0){
                        Double dis_per=Double.parseDouble(model.getDis_per());
                        Double dis_amt = (dis_per * Double.parseDouble(model.getPrice())) / 100f;

                        Double damt=quan*dis_amt;
                        model.setTot_disc(pf.setFormat(damt+""));
                    }
                    AppConst.dishorederlist.set(position,model);
                    EventBus.getDefault().post("updatetableorder");
                }
            }
        });

        holder.ivcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(M.isCustomAllow(M.key_dinein_delitem,mcontext)){
                    new AlertDialog.Builder(mcontext)
                            .setTitle(R.string.dialog_title_cancel_item)
                            .setMessage(mcontext.getString(R.string.dialog_cancel_item_alert))
                            .setNegativeButton(android.R.string.no, null)
                            .setPositiveButton(android.R.string.yes,
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface arg0, int arg1) {
                                            try {
                                                JSONObject cancelObj=new JSONObject();
                                                cancelObj.put("key",WifiHelper.api_cancel_order_item);
                                                cancelObj.put("orderdetail_id",model.getOrderdetailid());
                                                cancelObj.put("order_id",((PlaceOrder)mcontext).orderData.getId());
                                                cancelObj.put("user_ip",getLocalIp());
                                                sendToServer(cancelObj+"");
                                            }catch (JSONException e){}
                                        }
                                    }).create().show();
                }else{
                    final Dialog dialog1 = new Dialog(mcontext, android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen);
                    dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog1.setContentView(R.layout.dialog_waiter_password);
                    dialog1.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                    TextView tvtitle=(TextView)dialog1.findViewById(R.id.tvtitle);
                    tvtitle.setVisibility(View.VISIBLE);
                    tvtitle.setText(mcontext.getString(R.string.dialog_title_cancel_item));
                    TextView tvmsg=(TextView)dialog1.findViewById(R.id.tvmsg);
                    tvmsg.setVisibility(View.VISIBLE);
                    tvmsg.setText(mcontext.getString(R.string.dialog_cancel_item_alert_pin));
                    TextView tv=(TextView)dialog1.findViewById(R.id.tv);
                    tv.setText(mcontext.getString(R.string.type_outlet_pin));
                    PassCodeView circlePinField=(PassCodeView)dialog1.findViewById(R.id.type_pin);
                    circlePinField.setOnTextChangeListener(new PassCodeView.TextChangeListener() {
                        @Override
                        public void onTextChanged(String pin) {
                            if(pin.length()==4) {
                                if (pin.equalsIgnoreCase(M.getOutletPin(mcontext))) {
                                    dialog1.dismiss();
                                    try {
                                        JSONObject cancelObj=new JSONObject();
                                        cancelObj.put("key",WifiHelper.api_cancel_order_item);
                                        cancelObj.put("orderdetail_id",model.getOrderdetailid());
                                        cancelObj.put("order_id",((PlaceOrder)mcontext).orderData.getId());
                                        cancelObj.put("user_ip",getLocalIp());
                                        sendToServer(cancelObj+"");
                                    }catch (JSONException e){}
                                } else
                                    Toast.makeText(mcontext, R.string.invalid_pwd, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                    dialog1.show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return dishorederlist.size();
    }

    public void displayDetail(final DishOrderPojo model, final int spos){
        selectedid = "";unit_id="";unit_name="";
        tax_per_tot=0;
        taxtype="";
        if(vidlist==null)
            vidlist=new ArrayList<>();
        if(vlist==null)
            vlist=new ArrayList<>();
        vidlist.clear();
        vlist.clear();
        final Dialog dialog=new Dialog(mcontext, android.R.style.Theme_Black);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        LayoutInflater inflater = (LayoutInflater)mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.dialog_update_dish_detail, null);
        dialog.setContentView(layout);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        final TextView txtdname = (TextView) dialog.findViewById(R.id.txtcustdishname);
        final TextView edtqty=(TextView) dialog.findViewById(R.id.edtqty);
        final EditText etdishcomment = (EditText)dialog.findViewById(R.id.etnote);
        etdishcomment.setTypeface(AppConst.font_regular(mcontext));
        ImageView imginc=(ImageView)dialog.findViewById(R.id.imgdinc);
        ImageView imgdec=(ImageView)dialog.findViewById(R.id.imgddec);
        final Button btnupdate=(Button)dialog.findViewById(R.id.btnsave);
        btnupdate.setTypeface(AppConst.font_regular(mcontext));
        final Button btndelete=(Button)dialog.findViewById(R.id.btnremove);
        btndelete.setTypeface(AppConst.font_regular(mcontext));
        final TextView txtprice = (TextView) dialog.findViewById(R.id.txtcustdishprice);
        final LinearLayout llmodifier=(LinearLayout)dialog.findViewById(R.id.llmodifier);
        LinearLayout llcomp=(LinearLayout)dialog.findViewById(R.id.llcomp);
        ScrollView sv=(ScrollView)dialog.findViewById(R.id.sv);
        final ToggleButton tb=(ToggleButton)dialog.findViewById(R.id.tb);
        View dview=(View)dialog.findViewById(R.id.dview);
        llmodifier.setOrientation(LinearLayout.VERTICAL);
        final LinearLayout lldiscount=(LinearLayout)dialog.findViewById(R.id.lldiscount);
        lldiscount.setTag("-1");
        final LinearLayout lldiscper=(LinearLayout)dialog.findViewById(R.id.lldis_per);
        lldiscper.setVisibility(View.GONE);
        final EditText etdisamt=(EditText)dialog.findViewById(R.id.etdisamt);
        etdisamt.setTypeface(AppConst.font_regular(mcontext));
        RadioButton rbper=(RadioButton)dialog.findViewById(R.id.rbper);
        rbper.setTypeface(AppConst.font_regular(mcontext));
        RadioButton rbamt=(RadioButton)dialog.findViewById(R.id.rbamount);
        rbamt.setTypeface(AppConst.font_regular(mcontext));
        TextView ivclose=(TextView) dialog.findViewById(R.id.ivclose);
        //weight
        LinearLayout llweightype=(LinearLayout)dialog.findViewById(R.id.llweighttype);
        final TextView tvweight=(TextView)dialog.findViewById(R.id.tvweight);
        final TextView tvprice=(TextView)dialog.findViewById(R.id.tvprice);
        final TextView tvamt=(TextView)dialog.findViewById(R.id.tvpriceamount);
        final TextView tvcntweight=(TextView)dialog.findViewById(R.id.tvcntweight);
        final TextView tvpurchase=(TextView)dialog.findViewById(R.id.tvpurchase);
        tvpurchase.setText(model.getPurchased_unit_name());
        final TextView tvused=(TextView)dialog.findViewById(R.id.tvused);
        tvused.setText(model.getUsed_unit_name());

        final EditText etweight=(EditText)dialog.findViewById(R.id.etweight);
        etweight.setTypeface(AppConst.font_regular(mcontext));
        final EditText etamt=(EditText)dialog.findViewById(R.id.etamount);
        etamt.setTypeface(AppConst.font_regular(mcontext));
        Button buttonSeven = (Button)dialog.findViewById(R.id.buttonSeven);
        Button buttonEight= (Button)dialog.findViewById(R.id.buttonEight);
        Button buttonNine= (Button)dialog.findViewById(R.id.buttonNine);
        Button buttonFour = (Button)dialog.findViewById(R.id.buttonFour);
        Button buttonFive= (Button)dialog.findViewById(R.id.buttonFive);
        Button buttonSix= (Button)dialog.findViewById(R.id.buttonSix);
        Button buttonOne= (Button)dialog.findViewById(R.id.buttonOne);
        Button buttonTwo= (Button)dialog.findViewById(R.id.buttonTwo);
        Button buttonThree = (Button)dialog.findViewById(R.id.buttonThree);
        Button buttonClear = (Button)dialog.findViewById(R.id.buttonClear);
        Button buttonZero = (Button)dialog.findViewById(R.id.buttonZero);
        Button buttonEqual = (Button)dialog.findViewById(R.id.buttonEqual);
        buttonEqual.setText(".");
        final LinearLayout llw=(LinearLayout)dialog.findViewById(R.id.llweight);
        final LinearLayout llp=(LinearLayout)dialog.findViewById(R.id.llprice);
        LinearLayout llchangeprice=(LinearLayout)dialog.findViewById(R.id.llchangeprice);
        final EditText etchange=(EditText)dialog.findViewById(R.id.etchangeprice);
        etchange.setTypeface(AppConst.font_regular(mcontext));
        ChipCloud cc_disc=(ChipCloud)dialog.findViewById(R.id.cc_disc);
        final EditText etdiscount=(EditText)dialog.findViewById(R.id.etdiscount);
        etdiscount.setTypeface(fontregular);
        if(model.getAllow_open_price().equals("true")) {
            llchangeprice.setVisibility(View.VISIBLE);
            etchange.setText(model.getPrice());
        }else
            llchangeprice.setVisibility(View.GONE);
        if(M.getBrandId(mcontext).equals("schmi194")) {
            llcomp.setVisibility(View.GONE);
            lldiscount.setVisibility(View.GONE);
        }
        final double dishprice =  Double.valueOf(model.getPriceperdish());
        final String dishid=model.getDishid();
        final String dishname=model.getDishname();
        String price=model.getPrice();
        String preflag=model.getPreflag();

        List<PreModel> preflist=model.getPre();

        txtdname.setText(dishname);
        if(M.isPriceWT(mcontext))
            txtprice.setText(AppConst.currency + " " + pf.setFormat(model.getPriceper_without_tax()));
        else
            txtprice.setText(AppConst.currency + " " + pf.setFormat(model.getPriceperdish()));

        if(model.getTax_data()!=null && model.getTax_data().size()>0) {
            for (TaxData t : model.getTax_data()) {
                tax_per_tot = tax_per_tot + Float.parseFloat(t.getValue());
                taxtype=t.getTax_amount();
            }
        }

        edtqty.setText(model.getQty());

        rbamt.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b) {
                    lldiscper.setVisibility(View.GONE);
                    etdisamt.setVisibility(View.VISIBLE);
                }
                etdisamt.setSelection(etdisamt.getText().length());
            }
        });

        rbper.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b) {
                    lldiscper.setVisibility(View.VISIBLE);
                    etdisamt.setVisibility(View.GONE);
                }
            }
        });

        etdisamt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String pflag=model.getPreflag();
                if(model.getPreflag()==null || model.getPreflag().isEmpty())
                    pflag="false";
                if(model.getSold_by().equals("each") && pflag.equals("false")) {
                    etdisamt.removeTextChangedListener(this);
                    if (etdisamt.getText().toString().trim().length() > 0 &&
                            !etdisamt.getText().toString().equals(".")) {
                        Double samt = Double.parseDouble(model.getPrice());
                        if (etchange.getVisibility() == View.VISIBLE && etchange.getText().toString().trim().length() > 0)
                            samt = Double.parseDouble(etchange.getText().toString());
                        Double damt = Double.parseDouble(etdisamt.getText().toString());
                        if (samt < damt)
                            etdisamt.setText(samt + "");
                    }
                    etdisamt.setSelection(etdisamt.getText().length());
                    etdisamt.addTextChangedListener(this);
                }
            }
        });

        final String[] dilist=new String[]{"5","10","20","25", mcontext.getString(R.string.custom)};
        new ChipCloud.Configure()
                .chipCloud(cc_disc)
                .selectedFontColor(mcontext.getResources().getColor(R.color.white))
                .deselectedFontColor(mcontext.getResources().getColor(R.color.medium_grey))
                .selectTransitionMS(500)
                .deselectTransitionMS(250)
                .mode(ChipCloud.Mode.SINGLE)
                .labels(dilist).typeface(AppConst.font_medium(mcontext))
                .allCaps(true)
                .gravity(ChipCloud.Gravity.LEFT)
                .textSize(mcontext.getResources().getDimensionPixelSize(R.dimen.chip_textsize))
                .verticalSpacing(mcontext.getResources().getDimensionPixelSize(R.dimen.vertical_spacing))
                .minHorizontalSpacing(mcontext.getResources().getDimensionPixelSize(R.dimen.min_horizontal_spacing))
                .chipListener(new ChipListener() {
                    @Override
                    public void chipSelected(int index) {
                        lldiscount.setTag(index+"");
                        if(index==dilist.length-1)
                            etdiscount.setVisibility(View.VISIBLE);
                        else
                            etdiscount.setVisibility(View.GONE);
                    }
                    @Override
                    public void chipDeselected(int index) {
                        lldiscount.setTag("-1");
                    }
                })
                .build();
        if(model.getDis_per()!=null && !model.getDis_per().isEmpty() && Double.parseDouble(model.getDis_per())!=0){
            List<String> dl = new ArrayList<String>(Arrays.asList(dilist));
            String selper=model.getDis_per();
            if(selper.contains(".")){
                double onlydecimal=Double.parseDouble("0"+selper.substring(selper.indexOf(".")));
                if(onlydecimal==0)
                    selper=selper.substring(0,selper.indexOf("."));
            }
            if(dl.contains(selper)) {
                cc_disc.setSelectedChip(dl.indexOf(selper));
                rbper.setChecked(true);
            }else {
                Double damt=Double.parseDouble(model.getTot_disc())/Integer.parseInt(model.getQty());
                etdisamt.setText(damt+"");
                rbamt.setChecked(true);
            }
        }

        imginc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                quan = Long.parseLong(edtqty.getText().toString());
                quan++;
                edtqty.setText(quan + "");

            }

        });
        imgdec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                quan = Long.parseLong(edtqty.getText().toString());
                if (quan == 1) {
                    return;
                } else {
                    quan--;
                    edtqty.setText(quan + "");
                }
            }
        });

        btnupdate.setTypeface(fontregular);
        btndelete.setTypeface(fontregular);
        btndelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(AppConst.dishorederlist.size()>spos)
                    AppConst.dishorederlist.remove(spos);
                if(AppConst.selidlist.size()>spos)
                    AppConst.selidlist.remove(spos);
                dialog.dismiss();
                EventBus.getDefault().post("updatetableorder");
            }
        });
        btnupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DishOrderPojo custorder = new DishOrderPojo();
                quan = Long.parseLong(edtqty.getText().toString());
                String poscustdisid = dishid;
                String poscustdishname = dishname;
                if(txtdname.getTag()==null)
                    txtdname.setTag("0");

                DBDishes dbDishes=new DBDishes(mcontext);
                DishPojo dishPojo=dbDishes.getDishData(model.getDishid(),mcontext);
                Double newgst=Double.parseDouble(dishPojo.getTax_amt());
                double priceperdish =Double.parseDouble(model.getPriceperdish());
                double pwtax=Double.parseDouble(model.getPriceper_without_tax());

                String finalid = "", finalnm = "";
                String dish_comment = etdishcomment.getText().toString();

                if(vlist!=null && vlist.size()>0){
                    double vtot=0,vtot_wt=0;
                    for(VarPojo v1:vlist) {
                        Double vamt=Integer.parseInt(v1.getQuantity())*Double.parseDouble(v1.getAmount());
                        Double vamtwt=Integer.parseInt(v1.getQuantity())*Double.parseDouble(v1.getAmount_wt());
                        vtot=vtot+vamt;
                        vtot_wt=vtot_wt+vamtwt;
                        if(finalid.trim().length()==0) {
                            finalid = v1.getId();
                            finalnm=v1.getQuantity()+" X "+v1.getName();
                        }else {
                            finalid = finalid + "," + v1.getId();
                            finalnm=finalnm+","+v1.getQuantity()+" X "+v1.getName();
                        }
                    }
                    newgst=newgst+(vtot-vtot_wt);
                    priceperdish=priceperdish+vtot;
                    pwtax=pwtax+vtot_wt;
                    custorder.setVarPojoList(vlist);
                }else
                    custorder.setVarPojoList(new ArrayList<VarPojo>());

                String weight="",amt="",amtwithouttax="";
                if(model.getSold_by().equals("weight")){
                    if(isweight){
                        weight=etweight.getText()+"";
                        amt=tvamt.getTag()+"";
                    }else{
                        amt=etamt.getText()+"";
                        weight=tvcntweight.getTag()+"";
                    }

                    String snm="";
                    DBUnit dbUnit=new DBUnit(mcontext);
                    if(unit_id.equals(model.getPurchased_unit_id())) {
                        snm = dbUnit.getSortName(model.getPurchased_unit_id(), null);
                        if(snm==null && model.getPurchased_unit_id().equals(model.getUsed_unit_id()))
                            snm=dbUnit.getSortName(null,model.getUsed_unit_id());
                    }else
                        snm=dbUnit.getSortName(null,model.getUsed_unit_id());
                    if(snm==null)
                        snm=unit_name;
                    custorder.setSort_nm(snm);
                    if(llp.getTag()!=null)
                        amtwithouttax=llp.getTag()+"";
                    float tamt=Float.parseFloat(amt)-Float.parseFloat(amtwithouttax);

                    custorder.setWeight(weight);
                    custorder.setPrice(pf.setFormat(amt));
                    custorder.setPrice_without_tax(amtwithouttax);
                    custorder.setTax_amt(tamt+"");
                    custorder.setUnitid(unit_id);
                    custorder.setUnitname(unit_name);
                }else {
                    custorder.setTax_amt(model.getTax_amt());
                    custorder.setUnitid("");
                    custorder.setUnitname("");
                    custorder.setPrice_without_tax(pwtax+"");
                    custorder.setPrice(pf.setFormat(priceperdish+""));
                    custorder.setTax_amt(newgst+"");
                }
                if(etchange.getText().toString().trim().length()>0) {
                    if (etchange.getText().toString().equalsIgnoreCase(".")){
                        custorder.setTax_amt("0");
                        custorder.setPrice("0");
                        custorder.setPrice_without_tax("0");
                    }else {
                        Double cprice=Double.parseDouble(etchange.getText().toString());
                        if (taxtype != null && taxtype.trim().length() > 0) {
                            if (taxtype.equals("1")) {
                                priceperdish=cprice;
                                newgst = cprice - (cprice * (100 / (100 + tax_per_tot)));
                                pwtax = cprice - newgst;
                            } else if (taxtype.equals("0")) {
                                pwtax=cprice;
                                newgst = (cprice * tax_per_tot) / 100;
                                priceperdish = cprice + newgst;
                            }
                        } else {
                            priceperdish=cprice;
                            pwtax=cprice;
                            newgst=0.0;
                        }
                        custorder.setTax_amt(newgst+"");
                        custorder.setPrice(pf.setFormat(priceperdish+""));
                        custorder.setPrice_without_tax(pwtax+"");
                    }
                }
                Double dis_per = 0.0,disamt=0.0;
                if(!tb.isChecked() && lldiscount.getVisibility()==View.VISIBLE) {
                    if(lldiscper.getVisibility()==View.VISIBLE && lldiscount.getTag()!=null &&
                            !lldiscount.getTag().toString().equals("-1")) {
                        int dis_pos = Integer.parseInt(lldiscount.getTag().toString());
                        if (dis_pos == dilist.length - 1) {
                            dis_per = Double.parseDouble(etdiscount.getText().toString());
                        } else {
                            dis_per = Double.parseDouble(dilist[dis_pos]);
                        }
                    }else if(etdisamt.getVisibility()==View.VISIBLE && etdisamt.getText().toString().trim().length()>0){
                        disamt=Double.parseDouble(etdisamt.getText().toString());
                        if(Double.parseDouble(custorder.getPrice())<disamt)
                            disamt=Double.parseDouble(custorder.getPrice());
                    }
                }

                if(tb.isChecked()){
                    custorder.setTax_amt("0");
                    custorder.setPrice("0");
                    custorder.setPrice_without_tax("0");
                }else if(dis_per>0) {
                    Double dis_amt = (dis_per * Double.parseDouble(custorder.getPrice())) / 100f;

                    Double damt=quan*dis_amt;
                    custorder.setTot_disc(pf.setFormat(damt+""));
                    custorder.setDis_per(dis_per+"");
                }else if(disamt>0){
                    Double disper=(disamt*100)/Double.parseDouble(custorder.getPrice());
                    Double damt=quan*disamt;
                    custorder.setTot_disc(pf.setFormat(damt+""));
                    custorder.setDis_per(disper+"");
                }else if(model.getDiscount()!=null && model.getDiscount().trim().length()>0){
                    Double damt=quan*Double.parseDouble(model.getDiscount());
                    Double pamt=quan*Double.parseDouble(custorder.getPrice());
                    if (damt > pamt)
                        custorder.setTot_disc(pf.setFormat(pamt+""));
                    else
                        custorder.setTot_disc(pf.setFormat(damt+""));
                    custorder.setDis_per("0");
                }

                custorder.setDishid(poscustdisid);
                custorder.setDishname(poscustdishname);
                custorder.setQty(quan + "");
                custorder.setIsnew(true);
                custorder.setStatus("0");
                custorder.setPrefid(finalid);
                custorder.setPrenm(finalnm);
                custorder.setOrderdetailid(null);
                custorder.setDiscount(model.getDiscount());
                custorder.setDish_comment(dish_comment);
                custorder.setComment("");
                custorder.setPreflag(model.getPreflag());
                custorder.setCusineid(model.getCusineid());
                custorder.setPriceperdish(model.getPriceperdish());
                custorder.setPriceper_without_tax(model.getPriceper_without_tax());
                custorder.setPre(model.getPre());
                custorder.setInventory_item(model.getInventory_item());
                custorder.setSold_by(model.getSold_by());
                custorder.setPurchased_unit_id(model.getPurchased_unit_id());
                custorder.setPurchased_unit_name(model.getPurchased_unit_name());
                custorder.setUsed_unit_id(model.getUsed_unit_id());
                custorder.setUsed_unit_name(model.getUsed_unit_name());
                custorder.setDefault_sale_weight(model.getDefault_sale_weight());
                custorder.setTax_data(model.getTax_data());
                custorder.setTot_tax(model.getTot_tax());
                custorder.setOffer(model.getOffer());
                custorder.setHsn_no(model.getHsn_no());
                custorder.setAllow_open_price(model.getAllow_open_price());
                AppConst.dishorederlist.set(spos, custorder);
                notifyDataSetChanged();
                EventBus.getDefault().post("updatetableorder");
                dialog.dismiss();

            }
        });

        llmodifier.setTag("");
        sv.setVisibility(View.GONE);
        dview.setVisibility(View.GONE);
        if(model.getSold_by().equals("weight")){
            isweight=true;
            sv.setVisibility(View.GONE);
            dview.setVisibility(View.VISIBLE);
            llweightype.setVisibility(View.VISIBLE);
            llmodifier.setVisibility(View.GONE);
            etweight.setText(model.getWeight());
            etamt.setText(model.getPrice());
            etweight.setSelection(etweight.getText().length());
            etamt.setSelection(etamt.getText().length());
            txtprice.setVisibility(View.GONE);
            if(M.isPriceWT(mcontext))
                tvamt.setText(mcontext.getString(R.string.txt_amount)+" : "+ AppConst.currency + " " +model.getPrice_without_tax());
            else
                tvamt.setText(mcontext.getString(R.string.txt_amount)+" : "+ AppConst.currency + " " +model.getPrice());
            tvamt.setTag(model.getPrice());
            tvcntweight.setText(mcontext.getString(R.string.txt_weight)+" : "+model.getWeight()+" "+model.getUnitname());
            tvcntweight.setTag(model.getWeight());
            llp.setTag(model.getPrice_without_tax());
            unit_id=model.getUnitid();
            unit_name=model.getUnitname();
            if(model.getPurchased_unit_id().equals(model.getUsed_unit_id())) {
                tvused.setVisibility(View.GONE);
            }else {
                tvused.setVisibility(View.VISIBLE);
                tvpurchase.setTag("0");
            }
        }else if(preflag.equals("true")) {
            sv.setVisibility(View.VISIBLE);
            dview.setVisibility(View.VISIBLE);
            llweightype.setVisibility(View.GONE);
            List<String> alist=new ArrayList<>();
            TableLayout tbl=new TableLayout(mcontext);
            tbl.setStretchAllColumns(true);
            TableRow tr=null;
            if(model.getPrefid()!=null && model.getPrefid().trim().length()>0 && !model.getPrefid().equals("0")) {
                String prelist = model.getPrefid().trim();
                alist = new ArrayList<String>(Arrays.asList(prelist.split(",")));
                vidlist=alist;
                vlist=model.getVarPojoList();
            }

            int i=0,mcatI=0;
            ArrayList<String> ml=new ArrayList<>();
            final ArrayList<Modifier_cat> mlnm=new ArrayList<>();
            final ArrayList<LinearLayout> mlinear=new ArrayList<>();
            for(PreModel pre : preflist)
            {
                String modifier_name="";
                int mpos=0;
                if(!ml.contains(pre.getPretype())) {
                    ml.add(pre.getPretype());
                    DBModifier dbm=new DBModifier(mcontext);
                    Modifier_cat modi_cat=dbm.getModifierName(pre.getPretype());
                    modifier_name=modi_cat.getCat_name();
                    if(modi_cat.getModifier_selection()!=0)
                        modifier_name=modifier_name+" ("+mcontext.getString(R.string.txt_max)+modi_cat.getModifier_selection()+" "+mcontext.getString(R.string.txt_allowd)+")";
                    mlnm.add(modi_cat);

                    LinearLayout llmodi=new LinearLayout(mcontext);
                    if(mcatI%2==0 || ismob){
                        tr=new TableRow(mcontext);
                        tr.addView(llmodi,new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT, 1f));
                        tbl.addView(tr);
                    }else{
                        tr.addView(llmodi,new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT, 1f));
                    }
                    mcatI++;
                    TextView tvmodi=new TextView(mcontext);
                    tvmodi.setTextSize(18f);
                    tvmodi.setTextColor(mcontext.getResources().getColor(R.color.dark_grey));
                    tvmodi.setTypeface(AppConst.font_medium(mcontext));
                    tvmodi.setText(modifier_name);
                    llmodi.addView(tvmodi);
                    mlinear.add(llmodi);
                }

                i++;
                final String prefid = pre.getPreid().trim();
                final String Prenm = pre.getPrenm().trim();
                final String preprice = pre.getPreprice().trim();
                Typeface font = AppConst.font_regular(mcontext);
                if(ml.contains(pre.getPretype())) {
                    mpos = ml.indexOf(pre.getPretype());
                    View v = LayoutInflater.from(mcontext).inflate(R.layout.variation_row, null);
                    LinearLayout ll = (LinearLayout) v.findViewById(R.id.ll);
                    ll.setId(Integer.parseInt(pre.getPreid()));
                    final TextView txtqty=v.findViewById(R.id.tvqty);
                    ImageView iminc=v.findViewById(R.id.imgdinc);
                    ImageView imdec=v.findViewById(R.id.imgddec);
                    final CheckBox cb =v.findViewById(R.id.cb);
                    cb.setText(" " + Prenm + "\n "+ AppConst.currency + preprice);
                    cb.setTag(prefid+","+preprice);
                    cb.setTypeface(font);
                    cb.setHighlightColor(mcontext.getResources().getColor(R.color.colorPrimary));
                    mlinear.get(mpos).setOrientation(LinearLayout.VERTICAL);
                    mlinear.get(mpos).addView(v);
                    if(alist!=null && alist.size()>0 && alist.contains(prefid)) {
                        cb.setChecked(true);
                        VarPojo selv=model.getVarPojoList().get(alist.indexOf(prefid));
                        if(selv!=null){
                            txtqty.setText(selv.getQuantity());
                        }
                    }else
                        cb.setChecked(false);
                    final int finalMpos = mpos;
                    txtqty.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {

                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                            txtqty.removeTextChangedListener(this);
                            String[] tokens = cb.getTag().toString().split(",", -1);
                            String id = tokens[0];
                            if(vidlist!=null && vidlist.contains(id)){
                                int vpos=vidlist.indexOf(id);
                                VarPojo selv=vlist.get(vpos);
                                selv.setQuantity(s.toString());
                                vlist.set(vpos,selv);
                            }
                            txtqty.addTextChangedListener(this);
                        }
                    });
                    iminc.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int q=Integer.parseInt(txtqty.getText().toString());
                            q++;
                            txtqty.setText(q+"");
                        }
                    });
                    imdec.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int q=Integer.parseInt(txtqty.getText().toString());
                            if(q>1) {
                                q--;
                                txtqty.setText(q+"");
                            }
                        }
                    });
                    cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            String tag = buttonView.getTag().toString();
                            String[] tokens = tag.split(",", -1);
                            String id = tokens[0];
                            Double extraprice = Double.parseDouble(tokens[1]);
                            int sel=0;
                            if(mlinear.get(finalMpos).getTag()!=null)
                                sel=Integer.parseInt(mlinear.get(finalMpos).getTag().toString());
                            if (isChecked) {
                                if(!vidlist.contains(id)) {
                                    Boolean allowadd=true;
                                    int ms=mlnm.get(finalMpos).getModifier_selection();
                                    if(ms==0)
                                        allowadd=true;
                                    else if(sel<ms)
                                        allowadd=true;
                                    else
                                        allowadd=false;
                                    if(allowadd) {
                                        vidlist.add(id);
                                        VarPojo v = new VarPojo();
                                        v.setId(id);
                                        v.setName(Prenm);
                                        v.setQuantity(txtqty.getText().toString());
                                        if (taxtype != null && taxtype.trim().length() > 0) {
                                            if (taxtype.equals("1")) {
                                                v.setAmount(extraprice + "");
                                                double newgst = extraprice - (extraprice * (100 / (100 + tax_per_tot)));
                                                double prewtax = extraprice - newgst;
                                                v.setAmount_wt(prewtax + "");
                                            } else if (taxtype.equals("0")) {
                                                double newgst = (extraprice * tax_per_tot) / 100;
                                                double priceperdish = extraprice + newgst;
                                                v.setAmount(priceperdish + "");
                                                v.setAmount_wt(preprice);
                                            }
                                        } else {
                                            v.setAmount(preprice);
                                            v.setAmount_wt(preprice);
                                        }
                                        vlist.add(v);
                                        sel++;
                                        mlinear.get(finalMpos).setTag(sel+"");
                                    }else{
                                        cb.setChecked(false);
                                        Toast.makeText(mcontext,"Max."+ms+" Allowed",Toast.LENGTH_SHORT).show();
                                    }
                                }
                            } else if (!isChecked) {
                                if(vidlist.contains(id) && vidlist.size()>0 && vlist.size()>0) {
                                    int vpos = vidlist.indexOf(id);
                                    vidlist.remove(vpos);
                                    vlist.remove(vpos);
                                    if(sel>0){
                                        sel--;
                                        mlinear.get(finalMpos).setTag(sel+"");
                                    }
                                }
                            }

                        }
                    });
                }
            }
            llmodifier.addView(tbl);
        }
        ivclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        tb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    txtprice.setText(AppConst.currency + " " + 0);
                } else {
                    if(M.isPriceWT(mcontext))
                        txtprice.setText(AppConst.currency + " " + pf.setFormat(model.getPriceper_without_tax()));
                    else
                        txtprice.setText(AppConst.currency + " " + pf.setFormat(model.getPriceperdish()));
                }

                if(!isChecked && !M.getBrandId(mcontext).equals("schmi194") && M.isItemDiscount(mcontext))
                    lldiscount.setVisibility(View.VISIBLE);
                else
                    lldiscount.setVisibility(View.GONE);
            }
        });
        if(dishprice!=0 && Double.parseDouble(price)==0){
            tb.setChecked(true);
        }else
            tb.setChecked(false);

        if((model.getOffer()!=null && model.getOffer().trim().length()>0) || M.getBrandId(mcontext).equals("schmi194")
                || !M.isItemDiscount(mcontext))
            lldiscount.setVisibility(View.GONE);
        // WEIGHT
        if(model.getSold_by().equals("weight")) {
            llcomp.setVisibility(View.GONE);
            DBUnit dbUnit=new DBUnit(mcontext);
            final UnitPojo unitPojo=dbUnit.getUnitInfo(model.getPurchased_unit_id(),mcontext);
            final Double pamt = Double.parseDouble(model.getPriceperdish());
            final Double ptamt=Double.parseDouble(model.getPriceper_without_tax());
            if(unit_id.equals(model.getPurchased_unit_id()))
                tvpurchase.setTag("1");
            else
                tvpurchase.setTag("0");
            setunitChip(tvpurchase,tvused);
            etweight.addTextChangedListener(new TextWatcher() {
                Boolean ispurchase;
                Double we = 0.0,wet=0.0;

                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (tvpurchase.getTag().toString().equals("1")) {
                        ispurchase = true;
                    } else {
                        ispurchase = false;
                    }
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    etweight.setSelection(etweight.length());
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String w = etweight.getText().toString();
                    if(w.equals("."))
                        w=null;
                    if(w!=null && w.trim().length()>0) {
                        if (ispurchase) {
                            we = pamt * Double.parseDouble(w);
                            wet = ptamt * Double.parseDouble(w);
                            unit_id=model.getPurchased_unit_id();
                            unit_name=model.getPurchased_unit_name();
                        } else if(unitPojo!=null){
                            we=(Double.parseDouble(w)*pamt)/(Double.parseDouble(unitPojo.getUsed_unit()));
                            wet=(Double.parseDouble(w)*ptamt)/(Double.parseDouble(unitPojo.getUsed_unit()));
                            unit_id=model.getUsed_unit_id();
                            unit_name=model.getUsed_unit_name();
                        }
                    }else {
                        we = 0.0;
                        wet = 0.0;
                    }
                    we= AppConst.setdecimalfmt(we,mcontext);
                    wet= AppConst.setdecimalfmt(wet,mcontext);
                    if(M.isPriceWT(mcontext))
                        tvamt.setText(mcontext.getString(R.string.txt_amount)+" : " + AppConst.currency + " " + wet);
                    else
                        tvamt.setText(mcontext.getString(R.string.txt_amount)+" : " + AppConst.currency + " " + we);
                    tvamt.setTag(we+"");
                    llp.setTag(wet+"");
                }
            });

            etamt.addTextChangedListener(new TextWatcher() {
                Double we = 0.0;

                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    unit_name = model.getPurchased_unit_name();
                    unit_id = model.getPurchased_unit_id();
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    etamt.setSelection(etamt.length());
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String w = etamt.getText().toString();
                    if(w.equals("."))
                        w=null;
                    if (w != null && w.trim().length() > 0) {
                        we = Double.parseDouble(w) / pamt;
                        if(unitPojo != null &&  we<1 && !model.getPurchased_unit_id().equals(model.getUsed_unit_id())) {
                            Double weu = we * (Double.parseDouble(unitPojo.getUsed_unit()));
                            we = weu;
                            unit_name = model.getUsed_unit_name();
                            unit_id = model.getUsed_unit_id();
                            tvpurchase.setTag("0");
                        } else
                            tvpurchase.setTag("1");
                    } else {
                        we = 0.0;
                        tvpurchase.setTag("1");
                    }
                    we = AppConst.setdecimalfmt1(we, "#.###");
                    tvcntweight.setText(mcontext.getString(R.string.txt_weight)+" : " + we + " " + unit_name);
                    tvcntweight.setTag(we + "");
                    etweight.setText(we+"");
                    etweight.setSelection(etweight.getText().length());
                }
            });

            tvweight.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    isweight = true;
                    tvweight.setTextColor(mcontext.getResources().getColor(R.color.white));
                    tvweight.setTypeface(AppConst.font_medium(mcontext));
                    tvprice.setTextColor(mcontext.getResources().getColor(R.color.primary_text));
                    tvprice.setTypeface(AppConst.font_regular(mcontext));
                    llw.setVisibility(View.VISIBLE);
                    llp.setVisibility(View.GONE);
                    setunitChip(tvpurchase, tvused);
                    if (tvcntweight.getTag() != null && tvcntweight.getTag().toString().trim().length() > 0) {
                        etweight.setText(tvcntweight.getTag() + "");
                    }
                    etweight.setSelection(etweight.getText().length());
                }
            });

            tvprice.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    isweight = false;
                    tvprice.setTextColor(mcontext.getResources().getColor(R.color.white));
                    tvprice.setTypeface(AppConst.font_medium(mcontext));
                    tvweight.setTextColor(mcontext.getResources().getColor(R.color.primary_text));
                    tvweight.setTypeface(AppConst.font_regular(mcontext));
                    llw.setVisibility(View.GONE);
                    llp.setVisibility(View.VISIBLE);
                    if (tvamt.getTag() != null && tvamt.getTag().toString().trim().length() > 0)
                        etamt.setText(tvamt.getTag() + "");
                    etamt.setSelection(etamt.getText().length());
                }
            });

            tvpurchase.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    tvpurchase.setTag("1");
                    etweight.setText("");
                    setunitChip(tvpurchase, tvused);
                    etweight.setSelection(etweight.getText().length());

                }
            });

            tvused.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    tvpurchase.setTag("0");
                    etweight.setText("");
                    setunitChip(tvpurchase, tvused);
                    etweight.setSelection(etweight.getText().length());
                }
            });
            buttonZero.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (isweight)
                        etweight.setText(etweight.getText() + "0");
                    else
                        etamt.setText(etamt.getText() + "0");
                }
            });

            buttonOne.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (isweight)
                        etweight.setText(etweight.getText() + "1");
                    else
                        etamt.setText(etamt.getText() + "1");
                }
            });

            buttonTwo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (isweight)
                        etweight.setText(etweight.getText() + "2");
                    else
                        etamt.setText(etamt.getText() + "2");
                }
            });

            buttonThree.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (isweight)
                        etweight.setText(etweight.getText() + "3");
                    else
                        etamt.setText(etamt.getText() + "3");
                }
            });

            buttonFour.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (isweight)
                        etweight.setText(etweight.getText() + "4");
                    else
                        etamt.setText(etamt.getText() + "4");
                }
            });

            buttonFive.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (isweight)
                        etweight.setText(etweight.getText() + "5");
                    else
                        etamt.setText(etamt.getText() + "5");
                }
            });

            buttonSix.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (isweight)
                        etweight.setText(etweight.getText() + "6");
                    else
                        etamt.setText(etamt.getText() + "6");
                }
            });

            buttonSeven.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (isweight)
                        etweight.setText(etweight.getText() + "7");
                    else
                        etamt.setText(etamt.getText() + "7");
                }
            });

            buttonEight.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (isweight)
                        etweight.setText(etweight.getText() + "8");
                    else
                        etamt.setText(etamt.getText() + "8");
                }
            });

            buttonNine.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (isweight)
                        etweight.setText(etweight.getText() + "9");
                    else
                        etamt.setText(etamt.getText() + "9");
                }
            });


            buttonEqual.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (isweight) {
                        if (!etweight.getText().toString().contains("."))
                            etweight.setText(etweight.getText() + ".");
                    } else {
                        if (!etamt.getText().toString().contains("."))
                            etamt.setText(etamt.getText() + ".");
                    }
                }
            });

            buttonClear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (isweight) {
                        etweight.setText("");
                    } else {
                        etamt.setText("");
                    }

                }
            });
        }
        dialog.show();
    }

    public String replace(String str) {
        if (str.length() > 0 && str.charAt(str.length()-1)==',') {
            str = str.substring(0, str.length()-1);

        }
        return str;
    }

    private void setunitChip(TextView tvpurchase,TextView tvused){
        int padding=tvpurchase.getPaddingLeft();
        if(tvpurchase.getTag().equals("1")){

            tvpurchase.setTextColor(mcontext.getResources().getColor(R.color.white));
            tvpurchase.setBackground(mcontext.getResources().getDrawable(R.drawable.chip_selected));

            tvused.setTextColor(mcontext.getResources().getColor(R.color.primary_text));
            tvused.setBackground(mcontext.getResources().getDrawable(R.drawable.chip));
        }else{
            tvused.setTextColor(mcontext.getResources().getColor(R.color.white));
            tvused.setBackground(mcontext.getResources().getDrawable(R.drawable.chip_selected));

            tvpurchase.setTextColor(mcontext.getResources().getColor(R.color.primary_text));
            tvpurchase.setBackground(mcontext.getResources().getDrawable(R.drawable.chip));
        }
        tvpurchase.setPadding(padding,padding,padding,padding);
        tvused.setPadding(padding,padding,padding,padding);
    }

    private void sendToServer(String reqObj){
        if(AppConst.isMyServiceRunning(POServerService.class,mcontext)){
            if(localClient==null)
                localClient=new LocalClient(mcontext);
            localClient.sendSessionMessage(new Payload<String>(reqObj+""));
        }
    }

//    public String getLocalIp() {
//        WifiManager wm = (WifiManager) mcontext.getApplicationContext().getSystemService(mcontext.WIFI_SERVICE);
//        return Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
//    }

    public String getLocalIp() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface networkInterface = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = networkInterface.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                        String host = inetAddress.getHostAddress();
                        if (!TextUtils.isEmpty(host)) {

                            return host;
                        }
                    }
                }

            }
        } catch (Exception ex) {
            Log.e("IP Address", "getLocalIpAddress", ex);
        }
        return null;
    }
}