package com.royalpos.waiter.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.thunder413.datetimeutils.DateTimeStyle;
import com.github.thunder413.datetimeutils.DateTimeUtils;
import com.royalpos.waiter.POServerService;
import com.royalpos.waiter.PlaceOrder;
import com.royalpos.waiter.R;
import com.royalpos.waiter.WaiterActivity;
import com.royalpos.waiter.helper.AppConst;
import com.royalpos.waiter.helper.ConnectionDetector;
import com.royalpos.waiter.print.PrintFormat;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import de.greenrobot.event.EventBus;

public class ActiveTableAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_ITEM = 1;
    private JSONArray list;
    Context context;
    String TAG="ActiveTableAdapter";
    ConnectionDetector connectionDetector;
    PrintFormat pf;

    public ActiveTableAdapter(JSONArray list, Context mcontext) {
        context = mcontext;
        this.list = list;
        connectionDetector=new ConnectionDetector(context);
        pf=new PrintFormat(context);
    }

    @Override
    public int getItemViewType(int position) {
        return TYPE_ITEM;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.active_table_row, parent, false);
        return new ViewHolderPosts(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        final JSONObject model;
        final ViewHolderPosts itemview = (ViewHolderPosts) holder;
        try {
            model = list.getJSONObject(position);
            final String tblnm=model.getString("table_name");
            final String tblid=model.getString("table_id");
            final String otime=model.getString("order_time");
            String np=model.getString("np");
            final String oid=model.getString("order_id");
            final String ono=model.getString("order_no");
            final String token=model.getString("token");
            String unm=model.getString("usernm");
            final String uid=model.getString("userid");
            String amount=model.getString("amount");
            String merge_tbl=(model.has("merge_tbl"))?model.getString("merge_tbl"):"";
            String offerid="",disc="",billdisc="",sc="";
            if(model.has("offer_id"))
                offerid=model.getString("offer_id");
            if(model.has("discount"))
                disc=model.getString("discount");
            if(model.has("bill_amt_dis"))
                billdisc=model.getString("bill_amt_dis");
            if(model.has("service_charge"))
                sc=model.getString("service_charge");
            Log.d(TAG,tblnm+" "+sc);
            itemview.tvnm.setText(""+tblnm);
            itemview.tvno.setText(token);
            String amt="0";
            if(amount==null)
                amt="0";
            else
                amt=amount;
            itemview.tvamount.setText(""+AppConst.currency+" "+pf.setFormat(amt));
            itemview.tvuser.setText(""+unm);
            if(otime!=null && otime.trim().length()>0) {
                try {
                    String timeAgo = DateTimeUtils.getTimeAgo(context, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(otime), DateTimeStyle.AGO_SHORT_STRING);
                    itemview.tvtime.setText(timeAgo);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            if(np!=null && np.trim().length()>0 && !np.equals("0")) {
                itemview.tvpeople.setText(np);
                itemview.tvpeople.setVisibility(View.VISIBLE);
            }else
                itemview.tvpeople.setVisibility(View.GONE);

            if(merge_tbl!=null && !merge_tbl.isEmpty()){
                itemview.lloption.setVisibility(View.GONE);
                itemview.llmerge.setVisibility(View.VISIBLE);
                itemview.tvtbls.setText(merge_tbl);
            }else{
                itemview.lloption.setVisibility(View.VISIBLE);
                itemview.btnmerge.setVisibility(View.VISIBLE);
                itemview.btnchange.setVisibility(View.VISIBLE);
                itemview.vmerge.setVisibility(View.VISIBLE);
                itemview.llmerge.setVisibility(View.GONE);
            }
            if(model.has("pending_count")) {
                itemview.llcnt.setVisibility(View.VISIBLE);
                itemview.tvcnt.setText(context.getString(R.string.pending_item_count) + ": " + model.getInt("pending_count"));
            }else
                itemview.llcnt.setVisibility(View.GONE);
            itemview.btnchange.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(AppConst.isMyServiceRunning(POServerService.class,context)) {
                        try {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("order_id", oid);
                            jsonObject.put("table_id", tblid);
                            jsonObject.put("table_name", tblnm);
                            EventBus.getDefault().post("changeTable" + jsonObject);
                        } catch (JSONException e) {
                        }
                    }
                }
            });

            itemview.btnmerge.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    EventBus.getDefault().post("mergeTable"+model);
                }
            });

            final String finalOfferid = offerid;
            final String finalDisc = disc;
            final String finalBilldisc = billdisc;
            final String finalSc = sc;
            itemview.lltable.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(AppConst.isMyServiceRunning(POServerService.class,context)) {
                        if (AppConst.dishorederlist == null)
                            AppConst.dishorederlist = new ArrayList<>();
                        AppConst.dishorederlist.clear();
                        if (AppConst.selidlist == null)
                            AppConst.selidlist = new ArrayList<>();
                        AppConst.selidlist.clear();
                        if (AppConst.placelist == null)
                            AppConst.placelist = new ArrayList<>();
                        AppConst.placelist.clear();

                        Intent it = new Intent(context, PlaceOrder.class);
                        it.putExtra("orderid", oid);
                        it.putExtra("tbl_id", tblid + "");
                        it.putExtra("tbl_nm", tblnm);
                        it.putExtra("token", token);
                        it.putExtra("order_no", ono);
                        it.putExtra("ordertime", otime);
                        it.putExtra("offerid", finalOfferid);
                        it.putExtra("discount", finalDisc);
                        it.putExtra("bill_dis", finalBilldisc);
                        it.putExtra("service_charge", finalSc);
                        ((WaiterActivity) context).finish();
                        ((WaiterActivity) context).startActivity(it);
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return list.length();
    }

    public class ViewHolderPosts extends RecyclerView.ViewHolder {
        TextView tvnm,tvamount,tvuser,tvtime,tvpeople,tvno,tvtbls,tvcnt;
        Button btnchange,btnmerge;
        LinearLayout lltable,llmerge,lloption,llcnt;
        View vmerge;

        public ViewHolderPosts(View itemView) {
            super(itemView);
            tvnm = (TextView) itemView.findViewById(R.id.tvtablenm);
            tvamount = (TextView) itemView.findViewById(R.id.tvamount);
            tvuser = (TextView) itemView.findViewById(R.id.tvuser);
            tvtime=(TextView)itemView.findViewById(R.id.tvtime);
            tvpeople = (TextView) itemView.findViewById(R.id.tvpeople);
            tvno=(TextView)itemView.findViewById(R.id.tvno);
            btnchange=(Button)itemView.findViewById(R.id.btnchange);
            btnchange.setTypeface(AppConst.font_regular(context));
            lltable=(LinearLayout)itemView.findViewById(R.id.lltable);
            llmerge=(LinearLayout)itemView.findViewById(R.id.llmerge);
            lloption=(LinearLayout)itemView.findViewById(R.id.lloptions);
            btnmerge=(Button)itemView.findViewById(R.id.btnmerge);
            btnmerge.setTypeface(AppConst.font_regular(context));
            vmerge=(View)itemView.findViewById(R.id.vmerge);
            tvtbls=(TextView)itemView.findViewById(R.id.tvtbls);
            llcnt=(LinearLayout)itemView.findViewById(R.id.llitemcnt);
            tvcnt=(TextView)itemView.findViewById(R.id.tvcnt);
        }
    }
}
