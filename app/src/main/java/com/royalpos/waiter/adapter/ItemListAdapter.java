package com.royalpos.waiter.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.royalpos.waiter.R;
import com.royalpos.waiter.database.DBPrinter;
import com.royalpos.waiter.helper.AppConst;
import com.royalpos.waiter.model.DishOrderPojo;
import com.royalpos.waiter.model.KitchenPrinterPojo;
import com.royalpos.waiter.model.M;
import com.royalpos.waiter.model.VarPojo;
import com.royalpos.waiter.print.PrintFormat;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Reeva on 9/24/2015.
 */
public class ItemListAdapter extends RecyclerView.Adapter<ItemListAdapter.ContentViewHolder> {

    View view;
    ContentViewHolder contentviewholder;
    private static String TAG = "ItemListAdapter";
    List<DishOrderPojo> dishorederlist = new ArrayList<DishOrderPojo>();
    Context mcontext;
    ArrayList<String> catlist=new ArrayList<>();
    ArrayList<String> pidlist=new ArrayList<>();
    DBPrinter dbp;
    PrintFormat pf;

    public ItemListAdapter(Context mcontext, List<DishOrderPojo> dishorederlist ) {
        this.dishorederlist = dishorederlist;
        this.mcontext = mcontext;
        dbp=new DBPrinter(mcontext);
        if(AppConst.totlist==null)
            AppConst.totlist=new ArrayList<>();
        AppConst.totlist.clear();
        if(AppConst.kplist==null)
            AppConst.kplist=new ArrayList<>();
        AppConst.kplist.clear();
        pf=new PrintFormat(mcontext);
    }

    public static class ContentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txtdishname, txtqty,txtpre,txtprice,txtdisc;
        
        public ContentViewHolder(View itemView) {
            super(itemView);
            txtpre=(TextView)itemView.findViewById(R.id.txtpre);
            txtdishname = (TextView) itemView.findViewById(R.id.txtdishname);
            txtqty = (TextView) itemView.findViewById(R.id.txtqty);
            txtprice=(TextView)itemView.findViewById(R.id.txtprice);
            txtdisc=(TextView)itemView.findViewById(R.id.tvdiscount);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
        }
    }

    @Override
    public ItemListAdapter.ContentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dish_order_item, parent, false);
        contentviewholder = new ContentViewHolder(view);
        return contentviewholder;
    }

    @Override
    public void onBindViewHolder(final ItemListAdapter.ContentViewHolder holder, final int position) {

        final DishOrderPojo model=dishorederlist.get(position);
        String cid=model.getCusineid();
        if(cid!=null && cid.trim().length()>0 && !catlist.contains(cid)) {
            catlist.add(cid);
            KitchenPrinterPojo kprinter=dbp.getCatPrinters(cid);
            if(kprinter!=null) {
                if(!pidlist.contains(kprinter.getId())) {
                    pidlist.add(kprinter.getId());
                    AppConst.kplist.add(kprinter);
                    AppConst.totlist.add(1);
                }else{
                    int p=pidlist.indexOf(kprinter.getId());
                    int val=AppConst.totlist.get(p)+1;
                    AppConst.totlist.set(p,val);
                }
            }
        }

        String pq=model.getQty();
        if(model.getWeight()!=null && model.getUnitname()!=null && model.getWeight().trim().length()>0 && model.getUnitname().trim().length()>0){
            holder.txtdishname.setText(model.getDishname()+"  "+model.getWeight()+" "+model.getSort_nm());
        }else
            holder.txtdishname.setText(model.getDishname());

        if(M.isPriceWT(mcontext) && model.getPriceper_without_tax()!=null && model.getPriceper_without_tax().trim().length()>0)
            pq=pq+" X "+pf.setFormat(model.getPriceper_without_tax());
        else if(model.getPriceperdish()!=null && model.getPriceperdish().trim().length()>0)
            pq=pq+" X "+pf.setFormat(model.getPriceperdish());

        holder.txtqty.setText(pq);
        if(M.isPriceWT(mcontext) && model.getPrice_without_tax()!=null && model.getPrice_without_tax().trim().length()>0)
            holder.txtprice.setText(pf.setFormat(model.getPrice_without_tax()));
        else
            holder.txtprice.setText(pf.setFormat(model.getPrice()));

        if(model.getVarPojoList()!=null && model.getVarPojoList().size()>0){
            String pre="";
            for(VarPojo p:model.getVarPojoList()){
                String pnm=p.getName();
                if(M.isPriceWT(mcontext))
                    pnm=pnm+"-"+pf.setFormat(p.getAmount_wt());
                else
                    pnm=pnm+"-"+pf.setFormat(p.getAmount());
                if(pre.trim().length()==0)
                    pre=pnm;
                else
                    pre=pre+"\n"+pnm;
            }
            holder.txtpre.setText(pre);
        }else {
            String pre = model.getPrenm();
            if (pre != null && pre.trim().length() > 0) {
                holder.txtpre.setText(model.getPrenm());
            } else {
                holder.txtpre.setText("");
            }
        }

        if(model.getTot_disc()!=null && model.getTot_disc().trim().length()>0
                && Double.parseDouble(model.getTot_disc())>0) {
            holder.txtdisc.setVisibility(View.VISIBLE);
            holder.txtdisc.setText(mcontext.getString(R.string.txt_save)+": " +pf.setFormat(model.getTot_disc()));
        }else
            holder.txtdisc.setVisibility(View.GONE);
    }


    @Override
    public int getItemCount() {
        return dishorederlist.size();
    }

}