package com.royalpos.waiter.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import com.royalpos.waiter.model.Waiter;

import java.util.ArrayList;
import java.util.List;

public class DBWaiter extends SQLiteOpenHelper {

    public static final String TBL_WAITER = "tblwaiter";

    public static final String KEY_ID = "id";
    public static final String KEY_WID= "wid";
    public static final String KEY_WNAME = "wnm";
    public static final String KEY_KDK = "kdk_show";
    String TAG="DBWaiter";
    public static final String CREATE_TBL = "CREATE TABLE IF NOT EXISTS " + TBL_WAITER + "("
            + KEY_ID + " INTEGER PRIMARY KEY, " +KEY_WID + " TEXT, " + KEY_WNAME +" TEXT, "+ KEY_KDK+" TEXT )";

    public DBWaiter(Context c) {
        super(c, DBCusines.DATABASE_NAME, null, DBCusines.DATABASE_VERSION);
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(CREATE_TBL);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
      
    }

    public void addwaiting(Waiter data) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            db.execSQL(CREATE_TBL);
            ContentValues values = new ContentValues();
            values.put(KEY_WID, data.getUser_id());
            values.put(KEY_WNAME,data.getUser_name());
            values.put(KEY_KDK,"no");
            long i=db.insert(TBL_WAITER, null, values);
            db.close();
        }catch (SQLiteException e){

        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public int getCounts(){
        String selectQuery = "SELECT  * FROM " + TBL_WAITER ;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            if (db != null && db.isOpen()) {
                Cursor cursor = db.rawQuery(selectQuery, null);
                int cnt= cursor.getCount();
                cursor.close();
                return cnt;
            }
        }catch (SQLiteException e){

        }
        return 0;
    }

    public List<Waiter> getWaiterList(String isKitchen) {
        List<Waiter> list = new ArrayList<Waiter>();
        String selectQuery = "SELECT  * FROM " + TBL_WAITER +" where "+KEY_KDK+"='"+isKitchen+"'";
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    Waiter data = new Waiter();
                    data.setUser_id(Integer.parseInt(cursor.getString(cursor.getColumnIndexOrThrow(KEY_WID))));
                    data.setUser_name((cursor.getString(cursor.getColumnIndexOrThrow(KEY_WNAME))));
                    list.add(data);
                } while (cursor.moveToNext());
            }

            cursor.close();
            db.close();
        }catch (SQLiteException e){

        }
        return list;
    }
    
    public void deleteall() {
        // Select All Query
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete(TBL_WAITER,null,null);
            db.close();
        }catch (SQLiteException e){

        }
    }

}
