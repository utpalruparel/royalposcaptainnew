package com.royalpos.waiter.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.royalpos.waiter.model.CuisineListPojo;
import com.royalpos.waiter.model.KitchenPrinterPojo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DBPrinter extends SQLiteOpenHelper {

    public static final String TBL_KP = "tblprinter";

    public static final String KEY_ID = "id";
    public static final String KEY_CAT_ID = "category_id";
    public static final String KEY_PR_Name = "printer_name";//e.g., WIFI,USB
    public static final String KEY_PR_Title = "printer_title";//e.g., Kitchen Printer 1,Printer 2
    public static final String KEY_usbDeviceID = "usbDeviceID";
    public static final String KEY_usbVendorID = "usbVendorID";
    public static final String KEY_usbProductID = "usbProductID";
    public static final String KEY_blueToothDeviceAdress = "blueToothDeviceAdress";
    public static final String KEY_deviceType = "deviceType";
    public static final String KEY_deviceIP = "deviceIP";
    public static final String KEY_devicePort = "devicePort";// EPSON(devicetype-7):printerSeries
    public static final String KEY_link_code = "link_code";
    public static final String KEY_picturePath = "picturePath";
    public static final String KEY_logoProcesed = "logoProcesed";
    public static final String KEY_ADV = "advance_printer";//yes/no
    public static final String KEY_paper_width = "paper_width";//PrintFormat- normalsize/smallsize
    public static final String KEY_star_setting = "star_settings";//JSONObject- model,modelIndex,settings
    String TAG="DBPrinter";
    Context c;
    public static final String CREATE_KP = "CREATE TABLE IF NOT EXISTS " + TBL_KP + "("
            + KEY_ID + " INTEGER PRIMARY KEY, " +KEY_CAT_ID + " TEXT, "+KEY_PR_Name + " TEXT, "+KEY_PR_Title + " TEXT, "+ KEY_usbProductID + " TEXT, "+ KEY_blueToothDeviceAdress + " TEXT, "
            + KEY_deviceIP + " TEXT, "+ KEY_devicePort + " TEXT, "+ KEY_link_code + " TEXT, "+KEY_picturePath + " TEXT, "+KEY_logoProcesed + " TEXT, "+KEY_ADV+" TEXT,"
            + KEY_paper_width+" TEXT,"+KEY_star_setting+" TEXT,"
            + KEY_deviceType + " TEXT, "+ KEY_usbDeviceID + " TEXT, "+ KEY_usbVendorID+ " TEXT )";

    public DBPrinter(Context c) {
        super(c, DBCusines.DATABASE_NAME, null, DBCusines.DATABASE_VERSION);
        this.c=c;
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(CREATE_KP);
        db.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    public void addPrinter(KitchenPrinterPojo data) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_CAT_ID, data.getCategoryid());
            values.put(KEY_PR_Name,data.getPrinter_name());
            values.put(KEY_PR_Title,data.getPrinter_title());
            values.put(KEY_usbProductID,data.getUsbProductID());
            values.put(KEY_blueToothDeviceAdress,data.getBlueToothDeviceAdress());
            values.put(KEY_deviceIP,data.getDeviceIP());
            values.put(KEY_devicePort,data.getDevicePort());
            values.put(KEY_link_code,data.getLink_code());
            values.put(KEY_picturePath,data.getPicturePath());
            values.put(KEY_logoProcesed,data.getLogoProcesed());
            values.put(KEY_deviceType,data.getDeviceType());
            values.put(KEY_usbDeviceID,data.getUsbDeviceID());
            values.put(KEY_usbVendorID,data.getUsbVendorID());
            if(data.isAdv())
                values.put(KEY_ADV,"yes");
            else
                values.put(KEY_ADV,"no");
            if(data.getPaper_width()!=null)
                values.put(KEY_paper_width,data.getPaper_width());
            if(data.getStar_settings()!=null)
                values.put(KEY_star_setting,data.getStar_settings());
            long i=db.insert(TBL_KP, null, values);
        }catch (SQLiteException e){

        }
        db.close();
    }

    public void updatePrinterCategory(KitchenPrinterPojo data) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_CAT_ID, data.getCategoryid());
            values.put(KEY_PR_Name,data.getPrinter_name());
            values.put(KEY_PR_Title,data.getPrinter_title());
            values.put(KEY_usbProductID,data.getUsbProductID());
            values.put(KEY_blueToothDeviceAdress,data.getBlueToothDeviceAdress());
            values.put(KEY_deviceIP,data.getDeviceIP());
            values.put(KEY_devicePort,data.getDevicePort());
            values.put(KEY_link_code,data.getLink_code());
            values.put(KEY_picturePath,data.getPicturePath());
            values.put(KEY_logoProcesed,data.getLogoProcesed());
            values.put(KEY_deviceType,data.getDeviceType());
            values.put(KEY_usbDeviceID,data.getUsbDeviceID());
            values.put(KEY_usbVendorID,data.getUsbVendorID());
            if(data.isAdv())
                values.put(KEY_ADV,"yes");
            else
                values.put(KEY_ADV,"no");
            if(data.getPaper_width()!=null)
                values.put(KEY_paper_width,data.getPaper_width());
            if(data.getStar_settings()!=null)
                values.put(KEY_star_setting,data.getStar_settings());
            db.update(TBL_KP,values,KEY_ID+"='"+data.getId()+"'",null);
        }catch (SQLiteException e){

        }
        db.close();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public List<KitchenPrinterPojo> getPrinters() {
        SQLiteDatabase db = this.getWritableDatabase();
        List<KitchenPrinterPojo> list = new ArrayList<KitchenPrinterPojo>();
        String selectQuery = "SELECT  * FROM " + TBL_KP ;
        try {
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    KitchenPrinterPojo data = new KitchenPrinterPojo();

                    data.setBlueToothDeviceAdress(cursor.getString(cursor.getColumnIndexOrThrow(KEY_blueToothDeviceAdress)));
                    data.setCategoryid((cursor.getString(cursor.getColumnIndexOrThrow(KEY_CAT_ID))));
                    data.setPrinter_name((cursor.getString(cursor.getColumnIndexOrThrow(KEY_PR_Name))));
                    data.setPrinter_title((cursor.getString(cursor.getColumnIndexOrThrow(KEY_PR_Title))));
                    data.setDeviceIP((cursor.getString(cursor.getColumnIndexOrThrow(KEY_deviceIP))));
                    data.setDevicePort((cursor.getString(cursor.getColumnIndexOrThrow(KEY_devicePort))));
                    data.setDeviceType((cursor.getString(cursor.getColumnIndexOrThrow(KEY_deviceType))));
                    data.setId((cursor.getInt(cursor.getColumnIndexOrThrow(KEY_ID)))+"");
                    data.setLink_code((cursor.getString(cursor.getColumnIndexOrThrow(KEY_link_code))));
                    data.setLogoProcesed((cursor.getString(cursor.getColumnIndexOrThrow(KEY_logoProcesed))));
                    data.setPicturePath((cursor.getString(cursor.getColumnIndexOrThrow(KEY_picturePath))));
                    data.setUsbDeviceID((cursor.getString(cursor.getColumnIndexOrThrow(KEY_usbDeviceID))));
                    data.setUsbProductID((cursor.getString(cursor.getColumnIndexOrThrow(KEY_usbProductID))));
                    data.setUsbVendorID((cursor.getString(cursor.getColumnIndexOrThrow(KEY_usbVendorID))));

                    if(data.getCategoryid()!=null && data.getCategoryid().trim().length()>0){
                        String cid=data.getCategoryid();
                        String cnm="";
                        if(cid.contains(",")){
                            List<String> items = Arrays.asList(cid.split("\\s*,\\s*"));
                            DBCusines dbc=new DBCusines(c);
                            for (String s:items){
                                String nm=dbc.getCategorynm(s);
                                if(cnm.trim().length()>0)
                                    cnm=cnm+","+nm;
                                else
                                    cnm=nm;
                            }
                            data.setCategorynm(cnm);
                        }else{
                            DBCusines dbc=new DBCusines(c);
                            data.setCategorynm(dbc.getCategorynm(cid));
                        }
                    }else
                        data.setCategorynm("");
                    String adv=cursor.getString(cursor.getColumnIndexOrThrow(KEY_ADV));
                    String wdt=cursor.getString(cursor.getColumnIndexOrThrow(KEY_paper_width));
                    String star=cursor.getString(cursor.getColumnIndexOrThrow(KEY_star_setting));
                    if(star!=null && !star.isEmpty())
                        data.setStar_settings(star);
                    if(adv!=null && !adv.isEmpty() && adv.equals("yes"))
                        data.setAdv(true);
                    else
                        data.setAdv(false);
                    if(wdt!=null)
                        data.setPaper_width(wdt);
                    list.add(data);
                } while (cursor.moveToNext());
            }

            cursor.close();
        }catch (SQLiteException e){

        }
        db.close();
        return list;
    }

    public KitchenPrinterPojo getPrintersData(String pid) {
        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "SELECT  * FROM " + TBL_KP+" where "+KEY_ID+"='"+pid+"'" ;
        KitchenPrinterPojo data=null;
        try {
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    data = new KitchenPrinterPojo();

                    data.setBlueToothDeviceAdress(cursor.getString(cursor.getColumnIndexOrThrow(KEY_blueToothDeviceAdress)));
                    data.setCategoryid((cursor.getString(cursor.getColumnIndexOrThrow(KEY_CAT_ID))));
                    data.setPrinter_name((cursor.getString(cursor.getColumnIndexOrThrow(KEY_PR_Name))));
                    data.setPrinter_title((cursor.getString(cursor.getColumnIndexOrThrow(KEY_PR_Title))));
                    data.setDeviceIP((cursor.getString(cursor.getColumnIndexOrThrow(KEY_deviceIP))));
                    data.setDevicePort((cursor.getString(cursor.getColumnIndexOrThrow(KEY_devicePort))));
                    data.setDeviceType((cursor.getString(cursor.getColumnIndexOrThrow(KEY_deviceType))));
                    data.setId((cursor.getInt(cursor.getColumnIndexOrThrow(KEY_ID)))+"");
                    data.setLink_code((cursor.getString(cursor.getColumnIndexOrThrow(KEY_link_code))));
                    data.setLogoProcesed((cursor.getString(cursor.getColumnIndexOrThrow(KEY_logoProcesed))));
                    data.setPicturePath((cursor.getString(cursor.getColumnIndexOrThrow(KEY_picturePath))));
                    data.setUsbDeviceID((cursor.getString(cursor.getColumnIndexOrThrow(KEY_usbDeviceID))));
                    data.setUsbProductID((cursor.getString(cursor.getColumnIndexOrThrow(KEY_usbProductID))));
                    data.setUsbVendorID((cursor.getString(cursor.getColumnIndexOrThrow(KEY_usbVendorID))));
                    String adv=cursor.getString(cursor.getColumnIndexOrThrow(KEY_ADV));
                    String wdt=cursor.getString(cursor.getColumnIndexOrThrow(KEY_paper_width));
                    String star=cursor.getString(cursor.getColumnIndexOrThrow(KEY_star_setting));
                    if(star!=null && !star.isEmpty())
                        data.setStar_settings(star);
                    if(adv!=null && !adv.isEmpty() && adv.equals("yes"))
                        data.setAdv(true);
                    else
                        data.setAdv(false);
                    if(wdt!=null)
                        data.setPaper_width(wdt);
                } while (cursor.moveToNext());
            }
            cursor.close();
        }catch (SQLiteException e){

        }
        db.close();
        return data;
    }

    public KitchenPrinterPojo getCatPrinters(String catid) {
        String selectQuery="SELECT * FROM "+TBL_KP+" WHERE (',' || "+KEY_CAT_ID+" || ',') LIKE '%,"+catid+",%'";
        SQLiteDatabase db = this.getWritableDatabase();
        KitchenPrinterPojo data=null;
        try {
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    data = new KitchenPrinterPojo();

                    data.setBlueToothDeviceAdress(cursor.getString(cursor.getColumnIndexOrThrow(KEY_blueToothDeviceAdress)));
                    data.setCategoryid((cursor.getString(cursor.getColumnIndexOrThrow(KEY_CAT_ID))));
                    data.setPrinter_name((cursor.getString(cursor.getColumnIndexOrThrow(KEY_PR_Name))));
                    data.setPrinter_title((cursor.getString(cursor.getColumnIndexOrThrow(KEY_PR_Title))));
                    data.setDeviceIP((cursor.getString(cursor.getColumnIndexOrThrow(KEY_deviceIP))));
                    data.setDevicePort((cursor.getString(cursor.getColumnIndexOrThrow(KEY_devicePort))));
                    data.setDeviceType((cursor.getString(cursor.getColumnIndexOrThrow(KEY_deviceType))));
                    data.setId((cursor.getInt(cursor.getColumnIndexOrThrow(KEY_ID)))+"");
                    data.setLink_code((cursor.getString(cursor.getColumnIndexOrThrow(KEY_link_code))));
                    data.setLogoProcesed((cursor.getString(cursor.getColumnIndexOrThrow(KEY_logoProcesed))));
                    data.setPicturePath((cursor.getString(cursor.getColumnIndexOrThrow(KEY_picturePath))));
                    data.setUsbDeviceID((cursor.getString(cursor.getColumnIndexOrThrow(KEY_usbDeviceID))));
                    data.setUsbProductID((cursor.getString(cursor.getColumnIndexOrThrow(KEY_usbProductID))));
                    data.setUsbVendorID((cursor.getString(cursor.getColumnIndexOrThrow(KEY_usbVendorID))));
                    if(data.getPicturePath()==null)
                        data.setPicturePath("");
                    if(data.getCategoryid()!=null && data.getCategoryid().trim().length()>0){
                        String cid=data.getCategoryid();
                        String cnm="";
                        if(cid.contains(",")){
                            List<String> items = Arrays.asList(cid.split("\\s*,\\s*"));
                            DBCusines dbc=new DBCusines(c);
                            for (String s:items){
                                String nm=dbc.getCategorynm(s);
                                if(cnm.trim().length()>0)
                                    cnm=cnm+","+nm;
                                else
                                    cnm=nm;
                            }
                            data.setCategorynm(cnm);
                        }else{
                            DBCusines dbc=new DBCusines(c);
                            data.setCategorynm(dbc.getCategorynm(cid));
                        }
                    }else
                        data.setCategorynm("");
                    String adv=cursor.getString(cursor.getColumnIndexOrThrow(KEY_ADV));
                    String wdt=cursor.getString(cursor.getColumnIndexOrThrow(KEY_paper_width));
                    String star=cursor.getString(cursor.getColumnIndexOrThrow(KEY_star_setting));
                    if(star!=null && !star.isEmpty())
                        data.setStar_settings(star);
                    if(adv!=null && !adv.isEmpty() && adv.equals("yes"))
                        data.setAdv(true);
                    else
                        data.setAdv(false);
                    if(wdt!=null)
                        data.setPaper_width(wdt);
                } while (cursor.moveToNext());
            }

            cursor.close();
        }catch (SQLiteException e){

        }
        db.close();
        return data;
    }

    public List<CuisineListPojo> getcusines() {
        SQLiteDatabase db = this.getWritableDatabase();
        List<CuisineListPojo> dishdetaillist = new ArrayList<CuisineListPojo>();
        String selectQuery = "SELECT  * FROM " + DBCusines.TBL_CUISNE;
        try {
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    String cid=cursor.getString(cursor.getColumnIndexOrThrow(DBCusines.KEY_CUSINEID));
                    Cursor cursor1 = db.rawQuery("SELECT * FROM "+TBL_KP+" WHERE (',' || "+KEY_CAT_ID+" || ',') LIKE '%,"+cid+",%'", null);
                    if(cursor1.getCount()==0) {
                        CuisineListPojo data = new CuisineListPojo();
                        data.setCuisine_id(cursor.getString(cursor.getColumnIndexOrThrow(DBCusines.KEY_CUSINEID)));
                        data.setCuisine_name((cursor.getString(cursor.getColumnIndexOrThrow(DBCusines.KEY_CUSINENAME))));
                        data.setCuisine_image((cursor.getString(cursor.getColumnIndexOrThrow(DBCusines.KEY_IMAGE))));
                        dishdetaillist.add(data);
                    }
                } while (cursor.moveToNext());
            }

            cursor.close();
        }catch (SQLiteException e){
            Log.d(TAG,"error:"+e.getMessage());
        }
        db.close();
        return dishdetaillist;
    }

    public List<CuisineListPojo> updateCategory(String pid) {
        SQLiteDatabase db = this.getWritableDatabase();
        List<CuisineListPojo> dishdetaillist = new ArrayList<CuisineListPojo>();
        String selectQuery = "SELECT  * FROM " + DBCusines.TBL_CUISNE;
        try {
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    String cid=cursor.getString(cursor.getColumnIndexOrThrow(DBCusines.KEY_CUSINEID));
                    Cursor cursor1 = db.rawQuery("SELECT * FROM "+TBL_KP+" WHERE "+KEY_ID+"!='"+pid+"' and (',' || "+KEY_CAT_ID+" || ',') LIKE '%,"+cid+",%'", null);
                    if(cursor1.getCount()==0) {
                        CuisineListPojo data = new CuisineListPojo();
                        data.setCuisine_id(cursor.getString(cursor.getColumnIndexOrThrow(DBCusines.KEY_CUSINEID)));
                        data.setCuisine_name((cursor.getString(cursor.getColumnIndexOrThrow(DBCusines.KEY_CUSINENAME))));
                        data.setCuisine_image((cursor.getString(cursor.getColumnIndexOrThrow(DBCusines.KEY_IMAGE))));
                        dishdetaillist.add(data);
                    }
                    cursor1.close();
                } while (cursor.moveToNext());
            }

            cursor.close();
        }catch (SQLiteException e){
            Log.d(TAG,"error:"+e.getMessage());
        }
        db.close();
        return dishdetaillist;
    }

    public void deletePrinter(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.delete(TBL_KP,KEY_ID+"='"+id+"'",null);
        }catch (SQLiteException e){

        }
        db.close();
    }


    public void deleteall() {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.delete(TBL_KP,null,null);
        }catch (SQLiteException e){

        }
        db.close();
    }

    public int getPrinterCount(){
        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "SELECT  * FROM " + TBL_KP ;
        int cnt=0;
        try {
            if (db != null && db.isOpen()) {
                Cursor cursor = db.rawQuery(selectQuery, null);
                cnt= cursor.getCount();
                cursor.close();
            }
        }catch (SQLiteException e){

        }
        db.close();
        return cnt;
    }

}
