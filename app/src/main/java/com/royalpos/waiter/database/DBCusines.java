package com.royalpos.waiter.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import android.util.Log;

import com.royalpos.waiter.helper.AppConst;
import com.royalpos.waiter.model.CuisineListPojo;
import com.royalpos.waiter.model.M;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DBCusines extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "Dbrestaurant";
    public static final String TBL_CUISNE = "tblcusines";
    public static final int DATABASE_VERSION = 4;
    public static final String KEY_ID = "id";
    public static final String KEY_CUSINENAME = "cuisine_name";
    public static final String KEY_CUSINEID = "cuisine_id";
    public static final String KEY_IMAGE = "cuisine_image";
    String TAG="DBCusines";
    String CREATE_CUISINE = "CREATE TABLE IF NOT EXISTS " + TBL_CUISNE + "("
            + KEY_ID + " INTEGER PRIMARY KEY, " +KEY_CUSINENAME + " TEXT, " + KEY_CUSINEID + " TEXT, " + KEY_IMAGE + " TEXT" + ")";

    public DBCusines(Context c) {
        super(c, DATABASE_NAME, null, DATABASE_VERSION);
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL(CREATE_CUISINE);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_CUISINE);

        db.execSQL(DBDishes.CREATE_DISHES);

        db.execSQL(DBPreferences.CREATE_PRE);

        db.execSQL(DBCombo.CREATE_PRE);

        db.execSQL(DBPaymentType.CREATE_TYPE);

        db.execSQL(DBModifier.CREATE_MODI);

        db.execSQL(DBPrinter.CREATE_KP);

        db.execSQL(DBKP.CREATE_KPD);

        db.execSQL(DBUnit.CREATE_UNIT);
    }

    public void addCusines(CuisineListPojo data) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();

            db.execSQL(CREATE_CUISINE);
            ContentValues values = new ContentValues();
            values.put(KEY_CUSINENAME, data.getCuisine_name());
            values.put(KEY_CUSINEID,data.getCuisine_id());
            values.put(KEY_IMAGE,data.getCuisine_image());


            long i=db.insert(TBL_CUISNE, null, values);
            db.close(); // Closing database connection
        }catch (SQLiteException e){

        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(newVersion>1) {
            db.execSQL(DBDishes.CREATE_DISHES);
            Cursor dbCursord = db.query(DBDishes.TBL_DISH, null, null, null, null, null, null);
            List<String> slistd = Arrays.asList(dbCursord.getColumnNames());
            if (!slistd.contains(DBDishes.KEY_hsn_no))
                db.execSQL("ALTER TABLE " + DBDishes.TBL_DISH + " ADD COLUMN " + DBDishes.KEY_hsn_no + " TEXT");
            if (!slistd.contains(DBDishes.KEY_change_price))
                db.execSQL("ALTER TABLE " + DBDishes.TBL_DISH + " ADD COLUMN " + DBDishes.KEY_change_price + " TEXT");
            if (!slistd.contains(DBDishes.KEY_extra2))
                db.execSQL("ALTER TABLE " + DBDishes.TBL_DISH + " ADD COLUMN " + DBDishes.KEY_extra2 + " TEXT");
            if (!slistd.contains(DBDishes.KEY_price_discount))
                db.execSQL("ALTER TABLE " + DBDishes.TBL_DISH + " ADD COLUMN " + DBDishes.KEY_price_discount + " TEXT");
            if (!slistd.contains(DBDishes.KEY_pricewt_discount))
                db.execSQL("ALTER TABLE " + DBDishes.TBL_DISH + " ADD COLUMN " + DBDishes.KEY_pricewt_discount + " TEXT");
            if (!slistd.contains(DBDishes.KEY_taxamt_discount))
                db.execSQL("ALTER TABLE " + DBDishes.TBL_DISH + " ADD COLUMN " + DBDishes.KEY_taxamt_discount + " TEXT");

            Cursor dbCursor3 = db.query( DBPrinter.TBL_KP , null, null, null, null, null, null);
            List<String> slist3 = Arrays.asList(dbCursor3.getColumnNames());
            if (!slist3.contains(DBPrinter.KEY_ADV))
                db.execSQL("ALTER TABLE " + DBPrinter.TBL_KP + " ADD COLUMN " + DBPrinter.KEY_ADV+ " TEXT");
            if (!slist3.contains(DBPrinter.KEY_paper_width))
                db.execSQL("ALTER TABLE " + DBPrinter.TBL_KP + " ADD COLUMN " + DBPrinter.KEY_paper_width+ " TEXT");
            if (!slist3.contains(DBPrinter.KEY_star_setting))
                db.execSQL("ALTER TABLE " + DBPrinter.TBL_KP + " ADD COLUMN " + DBPrinter.KEY_star_setting+ " TEXT");
        }
    }

    public int getCuisineCount(){
        String selectQuery = "SELECT  * FROM " + TBL_CUISNE ;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            if (db != null && db.isOpen()) {
                Cursor cursor = db.rawQuery(selectQuery, null);
                int cnt= cursor.getCount();
                cursor.close();
                return cnt;
            }
        }catch (SQLiteException e){

        }
        return 0;
    }

    public List<CuisineListPojo> getcusines(Context context) {
        List<CuisineListPojo> dishdetaillist = new ArrayList<CuisineListPojo>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TBL_CUISNE ;
        if(M.isCatAlpha(context))
            selectQuery = "SELECT  * FROM " + TBL_CUISNE +" order by "+KEY_CUSINENAME ;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                AppConst.catnmlist=new String[cursor.getCount()];
                int i=0;
                do {
                    CuisineListPojo data = new CuisineListPojo();
                    data.setCuisine_id(cursor.getString(cursor.getColumnIndexOrThrow(KEY_CUSINEID)));
                    data.setCuisine_name((cursor.getString(cursor.getColumnIndexOrThrow(KEY_CUSINENAME))));
                    data.setCuisine_image((cursor.getString(cursor.getColumnIndexOrThrow(KEY_IMAGE))));
                    AppConst.catnmlist[i]=data.getCuisine_name();
                    dishdetaillist.add(data);
                    i++;
                } while (cursor.moveToNext());
            }

            cursor.close();
            db.close();
        }catch (SQLiteException e){

        }
        return dishdetaillist;
    }

    public String getCategorynm(String cid) {

        String selectQuery = "SELECT  "+KEY_CUSINENAME+" FROM " + TBL_CUISNE +" where "+KEY_CUSINEID+"='"+cid+"'";
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                return cursor.getString(0);
            }

            cursor.close();
            db.close();
        }catch (SQLiteException e){

        }
        return "";
    }

    public void deleteallcuisine() {
        try {
            // Select All Query
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete(TBL_CUISNE,null,null);
            db.close();
        }catch (SQLiteException e){

        }
    }

    public long getDataCounts() {
        long count=0;
        try {

            SQLiteDatabase db = this.getReadableDatabase();
            count = DatabaseUtils.queryNumEntries(db, TBL_CUISNE);
            count=count+DatabaseUtils.queryNumEntries(db, DBDishes.TBL_DISH);
            count=count+DatabaseUtils.queryNumEntries(db, DBPaymentType.TBL_TYPE);
            db.close();
        }catch (SQLiteException e){

        }
        return count;
    }

    public void checkField() {
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            db.execSQL(DBDishes.CREATE_DISHES);
            Cursor dbCursord = db.query(DBDishes.TBL_DISH, null, null, null, null, null, null);
            List<String> slistd = Arrays.asList(dbCursord.getColumnNames());
            if (!slistd.contains(DBDishes.KEY_hsn_no))
                db.execSQL("ALTER TABLE " + DBDishes.TBL_DISH + " ADD COLUMN " + DBDishes.KEY_hsn_no + " TEXT");
            if (!slistd.contains(DBDishes.KEY_change_price))
                db.execSQL("ALTER TABLE " + DBDishes.TBL_DISH + " ADD COLUMN " + DBDishes.KEY_change_price + " TEXT");
            if (!slistd.contains(DBDishes.KEY_extra2))
                db.execSQL("ALTER TABLE " + DBDishes.TBL_DISH + " ADD COLUMN " + DBDishes.KEY_extra2 + " TEXT");
            if (!slistd.contains(DBDishes.KEY_price_discount))
                db.execSQL("ALTER TABLE " + DBDishes.TBL_DISH + " ADD COLUMN " + DBDishes.KEY_price_discount + " TEXT");
            if (!slistd.contains(DBDishes.KEY_pricewt_discount))
                db.execSQL("ALTER TABLE " + DBDishes.TBL_DISH + " ADD COLUMN " + DBDishes.KEY_pricewt_discount + " TEXT");
            if (!slistd.contains(DBDishes.KEY_taxamt_discount))
                db.execSQL("ALTER TABLE " + DBDishes.TBL_DISH + " ADD COLUMN " + DBDishes.KEY_taxamt_discount + " TEXT");

            Cursor dbCursor3 = db.query( DBPrinter.TBL_KP , null, null, null, null, null, null);
            List<String> slist3 = Arrays.asList(dbCursor3.getColumnNames());
            if (!slist3.contains(DBPrinter.KEY_ADV))
                db.execSQL("ALTER TABLE " + DBPrinter.TBL_KP + " ADD COLUMN " + DBPrinter.KEY_ADV+ " TEXT");
            if (!slist3.contains(DBPrinter.KEY_paper_width))
                db.execSQL("ALTER TABLE " + DBPrinter.TBL_KP + " ADD COLUMN " + DBPrinter.KEY_paper_width+ " TEXT");
            if (!slist3.contains(DBPrinter.KEY_star_setting))
                db.execSQL("ALTER TABLE " + DBPrinter.TBL_KP + " ADD COLUMN " + DBPrinter.KEY_star_setting+ " TEXT");
            db.close();
        }catch (SQLiteException e){}
    }

}
