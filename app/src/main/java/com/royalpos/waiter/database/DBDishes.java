package com.royalpos.waiter.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import com.royalpos.waiter.model.ComboModel;
import com.royalpos.waiter.model.DishPojo;
import com.royalpos.waiter.model.Inventory_item;
import com.royalpos.waiter.model.M;
import com.royalpos.waiter.model.PreModel;
import com.royalpos.waiter.model.TaxData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DBDishes extends SQLiteOpenHelper {

    public static final String TBL_DISH = "tblDishItemNew";
    public static final String KEY_ID = "id";
    public static final String KEY_DISHID = "dishid";
    public static final String KEY_CUSINEID = "cusineid";
    public static final String KEY_DNAME = "dishnmae";
    public static final String KEY_DIS = "discription";
    public static final String KEY_PRICE = "price";
    public static final String KEY_PRICE_WITHOUT_TAX = "pricewithouttax";
    public static final String KEY_DIMG = "dishimage";
    public static final String KEY_TRACK_STOCK = "trackstock";
    public static final String KEY_CURRENT_STOCK = "currrentstock";
    public static final String KEY_BAR_CODE="barcode_no";
    public static final String KEY_PRODUCT="prodct_no";
    public static final String KEY_BATCH="batch_no";
    public static final String KEY_SOLDBY="sold_by";
    public static final String KEY_PURCHASE_UNIT="purchase_unit";
    public static final String KEY_USED_UNIT="used_unit";
    public static final String KEY_PURCHASE_UNITID="purchase_unit_id";
    public static final String KEY_USED_UNITID="used_unit_id";
    public static final String KEY_SALE_WEIGHT="default_sale_WEIGHT";
    public static final String KEY_TAX = "tax_data";
    public static final String KEY_TAX_AMT = "tax_amt";
    public static final String KEY_OFFER = "offer";
    public static final String KEY_DISCOUNT="discount";
    public static final String KEY_hsn_no="hsnno";
    public static final String KEY_change_price="extra1";//change price/Open price
    public static final String KEY_price_discount="price_dis";//not used
    public static final String KEY_pricewt_discount="price_wt_dis";//not used
    public static final String KEY_taxamt_discount="tax_amt_dis";//not used
    public static final String KEY_extra2="extra2";//not used
    String TAG="DBDishes";
    public static final String CREATE_DISHES = "CREATE TABLE IF NOT EXISTS " + TBL_DISH + "("
            + KEY_ID + " INTEGER PRIMARY KEY, " +KEY_DISHID + " TEXT, " + KEY_CUSINEID + " TEXT, " + KEY_DNAME + " TEXT, " +
            KEY_DIS + " TEXT, " + KEY_PRICE + " TEXT," + KEY_DIMG + " TEXT," +KEY_SALE_WEIGHT + " TEXT,"+
            KEY_PRICE_WITHOUT_TAX + " TEXT, " + KEY_TAX+ " TEXT," +KEY_TAX_AMT+ " TEXT," +
            KEY_TRACK_STOCK + " TEXT, " + KEY_CURRENT_STOCK + " TEXT,"+KEY_SOLDBY + " TEXT,"+KEY_PURCHASE_UNIT + " TEXT,"+KEY_USED_UNIT + " TEXT,"+
            KEY_PURCHASE_UNITID + " TEXT,"+KEY_USED_UNITID + " TEXT,"+KEY_BATCH + " TEXT,"+KEY_BAR_CODE + " TEXT, " + KEY_PRODUCT + " TEXT,"+
            KEY_hsn_no+" TEXT,"+KEY_change_price+" TEXT,"+KEY_extra2+" TEXT,"+KEY_price_discount+" TEXT,"+KEY_pricewt_discount+" TEXT,"+KEY_taxamt_discount+" TEXT,"+
            KEY_OFFER+" TEXT,"+KEY_DISCOUNT+" TEXT)";

    public DBDishes(Context c) {
        super(c, DBCusines.DATABASE_NAME, null, DBCusines.DATABASE_VERSION);
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL(CREATE_DISHES);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    public void addDishes(DishPojo data) {

        try {
            SQLiteDatabase db = this.getWritableDatabase();

            JSONArray jsonArray=new JSONArray();
            if(data.getTax_data()!=null && data.getTax_data().size()>0){
                for(TaxData t:data.getTax_data()){
                    JSONObject j=new JSONObject();
                    j.put("id",t.getId());
                    j.put("text",t.getText());
                    j.put("value",t.getValue());
                    j.put("tax_amount",t.getTax_amount());//1-include,0-exclude
                    jsonArray.put(j);
                }
            }

            ContentValues values = new ContentValues();
            values.put(KEY_DISHID, data.getDishid());
            values.put(KEY_CUSINEID, data.getCusineid());
            values.put(KEY_DNAME,data.getDishname());
            values.put(KEY_DIS,data.getDescription());
            values.put(KEY_PRICE,data.getPrice());
            values.put(KEY_PRICE_WITHOUT_TAX,data.getPrice_without_tax());
            if(data.getDishimage_url()!=null)
                values.put(KEY_DIMG,data.getDishimage_url());
            values.put(KEY_CURRENT_STOCK,data.getIn_stock());
            values.put(KEY_TRACK_STOCK,data.getComposite_item_track_stock());
            values.put(KEY_BATCH,data.getBatch_no());
            values.put(KEY_BAR_CODE,data.getBarcode_no());
            values.put(KEY_PRODUCT,data.getProdct_no());
            values.put(KEY_SOLDBY,data.getSold_by());
            values.put(KEY_PURCHASE_UNIT,data.getPurchased_unit_name());
            values.put(KEY_USED_UNIT,data.getUsed_unit_name());
            values.put(KEY_PURCHASE_UNITID,data.getPurchased_unit_id());
            values.put(KEY_USED_UNITID,data.getUsed_unit_id());
            values.put(KEY_SALE_WEIGHT,data.getDefault_sale_value());
            values.put(KEY_TAX,jsonArray+"");
            values.put(KEY_TAX_AMT,data.getTax_amt());
            values.put(KEY_OFFER,data.getOffers());
            values.put(KEY_DISCOUNT,data.getDiscount_amount());
            if(data.getHsn_no()!=null)
                values.put(KEY_hsn_no,data.getHsn_no());
            if(data.getAllow_open_price()!=null && data.getAllow_open_price().trim().length()>0) {
                values.put(KEY_change_price, data.getAllow_open_price());
                if(data.getAllow_open_price().equals("true")){
                    values.put(KEY_OFFER,"");
                    values.put(KEY_DISCOUNT,"");
                }
            }else
                values.put(KEY_change_price,"false");
            long i=db.insert(TBL_DISH, null, values);
            // Log.d(TAG,"insert dish:"+i);
            db.close();
        }catch (JSONException e){

        }catch (SQLiteException e){

        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public int getDishCount(){
        String selectQuery = "SELECT  * FROM " + TBL_DISH ;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            int cnt= cursor.getCount();
            cursor.close();
            db.close();
            return cnt;
        }catch (SQLiteException e){

        }
        return 0;
    }

    public List<DishPojo> getDishes(String cuisineid, Context context) {
        List<DishPojo> dishdetaillist = new ArrayList<DishPojo>();
        // Select All Query
        String selectQuery;
        if(cuisineid.equals("0")){
            if(M.isItemAlpha(context))
                selectQuery= "SELECT  * FROM " + TBL_DISH +" ORDER BY "+KEY_DNAME;
            else
                selectQuery= "SELECT  * FROM " + TBL_DISH;
        }else if(cuisineid.equals("-1")){
            selectQuery= "SELECT  * FROM " + TBL_DISH+" where "+KEY_DIMG+"!=''";
        }else {
            if (M.isItemAlpha(context))
                selectQuery = "SELECT  * FROM " + TBL_DISH + " where " + KEY_CUSINEID + " ='" + cuisineid + "' order by "+KEY_DNAME;
            else
                selectQuery = "SELECT  * FROM " + TBL_DISH + " where " + KEY_CUSINEID + " ='" + cuisineid + "'";
        }
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                DBPreferences dbPreferences = new DBPreferences(context);
                DBCombo dbCombo = new DBCombo(context);

                do {
                    DishPojo data = new DishPojo();
                    String dishid = cursor.getString(cursor.getColumnIndexOrThrow(KEY_DISHID));
                    String price=(cursor.getString(cursor.getColumnIndexOrThrow(KEY_PRICE)));
                    String pricewotax=(cursor.getString(cursor.getColumnIndexOrThrow(KEY_PRICE_WITHOUT_TAX)));
                    String taxamt=cursor.getString(cursor.getColumnIndexOrThrow(KEY_TAX_AMT));

                    data.setDishid(dishid);
                    data.setCusineid((cursor.getString(cursor.getColumnIndexOrThrow(KEY_CUSINEID))));
                    data.setDishname((cursor.getString(cursor.getColumnIndexOrThrow(KEY_DNAME))));
                    data.setDishimage((cursor.getString(cursor.getColumnIndexOrThrow(KEY_DIMG))));
                    data.setDescription((cursor.getString(cursor.getColumnIndexOrThrow(KEY_DIS))));
                    data.setPrice(price);
                    if(pricewotax!=null && pricewotax.trim().length()>0)
                        data.setPrice_without_tax(pricewotax);
                    else
                        data.setPrice_without_tax(price);
                    data.setComposite_item_track_stock(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TRACK_STOCK)));
                    data.setIn_stock(cursor.getString(cursor.getColumnIndexOrThrow(KEY_CURRENT_STOCK)));
                    data.setBarcode_no(cursor.getString(cursor.getColumnIndexOrThrow(KEY_BAR_CODE)));
                    data.setBatch_no(cursor.getString(cursor.getColumnIndexOrThrow(KEY_BATCH)));
                    data.setProdct_no(cursor.getString(cursor.getColumnIndexOrThrow(KEY_PRODUCT)));
                    data.setSold_by(cursor.getString(cursor.getColumnIndexOrThrow(KEY_SOLDBY)));
                    data.setPurchased_unit_name(cursor.getString(cursor.getColumnIndexOrThrow(KEY_PURCHASE_UNIT)));
                    data.setUsed_unit_name(cursor.getString(cursor.getColumnIndexOrThrow(KEY_USED_UNIT)));
                    data.setPurchased_unit_id(cursor.getString(cursor.getColumnIndexOrThrow(KEY_PURCHASE_UNITID)));
                    data.setUsed_unit_id(cursor.getString(cursor.getColumnIndexOrThrow(KEY_USED_UNITID)));
                    data.setDefault_sale_value(cursor.getString(cursor.getColumnIndexOrThrow(KEY_SALE_WEIGHT)));
                    data.setOffers(cursor.getString(cursor.getColumnIndexOrThrow(KEY_OFFER)));
                    data.setDiscount_amount(cursor.getString(cursor.getColumnIndexOrThrow(KEY_DISCOUNT)));
                    data.setHsn_no(cursor.getString(cursor.getColumnIndexOrThrow(KEY_hsn_no)));
                    data.setAllow_open_price(cursor.getString(cursor.getColumnIndexOrThrow(KEY_change_price)));
                    if(taxamt!=null && taxamt.trim().length()>0)
                        data.setTax_amt(taxamt);
                    else
                        data.setTax_amt("0");

                    List<PreModel> pre = dbPreferences.getPref(dishid);
                    if (pre != null && pre.size() > 0) {
                        data.setPreflag("true");
                        data.setPre(pre);
                    } else {
                        data.setPreflag("false");
                    }

                    List<ComboModel> combo = dbCombo.getcombo(dishid);
                    if (combo != null && combo.size() > 0) {
                        data.setCombo_flag("true");
                        data.setCombo_item(combo);
                    } else {
                        data.setCombo_flag("false");
                    }

                    String taxdata=cursor.getString(cursor.getColumnIndexOrThrow(KEY_TAX ));
                    if(taxdata!=null && taxdata.trim().length()>0) {
                        List<TaxData> tlist = new ArrayList<>();
                        JSONArray jsonArray = new JSONArray(taxdata);
                        if (jsonArray != null && jsonArray.length() > 0) {
                            tlist.clear();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject j = jsonArray.getJSONObject(i);
                                TaxData t = new TaxData();
                                t.setId(j.getString("id"));
                                t.setTax_amount(j.getString("tax_amount"));
                                t.setText(j.getString("text"));
                                t.setValue(j.getString("value"));
                                tlist.add(t);
                            }
                        }
                        data.setTax_data(tlist);
                    }
                    dishdetaillist.add(data);
                } while (cursor.moveToNext());
            }

            cursor.close();
            db.close();
        }catch (JSONException e){

        }catch (SQLiteException e){

        }
        return dishdetaillist;
    }

    public List<DishPojo> filterItems(String txt, Context context) {
        List<DishPojo> dishdetaillist = new ArrayList<DishPojo>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TBL_DISH + " where "+KEY_DNAME+" like '%"+txt+"%' or "+
                KEY_BATCH+" like '%"+txt+"%' or "+ KEY_BAR_CODE+" like '%"+txt+"%' or "+KEY_PRODUCT+" like '%"+txt+"%'";
        //Log.d(TAG,selectQuery);
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                DBPreferences dbPreferences = new DBPreferences(context);
                DBCombo dbCombo = new DBCombo(context);

                do {
                    DishPojo data = new DishPojo();
                    String dishid = cursor.getString(cursor.getColumnIndexOrThrow(KEY_DISHID));
                    String price=(cursor.getString(cursor.getColumnIndexOrThrow(KEY_PRICE)));
                    String pricewotax=(cursor.getString(cursor.getColumnIndexOrThrow(KEY_PRICE_WITHOUT_TAX)));
                    String taxamt=cursor.getString(cursor.getColumnIndexOrThrow(KEY_TAX_AMT));

                    data.setDishid(dishid);
                    data.setCusineid((cursor.getString(cursor.getColumnIndexOrThrow(KEY_CUSINEID))));
                    data.setDishname((cursor.getString(cursor.getColumnIndexOrThrow(KEY_DNAME))));
                    data.setDishimage((cursor.getString(cursor.getColumnIndexOrThrow(KEY_DIMG))));
                    data.setDescription((cursor.getString(cursor.getColumnIndexOrThrow(KEY_DIS))));
                    data.setPrice(price);
                    if(pricewotax!=null && pricewotax.trim().length()>0)
                        data.setPrice_without_tax(pricewotax);
                    else
                        data.setPrice_without_tax(price);
                    data.setComposite_item_track_stock(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TRACK_STOCK)));
                    data.setIn_stock(cursor.getString(cursor.getColumnIndexOrThrow(KEY_CURRENT_STOCK)));
                    data.setBarcode_no(cursor.getString(cursor.getColumnIndexOrThrow(KEY_BAR_CODE)));
                    data.setBatch_no(cursor.getString(cursor.getColumnIndexOrThrow(KEY_BATCH)));
                    data.setProdct_no(cursor.getString(cursor.getColumnIndexOrThrow(KEY_PRODUCT)));
                    data.setSold_by(cursor.getString(cursor.getColumnIndexOrThrow(KEY_SOLDBY)));
                    data.setPurchased_unit_name(cursor.getString(cursor.getColumnIndexOrThrow(KEY_PURCHASE_UNIT)));
                    data.setUsed_unit_name(cursor.getString(cursor.getColumnIndexOrThrow(KEY_USED_UNIT)));
                    data.setPurchased_unit_id(cursor.getString(cursor.getColumnIndexOrThrow(KEY_PURCHASE_UNITID)));
                    data.setUsed_unit_id(cursor.getString(cursor.getColumnIndexOrThrow(KEY_USED_UNITID)));
                    data.setDefault_sale_value(cursor.getString(cursor.getColumnIndexOrThrow(KEY_SALE_WEIGHT)));
                    data.setOffers(cursor.getString(cursor.getColumnIndexOrThrow(KEY_OFFER)));
                    data.setDiscount_amount(cursor.getString(cursor.getColumnIndexOrThrow(KEY_DISCOUNT)));
                    data.setHsn_no(cursor.getString(cursor.getColumnIndexOrThrow(KEY_hsn_no)));
                    data.setAllow_open_price(cursor.getString(cursor.getColumnIndexOrThrow(KEY_change_price)));
                    if(taxamt!=null && taxamt.trim().length()>0)
                        data.setTax_amt(taxamt);
                    else
                        data.setTax_amt("0");

                    List<PreModel> pre = dbPreferences.getPref(dishid);
                    if (pre != null && pre.size() > 0) {
                        data.setPreflag("true");
                        data.setPre(pre);
                    } else {
                        data.setPreflag("false");
                    }

                    List<ComboModel> combo = dbCombo.getcombo(dishid);
                    if (combo != null && combo.size() > 0) {
                        data.setCombo_flag("true");
                        data.setCombo_item(combo);
                    } else {
                        data.setCombo_flag("false");
                    }

                    String taxdata=cursor.getString(cursor.getColumnIndexOrThrow(KEY_TAX ));
                    if(taxdata!=null && taxdata.trim().length()>0) {
                        List<TaxData> tlist = new ArrayList<>();
                        JSONArray jsonArray = new JSONArray(taxdata);
                        if (jsonArray != null && jsonArray.length() > 0) {
                            tlist.clear();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject j = jsonArray.getJSONObject(i);
                                TaxData t = new TaxData();
                                t.setId(j.getString("id"));
                                t.setTax_amount(j.getString("tax_amount"));
                                t.setText(j.getString("text"));
                                t.setValue(j.getString("value"));
                                tlist.add(t);
                            }
                        }
                        data.setTax_data(tlist);
                    }
                    dishdetaillist.add(data);
                } while (cursor.moveToNext());
            }

            cursor.close();
            db.close();
        }catch (JSONException e){

        }catch (SQLiteException e){

        }
        return dishdetaillist;
    }

    public DishPojo getDishData(String id, Context context) {
        DishPojo dishdetaillist =new DishPojo();
        // Select All Query
        String selectQuery;
        selectQuery= "SELECT  * FROM " + TBL_DISH +" where "+KEY_DISHID+" ='"+id+"'";
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                DBPreferences dbPreferences = new DBPreferences(context);
                DBCombo dbCombo = new DBCombo(context);
                do {
                    DishPojo data = new DishPojo();
                    String dishid = cursor.getString(cursor.getColumnIndexOrThrow(KEY_DISHID));
                    String price=(cursor.getString(cursor.getColumnIndexOrThrow(KEY_PRICE)));
                    String pricewotax=(cursor.getString(cursor.getColumnIndexOrThrow(KEY_PRICE_WITHOUT_TAX)));
                    String taxamt=cursor.getString(cursor.getColumnIndexOrThrow(KEY_TAX_AMT));

                    data.setDishid(dishid);
                    data.setCusineid((cursor.getString(cursor.getColumnIndexOrThrow(KEY_CUSINEID))));
                    data.setDishname((cursor.getString(cursor.getColumnIndexOrThrow(KEY_DNAME))));
                    data.setDishimage((cursor.getString(cursor.getColumnIndexOrThrow(KEY_DIMG))));
                    data.setDescription((cursor.getString(cursor.getColumnIndexOrThrow(KEY_DIS))));
                    data.setOffers(cursor.getString(cursor.getColumnIndexOrThrow(KEY_OFFER)));
                    data.setDiscount_amount(cursor.getString(cursor.getColumnIndexOrThrow(KEY_DISCOUNT)));
                    data.setPrice(price);
                    if(pricewotax!=null && pricewotax.trim().length()>0)
                        data.setPrice_without_tax(pricewotax);
                    else
                        data.setPrice_without_tax(price);
                    data.setComposite_item_track_stock(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TRACK_STOCK)));
                    data.setIn_stock(cursor.getString(cursor.getColumnIndexOrThrow(KEY_CURRENT_STOCK)));
                    data.setBarcode_no(cursor.getString(cursor.getColumnIndexOrThrow(KEY_BAR_CODE)));
                    data.setBatch_no(cursor.getString(cursor.getColumnIndexOrThrow(KEY_BATCH)));
                    data.setProdct_no(cursor.getString(cursor.getColumnIndexOrThrow(KEY_PRODUCT)));
                    data.setSold_by(cursor.getString(cursor.getColumnIndexOrThrow(KEY_SOLDBY)));
                    data.setPurchased_unit_name(cursor.getString(cursor.getColumnIndexOrThrow(KEY_PURCHASE_UNIT)));
                    data.setUsed_unit_name(cursor.getString(cursor.getColumnIndexOrThrow(KEY_USED_UNIT)));
                    data.setPurchased_unit_id(cursor.getString(cursor.getColumnIndexOrThrow(KEY_PURCHASE_UNITID)));
                    data.setUsed_unit_id(cursor.getString(cursor.getColumnIndexOrThrow(KEY_USED_UNITID)));
                    data.setDefault_sale_value(cursor.getString(cursor.getColumnIndexOrThrow(KEY_SALE_WEIGHT)));
                    data.setHsn_no(cursor.getString(cursor.getColumnIndexOrThrow(KEY_hsn_no)));
                    data.setAllow_open_price(cursor.getString(cursor.getColumnIndexOrThrow(KEY_change_price)));
                    if(taxamt!=null && taxamt.trim().length()>0)
                        data.setTax_amt(taxamt);
                    else
                        data.setTax_amt("0");

                    List<PreModel> pre = dbPreferences.getPref(dishid);
                    if (pre != null && pre.size() > 0) {
                        data.setPreflag("true");
                        data.setPre(pre);
                    } else {
                        data.setPreflag("false");
                    }

                    List<ComboModel> combo = dbCombo.getcombo(dishid);
                    if (combo != null && combo.size() > 0) {
                        data.setCombo_flag("true");
                        data.setCombo_item(combo);
                    } else {
                        data.setCombo_flag("false");
                    }

                    String taxdata=cursor.getString(cursor.getColumnIndexOrThrow(KEY_TAX ));
                    if(taxdata!=null && taxdata.trim().length()>0) {
                        List<TaxData> tlist = new ArrayList<>();
                        JSONArray jsonArray = new JSONArray(taxdata);
                        if (jsonArray != null && jsonArray.length() > 0) {
                            tlist.clear();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject j = jsonArray.getJSONObject(i);
                                TaxData t = new TaxData();
                                t.setId(j.getString("id"));
                                t.setTax_amount(j.getString("tax_amount"));
                                t.setText(j.getString("text"));
                                t.setValue(j.getString("value"));
                                tlist.add(t);
                            }
                        }
                        data.setTax_data(tlist);
                    }
                    dishdetaillist=data;
                } while (cursor.moveToNext());
            }

            cursor.close();
            db.close();
        }catch (JSONException e){

        }catch (SQLiteException e){

        }
        return dishdetaillist;
    }

    public void deleteallcuisine() {
        // Select All Query
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete(TBL_DISH, null, null);
            db.close();
        }catch (SQLiteException e){

        }
    }
}
