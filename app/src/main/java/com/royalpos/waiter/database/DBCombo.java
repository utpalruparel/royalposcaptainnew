package com.royalpos.waiter.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import com.royalpos.waiter.model.ComboModel;

import java.util.ArrayList;
import java.util.List;

public class DBCombo extends SQLiteOpenHelper {

    public static final String TBL_PRE = "tblcombo";

    public static final String KEY_ID = "id";
    public static final String KEY_COMBO_ID = "combo_id";
    public static final String KEY_COMBO_NAME = "combo_name";
    public static final String KEY_PRE_QTY = "combo_qty";
    public static final String KEY_DISH_ID = "dish_id";
    public static final String CREATE_PRE = "CREATE TABLE IF NOT EXISTS " + TBL_PRE + "("
            + KEY_ID + " INTEGER PRIMARY KEY, " +KEY_COMBO_ID + " TEXT, " + KEY_COMBO_NAME
            + " TEXT, " + KEY_PRE_QTY + " TEXT,"+
            KEY_DISH_ID + " TEXT" + ")";

    public DBCombo(Context c) {
        super(c, DBCusines.DATABASE_NAME, null, DBCusines.DATABASE_VERSION);
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL(CREATE_PRE);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    public void addcombo(ComboModel data, String dishid) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();

            db.execSQL(CREATE_PRE);
            ContentValues values = new ContentValues();
            values.put(KEY_COMBO_ID, data.getItem_id());
            values.put(KEY_COMBO_NAME, data.getItem_name());
            values.put(KEY_PRE_QTY, data.getQty());
            values.put(KEY_DISH_ID, dishid);

            db.insert(TBL_PRE, null, values);
            db.close();
        }catch (SQLiteException e){

        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public List<ComboModel> getcombo(String dishid) {
        List<ComboModel> prelist = new ArrayList<ComboModel>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TBL_PRE+" WHERE "+KEY_DISH_ID+" ='"+dishid+"'" ;
        try {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                ComboModel data = new ComboModel();
                data.setItem_id(cursor.getString(cursor.getColumnIndexOrThrow(KEY_COMBO_ID)));
                data.setItem_name((cursor.getString(cursor.getColumnIndexOrThrow(KEY_COMBO_NAME))));
                data.setQty((cursor.getString(cursor.getColumnIndexOrThrow(KEY_PRE_QTY))));
                data.setDishid((cursor.getString(cursor.getColumnIndexOrThrow(KEY_DISH_ID))));
                prelist.add(data);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        }catch (SQLiteException e){

        }
        return prelist;
    }

    public List<ComboModel> getModifier(String dishid, String pref) {
        List<ComboModel> prelist = new ArrayList<ComboModel>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TBL_PRE+" WHERE "+KEY_DISH_ID+" ='"+dishid+"' " +
                "and "+KEY_COMBO_ID+" in ("+pref+")" ;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    ComboModel data = new ComboModel();
                    data.setItem_id(cursor.getString(cursor.getColumnIndexOrThrow(KEY_COMBO_ID)));
                    data.setItem_name((cursor.getString(cursor.getColumnIndexOrThrow(KEY_COMBO_NAME))));
                    data.setQty((cursor.getString(cursor.getColumnIndexOrThrow(KEY_PRE_QTY))));
                    data.setDishid((cursor.getString(cursor.getColumnIndexOrThrow(KEY_DISH_ID))));

                    prelist.add(data);
                } while (cursor.moveToNext());
            }

            cursor.close();
            db.close();
        }catch (SQLiteException e){

        }
        return prelist;
    }

    public void deleteAllPre() {
        // Select All Query
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete(TBL_PRE,null,null);
            db.close();
        }catch (SQLiteException e){

        }
    }

}
