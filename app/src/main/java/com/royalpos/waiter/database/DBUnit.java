package com.royalpos.waiter.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import com.royalpos.waiter.model.UnitPojo;

import java.util.ArrayList;
import java.util.List;

public class DBUnit extends SQLiteOpenHelper {

    public static final String TBL_UNIT = "tblunit";
    public static final String KEY_ID = "id";
    public static final String KEY_PURCHASE_ID = "purchase_id";
    public static final String KEY_PURCHASE_NAME = "purchase_name";
    public static final String KEY_PURCHASE_SORT = "purchase_sortname";
    public static final String KEY_USE_ID = "use_id";
    public static final String KEY_USE_NAME = "use_name";
    public static final String KEY_USE_SORT = "use_sortname";
    public static final String KEY_UNIT_FORMULA = "unit_formula";
    String TAG="DBUnit";
    public static final String CREATE_UNIT = "CREATE TABLE IF NOT EXISTS " + TBL_UNIT + "("
            + KEY_ID + " INTEGER PRIMARY KEY, " +KEY_PURCHASE_ID + " TEXT, " + KEY_PURCHASE_NAME + " TEXT, "+KEY_PURCHASE_SORT + " TEXT, "+
            KEY_USE_ID + " TEXT, " + KEY_USE_NAME + " TEXT, "+KEY_USE_SORT + " TEXT, "+KEY_UNIT_FORMULA+" TEXT)";


    public DBUnit(Context c) {
        super(c, DBCusines.DATABASE_NAME, null, DBCusines.DATABASE_VERSION);
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(CREATE_UNIT);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    public void addUnit(UnitPojo data) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String CREATE_UNIT = "CREATE TABLE IF NOT EXISTS " + TBL_UNIT + "("
                    + KEY_ID + " INTEGER PRIMARY KEY, " +KEY_PURCHASE_ID + " TEXT, " + KEY_PURCHASE_NAME + " TEXT, "+KEY_PURCHASE_SORT + " TEXT, "+
                    KEY_USE_ID + " TEXT, " + KEY_USE_NAME + " TEXT, "+KEY_USE_SORT + " TEXT, "+KEY_UNIT_FORMULA+" TEXT)";

            db.execSQL(CREATE_UNIT);
            ContentValues values = new ContentValues();
            values.put(KEY_PURCHASE_ID, data.getPurchased_unit_id());
            values.put(KEY_PURCHASE_NAME, data.getPurchased_unit_name());
            values.put(KEY_PURCHASE_SORT, data.getPurchased_unit_display_name());
            values.put(KEY_USE_ID, data.getUsed_unit_id());
            values.put(KEY_USE_NAME, data.getUsed_unit_name());
            values.put(KEY_USE_SORT, data.getUsed_unit_display_name());
            values.put(KEY_UNIT_FORMULA, data.getUsed_unit());

            long i=db.insert(TBL_UNIT, null, values);

            db.close();
        }catch (SQLiteException e){

        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public int getUnitCount(){
        String selectQuery = "SELECT  * FROM " + TBL_UNIT ;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            if (db != null && db.isOpen()) {
                Cursor cursor = db.rawQuery(selectQuery, null);

                int cnt= cursor.getCount();
                cursor.close();
                return cnt;
            }
        }catch (SQLiteException e){

        }
        return 0;
    }

    public String getSortName(String pid,String uid){
        String selectQuery ;
        if(uid!=null)
            selectQuery = "SELECT  "+KEY_USE_SORT+" FROM " + TBL_UNIT+" where "+KEY_USE_ID+"='"+uid+"'";
        else
            selectQuery = "SELECT  "+KEY_PURCHASE_SORT+" FROM " + TBL_UNIT+" where "+KEY_PURCHASE_ID+"='"+pid+"'";
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            if (db != null && db.isOpen()) {
                Cursor cursor = db.rawQuery(selectQuery, null);
                int cnt= cursor.getCount();
                if(cnt>0) {
                    cursor.moveToFirst();
                    return cursor.getString(0);
                }
                cursor.close();
                return null;
            }
        }catch (SQLiteException e){

        }
        return null;
    }

    public UnitPojo getUnitInfo(String pid,Context context) {
        // Select All Query
        String selectQuery;
        selectQuery= "SELECT  * FROM " + TBL_UNIT+" where "+KEY_PURCHASE_ID+"='"+pid+"'";
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    UnitPojo data = new UnitPojo();
                    data.setPurchased_unit_id(cursor.getString(cursor.getColumnIndexOrThrow(KEY_PURCHASE_ID)));
                    data.setPurchased_unit_name(cursor.getString(cursor.getColumnIndexOrThrow(KEY_PURCHASE_NAME)));
                    data.setUsed_unit_id(cursor.getString(cursor.getColumnIndexOrThrow(KEY_USE_ID)));
                    data.setUsed_unit_name(cursor.getString(cursor.getColumnIndexOrThrow(KEY_USE_NAME)));
                    data.setUsed_unit(cursor.getString(cursor.getColumnIndexOrThrow(KEY_UNIT_FORMULA)));
                    data.setPurchased_unit_display_name(cursor.getString(cursor.getColumnIndexOrThrow(KEY_PURCHASE_SORT)));
                    data.setUsed_unit_display_name(cursor.getString(cursor.getColumnIndexOrThrow(KEY_USE_SORT)));
                    return data;
                } while (cursor.moveToNext());
            }

            cursor.close();
            db.close();
        }catch (SQLiteException e){

        }
        return null;
    }

    public UnitPojo getUsedUnitInfo(String pid,Context context) {
        // Select All Query
        String selectQuery;
        selectQuery= "SELECT  * FROM " + TBL_UNIT+" where "+KEY_USE_ID+"='"+pid+"'";
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    UnitPojo data = new UnitPojo();
                    data.setPurchased_unit_id(cursor.getString(cursor.getColumnIndexOrThrow(KEY_PURCHASE_ID)));
                    data.setPurchased_unit_name(cursor.getString(cursor.getColumnIndexOrThrow(KEY_PURCHASE_NAME)));
                    data.setUsed_unit_id(cursor.getString(cursor.getColumnIndexOrThrow(KEY_USE_ID)));
                    data.setUsed_unit_name(cursor.getString(cursor.getColumnIndexOrThrow(KEY_USE_NAME)));
                    data.setUsed_unit(cursor.getString(cursor.getColumnIndexOrThrow(KEY_UNIT_FORMULA)));
                    data.setPurchased_unit_display_name(cursor.getString(cursor.getColumnIndexOrThrow(KEY_PURCHASE_SORT)));
                    data.setUsed_unit_display_name(cursor.getString(cursor.getColumnIndexOrThrow(KEY_USE_SORT)));
                    return data;
                } while (cursor.moveToNext());
            }

            cursor.close();
            db.close();
        }catch (SQLiteException e){

        }
        return null;
    }



    public List<UnitPojo> getDishes(Context context) {
        List<UnitPojo> dishdetaillist = new ArrayList<UnitPojo>();
        // Select All Query
        String selectQuery;
            selectQuery= "SELECT  * FROM " + TBL_UNIT;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    UnitPojo data = new UnitPojo();
                    data.setPurchased_unit_id(cursor.getString(cursor.getColumnIndexOrThrow(KEY_PURCHASE_ID)));
                    data.setPurchased_unit_name(cursor.getString(cursor.getColumnIndexOrThrow(KEY_PURCHASE_NAME)));
                    data.setUsed_unit_id(cursor.getString(cursor.getColumnIndexOrThrow(KEY_USE_ID)));
                    data.setUsed_unit_name(cursor.getString(cursor.getColumnIndexOrThrow(KEY_USE_NAME)));
                    data.setUsed_unit(cursor.getString(cursor.getColumnIndexOrThrow(KEY_UNIT_FORMULA)));
                    data.setPurchased_unit_display_name(cursor.getString(cursor.getColumnIndexOrThrow(KEY_PURCHASE_SORT)));
                    data.setUsed_unit_display_name(cursor.getString(cursor.getColumnIndexOrThrow(KEY_USE_SORT)));
                    dishdetaillist.add(data);
                } while (cursor.moveToNext());
            }

            cursor.close();
            db.close();
        }catch (SQLiteException e){

        }
        return dishdetaillist;
    }

    public void deleteall() {
        // Select All Query
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete(TBL_UNIT, null, null);
            db.close();
        }catch (SQLiteException e){

        }
    }
}
