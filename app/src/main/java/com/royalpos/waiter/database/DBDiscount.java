package com.royalpos.waiter.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import com.royalpos.waiter.model.DiscountPojo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Reeva on 10/24/2018
 */
public class DBDiscount extends SQLiteOpenHelper {

    public static final String TBL_DISCOUNT = "tbldiscount";

    public static final String KEY_ID = "id";
    public static final String KEY_DID = "dis_id";
    public static final String KEY_TEXT = "dis_name";
    public static final String KEY_PER = "dis_per";
    public static final String KEY_VALUE = "dis_val";
    public static final String KEY_DTYPE = "dis_type";
    // per,amount,free item
    public static final String KEY_OTYPE = "offer_type";
    // Item (per,amount)
    // Bill amount (per,amount,free item)
    public static final String KEY_ITEMID = "item_id";
    public static final String KEY_BILLAMT = "bill_amount";
    String TAG="DBDiscount";
    public static final String CREATE_DIS = "CREATE TABLE IF NOT EXISTS " + TBL_DISCOUNT + "("
            + KEY_ID + " INTEGER PRIMARY KEY, " +KEY_DID+" TEXT, "+KEY_TEXT + " TEXT, " + KEY_VALUE + " TEXT, "+ KEY_PER+" TEXT,"+
            KEY_DTYPE+" TEXT,"+KEY_OTYPE+" TEXT,"+KEY_ITEMID+" TEXT,"+KEY_BILLAMT+" TEXT)";

    public DBDiscount(Context c) {
        super(c, DBCusines.DATABASE_NAME, null, DBCusines.DATABASE_VERSION);
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(CREATE_DIS);
        db.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) { }

    public void addDiscount(DiscountPojo data) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            db.execSQL(CREATE_DIS);

            ContentValues values = new ContentValues();
            values.put(KEY_DID, data.getId());
            values.put(KEY_TEXT, data.getName());
            values.put(KEY_PER,data.getDiscount_percentage());
            values.put(KEY_VALUE,data.getDiscount_amt());
            values.put(KEY_DTYPE,data.getDiscount_type());
            values.put(KEY_BILLAMT,data.getBill_amt());
            values.put(KEY_ITEMID,data.getItem_id());
            values.put(KEY_OTYPE,"Bill Amount");
            db.insert(TBL_DISCOUNT, null, values);
            db.close();
        }catch (SQLiteException e){

        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public int getDiscCount(){
        String selectQuery = "SELECT  * FROM " + TBL_DISCOUNT ;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            if(db!=null && db.isOpen()) {
                Cursor cursor = db.rawQuery(selectQuery, null);
                return cursor.getCount();
            }
        }catch (SQLiteException e){

        }
        return 0;
    }

    public List<DiscountPojo> discountList(double billamt) {
        List<DiscountPojo> list = new ArrayList<DiscountPojo>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TBL_DISCOUNT+" where cast("+KEY_BILLAMT+" as float) <="+billamt ;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    DiscountPojo data = new DiscountPojo();
                    data.setId(cursor.getString(cursor.getColumnIndexOrThrow(KEY_DID)));
                    data.setDiscount_amt((cursor.getString(cursor.getColumnIndexOrThrow(KEY_VALUE))));
                    data.setBill_amt((cursor.getString(cursor.getColumnIndexOrThrow(KEY_BILLAMT))));
                    data.setName((cursor.getString(cursor.getColumnIndexOrThrow(KEY_TEXT))));
                    data.setDiscount_percentage((cursor.getString(cursor.getColumnIndexOrThrow(KEY_PER))));
                    data.setDiscount_type((cursor.getString(cursor.getColumnIndexOrThrow(KEY_DTYPE))));
                    data.setItem_id((cursor.getString(cursor.getColumnIndexOrThrow(KEY_ITEMID))));

                    list.add(data);
                } while (cursor.moveToNext());
            }

            cursor.close();
            db.close();
        }catch (SQLiteException e){

        }
        return list;
    }

    public List<DiscountPojo> discountListDinIn(double billamt) {
        List<DiscountPojo> list = new ArrayList<DiscountPojo>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TBL_DISCOUNT+" where cast("+KEY_BILLAMT+" as float) <="+billamt+" and "+KEY_DTYPE+"!='complementary'" ;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    DiscountPojo data = new DiscountPojo();
                    data.setId(cursor.getString(cursor.getColumnIndexOrThrow(KEY_DID)));
                    data.setDiscount_amt((cursor.getString(cursor.getColumnIndexOrThrow(KEY_VALUE))));
                    data.setBill_amt((cursor.getString(cursor.getColumnIndexOrThrow(KEY_BILLAMT))));
                    data.setName((cursor.getString(cursor.getColumnIndexOrThrow(KEY_TEXT))));
                    data.setDiscount_percentage((cursor.getString(cursor.getColumnIndexOrThrow(KEY_PER))));
                    data.setDiscount_type((cursor.getString(cursor.getColumnIndexOrThrow(KEY_DTYPE))));
                    data.setItem_id((cursor.getString(cursor.getColumnIndexOrThrow(KEY_ITEMID))));

                    list.add(data);
                } while (cursor.moveToNext());
            }

            cursor.close();
            db.close();
        }catch (SQLiteException e){

        }
        return list;
    }

    public List<String> getDiscountsName(String ids) {
        List<String> list = new ArrayList<String>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TBL_DISCOUNT +" where "+KEY_DID+" IN ("+ids+")";
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    list.add((cursor.getString(cursor.getColumnIndexOrThrow(KEY_TEXT))));

                } while (cursor.moveToNext());
            }

            cursor.close();
            db.close();
        }catch (SQLiteException e){

        }
        return list;
    }

    public void deleteall() {
        // Select All Query
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete(TBL_DISCOUNT,null,null);
            db.close();
        }catch (SQLiteException e){

        }
    }

}
