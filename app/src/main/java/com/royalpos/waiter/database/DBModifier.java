package com.royalpos.waiter.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import com.royalpos.waiter.model.Modifier_cat;

import java.util.ArrayList;
import java.util.List;

public class DBModifier extends SQLiteOpenHelper {

    public static final String TBL_MOD = "tblmodifier";

    public static final String KEY_ID = "id";
    public static final String KEY_CAT_ID = "cat_id";
    public static final String KEY_CAT_NAME = "cat_name";
    public static final String KEY_SEL = "modifier_selection";
    public static final String KEY_DISH_ID = "dish_id";
    public static final String CREATE_MODI = "CREATE TABLE IF NOT EXISTS " + TBL_MOD + "("
            + KEY_ID + " INTEGER PRIMARY KEY, " +KEY_CAT_ID + " TEXT, " + KEY_CAT_NAME + " TEXT, " + KEY_SEL + " INTEGER,"+
            KEY_DISH_ID + " TEXT" + ")";

    public DBModifier(Context c) {
        super(c, DBCusines.DATABASE_NAME, null, DBCusines.DATABASE_VERSION);
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL(CREATE_MODI);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    public void addModi(Modifier_cat data, String dishid) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();

            db.execSQL(CREATE_MODI);
            ContentValues values = new ContentValues();
            values.put(KEY_CAT_ID, data.getCat_id());
            values.put(KEY_CAT_NAME, data.getCat_name());
            values.put(KEY_SEL, data.getModifier_selection());
            values.put(KEY_DISH_ID, dishid);

            db.insert(TBL_MOD, null, values);
            db.close();
        }catch (SQLiteException e){

        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public Modifier_cat getModifierName(String mid) {
        String selectQuery = "SELECT  * FROM " + TBL_MOD+" WHERE "+KEY_CAT_ID+" ='"+mid+"'" ;
        try {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Modifier_cat data = new Modifier_cat();
                data.setCat_id(cursor.getString(cursor.getColumnIndexOrThrow(KEY_CAT_ID)));
                data.setCat_name((cursor.getString(cursor.getColumnIndexOrThrow(KEY_CAT_NAME))));
                data.setModifier_selection((cursor.getInt(cursor.getColumnIndexOrThrow(KEY_SEL))));
                return  data;

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        }catch (SQLiteException e){

        }
        return null;
    }

    public List<Modifier_cat> getModifier(String dishid, String pref) {
        List<Modifier_cat> prelist = new ArrayList<Modifier_cat>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TBL_MOD+" WHERE "+KEY_DISH_ID+" ='"+dishid+"' and "+KEY_CAT_ID+" in ("+pref+")" ;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    Modifier_cat data = new Modifier_cat();
                    data.setCat_id(cursor.getString(cursor.getColumnIndexOrThrow(KEY_CAT_ID)));
                    data.setCat_name((cursor.getString(cursor.getColumnIndexOrThrow(KEY_CAT_NAME))));
                    data.setModifier_selection((cursor.getInt(cursor.getColumnIndexOrThrow(KEY_SEL))));
                    prelist.add(data);
                } while (cursor.moveToNext());
            }

            cursor.close();
            db.close();
        }catch (SQLiteException e){

        }
        return prelist;
    }

    public void deleteAllModi() {
        // Select All Query
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete(TBL_MOD,null,null);
            db.close();
        }catch (SQLiteException e){

        }
    }

    public void check(Modifier_cat m,String did){
        String selectQuery = "SELECT  * FROM " + TBL_MOD+" where "+KEY_CAT_ID+"="+m.getCat_id() ;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            if(cursor.getCount()==0){
                addModi(m,did);
            }
        }catch (SQLiteException e){

        }

    }

}
