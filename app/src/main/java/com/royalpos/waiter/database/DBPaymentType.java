package com.royalpos.waiter.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import android.util.Log;

import com.royalpos.waiter.model.PaymentModePojo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DBPaymentType extends SQLiteOpenHelper {

    public static final String TBL_TYPE = "tblpaymenttype";

    public static final String KEY_ID = "id";
    public static final String KEY_TYPE_ID = "payment_type_id";
    public static final String KEY_TYPE = "type";
    public static final String KEY_ROUNDING="is_rounding";
    String TAG="DBPaymentType";

    public static final String CREATE_TYPE = "CREATE TABLE  IF NOT EXISTS " + TBL_TYPE + "("
            + KEY_ID + " INTEGER PRIMARY KEY, " +KEY_TYPE_ID + " TEXT, " + KEY_TYPE + " TEXT, "+KEY_ROUNDING+" TEXT DEFAULT 'off')";

    public DBPaymentType(Context c) {
        super(c, DBCusines.DATABASE_NAME, null, DBCusines.DATABASE_VERSION);
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(CREATE_TYPE);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    public void addType(PaymentModePojo data) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            db.execSQL(CREATE_TYPE);
            ContentValues values = new ContentValues();
            values.put(KEY_TYPE_ID, data.getId());
            values.put(KEY_TYPE, data.getType());
            values.put(KEY_ROUNDING,data.getIs_rounding_on());
            long i = db.insert(TBL_TYPE, null, values);
            db.close();
        }catch (SQLiteException e){

        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public int getTypeCount(){
        String selectQuery = "SELECT  * FROM " + TBL_TYPE ;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            int cnt= cursor.getCount();
            cursor.close();
            db.close();
            return cnt;
        }catch (SQLiteException e){

        }
        return 0;
    }

    public List<PaymentModePojo> getPaymenttype() {
        List<PaymentModePojo> dishdetaillist = new ArrayList<PaymentModePojo>();
        SQLiteDatabase db = this.getWritableDatabase();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TBL_TYPE ;
        try {
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    PaymentModePojo data = new PaymentModePojo();
                    data.setId(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TYPE_ID)));
                    data.setType((cursor.getString(cursor.getColumnIndexOrThrow(KEY_TYPE))));
                    data.setIs_rounding_on(cursor.getString(cursor.getColumnIndexOrThrow(KEY_ROUNDING)));
                    dishdetaillist.add(data);
                } while (cursor.moveToNext());
            }

            cursor.close();
            db.close();
        }catch (SQLiteException e){
            String CREATE_TYPE = "CREATE TABLE IF NOT EXISTS " + DBPaymentType.TBL_TYPE + "("
                    + DBPaymentType.KEY_ID + " INTEGER PRIMARY KEY, " +DBPaymentType.KEY_TYPE_ID + " TEXT, " + DBPaymentType.KEY_TYPE + " TEXT, "+DBPaymentType.KEY_ROUNDING+" TEXT DEFAULT 'no')";
            db.execSQL(CREATE_TYPE);

            Cursor dbCursor1 = db.query( DBPaymentType.TBL_TYPE , null, null, null, null, null, null);
            List<String> slist1 = Arrays.asList(dbCursor1.getColumnNames());
            if (!slist1.contains(DBPaymentType.KEY_ROUNDING)){
                db.execSQL("ALTER TABLE " + DBPaymentType.TBL_TYPE  + " ADD COLUMN " + DBPaymentType.KEY_ROUNDING + " TEXT DEFAULT 'off'");
                Cursor cursor = db.rawQuery(selectQuery, null);

                if (cursor.moveToFirst()) {
                    do {
                        PaymentModePojo data = new PaymentModePojo();
                        data.setId(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TYPE_ID)));
                        data.setType((cursor.getString(cursor.getColumnIndexOrThrow(KEY_TYPE))));
                        data.setIs_rounding_on(cursor.getString(cursor.getColumnIndexOrThrow(KEY_ROUNDING)));
                        dishdetaillist.add(data);
                    } while (cursor.moveToNext());
                }

                cursor.close();
                db.close();
            }
        }
        return dishdetaillist;
    }

    public String getName(String id) {
        String txt="";
        String selectQuery = "SELECT  * FROM " + TBL_TYPE+" where "+KEY_TYPE_ID+"='"+id+"'" ;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    txt=cursor.getString(cursor.getColumnIndexOrThrow(KEY_TYPE));
                } while (cursor.moveToNext());
            }

            cursor.close();
            db.close();
        }catch (SQLiteException e){
        }
        return txt;
    }

    public void deletealltype() {
        // Select All Query
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete(TBL_TYPE,null,null);
            db.close();
        }catch (SQLiteException e){

        }
    }

}
