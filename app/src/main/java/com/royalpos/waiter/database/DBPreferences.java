package com.royalpos.waiter.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import com.royalpos.waiter.model.PreModel;

import java.util.ArrayList;
import java.util.List;

public class DBPreferences extends SQLiteOpenHelper {

    public static final String TBL_PRE = "tblpreference";

    public static final String KEY_ID = "id";
    public static final String KEY_PRE_ID = "pre_id";
    public static final String KEY_PRE_TYPE = "pre_type";
    public static final String KEY_PRE_NAME = "pre_name";
    public static final String KEY_PRE_PRICE = "pre_price";
    public static final String KEY_DISH_ID = "dish_id";
    public static final String CREATE_PRE = "CREATE TABLE IF NOT EXISTS " + TBL_PRE + "("
            + KEY_ID + " INTEGER PRIMARY KEY, " +KEY_PRE_ID + " TEXT, " + KEY_PRE_TYPE + " TEXT, " + KEY_PRE_NAME + " TEXT,"+
            KEY_PRE_PRICE + " TEXT,"+ KEY_DISH_ID + " TEXT" + ")";

    public DBPreferences(Context c) {
        super(c, DBCusines.DATABASE_NAME, null, DBCusines.DATABASE_VERSION);
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL(CREATE_PRE);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    }



    public void addPref(PreModel data, String dishid) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();

            db.execSQL(CREATE_PRE);
            ContentValues values = new ContentValues();
            values.put(KEY_PRE_ID, data.getPreid());
            values.put(KEY_PRE_TYPE, data.getPretype());
            values.put(KEY_PRE_NAME, data.getPrenm());
            values.put(KEY_PRE_PRICE, data.getPreprice());
            values.put(KEY_DISH_ID, dishid);

            db.insert(TBL_PRE, null, values);
            db.close();
        }catch (SQLiteException e){

        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public List<PreModel> getPref(String dishid) {
        List<PreModel> prelist = new ArrayList<PreModel>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TBL_PRE+" WHERE "+KEY_DISH_ID+" ='"+dishid+"'" ;
        try {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                PreModel data = new PreModel();
                data.setPreid(cursor.getString(cursor.getColumnIndexOrThrow(KEY_PRE_ID)));
                data.setPrenm((cursor.getString(cursor.getColumnIndexOrThrow(KEY_PRE_NAME))));
                data.setPreprice((cursor.getString(cursor.getColumnIndexOrThrow(KEY_PRE_PRICE))));
                data.setPretype((cursor.getString(cursor.getColumnIndexOrThrow(KEY_PRE_TYPE))));

                prelist.add(data);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        }catch (SQLiteException e){

        }
        return prelist;
    }

    public List<PreModel> getModifier(String dishid, String pref) {
        List<PreModel> prelist = new ArrayList<PreModel>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TBL_PRE+" WHERE "+KEY_DISH_ID+" ='"+dishid+"' and "+KEY_PRE_ID+" in ("+pref+")" ;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    PreModel data = new PreModel();
                    data.setPreid(cursor.getString(cursor.getColumnIndexOrThrow(KEY_PRE_ID)));
                    data.setPrenm((cursor.getString(cursor.getColumnIndexOrThrow(KEY_PRE_NAME))));
                    data.setPreprice((cursor.getString(cursor.getColumnIndexOrThrow(KEY_PRE_PRICE))));
                    data.setPretype((cursor.getString(cursor.getColumnIndexOrThrow(KEY_PRE_TYPE))));

                    prelist.add(data);
                } while (cursor.moveToNext());
            }

            cursor.close();
            db.close();
        }catch (SQLiteException e){

        }
        return prelist;
    }

    public void deleteAllPre() {
        // Select All Query
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete(TBL_PRE,null,null);
            db.close();
        }catch (SQLiteException e){

        }
    }

}
