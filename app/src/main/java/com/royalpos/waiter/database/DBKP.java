package com.royalpos.waiter.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.royalpos.waiter.model.KOPojo;

import java.util.ArrayList;

public class DBKP extends SQLiteOpenHelper {

    public static final String TBL_KPD = "tblkitchenprint";

    public static final String KEY_ID = "id";
    public static final String KEY_order_no = "order_no";
    public static final String KEY_time = "order_time";
    public static final String KEY_data = "print_data";
    public static final String KEY_is_upload = "is_upload";
    public static final String KEY_created_at = "created_at";
    String TAG="DBKP";
    public static final String CREATE_KPD = "CREATE TABLE IF NOT EXISTS " + TBL_KPD + "("
            + KEY_ID + " INTEGER PRIMARY KEY, " +KEY_order_no + " TEXT, " + KEY_time
            + " TEXT, " + KEY_data + " TEXT,"+KEY_is_upload + " TEXT,created_at DATETIME DEFAULT CURRENT_TIMESTAMP )";

    public DBKP(Context c) {
        super(c, DBCusines.DATABASE_NAME, null, DBCusines.DATABASE_VERSION);
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL(CREATE_KPD);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    public void addData(String ono,String tm,String data) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();

            db.execSQL(CREATE_KPD);
            ContentValues values = new ContentValues();
            values.put(KEY_order_no,ono);
            values.put(KEY_time, tm);
            values.put(KEY_data, data);
            values.put(KEY_is_upload, "no");

            db.insert(TBL_KPD, null, values);
            db.close();

        }catch (SQLiteException e){
            Log.d(TAG,"error:"+e.getMessage());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public int getCount(String ono) {
        int cnt=0;
        String selectQuery = "SELECT  * FROM " + TBL_KPD+" where "+KEY_order_no+"='"+ono+"'";
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            cnt=cursor.getCount();
            cursor.close();
            db.close();
        }catch (SQLiteException e){

        }
        return cnt;
    }

    public ArrayList<KOPojo> getList() {
        ArrayList<KOPojo> list=new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TBL_KPD+" order by id desc";
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    KOPojo koPojo=new KOPojo();
                    koPojo.setData(cursor.getString(cursor.getColumnIndexOrThrow(KEY_data)));
                    koPojo.setOrder_time(cursor.getString(cursor.getColumnIndexOrThrow(KEY_time)));
                    koPojo.setToken(cursor.getString(cursor.getColumnIndexOrThrow(KEY_order_no)));
                    koPojo.setId(cursor.getInt(cursor.getColumnIndexOrThrow(KEY_ID))+"");
                    list.add(koPojo);
                } while (cursor.moveToNext());
            }

            cursor.close();
            db.close();
        }catch (SQLiteException e){

        }
        return list;
    }

    public void deleteAll() {
        // Select All Query
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete(TBL_KPD,null,null);
            db.close();
        }catch (SQLiteException e){

        }
    }

}
