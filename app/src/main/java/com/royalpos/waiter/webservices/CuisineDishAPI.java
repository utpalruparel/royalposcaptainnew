package com.royalpos.waiter.webservices;

import com.royalpos.waiter.model.CuisineListPojo;
import com.royalpos.waiter.model.DiscountPojo;
import com.royalpos.waiter.model.DishPojo;
import com.royalpos.waiter.model.SuccessPojo;
import com.royalpos.waiter.model.UnitPojo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface CuisineDishAPI {

    @FormUrlEncoded
    @POST("get_cuisine_by_restaurant")
    Call<List<CuisineListPojo>> getCuisines(
            @Field("restaurant_id") String restaurant_id,
            @Field("rest_unique_id") String rest_unique_id
    );

    @FormUrlEncoded
    @POST("get_all_dish_modifier_new")
    Call<List<DishPojo>> getDishes(
            @Field("restaurant_id") String restaurant_id,
            @Field("rest_unique_id") String rest_unique_id
    );

    @FormUrlEncoded
    @POST("get_dish_by_cuisine_restaurant_modifier_new")
    Call<List<DishPojo>> getDishCuisine(
            @Field("restaurant_id") String restaurant_id,
            @Field("rest_unique_id") String rest_unique_id,
            @Field("cuisine_id") String cuisine_id
    );


    @FormUrlEncoded
    @POST("test_add_demo_data")
    Call<SuccessPojo> addDemoData(
            @Field("restaurant_id") String restaurant_id,
            @Field("rest_unique_id") String rest_unique_id
    );

    @FormUrlEncoded
    @POST("get_unit_list")
    Call<List<UnitPojo>> getUnitList(
            @Field("restaurant_id") String restaurant_id,
            @Field("rest_unique_id") String rest_unique_id
    );

    @FormUrlEncoded
    @POST("get_bill_amt_offer_data")
    Call<List<DiscountPojo>> billOffer(
            @Field("restaurant_id") String restaurant_id,
            @Field("rest_unique_id") String rest_unique_id
    );

}
