package com.royalpos.waiter.webservices;

import com.royalpos.waiter.model.DailyBalPojo;
import com.royalpos.waiter.model.SuccessPojo;
import com.royalpos.waiter.model.Waiter;
import com.royalpos.waiter.model.WaiterLoginPojo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface WaiterAPI {

    @FormUrlEncoded
    @POST("waiter_login")
    Call<WaiterLoginPojo> waiterLogin(
            @Field("restaurant_id") String restaurant_id,
            @Field("rest_unique_id") String rest_unique_id,
            @Field("username") String username,
            @Field("password") String password,
            @Field("checkpin") String checkpin,
            @Field("device_id") String device_id
    );

    @FormUrlEncoded
    @POST("get_captains")
    Call<List<Waiter>> getWaiters(
            @Field("restaurant_id") String restaurant_id,
            @Field("rest_unique_id") String rest_unique_id
    );

    @FormUrlEncoded
    @POST("balance_entry")
    Call<SuccessPojo> updateBalance(
            @Field("restaurant_id") String restaurant_id,
            @Field("rest_unique_id") String rest_unique_id,
            @Field("type") String type,//(opening/closing)
            @Field("user_id") String user_id,
            @Field("balance") String balance,
            @Field("daily_balance_id") String daily_balance_id,
            @Field("expected_amt") String expected_amt,
            @Field("difference_amt") String difference_amt,
            @Field("reason") String reason
    );

    @FormUrlEncoded
    @POST("check_daily_bal_add")
    Call<DailyBalPojo> dailybalanceInfo(
            @Field("restaurant_id") String restaurant_id,
            @Field("rest_unique_id") String rest_unique_id
    );

    @FormUrlEncoded
    @POST("waiter_logout")
    Call<SuccessPojo> waiterLogout(
            @Field("restaurant_id") String restaurant_id,
            @Field("rest_unique_id") String rest_unique_id,
            @Field("device_id") String device_id
    );
}
