package com.royalpos.waiter.webservices;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.royalpos.waiter.helper.AppConst;
import com.royalpos.waiter.model.M;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIServiceHeader {
    public APIServiceHeader() {

    }
    private static Context mContext;

    protected static Retrofit retrofit;
    public static <S> S createService(final Context context, Class<S> serviceClass) {

        mContext= context;

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);


//        File httpCacheDirectory = new File(Environment.getExternalStorageDirectory(), "RoyalPOS");// Here to facilitate the file directly on the SD Kagan catalog HttpCache in ， Generally put in context.getCacheDir() in
//        int cacheSize = 20 * 1024 * 1024;// Set cache file size 10M
//        Cache cache = new Cache(httpCacheDirectory, cacheSize);
         OkHttpClient httpClient = new OkHttpClient.Builder()
                 .addInterceptor(interceptor)
                .connectTimeout(30, TimeUnit.SECONDS)// Set connection timeout
                .readTimeout(30, TimeUnit.SECONDS)// Read timeout
                .writeTimeout(30, TimeUnit.SECONDS)// Write timeout
                 .addInterceptor(new Interceptor() {
                     @Override
                     public Response intercept(Chain chain) throws IOException {
                         Request original = chain.request();
                         Request request = original.newBuilder()
                                 .header("brand-id", M.getBrandId(context))
                                 .method(original.method(), original.body())
                                 .build();

                         return chain.proceed(request);
                     }
                 })
                .build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        retrofit=new Retrofit.Builder()
                .baseUrl(AppConst.MAIN)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return retrofit.create(serviceClass);
    }

//    static Interceptor REWRITE_CACHE_CONTROL_INTERCEPTOR = new Interceptor() {
//        @Override
//        public Response intercept(Chain chain) throws IOException {
//
//            Request request = chain.request();
//            Response response = chain.proceed(request);
//            ConnectionDetector connectionDetector=new ConnectionDetector(mContext);
//
//            if (connectionDetector.isConnectingToInternet()) {
//                // Get header information
//                String cacheControl =request.cacheControl().toString();
//                return response.newBuilder()
//                        .removeHeader("Pragma")// Clear header information ， Because server if not supported ， Will return some interference information ， Does not clear the following can not be effective
//                        .header("Cache-Control", cacheControl)
//                        .build();
//            }
//            return response;
//        }
//    };
}
