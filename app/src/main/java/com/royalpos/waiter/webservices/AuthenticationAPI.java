package com.royalpos.waiter.webservices;

import com.royalpos.waiter.model.AccountPojo;
import com.royalpos.waiter.model.CheckCodePojo;
import com.royalpos.waiter.model.CustomerPojo;
import com.royalpos.waiter.model.DeviceCodePojo;
import com.royalpos.waiter.model.LoginPojo;
import com.royalpos.waiter.model.MasterLoginPojo;
import com.royalpos.waiter.model.MembershipPojo;
import com.royalpos.waiter.model.RestaurantPojo;
import com.royalpos.waiter.model.SuccessPojo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface AuthenticationAPI {

    @FormUrlEncoded
    @POST("admin_app_login")
    Call<MasterLoginPojo> masterLogin(
            @Field("username") String username,
            @Field("password") String userpwd
    );

    @FormUrlEncoded
    @POST("restaurant_login")
    Call<LoginPojo> login(
            @Field("username") String username,
            @Field("password") String userpwd
    );

    @GET("rest_list")
    Call<List<RestaurantPojo>> getRestList();

    @FormUrlEncoded
    @POST("outlet_forgot_pwd")
    Call<SuccessPojo> forgotRestpwd(
            @Field("username") String username
    );

    @FormUrlEncoded
    @POST("search_customer")
    Call<List<CustomerPojo>> searchCustomer(
            @Field("restaurant_id") String restaurant_id,
            @Field("search") String search
    );

    @FormUrlEncoded
    @POST("splashscreen_api")
    Call<MembershipPojo> checkMembership(
            @Field("restaurant_id") String restaurant_id,
            @Field("rest_unique_id") String rest_unique_id,
            @Field("username") String username
    );

    @FormUrlEncoded
    @POST("get_outlet_status")
    Call<AccountPojo> getACStatus(
            @Field("restaurant_id") String restaurant_id
    );

    @FormUrlEncoded
    @POST("device_code_login_captain_app")
    Call<DeviceCodePojo> deviceCodeLogin(
            @Field("device_code") String device_code,
            @Field("device_id") String device_id,
            @Field("platform") String platform
    );

    @FormUrlEncoded
    @POST("check_device_code")
    Call<CheckCodePojo> checkCode(
            @Field("device_code") String device_code
    );


}
