package com.royalpos.waiter.webservices;

import com.google.gson.JsonObject;
import com.royalpos.waiter.model.PaymentModePojo;
import com.royalpos.waiter.model.PreferencePojo;
import com.royalpos.waiter.model.RoundingPojo;
import com.royalpos.waiter.model.SendOrderPojo;
import com.royalpos.waiter.model.SuccessPojo;
import com.royalpos.waiter.model.TransactionPojo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface OrderAPI {

    @FormUrlEncoded
    @POST("payment_mode")
    Call<List<PaymentModePojo>> getPaymentMode(
            @Field("restaurant_id") String restaurant_id,
            @Field("rest_unique_id") String rest_unique_id
    );

    @FormUrlEncoded
    @POST("get_rest_logo_footer")
    Call<List<PreferencePojo>> get_rest_logo_footer(
            @Field("restaurant_id") String restaurant_id,
            @Field("rest_unique_id") String rest_unique_id
    );

    @POST("send_take_away_order_email_to_customer")
    Call<SuccessPojo> sendEmailTakeAway(@Body SendOrderPojo jsonObject);

    @FormUrlEncoded
    @POST("sync_data")
    Call<RoundingPojo> getRoundingInfo(
            @Field("restaurant_id") String restaurant_id,
            @Field("rest_unique_id") String rest_unique_id,
            @Field("cashier_id") String cashier_id
    );

    @FormUrlEncoded
    @POST("init_txn_phone_pe")
    Call<TransactionPojo> initTransPhonePe(
            @Field("restaurant_id") String restaurant_id,
            @Field("rest_unique_id") String rest_unique_id,
            @Field("user_id") String user_id,
            @Field("order_no") String order_no,
            @Field("order_timestamp") String order_timestamp,
            @Field("amount") String amount,
            @Field("payment_mode") String payment_mode,//phonepe
            @Field("payment_type") String payment_type//dynamicqr
    );

    @FormUrlEncoded
    @POST("check_phone_pe_txn_status")
    Call<JsonObject> checkPhonePeStatus(
            @Field("restaurant_id") String restaurant_id,
            @Field("rest_unique_id") String rest_unique_id,
            @Field("transaction_id") String transaction_id
    );

    @FormUrlEncoded
    @POST("collect_payment_phone_pe")
    Call<TransactionPojo> collectPayment(
            @Field("restaurant_id") String restaurant_id,
            @Field("rest_unique_id") String rest_unique_id,
            @Field("user_id") String user_id,
            @Field("order_no") String order_no,
            @Field("order_timestamp") String order_timestamp,
            @Field("amount") String amount,
            @Field("payment_mode") String payment_mode,//phonepe
            @Field("payment_type") String payment_type,//collectphonepe
            @Field("phone_no") String phone_no
    );

    @FormUrlEncoded
    @POST("cancel_payment_request_phone_pe")
    Call<TransactionPojo> cancelPayment(
            @Field("restaurant_id") String restaurant_id,
            @Field("rest_unique_id") String rest_unique_id,
            @Field("transaction_id") String transaction_id
    );
}
