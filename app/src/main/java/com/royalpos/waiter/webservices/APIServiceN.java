package com.royalpos.waiter.webservices;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.royalpos.waiter.helper.AppConst;
import com.royalpos.waiter.helper.ConnectionDetector;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIServiceN {
    public APIServiceN() {

    }
    private static Context mContext;

    protected static Retrofit retrofit;
    public static <S> S createService(Context context, Class<S> serviceClass) {

        mContext= context;
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.NONE);

         OkHttpClient httpClient = new OkHttpClient.Builder()
                 .addInterceptor(interceptor)
                .connectTimeout(25, TimeUnit.SECONDS)// Set connection timeout
                .readTimeout(25, TimeUnit.SECONDS)// Read timeout
                .writeTimeout(25, TimeUnit.SECONDS)// Write timeout
                .build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        retrofit=new Retrofit.Builder()
                .baseUrl(AppConst.MAIN)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return retrofit.create(serviceClass);
    }

    static Interceptor REWRITE_CACHE_CONTROL_INTERCEPTOR = new Interceptor() {
        @Override
        public Response intercept(Chain chain) throws IOException {

            Request request = chain.request();
            Response response = chain.proceed(request);
            ConnectionDetector connectionDetector=new ConnectionDetector(mContext);

            if (connectionDetector.isConnectingToInternet()) {
                // Get header information
                String cacheControl =request.cacheControl().toString();
                return response.newBuilder()
                        .removeHeader("Pragma")// Clear header information ， Because server if not supported ， Will return some interference information ， Does not clear the following can not be effective
                        .header("Cache-Control", cacheControl)
                        .build();
            }
            return response;
        }
    };
}
