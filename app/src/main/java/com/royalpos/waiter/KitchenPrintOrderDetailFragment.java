package com.royalpos.waiter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.royalpos.waiter.database.DBPrinter;
import com.royalpos.waiter.helper.AppConst;
import com.royalpos.waiter.model.KOPojo;
import com.royalpos.waiter.model.KitchenPrinterPojo;
import com.royalpos.waiter.model.M;
import com.royalpos.waiter.print.KitchenPrintActivity;
import com.royalpos.waiter.print.PrintFormat;
import com.royalpos.waiter.print.newbluetooth.AsyncNewPrintNew;
import com.royalpos.waiter.print.newbluetooth.KitchenGlobalsNew;
import com.royalpos.waiter.print.newbluetooth.KitchenPrintActivityNew;
import com.royalpos.waiter.universalprinter.Globals;
import com.royalpos.waiter.universalprinter.KitchenGlobals;
import com.royalpos.waiter.utils.AidlUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class KitchenPrintOrderDetailFragment extends Fragment implements View.OnClickListener {

    public static final String ARG_ITEM_ID = "orderid";
    String id;
    public static KOPojo kdata;
    LinearLayout llitem;
    TextView tvorderno,tvtime;
    Button btnprint;
    Context context;
    String TAG="KitchenPrintOrderDetailFragment";
    ArrayList<KitchenPrinterPojo> kplist=new ArrayList<>();
    ArrayList<Integer> totlist=new ArrayList<>();
    PrintFormat pf;
    private SharedPreferences sharedPrefs = null;

    public KitchenPrintOrderDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            id =getArguments().getString(ARG_ITEM_ID);

            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle("Detail");
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.kitchenprintorderactivity_detail, container, false);
        context=getActivity();
        pf=new PrintFormat(context);
        tvorderno=(TextView)rootView.findViewById(R.id.tvorderno);
        tvtime=(TextView)rootView.findViewById(R.id.tvtime);
        llitem=(LinearLayout)rootView.findViewById(R.id.llitem);
        btnprint=(Button)rootView.findViewById(R.id.btnprint);
        btnprint.setTypeface(AppConst.font_regular(context));
        if(kplist==null)
            kplist=new ArrayList<>();
        kplist.clear();
        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);  // * load preference
        // manager
        Globals.loadPreferences(sharedPrefs);
        KitchenGlobals.loadPreferences(sharedPrefs);
        AidlUtil.getInstance().connectPrinterService(context);
        if(kdata!=null){
            btnprint.setVisibility(View.VISIBLE);
            tvorderno.setText(kdata.getToken());
            tvtime.setText(kdata.getOrder_time());
            ArrayList<String> catlist=new ArrayList<>();
            ArrayList<String> pidlist=new ArrayList<>();
            try {
                JSONObject jsonObject=new JSONObject(kdata.getData());
                DBPrinter dbp=new DBPrinter(context);
                if (jsonObject.has("item")){
                    JSONArray ja=jsonObject.getJSONArray("item");
                    llitem.removeAllViews();
                    for(int i=0;i<ja.length();i++){
                        JSONObject j=ja.getJSONObject(i);
                        View v = LayoutInflater.from(context).inflate(R.layout.ko_row, null);
                        LinearLayout ll = (LinearLayout) v.findViewById(R.id.ll);
                        ll.setId(i);
                        TextView tvnm = (TextView) v.findViewById(R.id.tvnm);
                        TextView tvqty = (TextView) v.findViewById(R.id.tvqty);
                        TextView tvpre = (TextView) v.findViewById(R.id.tvpre);

                        tvnm.setText(j.getString("name"));
                        tvqty.setText(j.getString("qty"));
                        if(j.has("prenm"))
                            tvpre.setText(j.getString("prenm"));
                        llitem.addView(ll);
                        String cid=j.getString("cusineid");
                        if(!catlist.contains(cid)) {
                            catlist.add(cid);
                            KitchenPrinterPojo kprinter=dbp.getCatPrinters(cid);
                            if(kprinter!=null) {
                                if(!pidlist.contains(kprinter.getId())) {
                                    pidlist.add(kprinter.getId());
                                    kplist.add(kprinter);
                                    totlist.add(1);
                                }else{
                                    int p=pidlist.indexOf(kprinter.getId());
                                    int val=totlist.get(p)+1;
                                    totlist.set(p,val);
                                }
                            }
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else
            btnprint.setVisibility(View.GONE);

        btnprint.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View view) {
        if(view==btnprint){
            if(M.isKitchenCatPrinter(context) && kplist!=null && kplist.size()>0){
                int k=0,kms=0;
                for(final KitchenPrinterPojo kp:kplist){
                    if(kp.getDeviceType()!=null && kp.getDeviceType().trim().length()>0 && !kp.getDeviceType().equals("0")) {
                        new Handler().postDelayed(new Runnable() {
                            public void run() {
                                kitchenreciept(kp.getCategoryid(), kp.getId(),Integer.parseInt(kp.getDeviceType()),kp.isAdv(),kp.getPaper_width());
                            }
                        },kms);
                    }
                    kms=kms+(totlist.get(k)*1000);
                    k++;
                }
            }else if(M.isKitchenPrinter(context) && KitchenGlobals.deviceType!=0)
                kitchenreciept(null,null,KitchenGlobals.deviceType,M.isadvanceprint(M.key_kitchen,context),M.retriveVal(M.key_kitchen_width,context));
        }
    }

    private boolean kitchenreciept(String catid,String pid,int devicetype,boolean isAdv,String wdt) {
        StringBuilder textData = new StringBuilder();
      //  JSONObject jprint=new JSONObject();
        try {
            JSONObject jsonObject=new JSONObject(kdata.getData());
            pf.setPaperSize(wdt);
            Log.d(TAG,"print data:"+jsonObject);
            if(M.isCustomAllow(M.key_top_space,context))
                textData.append("\n\n\n");
            if(devicetype!=5) {
                textData.append("$al_center$$al_center$");
                textData.append("$bigw$" + " $al_center$");
                textData.append(pf.padLine2(context.getResources().getString(R.string.txt_token) + " # " + jsonObject.getString("token"), "") + " $big$\n");
            }else
                textData.append(pf.padLine2(context.getResources().getString(R.string.txt_token) + " # " + jsonObject.getString("token"), "") + "\n");
            textData.append(pf.padLine2(M.getRestName(context),"")+"\n");
       //     jprint.put("restname",M.getRestName(context));
            textData.append(pf.divider()+"\n");
            textData.append(pf.padLine2(getString(R.string.txt_order_by)+" : "+M.getWaitername(context),kdata.getOrder_time())+"\n");
        //    jprint.put("orderby",M.getWaitername(context));
            //jprint.put("time",kdata.getOrder_time());
            if(jsonObject.has("ordertype"))
                textData.append(pf.padLine2("   ",jsonObject.getString("ordertype"))+"\n");
            //jprint.put("token",getString(R.string.txt_token)+" # "+token_number+"\t"+ ""+ordertype+"\t"+payment_mode_text +"\n");
            textData.append(pf.divider()+"\n");
            String customername=null;
            if(jsonObject.has("custnm"))
                customername=jsonObject.getString("custnm");
            if(customername!=null && customername.length()>0) {
                textData.append(pf.padLine2(getString(R.string.customer)+" \t"+customername,""));
             //   jprint.put("custnm",getString(R.string.customer)+" \t"+customername);
                textData.append(pf.divider()+"\n");
            }
            JSONArray ja=jsonObject.getJSONArray("item");
            for (int i = 0; i < ja.length(); i++) {
                JSONObject jd=ja.getJSONObject(i);//new JSONObject();
                Boolean isprint=false;
                if(catid!=null && catid.trim().length()>0 && jd.has("cusineid")){
                    List<String> items = Arrays.asList(catid.split("\\s*,\\s*"));
                    if(items.contains(jd.getString("cusineid")))
                        isprint=true;
                    else
                        isprint=false;
                }else{
                    isprint=true;
                }
                if(isprint) {
                    String qty = jd.getString("qty");
                    String name = jd.getString("name");
                    String we="";
                    if(jd.has("weight"))
                        we=jd.getString("weight");

                    if(devicetype<5)
                        textData.append("$bold$"+pf.padLine2(qty+" X "+name+" "+we, "  ") + "$unbold$\n");
                    else
                        textData.append(pf.padLine2(qty+" X "+name+" "+we, "  ") + "\n");

                    if (jd.has("prenm")) {
                        String prefname = jd.getString("prenm");
                        if(prefname!=null && prefname.length()>0)
                            textData.append(pf.padLine2(  "    " + prefname, " ") + "\n");

                    }
                    if (jd.has("dish_comment")) {
                        if (jd.getString("dish_comment").length() > 0) {
                            String dish_comment = jd.getString("dish_comment");
                            textData.append(pf.padLine2("    " +dish_comment, " ") + "\n");
                        }
                    }

                    if (jd.has("combo")) {
                        if (jd.getString("combo").length() > 0) {
                            String combo = jd.getString("combo");
                            textData.append(pf.padLine2(combo,  "") + "\n");
                        }
                    }
                }
            }
            textData.append(pf.divider()+"\n");
            if(devicetype!=5) {
//            DBKP dbkp=new DBKP(context);
//            dbkp.addData(order_no,tm,jprint+"");
                Log.d(TAG, "Kitchen Data:" + pid);
                Log.d(TAG, textData + "");
                Intent sendIntent;
                AsyncNewPrintNew.isKitchenAdv=isAdv;
                if(isAdv){
                    KitchenGlobalsNew.setJsonObject(jsonObject);
                    sendIntent = new Intent(context, KitchenPrintActivityNew.class);
                    sendIntent.putExtra(Intent.EXTRA_TEXT,jsonObject+"");
                }else {
                    textData.append("$intro$$intro$$intro$$intro$$cutt$$intro$");
                    sendIntent = new Intent(context, KitchenPrintActivity.class);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, textData.toString());
                }
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, textData.toString());
                sendIntent.putExtra("printerid", pid);
                sendIntent.putExtra("internal", "1");
                sendIntent.setType("text/plain");
                this.startActivity(sendIntent);
            }else{
                AidlUtil.getInstance().printText(textData.toString(), 24, true, false);
            }

        }catch (JSONException e){
            Log.d(TAG,"error:"+e.getMessage());
        }
        catch (Exception e) {
            Log.d(TAG, e.getMessage());
        }

        textData = null;

        return true;
    }
}

