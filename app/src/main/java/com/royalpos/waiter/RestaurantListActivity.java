package com.royalpos.waiter;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.royalpos.waiter.adapter.RestaurantAdapter;
import com.royalpos.waiter.helper.ConnectionDetector;
import com.royalpos.waiter.helper.MemoryCache;
import com.royalpos.waiter.model.M;
import com.royalpos.waiter.model.RestaurantPojo;
import com.royalpos.waiter.webservices.APIServiceHeader;
import com.royalpos.waiter.webservices.AuthenticationAPI;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class RestaurantListActivity extends AppCompatActivity {

    Context context;
    RecyclerView rv;
    public LinearLayoutManager layoutManager;
    String TAG="RestaurantListActivity";
    RestaurantAdapter adapter;
    ConnectionDetector connectionDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getResources().getBoolean(R.bool.portrait_only)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }else
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        setContentView(R.layout.activity_restaurant_list);
        MemoryCache memoryCache = new MemoryCache();
        requestForSpecificPermission();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        context=RestaurantListActivity.this;
        connectionDetector=new ConnectionDetector(context);
        rv=(RecyclerView)findViewById(R.id.rvrest);
        layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv.setLayoutManager(layoutManager);

        if(M.getBrandId(context)!=null && M.getBrandId(context).trim().length()>0)
            getList();
        else
            finish();
    }
    private void requestForSpecificPermission() {
        ActivityCompat.requestPermissions(this, new String[]{android. Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 101);
    }
    private void getList() {
        if(connectionDetector.isConnectingToInternet()) {
            M.showLoadingDialog(context);
            AuthenticationAPI mAuthenticationAPI = APIServiceHeader.createService(context, AuthenticationAPI.class);
            Call<List<RestaurantPojo>> call = mAuthenticationAPI.getRestList();
            call.enqueue(new retrofit2.Callback<List<RestaurantPojo>>() {
                @Override
                public void onResponse(Call<List<RestaurantPojo>> call, Response<List<RestaurantPojo>> response) {
                    M.hideLoadingDialog();
                    if (response.isSuccessful()) {
                        List<RestaurantPojo> pojo = response.body();
                        if (pojo != null) {
                            if(pojo.size()>0){
                                adapter=new RestaurantAdapter(pojo,context);
                                rv.setAdapter(adapter);
                            }
                        }

                    } else {
                        int statusCode = response.code();
                        ResponseBody errorBody = response.errorBody();
                        Log.d(TAG, "error:" + statusCode + " " + errorBody);
                    }
                    M.hideLoadingDialog();
                }

                @Override
                public void onFailure(Call<List<RestaurantPojo>> call, Throwable t) {
                    Log.d(TAG, "fail:" + t.getMessage());
                    M.hideLoadingDialog();
                }
            });

        }else
        {
            M.showToast(context, getString(R.string.no_internet_error));
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
