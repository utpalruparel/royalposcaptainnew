package com.royalpos.waiter;

import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.royalpos.waiter.helper.AppConst;
import com.royalpos.waiter.helper.SelectLanguage;

import java.util.ArrayList;

public class ChooseLangActivity extends AppCompatActivity {

    ListView lv;
    Button btncontinue;
    String TAG="ChooseLangActivity";
    Context context;
    String lang="";
    ArrayList<String> langlist;
    ArrayList<String> sortlist;
    Boolean isFirsttime=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_lang);

        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        context=ChooseLangActivity.this;

        lv=(ListView)findViewById(R.id.lvlang);
        btncontinue=(Button)findViewById(R.id.btnchoose);
        btncontinue.setTypeface(AppConst.font_regular(context));

        setLang();

        btncontinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Boolean ischange=true;
                if(SelectLanguage.getLang(context)!=null && SelectLanguage.getLang(context).trim().length()>0){
                    if(SelectLanguage.getLang(context).equals(lang))
                        ischange=false;
                }
                SelectLanguage.saveLocale(lang,sortlist.get(langlist.indexOf(lang)),context);
                SelectLanguage.changeLang(sortlist.get(langlist.indexOf(lang)), context);

                if(ischange || isFirsttime) {
                    Intent it = new Intent(context, SplashScreen.class);
                    it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    finish();
                    startActivity(it);
                }else
                    finish();
            }
        });
    }

    private void setLang(){

        if(langlist==null)
            langlist=new ArrayList<>();
        if(sortlist==null)
            sortlist=new ArrayList<>();
        langlist.clear();
        sortlist.clear();

        langlist.add("English");
        sortlist.add("en");

        langlist.add("Hindi");
        sortlist.add("hi");

        langlist.add("Arabic");
        sortlist.add("ar");

        langlist.add("Spanish");
        sortlist.add("es");

        if(SelectLanguage.getLocale(context)!=null && SelectLanguage.getLocale(context).trim().length()>0){
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
            lang=SelectLanguage.getLang(context);
        }else{
            isFirsttime=true;
            lang=langlist.get(0);
            SelectLanguage.saveLocale(lang,sortlist.get(0),context);
            SelectLanguage.changeLang(sortlist.get(langlist.indexOf(lang)), context);
        }

        CustomListAdapter adapter=new CustomListAdapter(context);
        lv.setAdapter(adapter);
    }

    public class CustomListAdapter extends BaseAdapter {
        private Context context;

        public CustomListAdapter(Context context) {
            this.context = context;
        }

        @Override
        public int getCount() {
            return langlist.size();
        }

        @Override
        public Object getItem(int position) {
            return langlist.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(context).
                        inflate(R.layout.lang_row, parent, false);
            }
            final String currentItem = langlist.get(position);
            final ImageView iv=(ImageView)convertView.findViewById(R.id.ivcheck);
            final TextView textViewItemName = (TextView)convertView.findViewById(R.id.tvlang);
            LinearLayout ll=(LinearLayout)convertView.findViewById(R.id.ll);
            textViewItemName.setText(currentItem);
            if(currentItem.equals(lang)){
                textViewItemName.setTextColor(context.getResources().getColor(R.color.blue1));
                iv.setVisibility(View.VISIBLE);
            }else{
                textViewItemName.setTextColor(context.getResources().getColor(R.color.primary_text));
                iv.setVisibility(View.GONE);
            }

            ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    lang=currentItem;
                    notifyDataSetChanged();
                }
            });

            return convertView;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if(isFirsttime)
            btncontinue.performClick();
        else
            super.onBackPressed();
    }
}
