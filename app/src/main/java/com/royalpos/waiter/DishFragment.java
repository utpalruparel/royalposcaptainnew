package com.royalpos.waiter;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.royalpos.waiter.adapter.TableDishesListAdapter;
import com.royalpos.waiter.database.DBDishes;
import com.royalpos.waiter.helper.ConnectionDetector;
import com.royalpos.waiter.helper.MemoryCache;
import com.royalpos.waiter.model.DishPojo;
import com.royalpos.waiter.model.M;
import com.royalpos.waiter.webservices.APIServiceHeader;
import com.royalpos.waiter.webservices.CuisineDishAPI;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;

public class DishFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "istable";
    private String cuisineid,TAG="DishFragment";
    View view;
    RecyclerView mRecyclerView1;
    Context context;
    ConnectionDetector connectionDetector;
    String runiqueid,restaurantid,bus_type="";
    Boolean istable;
    int col;

    public List<DishPojo> custdisheslist = new ArrayList<DishPojo>();

    public DishFragment() {
        // Required empty public constructor
    }

    public static DishFragment newInstance(String param1,Boolean istable) {
        DishFragment fragment = new DishFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putBoolean(ARG_PARAM2,istable);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            cuisineid = getArguments().getString(ARG_PARAM1);
            istable = getArguments().getBoolean(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view= inflater.inflate(R.layout.fragment_dish, container, false);
        MemoryCache memoryCache = new MemoryCache();
        context=getActivity();
        connectionDetector=new ConnectionDetector(context);
        restaurantid= M.getRestID(context);
        runiqueid= M.getRestUniqueID(context);
        bus_type=M.getType(context);
        mRecyclerView1 = (RecyclerView)view. findViewById(R.id.rvdishes);
        if(getResources().getBoolean(R.bool.portrait_only)) {
            mRecyclerView1.setPadding(1, 1, 1, 1);
            col=3;
        }else {
            mRecyclerView1.setPadding(8, 8, 8, 8);
            col=4;
        }
        if(M.getListType(context).equals("list")){
            mRecyclerView1.setLayoutManager(new LinearLayoutManager(context));
        }else if(M.getListType(context).equals("grid")){
            mRecyclerView1.setLayoutManager(new GridLayoutManager(context,col));
        }else if(M.getListType(context).equals("gridimg")){
            mRecyclerView1.setLayoutManager(new GridLayoutManager(context,col));
        }

        mRecyclerView1.setHasFixedSize(true);

        getDishes();

        return view;
    }

    //get cust dishes
    private void getDishes() {
        DBDishes dbDishes=new DBDishes(context);
        List<DishPojo> dishlist=dbDishes.getDishes(cuisineid,context);
        if(dishlist!=null && dishlist.size()>0){
            if(custdisheslist!=null)
                custdisheslist.clear();
            if (dishlist != null) {
                custdisheslist = dishlist;
                setAdapterData(custdisheslist);
                mRecyclerView1.scrollToPosition(0);
            }
        }else if(connectionDetector.isConnectingToInternet()){
            CuisineDishAPI mAuthenticationAPI = APIServiceHeader.createService(context, CuisineDishAPI.class);
            Call<List<DishPojo>> call = mAuthenticationAPI.getDishCuisine(restaurantid, runiqueid, cuisineid);
            call.enqueue(new retrofit2.Callback<List<DishPojo>>() {
                @Override
                public void onResponse(Call<List<DishPojo>> call, retrofit2.Response<List<DishPojo>> response) {
                    if (response.isSuccessful()) {
                        List<DishPojo> custDishesPojos = response.body();
                        if(custdisheslist!=null)
                            custdisheslist.clear();
                        if (custDishesPojos != null) {
                            custdisheslist = custDishesPojos;
                            setAdapterData(custdisheslist);
                        }
                    } else {
                        int statusCode = response.code();
                        ResponseBody errorBody = response.errorBody();
                        Log.d(TAG, "error:" + statusCode + " " + errorBody);
                    }
                }

                @Override
                public void onFailure(Call<List<DishPojo>> call, Throwable t) {
                    Log.d(TAG, "fail:" + t.getMessage());
                }
            });
        }
    }

    private void setAdapterData(List<DishPojo> dishlist){
        TableDishesListAdapter ada = new TableDishesListAdapter(context, dishlist, false);
        mRecyclerView1.setAdapter(ada);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
       if(item.getItemId()==R.id.item_list){
            mRecyclerView1.setLayoutManager(new LinearLayoutManager(context));
            M.setlistType("list", context);
            getDishes();
        }else if(item.getItemId()==R.id.item_grid){
            mRecyclerView1.setLayoutManager(new GridLayoutManager(context,col));
            M.setlistType("grid", context);
            getDishes();
        }else if(item.getItemId()==R.id.item_grid_img){
            mRecyclerView1.setLayoutManager(new GridLayoutManager(context,col));
            M.setlistType("gridimg", context);
            getDishes();
        }
//        else if(item.getItemId()==R.id.item_search){
//            Intent it = new Intent(context, SearchActivity.class);
//            it.putExtra("table",istable);
//            startActivity(it);
//        }
        return super.onOptionsItemSelected(item);
    }

}
