package com.royalpos.waiter;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.royalpos.waiter.helper.AppConst;
import com.royalpos.waiter.helper.ConnectionDetector;
import com.royalpos.waiter.model.AccountPojo;
import com.royalpos.waiter.model.M;
import com.royalpos.waiter.webservices.APIServiceHeader;
import com.royalpos.waiter.webservices.AuthenticationAPI;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.ResponseBody;
import retrofit2.Call;

public class MyAccount extends AppCompatActivity{

    TextView tv;
    Context context;
    ConnectionDetector connectionDetector;
    String TAG="MyAccount";
    SimpleDateFormat defaultfmt=new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat dtfmt=new SimpleDateFormat("dd MMM yyyy");
    String action=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);
        if(getResources().getBoolean(R.bool.portrait_only)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        }
        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);

        context=MyAccount.this;
        connectionDetector=new ConnectionDetector(context);
        tv=(TextView)findViewById(R.id.tv);

        if(getIntent().getAction()!=null)
            action=getIntent().getAction();

        getData();
    }

    private void getData() {
        if(connectionDetector.isConnectingToInternet()) {
            M.showLoadingDialog(context);
                AuthenticationAPI mAuthenticationAPI = APIServiceHeader.createService(context, AuthenticationAPI.class);
                Call<AccountPojo> call = mAuthenticationAPI.getACStatus(M.getRestID(context));
                call.enqueue(new retrofit2.Callback<AccountPojo>() {
                    @Override
                    public void onResponse(Call<AccountPojo> call, retrofit2.Response<AccountPojo> response) {
                        M.hideLoadingDialog();
                        if (response.isSuccessful()) {
                            AccountPojo pojo=response.body();
                            String txt="";
                            if(pojo.getSuccess().equals("1")){
                                txt=pojo.getStatus();
                                M.setExpiry(pojo.getMembership_end_date(),context);
                                if(pojo.getStatus().equals("expired")){
                                    try {
                                        Date dt=defaultfmt.parse(pojo.getMembership_end_date());
                                        txt=getString(R.string.txt_membership_expire)+"\n"+dtfmt.format(dt);
                                        tv.setText(txt);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                }else if(pojo.getStatus().equals("purchased")){
                                    try {
                                        Date dt=defaultfmt.parse(pojo.getMembership_end_date());
                                        txt=getString(R.string.Membership_Expires_on)+" \n"+dtfmt.format(dt);
                                        tv.setText(txt);
                                        checkMembership();
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    return;
                                }else if(pojo.getStatus().equals("free")){
                                    try {
                                        Date dt=defaultfmt.parse(pojo.getMembership_end_date());
                                        txt=getString(R.string.free_trial)+"\n"+getString(R.string.expires_on)+"\n"+dtfmt.format(dt);
                                        tv.setText(txt);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        } else {
                            int statusCode = response.code();
                            ResponseBody errorBody = response.errorBody();
                            Log.d(TAG, "error:" + statusCode + " " + errorBody);
                        }
                    }

                    @Override
                    public void onFailure(Call<AccountPojo> call, Throwable t) {
                        M.hideLoadingDialog();
                        Log.d("response:", "fail:" + t.getMessage());
                        checkMembership();
                    }
                });
        }else{
            M.showToast(context,getString(R.string.no_internet_error));
            checkMembership();
        }
    }

    private void checkMembership(){
        String dt=M.getExpiry(context);
        if(dt!=null && dt.trim().length()>0) {
            try {
                Date d = new SimpleDateFormat("yyyy-MM-dd").parse(dt);
                if (new Date().before(d)) {
                    if(action!=null && !action.equals("showAccount"))
                        redirectToMain();
                }else{
                    String txt=getString(R.string.txt_membership_expire)+" \n"+dtfmt.format(d);
                    tv.setText(txt);
                }
            } catch (ParseException e) {
                Log.d(TAG,"date error"+e.getMessage());
                e.printStackTrace();
            }
        }else{

        }
    }

    private void redirectToMain(){

        if(M.iswaiterloggedin(context)) {
            Intent it = new Intent(context, WaiterActivity.class);
            finish();
            startActivity(it);
            overridePendingTransition(0, 0);
        }else if(M.getDeviceCode(context)!=null){
            M.waiterlogOut(context);
            Intent mIntent = new Intent(context, LoginActivity.class);
            finish();
            startActivity(mIntent);
        }else{
            AppConst.currency = M.getCurrency(context);
            Intent mIntent = new Intent(context, WaiterListActivity.class);
            finish();
            startActivity(mIntent);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}
