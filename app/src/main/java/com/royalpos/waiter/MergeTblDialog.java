package com.royalpos.waiter;

import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.royalpos.waiter.helper.AppConst;
import com.royalpos.waiter.model.TablePojo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

public class MergeTblDialog extends Dialog {

    Context context;
    String TAG="MergeTblDialog";
    ArrayList<String> selList=new ArrayList<>();
    JSONArray tlist;

    public MergeTblDialog(@NonNull final Context context, final JSONObject tablePojo, JSONArray tlist) {
        super(context);
        this.context=context;
        this.tlist=tlist;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_merge_table);
        getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        TextView tvtitle = (TextView) findViewById(R.id.tvtitle);
        ListView lv = (ListView) findViewById(R.id.lvtable);
        Button btn = (Button) findViewById(R.id.btnsubmit);
        btn.setTypeface(AppConst.font_regular(context));
        ImageView ivclose = (ImageView) findViewById(R.id.ivclose);
        try {
            tvtitle.setText("Merge Table\n" + tablePojo.getString("table_name"));

            CustomListAdapter tada = new CustomListAdapter(context, tlist);
            lv.setAdapter(tada);

            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (selList.size() > 0) {
                        String t = TextUtils.join(",", selList);
//                        DBTable dbTable = new DBTable(context);
//                        String oids = dbTable.getOrderTbl(t);
//                        DBOfflineOrder dbOfflineOrder = new DBOfflineOrder(context);
//                        dbOfflineOrder.addMerged(t, tablePojo.getTable_id() + "", tablePojo.getOrder_id(), oids);
                        EventBus.getDefault().post("addMerge"+t);
                        dismiss();
                    } else
                        Toast.makeText(context, "Please select table for merge", Toast.LENGTH_SHORT).show();
                }
            });
        }catch (JSONException e){

        }
        ivclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    public class CustomListAdapter extends BaseAdapter {
        private Context context;
        JSONArray list;

        public CustomListAdapter(Context context,JSONArray list) {
            this.context = context;
            this.list=list;
        }

        @Override
        public int getCount() {
            return list.length();
        }

        @Override
        public Object getItem(int position) {
            try {
                return list.getJSONObject(position);
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(context).
                        inflate(R.layout.lang_row, parent, false);
            }
            try {
                JSONObject jTbl=list.getJSONObject(position);
            final String currentItem = jTbl.getString("name")+" ("+jTbl.getString("status").toUpperCase()+")";
            final String currentItemid = jTbl.getString("id");
            final ImageView iv=(ImageView)convertView.findViewById(R.id.ivcheck);
            final TextView textViewItemName = (TextView)convertView.findViewById(R.id.tvlang);
            LinearLayout ll=(LinearLayout)convertView.findViewById(R.id.ll);
            textViewItemName.setText(currentItem);
            if(selList.contains(currentItemid)){
                textViewItemName.setTextColor(context.getResources().getColor(R.color.blue1));
                iv.setVisibility(View.VISIBLE);
            }else{
                textViewItemName.setTextColor(context.getResources().getColor(R.color.primary_text));
                iv.setVisibility(View.GONE);
            }

            ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(selList.contains(currentItemid)){
                        selList.remove(currentItemid);
                        notifyDataSetChanged();
                    }else {
                        selList.add(currentItemid);
                        notifyDataSetChanged();
                    }
                }
            });
            }catch (JSONException e){}
            return convertView;
        }
    }
}
