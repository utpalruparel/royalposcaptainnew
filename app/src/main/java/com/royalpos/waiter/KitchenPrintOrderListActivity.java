package com.royalpos.waiter;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.royalpos.waiter.database.DBKP;
import com.royalpos.waiter.model.KOPojo;

import java.util.ArrayList;
import java.util.List;

public class KitchenPrintOrderListActivity extends AppCompatActivity {

    private boolean mTwoPane;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kitchenprintorderactivity_list);
        if(getResources().getBoolean(R.bool.portrait_only)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }else
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
        context=KitchenPrintOrderListActivity.this;
        if (findViewById(R.id.kitchenprintorderactivity_detail_container) != null) {
            mTwoPane = true;
        }

        RecyclerView recyclerView =(RecyclerView)findViewById(R.id.kitchenprintorderactivity_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setHasFixedSize(true);

        DBKP dbkp=new DBKP(context);
        ArrayList<KOPojo> klist=dbkp.getList();
        OrderListAdapter ada=new OrderListAdapter(klist,context);
        recyclerView.setAdapter(ada);

    }

    public class OrderListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private static final int TYPE_ITEM = 1;
        private List<KOPojo> list;
        Context context;
        String TAG="OrderListAdapter";

        public OrderListAdapter(List<KOPojo> list, Context mcontext) {

            context = mcontext;
            this.list = list;
        }

        @Override
        public int getItemViewType(int position) {
            return TYPE_ITEM;
        }


        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_list_row, parent, false);
            return new ViewHolderPosts(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

            final KOPojo model=list.get(position);
            final ViewHolderPosts itemview = (ViewHolderPosts) holder;

            itemview.tvorder.setText(model.getToken());
            itemview.tvtime.setText(model.getOrder_time());
            itemview.ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    KitchenPrintOrderDetailFragment.kdata=model;
                    if (mTwoPane) {
                        Bundle arguments = new Bundle();
                        arguments.putString("orderid", model.getId()+"");
                        KitchenPrintOrderDetailFragment fragment = new KitchenPrintOrderDetailFragment();
                        fragment.setArguments(arguments);
                        ((KitchenPrintOrderListActivity)context).getSupportFragmentManager().beginTransaction()
                                .replace(R.id.kitchenprintorderactivity_detail_container, fragment)
                                .commit();
                    } else {
                        Intent intent = new Intent(context, KitchenPrintOrderDetailActivity.class);
                        intent.putExtra("orderid",model.getId()+"");
                        context.startActivity(intent);
                    }
                }
            });

        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public class ViewHolderPosts extends RecyclerView.ViewHolder {
            TextView tvorder,tvtime,tvamt;
            LinearLayout ll;
            View dv;

            public ViewHolderPosts(View itemView) {
                super(itemView);
                tvorder = (TextView) itemView.findViewById(R.id.tvorderid);
                tvamt = (TextView) itemView.findViewById(R.id.tvamount);
                tvamt.setVisibility(View.GONE);
                tvtime=(TextView)itemView.findViewById(R.id.tvtime);
                ll=(LinearLayout)itemView.findViewById(R.id.ll);
                dv=(View)itemView.findViewById(R.id.dv);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }
}
