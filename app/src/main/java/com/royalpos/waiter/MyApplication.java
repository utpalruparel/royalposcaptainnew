package com.royalpos.waiter;

import android.app.Application;
import android.content.Context;
import androidx.multidex.MultiDex;

import com.royalpos.waiter.utils.AidlUtil;

import java.util.Locale;

public class MyApplication extends Application {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    private boolean isAidl;
    public boolean isAidl() {
        return isAidl;
    }

    public void setAidl(boolean aidl) {
        isAidl = aidl;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Locale.setDefault(new Locale("en", "US"));
       // Fabric.with(this, new Crashlytics());
       // FirebaseCrash.report(new Exception());

        isAidl = true;
        AidlUtil.getInstance().connectPrinterService(this);
    }
}
