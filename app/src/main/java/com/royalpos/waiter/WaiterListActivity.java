package com.royalpos.waiter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.royalpos.waiter.adapter.WaiterAdapter;
import com.royalpos.waiter.database.DBWaiter;
import com.royalpos.waiter.helper.AppConst;
import com.royalpos.waiter.helper.ConnectionDetector;
import com.royalpos.waiter.model.M;
import com.royalpos.waiter.model.Waiter;
import com.royalpos.waiter.webservices.APIServiceHeader;
import com.royalpos.waiter.webservices.WaiterAPI;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class WaiterListActivity extends AppCompatActivity {

    RecyclerView rv;
    Context context;
    ConnectionDetector connectionDetector;
    String TAG="WaiterListActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waiter_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        context=WaiterListActivity.this;
        connectionDetector=new ConnectionDetector(context);

        rv=(RecyclerView)findViewById(R.id.rvwaiter);
        rv.setLayoutManager(new LinearLayoutManager(context));
        rv.setHasFixedSize(true);

        getWaiters();
    }

    private void getWaiters() {
        final DBWaiter dbWaiter=new DBWaiter(context);
        if(dbWaiter.getCounts()==0) {
            if (connectionDetector.isConnectingToInternet()) {
                M.showLoadingDialog(context);
                String restaurantid = M.getRestID(context);
                String runiqueid = M.getRestUniqueID(context);
                WaiterAPI mAuthenticationAPI = APIServiceHeader.createService(context, WaiterAPI.class);
                Call<List<Waiter>> call = mAuthenticationAPI.getWaiters(restaurantid, runiqueid);
                call.enqueue(new retrofit2.Callback<List<Waiter>>() {
                    @Override
                    public void onResponse(Call<List<Waiter>> call, Response<List<Waiter>> response) {
                        M.hideLoadingDialog();
                        if (response.isSuccessful()) {
                            List<Waiter> pojo = response.body();
                            if (pojo != null) {
                                if (pojo.size() > 0) {
                                    dbWaiter.deleteall();
                                    for(Waiter w:pojo)
                                        dbWaiter.addwaiting(w);
                                    setWaiterList();
                                } else
                                    Toast.makeText(context, R.string.empty_userlist, Toast.LENGTH_SHORT).show();
                            } else
                                Toast.makeText(context, R.string.empty_userlist, Toast.LENGTH_SHORT).show();
                        } else {
                            int statusCode = response.code();
                            ResponseBody errorBody = response.errorBody();
                            Log.d(TAG, "error:" + statusCode + " " + errorBody);
                            if(AppConst.waiters!=null && AppConst.waiters.size()>0){
                                for(Waiter w:AppConst.waiters)
                                    dbWaiter.addwaiting(w);
                                setWaiterList();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Waiter>> call, Throwable t) {
                        Log.d(TAG, "fail:" + t.getMessage());
                        M.hideLoadingDialog();
                        if(AppConst.waiters!=null && AppConst.waiters.size()>0){
                            for(Waiter w:AppConst.waiters)
                                dbWaiter.addwaiting(w);
                            setWaiterList();
                        }
                    }
                });
            }else if(AppConst.waiters!=null && AppConst.waiters.size()>0){
                for(Waiter w:AppConst.waiters)
                    dbWaiter.addwaiting(w);
                setWaiterList();
            }else
                Toast.makeText(context,R.string.no_internet_error,Toast.LENGTH_SHORT).show();
        }else{
            setWaiterList();
        }
    }

    private void setWaiterList(){
        ArrayList<Waiter> wu = new ArrayList<>();
        wu.clear();
        DBWaiter dbWaiter=new DBWaiter(context);
        wu= (ArrayList<Waiter>) dbWaiter.getWaiterList("no");
        WaiterAdapter adapter = new WaiterAdapter(wu, context);
        rv.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.logout){
            M.logOut(context);
            Intent it=new Intent(context,LoginActivity.class);
            finish();
            startActivity(it);
            overridePendingTransition(0,0);
        }
        return super.onOptionsItemSelected(item);
    }
}
