package com.royalpos.waiter.helper;

public class WifiHelper {

    public static String KEY_CONNECTION="connection";
    public static String KEY_EXIT_SERVER="exitServer";
    public static String TXT_SUCCESS="1",TXT_FAIL="0";

    //APIS Request
    public static String api_inactive_table="getInactiveTable";
    public static String api_active_table="getActiveTable";
    public static String api_start_order="startNewOrder";
    public static String api_get_order_detail="getOrderDetail";
    public static String api_update_order="updateOrder";
    public static String api_cancel_order_item="cancelItem";
    public static String api_cancel_order="cancelOrder";
    public static String api_update_discount_offer="updateDiscountOffer";
    public static String api_finish_order="finishOrder";
    public static String api_change_table="changeTable";
    public static String api_custom_table="getCustomTable";
    public static String api_split_order="splitOrder";
    public static String api_merge_table="getMergeTbl";
    public static String api_add_merge="addmergeTbl";
    public static String api_update_item_status="updateItemStatus";
    public static String api_update_payment_trans="updatePaymentTrans";
    //API Response
    public static String api_inactive_table_response="getInactiveTableResult";
    public static String api_start_order_response="startNewOrderResult";
    public static String api_active_table_response="getActiveTableResult";
    public static String api_get_order_detail_response="getOrderDetailResult";
    public static String api_update_order_response="updateOrderResult";
    public static String api_cancel_order_item_response="cancelItemResult";
    public static String api_cancel_order_response="cancelOrderResult";
    public static String api_update_discount_offer_response="updateDiscountOfferResult";
    public static String api_finish_order_response="finishOrderResult";
    public static String api_change_table_response="changeTableResult";
    public static String api_custom_table_response="getCustomTableResult";
    public static String api_split_order_response="splitOrderResult";
    public static String api_merge_table_response="getMergeTblResult";
    public static String api_add_merge_response="addmergeTblResult";
    public static String api_update_item_status_response="updateItemStatusResult";
    public static String api_update_payment_trans_response="updatePaymentTransResult";
}
