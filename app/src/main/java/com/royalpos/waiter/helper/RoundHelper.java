package com.royalpos.waiter.helper;

import android.content.Context;
import android.util.Log;

import com.royalpos.waiter.model.M;
import com.royalpos.waiter.print.PrintFormat;

import org.json.JSONException;
import org.json.JSONObject;

public class RoundHelper {

    double val_interval=0; // 1, 0.50,0.05
    int rule_tag=0; // 1,2,3,4
    Context context;
    //1- Round Up , 2- Round Down , 3- Always up, 4- Always Down
    public static String rounding_id="0",interval_val="";
    String TAG="RoundHelper";

    public RoundHelper(Context context) {
        this.context=context;
        if(M.getRoundFormat(context)) {
            String roundInfo = M.getRoundBoundary(context);
            if(roundInfo!=null && roundInfo.trim().length()>0){
                try {
                    JSONObject jsonObject=new JSONObject(roundInfo);
                    String rtag=jsonObject.getString("rule_tag");
                    String rval=jsonObject.getString("val_interval");
                    if(rtag!=null && rtag.trim().length()>0)
                        this.rule_tag=Integer.parseInt(rtag);
                    if(rval!=null && rval.trim().length()>0)
                        this.val_interval=Double.parseDouble(rval);
                    rounding_id=jsonObject.getString("rounding_id");
                    interval_val=rval;
                }catch (JSONException e){}
            }
        }
    }

    public String applyRoundFormat(double bill_amt){
        double result=bill_amt;
        String txtbillamt=String.valueOf(bill_amt);
        String onlyint=txtbillamt.substring(0,txtbillamt.indexOf("."));
        double onlydecimal=Double.parseDouble("0"+txtbillamt.substring(txtbillamt.indexOf(".")));
        if(onlydecimal==0)
            result=bill_amt;
        else if(M.getDecimalVal(context).equals("3"))
            result=bill_amt;
        else if(val_interval==1){
            if(rule_tag==1)
                result=Math.round(bill_amt);
            else if(rule_tag==3)
                result=Math.ceil(bill_amt);
            else if(rule_tag==4)
                result=Math.floor(bill_amt);
            else if(rule_tag==2){
                if(onlydecimal<=0.50)
                    result=Math.floor(bill_amt);
                else
                    result=Math.ceil(bill_amt);
            }
        }else if(val_interval==0.50){
            if(rule_tag==4){
                if(onlydecimal>0 && onlydecimal<0.50)
                    result=Double.parseDouble(onlyint);
                else
                    result=Double.parseDouble(onlyint)+0.50;
            }else if(rule_tag==3){
                if(onlydecimal>0 && onlydecimal<=0.50)
                    result=Double.parseDouble(onlyint)+0.50;
                else
                    result=Double.parseDouble(onlyint)+1;
            }else if(rule_tag==2){
                if(onlydecimal>0 && onlydecimal<=0.25)
                    result=Double.parseDouble(onlyint);
                else if(onlydecimal>=0.26 && onlydecimal<=0.75)
                    result=Double.parseDouble(onlyint)+0.50;
                else
                    result=Double.parseDouble(onlyint)+1;
            }else if(rule_tag==1){
                if(onlydecimal>0 && onlydecimal<=0.24)
                    result=Double.parseDouble(onlyint);
                else if(onlydecimal>=0.25 && onlydecimal<=0.74)
                    result=Double.parseDouble(onlyint)+0.50;
                else
                    result=Double.parseDouble(onlyint)+1;
            }
        }else if(val_interval==0.05){
            String tofirstDecimal=txtbillamt.substring(0, txtbillamt.indexOf(".")+2);
            Double fd=Double.parseDouble(tofirstDecimal);
            Double lastDecimal=Double.parseDouble(txtbillamt.replace(tofirstDecimal,"0.0"));
            if(lastDecimal==0)
                result=fd;
            else if(lastDecimal==0.05)
                result=bill_amt;
            else if(rule_tag==1 || rule_tag==2){
                if(lastDecimal>0 && lastDecimal<0.03)
                    result=fd;
                else if(lastDecimal>=0.03 && lastDecimal<=0.07)
                    result=fd+0.05;
                else if(lastDecimal>=0.08 && lastDecimal<=0.10)
                    result=fd+0.10;
            }else if(rule_tag==3){
                if(lastDecimal>0 && lastDecimal<=0.05)
                    result=fd+0.05;
                else
                    result=fd+0.10;
            }else if(rule_tag==4){
                if(lastDecimal>0 && lastDecimal<0.05)
                    result=fd;
                else
                    result=fd+0.05;
            }
        }
        PrintFormat pf=new PrintFormat(context);
        return pf.setFormat(result+"");
    }


    public String extractRoundJSON(String roundJSON){

        String cashrounding=null;
        if (roundJSON != null && roundJSON.length() != 0) {
            try {
                JSONObject rJson = new JSONObject(roundJSON);
                String rval = rJson.getString("cash_rounding");
                if (rval != null && rval.length() != 0) {
                    PrintFormat pf = new PrintFormat(context);
                    cashrounding = pf.setFormat(rval);
                    if (Double.parseDouble(cashrounding) == 0) {
                        cashrounding = null;
                    }

                } else
                    cashrounding = null;
            } catch (JSONException e) {
                Log.d("OtherPrintReceipt", "extract data error:" + e.getMessage());
            }
        }
        return cashrounding;
    }


    //Demo text

    //        double val=100,val1=100.5,val2=100.22,val3=100.62;
//        Log.d(TAG,"-------------------------val : "+val+"-------------------------------");
//        RoundHelper roundHelper=new RoundHelper(context,val,1,1);
//        Log.d(TAG,"round up: "+roundHelper.applyRoundFormat());
//
//        roundHelper=new RoundHelper(context,val,2,1);
//        Log.d(TAG,"round down: "+roundHelper.applyRoundFormat());
//
//        roundHelper=new RoundHelper(context,val,3,1);
//        Log.d(TAG,"up: "+roundHelper.applyRoundFormat());
//
//        roundHelper=new RoundHelper(context,val,4,1);
//        Log.d(TAG,"down: "+roundHelper.applyRoundFormat());
//
//        Log.d(TAG,"-------------------------val : "+val1+"-------------------------------");
//
//        roundHelper=new RoundHelper(context,val1,1,1);
//        Log.d(TAG,"round up: "+roundHelper.applyRoundFormat());
//
//        roundHelper=new RoundHelper(context,val1,2,1);
//        Log.d(TAG,"round down: "+roundHelper.applyRoundFormat());
//
//        roundHelper=new RoundHelper(context,val1,3,1);
//        Log.d(TAG,"up: "+roundHelper.applyRoundFormat());
//
//        roundHelper=new RoundHelper(context,val1,4,1);
//        Log.d(TAG,"down: "+roundHelper.applyRoundFormat());
//
//        Log.d(TAG,"-------------------------val : "+val2+"-------------------------------");
//
//        roundHelper=new RoundHelper(context,val2,1,1);
//        Log.d(TAG,"round up: "+roundHelper.applyRoundFormat());
//
//        roundHelper=new RoundHelper(context,val2,2,1);
//        Log.d(TAG,"round down: "+roundHelper.applyRoundFormat());
//
//        roundHelper=new RoundHelper(context,val2,3,1);
//        Log.d(TAG,"up: "+roundHelper.applyRoundFormat());
//
//        roundHelper=new RoundHelper(context,val2,4,1);
//        Log.d(TAG,"down: "+roundHelper.applyRoundFormat());
//
//        Log.d(TAG,"-------------------------val : "+val3+"-------------------------------");
//
//        roundHelper=new RoundHelper(context,val3,1,1);
//        Log.d(TAG,"round up: "+roundHelper.applyRoundFormat());
//
//        roundHelper=new RoundHelper(context,val3,2,1);
//        Log.d(TAG,"round down: "+roundHelper.applyRoundFormat());
//
//        roundHelper=new RoundHelper(context,val3,3,1);
//        Log.d(TAG,"up: "+roundHelper.applyRoundFormat());
//
//        roundHelper=new RoundHelper(context,val3,4,1);
//        Log.d(TAG,"down: "+roundHelper.applyRoundFormat());

}