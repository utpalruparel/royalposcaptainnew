package com.royalpos.waiter.helper;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import java.util.Locale;

/**
 * Created by utpal on 14/11/17.
 */

public class SelectLanguage {

    public static void loadLocale(Context context) {
        String langPref = "Language";
        SharedPreferences prefs = context.getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        String language = prefs.getString(langPref, "");
        changeLang(language, context);
    }

    public static void changeLang(String lang, Context context) {
        if (lang.equalsIgnoreCase(""))
            return;
        Locale myLocale = new Locale(lang);

       // saveLocale(lang, context);
        Locale.setDefault(new Locale(lang, "US"));
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        context.getResources().updateConfiguration(config,context
                .getResources().getDisplayMetrics());

    }

    public static void saveLocale(String lang,String locale, Context context) {
        String langPref = "Language";
        SharedPreferences prefs = context.getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(langPref, lang);
        editor.commit();

        SharedPreferences.Editor editor1 = prefs.edit();
        editor1.putString("lang_loacle", locale);
        editor1.commit();
    }

    public static String getLocale( Context context) {
        String langPref = "en";
        SharedPreferences prefs = context.getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        return  prefs.getString("lang_loacle", "");
    }

    public static String getLang( Context context) {
        String langPref = "english";
        SharedPreferences prefs = context.getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        return  prefs.getString("Language", "");
    }
}
