package com.royalpos.waiter.helper;

import android.content.Context;
import android.util.Log;

import com.royalpos.waiter.model.M;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by reeva on 27/9/18.
 */

public class GenerateToken {

    String TAG="GenerateToken";
    long token= 1;

    public JSONObject generateToken(Context context){
        if(Integer.parseInt(new SimpleDateFormat("yyyy").format(new Date()))>2019){
            return generateNewToken(context);
        }else {
            String tok = M.getToken(context);
            String tokenfmt = AppConst.getTimestamp(context) + "-" + M.getWaiterid(context);
            JSONObject j = M.getTokenInfo(context);
            if (j != null && j.has("order_no")) {

                try {
                    String no = j.getString("order_no");
                    if (no.contains("-"))
                        no = j.getString("order_no").split("-")[1];
                    no = no.replaceFirst(M.getWaiterid(context), "");
                    if (no != null && no.contains("-")) {
                        token = 1;
                    } else if (no != null && tok != null && j.getString("order_no").equals(tok)) {
                        token = Long.parseLong(no) + 1;
                    } else {
                        if (tok != null && tok.trim().length() > 0) {
                            String last_no;
                            if (tok.contains("-"))
                                last_no = tok.split("-")[1];
                            else
                                last_no = tok;
                            last_no = last_no.replaceFirst(M.getWaiterid(context), "");
                            if (Long.parseLong(last_no) > Long.parseLong(no)) {
                                token = Long.parseLong(last_no) + 1;
                            } else if (Long.parseLong(last_no) == Long.parseLong(no)) {
                                token = Long.parseLong(last_no) + 1;
                            } else {
                                token = Long.parseLong(no) + 1;
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else if (tok != null && tok.trim().length() > 0) {
                String last_no;
                if (tok.contains("-"))
                    last_no = tok.split("-")[1];
                else
                    last_no = tok;
                last_no = last_no.replaceFirst(M.getWaiterid(context), "");
                token = Long.parseLong(last_no) + 1;
            }

            String order_no = tokenfmt + token;
            String token_number = M.getWaiterid(context) + token;
            Log.d(TAG, token + " " + order_no + " " + token_number);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("token", token_number);
                jsonObject.put("orderno", order_no);
                return jsonObject;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public JSONObject generateNewToken(Context context){
        String oldtokenfmt=AppConst.getTimestamp(context)+"-"+M.getWaiterid(context);
        String currYR=new SimpleDateFormat("yyyy").format(new Date());
        String tok=M.getToken(context);
        String tokenfmt=new SimpleDateFormat("yyMMdd").format(new Date())+M.getRestID(context)+M.getWaiterid(context)+"-"+M.getWaiterid(context);
        JSONObject j=M.getTokenInfo(context);
        Log.d(TAG,"last order:"+j);
        if(j!=null && j.has("order_no")){
            try {
                String no=j.getString("order_no");
                if(no.startsWith(oldtokenfmt)){
                    token = 1;
                }else {
                    String oYR = new SimpleDateFormat("yyyy").format(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(j.getString("order_time")));
                    if (no.contains("-"))
                        no = j.getString("order_no").split("-")[1];
                    no = no.replaceFirst(M.getWaiterid(context), "");
                    if (no != null && no.contains("-")) {
                        token = 1;
                    }else if(!currYR.equals(oYR)){
                        token = 1;
                    }else if (no != null && tok != null && j.getString("order_no").equals(tok)) {
                        token = Long.parseLong(no) + 1;
                    } else {
                        if (tok != null && tok.trim().length() > 0) {
                            String last_no;
                            if (tok.contains("-"))
                                last_no = tok.split("-")[1];
                            else
                                last_no = tok;
                            last_no = last_no.replaceFirst(M.getWaiterid(context), "");
                            if (Long.parseLong(last_no) > Long.parseLong(no)) {
                                token = Long.parseLong(last_no) + 1;
                            } else if (Long.parseLong(last_no) == Long.parseLong(no)) {
                                token = Long.parseLong(last_no) + 1;
                            } else {
                                token = Long.parseLong(no) + 1;
                            }
                        }
                    }
                }
            }catch (ParseException e){

            }catch (JSONException e) {
                e.printStackTrace();
            }

        }else if(tok!=null && tok.trim().length()>0){
            String last_no ;
            if(tok.startsWith(oldtokenfmt)){
                token=1;
            }else {
                String oYR = tok.split("-")[0].substring(0, 2);
                String cYR = currYR.substring(2);
                if (!cYR.equals(oYR)) {
                    token=1;
                } else {
                    if (tok.contains("-"))
                        last_no = tok.split("-")[1];
                    else
                        last_no = tok;
                    last_no = last_no.replaceFirst(M.getWaiterid(context), "");
                    token = Long.parseLong(last_no) + 1;
                }
            }
        }

        String order_no=tokenfmt+token;
        String token_number = M.getWaiterid(context)+token;
        Log.d(TAG,token+" "+order_no+" "+token_number);
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("token",token_number);
            jsonObject.put("orderno",order_no);
            return jsonObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean checkSysTime(String tm,Context context){
        boolean isRight=true;
        try {
            Date sysdt=new SimpleDateFormat("yyyy-MM-dd").parse(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
            if(tm!=null){
                Date sdt=new SimpleDateFormat("yyyy-MM-dd").parse(tm);
                if(sysdt.equals(sdt))
                    isRight=true;
                else
                    isRight=false;
            }
        } catch (ParseException e){
        }
        return isRight;
    }
}
