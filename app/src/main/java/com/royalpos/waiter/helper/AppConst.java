package com.royalpos.waiter.helper;

import android.app.ActivityManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.TextView;


import com.royalpos.waiter.R;
import com.royalpos.waiter.model.CustomerPojo;
import com.royalpos.waiter.model.DishOrderPojo;
import com.royalpos.waiter.model.KitchenPrinterPojo;
import com.royalpos.waiter.model.M;
import com.royalpos.waiter.model.OrderDetailPojo;
import com.royalpos.waiter.model.PreferencePojo;
import com.royalpos.waiter.model.Waiter;
import com.royalpos.waiter.universalprinter.Globals;
import com.royalpos.waiter.universalprinter.KitchenGlobals;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class AppConst {

    public static final String TAG = "RoyalPOSCaptain";

    //public static final String restaurant_logo_url="https://royalpos.in/rposuploads/uploads/";

    //public static final String Server_url ="https://royalpos.in/royalpos/";//
    public static final String Server_url ="http://swiftomatics.in/testroyalpos/";
    //public static final String Server_url ="http://192.168.0.7/royalpos/";//
    public static final String MAIN = Server_url+"public/";
    public static final String restaurant_logo_url="https://royalpos.in/rposuploads/uploads/";
    public static final String dish_img_url=MAIN+"uploads/";
    public static final String topic_img_url=MAIN+"uploads/topics/";
    public static String appurl = "https://play.google.com/store/apps/details?id=com.royalpos.waiter";
    public static String share = "Checkout this Amazing Cloud Based Point of Sale App! Download Royal POS Now! "+appurl;
    public static String whatsappnum="+918780228978";
    public static String ANDROID_YOUTUBE_API_KEY = "AIzaSyCzTgg8cj9zK3XpnukN_aPdEd8b6Htn8ck";
    public static String folder_dir= Environment.getExternalStorageDirectory()+"/RoyalPOSCaptain/";
    public static String item_img_dir=AppConst.folder_dir+"ItemImg/";

    public static ArrayList<Waiter> waiters=null;
    public static String currency = "";
    public static List<OrderDetailPojo> placelist;
    public static List<DishOrderPojo> dishorederlist;
    public static ArrayList<Integer> selidlist;
    public static ArrayList<PreferencePojo> taxdata;
    public static ArrayList<KitchenPrinterPojo> kplist=new ArrayList<>();
    public static ArrayList<Integer> totlist=new ArrayList<>();
    public static String fcm_id=null;
    public static String tbl_inactive_status="inactive";
    public static String tbl_active_status="active";
    public static String type="";
    public static CustomerPojo selCust;
    public static Boolean isDelivered=false;
    public static String[] catnmlist;
    public static Boolean choose_lang=false;
    public static String sendBillFormat = "Thank you for visiting {{brandname}} \n Your Invoice " +
            " is {{amount}} of Order NO : {{order_no}} \n Powered By Royal POS";
    public static String sendBillFormat_loyalty = "Thank you for visiting {{brandname}} \n Your " +
            "Invoice " +
            " is {{amount}} of Order NO : {{order_no}} \n You have used " +
            "{{loyalty_points}} Loyalty Points \n Powered" +
            " By Royal POS";

    public static Typeface font_regular(Context context){
        Typeface typeface=Typeface.createFromAsset(context.getAssets(), "fonts/RobotoRegular.ttf");
        return typeface;
    }

    public static Typeface font_medium(Context context){
        Typeface typeface=Typeface.createFromAsset(context.getAssets(), "fonts/RobotoMedium.ttf");
        return typeface;
    }

    public static Typeface font_italic(Context context){
        Typeface typeface=Typeface.createFromAsset(context.getAssets(), "fonts/RobotoItalic.ttf");
        return typeface;
    }

    public static Typeface font_arabic(Context context){
        Typeface typeface=Typeface.createFromAsset(context.getAssets(), "fonts/Kawkab.ttf");
        return typeface;
    }

    public static String getTimestamp(Context context) {
        String txt=M.getRestUniqueID(context).substring(0,3)+M.getRestID(context)+M.getWaiterid(context);
        return txt.toUpperCase();//(int) (System.currentTimeMillis() /1000L);
    }

    public static Double setdecimalfmt(double value,Context context){
        String pattern="#.##";
        if(M.getDecimalVal(context).equals("3"))
            pattern="#.###";

        DecimalFormat decimalFormat1 = new DecimalFormat(pattern);
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();

        dfs.setDecimalSeparator('.');
        decimalFormat1.setDecimalFormatSymbols(dfs);
        decimalFormat1.getNumberInstance(Locale.ENGLISH);
        decimalFormat1.applyPattern(pattern);
        double number=0;
        String amt=decimalFormat1.format((double)Math.round(value * 100) / 100)
                .replace("٠", "0").replace("١", "1").replace("٢", "2")
                .replace("٣", "3").replace("٤", "4").replace("٥", "5")
                .replace("٦", "6").replace("٧", "7").replace("٨", "8")
                .replace("٩", "9").replace("٫", ".");
        number=Double.parseDouble(amt);

        return number;
    }

    public static Double setdecimalfmt1(double value,String pattern){
        DecimalFormat decimalFormat1 = new DecimalFormat(pattern);
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();

        dfs.setDecimalSeparator('.');
        decimalFormat1.setDecimalFormatSymbols(dfs);
        decimalFormat1.getNumberInstance(Locale.ENGLISH);
        double number=0;
        String amt=decimalFormat1.format((double)Math.round(value * 100) /100)
                .replace("٠", "0").replace("١", "1").replace("٢", "2")
                .replace("٣", "3").replace("٤", "4").replace("٥", "5")
                .replace("٦", "6").replace("٧", "7").replace("٨", "8")
                .replace("٩", "9").replace("٫", ".");
        number=Double.parseDouble(amt);
        return number;
    }

    public static String arabicToEng(String number) {
        return number.replace("٠", "0").replace("١", "1").replace("٢", "2")
                .replace("٣", "3").replace("٤", "4").replace("٥", "5")
                .replace("٦", "6").replace("٧", "7").replace("٨", "8")
                .replace("٩", "9").replace("٫", ".");
    }

    public static void checkSame(Context context){
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        if(M.isCashPrinter(context) && M.isKitchenPrinter(context)) {
            Globals.loadPreferences(sharedPrefs);
            KitchenGlobals.loadPreferences(sharedPrefs);
            if (KitchenGlobals.deviceType == Globals.deviceType) {
                M.setSamePrinter(false, context);
//                if(Globals.deviceType<5 && (M.isadvanceprint(M.key_kitchen,context) || M.isadvanceprint(M.key_bill,context))){
//                    M.setSamePrinter(false, context);
//                }else

                if(Globals.deviceType<5 && (M.isadvanceprint(M.key_kitchen,context))){

                    if(M.isadvanceprint(M.key_bill,context)){


                        M.setSamePrinter(issamecheck(context), context);

                    }


                } else if(Globals.deviceType<5 && (M.isadvanceprint(M.key_bill,context))){

                    if(M.isadvanceprint(M.key_kitchen,context)){


                        M.setSamePrinter(issamecheck(context), context);

                    }


                } else if(Globals.deviceType==1){
                    if(Globals.deviceIP.equals(KitchenGlobals.deviceIP) && Globals.devicePort==KitchenGlobals.devicePort)
                        M.setSamePrinter(true, context);
                }else if(Globals.deviceType==3){
                    if(Globals.usbDeviceID==KitchenGlobals.usbDeviceID && Globals.usbProductID==KitchenGlobals.usbProductID
                            && Globals.usbVendorID==KitchenGlobals.usbVendorID)
                        M.setSamePrinter(true, context);
                }else if(Globals.deviceType==4){
                    if(Globals.blueToothDeviceAdress.equals(KitchenGlobals.blueToothDeviceAdress))
                        M.setSamePrinter(true, context);
                }else if(Globals.deviceType==5 && KitchenGlobals.deviceType==5)
                    M.setSamePrinter(false, context);
                else if(Globals.deviceType==6 && KitchenGlobals.deviceType==6)
                    M.setSamePrinter(true, context);
                else if(Globals.deviceType==7 && KitchenGlobals.deviceType==7){
                    if(M.getCashPrinterIP(context).equals(M.getKitchenPrinterIP(context)))
                        M.setSamePrinter(true, context);
                }else if(Globals.deviceType==8 && KitchenGlobals.deviceType==8)
                    M.setSamePrinter(true, context);
            } else {
                M.setSamePrinter(false, context);
            }
        }else
            M.setSamePrinter(false, context);
        Log.d(TAG,Globals.deviceType+" "+KitchenGlobals.deviceType);
        Log.d(TAG,"issame:"+M.isSamePrinter(context));
    }

    public static boolean issamecheck(Context context){
        boolean issame = false;
        if(Globals.deviceType==1){
            if(Globals.deviceIP.equals(KitchenGlobals.deviceIP) && Globals.devicePort==KitchenGlobals.devicePort)
                issame= true;
        }else if(Globals.deviceType==3){
            if(Globals.usbDeviceID==KitchenGlobals.usbDeviceID && Globals.usbProductID==KitchenGlobals.usbProductID
                    && Globals.usbVendorID==KitchenGlobals.usbVendorID)
                issame= true;
        }else if(Globals.deviceType==4){
            if(Globals.blueToothDeviceAdress.equals(KitchenGlobals.blueToothDeviceAdress))
                issame= true;
        }else if(Globals.deviceType==5 && KitchenGlobals.deviceType==5)
            issame= false;
        else if(Globals.deviceType==6 && KitchenGlobals.deviceType==6)
            issame= true;
        else if(Globals.deviceType==7 && KitchenGlobals.deviceType==7){
            if(M.getCashPrinterIP(context).equals(M.getKitchenPrinterIP(context)))
                issame= true;
        }else if(Globals.deviceType==8 && KitchenGlobals.deviceType==8)
            issame= true;

        return issame;
    }

    public static void analytics(Context context, String name) {
//        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
//        Bundle bundle = new Bundle();
//        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "" + M.getBrandId(context));
//        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "" + M.getBrandName(context));
//        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, name);
//        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

    }

    public static boolean isMyServiceRunning(Class<?> serviceClass,Context context) {
        ActivityManager manager = (ActivityManager)context. getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static void animation(final String text, final TextView txtview, final Context context){

        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {

            int count = 0;

            @Override
            public void run() {
                count++;

                if (count == 1)
                {
                    txtview.setText(text+"\n"+context.getString(R.string.progress_dialog_txt1)+".");
                }
                else if (count == 2)
                {
                    txtview.setText(text + "\n"+context.getString(R.string.progress_dialog_txt1)+"..");
                }
                else if (count == 3)
                {
                    txtview.setText(text +"\n"+context.getString(R.string.progress_dialog_txt1)+"...");
                }

                if (count == 3)
                    count = 0;

                handler.postDelayed(this, 1 * 800);
            }
        };
        handler.postDelayed(runnable, 1 * 1000);
    }

    // check active ,inactive table response
    //usb printer
    // finish activity after place order
}
