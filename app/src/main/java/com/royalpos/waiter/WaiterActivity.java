package com.royalpos.waiter;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.royalpos.waiter.adapter.ActiveCustomTableAdapter;
import com.royalpos.waiter.adapter.ActiveTableAdapter;
import com.royalpos.waiter.database.DBCombo;
import com.royalpos.waiter.database.DBCusines;
import com.royalpos.waiter.database.DBDiscount;
import com.royalpos.waiter.database.DBDishes;
import com.royalpos.waiter.database.DBModifier;
import com.royalpos.waiter.database.DBPaymentType;
import com.royalpos.waiter.database.DBPreferences;
import com.royalpos.waiter.database.DBUnit;
import com.royalpos.waiter.dialog.JoinServerDialog;
import com.royalpos.waiter.helper.AppConst;
import com.royalpos.waiter.helper.ConnectionDetector;
import com.royalpos.waiter.helper.GenerateToken;
import com.royalpos.waiter.helper.WifiHelper;
import com.royalpos.waiter.model.CheckCodePojo;
import com.royalpos.waiter.model.ComboModel;
import com.royalpos.waiter.model.CuisineListPojo;
import com.royalpos.waiter.model.DiscountPojo;
import com.royalpos.waiter.model.DishPojo;
import com.royalpos.waiter.model.M;
import com.royalpos.waiter.model.Modifier_cat;
import com.royalpos.waiter.model.PaymentModePojo;
import com.royalpos.waiter.model.PreModel;
import com.royalpos.waiter.model.RoundingPojo;
import com.royalpos.waiter.model.SuccessPojo;
import com.royalpos.waiter.model.TablePojo;
import com.royalpos.waiter.model.UnitPojo;
import com.royalpos.waiter.print.PrinterSettings;
import com.royalpos.waiter.webservices.APIServiceHeader;
import com.royalpos.waiter.webservices.AuthenticationAPI;
import com.royalpos.waiter.webservices.CuisineDishAPI;
import com.royalpos.waiter.webservices.OrderAPI;
import com.royalpos.waiter.webservices.WaiterAPI;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.greenrobot.event.EventBus;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class WaiterActivity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView rv,rvinactive,rvguest;
    LinearLayout llcustom;
    LinearLayout llwifi,llserver;
    TextView tvusername,tvguest,tvinactive,tvactive;
    Button btnaddcustom;
    Context context;
    String TAG="WaiterActivity";
    String order_no="",token_number,restaurantid,runiqueid;
    POServerService mService;
    Boolean mBound=false;
    JSONObject changetblJson,mergetblJson;
    int col=2;
    JoinServerDialog dialog;
    Dialog dialog1;
    MergeTblDialog mergeTblDialog;
    ConnectionDetector connectionDetector;
    long refreshtime=1*60*1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waiter);

        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        context=WaiterActivity.this;
        connectionDetector=new ConnectionDetector(context);
        restaurantid=M.getRestID(context);
        runiqueid=M.getRestUniqueID(context);

        rv=(RecyclerView)findViewById(R.id.rvtables);
        rvinactive=(RecyclerView)findViewById(R.id.rvinactive);
        rvguest=(RecyclerView)findViewById(R.id.rvguest);
        if(M.getTableRow(context)==null) {
            if (getResources().getBoolean(R.bool.portrait_only))
                col=1;
            else
                col=3;
        }else
            col=Integer.parseInt(M.getTableRow(context));
        rv.setLayoutManager(new GridLayoutManager(context,col));
        rv.setNestedScrollingEnabled(false);
        rv.setHasFixedSize(true);

        rvinactive.setLayoutManager(new GridLayoutManager(context,col));
        rvinactive.setNestedScrollingEnabled(false);
        rvinactive.setHasFixedSize(true);

        rvguest.setLayoutManager(new GridLayoutManager(context,col));
        rvguest.setNestedScrollingEnabled(false);
        rvguest.setHasFixedSize(true);

        llcustom=(LinearLayout)findViewById(R.id.llcustom);
        tvusername=(TextView)findViewById(R.id.tvusername);
        tvusername.setText(M.getWaitername(context));
        tvactive=(TextView)findViewById(R.id.tvactive);
        tvinactive=(TextView)findViewById(R.id.tvinactive);
        tvguest=(TextView)findViewById(R.id.tvguest);
        btnaddcustom=(Button)findViewById(R.id.btnaddcustom);
        btnaddcustom.setTypeface(AppConst.font_regular(context));

        if(AppConst.dishorederlist!=null)
            AppConst.dishorederlist.clear();
        if(AppConst.selidlist!=null)
            AppConst.selidlist.clear();
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        if(M.getDeviceCode(context)!=null && M.getDeviceCode(context).trim().length()>0)
            checkDeviceCode();

        generateToken();
        bindService();

        tvguest.setOnClickListener(this);
        tvinactive.setOnClickListener(this);
        tvactive.setOnClickListener(this);
        btnaddcustom.setOnClickListener(this);

    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(broadcastReceiver,new IntentFilter(POServerService.RECEIVE_MSG));
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getExtras()!=null){
                if(intent.hasExtra("stopService")){
                    mBound = false;
                    joinDialog();
                }else {
                    String res = intent.getStringExtra("response");
                    Log.d(TAG,"broadcastReceiver:"+res);
                    if (res != null && !res.isEmpty() && !res.equals("null"))
                        setData(intent.getStringExtra("response"));
                }
            }
        }
    };

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,IBinder service) {
            POServerService.LocalBinder binder = (POServerService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;

            mService.updateConnect(true);

            sendRequest(WifiHelper.api_active_table);
            sendRequest(WifiHelper.api_inactive_table);
            sendRequest(WifiHelper.api_custom_table);

//            if(!thread.isAlive())
//                thread.start();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
            if(thread.isAlive())
                thread.interrupt();
        }
    };

    private void bindService(){
        if(AppConst.isMyServiceRunning(POServerService.class,context)) {
            Intent intent = new Intent(this, POServerService.class);
            bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        }else{
            joinDialog();
        }
    }

    Thread thread = new Thread() {

        @Override
        public void run() {
            try {
                while (!thread.isInterrupted()) {
                    Thread.sleep(refreshtime);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String lasttime=M.retriveVal(M.key_server_lasttime,context);
                            if(lasttime!=null && !lasttime.isEmpty()){
                                long diffTm=new Date().getTime()-Long.parseLong(lasttime);
                                if(diffTm>60*1000){
                                    joinDialog();
                                }
                            }
                        }
                    });
                }
            } catch (InterruptedException e) {
            }
        }
    };

    private void joinDialog(){

        mBound = false;
        rv.setVisibility(View.GONE);
        rvinactive.setVisibility(View.GONE);
        if(dialog!=null && dialog.isShowing()){}
        else {
            try {
                dialog = new JoinServerDialog(context, WaiterActivity.this, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        logout(dialog);
                    }
                });
                dialog.show();
            }catch (Exception e){
                Log.d(TAG,"error:"+e.getMessage());
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @SuppressWarnings("unused")
    public void onEventMainThread(String pusher) {
        try {
            if(pusher.startsWith("changeTable")){
                String req=pusher.replaceFirst("changeTable","");
                changetblJson=new JSONObject(req);
                sendRequest(WifiHelper.api_inactive_table);
            }if(pusher.equals("updateColCount")) {
                if(col!=Integer.parseInt(M.getTableRow(context))) {
                    col=Integer.parseInt(M.getTableRow(context));
                    rv.setLayoutManager(new GridLayoutManager(context, Integer.parseInt(M.getTableRow(context))));
                }
            }else if(pusher.startsWith("mergeTable")){
                String req=pusher.replaceFirst("mergeTable","");
                mergetblJson=new JSONObject(req);

                sendRequest(WifiHelper.api_merge_table+mergetblJson.getString("table_id"));

            }else if(pusher.startsWith("addMerge")){
                String req=pusher.replaceFirst("addMerge","");
                JSONObject j = new JSONObject(),jMsg=new JSONObject();
                j.put("key", WifiHelper.api_add_merge);
                j.put("sel_tbl",req);
                j.put("main_tbl",mergetblJson+"");
                j.put("user_ip",getLocalIp());
                sendRequest(j + "");
            }
        }catch (JSONException e){
            Log.d(TAG,"eventbus error:"+e);
        }
    }

    private void openLogoutDialog(){
        final Dialog dialog9=new Dialog(context);
        dialog9.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog9.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT);
        dialog9.setContentView(R.layout.dialog_logout);
        TextView tvyes=(TextView)dialog9.findViewById(R.id.tvyes);
        TextView tvno=(TextView)dialog9.findViewById(R.id.tvno);
        tvyes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout(dialog9);
            }
        });
        tvno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog9.dismiss();
            }
        });
        dialog9.show();
    }

    private void setData(String res){
        if(res!=null && !res.isEmpty() && AppConst.isMyServiceRunning(POServerService.class,context)) {
            try {
                JSONObject jsonObject = new JSONObject(res);
                if (jsonObject.has("key")) {
                    M.saveVal(M.key_server_lasttime,new Date().getTime()+"",context);
                    String msg_key = jsonObject.getString("key");
                    String data = "";
                    if (jsonObject.has("msg"))
                        data = jsonObject.getString("msg");
                    if (msg_key.equals(WifiHelper.KEY_CONNECTION)) {
                        if (jsonObject.getString("success").equals(WifiHelper.TXT_FAIL)) {
                            if(mBound)
                                mService.closeService();
                            else {

                                stopService(new Intent(context, POServerService.class));
                            }
                        } else {
                            if(dialog!=null && dialog.isShowing())
                                dialog.dismiss();
                            if(jsonObject.has("show_sitting")) {
                                M.setSitting(jsonObject.getBoolean("show_sitting"), context);
                            }
                            bindService();
                            if(M.getWaiterid(context).equals(jsonObject.getString("user_id"))){
                                if(jsonObject.has("last_order_time") && jsonObject.has("last_order_no")){
                                    M.setTokenInfo(jsonObject.getString("last_order_time"),
                                            jsonObject.getString("last_order_no"),context);
                                }
                            }
                            generateToken();
                        }
                    } else if (msg_key.equals(WifiHelper.api_inactive_table_response)){
                        if (data != null && data.trim().length() > 0) {
                            JSONArray jsonArray = new JSONArray(data);
                            if (jsonArray != null) {
                                tvinactive.setText(context.getString(R.string.inactive_tables)+" ("+jsonArray.length()+")");
                                InActiveTableAdapter inadapter = new InActiveTableAdapter(jsonArray, context);
                                rvinactive.setAdapter(inadapter);
                                rvinactive.setVisibility(View.VISIBLE);
                            }else
                                rvinactive.setVisibility(View.GONE);

                            if(changetblJson!=null)
                                setInactiveTable(jsonArray);
                        }
                    } else if (msg_key.equals(WifiHelper.api_active_table_response)) {
                        if (data != null && data.trim().length() > 0) {
                            JSONArray jsonArray = new JSONArray(data);
                            if (jsonArray != null) {
                                tvactive.setText(context.getString(R.string.active_tables)+" ("+jsonArray.length()+")");
                                ActiveTableAdapter adapter = new ActiveTableAdapter(jsonArray, context);
                                rv.setAdapter(adapter);
                                rv.setVisibility(View.VISIBLE);
                            } else {
                                rv.setVisibility(View.GONE);
                            }
                        }
                    }else if (msg_key.equals(WifiHelper.api_custom_table_response)) {
                        if(jsonObject.has("message")){
                            llcustom.setVisibility(View.GONE);

                        }else if (data != null && data.trim().length() > 0) {
                            llcustom.setVisibility(View.VISIBLE);
                            JSONArray jsonArray = new JSONArray(data);
                            tvguest.setText(context.getString(R.string.other_sitting)+" (0)");
                            if (jsonArray != null) {
                                tvguest.setText(context.getString(R.string.other_sitting)+" ("+jsonArray.length()+")");
                                ActiveCustomTableAdapter adapter = new ActiveCustomTableAdapter(jsonArray, context);
                                rvguest.setAdapter(adapter);
                                rvguest.setVisibility(View.VISIBLE);
                            }else {
                                rvguest.setVisibility(View.GONE);
                            }
                        }
                    } else if (msg_key.equals(WifiHelper.api_start_order_response)) {
                        if (data != null && data.trim().length() > 0 && data.contains("{")) {
                            JSONObject jData = new JSONObject(data);
                            if (jsonObject.getString("success").equals(WifiHelper.TXT_FAIL) &&
                                    jsonObject.getString("user_id").equals(M.getWaiterid(context)))
                                Toast.makeText(context, data, Toast.LENGTH_SHORT).show();
                            else if (jsonObject.getString("success").equals(WifiHelper.TXT_SUCCESS) &&
                                    jData.getString("user_id").equals(M.getWaiterid(context))) {
                                M.setToken(jData.getString("order_no"), context);
                                Intent it = new Intent(context, PlaceOrder.class);
                                it.putExtra("orderid", jData.getString("order_id"));
                                it.putExtra("tbl_id", jData.getString("table_id"));
                                if(jData.getString("table_id").equals("0"))
                                    it.putExtra("tbl_nm", jData.getString("sitting"));
                                else
                                    it.putExtra("tbl_nm", jData.getString("table_nm"));
                                it.putExtra("token", jData.getString("token"));
                                it.putExtra("order_no", jData.getString("order_no"));
                                it.putExtra("ordertime", jData.getString("ordertime"));
                                it.putExtra("custnm", jData.getString("cust_nm"));
                                it.putExtra("custph", jData.getString("cust_ph"));
                                finish();
                                startActivity(it);
                            }
                        }
                    }else if(msg_key.equals(WifiHelper.api_change_table_response)) {
                        String uid= "";
                        if(jsonObject.has("user_id"))
                            uid=jsonObject.getString("user_id");
                        if (data != null && uid!=null && uid.equals(M.getWaiterid(context)))
                            Toast.makeText(context, data, Toast.LENGTH_SHORT).show();
                    }else if(msg_key.equals(WifiHelper.api_merge_table_response)){
                        if (data != null && data.trim().length() > 0) {
                            JSONArray jsonArray = new JSONArray(data);
                            if(mergetblJson!=null) {
                                if(jsonArray!=null && jsonArray.length()>0){
                                    if(mergeTblDialog==null) {
                                        mergeTblDialog = new MergeTblDialog(context, mergetblJson, jsonArray);
                                        mergeTblDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                            @Override
                                            public void onDismiss(DialogInterface dialogInterface) {
                                                mergeTblDialog = null;
                                            }
                                        });
                                        if (!mergeTblDialog.isShowing())
                                            mergeTblDialog.show();
                                    }
                                }else
                                    Toast.makeText(context,"Table not found",Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            } catch (JSONException e) {
                Log.d(TAG, "wifi json error:" + e.getMessage());
            }
        }
    }

    private void setInactiveTable(final JSONArray tlist) {
        ArrayList<String> tnmlist=new ArrayList<>();
        tnmlist.clear();
        final ArrayList<TablePojo> tbllist=new ArrayList<>();
        tbllist.clear();
        for(int t=0;t<tlist.length();t++){
            try {
                JSONObject j=tlist.getJSONObject(t);
                String id=j.getString("table_id");
                String nm=j.getString("table_name");
                tbllist.add(new TablePojo(id,nm));
                tnmlist.add(nm);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if(tnmlist!=null && tnmlist.size()>0) {
            changeTable(tbllist,tnmlist);
        }
    }

    private void changeTable(final ArrayList<TablePojo> tlist, final ArrayList<String> tnmlist){
        String tbid=null,tblnm=null,oid=null;
        try {
            tbid=changetblJson.getString("table_id");
            tblnm=changetblJson.getString("table_name");
            oid=changetblJson.getString("order_id");
            changetblJson=null;
            final Dialog d = new Dialog(context);
            d.requestWindowFeature(Window.FEATURE_NO_TITLE);
            d.setContentView(R.layout.dialog_change_table);
            d.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            EditText etoldtbl = (EditText) d.findViewById(R.id.etoldtbl);
            final EditText etntbl = (EditText) d.findViewById(R.id.etnewtbl);
            Button btnchange = (Button) d.findViewById(R.id.btnchange);

            etoldtbl.setText(tblnm);

            etntbl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(tnmlist!=null && tnmlist.size()>0) {
                        final Dialog dialog_inactive_table = new Dialog(context);
                        dialog_inactive_table.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog_inactive_table.setContentView(R.layout.dialog_inactive_table);
                        dialog_inactive_table.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        ListView lv = (ListView) dialog_inactive_table.findViewById(R.id.lvtable);
                        ArrayAdapter<String> tada = new ArrayAdapter<String>(context, R.layout.inactive_table_row, R.id.txt, tnmlist);
                        lv.setAdapter(tada);
                        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {
                                etntbl.setText(tlist.get(i).getName());
                                etntbl.setTag(tlist.get(i).getId());
                                etntbl.setError(null);
                                dialog_inactive_table.dismiss();
                            }
                        });
                        dialog_inactive_table.show();
                    }
                }
            });

            final String finalOid = oid;
            final String finalTbid = tbid;
            btnchange.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (etntbl.getTag() == null) {
                        etntbl.setError(context.getString(R.string.txt_table_not_select));
                    } else {
                        try {
                            JSONObject jsonObject=new JSONObject();
                            jsonObject.put("order_id", finalOid);
                            jsonObject.put("user_id", M.getWaiterid(context));
                            jsonObject.put("old_table_id", finalTbid);
                            jsonObject.put("new_table_id",etntbl.getTag().toString());
                            jsonObject.put("key",WifiHelper.api_change_table);
                            jsonObject.put("user_ip",getLocalIp());
                            sendRequest(jsonObject+"");
                            d.dismiss();
                        }catch (JSONException e){}

                    }
                }
            });
            if(tbid!=null && tbid!=null && oid!=null)
                d.show();
        }catch (JSONException e){}

    }

    public void sendRequest(String req_data){
        if(AppConst.isMyServiceRunning(POServerService.class,context)){// && AppConst.isMyServiceRunning(ClientService.class,context)){
            mService.sendMsg(req_data);
        }else
            joinDialog();
    }

    private void generateToken() {
        GenerateToken gt=new GenerateToken();
        JSONObject jo=gt.generateToken(context);
        if(jo!=null){
            try {
                if(jo.has("orderno"))
                    order_no=jo.getString("orderno");
                if(jo.has("token"))
                    token_number=jo.getString("token");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        Log.d(TAG,order_no+" {"+token_number+"}");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==301){
            generateToken();
            if(AppConst.isMyServiceRunning(POServerService.class,context)){
                sendRequest(WifiHelper.api_active_table);
                sendRequest(WifiHelper.api_inactive_table);
                sendRequest(WifiHelper.api_custom_table);
            }else
                joinDialog();
        }else if(requestCode==302){
            dialog.checkWifi();
        }
    }

    private void logout(final Dialog dialog9){
        ConnectionDetector connectionDetector=new ConnectionDetector(context);
        String deviceid= Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        if(connectionDetector.isConnectingToInternet()){
            M.showLoadingDialog(context);

            WaiterAPI mAuthenticationAPI = APIServiceHeader.createService(context, WaiterAPI.class);
            Call<SuccessPojo> call = mAuthenticationAPI.waiterLogout(M.getRestID(context),M.getRestUniqueID(context),deviceid);
            call.enqueue(new retrofit2.Callback<SuccessPojo>() {
                @Override
                public void onResponse(Call<SuccessPojo> call, retrofit2.Response<SuccessPojo> response) {
                    M.hideLoadingDialog();
                    if (response.isSuccessful()) {
                        if(response.body().getSuccess()==1){
                            try {
                                JSONObject j=new JSONObject();
                                j.put("key","Disconnect");
                                j.put("ip",getLocalIp());
                                j.put("device_id",Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));
                                sendRequest(j+"");
                            }catch (JSONException e){}
                            Intent mIntent = new Intent(context, WaiterListActivity.class);
                            if(M.getDeviceCode(context)!=null)
                                mIntent= new Intent(context, LoginActivity.class);
                            else if(dialog9!=null)
                                dialog9.dismiss();
                            if(AppConst.isMyServiceRunning(POServerService.class,context))
                                stopService(new Intent(context,POServerService.class));
                            M.waiterlogOut(context);
                            if(mBound) {
                                unbindService(mConnection);
                                mBound=false;
                            }

                            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            finish();
                            startActivity(mIntent);
                        }
                    } else {
                        int statusCode = response.code();
                        ResponseBody errorBody = response.errorBody();
                        Log.d(TAG, "error:" + statusCode + " " + errorBody);
                    }
                }

                @Override
                public void onFailure(Call<SuccessPojo> call, Throwable t) {
                    Log.d(TAG, "fail:" + t.getMessage());
                    M.hideLoadingDialog();
                }
            });
        }else
            Toast.makeText(context,R.string.no_internet_error,Toast.LENGTH_SHORT).show();
    }

    private void getCuisine() {
        if(connectionDetector.isConnectingToInternet()) {
            CuisineDishAPI mAuthenticationAPI = APIServiceHeader.createService(context, CuisineDishAPI.class);
            Call<List<CuisineListPojo>> call = mAuthenticationAPI.getCuisines(restaurantid, runiqueid);
            call.enqueue(new retrofit2.Callback<List<CuisineListPojo>>() {
                @Override
                public void onResponse(Call<List<CuisineListPojo>> call, retrofit2.Response<List<CuisineListPojo>> response) {
                    if (response.isSuccessful()) {
                        List<CuisineListPojo> custCuisineListPojos = response.body();
                        if (custCuisineListPojos != null) {
                            if (custCuisineListPojos != null) {
                                {
                                    DBCusines db = new DBCusines(context);
                                    db.deleteallcuisine();
                                    for (CuisineListPojo pojo : custCuisineListPojos) {
                                        db.addCusines(pojo);
                                    }
                                }
                            }
                            String tm=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
                            Log.d(TAG,"timing:"+tm);
                            M.setUpdateTime(tm,context);
                            getAllDishes();
                        }
                    } else {
                        if(dialog1.isShowing())
                            dialog1.dismiss();
                        int statusCode = response.code();
                        ResponseBody errorBody = response.errorBody();
                        Log.d(TAG, "error:" + statusCode + " " + errorBody);
                    }
                }

                @Override
                public void onFailure(Call<List<CuisineListPojo>> call, Throwable t) {
                    Log.d("response:", "fail:" + t.getMessage());
                    if(dialog1.isShowing())
                        dialog1.dismiss();
                }
            });
        }else {
            Toast.makeText(context, R.string.no_internet_error, Toast.LENGTH_SHORT).show();
            if(dialog1.isShowing())
                dialog1.dismiss();
        }
    }

    private void getAllDishes() {
        if(connectionDetector.isConnectingToInternet()){
            CuisineDishAPI mAuthenticationAPI = APIServiceHeader.createService(context, CuisineDishAPI.class);
            Call<List<DishPojo>> call = mAuthenticationAPI.getDishes(restaurantid, runiqueid);
            call.enqueue(new retrofit2.Callback<List<DishPojo>>() {
                @Override
                public void onResponse(Call<List<DishPojo>> call, retrofit2.Response<List<DishPojo>> response) {
                    if (response.isSuccessful()) {
                        List<DishPojo> dishiesPojos = response.body();
                        DBDishes db = new DBDishes(context);
                        db.deleteallcuisine();
                        DBPreferences dbp=new DBPreferences(context);
                        dbp.deleteAllPre();
                        DBModifier dbm=new DBModifier(context);
                        dbm.deleteAllModi();
                        DBCombo dbCombo = new DBCombo(context);
                        dbCombo.deleteAllPre();
                        if (dishiesPojos != null) {
                            for (DishPojo dishesdata : dishiesPojos) {
                                String dishid = dishesdata.getDishid();
                                db.addDishes(dishesdata);
                                if(dishesdata.getPreflag().equals("true") && dishesdata.getPre()!=null) {
                                    for (PreModel data : dishesdata.getPre()) {
                                        dbp.addPref(data, dishid);
                                    }
                                    for(Modifier_cat mdata:dishesdata.getModifier_cat()){
                                        dbm.check(mdata,dishid);
                                    }
                                }

                                if(dishesdata.getCombo_flag()!=null) {

                                    if (dishesdata.getCombo_flag().equals("true") && dishesdata.getCombo_item() !=
                                            null) {
                                        for (ComboModel combo : dishesdata.getCombo_item()) {
                                            dbCombo.addcombo(combo, dishid);
                                        }

                                    }
                                }
                            }
                        }
                        String tm=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
                        Log.d(TAG,"timing:"+tm);
                        M.setUpdateTime(tm,context);
                        getRoundingInfo();
                    } else {
                        int statusCode = response.code();
                        ResponseBody errorBody = response.errorBody();
                        Log.d(TAG, "error:" + statusCode + " " + errorBody);
                        if(dialog1.isShowing())
                            dialog1.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<List<DishPojo>> call, Throwable t) {
                    Log.d("response:", "fail:" + t.getMessage());
                    if(dialog1.isShowing())
                        dialog1.dismiss();
                }
            });
        }else {
            Toast.makeText(context, R.string.no_internet_error, Toast.LENGTH_SHORT).show();
            if(dialog1.isShowing())
                dialog1.dismiss();
        }
    }

    void getRoundingInfo() {
        if(connectionDetector.isConnectingToInternet()){
            OrderAPI mAuthenticationAPI = APIServiceHeader.createService(context, OrderAPI.class);
            Call<RoundingPojo> call = mAuthenticationAPI.getRoundingInfo(restaurantid,runiqueid,M.getWaiterid(context));
            call.enqueue(new retrofit2.Callback<RoundingPojo>() {
                @Override
                public void onResponse(Call<RoundingPojo> call, retrofit2.Response<RoundingPojo> response) {
                    if (response.isSuccessful()) {
                        RoundingPojo resp = response.body();
                        if(resp!=null){
                            if(resp.getIs_rounding_on().equals("on") &&
                                    resp.getRounding_id()!=null && !resp.getRounding_id().equals("0")){
                                try {
                                    JSONObject jsonObject=new JSONObject();
                                    jsonObject.put("val_interval",resp.getInterval_number());
                                    jsonObject.put("rule_tag",resp.getRounding_tag());
                                    jsonObject.put("rounding_id",resp.getRounding_id());
                                    M.setRoundBoundary(jsonObject+"",context);
                                    M.setRoundFormat(true,context);
                                }catch (JSONException e){}
                            }else
                                M.setRoundFormat(false,context);
                            if(resp.getShow_item_price_without_tax()!=null){
                                M.setPriceWT(Boolean.parseBoolean(resp.getShow_item_price_without_tax()),context);
                            }
                            if(resp.getAllow_payment()!=null)
                                M.setCashierPermission(Boolean.parseBoolean(resp.getAllow_payment()),context);
                            M.setOutletPin(resp.getPin(),context);
                            M.setPointsAmt(resp.getPoints_per_one_currency(),context);
                            if(resp.getAllow_item_wise_discount()!=null && resp.getAllow_item_wise_discount().equals("enable"))
                                M.setItemDiscount(true,context);
                            else
                                M.setItemDiscount(false,context);
                            if(!resp.getService_charges().equals("disable"))
                                M.setServiceCharge(true,context);
                            else
                                M.setServiceCharge(false,context);
                            M.saveVal(M.key_tip_cal,resp.getTip_calculation(),context);
                            M.saveVal(M.key_tip_pref,resp.getTip_calculation_preference(),context);
                            M.saveVal(M.key_phonepe_status,resp.getPhonepe_enable(),context);
                            M.saveVal(M.key_phonepe_paymode,resp.getPhonepe_mapped_pay_mode(),context);
                            M.saveVal(M.key_phonepe_paymode_qrcode,resp.getPhonepe_upi_dynamic_qrcode(),context);
                        }
                        String tm=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
                        Log.d(TAG,"timing:"+tm);
                        M.setUpdateTime(tm,context);
                        getPaymentMode();
                    } else {
                        int statusCode = response.code();
                        ResponseBody errorBody = response.errorBody();
                        Log.d(TAG, "rounding:" + statusCode + " " + errorBody);
                        if(dialog1.isShowing())
                            dialog1.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<RoundingPojo> call, Throwable t) {
                    Log.d(TAG, "rounding fail:" + t.getMessage());
                    if(dialog1.isShowing())
                        dialog1.dismiss();
                }
            });
        }else {
            Toast.makeText(context, R.string.no_internet_error, Toast.LENGTH_SHORT).show();
            if(dialog1.isShowing())
                dialog1.dismiss();
        }
    }

    void getPaymentMode() {

        if(connectionDetector.isConnectingToInternet()){
            OrderAPI mAuthenticationAPI = APIServiceHeader.createService(context, OrderAPI.class);
            Call<List<PaymentModePojo>> call = mAuthenticationAPI.getPaymentMode(restaurantid,runiqueid);
            call.enqueue(new retrofit2.Callback<List<PaymentModePojo>>() {
                @Override
                public void onResponse(Call<List<PaymentModePojo>> call, retrofit2.Response<List<PaymentModePojo>> response) {
                    if (response.isSuccessful()) {
                        List<PaymentModePojo> resp = response.body();
                        if (resp != null) {
                            DBPaymentType dbPaymentType=new DBPaymentType(context);
                            dbPaymentType.deletealltype();
                            for (PaymentModePojo p:resp){
                                dbPaymentType.addType(p);
                            }
                        }
                        String tm=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
                        Log.d(TAG,"timing:"+tm);
                        M.setUpdateTime(tm,context);
                        getUnits();
                    } else {
                        int statusCode = response.code();
                        ResponseBody errorBody = response.errorBody();
                        Log.d(TAG, "error:" + statusCode + " " + errorBody);
                        if(dialog1.isShowing())
                            dialog1.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<List<PaymentModePojo>> call, Throwable t) {
                    Log.d("response:", "fail:" + t.getMessage());
                    if(dialog1.isShowing())
                        dialog1.dismiss();
                }
            });
        }else {
            Toast.makeText(context, R.string.no_internet_error, Toast.LENGTH_SHORT).show();
            if(dialog1.isShowing())
                dialog1.dismiss();
        }
    }

    void getUnits() {
        if(connectionDetector.isConnectingToInternet()){
            CuisineDishAPI mAuthenticationAPI = APIServiceHeader.createService(context, CuisineDishAPI.class);
            Call<List<UnitPojo>> call = mAuthenticationAPI.getUnitList(restaurantid,runiqueid);
            call.enqueue(new retrofit2.Callback<List<UnitPojo>>() {
                @Override
                public void onResponse(Call<List<UnitPojo>> call, retrofit2.Response<List<UnitPojo>> response) {
                    if (response.isSuccessful()) {
                        List<UnitPojo> resp = response.body();
                        if(resp!=null){
                            DBUnit dbUnit=new DBUnit(context);
                            dbUnit.deleteall();
                            for (UnitPojo p:resp){
                                dbUnit.addUnit(p);
                            }
                        }
                        String tm=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
                        Log.d(TAG,"timing:"+tm);
                        M.setUpdateTime(tm,context);
                        getDiscount();
                    } else {
                        int statusCode = response.code();
                        ResponseBody errorBody = response.errorBody();
                        Log.d(TAG, "error:" + statusCode + " " + errorBody);
                        if(dialog1.isShowing())
                            dialog1.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<List<UnitPojo>> call, Throwable t) {
                    Log.d(TAG, "unit fail:" + t.getMessage());
                    if(dialog1.isShowing())
                        dialog1.dismiss();
                }
            });
        }else {
            Toast.makeText(context, R.string.no_internet_error, Toast.LENGTH_SHORT).show();
            if(dialog1.isShowing())
                dialog1.dismiss();
        }
    }

    void getDiscount() {
        if(connectionDetector.isConnectingToInternet()){
            CuisineDishAPI mAuthenticationAPI = APIServiceHeader.createService(context, CuisineDishAPI.class);
            Call<List<DiscountPojo>> call = mAuthenticationAPI.billOffer(restaurantid,runiqueid);
            call.enqueue(new retrofit2.Callback<List<DiscountPojo>>() {
                @Override
                public void onResponse(Call<List<DiscountPojo>> call, retrofit2.Response<List<DiscountPojo>> response) {
                    if (response.isSuccessful()) {
                        List<DiscountPojo> resp = response.body();
                        if (resp != null) {
                            DBDiscount dbDiscount=new DBDiscount(context);
                            dbDiscount.deleteall();
                            for(DiscountPojo d:resp){
                                dbDiscount.addDiscount(d);
                            }
                        }
                        String tm=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
                        Log.d(TAG,"timing:"+tm);
                        M.setUpdateTime(tm,context);
                        if(AppConst.dishorederlist!=null)
                            AppConst.dishorederlist.clear();
                        if(AppConst.selidlist!=null)
                            AppConst.selidlist.clear();
                        if(dialog1.isShowing())
                            dialog1.dismiss();
                    } else {
                        int statusCode = response.code();
                        ResponseBody errorBody = response.errorBody();
                        Log.d(TAG, "error:" + statusCode + " " + errorBody);
                        if(dialog1.isShowing())
                            dialog1.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<List<DiscountPojo>> call, Throwable t) {
                    Log.d(TAG, "discount fail:" + t.getMessage());
                    if(dialog1.isShowing())
                        dialog1.dismiss();
                }
            });
        }else {
            Toast.makeText(context, R.string.no_internet_error, Toast.LENGTH_SHORT).show();
            if(dialog1.isShowing())
                dialog1.dismiss();
        }
    }

    @Override
    public void onClick(View v) {
        if(tvactive==v){
            if (v.getTag().equals("0")) {
                v.setTag("1");
                tvactive.setCompoundDrawablesWithIntrinsicBounds(null,null,context.getResources().getDrawable(R.drawable.ic_up_arrow),null);
                rv.setVisibility(View.VISIBLE);
            } else {
                v.setTag("0");
                tvactive.setCompoundDrawablesWithIntrinsicBounds(null,null,context.getResources().getDrawable(R.drawable.ic_down_arrow),null);
                rv.setVisibility(View.GONE);
            }
        }else if(v==tvinactive){
            if (v.getTag().equals("0")) {
                v.setTag("1");
                tvinactive.setCompoundDrawablesWithIntrinsicBounds(null,null,context.getResources().getDrawable(R.drawable.ic_up_arrow),null);
                rvinactive.setVisibility(View.VISIBLE);
            } else {
                v.setTag("0");
                tvinactive.setCompoundDrawablesWithIntrinsicBounds(null,null,context.getResources().getDrawable(R.drawable.ic_down_arrow),null);
                rvinactive.setVisibility(View.GONE);
            }
        }else if(v==tvguest){
            if (v.getTag().equals("0")) {
                v.setTag("1");
                tvguest.setCompoundDrawablesWithIntrinsicBounds(null,null,context.getResources().getDrawable(R.drawable.ic_up_arrow),null);
                rvguest.setVisibility(View.VISIBLE);
            } else {
                v.setTag("0");
                tvguest.setCompoundDrawablesWithIntrinsicBounds(null,null,context.getResources().getDrawable(R.drawable.ic_down_arrow),null);
                rvguest.setVisibility(View.GONE);
            }
        }else if(v==btnaddcustom){
            addCustom();
        }
    }

    private void addCustom(){
        generateToken();
        final Dialog cDialog=new Dialog(context);
        cDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        cDialog.setContentView(R.layout.dialog_add_custom_dinein);
        cDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        final EditText etnm=(EditText)cDialog.findViewById(R.id.etcustomername);
        etnm.setTypeface(AppConst.font_regular(context));
        final EditText etph=(EditText)cDialog.findViewById(R.id.etphone);
        etph.setTypeface(AppConst.font_regular(context));
        final EditText etsitting=(EditText)cDialog.findViewById(R.id.etsitting);
        etsitting.setTypeface(AppConst.font_regular(context));
        TextView btnsubmit=(TextView)cDialog.findViewById(R.id.btnsubmit);
        TextView ivclose=(TextView)cDialog.findViewById(R.id.ivclose);

        ivclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cDialog.dismiss();
            }
        });

        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etsitting.getText().toString().trim().length()==0)
                    etsitting.setError(getString(R.string.empty_field));
                else{
                    try {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("rest_id", M.getRestID(context));
                        jsonObject.put("rest_uniq_id", M.getRestUniqueID(context));
                        jsonObject.put("table_id", "0");
                        jsonObject.put("userid", M.getWaiterid(context));
                        jsonObject.put("usernm", M.getWaitername(context));
                        jsonObject.put("order_no", order_no);
                        jsonObject.put("token_number", token_number);
                        jsonObject.put("noppl","");
                        jsonObject.put("sitting",etsitting.getText().toString());
                        jsonObject.put("cust_nm",etnm.getText().toString());
                        jsonObject.put("cust_ph",etph.getText().toString());

                        JSONObject startOrder = new JSONObject();
                        startOrder.put("key", WifiHelper.api_start_order);
                        startOrder.put("msg", jsonObject + "");
                        jsonObject.put("user_ip",getLocalIp());
                        sendRequest(startOrder + "");
                        cDialog.dismiss();
                    } catch (JSONException e) {
                    }
                }
            }
        });
        cDialog.show();
    }

    public class InActiveTableAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private static final int TYPE_ITEM = 1;
        private JSONArray list;
        Context context;
        String TAG="InActiveTableAdapter";

        public InActiveTableAdapter(JSONArray list, Context mcontext) {
            context = mcontext;
            this.list = list;
        }

        @Override
        public int getItemViewType(int position) {
            return TYPE_ITEM;
        }


        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.inactivetable_row, parent, false);
            return new ViewHolderPosts(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

            final ViewHolderPosts itemview = (ViewHolderPosts) holder;
            try {

                JSONObject j = list.getJSONObject(position);
                String tid = j.getString("table_id");
                String tnm = j.getString("table_name");

                itemview.tvnm.setText("" +tnm);
                itemview.tvnm.setTag(tid);

                itemview.cvtbl.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //   if(mBound && AppConst.isMyServiceRunning(POServerService.class,context)) {
                        final Dialog ynd = new Dialog(context);
                        ynd.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        ynd.setCancelable(false);
                        ynd.setContentView(R.layout.dialog_add_no_member);
                        ynd.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                        TextView tvyes = (TextView) ynd.findViewById(R.id.tvyes);
                        TextView tvno = (TextView) ynd.findViewById(R.id.tvno);
                        final EditText etno = (EditText) ynd.findViewById(R.id.etno);
                        etno.setTypeface(AppConst.font_regular(context));

                        tvyes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                try {
                                    JSONObject jsonObject = new JSONObject();
                                    jsonObject.put("rest_id", M.getRestID(context));
                                    jsonObject.put("rest_uniq_id", M.getRestUniqueID(context));
                                    jsonObject.put("table_id", itemview.tvnm.getTag() + "");
                                    jsonObject.put("userid", M.getWaiterid(context));
                                    jsonObject.put("usernm", M.getWaitername(context));
                                    jsonObject.put("order_no", order_no);
                                    jsonObject.put("token_number", token_number);
                                    jsonObject.put("noppl", etno.getText().toString());

                                    JSONObject startOrder = new JSONObject();
                                    startOrder.put("key", WifiHelper.api_start_order);
                                    startOrder.put("msg", jsonObject + "");
                                    jsonObject.put("user_ip",getLocalIp());
                                    sendRequest(startOrder + "");

                                    ynd.dismiss();
                                } catch (JSONException e) {
                                }
                            }
                        });

                        tvno.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                ynd.dismiss();
                            }
                        });

                        ynd.show();
                        // }
                    }
                });
            }catch (JSONException e){}
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemCount() {
            return list.length();
        }

        public class ViewHolderPosts extends RecyclerView.ViewHolder {
            TextView tvnm;
            CardView cvtbl;

            public ViewHolderPosts(View itemView) {
                super(itemView);
                tvnm = (TextView) itemView.findViewById(R.id.tvtablename);
                cvtbl=(CardView)itemView.findViewById(R.id.lltbl);
            }
        }
    }

    private void checkDeviceCode() {
        if(connectionDetector.isConnectingToInternet()) {
            AuthenticationAPI mAuthenticationAPI =APIServiceHeader.createService(context, AuthenticationAPI.class);
            Call<CheckCodePojo> call = mAuthenticationAPI.checkCode(M.getDeviceCode(context));
            call.enqueue(new retrofit2.Callback<CheckCodePojo>() {
                @Override
                public void onResponse(Call<CheckCodePojo> call, Response<CheckCodePojo> response) {
                    if (response.isSuccessful()) {
                        CheckCodePojo pojo = response.body();
                        if (pojo != null) {
                            if(pojo.getMessage()!=null && pojo.getMessage().trim().length()>0)
                                Toast.makeText(context,pojo.getMessage(),Toast.LENGTH_SHORT).show();
                            if(pojo.getUser_exist()!=null && pojo.getUser_exist().equals("no")){
                                logout(null);
                            }
                        }
                    } else {
                        int statusCode = response.code();
                        ResponseBody errorBody = response.errorBody();
                        Log.d(TAG, "error:" + statusCode + " " + errorBody);
                    }
                }

                @Override
                public void onFailure(Call<CheckCodePojo> call, Throwable t) {
                    Log.d(TAG, "fail:" + t.getMessage());
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_more,menu);
        return  super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.ilogout){
            openLogoutDialog();
        }else if(item.getItemId()==R.id.isetting){
            Intent it=new Intent(context,PrinterSettings.class);
            startActivityForResult(it,301);
        }else if(item.getItemId()==R.id.ikot){
            Intent it=new Intent(context,KitchenPrintOrderListActivity.class);
            startActivityForResult(it,301);
        }else if(item.getItemId()==R.id.iitem){
            dialog1 = new Dialog(context, android.R.style.Theme_DeviceDefault_NoActionBar_Fullscreen);
            dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog1.setContentView(R.layout.dialog_progress);
            ImageView ivtopic=(ImageView)dialog1.findViewById(R.id.ivtopic);
            TextView tvtopic=(TextView)dialog1.findViewById(R.id.tvtopic);
            TextView tvcat=(TextView)dialog1.findViewById(R.id.tvcat);
            TextView tvtxt=(TextView)dialog1.findViewById(R.id.tvtxt);
            LinearLayout lltopic=(LinearLayout)dialog1.findViewById(R.id.lltopic);
            lltopic.setVisibility(View.GONE);
            AppConst.animation(context.getString(R.string.progress_dialog_txt), tvtxt,context);
            if(M.getTopic(context)!=null) {
                try {
                    JSONObject j=new JSONObject(M.getTopic(context));
                    if (j != null) {
                        if (j.has("text") && !j.getString("text").isEmpty()) {
                            tvtopic.setText(Html.fromHtml(j.getString("text")));
                            tvtopic.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
                            lltopic.setVisibility(View.VISIBLE);
                            tvtxt.setVisibility(View.VISIBLE);
                        }
                        if (j.has("cat") && !j.getString("cat").isEmpty()) {
                            tvcat.setText(j.getString("cat"));
                            tvcat.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
                            lltopic.setVisibility(View.VISIBLE);
                            tvtxt.setVisibility(View.VISIBLE);
                        }
                        if (j.has("img") && !j.getString("img").isEmpty()) {
                            Picasso.get()
                                    .load(AppConst.topic_img_url + j.getString("img"))
                                    .into(ivtopic);
                            ivtopic.setVisibility(View.VISIBLE);
                            lltopic.setVisibility(View.VISIBLE);
                            tvtxt.setVisibility(View.VISIBLE);
                        }
                    }
                }catch (JSONException e){}
            }
            getCuisine();
            dialog1.show();
        }else if(item.getItemId()==R.id.item_table){
            if(mBound && AppConst.isMyServiceRunning(POServerService.class,context)) {
                sendRequest(WifiHelper.api_active_table);
                sendRequest(WifiHelper.api_inactive_table);
                sendRequest(WifiHelper.api_custom_table);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void unRegister(){
        try {
            if(broadcastReceiver!=null)
                unregisterReceiver(broadcastReceiver);
        }catch (IllegalArgumentException e){}
    }

    @Override
    protected void onDestroy() {
        if(thread.isAlive())
            thread.interrupt();
        unRegister();
        EventBus.getDefault().unregister(this);
        if(dialog!=null && dialog.isShowing())
            dialog.dismiss();
        if(dialog1!=null && dialog1.isShowing())
            dialog1.dismiss();
        if(mBound) {
            unbindService(mConnection);
            mBound=false;
        }
        super.onDestroy();
    }


    public String getLocalIp() {
        return mService.getIPAddress(true);
    }
}
