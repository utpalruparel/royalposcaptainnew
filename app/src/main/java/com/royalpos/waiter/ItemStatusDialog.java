package com.royalpos.waiter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.royalpos.waiter.helper.AppConst;
import com.royalpos.waiter.helper.WifiHelper;
import com.royalpos.waiter.model.DishOrderPojo;

import org.json.JSONException;
import org.json.JSONObject;

import de.greenrobot.event.EventBus;

public class ItemStatusDialog extends Dialog implements View.OnClickListener {

    ImageView ivclose;
    TextView tvname,tvpref,tvstatus0,tvstatus1,tvstatus2,tvstatus4;
    Context context;
    String TAG="ItemStatusDialog";
    DishOrderPojo data;
    int pos;

    public ItemStatusDialog(@NonNull Context context, DishOrderPojo data, int position) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_item_status);
        getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        this.context=context;
        this.data=data;
        this.pos=position;

        ivclose=(ImageView)findViewById(R.id.ivclose);
        tvname=(TextView)findViewById(R.id.tvname);
        tvpref=(TextView)findViewById(R.id.tvpref);
        tvstatus0=(TextView)findViewById(R.id.tvstatus0);
        tvstatus1=(TextView)findViewById(R.id.tvstatus1);
        tvstatus2=(TextView)findViewById(R.id.tvstatus2);
        tvstatus4=(TextView)findViewById(R.id.tvstatus4);

        tvname.setText(data.getDishname());
        tvpref.setText(data.getPrenm());

        if(data.getStatus().equals("0")){
            tvstatus1.setOnClickListener(this);
            tvstatus2.setOnClickListener(this);
            tvstatus4.setOnClickListener(this);

            tvstatus0.setCompoundDrawablesWithIntrinsicBounds(null,null,context.getResources().getDrawable(R.drawable.ic_done),null);
        }else if(data.getStatus().equals("1")){
            tvstatus2.setOnClickListener(this);
            tvstatus4.setOnClickListener(this);

            tvstatus0.setCompoundDrawablesWithIntrinsicBounds(null,null,context.getResources().getDrawable(R.drawable.ic_done),null);
            tvstatus1.setCompoundDrawablesWithIntrinsicBounds(null,null,context.getResources().getDrawable(R.drawable.ic_done),null);
        }else if(data.getStatus().equals("2")){
            tvstatus0.setCompoundDrawablesWithIntrinsicBounds(null,null,context.getResources().getDrawable(R.drawable.ic_done),null);
            tvstatus1.setCompoundDrawablesWithIntrinsicBounds(null,null,context.getResources().getDrawable(R.drawable.ic_done),null);
            tvstatus2.setCompoundDrawablesWithIntrinsicBounds(null,null,context.getResources().getDrawable(R.drawable.ic_done),null);
            tvstatus4.setCompoundDrawablesWithIntrinsicBounds(null,null,context.getResources().getDrawable(R.drawable.ic_done),null);
        }else if(data.getStatus().equals("4")){
            tvstatus2.setOnClickListener(this);

            tvstatus0.setCompoundDrawablesWithIntrinsicBounds(null,null,context.getResources().getDrawable(R.drawable.ic_done),null);
            tvstatus1.setCompoundDrawablesWithIntrinsicBounds(null,null,context.getResources().getDrawable(R.drawable.ic_done),null);
            tvstatus4.setCompoundDrawablesWithIntrinsicBounds(null,null,context.getResources().getDrawable(R.drawable.ic_done),null);
        }

        ivclose.setOnClickListener(this);
    }

    @Override
    public void onClick(final View view) {
        if(view==ivclose)
            dismiss();
        else if(view==tvstatus1 || view==tvstatus2 || view==tvstatus4){
            if(tvstatus2==view) {
                new AlertDialog.Builder(context)
                        .setTitle(context.getString(R.string.status_2) + " Status")
                        .setMessage("Are you sure?")
                        .setNegativeButton(android.R.string.no, null)
                        .setPositiveButton(android.R.string.yes,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        updateStatus(view.getTag().toString());
                                        dismiss();
                                    }
                                }).create().show();
            }else{
                updateStatus(view.getTag().toString());
                dismiss();
            }
        }
    }

    private void updateStatus(String status){
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("status", status);
            jsonObject.put("id", data.getOrderdetailid());
            jsonObject.put("position",pos);
            EventBus.getDefault().post(WifiHelper.api_update_item_status+jsonObject);
        }catch (JSONException e){}
    }
}
