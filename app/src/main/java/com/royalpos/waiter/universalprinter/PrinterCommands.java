package com.royalpos.waiter.universalprinter;

import android.graphics.Bitmap;
import android.graphics.Color;

import java.io.OutputStream;
import java.lang.reflect.Array;
import java.util.BitSet;

public class PrinterCommands {


    public static final byte HT = 0x9;
    public static final byte LF = 0x0A;
    public static final byte CR = 0x0D;
    public static final byte ESC = 0x1B;
    public static final byte DLE = 0x10;
    public static final byte GS = 0x1D;
    public static final byte FS = 0x1C;
    public static final byte STX = 0x02;
    public static final byte US = 0x1F;
    public static final byte CAN = 0x18;
    public static final byte CLR = 0x0C;
    public static final byte EOT = 0x04;

    public static final byte[] INIT = {27, 64};
    public static byte[] FEED_LINE = {10};

    public static byte[] SELECT_FONT_A = {20, 33, 0};

    public static byte[] SET_BAR_CODE_HEIGHT = {29, 104, 100};
    public static byte[] PRINT_BAR_CODE_1 = {29, 107, 2};
    public static byte[] SEND_NULL_BYTE = {0x00};

    public static byte[] SELECT_PRINT_SHEET = {0x1B, 0x63, 0x30, 0x02};
    public static byte[] FEED_PAPER_AND_CUT = {0x1D, 0x56, 66, 0x00};

    public static byte[] SELECT_CYRILLIC_CHARACTER_CODE_TABLE = {0x1B, 0x74, 0x11};

    public static byte[] SELECT_BIT_IMAGE_MODE = {0x1B, 0x2A, 33, -128, 0};
    public static byte[] SET_LINE_SPACING_24 = {0x1B, 0x33, 24};
    public static byte[] SET_LINE_SPACING_30 = {0x1B, 0x33, 30};

    public static byte[] TRANSMIT_DLE_PRINTER_STATUS = {0x10, 0x04, 0x01};
    public static byte[] TRANSMIT_DLE_OFFLINE_PRINTER_STATUS = {0x10, 0x04, 0x02};
    public static byte[] TRANSMIT_DLE_ERROR_STATUS = {0x10, 0x04, 0x03};
    public static byte[] TRANSMIT_DLE_ROLL_PAPER_SENSOR_STATUS = {0x10, 0x04, 0x04};

    public static final byte[] ESC_FONT_COLOR_DEFAULT = new byte[] { 0x1B, 'r',0x00 };
    public static final byte[] FS_FONT_ALIGN = new byte[] { 0x1C, 0x21, 1, 0x1B,
            0x21, 1 };
    public static final byte[] ESC_ALIGN_LEFT = new byte[] { 0x1b, 'a', 0x00 };
    public static final byte[] ESC_ALIGN_RIGHT = new byte[] { 0x1b, 'a', 0x02 };
    public static final byte[] ESC_ALIGN_CENTER = new byte[] { 0x1b, 'a', 0x01 };
    public static final byte[] ESC_CANCEL_BOLD = new byte[] { 0x1B, 0x45, 0 };


    /*********************************************/
    public static final byte[] ESC_HORIZONTAL_CENTERS = new byte[] { 0x1B, 0x44, 20, 28, 00};
    public static final byte[] ESC_CANCLE_HORIZONTAL_CENTERS = new byte[] { 0x1B, 0x44, 00 };
    /*********************************************/

    public static final byte[] ESC_ENTER = new byte[] { 0x1B, 0x4A, 0x40 };
    public static final byte[] PRINTE_TEST = new byte[] { 0x1D, 0x28, 0x41 };

    BitSet dots;

    private boolean shouldPrintColor(int i) {
        boolean z = false;
        if (((i >> 24) & 255) != 255) {
            return false;
        }
        if (((int) (((((double) ((i >> 16) & 255)) * 0.299d) + (((double) ((i >> 8) & 255)) * 0.587d)) + (((double) (i & 255)) * 0.114d))) < 127) {
            z = true;
        }
        return z;
    }

//    private byte[] collectSlice(int i, int i2, int[][] iArr) {
//        byte[] bArr = new byte[]{(byte) 0, (byte) 0, (byte) 0};
//        int i3 = i;
//        int i4 = 0;
//        while (i3 < i + 24 && i4 < 3) {
//            byte b = (byte) 0;
//            for (int i5 = 0; i5 < 8; i5++) {
//                int i6 = i3 + i5;
//                if (i6 < iArr.length) {
//                    b = (byte) (b | ((byte) (shouldPrintColor(0)));
//                }
//            }
//            bArr[i4] = b;
//            i3 += 8;
//            i4++;
//        }
//        return bArr;
//    }

    private void convertArgbToGrayscale(Bitmap bitmap, int i, int i2) {
        this.dots = new BitSet();
        int i3 = 0;
        int i4 = 0;
        while (i3 < i2) {
            int i5 = i4;
            i4 = 0;
            while (i4 < i) {
                try {
                    int pixel = bitmap.getPixel(i4, i3);
                    if (((int) (((((double) Color.red(pixel)) * 0.299d) + (((double) Color.green(pixel)) * 0.587d)) + (((double) Color.blue(pixel)) * 0.114d))) < 55) {
                        this.dots.set(i5);
                    }
                    i5++;
                    i4++;
                } catch (Exception unused) {
                    return;
                }
            }
            i3++;
            i4 = i5;
        }
    }

    private void printImage(OutputStream outputStream, Bitmap bitmap) throws Exception {
        int[][] pixelsSlow = getPixelsSlow(bitmap);
        outputStream.write(SET_LINE_SPACING_24);
        for (int i = 0; i < pixelsSlow.length; i += 24) {
            outputStream.write(SELECT_BIT_IMAGE_MODE);
            outputStream.write(new byte[]{(byte) (pixelsSlow[i].length & 255), (byte) ((65280 & pixelsSlow[i].length) >> 8)});
            for (int i2 = 0; i2 < pixelsSlow[i].length; i2++) {
//                outputStream.write(i, i2, pixelsSlow);
            }
            outputStream.write(FEED_LINE);
        }
        outputStream.write(SET_LINE_SPACING_30);
    }

    private int[][] getPixelsSlow(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int[][] iArr = (int[][]) Array.newInstance(int.class, new int[]{height, width});
        for (int i = 0; i < height; i++) {
            for (int i2 = 0; i2 < width; i2++) {
                int pixel = bitmap.getPixel(i2, i) & 255;
                iArr[i][i2] = (((((bitmap.getPixel(i2, i) >> 16) & 255) << 16) | -16777216) | (((bitmap.getPixel(i2, i) >> 8) & 255) << 8)) | pixel;
            }
        }
        return iArr;
    }
}
