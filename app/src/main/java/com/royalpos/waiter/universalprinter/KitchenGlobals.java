package com.royalpos.waiter.universalprinter;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;

import androidx.annotation.Nullable;

import com.royalpos.waiter.R;
import com.royalpos.waiter.model.M;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import static java.lang.Math.abs;


public class KitchenGlobals {
    // TODO IMPORTANT CONFIG CHANGES
    // ** IMPORTANT CONFIGURATION
    private static String serverRealUrl = "http://www.myserver.com/api";    //Real Server configuration
    private static String serverLocalUrl = "http://localhost:80/api";             //local configuration
    /**
     * Substitute you own sender ID here. This is the project number you got
     * from the API Console, as described in "Getting Started."
     */

    public static String SENDER_ID = "000000";
    public static String base64EncodedPublicKey = "xxxxx";
    //DEBUG MODO
    public static boolean debugModeBoolean = true;  //USE in DEBUG MODE
     //public static boolean debugModeBoolean = false;  //USE in REAL  MODE


    //PRINT FROM WEB ENABLES
    //public static boolean printFronWebEnabled = true;  //USE to activate print fron web
    public static boolean printFronWebEnabled = false;  //USE  to deactivate print fron web

    //GCM Register
    //public static boolean gcmRegisterEnabled = true;  //USE to register device in GCM
    public static boolean gcmRegisterEnabled = false;  // not use gcm services


    //IN APP BILLING
    //public static boolean inAppBillingModeON = true;  //USE in pay app modo on
    public static boolean inAppBillingModeON = false;  //USE in pay app modo off


    private static String internalClassName = "Globals";    // * Tag used on log messages.
    public static int deviceType = 0;                       // * Device type
    //0 Pending config (Default as start)
    //1 NET Print
    //2 RS232   ->Pending
    //3 USB
    //4 BT

    //Google subscriptions
    public static boolean mSubscribedToYearlyUsed = false;


    // Bluetooth  variable Config
    public static BluetoothAdapter mBluetoothAdapter;
    public static BluetoothSocket mmSocket;
    public static BluetoothDevice mmDevice;


    public static String logoProcesed = "";

    public static OutputStream mmOutputStream;
    public static InputStream mmInputStream;


    // NET Printer Config
    public static String deviceIP = "";
    public static int devicePort = 0;
    public static int usbDeviceID = 0;//
    public static int usbVendorID = 0;//
    public static int usbProductID = 0;//
    public static String blueToothDeviceAdress = "";
    public static int blueToothSpinnerSelected = -1;

    public static String tmpDeviceIP = "";
    public static int tmpDevicePort = 0;
    public static int tmpUsbDeviceID = 0;
    public static int tmpUsbVendorID = 0;
    public static int tmpUsbProductID = 0;
    public static String link_code = "";
    public static String tmpBlueToothDeviceAdress = "";


    public static String getServerUrl() {
        String actualServer = "";
        if (debugModeBoolean) {
            actualServer = serverLocalUrl;
        } else {
            actualServer = serverRealUrl;
        }

        return actualServer;
    }

    public static String server_registerPrinter = "registerPrinter";
    public static String server_getData = "getData";
    public static String regid = "";
    public static String picturePath = "";


    public static void savePreferences(SharedPreferences sharedPrefs) {
        Log.i(internalClassName, "savePreferences");

        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putString("kusbDeviceID", String.valueOf(usbDeviceID));
        editor.putString("kusbVendorID", String.valueOf(usbVendorID));//
        editor.putString("kusbProductID", String.valueOf(usbProductID));//


        editor.putString("kblueToothDeviceAdress", String.valueOf(blueToothDeviceAdress));


        editor.putString("kdeviceType", String.valueOf(deviceType));
        //Ethernet
        editor.putString("kdeviceIP", String.valueOf(deviceIP));
        editor.putString("kdevicePort", String.valueOf(devicePort));

        editor.putString("klink_code", String.valueOf(link_code));
        editor.putString("kpicturePath", String.valueOf(picturePath));
        editor.putString("klogoProcesed", logoProcesed);

        Log.i(internalClassName, "               deviceType=" + String.valueOf(deviceType));
        Log.i(internalClassName, "               deviceIP=" + String.valueOf(deviceIP));
        Log.i(internalClassName, "               devicePort=" + String.valueOf(devicePort));
        Log.i(internalClassName, "               usbDeviceID=" + String.valueOf(usbDeviceID));
        Log.i(internalClassName, "               blueToothDeviceAdress=" + String.valueOf(blueToothDeviceAdress));


        editor.commit();

    }

    public static void loadPreferences(SharedPreferences sharedPrefs) {
        Log.i(internalClassName, "loadPreferences");

        usbDeviceID = mathIntegerFromString(sharedPrefs.getString("kusbDeviceID", "0"), 0);
        usbVendorID = mathIntegerFromString(sharedPrefs.getString("kusbVendorID", "0"), 0);
        usbProductID = mathIntegerFromString(sharedPrefs.getString("kusbProductID", "0"), 0);


        link_code = sharedPrefs.getString("klink_code", "");
        picturePath = sharedPrefs.getString("kpicturePath", "");
        blueToothDeviceAdress = sharedPrefs.getString("kblueToothDeviceAdress", "x:x:x:x");

        deviceType = mathIntegerFromString(sharedPrefs.getString("kdeviceType", "0"), 0);
        //Ethernet
        deviceIP = sharedPrefs.getString("kdeviceIP", "0.0.0.0");
        devicePort = mathIntegerFromString(sharedPrefs.getString("kdevicePort", "9100"), 9100);
        logoProcesed = sharedPrefs.getString("klogoProcesed", "");


        Log.i(internalClassName, "               deviceType=" + String.valueOf(deviceType));
        Log.i(internalClassName, "               deviceIP=" + String.valueOf(deviceIP));
        Log.i(internalClassName, "               devicePort=" + String.valueOf(devicePort));
        Log.i(internalClassName, "               usbDeviceID=" + String.valueOf(usbDeviceID));
        Log.i(internalClassName, "               blueToothDeviceAdress=" + String.valueOf(blueToothDeviceAdress));


    }




    public static Integer mathIntegerFromString(String inti, Integer defi) {
        //used for preferences
        Integer returi = defi;
        if (!inti.equals("")) {
            try {
                returi = Integer.valueOf(inti);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }

        return returi;

    }




    public static String prepareDataToPrint(String data) {
        //ESC POS Comamnds convertions


        data = data.replace("$bold$", "·27·E·1·");
        data = data.replace("$unbold$", "·27·E·0·");


        data = data.replace("$intro$", "·13··10·");
        data = data.replace("$cut$", "·27·m");
        data = data.replace("$cutt$", "·27·i");

        data = data.replace("$al_left$", "·27·a·0·");
        data = data.replace("$al_center$", "·27·a·1·");
        data = data.replace("$al_right$", "·27·a·2·");

        data = data.replace("$small$", "·27·!·1·");
        data = data.replace("$smallh$", "·27·!·17·");
        data = data.replace("$smallw$", "·27·!·33·");
        data = data.replace("$smallhw$", "·27·!·49·");

        data = data.replace("$smallu$", "·27·!·129·");
        data = data.replace("$smalluh$", "·27·!·145·");
        data = data.replace("$smalluw$", "·27·!·161·");
        data = data.replace("$smalluhw$", "·27·!·177·");


        data = data.replace("$big$", "·27·!·0·");
        data = data.replace("$bigh$", "·27·!·16·");
        data = data.replace("$bigw$", "·27·!·32·");
        data = data.replace("$bighw$", "·27·!·48·");
        data = data.replace("$bigl$", "·27·!·12·");
        data = data.replace("$bigu$", "·27·!·128·");
        data = data.replace("$biguh$", "·27·!·144·");
        data = data.replace("$biguw$", "·27·!·160·");
        data = data.replace("$biguhw$", "·27·!·176·");

        data = data.replace("$drawer$", "·27··112··48··200··100·");
        data = data.replace("$drawer2$", "·27··112··49··200··100·");

        data = data.replace("$enter$", "·13··10·");

        data = data.replace("$letrap$", "·27·!·1·");
        data = data.replace("$letraph$", "·27·!·17·");
        data = data.replace("$letrapw$", "·27·!·33·");
        data = data.replace("$letraphw$", "·27·!·49·");

        data = data.replace("$letrapu$", "·27·!·129·");
        data = data.replace("$letrapuh$", "·27·!·145·");
        data = data.replace("$letrapuw$", "·27·!·161·");
        data = data.replace("$letrapuhw$", "·27·!·177·");


        data = data.replace("$letrag$", "·27·!·0·");
        data = data.replace("$letragh$", "·27·!·16·");
        data = data.replace("$letragw$", "·27·!·32·");
        data = data.replace("$letraghw$", "·27·!·48·");

        data = data.replace("$letragu$", "·27·!·128·");
        data = data.replace("$letraguh$", "·27·!·144·");
        data = data.replace("$letraguw$", "·27·!·160·");
        data = data.replace("$letraguhw$", "·27·!·176·");


        data = data.replace("$letracorte$", "·27·m");
        data = data.replace("$LETRACORTE$", "·27·m");


        data = data.replace("$ENTER$", "·13··10·");

        data = data.replace("$LETRAP$", "·27·!·1·");
        data = data.replace("$LETRAPH$", "·27·!·17·");
        data = data.replace("$LETRAPW$", "·27·!·33·");
        data = data.replace("$LETRAPHW$", "·27·!·49·");

        data = data.replace("$LETRAPU$", "·27·!·129·");
        data = data.replace("$LETRAPUH$", "·27·!·145·");
        data = data.replace("$LETRAPUW$", "·27·!·161·");
        data = data.replace("$LETRAPUHW$", "·27·!·177·");


        data = data.replace("$LETRAG$", "·27·!·0·");
        data = data.replace("$LETRAGH$", "·27·!·16·");
        data = data.replace("$LETRAGW$", "·27·!·32·");
        data = data.replace("$LETRAGHW$", "·27·!·48·");

        data = data.replace("$LETRAGU$", "·27·!·128·");
        data = data.replace("$LETRAGUH$", "·27·!·144·");
        data = data.replace("$LETRAGUW$", "·27·!·160·");
        data = data.replace("$LETRAGUHW$", "·27·!·176·");



        data = data.replace("á", "·160·");
        data = data.replace("à", "·133·");
        data = data.replace("â", "·131·");
        data = data.replace("ä", "·132·");
        data = data.replace("å", "·134·");

        data = data.replace("Á", "·193·");
        data = data.replace("À", "·192·");
        data = data.replace("Â", "·194·");
        data = data.replace("Ä", "·142·");
        data = data.replace("Å", "·143·");


        data = data.replace("é", "·130·");
        data = data.replace("è", "·138·");
        data = data.replace("ê", "·136·");
        data = data.replace("ë", "·137·");
        //data = data.replace("","··");


        data = data.replace("É", "·144·");
        data = data.replace("È", "··");
        data = data.replace("Ê", "··");
        data = data.replace("Ë", "··");

        data = data.replace("ñ", "·164·");
        data = data.replace("Ñ", "·165·");

        //data = data.replace("","··");


        data = data.replace("í", "··");
        data = data.replace("ì", "·141·");
        data = data.replace("î", "·140·");
        data = data.replace("ï", "·139·");
        //data = data.replace("","··");


        data = data.replace("ó", "·149·");
        data = data.replace("ò", "··");
        data = data.replace("ô", "·147·");
        data = data.replace("ö", "·148·");
        //data = data.replace("","··");

        data = data.replace("Ó", "··");
        data = data.replace("Ò", "··");
        data = data.replace("Ô", "··");
        data = data.replace("Ö", "·153·");
        //data = data.replace("","··");

        data = data.replace("ú", "··");
        data = data.replace("ù", "·151·");
        data = data.replace("û", "··");
        data = data.replace("ü", "·129·");
        //data = data.replace("","··");


        data = data.replace("ú", "··");
        data = data.replace("ù", "·151·");
        data = data.replace("û", "··");
        data = data.replace("ü", "·129·");

        //data = data.replace("..", "");

        for (int l = 0; l <= 255; l++) {
            data = data.replace("·" + String.valueOf(l) + "·", String.valueOf(((char) l)));
            data = data.replace("$codepage" + String.valueOf(l) + "$",  String.valueOf(((char) 27)) + "t" + String.valueOf(((char) l)));
        }

        Log.d("here----", ""+data);

        return data;
    }


    // Get var from data text
    public static String getVble(String data, String param, String defaultValue) {
        String vble = defaultValue;
        int found_start = data.indexOf("$" + param + "=");
        if (found_start > -1) {
            found_start = found_start + ("$" + param + "=").length();
            String substr = data.substring(found_start);
            int found_end = substr.indexOf("$");
            vble = data.substring(found_start, found_start + found_end);
        }
        return vble;
    }


    //Convert string in Byte asccii to print
    public static byte[] stringToBytesASCII(String str) {
        byte[] b = new byte[str.length()];
        for (int i = 0; i < b.length; i++) {
            b[i] = (byte) str.charAt(i);
        }
        Log.d("bytes=-===", ""+b.toString());
        return b;
    }




    // generate demo text
    public static String getDemoText(Resources resources, Context context) {
        String dataToPrint = "";



        StringBuilder textData = new StringBuilder();
        textData.append("$codepage3$");
            textData.append("$al_center$$al_center$");

            textData.append("$big$"+M.getBrandName(context)+"$intro$");
            textData.append(M.getRestAddress(context)+"$intro$");
            textData.append("Phone: "+ M.getRestPhoneNumber(context)+"$intro$");
            textData.append("GST# "+ M.getGST(context)+"$intro$");
            textData.append("--------------------------------------$intro$");
            textData.append("Order By : "+ M.getWaitername(context)+"\t $intro$");


        textData.append(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"\n");
        textData.append("ORDER # 1025555555555"+"\n");

        textData.append("--------------------------------------\n");
        textData.append("TOKEN # "+203+"\t"+ "dine in" +"\n");



        textData.append("--------------------------------------\n");

        textData.append("Order comment \t");
        textData.append("need spicy" + "\n");

        textData.append("--------------------------------------\n");


        String qty = "1";
        textData.append(padLine(qty + "    CHEESE MAYO FRIES", 120 + "", 40, " ") + "\n");
        textData.append(padLine(qty + "    CHEESE MAYO FRIES", 120 + "", 40, " ") + "\n");
        textData.append(padLine(qty + "    CHEESE MAYO FRIES", 120 + "", 40, " ") + "\n");
        textData.append(padLine(qty + "    CHEESE MAYO FRIES", 120 + "", 40, " ") + "\n");



        textData.append("--------------------------------------\n");


        textData.append("SUBTOTAL                    "+480+"\n");
        textData.append("CGST @"+6+"%                    "+29+"\n");
        textData.append("SGST @"+6+"%                    "+29+"\n");



        textData.append("TOTAL       538"+"\n");



        textData.append("CASH                         600"+"\n");
        textData.append("CHANGE                     "+62+"\n");

        textData.append("--------------------------------------\n");


        textData.append("Thank you  "+"Visit Again\n");




//        dataToPrint = dataToPrint + "$codepage0$$intro$";
//        dataToPrint = dataToPrint + Globals.getImageDataPosPrinterLogo(resources);
//
//        dataToPrint = dataToPrint + "$bighw$PosPrinterDriver.com$intro$$intro$";
//        dataToPrint = dataToPrint + "$big$ ESC/POS Types:$intro$ ";
//        dataToPrint = dataToPrint + "$small$ ·36·small·36· ->Small type letter$intro$";
//        dataToPrint = dataToPrint + "$big$ ·36·big·36· ->Big type letter$intro$";
//
//
//        dataToPrint = dataToPrint + "$big$ ESC/POS functions:$intro$ ";
//        dataToPrint = dataToPrint + "$big$ ·36·cut·36·: Cutt paper$intro$ ";
//        dataToPrint = dataToPrint + "$big$ ·36·cutt·36·: Cutt total paper$intro$ ";
//        dataToPrint = dataToPrint + "$big$ ·36·drawer·36·: Open drawer 1 $intro$ ";
//        dataToPrint = dataToPrint + "$big$ ·36·drawer2·36·: Open drawer 2 $intro$ ";
//
//        dataToPrint = dataToPrint + "Specials characters capabilities $intro$";
        //test to find page codes
        //for (int l = 50; l <= 255; l++) {
        //  datos=datos+    "valor "+ String.valueOf(l) + "= ·" + String.valueOf(l) + "·";
        //}
     /*   String defTextToPrint="ABCDEFGHIJKLMNOPQRSTUVWXYZ $intro$aáàâäå  AÁÀÂÄÅ $intro$eéèêë  " +
                "EÉÈÊË$intro$iíìîï IÍÌÎÏ $intro$oóòôö OÓÒÔÖ $intro$uúùûü UÚÙÛÜ $intro$ñ Ñ €  $intro$";



        dataToPrint = dataToPrint + "$codepage0$$intro$";
        dataToPrint = dataToPrint + "Select code page 0 PC437 [U.S.A., Standard Europe]$intro$";
        dataToPrint = dataToPrint + defTextToPrint;

        dataToPrint = dataToPrint + "$codepage1$$intro$";
        dataToPrint = dataToPrint + "Select code page 1 [Katakana]$intro$";
        dataToPrint = dataToPrint + defTextToPrint;

        dataToPrint = dataToPrint + "$codepage2$$intro$";
        dataToPrint = dataToPrint + "Select code page 2 PC850 [Multilingual]$intro$";
        dataToPrint = dataToPrint + defTextToPrint;

        dataToPrint = dataToPrint + "$codepage3$$intro$";
        dataToPrint = dataToPrint + "Select code page 3 PC860 [Portuguese]$intro$";
        dataToPrint = dataToPrint + defTextToPrint;

        dataToPrint = dataToPrint + "$codepage4$$intro$";
        dataToPrint = dataToPrint + "Select code page 4 PC863 [Canadian-French]$intro$";
        dataToPrint = dataToPrint + defTextToPrint;

        dataToPrint = dataToPrint + "$codepage5$$intro$";
        dataToPrint = dataToPrint + "Select code page 5 PC865 [Nordic]$intro$";
        dataToPrint = dataToPrint + defTextToPrint;

        dataToPrint = dataToPrint + "$codepage17$$intro$";
        dataToPrint = dataToPrint + "Select code page 17 PC866 [Cyrillic]$intro$";
        dataToPrint = dataToPrint + defTextToPrint;

        //0 PC437 [U.S.A., Standard Europe]
        //1 Katakana
       // 2 PC850 [Multilingual]
       // 3 PC860 [Portuguese]
       // 4 PC863 [Canadian-French]
       // 5 PC865 [Nordic]
       // 17 PC866 [Cyrillic #2]
        dataToPrint = dataToPrint + "$codepage31$$intro$";
        dataToPrint = dataToPrint + "Select code page 31 HEBREW $intro$";
        dataToPrint = dataToPrint + defTextToPrint;


//


        if (Globals.picturePath != "") {
            dataToPrint = dataToPrint + Globals.getImageData();
        }
        Log.e(internalClassName, "dataToPrint" + dataToPrint);*/

        textData.append("$intro$$intro$$intro$$intro$$cutt$$intro$");

        dataToPrint = textData.toString();
        return dataToPrint;
    }

    protected static String padLine(@Nullable String partOne, @Nullable String partTwo, int columnsPerLine, String str){
        if(partOne == null) {partOne = "";}
        if(partTwo == null) {partTwo = "";}
        if(str == null) {str = " ";}
        String concat;
        if((partOne.length() + partTwo.length()) > columnsPerLine) {
            concat = partOne + str + partTwo;
        } else {
            int padding = columnsPerLine - (partOne.length() + partTwo.length());
            concat = partOne + repeat(str, padding) + partTwo;
        }
        return concat;
    }

    protected static String repeat(String str, int i){
        return new String(new char[i]).replace("\0", str);
    }

    //get actual logo data
    public static String getImageData() {

        if (KitchenGlobals.picturePath !=null && KitchenGlobals.picturePath.trim().length()>0) {
            if (KitchenGlobals.logoProcesed.length() > 0) {
                //response with calculated logo data text
                return KitchenGlobals.logoProcesed;
            } else {
                Log.e(internalClassName, "CALCULANDO LOGO:");

                String INITIALIZE_PRINTER = "·27··64·";             //Chr(&H1B) & Chr(&H40);
                String PRINT_AND_FEED_PAPER = "·10·";               //Chr(&HA);
                String CMDSELECT_BIT_IMAGE_MODE = "·27··42·";       //Chr(&H1B) & Chr(&H2A);
                String SET_LINE_SPACING = "·27··51·";               //Chr(&H1B) & Chr(&H33);
                String SETLINESPACING24 = SET_LINE_SPACING + "·24·";

                BitmapFactory.Options options_load = new BitmapFactory.Options();
                options_load.inScaled = false;
                Bitmap miImagetest = null;
                Bitmap miImage = null;
                if(picturePath==null)
                    picturePath="";

                if (picturePath.trim().length()==0) {
                    miImage = BitmapFactory.decodeResource(Resources.getSystem(), R.drawable.rpbanner,
                            options_load);

                } else {
                    miImagetest = BitmapFactory.decodeFile(KitchenGlobals.picturePath);


                    if ((miImagetest.getHeight() > 500) || miImagetest.getWidth() > 500) {
                        Bitmap resizedBitmap = null;
                        int px = miImagetest.getWidth();
                        int py = miImagetest.getHeight();
                        int pp = 0;
                        if (px > py) {
                            px = 500;
                            pp = py * px / miImagetest.getWidth();
                            py = pp;
                        } else {
                            py = 500;
                            pp = px * py / miImagetest.getHeight();
                            px = pp;
                        }
                        miImage = Bitmap.createScaledBitmap(miImagetest, px, py, false);
                    } else {
                        miImage = miImagetest;

                    }
                }

                int image_h = miImage.getHeight();
                int p_Width = miImage.getWidth();
        /*
         int[] imageBitsInvertidos = new int[p_Width * image_h];
         for (int p_Y = 0; p_Y < image_h; p_Y++) {
            for (int p_X = 0; p_X < p_Width; p_X++) {
                int p_PosicionInvertida = (image_h - p_Y - 1) * p_Width + p_X;
                imageBitsInvertidos[p_PosicionInvertida] = miImage.getPixel(p_X, p_Y);
            }
        }*/
                String widthLSB = "";
                String widthMSB = "";
                String hexdata = "0000" + Integer.toHexString(p_Width);
                widthLSB = hexdata.substring(hexdata.length() - 2, hexdata.length());
                widthMSB = hexdata.substring(hexdata.length() - 4, hexdata.length() - 2);


                String SELECTBITIMAGEMODE = CMDSELECT_BIT_IMAGE_MODE + "·33··" + Integer.parseInt(widthLSB, 16) + "··" + Integer.parseInt(widthMSB, 16) + "·";// & Chr(33) & Chr(widthLSB) & Chr(widthMSB)


                String p_TxtPrint = INITIALIZE_PRINTER;
                p_TxtPrint = p_TxtPrint + SETLINESPACING24;
                String p_DatatoPrint = "";
                int offset = 0;
                while (offset < image_h) {
                    int imageDataLineIndex = 0;
                    int p_BytesAncho = 3 * p_Width - 1;
                    int[] imageDataLine = new int[p_BytesAncho + 10];//(3 * (image_w + 10))];
                    for (int X = 0; X < p_Width; X++) {
                        //' Remember, 24 dots = 24 bits = 3 bytes.
                        //' The ' k ' variable keeps track of which of those
                        //' three bytes that we' re currently scribbling into.
                        for (int K = 0; K <= 2; K++) {
                            int slice = 0;
                            String strSlice = "";
                            //' A byte is 8 bits. The ' b ' variable keeps track
                            //' of which bit in the byte we' re recording.
                            for (int b = 0; b <= 7; b++) {
                                //' Calculate the y position that we' re currently
                                //' trying to draw. We take our offset, divide it
                                //' by 8 so we' re talking about the y offset in
                                //' terms of bytes, add our current ' k ' byte
                                //' offset to that, multiple by 8 to get it in terms
                                //' of bits again, and add our bit offset to it.
                                int zz2 = abs(offset / 8);
                                int y = ((zz2 + K) * 8) + b;
                                int I = (y * (p_Width)) + (X);
                                //int y = (((abs(offset / 8)) + K) * 8) + b;
                                //' Calculate the location of the pixel we want in the bit array  It' ll be at(y * width) + x.
                                // int I = (y * p_Width) + X;
                                // ' If the image (or this stripe of the image)                         ' is shorter than 24 dots, pad with zero.
                                boolean v = false;

                                if (I <= (image_h * p_Width)) {
                                    if (y < image_h) {
                                        int midato = miImage.getPixel(X, y);
                                        int alfa = (midato >> 24) & 0xff;     //bitwise shifting

                                        int R = (midato >> 16) & 0xff;     //bitwise shifting
                                        int G = (midato >> 8) & 0xff;
                                        int B = midato & 0xff;
                                        double total = abs(0.2126 * R + 0.7152 * G + 0.0722 * B);

                                        if (total > 80) {
                                            v = true;
                                        }
                                    } else {
                                        v = true;
                                    }
                                } else {
                                    v = true;
                                }
                                if (v == true) {
                                    strSlice = strSlice + "0";
                                } else {
                                    strSlice = strSlice + "1";
                                }
                            }
                            imageDataLine[imageDataLineIndex + K] = Integer.parseInt(strSlice, 2);
                            strSlice = "";
                        }
                        imageDataLineIndex = imageDataLineIndex + 3;
                    }
                    String p_dataBytes = "";
                    for (int XX = 0; XX < imageDataLineIndex; XX++) {
                        p_dataBytes = p_dataBytes + String.valueOf(((char) imageDataLine[XX]));
                        //p_dataBytes = p_dataBytes + "·" + imageDataLine[XX] + "·";
                    }
                    p_DatatoPrint = p_DatatoPrint + SELECTBITIMAGEMODE + p_dataBytes + PRINT_AND_FEED_PAPER;

                    offset = offset + 24;
                }

                p_TxtPrint = p_TxtPrint + p_DatatoPrint + SETLINESPACING24 + INITIALIZE_PRINTER;
                //'& vbCrLf & SELECT_BIT_IMAGE_MODE & Chr(33) & Chr(0) & Chr(0) & Chr(0) & setLineSpacing30Dots & "hello World! " & vbCrLf
                p_TxtPrint = p_TxtPrint + "·29··60·";//& Chr( & H1D)&Chr( & H3C)
                KitchenGlobals.logoProcesed = p_TxtPrint;
                Log.e(internalClassName, "CALCULANDO LOGO FIN:");
                return p_TxtPrint;
            }
        } else {
            return "";
        }
    }


    public static String getImageDataPosPrinterLogo(Resources resources) {
        String INITIALIZE_PRINTER = "·27··64·";//Chr(&H1B) & Chr(&H40);
        String PRINT_AND_FEED_PAPER = "·10·";//Chr(&HA);
        String CMDSELECT_BIT_IMAGE_MODE = "·27··42·";//Chr(&H1B) & Chr(&H2A);
        String SET_LINE_SPACING = "·27··51·";//Chr(&H1B) & Chr(&H33);
        String SETLINESPACING24 = SET_LINE_SPACING + "·24·";

        BitmapFactory.Options options_load = new BitmapFactory.Options();
        options_load.inScaled = false;
        Bitmap miImagetest = null;
        Bitmap miImage = null;

        miImage = BitmapFactory.decodeResource(resources, R.drawable.rpbanner, options_load);


        int image_h = miImage.getHeight();
        int p_Width = miImage.getWidth();


        String widthLSB = "";
        String widthMSB = "";
        String hexdata = "0000" + Integer.toHexString(p_Width);
        widthLSB = hexdata.substring(hexdata.length() - 2, hexdata.length());
        widthMSB = hexdata.substring(hexdata.length() - 4, hexdata.length() - 2);


        String SELECTBITIMAGEMODE = CMDSELECT_BIT_IMAGE_MODE + "·33··" + Integer.parseInt(widthLSB, 16) + "··" + Integer.parseInt(widthMSB, 16) + "·";// & Chr(33) & Chr(widthLSB) & Chr(widthMSB)


        String p_TxtPrint = INITIALIZE_PRINTER;
        p_TxtPrint = p_TxtPrint + SETLINESPACING24;
        String p_DatatoPrint = "";
        int offset = 0;
        while (offset < image_h) {
            int imageDataLineIndex = 0;
            int p_BytesAncho = 3 * p_Width - 1;
            int[] imageDataLine = new int[p_BytesAncho + 10];//(3 * (image_w + 10))];
            for (int X = 0; X < p_Width; X++) {
                //' Remember, 24 dots = 24 bits = 3 bytes.
                //' The ' k ' variable keeps track of which of those
                //' three bytes that we' re currently scribbling into.
                for (int K = 0; K <= 2; K++) {
                    int slice = 0;
                    String strSlice = "";
                    //' A byte is 8 bits. The ' b ' variable keeps track
                    //' of which bit in the byte we' re recording.
                    for (int b = 0; b <= 7; b++) {
                        //' Calculate the y position that we' re currently
                        //' trying to draw. We take our offset, divide it
                        //' by 8 so we' re talking about the y offset in
                        //' terms of bytes, add our current ' k ' byte
                        //' offset to that, multiple by 8 to get it in terms
                        //' of bits again, and add our bit offset to it.
                        int zz2 = abs(offset / 8);
                        int y = ((zz2 + K) * 8) + b;
                        int I = (y * (p_Width)) + (X);
                        //int y = (((abs(offset / 8)) + K) * 8) + b;
                        //' Calculate the location of the pixel we want in the bit array  It' ll be at(y * width) + x.
                        // int I = (y * p_Width) + X;
                        // ' If the image (or this stripe of the image)                         ' is shorter than 24 dots, pad with zero.
                        boolean v = false;

                        if (I <= (image_h * p_Width)) {
                            //Log.e(internalClassName, "IMAGEN= getImageData x,y:" + String.valueOf(X) + "," + String.valueOf(y));
                            if (y < image_h) {
                                int midato = miImage.getPixel(X, y);
                                int alfa = (midato >> 24) & 0xff;     //bitwise shifting

                                int R = (midato >> 16) & 0xff;     //bitwise shifting
                                int G = (midato >> 8) & 0xff;
                                int B = midato & 0xff;
                                double total = abs(0.2126 * R + 0.7152 * G + 0.0722 * B);

                                // Log.e(internalClassName, "valores:  total===" + String.valueOf(total)  + "->%="  );
                                if (total > 80) {
                                    v = true;
                                }
                            } else {
                                v = true;
                            }
                        } else {
                            v = true;
                        }
                        if (v == true) {
                            strSlice = strSlice + "0";
                        } else {
                            strSlice = strSlice + "1";
                        }
                    }

                    imageDataLine[imageDataLineIndex + K] = Integer.parseInt(strSlice, 2);
                }
                imageDataLineIndex = imageDataLineIndex + 3;
            }
            String p_dataBytes = "";
            for (int XX = 0; XX < imageDataLineIndex; XX++) {
                p_dataBytes = p_dataBytes + String.valueOf(((char) imageDataLine[XX]));
            }
            p_DatatoPrint = p_DatatoPrint + SELECTBITIMAGEMODE + p_dataBytes + PRINT_AND_FEED_PAPER;

            offset = offset + 24;
        }

        p_TxtPrint = p_TxtPrint + p_DatatoPrint + SETLINESPACING24 + INITIALIZE_PRINTER;
        p_TxtPrint = p_TxtPrint + "·29··60·";//& Chr( & H1D)&Chr( & H3C)

        return p_TxtPrint;
    }


    public static String SaveImage(Bitmap finalBitmap, String name) {

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/saved_images");
        myDir.mkdirs();
        String fname = "Image-" + name + ".jpg";
        File file = new File(myDir, fname);
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return file.toString();
    }

    public static boolean get_status_licence() {
        return mSubscribedToYearlyUsed;
    }
}



/*  public static Bitmap toGrayscale(Bitmap bmpOriginal)
    {
        int width, height;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();

        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);
        c.drawBitmap(bmpOriginal, 0, 0, paint);
        return bmpGrayscale;
    }
*/
/*

    public static byte[] stringToBytesASCIIOK(String str) {
        String TAG = "posprinterdriver_AsyncNetPrint";

//        byte[] b = new byte[0];
//        try {
//            b = str.getBytes("US-ASCII");
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }


        byte[] b = new byte[str.length()];
        if (str.length() > 0) {
            Log.i(TAG, "                 Caracter de 2 partes=" + str + " ->parte1=" + str.charAt(0) + "->" + str.charAt(1));

        }
//      for (int i = 0; i < b.length; i++) {
//
//          b[i] = (byte) str.charAt(i);
//
//
//      }
        for (int i = 0; i < b.length; i++) {

            b[i] = (byte) str.charAt(i);
            Log.i(TAG, "                CAdena encontrada=" + i + " ->parte1=" + str.charAt(i) + " ->parte1=" + (int) str.charAt(i));

        }

        return b;

    }
*/
