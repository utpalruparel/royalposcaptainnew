package com.royalpos.waiter.universalprinter;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Environment;
import android.util.Log;

import androidx.annotation.Nullable;

import com.royalpos.waiter.model.M;
import com.royalpos.waiter.print.PrintFormat;
import com.royalpos.waiter.receipt.ReceiptBuilder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static java.lang.Math.abs;


public class Globals {
    // TODO IMPORTANT CONFIG CHANGES
    // ** IMPORTANT CONFIGURATION
    private static String serverRealUrl = "http://www.myserver.com/api";    //Real Server configuration
    private static String serverLocalUrl = "http://localhost:80/api";             //local configuration
    /**
     * Substitute you own sender ID here. This is the project number you got
     * from the API Console, as described in "Getting Started."
     */

    public static String SENDER_ID = "000000";
    public static String base64EncodedPublicKey = "xxxxx";
    //DEBUG MODO
    public static boolean debugModeBoolean = true;  //USE in DEBUG MODE
    //public static boolean debugModeBoolean = false;  //USE in REAL  MODE


    //PRINT FROM WEB ENABLES
    //public static boolean printFronWebEnabled = true;  //USE to activate print fron web
    public static boolean printFronWebEnabled = false;  //USE  to deactivate print fron web

    //GCM Register
    //public static boolean gcmRegisterEnabled = true;  //USE to register device in GCM
    public static boolean gcmRegisterEnabled = false;  // not use gcm services


    //IN APP BILLING
    //public static boolean inAppBillingModeON = true;  //USE in pay app modo on
    public static boolean inAppBillingModeON = false;  //USE in pay app modo off


    private static String internalClassName = "Globals";    // * Tag used on log messages.
    public static int deviceType = 0;                       // * Device type
    //0 Pending config (Default as start)
    //1 NET Print
    //2 RS232   ->Pending
    //3 USB
    //4 BT
    //5 SUNMI
    //6 MSWIPE (wise pos plus)
    //7 EPSON
    //8 mswipe (wise pos neo)
    //Google subscriptions
    public static boolean mSubscribedToYearlyUsed = false;

    public static String starprinter = "";
    // Bluetooth  variable Config
    public static BluetoothAdapter mBluetoothAdapter;
    public static BluetoothSocket mmSocket;
    public static BluetoothDevice mmDevice;


    public static String logoProcesed = "";

    public static OutputStream mmOutputStream;
    public static InputStream mmInputStream;


    // NET Printer Config
    public static String deviceIP = "";
    public static int devicePort = 0;
    public static int usbDeviceID = 0;
    public static int usbVendorID = 0;
    public static int usbProductID = 0;
    public static String blueToothDeviceAdress = "";
    public static int blueToothSpinnerSelected = -1;

    public static String tmpDeviceIP = "";
    public static int tmpDevicePort = 0;
    public static int tmpUsbDeviceID = 0;
    public static int tmpUsbVendorID = 0;
    public static int tmpUsbProductID = 0;
    public static String link_code = "";
    public static String tmpBlueToothDeviceAdress = "";


    public static String getServerUrl() {
        String actualServer = "";
        if (debugModeBoolean) {
            actualServer = serverLocalUrl;
        } else {
            actualServer = serverRealUrl;
        }

        return actualServer;
    }

    public static String server_registerPrinter = "registerPrinter";
    public static String server_getData = "getData";
    public static String regid = "";
    public static String picturePath = "";

    public static Bitmap imgbm = null;

    public static void savePreferences(SharedPreferences sharedPrefs) {
        Log.i(internalClassName, "savePreferences");


        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putString("usbDeviceID", String.valueOf(usbDeviceID));
        editor.putString("usbVendorID", String.valueOf(usbVendorID));
        editor.putString("usbProductID", String.valueOf(usbProductID));


        editor.putString("blueToothDeviceAdress", String.valueOf(blueToothDeviceAdress));


        editor.putString("deviceType", String.valueOf(deviceType));
        //Ethernet
        editor.putString("deviceIP", String.valueOf(deviceIP));
        editor.putString("devicePort", String.valueOf(devicePort));

        editor.putString("link_code", String.valueOf(link_code));
        editor.putString("picturePath", String.valueOf(picturePath));
        editor.putString("logoProcesed", logoProcesed);

        Log.i(internalClassName, "               deviceType=" + String.valueOf(deviceType));
        Log.i(internalClassName, "               deviceIP=" + String.valueOf(deviceIP));
        Log.i(internalClassName, "               devicePort=" + String.valueOf(devicePort));
        Log.i(internalClassName, "               usbDeviceID=" + String.valueOf(usbDeviceID));
        Log.i(internalClassName, "               blueToothDeviceAdress=" + String.valueOf(blueToothDeviceAdress));


        editor.commit();

    }

    public static void savePreferenceskitchen(SharedPreferences sharedPrefs) {
        Log.i(internalClassName, "savePreferences");


        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putString("usbDeviceID", String.valueOf(usbDeviceID));
        editor.putString("usbVendorID", String.valueOf(usbVendorID));
        editor.putString("usbProductID", String.valueOf(usbProductID));


        editor.putString("blueToothDeviceAdress", String.valueOf(blueToothDeviceAdress));


        editor.putString("deviceType", String.valueOf(deviceType));
        //Ethernet
        editor.putString("deviceIP", String.valueOf(deviceIP));
        editor.putString("devicePort", String.valueOf(devicePort));

        editor.putString("link_code", String.valueOf(link_code));
        editor.putString("picturePath", String.valueOf(picturePath));
        editor.putString("logoProcesed", logoProcesed);

        Log.i(internalClassName, "               deviceType=" + String.valueOf(deviceType));
        Log.i(internalClassName, "               deviceIP=" + String.valueOf(deviceIP));
        Log.i(internalClassName, "               devicePort=" + String.valueOf(devicePort));
        Log.i(internalClassName, "               usbDeviceID=" + String.valueOf(usbDeviceID));
        Log.i(internalClassName, "               blueToothDeviceAdress=" + String.valueOf(blueToothDeviceAdress));


        editor.commit();

    }



    public static void loadPreferences(SharedPreferences sharedPrefs) {
        Log.i(internalClassName, "loadPreferences");

        usbDeviceID = mathIntegerFromString(sharedPrefs.getString("usbDeviceID", "0"), 0);
        usbVendorID = mathIntegerFromString(sharedPrefs.getString("usbVendorID", "0"), 0);
        usbProductID = mathIntegerFromString(sharedPrefs.getString("usbProductID", "0"), 0);


        link_code = sharedPrefs.getString("link_code", "");
        picturePath = sharedPrefs.getString("picturePath", "");
        blueToothDeviceAdress = sharedPrefs.getString("blueToothDeviceAdress", "x:x:x:x");

        deviceType = mathIntegerFromString(sharedPrefs.getString("deviceType", "0"), 0);
        //Ethernet
        deviceIP = sharedPrefs.getString("deviceIP", "192.168.0.98");
        devicePort = mathIntegerFromString(sharedPrefs.getString("devicePort", "9100"), 9100);
        logoProcesed = sharedPrefs.getString("logoProcesed", "");


        Log.i(internalClassName, "               deviceType=" + String.valueOf(deviceType));
        Log.i(internalClassName, "               deviceIP=" + String.valueOf(deviceIP));
        Log.i(internalClassName, "               devicePort=" + String.valueOf(devicePort));
        Log.i(internalClassName, "               usbDeviceID=" + String.valueOf(usbDeviceID));
        Log.i(internalClassName, "               blueToothDeviceAdress=" + String.valueOf(blueToothDeviceAdress));


    }


    public static void loadPreferenceskitchen(SharedPreferences sharedPrefs) {
        Log.i(internalClassName, "loadPreferences");


        usbDeviceID = mathIntegerFromString(sharedPrefs.getString("usbDeviceID", "0"), 0);
        usbVendorID = mathIntegerFromString(sharedPrefs.getString("usbVendorID", "0"), 0);
        usbProductID = mathIntegerFromString(sharedPrefs.getString("usbProductID", "0"), 0);


        link_code = sharedPrefs.getString("link_code", "");
        picturePath = sharedPrefs.getString("picturePath", "");
        blueToothDeviceAdress = sharedPrefs.getString("blueToothDeviceAdress", "x:x:x:x");

        deviceType = mathIntegerFromString(sharedPrefs.getString("deviceType", "0"), 0);
        //Ethernet
        deviceIP = sharedPrefs.getString("deviceIP", "192.168.0.98");
        devicePort = mathIntegerFromString(sharedPrefs.getString("devicePort", "9100"), 9100);
        logoProcesed = sharedPrefs.getString("logoProcesed", "");


        Log.i(internalClassName, "               deviceType=" + String.valueOf(deviceType));
        Log.i(internalClassName, "               deviceIP=" + String.valueOf(deviceIP));
        Log.i(internalClassName, "               devicePort=" + String.valueOf(devicePort));
        Log.i(internalClassName, "               usbDeviceID=" + String.valueOf(usbDeviceID));
        Log.i(internalClassName, "               blueToothDeviceAdress=" + String.valueOf(blueToothDeviceAdress));


    }

    public static Integer mathIntegerFromString(String inti, Integer defi) {
        //used for preferences
        Integer returi = defi;
        if (!inti.equals("")) {
            try {
                returi = Integer.valueOf(inti);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }

        return returi;

    }




    public static String prepareDataToPrint(String data) {
        //ESC POS Comamnds convertions


        data = data.replace("$bold$", "·27·E·1·");
        data = data.replace("$unbold$", "·27·E·0·");


        data = data.replace("$intro$", "·13··10·");
        data = data.replace("$cut$", "·27·m");
        data = data.replace("$cutt$", "·27·i");

        data = data.replace("$al_left$", "·27·a·0·");
        data = data.replace("$al_center$", "·27·a·1·");
        data = data.replace("$al_right$", "·27·a·2·");

        data = data.replace("$small$", "·27·!·1·");
        data = data.replace("$smallh$", "·27·!·17·");
        data = data.replace("$smallw$", "·27·!·33·");
        data = data.replace("$smallhw$", "·27·!·49·");

        data = data.replace("$smallu$", "·27·!·129·");
        data = data.replace("$smalluh$", "·27·!·145·");
        data = data.replace("$smalluw$", "·27·!·161·");
        data = data.replace("$smalluhw$", "·27·!·177·");

        data = data.replace("$big$", "·27·!·0·");
        data = data.replace("$bigh$", "·27·!·16·");
        data = data.replace("$bigw$", "·27·!·32·");
        data = data.replace("$bighw$", "·27·!·48·");
        data = data.replace("$bigl$", "·27·!·12·");
        data = data.replace("$bigu$", "·27·!·128·");
        data = data.replace("$biguh$", "·27·!·144·");
        data = data.replace("$biguw$", "·27·!·160·");
        data = data.replace("$biguhw$", "·27·!·176·");

        data = data.replace("$drawer$", "·27··112··48··200··100·");
        data = data.replace("$drawer2$", "·27··112··49··200··100·");


        data = data.replace("$enter$", "·13··10·");

        data = data.replace("$letrap$", "·27·!·1·");
        data = data.replace("$letraph$", "·27·!·17·");
        data = data.replace("$letrapw$", "·27·!·33·");
        data = data.replace("$letraphw$", "·27·!·49·");

        data = data.replace("$letrapu$", "·27·!·129·");
        data = data.replace("$letrapuh$", "·27·!·145·");
        data = data.replace("$letrapuw$", "·27·!·161·");
        data = data.replace("$letrapuhw$", "·27·!·177·");


        data = data.replace("$letrag$", "·27·!·0·");
        data = data.replace("$letragh$", "·27·!·16·");
        data = data.replace("$letragw$", "·27·!·32·");
        data = data.replace("$letraghw$", "·27·!·48·");

        data = data.replace("$letragu$", "·27·!·128·");
        data = data.replace("$letraguh$", "·27·!·144·");
        data = data.replace("$letraguw$", "·27·!·160·");
        data = data.replace("$letraguhw$", "·27·!·176·");


        data = data.replace("$letracorte$", "·27·m");
        data = data.replace("$LETRACORTE$", "·27·m");


        data = data.replace("$ENTER$", "·13··10·");

        data = data.replace("$LETRAP$", "·27·!·1·");
        data = data.replace("$LETRAPH$", "·27·!·17·");
        data = data.replace("$LETRAPW$", "·27·!·33·");
        data = data.replace("$LETRAPHW$", "·27·!·49·");

        data = data.replace("$LETRAPU$", "·27·!·129·");
        data = data.replace("$LETRAPUH$", "·27·!·145·");
        data = data.replace("$LETRAPUW$", "·27·!·161·");
        data = data.replace("$LETRAPUHW$", "·27·!·177·");


        data = data.replace("$LETRAG$", "·27·!·0·");
        data = data.replace("$LETRAGH$", "·27·!·16·");
        data = data.replace("$LETRAGW$", "·27·!·32·");
        data = data.replace("$LETRAGHW$", "·27·!·48·");

        data = data.replace("$LETRAGU$", "·27·!·128·");
        data = data.replace("$LETRAGUH$", "·27·!·144·");
        data = data.replace("$LETRAGUW$", "·27·!·160·");
        data = data.replace("$LETRAGUHW$", "·27·!·176·");



        data = data.replace("á", "·160·");
        data = data.replace("à", "·133·");
        data = data.replace("â", "·131·");
        data = data.replace("ä", "·132·");
        data = data.replace("å", "·134·");

        data = data.replace("Á", "·193·");
        data = data.replace("À", "·192·");
        data = data.replace("Â", "·194·");
        data = data.replace("Ä", "·142·");
        data = data.replace("Å", "·143·");


        data = data.replace("é", "·130·");
        data = data.replace("è", "·138·");
        data = data.replace("ê", "·136·");
        data = data.replace("ë", "·137·");
        //data = data.replace("","··");


        data = data.replace("É", "·144·");
        data = data.replace("È", "··");
        data = data.replace("Ê", "··");
        data = data.replace("Ë", "··");

        data = data.replace("ñ", "·164·");
        data = data.replace("Ñ", "·165·");

        //data = data.replace("","··");


        data = data.replace("í", "··");
        data = data.replace("ì", "·141·");
        data = data.replace("î", "·140·");
        data = data.replace("ï", "·139·");
        //data = data.replace("","··");


        data = data.replace("ó", "·149·");
        data = data.replace("ò", "··");
        data = data.replace("ô", "·147·");
        data = data.replace("ö", "·148·");
        //data = data.replace("","··");

        data = data.replace("Ó", "··");
        data = data.replace("Ò", "··");
        data = data.replace("Ô", "··");
        data = data.replace("Ö", "·153·");
        //data = data.replace("","··");

        data = data.replace("ú", "··");
        data = data.replace("ù", "·151·");
        data = data.replace("û", "··");
        data = data.replace("ü", "·129·");
        //data = data.replace("","··");


        data = data.replace("ú", "··");
        data = data.replace("ù", "·151·");
        data = data.replace("û", "··");
        data = data.replace("ü", "·129·");

        //data = data.replace("..", "");

        for (int l = 0; l <= 255; l++) {
            data = data.replace("·" + String.valueOf(l) + "·", String.valueOf(((char) l)));
            data = data.replace("$codepage" + String.valueOf(l) + "$",  String.valueOf(((char) 27)) + "t" + String.valueOf(((char) l)));
        }
        Log.d("here----", ""+data);

        return data;
    }


    // Get var from data text
    public static String getVble(String data, String param, String defaultValue) {
        String vble = defaultValue;
        int found_start = data.indexOf("$" + param + "=");
        if (found_start > -1) {
            found_start = found_start + ("$" + param + "=").length();
            String substr = data.substring(found_start);
            int found_end = substr.indexOf("$");
            vble = data.substring(found_start, found_start + found_end);
        }
        return vble;
    }

    public static byte[] stringToBytesASCII(String input) {
        return stringToBytesASCIIOK(input);
        // return createImageFromText(100, 500,18 ,input, "Center");
    }

    public static Bitmap createImageFromText() {

        String qty = "4";
        ReceiptBuilder receipt = new ReceiptBuilder(500);
        receipt.setMargin(20, 20).
                setAlign(Paint.Align.CENTER).
                setColor(Color.BLACK).
                setTextSize(20).
                addText("RoyalPOS").
                addBlankSpace(10).
                setTextSize(14).
                addBlankSpace(5).
                addText("Raj World, Palanpur Canal Road, Surat").
                addBlankSpace(5).
                addText("Phone: 9909777668").
                addBlankSpace(5).
                addText("GST# 5456465456465").
                addBlankSpace(5).
                addText("Order By : Utpal").
                addBlankSpace(5).
                addText("----------------------------\n").
                addBlankSpace(5).
                addText("TOKEN # "+203+"\t"+ "dine in" +"\n").
                addBlankSpace(5).
                addText("----------------------------\n").
                addBlankSpace(5).
                addText(padLine(qty + "    جبن مايو فريز", 120 + "", 40, " ") + "\n").
                addBlankSpace(5).
                addText("--------------------------------------\n").

                addBlankSpace(5).
                addText("SUBTOTAL                    "+480+"\n").
                addBlankSpace(5).
                addText("CGST @"+6+"%                    "+29+"\n").
                addBlankSpace(5).
                addText("SGST @"+6+"%                    "+29+"\n").
                addBlankSpace(5).


                addText("TOTAL       538"+"\n").
                addBlankSpace(5).
                addText("CASH                         600"+"\n").
                addBlankSpace(5).
                addText("CHANGE                     "+62+"\n").
                addBlankSpace(5).
                addText("--------------------------------------\n").
                addBlankSpace(5).

                addText("Thank you  "+"Visit Again\n");


            imgbm = receipt.build();



                SaveImage(receipt.build(), "file.png");
        return receipt.build();
    }

    public static String getSunmiDemoText(Context context) {
        String dataToPrint = "";

        StringBuilder textData = new StringBuilder();

        textData.append(M.getBrandName(context)+"\n");
        textData.append(M.getRestAddress(context)+"\n");
        textData.append("Phone: "+ M.getRestPhoneNumber(context)+"\n");
        textData.append("GST# "+ M.getGST(context)+"\n");
        textData.append("--------------------------------------"+"\n");
        textData.append("Order By : "+ M.getWaitername(context)+"\n");


        textData.append(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"\n");
        textData.append("ORDER # 1025555555555"+"\n");

        textData.append("----------------------------\n");
        textData.append("TOKEN # "+203+"\t"+ "dine in" +"\n");



        textData.append("----------------------------\n");


        String qty = "4";
        textData.append(padLine(qty + "CHEESE MAYO FRIES", 120 + "", 40, " ") + "\n");

        textData.append("--------------------------------------\n");


        textData.append("SUBTOTAL                    "+480+"\n");
        textData.append("CGST @"+6+"%                    "+29+"\n");
        textData.append("SGST @"+6+"%                    "+29+"\n");



        textData.append("TOTAL       538"+"\n");

        textData.append("CASH                         600"+"\n");
        textData.append("CHANGE                     "+62+"\n");

        textData.append("--------------------------------------\n");


        textData.append("Thank you  "+"Visit Again\n");

       
        dataToPrint = textData.toString();

        starprinter = textData.toString();
        return dataToPrint;
    }

    // generate demo text
    public static String getDemoText(Resources resources, Context context,String papersize) {
        PrintFormat pf=new PrintFormat(context);
        pf.setPaperSize(papersize);
        String dataToPrint = "";
        StringBuilder textData = new StringBuilder();
        textData.append("$codepage3$");
        textData.append("$al_center$$al_center$");

        textData.append("$big$"+pf.padLine3(M.getBrandName(context),"")+"$intro$"+"\n");

        textData.append(pf.padLine2(M.getRestAddress(context),"")+"$intro$"+"\n");
        textData.append(pf.padLine2("Phone: "+ M.getRestPhoneNumber(context),"")+"$intro$"+"\n");
        textData.append(pf.padLine2("GST# "+ M.getGST(context),"")+"$intro$"+"\n");
        textData.append(pf.divider()+"\n");
        textData.append(pf.padLine2("Order By : "+ M.getWaitername(context),"")+"\t $intro$"+"\n");


        textData.append(pf.padLine2(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()),"")+"\n");
        textData.append(pf.padLine2("ORDER # 1025555555555","")+"\n");

        textData.append(pf.divider()+"\n");
        textData.append(pf.padLine2("TOKEN # "+203, "dine in") +"\n");



        textData.append(pf.divider()+"\n");

        textData.append(pf.padLine2("Order comment \t",""));
        textData.append(pf.padLine2("need spicy" ,"")+ "\n");

        textData.append(pf.divider()+"\n");


        String qty = "1";
        textData.append(pf.padLine1(" CHEESE MAYO FRIES", qty , 120 + "", "120") + "\n");
        textData.append(pf.padLine1(" CHEESE MAYO FRIES", qty ,120 + "", "120") + "\n");
        textData.append(pf.padLine1(" CHEESE MAYO FRIES", qty ,120 + "", "120") + "\n");
        textData.append(pf.padLine1(" CHEESE MAYO FRIES", qty ,120 + "", "120") + "\n");


        textData.append(pf.divider()+"\n");


        textData.append(pf.padLine2("SUBTOTAL",""+480)+"\n");
        textData.append(pf.padLine2("CGST @"+6+"%",29+"")+"\n");
        textData.append(pf.padLine2("SGST @"+6+"%",29+"")+"\n");



        textData.append(pf.padLine3("TOTAL","538")+"\n");



        textData.append(pf.padLine2("CASH","600")+"\n");
        textData.append(pf.padLine2("CHANGE",62+"")+"\n");

        textData.append(pf.divider()+"\n");


        textData.append(pf.padLine2("Thank you ","")+pf.padLine2("Visit Again","")+"\n");


        textData.append("$intro$$intro$$intro$$intro$$cutt$$intro$");

        dataToPrint = textData.toString();


        return dataToPrint;
    }

    protected static String padLine(@Nullable String partOne, @Nullable String partTwo, int columnsPerLine, String str){
        if(partOne == null) {partOne = "";}
        if(partTwo == null) {partTwo = "";}
        if(str == null) {str = " ";}
        String concat;
        if((partOne.length() + partTwo.length()) > columnsPerLine) {
            concat = partOne + str + partTwo;
        } else {
            int padding = columnsPerLine - (partOne.length() + partTwo.length());
            concat = partOne + repeat(str, padding) + partTwo;
        }
        return concat;
    }

    protected static String repeat(String str, int i){
        return new String(new char[i]).replace("\0", str);
    }

    public static final byte GS =  0x1D;// Group separator
    //get actual logo data
//    public static byte[] getImageData(Bitmap bitmap) {
//
//        SaveImage(bitmap, "invoice");
//
//
//
//    }


    public static byte[] stringToBytesASCIIOK(String str) {
        String TAG = "posprinterdriver_AsyncNetPrint";

        byte[] b = new byte[0];
        try {
            b = str.getBytes("US-ASCII");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < b.length; i++) {

            b[i] = (byte) str.charAt(i);
            Log.i("", "                CAdena encontrada=" + i + " ->parte1=" + str.charAt(i) + " ->parte1=" + (int) str.charAt(i));

        }

        return b;

    }

    public static String SaveImage(Bitmap finalBitmap, String name) {

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/saved_images/");
        myDir.mkdirs();
        String fname = name;
        File file = new File(myDir, fname);

        try {

                FileOutputStream out = new FileOutputStream(file);
                finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                out.flush();
                out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return file.toString();
    }

    public static boolean get_status_licence() {
        return mSubscribedToYearlyUsed;
    }




    public static String getImageDataPosPrinterLogo(Bitmap bm) {
        String INITIALIZE_PRINTER = "·27··64·";//Chr(&H1B) & Chr(&H40);
        String PRINT_AND_FEED_PAPER = "·10·";//Chr(&HA);
        String CMDSELECT_BIT_IMAGE_MODE = "·27··42·";//Chr(&H1B) & Chr(&H2A);
        String SET_LINE_SPACING = "·27··51·";//Chr(&H1B) & Chr(&H33);
        String SETLINESPACING24 = SET_LINE_SPACING + "·24·";

        BitmapFactory.Options options_load = new BitmapFactory.Options();
        options_load.inScaled = false;
        Bitmap miImagetest = null;
        Bitmap miImage = bm;



        int image_h = miImage.getHeight();
        int p_Width = miImage.getWidth();


        String widthLSB = "";
        String widthMSB = "";
        String hexdata = "0000" + Integer.toHexString(p_Width);
        widthLSB = hexdata.substring(hexdata.length() - 2, hexdata.length());
        widthMSB = hexdata.substring(hexdata.length() - 4, hexdata.length() - 2);


        String SELECTBITIMAGEMODE = CMDSELECT_BIT_IMAGE_MODE + "·33··" + Integer.parseInt(widthLSB, 16) + "··" + Integer.parseInt(widthMSB, 16) + "·";// & Chr(33) & Chr(widthLSB) & Chr(widthMSB)


        String p_TxtPrint = INITIALIZE_PRINTER;
        p_TxtPrint = p_TxtPrint + SETLINESPACING24;
        String p_DatatoPrint = "";
        int offset = 0;
        while (offset < image_h) {
            int imageDataLineIndex = 0;
            int p_BytesAncho = 3 * p_Width - 1;
            int[] imageDataLine = new int[p_BytesAncho + 10];//(3 * (image_w + 10))];
            for (int X = 0; X < p_Width; X++) {
                //' Remember, 24 dots = 24 bits = 3 bytes.
                //' The ' k ' variable keeps track of which of those
                //' three bytes that we' re currently scribbling into.
                for (int K = 0; K <= 2; K++) {
                    int slice = 0;
                    String strSlice = "";
                    //' A byte is 8 bits. The ' b ' variable keeps track
                    //' of which bit in the byte we' re recording.
                    for (int b = 0; b <= 7; b++) {
                        //' Calculate the y position that we' re currently
                        //' trying to draw. We take our offset, divide it
                        //' by 8 so we' re talking about the y offset in
                        //' terms of bytes, add our current ' k ' byte
                        //' offset to that, multiple by 8 to get it in terms
                        //' of bits again, and add our bit offset to it.
                        int zz2 = abs(offset / 8);
                        int y = ((zz2 + K) * 8) + b;
                        int I = (y * (p_Width)) + (X);
                        //int y = (((abs(offset / 8)) + K) * 8) + b;
                        //' Calculate the location of the pixel we want in the bit array  It' ll be at(y * width) + x.
                        // int I = (y * p_Width) + X;
                        // ' If the image (or this stripe of the image)                         ' is shorter than 24 dots, pad with zero.
                        boolean v = false;

                        if (I <= (image_h * p_Width)) {
                            //Log.e(internalClassName, "IMAGEN= getImageData x,y:" + String.valueOf(X) + "," + String.valueOf(y));
                            if (y < image_h) {
                                int midato = miImage.getPixel(X, y);
                                int alfa = (midato >> 24) & 0xff;     //bitwise shifting

                                int R = (midato >> 16) & 0xff;     //bitwise shifting
                                int G = (midato >> 8) & 0xff;
                                int B = midato & 0xff;
                                double total = abs(0.2126 * R + 0.7152 * G + 0.0722 * B);

                                // Log.e(internalClassName, "valores:  total===" + String.valueOf(total)  + "->%="  );
                                if (total > 80) {
                                    v = true;
                                }
                            } else {
                                v = true;
                            }
                        } else {
                            v = true;
                        }
                        if (v == true) {
                            strSlice = strSlice + "0";
                        } else {
                            strSlice = strSlice + "1";
                        }
                    }

                    imageDataLine[imageDataLineIndex + K] = Integer.parseInt(strSlice, 2);
                }
                imageDataLineIndex = imageDataLineIndex + 3;
            }
            String p_dataBytes = "";
            for (int XX = 0; XX < imageDataLineIndex; XX++) {
                p_dataBytes = p_dataBytes + String.valueOf(((char) imageDataLine[XX]));
            }
            p_DatatoPrint = p_DatatoPrint + SELECTBITIMAGEMODE + p_dataBytes + PRINT_AND_FEED_PAPER;

            offset = offset + 24;
        }

        p_TxtPrint = p_TxtPrint + p_DatatoPrint + SETLINESPACING24 + INITIALIZE_PRINTER;
        p_TxtPrint = p_TxtPrint + "·29··60·";//& Chr( & H1D)&Chr( & H3C)

        return p_TxtPrint;
    }

    private static byte RGB2Gray(int r, int g, int b) {
        return (false ? ((int) (0.29900 * r + 0.58700 * g + 0.11400 * b) > 200)
                : ((int) (0.29900 * r + 0.58700 * g + 0.11400 * b) < 200)) ? (byte) 1 : (byte) 0;
    }


    public static String decodeBitmap(Bitmap bmp){


        int bmpWidth = bmp.getWidth();
        int bmpHeight = bmp.getHeight();

        List<String> list = new ArrayList<String>(); //binaryString list
        StringBuffer sb;


        int bitLen = bmpWidth / 8;
        int zeroCount = bmpWidth % 8;

        String zeroStr = "";
        if (zeroCount > 0) {
            bitLen = bmpWidth / 8 + 1;
            for (int i = 0; i < (8 - zeroCount); i++) {
                zeroStr = zeroStr + "0";
            }
        }

        for (int i = 0; i < bmpHeight; i++) {
            sb = new StringBuffer();
            for (int j = 0; j < bmpWidth; j++) {
                int color = bmp.getPixel(j, i);

                int r = (color >> 16) & 0xff;
                int g = (color >> 8) & 0xff;
                int b = color & 0xff;

                // if color close to white，bit='0', else bit='1'
                if (r > 160 && g > 160 && b > 160)
                    sb.append("0");
                else
                    sb.append("1");
            }
            if (zeroCount > 0) {
                sb.append(zeroStr);
            }
            list.add(sb.toString());
        }

        List<String> bmpHexList = binaryListToHexStringList(list);
        String commandHexString = "1D763000";
        String widthHexString = Integer
                .toHexString(bmpWidth % 8 == 0 ? bmpWidth / 8
                        : (bmpWidth / 8 + 1));
        if (widthHexString.length() > 2) {
            Log.e("decodeBitmap error", " width is too large");
            return null;
        } else if (widthHexString.length() == 1) {
            widthHexString = "0" + widthHexString;
        }
        widthHexString = widthHexString + "00";

        String heightHexString = Integer.toHexString(bmpHeight);
        if (heightHexString.length() > 2) {
            Log.e("decodeBitmap error", " height is too large");
            return null;
        } else if (heightHexString.length() == 1) {
            heightHexString = "0" + heightHexString;
        }
        heightHexString = heightHexString + "00";

        List<String> commandList = new ArrayList<String>();
        commandList.add(commandHexString+widthHexString+heightHexString);
        commandList.addAll(bmpHexList);

        return commandList.toString();
    }


    public static List<String> binaryListToHexStringList(List<String> list) {
        List<String> hexList = new ArrayList<String>();
        for (String binaryStr : list) {
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < binaryStr.length(); i += 8) {
                String str = binaryStr.substring(i, i + 8);

                String hexString = myBinaryStrToHexString(str);
                sb.append(hexString);
            }
            hexList.add(sb.toString());
        }
        return hexList;

    }

    private static String[] binaryArray = { "0000", "0001", "0010", "0011",
            "0100", "0101", "0110", "0111", "1000", "1001", "1010", "1011",
            "1100", "1101", "1110", "1111" };

    private static String hexStr = "0123456789ABCDEF";
    public static String myBinaryStrToHexString(String binaryStr) {
        String hex = "";
        String f4 = binaryStr.substring(0, 4);
        String b4 = binaryStr.substring(4, 8);
        for (int i = 0; i < binaryArray.length; i++) {
            if (f4.equals(binaryArray[i]))
                hex += hexStr.substring(i, i + 1);
        }
        for (int i = 0; i < binaryArray.length; i++) {
            if (b4.equals(binaryArray[i]))
                hex += hexStr.substring(i, i + 1);
        }

        return hex;
    }
}

