package com.royalpos.waiter.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.gson.JsonObject;
import com.royalpos.waiter.R;
import com.royalpos.waiter.helper.AppConst;
import com.royalpos.waiter.helper.ConnectionDetector;
import com.royalpos.waiter.model.M;
import com.royalpos.waiter.model.TransactionPojo;
import com.royalpos.waiter.webservices.APIServiceHeader;
import com.royalpos.waiter.webservices.OrderAPI;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Response;

public class OnlineTransDialog extends Dialog implements View.OnClickListener {

    ImageView ivqrcode,ivclose;
    LinearLayout llqrcode;
    Button btnphpe,btnprint,btnclose,btnSuccess,btnmanual;
    TextView tvtitle,tvorderno,tvamount,tvcollection;
    Context context;
    String TAG="OnlineTransDialog";
    public String transJson;
    TransactionPojo transactionPojo;
    Dialog mDialog;

    public OnlineTransDialog(@NonNull Context context, View.OnClickListener printClick, View.OnClickListener successClick) {
        super(context,android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_phonepe);
        this.context=context;
        getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        ivqrcode=(ImageView)findViewById(R.id.ivqrcode);
        ivclose=(ImageView)findViewById(R.id.ivclose);
        btnphpe=(Button)findViewById(R.id.btndone);
        btnphpe.setTypeface(AppConst.font_regular(context));
        btnprint=(Button)findViewById(R.id.btnprint);
        btnprint.setTypeface(AppConst.font_regular(context));
        btnclose=(Button)findViewById(R.id.btnclose);
        btnclose.setTypeface(AppConst.font_regular(context));
        btnmanual=(Button)findViewById(R.id.btnmanual);
        btnmanual.setTypeface(AppConst.font_regular(context));
        tvorderno=(TextView)findViewById(R.id.tvorderno);
        tvamount=(TextView)findViewById(R.id.tvamount);
        tvcollection=(TextView)findViewById(R.id.tvcollection);
        tvtitle=(TextView)findViewById(R.id.tvtitle);
        tvtitle.setText(R.string.title_phonepe);
        llqrcode=(LinearLayout)findViewById(R.id.llqrcode);
        btnSuccess=new Button(context);

        btnprint.setOnClickListener(printClick);
            btnSuccess.setOnClickListener(successClick);
            btnphpe.setOnClickListener(this);
            btnclose.setOnClickListener(this);
            ivclose.setOnClickListener(this);
            btnmanual.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v==ivclose || v==btnclose) {
            cancelPayment();
        }else if(v==btnphpe){
            checkStatus();
        }else if(v==btnmanual){
            mDialog=new Dialog(context);
            mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            mDialog.setContentView(R.layout.dialog_confirm_trans);
            final EditText ettrans=mDialog.findViewById(R.id.ettrans);
            ettrans.setTypeface(AppConst.font_regular(context));
            TextView btnyes=mDialog.findViewById(R.id.tvyes);
            TextView btnno=mDialog.findViewById(R.id.tvno);

            btnyes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!ettrans.getText().toString().isEmpty()){
                        JSONObject rJSON=new JSONObject();
                        try {
                            if (llqrcode.getVisibility() == View.VISIBLE)
                                rJSON.put("type", "phonepe_upi");
                            else
                                rJSON.put("type", "phonepe");
                            rJSON.put("token",ettrans.getText().toString());
                            transJson = rJSON.toString();
                            btnSuccess.performClick();
                        }catch (JSONException e){}
                    }else{
                        transJson=null;
                        btnSuccess.performClick();
                    }
                    mDialog.dismiss();
                }
            });

            btnno.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                }
            });
            mDialog.show();
        }
    }

    public void setData(String order_no, String amount, Bitmap qrbitmap,TransactionPojo transRes){
        transactionPojo=transRes;
        tvorderno.setText(context.getString(R.string.invoice_no)+" # "+order_no);
        tvamount.setText(AppConst.currency+amount);

        if(qrbitmap!=null) {
            ivqrcode.setImageBitmap(qrbitmap);
            llqrcode.setVisibility(View.VISIBLE);
            tvcollection.setVisibility(View.GONE);
        }else{
            tvcollection.setVisibility(View.VISIBLE);
            llqrcode.setVisibility(View.GONE);
        }
    }

    private void checkStatus(){
        ConnectionDetector connectionDetector=new ConnectionDetector(context);
        if(connectionDetector.isConnectingToInternet()){
            M.showLoadingDialog(context);
            OrderAPI mAuthenticationAPI = APIServiceHeader.createService(context, OrderAPI.class);
            Call<JsonObject> call = mAuthenticationAPI.checkPhonePeStatus(M.getRestID(context),M.getRestUniqueID(context),transactionPojo.getData().getTransactionId());
            call.enqueue(new retrofit2.Callback<JsonObject>(){
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    M.hideLoadingDialog();
                    JsonObject res=response.body();
                    String state="";
                    if(res!=null && res.has("data")){
                        JsonObject dj=res.get("data").getAsJsonObject();
                        if(dj.has("paymentState"))
                            state=dj.get("paymentState").getAsString();
                    }
                    if(state!=null && res.get("success").getAsBoolean() && state.equalsIgnoreCase("COMPLETED")){
                        try {
                            JSONObject rJSON = new JSONObject(res.toString());
                            JSONObject dJSON=rJSON.getJSONObject("data");
                            if(llqrcode.getVisibility()==View.VISIBLE)
                                rJSON.put("type","phonepe_upi");
                            else
                                rJSON.put("type","phonepe");
                            if(dJSON!=null && dJSON.has("providerReferenceId"))
                                rJSON.put("token",dJSON.getString("providerReferenceId"));
                            transJson = rJSON.toString();
                            Log.d(TAG,"phone res:"+transJson);
                            btnSuccess.performClick();
                        }catch (JSONException e){}
                    }else if(res.has("message"))
                        Toast.makeText(context,res.get("message").getAsString(),Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    M.hideLoadingDialog();
                }
            });
        }else
            Toast.makeText(context,R.string.no_internet_alert,Toast.LENGTH_SHORT).show();
    }

    private void cancelPayment(){
        ConnectionDetector connectionDetector=new ConnectionDetector(context);
        if(connectionDetector.isConnectingToInternet()){
            M.showLoadingDialog(context);
            OrderAPI mAuthenticationAPI = APIServiceHeader.createService(context, OrderAPI.class);
            Call<TransactionPojo> call = mAuthenticationAPI.cancelPayment(M.getRestID(context),M.getRestUniqueID(context),transactionPojo.getData().getTransactionId());
            call.enqueue(new retrofit2.Callback<TransactionPojo>(){
                @Override
                public void onResponse(Call<TransactionPojo> call, Response<TransactionPojo> response) {
                    M.hideLoadingDialog();
                    TransactionPojo res=response.body();
                    if(res!=null) {
                        if (res.getSuccess().equals("4")) {
                            checkStatus();
                        } else if (res.getMessage() != null && !res.getMessage().isEmpty()) {
                            Toast.makeText(context, res.getMessage(), Toast.LENGTH_SHORT).show();
                            dismiss();
                        }else
                            dismiss();
                    }
                }

                @Override
                public void onFailure(Call<TransactionPojo> call, Throwable t) {
                    M.hideLoadingDialog();
                }
            });
        }else
            Toast.makeText(context,R.string.no_internet_alert,Toast.LENGTH_SHORT).show();
    }

    public void onDismiss(){
        if(mDialog!=null && mDialog.isShowing())
            mDialog.dismiss();
    }

}
