package com.royalpos.waiter.dialog.alertdialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.royalpos.waiter.R;
import com.royalpos.waiter.helper.AppConst;
import com.royalpos.waiter.helper.ConnectionDetector;
import com.royalpos.waiter.model.OfflineOrder;

public class PhonePeAlertDialog extends Dialog {

    TextView btnyes,btnno;
    public CheckBox cbqrcode;
    String TAG="PhonePeAlertDialog";
    Context context;
    ConnectionDetector connectionDetector;
    OfflineOrder offlineOrder;

    public PhonePeAlertDialog(@NonNull Context context, OfflineOrder order, View.OnClickListener posClick) {
        super(context);
        setContentView(R.layout.phonepe_alert_dialog);
        this.context=context;
        this.connectionDetector=new ConnectionDetector(context);
        this.offlineOrder=order;

        btnyes=findViewById(R.id.btnyes);
        btnyes.setOnClickListener(posClick);
        btnno=findViewById(R.id.btnno);
        cbqrcode=findViewById(R.id.cbqrcode);
        cbqrcode.setTypeface(AppConst.font_regular(context));

        btnno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
