package com.royalpos.waiter.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.provider.Settings;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.royalpos.waiter.POServerService;
import com.royalpos.waiter.R;
import com.royalpos.waiter.helper.AppConst;

public class JoinServerDialog extends Dialog {
    Context context;
    AppCompatActivity activity;
    LinearLayout llwifi,llserver;

    public JoinServerDialog(final Context context, final AppCompatActivity activity, View.OnClickListener logoutClick) {
        super(context);
        this.context=context;
        this.activity=activity;
        setCancelable(false);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_server_connection);

        Button btnjoin = (Button) findViewById(R.id.btnjoin);
        btnjoin.setTypeface(AppConst.font_regular(context));
        Button btnlogout = (Button) findViewById(R.id.btnlogout);
        btnlogout.setTypeface(AppConst.font_regular(context));
        btnlogout.setVisibility(View.GONE);
        Button btnsetting=(Button)findViewById(R.id.btnsettings);
        btnsetting.setTypeface(AppConst.font_regular(context));

        llwifi=(LinearLayout)findViewById(R.id.llwifi);
        llserver=(LinearLayout)findViewById(R.id.llserver);
        final EditText etip=(EditText)findViewById(R.id.etip);
        etip.setTypeface(AppConst.font_regular(context));
      //  etip.setText("192.168.0.131");
        checkWifi();

        if(logoutClick!=null) {
            btnlogout.setVisibility(View.VISIBLE);
            btnlogout.setOnClickListener(logoutClick);
        }

        btnjoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                if (wifi.isWifiEnabled()) {
                    if(etip.getText().toString().isEmpty()){
                        etip.setError("IP Address required");
                    }else {
                        Boolean isPOS = AppConst.isMyServiceRunning(POServerService.class, context);
                        if (isPOS)
                            context.stopService(new Intent(context, POServerService.class));


                        new Handler().postDelayed(new Runnable() {
                            public void run() {
                                Intent it = new Intent(context, POServerService.class);
                                it.setAction("ip:" + etip.getText().toString());
                                context.startService(it);
                            }
                        }, 2000);
                    }
                }else
                    checkWifi();
            }
        });

        btnsetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.startActivityForResult(new Intent(Settings.ACTION_WIFI_SETTINGS), 302);
            }
        });
    }

    public void checkWifi(){

        WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        if (wifi.isWifiEnabled()) {
            llwifi.setVisibility(View.GONE);
            llserver.setVisibility(View.VISIBLE);
        } else {
            llwifi.setVisibility(View.VISIBLE);
            llserver.setVisibility(View.GONE);
        }

    }
}
