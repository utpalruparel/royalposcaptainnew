package com.starmicronics.starprntsdk;

import android.os.Bundle;

import com.royalpos.waiter.R;

import androidx.appcompat.app.AppCompatActivity;

public class TitleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_title);
    }
}
