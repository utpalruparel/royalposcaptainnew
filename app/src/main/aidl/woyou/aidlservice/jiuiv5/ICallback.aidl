package woyou.aidlservice.jiuiv5;

interface ICallback {

	oneway void onRunResult(boolean isSuccess, int code, String msg);

}